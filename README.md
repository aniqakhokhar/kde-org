# kde.org

## Clone

```
svn co svn://anonsvn.kde.org/home/kde/trunk/www/sites/www kde-org
cd kde-org
git clone https://anongit.kde.org/websites/capacity.git media
```

## Dev

```
composer install
php -S localhost:8001 routing.php
```

