<?php
    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "KDE Project Awards"
    ]);

    require('../../aether/header.php');
    $site_root = "../../";
?>

<main class="container">

<h1>KDE Project Awards</h1>

<div id="quicklinks">
[ 
<a href="#2009">2009</a> |
<a href="#2008">2008</a> |
<a href="#2007">2007</a> |
<a href="#2006">2006</a> |
<a href="#2005">2005</a> |
<a href="#2004">2004</a> |
<a href="#2003">2003</a> |
<a href="#2002">2002</a> |
<a href="#2001">2001</a> |
<a href="#2000">2000</a> |
<a href="#1999">1999</a>
]
</div>

<h2><a name="2009"></a>Awards in 2009</h2>

  <h3>KDE voted Free Software Project of the Year</h3>
  <img align="right" alt="Linux Format Readers Choice Award 2009" src="images/LXF-Logo.png"  class="sideimage" />
  <p><a href="http://www.linuxformat.co.uk/">Linux Format</a> magazine has unveiled its annual Reader Awards (<a href="http://www.linuxformat.co.uk/pdfs/download.php?PDF=LXF115.awards.pdf">PDF</a>) for 2008 and KDE won a 'landslide' victory in the category of Free Software Project of the year in recognition of the 'incredible' work done with KDE 4. <a href="http://amarok.kde.org">Amarok</a>, <a href="http://www.konqueror.org">Konqueror</a>, Qt and the KDE-based Asus Eee PC were also recognised in the awards. <br />
  Read more in the <a href="http://dot.kde.org/2009/01/20/kde-voted-free-software-project-year">dot-article</a>.</p>

<h2><a name="2008"></a>Awards in 2008</h2>

  <h3>KDE-Applications win Linux Journal's Reader's Choice Award 2008</h3>

  <img align="right" alt="Linux Journal Readers Choice Award 2008" src="images/ReadersChoice2008_logo-75x125.png" class="sideimage" />
  <p>The readers of <a href="http://www.linuxjournal.com/">Linux Journal</a>
  chose the KDE-Applications <a href="http://amarok.kde.org/">Amarok</a>
  as their <a href="http://www.linuxjournal.com/article/10065">Favorite
  Audio Tool</a> and <a href="http://www.digikam.org/">digiKam</a> as
  their <a href="http://www.linuxjournal.com/article/10065">Favorite
  Digital Photo Management Tool</a>.</p>

  <h3>2008 LinuxQuestions.org Members' Choice Awards</h3>
  <img src="images/2008_desktop-environment-KDE.png" alt="LinuxQuestions Award 2008" align="right"  class="sideimage" />

  <p><a href="http://www.kde.org/">KDE</a> won again the <a href="http://www.linuxquestions.org/">LinuxQuestions.org</a> Members' Choice Award <a href="http://www.linuxquestions.org/questions/2008-linuxquestions.org-members-choice-awards-79/desktop-environment-of-the-year-610190/">Desktop
  Environment of the Year</a>.</p>

  <p>Not only the Desktop Enviroment recieved excellent remarks. Also some KDE-Applications were voted number one in their categories. Amarok is by far the most popular application in the category <a href="http://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/audio-media-player-application-of-the-year-610226/">Audio
  Media Player Application of the Year</a> and K3b is the favoured <a href="http://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/multimedia-utility-of-the-year-610224/">Multimedia
  Utility of the Year</a>.</p>

<h2><a name="2007"></a>Awards in 2007</h2>

  <h3>2007 LinuxQuestions.org Members' Choice Awards</h3>
  <img src="images/2007_desktop_btn.png" alt="LinuxQuestions Award 2007" align="right" class="sideimage" />
  <p><a href="http://www.kde.org/">KDE</a> is again <a href="http://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/desktop-environment-of-the-year-610190/">Desktop
  Environment of the Year</a>. The following KDE-Applications were
  voted number one in their categories by the members of
  <a href="http://www.linuxquestions.org/">LinuxQuestions.org</a>.
  Amarok (<a href="http://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/audio-media-player-application-of-the-year-610226/">Audio
  Media Player Application of the Year</a>), K3b (<a href="http://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/multimedia-utility-of-the-year-610224/">Multimedia
  Utility of the Year</a>) and Konqueror (<a href="http://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/file-manager-of-the-year-610231/">File
  Manager of the Year</a>).</p>

<h2><a name="2006"></a>Awards in 2006</h2>
  <h3>Linux Journal's Editor's Choice Award 2006</h3>
  <p>
    <img src="images/EditorsChoice2006-LinuxJournal-120x120.png" alt="Linux Journal Editors Choice 2006" align="right" class="sideimage" />
    KDE wins <a href="http://www.linuxjournal.com/">Linux Journal</a>'s
  <a href="http://www.linuxjournal.com/article/9368">Editors' Choice
  2006</a> award (Desktop Environment). The K Desktop Environment is
  lauded for the richness of the features, the user friendly and
  intuitive usability and the depths of it's power. KDE is the desktop
  with everything for casual users as well as for power users.</p>

  <h3>Free Software Project of the Year award 2006</h3>
  <img align="right" alt="Linux Format Readers Choice Award 2006" src="images/LXF-Logo.png" class="sideimage" />
  <p>KDE won the "Free Software Project of the
  Year award" in <a href="http://www.linuxformat.co.uk/">Linux
  Format's</a> Reader Awards.</p>

  <h3>2006 LinuxQuestions.org Members' Choice Awards</h3>
  <p>
    <img src="images/2006_desktop_btn.png" alt="LinuxQuestions Award 2006" align="right" class="sideimage" />
  The members of <a href="http://www.linuxquestions.org/">LinuxQuestions.org</a>
  voted KDE for <a href="http://www.linuxquestions.org/questions/2006-linuxquestions.org-members-choice-awards-76/desktop-environment-of-the-year-514945/">Desktop
  Environment of the Year</a>. Several other KDE-Applications received
  very good results. Winners in their categories are: amaroK (<a href="http://www.linuxquestions.org/questions/2006-linuxquestions.org-members-choice-awards-76/audio-media-player-application-of-the-year-514978/">Audio
  Media Player Application of the Year</a>), K3b (<a href="http://www.linuxquestions.org/questions/2006-linuxquestions.org-members-choice-awards-76/multimedia-utility-of-the-year-514982/">Multimedia
  Utility of the Year</a>) and Quanta (<a href="http://www.linuxquestions.org/questions/2006-linuxquestions.org-members-choice-awards-76/web-development-editor-of-the-year-514964/">Web
  Development Editor of the Year</a>).</p>

<h2><a name="2005"></a>Awards in 2005</h2>

  <h3>2005 LinuxQuestions.org Members' Choice Awards</h3>
  <img src="images/LQ-2005MCA_Desktop.png" alt="2005 LinuxQuestions.org Member's Choice Awards : KDE - Desktop Environment, amaroK - Audio Multimedia Application, Konqueror - File Manager and Quanta - Web Development Editor" class="sideimage" />
  <p>The members of <a href="http://www.linuxquestions.org/">LinuxQuestions.org</a>
  chose <a href="http://www.kde.org">KDE</a> as the <a href="http://www.linuxquestions.org/questions/t409028.html">Desktop Environment</a>,  <a href="http://amarok.kde.org">amaroK</a> as the <a href="http://www.linuxquestions.org/questions/t409046.html">Audio Multimedia Application</a>, <a href="http://www.konqueror.org">Konqueror</a> as <a href="http://www.linuxquestions.org/questions/t409055.html">File Manager</a> and 
  <a href="http://quanta.kdewebdev.org">Quanta</a> the <a href="http://www.linuxquestions.org/questions/t409044.html">Web Development Editor</a> for the year of 2005.</p>

  <h3>TUX 2005 Readers' Choice Award</h3>
  <img src="images/tux-readerschoice-winner.png" alt="TUX 2005 Readers' Choice Award" class="sideimage" />
   <p>The readers of <a href="http://www.tuxmagazine.com/">TUX Magazin</a> chose <a href="http://www.kde.org">KDE</a> as Favorite Desktop Environment and <a href="http://digikam.org">Digikam</a> as Favorite Digital Photo Management Tool in the <a href="http://www.tuxmagazine.com/node/1000151">TUX 2005 Readers' Choice Award</a>. </p>

  <h3>2005 Usenix Software Tools User Group Award</h3>
  <img src="images/usenix.png" alt="Usenix Software Tools User GroupAward" class="sideimage" />
   <p>The <a href="http://www.usenix.org/about/stug.html">Software Tools User Group Award</a> recognises significant contributions to the community.  The 2005 award was made jointly to Mattias Ettrich of KDE and Miguel de Icaza of Gnome for their contribution to desktop Unix.</p>

  <h3>Intevation Prize for Achievements in Free Software For Kalzium</h3>
  <p>Carsten Niehaus won the <a href="http://dot.kde.org/1139779450/">Intevation Prize for Achievements in Free Software</a> for his work on <a href="http://edu.kde.org/kalzium/">Kalzium</a>, KDE's interactive periodic table.
  </p>


<h2><a name="2004"></a>Awards in 2004</h2>

  <h3>2004 LinuxQuestions.org Members' Choice Awards</h3>
  <img src="images/lq_2004mca.png" alt="2004 LinuxQuestions.org Member's Choice Awards : KDE - Desktop Environment, KDevelop - IDE and  Konqueror - File Manager " class="sideimage"/>
  <p>The members of <a href="http://www.linuxquestions.org/">LinuxQuestions.org</a>
  chose <a href="http://www.kde.org/">KDE</a> as the <a href="http://www.linuxquestions.org/questions/t272100.html">Desktop Environment</a>,  <a href="http://www.kdevelop.org/">KDevelop</a> as the <a href="http://www.linuxquestions.org/questions/t272124.html">IDE</a>, <a href="http://www.konqueror.org/">Konqueror</a> as <a href="http://www.linuxquestions.org/questions/t272138.html">File Manager</a> and 
  <a href="http://quanta.kdewebdev.org">Quanta</a> the <a href="http://www.linuxquestions.org/questions/t272123.html">Web Development Editor</a> for the year of 2004.</p>

  <h3>Best Mail Client for Kontact/KMail</h3>
  <img src="images/lnm_award2004_logo.png" alt="Kontact/KMail Best Mail Client in Linux New Media Award 2004" class="sideimage" />
  <p><a href="http://www.linuxnewmedia.de/">Linux New Media</a>'s <a href="http://www.linuxnewmedia.de/Award_2004/en">Editor's Choice Award</a>
  for 2004 was granted to Kontact/KMail in the category "Best Mail Client".</p>

  <h3>Favorite Desktop Environment for KDE</h3>
  <img src="images/rc2004.png" alt="KDE best desktop environment in Linux Journal's 2004 Readers' Choice Award" class="sideimage" />
  <p><a href="http://www.linuxjournal.com/">Linux Journal</a>'s <a href="http://www.linuxjournal.com/article.php?sid=7724">Readers' Choice Award</a>
  for 2004 was granted to KDE in the category "Favorite Desktop Environment".</p>

  <h3>Best Desktop Environment for KDE 3.3</h3>
  <img src="images/openchoice2k4.png" alt="KDE best in Open Choice Awards 2004" class="sideimage" />
  <p><a href="http://www.ofb.biz">Open for Business</a>' <a href="http://www.ofb.biz/modules.php?name=News&amp;file=article&amp;sid=330">Open Choice Awards 2004</a>
  chose KDE to be awarded as "Best Desktop Environment"".</p>

  <h3>LinuxUser &amp; Developer Awards 2004</h3>
  <img src="images/ludex.png" alt="LinuxUser &amp; Developer Expo 2004 Best Desktop Environment" class="sideimage" />
  <p><a href="http://www.linuxuserexpo.com/">LinuxUser &amp; Developer Expo 2004</a> is one of the largest
  Linux-related shows in the UK. After being nominated for the award along with Ximian Desktop
  and Sun Java Desktop, KDE developer Richard Moore was proud to accept the award for
  "Best Desktop Environment" for KDE 3.2 at the awards ceremony.
  </p>


  <h3>2004 LinuxWorld Conference &amp; Expo Product Excellence Award</h3>
  <img src="images/lwce_award_logo_2004.png" alt="LinuxWorld Conference &amp; Expo - Product Excellence Awards" class="sideimage" />
  <p><a href="http://www.idgworldexpo.com">IDG World Expo</a>, the leading producer
  of world-class tradeshows, conferences and events for technology markets
  around the globe,
  <a href="http://www.businesswire.com/cgi-bin/f_headline.cgi?bw.012204/240225110">
  awarded KDevelop 3.0
  with the LinuxWorld Product Excellence Award for Best Development Tools</a>.
  The LinuxWorld Product Excellence Awards represent major areas of innovation
  in the Linux and open source community.
  </p>

<h2><a name="2003"></a>Awards in 2003</h2>

  <h3>LinuxWorld Magazine 2003 Readers' Choice Awards</h3>
  <img src="images/LWM_RCA_logo.png" alt="LinuxWorld Magazin - Readers' Choice Award" class="sideimage" />
  <p>The <a href="http://www.linuxworld.com">LinuxWorld Magazine</a>
  <a href="http://www.linuxworld.com/story/39231.htm">2003 Readers' Choice Awards</a>,
  often referred to as the "Oscars of the Software Industry," recognizes excellence in the solutions
  provided by the top Linux vendors in the market. We are therefore very proud that KDE 3.1
  has won first price in the "Best Linux Desktop Manager" category.
  </p>

  <h3>2003 LinuxQuestions.org Members' Choice Awards</h3>
  <img src="images/LQ-2003MCA-Desktop.png" alt="LinuxQuestions.org 2003 Members' Choice Award - Winnder - Desktop Environment"   class="sideimage" />
  <p>The members of <a href="http://www.linuxquestions.org/">LinuxQuestions.org</a>
  chose KDE as the best Desktop Environment and Quanta as the Web Development Editor of the Year 2003.
  </p>

  <h3>Best Integrated Development Environment for KDevelop</h3>
  <img src="images/LnmAwardLogo.png" alt="KDevelop best IDE in Linux New Media's 2003 Editors' Choice Award" class="sideimage" />
  <p><a href="http://www.linuxnewmedia.de/en">Linux New Media</a>'s <a href="http://www.linux-magazin.de/Artikel/ausgabe/2003/12/award/award.html">Editors' Choice Award</a>
  for 2003 was granted to KDevelop in the category "IDE Development System".
  </p>

  <h3>Favorite Desktop Environment for KDE</h3>
  <img src="images/rc2003.png" alt="KDE best desktop environment in Linux Journal's 2003 Readers' Choice Award" class="sideimage" />
  <p><a href="http://www.linuxjournal.com/">Linux Journal</a>'s <a href="http://pr.linuxjournal.com/article.php?sid=785">Readers' Choice Award</a>
  for 2003 was granted to KDE in the category "Favorite Desktop Environment".
  </p>

  <h3>Best Desktop Environment for KDE 3.1,<br/>Best Web Browser for Konqueror 3.1</h3>
  <img src="images/openchoice2k2.png" alt="KDE and Konqueror best in their categories in Open Choice Awards 2003" class="sideimage" />
  <p><a href="http://www.ofb.biz">Open for Business</a>' <a href="http://www.ofb.biz/modules.php?name=News&amp;file=article&amp;sid=265">Open Choice Awards 2003</a>
  chose KDE to be awarded as "Best Desktop Environment" and Konqueror as "Best Web Browser".
  </p>

<h2><a name="2002"></a>Awards in 2002</h2>

  <h3>Best Desktop Environment for KDE and <br /> Best Email Client for KMail</h3>
  <img src="images/rc02_small.png" alt="KDE and KMail best in their categories in Linux Journal's 2002 Readers' Choice Award"  class="sideimage" />
  <p><a href="http://www.linuxjournal.com/">Linux Journal</a>'s <a href="http://www.linuxjournal.com/article.php?sid=6380">Readers' Choice Award</a>
  for 2002 was granted to KDE in the category "Desktop Environment" and to KMail in the category "EMail Client".
  </p>

  <h3>Best Consumer Software for KDE 3.0</h3>
  <p><a href="http://www.linuxjournal.com/">Linux Journal</a>'s <a href="http://www.linuxjournal.com/article.php?sid=6181">Editors' Choice Award</a>
  for 2002 was granted to KDE as "Best Consumer Software". Honorable mentions were Konqueror in category "Web Client" and KDevelop in category "Development Tool".
  </p>

  <h3>Best Desktop Environment for KDE 3.0, <br />
    Best Email Client for KMail and <br/>
    Best Development Tool for KDevelop</h3>
  <img src="images/openchoice2k2.png" alt="KDE, KMail and KDevelop best in their categories in Open Choice Awards 2002" class="sideimage"/>
  <p><a href="http://www.ofb.biz">Open for Business</a>' <a href="http://www.ofb.biz/modules.php?name=News&amp;file=article&amp;sid=146">Open Choice Awards 2002</a>
  chose KDE to be awarded as "Best Desktop Environment", KMail as "Best Email Client" and KDevelop as "Best Development Tool". Additionally Kopete was
  mentionend as runner up for "Best Communication Software".
  </p>

<h2><a name="2001"></a>Awards in 2001</h2>

  <h3>Best Client-Side Software for Konqueror and <br />
    Best Programming Tool for KDevelop</h3>
  <p>These awards, consisting of 3000 DM for each award, were awarded by Linux New Media at Systeme fair in M&uuml;nchen (Munich). Read the <a href="http://dot.kde.org/1003720345/">dot story</a>.
  </p>

  <h3>Linux Journal - 2001 Readers' Choice Awards</h3>
  <img src="images/rc01_small.png" alt="KDE: Favorite Desktop Environment in Linux Journal's 2001 Readers' Choice Award" class="sideimage"/>
  <p><a href="http://www.linuxjournal.com/">Linux Journal</a>'s <a href="http://www2.linuxjournal.com/lj-issues/issue91/5441.html">Readers' Choice Award</a> for 2001 was granted to KDE in the category "Desktop Environments". Parts of the KDE project were also nominated in other categories:</p>
<ul>
<li>KWord - third position in "Favorite Word Processor" category</li>
<li>KOffice - second position in "Favorite Office Suite" category</li>
<li>KDevelop - third position in "Favorite Development Tool" category</li>
<li>Konqueror - third position in "Favorite Web Browser" category</li>
<li>KMail - second position in "Favorite Mail Client" category</li>
<li>Tea - third position in "Favorite Programming Beverage" category :-)</li>
</ul>
  <p>You can also <a href="http://dot.kde.org/1002945009/">read the news spot</a> over at <a href="http://dot.kde.org/">the Dot</a>.
  </p>

  <h3>Best Web Browser in Linux Magazine's 2001 Editors' Choice Awards (the Tuxies)</h3>
  <img src="images/Tuxie_2001.png" alt="Konqueror: Best Web Browser in Linux Magazine's 2001 Editors' Choice Award" class="sideimage"/>
  <p><a href="http://www.linux-mag.com/">Linux Magazine</a>'s Editors chose Konqueror to be awarded with the Tuxie for the Best Browser of 2001. This year's Linux Magazine Editors' Choice Awards were published in the September issue.</p>

  <h3>LinuxWorld Expo Best Open Source Project</h3>
  <img src="images/LWE-award_08_2001.jpg" alt="LinuxWorld Expo Award" class="sideimage"/>
  <p>Continuing our streak at LinuxWorld Expo!  Read the <a href="http://dot.kde.org/999120705/">news bit</a></p>

<h2><a name="2000"></a>Awards in 2000</h2>

  <h3>Linux Journal Editors' Choice</h3>
  <img src="images/ljedchoice-2000.png"  alt="Linux Journal Editors' Choice Award 2000" class="sideimage"/>
  <p>The readers' selected us earlier this year and now it's the editors'
  turn!  Read the <a href="http://dot.kde.org/974348698/">news bit</a></p>

  <h3>Linux Journal Readers' Choice</h3>
  <img src="images/rc00_small.png" alt="Linux Journal Readers' Choice Award 2000" class="sideimage"/>
  <p>Second year in a row!  Read the <a href="http://dot.kde.org/971786749/">news bit</a></p>

  <h3>Linux Community Award</h3>
  <a href="images/community.jpg"><img src="images/community-mini.jpg" alt="Linux Community Award" class="sideimage"/></a>
  <p>Read the <a href="http://dot.kde.org/970766758/">news bit</a></p>

  <h3>LinuxWorld Expo Show Favorite</h3>
  <img src="images/LWE-award_08_2000-big.jpg" alt="LinuxWorld Expo Award" class="sideimage"/>
  <p>Read the <a href="/announcements/lwe_08_2000.BW.press-release.php">press release</a></p>

<h2><a name="1999"></a>Awards in 1999</h2>

  <h3>Linux Journal Readers' Choice</h3>
  <img src="images/rc99_small.png" alt="Linux Journal Readers' Choice Award" class="sideimage"/>

  <h3>CeBIT "Software Innovation of the Year"</h3>
  <img src="images/innov-award.png"  alt="CeBIT Innovation of the Year Award" class="sideimage"/>
  <p>Read the <a href="/announcements/ZD-Innov-CeBIT99.php">press-release</a><br />
  <a href="images/innov-award-1.jpg">[ Pic 1]</a> |
  <a href="images/innov-award-2.jpg">[ Pic 2]</a> |
  <a href="images/innov-award-3.jpg">[ Pic 3]</a> |
  <a href="images/innov-award-4.jpg">[ Pic 4]</a>
  </p>

  <h3>LinuxWorld Editor Choice Award</h3>
  <img src="images/lwedchoice-200.png" alt="LinuxWorld Editor Choice Award" class="sideimage"/>
  <p>Read the <a href="http://www.linuxworld.com/linuxworld/lw-1999-08/lw-08-penguin_1.html">LinuxWorld article</a>.
  </p>

</main>
<?php
  require('../../aether/footer.php');
