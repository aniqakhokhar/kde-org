<?php
    require('../../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "About KDE"
    ]);

    require('../../aether/header.php');
    $site_root = "../../";
?>

<main class="container">

<h1>About KDE</h1>

<p>KDE is an international team co-operating on development
and distribution of Free, Open Source Software for desktop
and portable computing. Our community has developed a
wide variety of applications for communication, work,
education and entertainment. We have a strong focus on
finding innovative solutions to old and new problems,
creating a vibrant, open atmosphere for experimentation.
</p>

<h3>What makes KDE so exciting?</h3>

<p>
The best thing about KDE is our amazing community! We
are open to new members, offering help and allowing them
to experiment, learn and grow. Our products are used by
millions of home and office workers, and are being
deployed in schools around the world - Brazil alone has
over 50 million school children using KDE-based software
to browse, learn and communicate!
</p>

<h3>Is it difficult to get involved?</h3>

<p>
Not at all! Every day more and more people join our ever-growing
 family of contributors. KDE has a strong
infrastructure of web resources, forums, mailing-lists, IRC
and many other communication services. We can provide
feedback on your code and other contributions with the goal
of helping you learn. Of course, a pro-active attitude and a
friendly personality helps.  
</p>

<h3>What does KDE produce?</h3>

<p>
For users on Linux and Unix, KDE offers a full suite of
user workspace applications which allow interaction with
these operating systems in a modern, graphical user
interface. This includes Plasma Desktop, KDE's innovative desktop
interface. Other workspace applications are included to aid with system
configuration, running programs, or interacting with
hardware devices. While the fully integrated KDE
Workspaces are only available on Linux and Unix, some of
these features are available on other platforms.
</p>
<p>
In addition to the workspace, KDE produces a number of
key applications such as the Konqueror web browser,
Dolphin file manager and Kontact, the comprehensive
personal information management suite. However, our list of applications includes
many others, including those for education, multimedia, office productivity, 
networking, games and much more. Most
applications are available on all platforms supported by the KDE Development.
</p>
<p>
KDE also brings to the forefront many innovations for application
developers. An entire infrastructure has been designed and implemented
 to help programmers create robust and comprehensive applications 
in the most efficient manner, eliminating the complexity and tediousness of
creating highly functional applications.
</p>
<p>
It is our hope and continued ambition that the KDE team will bring open, reliable,
stable and monopoly-free computing to the everyday user.
</p>

<h3>KDE is about freedom</h3>

<p>All of the software that KDE produces is a Free and Open Source Software.
KDE brings together hundreds of
artists, designers, programmers, translators, users, writers and other contributors
from across the globe. The team is committed to the development and spirit of free software.
Every single line of KDE source code is made
available under an open source license. The current
<a href="http://techbase.kde.org/Policies/Licensing_Policy">licensing policy</a>
is also available online.
This effectively means that everyone is free to
modify and distribute source code written by KDE. Implications of this
are several, in particular KDE software is available free
of charge to anyone, and will always be so.</p>

<h3>Legal Bodies</h3>
<ul>
<li><a href="http://ev.kde.org">KDE e.V.</a> is the foundation which oversees the community for financial and legal representation.</li>
<li><a href="kdefreeqtfoundation.php">KDE Free Qt Foundation</a> governs our agreement with Qt Company.</li>
</ul>

</main>
<?php require('../../aether/footer.php'); ?>
