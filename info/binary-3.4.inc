<ul>

<!-- CONECTIVA LINUX -->
<li><a href="http://www.conectiva.com/">Conectiva Linux</a>:
  <ul type="disc">
    <li>
      conectiva:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4/Conectiva/conectiva/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>


<!--   REDHAT LINUX -->
<li>
  <a href="http://www.redhat.com/">Red Hat</a>
  :
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4/RedHat/Fedora3/noarch/">Language
        packages</a> (all versions and architectures)
    </li>
    <li>
      Fedora 3: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4/RedHat/Fedora3/i386/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>

<!-- KDE-RedHat -->
<li>
 <a href="http://kde-redhat.sourceforge.net/">KDE-RedHat (unofficial) Packages</a>:
 <ul type="disc">
 <li>
 <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4/contrib/kde-redhat/all/">all platforms</a>
 </li>
 <li>
 <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4/contrib/kde-redhat/redhat/9/">Red Hat 9</a>
 </li>
 <li>
 <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4/contrib/kde-redhat/fedora/2/">Fedora Core 2</a>
 </li>
 <li>
 <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4/contrib/kde-redhat/fedora/3/">Fedora Core 3</a>
 </li>
 <li>
 <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4/contrib/kde-redhat/redhat/el3">Red Hat Enterprise 3</a>
 </li>
 <li>
 <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4/contrib/kde-redhat/redhat/el4/">Red Hat Enterprise 4</a>
 </li>
 </ul>
 <p />
</li>

<!-- SLACKWARE LINUX -->
<li>
  <a href="http://www.slackware.org/">Slackware</a> (Unofficial contribution)
 (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.4/contrib/Slackware/10.1/README">README</a>)
   :
   <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4/contrib/Slackware/noarch/">Language packages</a>
    </li>
     <li>
       10.1: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4/contrib/Slackware/10.1/">Intel i486</a>
     </li>
   </ul>
  <p />
</li>

<!-- SOLARIS -->
<li>
 <a href="http://solaris.kde.org">KDE Solaris</a> (Unofficial contribution):
 <ul type="disc">
 <li>
 9: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4/contrib/Solaris/GCC/9/">Sparc (GCC)</a>
 </li>
 </ul>
 <p />
</li>

<!--   SUSE LINUX -->
<li>
  <a href="http://www.novell.com/linux/suse/">SuSE Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.4/SuSE/README">README</a>)
      :
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4/SuSE/noarch/">Language
        packages</a> (all versions and architectures)
    </li>
    <li>
      9.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4/SuSE/ix86/9.2/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4/SuSE/x86_64/9.2/">AMD x86-64</a>
    </li>
    <li>
      9.1:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4/SuSE/ix86/9.1/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4/SuSE/x86_64/9.1/">AMD x86-64</a>
    </li>
    <li>
      9.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4/SuSE/ix86/9.0/">Intel i586</a>
    </li>
    <li>
      8.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.4/SuSE/ix86/8.2/">Intel i586</a>
    </li>
  </ul>
  <p />
</li>

</ul>
