
<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">Sha1 Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/attica-5.62.0.tar.xz">attica-5.62.0</a></td>
   <td align="right">61kB</td>
   <td><tt>f312a826b7157df21ccdb2bd33c7a8130e474cf3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/baloo-5.62.0.tar.xz">baloo-5.62.0</a></td>
   <td align="right">267kB</td>
   <td><tt>21b4d08104420dbbbd22fde1b3f1580be82b060c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/bluez-qt-5.62.0.tar.xz">bluez-qt-5.62.0</a></td>
   <td align="right">96kB</td>
   <td><tt>b64285508b2bf4f0af55abcc1a12d74bfa5f9a24</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/breeze-icons-5.62.0.tar.xz">breeze-icons-5.62.0</a></td>
   <td align="right">2.5MB</td>
   <td><tt>331b75560f52e6d7cc4edfbd8894df8246bfb1a8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/extra-cmake-modules-5.62.0.tar.xz">extra-cmake-modules-5.62.0</a></td>
   <td align="right">322kB</td>
   <td><tt>4a27105840a9422e3525ff0dc9cb4facb81d4483</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/frameworkintegration-5.62.0.tar.xz">frameworkintegration-5.62.0</a></td>
   <td align="right">1.6MB</td>
   <td><tt>f25be1a3b479e131bc99183182d6e2d48f412235</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kactivities-5.62.0.tar.xz">kactivities-5.62.0</a></td>
   <td align="right">59kB</td>
   <td><tt>597d7285cb122af2c9d643e9e0e7593819be65b8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kactivities-stats-5.62.0.tar.xz">kactivities-stats-5.62.0</a></td>
   <td align="right">61kB</td>
   <td><tt>bc75624b927d5aac4d09143c939d39cc20e1ba91</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kapidox-5.62.0.tar.xz">kapidox-5.62.0</a></td>
   <td align="right">387kB</td>
   <td><tt>bdc87484d2116956db887f72030c2fdc082acb99</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/karchive-5.62.0.tar.xz">karchive-5.62.0</a></td>
   <td align="right">448kB</td>
   <td><tt>96938d0124700005fc1b5224d94349f4e49ea413</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kauth-5.62.0.tar.xz">kauth-5.62.0</a></td>
   <td align="right">84kB</td>
   <td><tt>097838256f872f970c32f80733476bc77e70b15f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kbookmarks-5.62.0.tar.xz">kbookmarks-5.62.0</a></td>
   <td align="right">117kB</td>
   <td><tt>fbdc1397f55657777bde6c49abe2385f0b8a03f4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kcmutils-5.62.0.tar.xz">kcmutils-5.62.0</a></td>
   <td align="right">232kB</td>
   <td><tt>fa4144f384a0b0a9c0e39a6824b041a4adfdda10</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kcodecs-5.62.0.tar.xz">kcodecs-5.62.0</a></td>
   <td align="right">215kB</td>
   <td><tt>c59d785567eaa012b7bb97e4d399642ca2be6df9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kcompletion-5.62.0.tar.xz">kcompletion-5.62.0</a></td>
   <td align="right">116kB</td>
   <td><tt>ee8f6c1d6f2a5d1066d689eda1f892aa7557936a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kconfig-5.62.0.tar.xz">kconfig-5.62.0</a></td>
   <td align="right">236kB</td>
   <td><tt>02bfb8d2a6c268da032d3ed6784659fb1f886aca</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kconfigwidgets-5.62.0.tar.xz">kconfigwidgets-5.62.0</a></td>
   <td align="right">368kB</td>
   <td><tt>2bc0b7c6fdd3f1dbafe0e2a12af2b83300ec0b8e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kcoreaddons-5.62.0.tar.xz">kcoreaddons-5.62.0</a></td>
   <td align="right">362kB</td>
   <td><tt>cb42d18d0426a943bf1f4d43923411ebe2c87ca5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kcrash-5.62.0.tar.xz">kcrash-5.62.0</a></td>
   <td align="right">22kB</td>
   <td><tt>647a36707d3c53a68a882328512d938adce5c419</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kdbusaddons-5.62.0.tar.xz">kdbusaddons-5.62.0</a></td>
   <td align="right">38kB</td>
   <td><tt>56e8416b9bd84866ced2adb5e03383a56bfc70e1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kdeclarative-5.62.0.tar.xz">kdeclarative-5.62.0</a></td>
   <td align="right">170kB</td>
   <td><tt>55d277ed3602c27265f4a74a2dba3aac65042c42</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kded-5.62.0.tar.xz">kded-5.62.0</a></td>
   <td align="right">37kB</td>
   <td><tt>c9ae3feee13973d8b93466bdf0cbf24a77f4024f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kdesu-5.62.0.tar.xz">kdesu-5.62.0</a></td>
   <td align="right">46kB</td>
   <td><tt>6cba47245062a2748617f4d7a1c9ce387341248b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kdnssd-5.62.0.tar.xz">kdnssd-5.62.0</a></td>
   <td align="right">56kB</td>
   <td><tt>701e117f1f0de231fb197c7763d870bb33c7a2df</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kdoctools-5.62.0.tar.xz">kdoctools-5.62.0</a></td>
   <td align="right">417kB</td>
   <td><tt>911c7fd904c52c2d76b8e8a06d3d28500c3d1ac8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kemoticons-5.62.0.tar.xz">kemoticons-5.62.0</a></td>
   <td align="right">1.6MB</td>
   <td><tt>2313d89678d0724298590950a5231ebe5d1c3937</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kfilemetadata-5.62.0.tar.xz">kfilemetadata-5.62.0</a></td>
   <td align="right">410kB</td>
   <td><tt>df4a45fed589a3b4c9fbe896293cd6cb349be090</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kglobalaccel-5.62.0.tar.xz">kglobalaccel-5.62.0</a></td>
   <td align="right">82kB</td>
   <td><tt>d0306526ebbc7c38b80339a52fa87056c4528cb5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kguiaddons-5.62.0.tar.xz">kguiaddons-5.62.0</a></td>
   <td align="right">39kB</td>
   <td><tt>5599f256f6ee74238030e464c8ce411208ec604a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kholidays-5.62.0.tar.xz">kholidays-5.62.0</a></td>
   <td align="right">201kB</td>
   <td><tt>e6a93f269e4384e77b1e6ad2e44a8e7679d41649</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/ki18n-5.62.0.tar.xz">ki18n-5.62.0</a></td>
   <td align="right">571kB</td>
   <td><tt>2c0b1bfc831e32e13b4791587d5f2e7769b05b0d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kiconthemes-5.62.0.tar.xz">kiconthemes-5.62.0</a></td>
   <td align="right">204kB</td>
   <td><tt>3462fcf3fe7f5fe1add7cc4881b991726b16931f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kidletime-5.62.0.tar.xz">kidletime-5.62.0</a></td>
   <td align="right">26kB</td>
   <td><tt>32329b38b99ef43df4075846169c97ddc159ebf6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kimageformats-5.62.0.tar.xz">kimageformats-5.62.0</a></td>
   <td align="right">200kB</td>
   <td><tt>633640e9c9195d828e771210cb4ab2d53a4165dd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kinit-5.62.0.tar.xz">kinit-5.62.0</a></td>
   <td align="right">117kB</td>
   <td><tt>ef976367e35b40336863653e64db7b57bd2a79b2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kio-5.62.0.tar.xz">kio-5.62.0</a></td>
   <td align="right">3.0MB</td>
   <td><tt>4e22efc08e85a77c5e02d2f13657fb5952f3a42a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kirigami2-5.62.0.tar.xz">kirigami2-5.62.0</a></td>
   <td align="right">139kB</td>
   <td><tt>406978e4080949a7ca6a4c375b566a3e64b627e3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kitemmodels-5.62.0.tar.xz">kitemmodels-5.62.0</a></td>
   <td align="right">380kB</td>
   <td><tt>e75b0d14ca86bd25a8dab07a93ea5343952a9126</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kitemviews-5.62.0.tar.xz">kitemviews-5.62.0</a></td>
   <td align="right">73kB</td>
   <td><tt>4eaea613ef8e837d7c342e73b953dc2a9523eab3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kjobwidgets-5.62.0.tar.xz">kjobwidgets-5.62.0</a></td>
   <td align="right">86kB</td>
   <td><tt>7d7419c605d78de7abd7a4fcc4671a4beb067a89</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/knewstuff-5.62.0.tar.xz">knewstuff-5.62.0</a></td>
   <td align="right">991kB</td>
   <td><tt>e9c4306b09343c72d659731a28e112bbcbe4451e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/knotifications-5.62.0.tar.xz">knotifications-5.62.0</a></td>
   <td align="right">113kB</td>
   <td><tt>a570b8b3f1a7fc89a54129de847f13ab0dc47db7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/knotifyconfig-5.62.0.tar.xz">knotifyconfig-5.62.0</a></td>
   <td align="right">82kB</td>
   <td><tt>c884ec974ded9aba8db3a7e7362efbcc3b39a23d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kpackage-5.62.0.tar.xz">kpackage-5.62.0</a></td>
   <td align="right">134kB</td>
   <td><tt>056dbec78d7e16561ece40ccd58a1d64e24bcebc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kparts-5.62.0.tar.xz">kparts-5.62.0</a></td>
   <td align="right">171kB</td>
   <td><tt>657b6d3acb97d7c6ec08e1b72a43ff4003a7c30e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kpeople-5.62.0.tar.xz">kpeople-5.62.0</a></td>
   <td align="right">60kB</td>
   <td><tt>6597b97a1b97f1587de319096637449c820a92e2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kplotting-5.62.0.tar.xz">kplotting-5.62.0</a></td>
   <td align="right">29kB</td>
   <td><tt>bcfacb661e6621acefc7f6246d7f3ba2b992bcb6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kpty-5.62.0.tar.xz">kpty-5.62.0</a></td>
   <td align="right">56kB</td>
   <td><tt>264490679d64d2a5b764ce5acb99bcf08470b4f7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/krunner-5.62.0.tar.xz">krunner-5.62.0</a></td>
   <td align="right">60kB</td>
   <td><tt>d9eeecef89aec7523e0fc4fee7c182de69ec82e8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kservice-5.62.0.tar.xz">kservice-5.62.0</a></td>
   <td align="right">244kB</td>
   <td><tt>3cfd3f33f9fc075c4f343d120723de7fd3b227a8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/ktexteditor-5.62.0.tar.xz">ktexteditor-5.62.0</a></td>
   <td align="right">2.2MB</td>
   <td><tt>176823adad4482af70362438af4822a837302f93</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/ktextwidgets-5.62.0.tar.xz">ktextwidgets-5.62.0</a></td>
   <td align="right">303kB</td>
   <td><tt>283fad69d8f01c019bf7d7d35eac14630dc7f86e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kunitconversion-5.62.0.tar.xz">kunitconversion-5.62.0</a></td>
   <td align="right">806kB</td>
   <td><tt>8acd7323e64bde5ecc4983af68e3c43bc21b37f1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kwallet-5.62.0.tar.xz">kwallet-5.62.0</a></td>
   <td align="right">286kB</td>
   <td><tt>0cdd3ee26b6527b6b80b46e6fb83f9c400250b91</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kwayland-5.62.0.tar.xz">kwayland-5.62.0</a></td>
   <td align="right">329kB</td>
   <td><tt>de4ea60a2b6c018dc88eb0207d29acaa091f8b7e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kwidgetsaddons-5.62.0.tar.xz">kwidgetsaddons-5.62.0</a></td>
   <td align="right">2.0MB</td>
   <td><tt>ac0e541f6394f5cdda5cfeb358ad7f80ec63d22b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kwindowsystem-5.62.0.tar.xz">kwindowsystem-5.62.0</a></td>
   <td align="right">167kB</td>
   <td><tt>db6c3107e2a2b00240ac31b6336c77528a1c70a0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kxmlgui-5.62.0.tar.xz">kxmlgui-5.62.0</a></td>
   <td align="right">854kB</td>
   <td><tt>df0a6f7d72ad86f9278d7844cbd072baa3417e4d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/kxmlrpcclient-5.62.0.tar.xz">kxmlrpcclient-5.62.0</a></td>
   <td align="right">29kB</td>
   <td><tt>92316817b8489ced61fa69e53ae40c23798309a4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/modemmanager-qt-5.62.0.tar.xz">modemmanager-qt-5.62.0</a></td>
   <td align="right">99kB</td>
   <td><tt>283884c86eae221af336c8957b60d839fb77159a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/networkmanager-qt-5.62.0.tar.xz">networkmanager-qt-5.62.0</a></td>
   <td align="right">181kB</td>
   <td><tt>2233329e979bc8fc80ce1b716a9e1cba12b162a3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/oxygen-icons5-5.62.0.tar.xz">oxygen-icons5-5.62.0</a></td>
   <td align="right">225MB</td>
   <td><tt>dcb3c3964dd9b2546dfa4409b3b8569b52e575ac</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/plasma-framework-5.62.0.tar.xz">plasma-framework-5.62.0</a></td>
   <td align="right">2.9MB</td>
   <td><tt>236ae1b1566cb05c4a779d08c63e24f7900eabe6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/prison-5.62.0.tar.xz">prison-5.62.0</a></td>
   <td align="right">41kB</td>
   <td><tt>24e8ac10baf9b6945af9b87aae825401e2a251a1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/purpose-5.62.0.tar.xz">purpose-5.62.0</a></td>
   <td align="right">149kB</td>
   <td><tt>03d4ace146985c14dc98cda2643988aff7673af2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/qqc2-desktop-style-5.62.0.tar.xz">qqc2-desktop-style-5.62.0</a></td>
   <td align="right">43kB</td>
   <td><tt>e8a8c2470f8e2901b1f717416801c3c816e5318a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/solid-5.62.0.tar.xz">solid-5.62.0</a></td>
   <td align="right">258kB</td>
   <td><tt>5e801f88ce00c23d33e834525a4c2057d0d58921</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/sonnet-5.62.0.tar.xz">sonnet-5.62.0</a></td>
   <td align="right">280kB</td>
   <td><tt>1a202c258c193a80749cb2310f2e3c1c8a10f850</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/syndication-5.62.0.tar.xz">syndication-5.62.0</a></td>
   <td align="right">155kB</td>
   <td><tt>8a8832c8732e6ef6760f231d6e2b4fe5b28242ec</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/syntax-highlighting-5.62.0.tar.xz">syntax-highlighting-5.62.0</a></td>
   <td align="right">1.4MB</td>
   <td><tt>da33b8c4c40679fb3d305cf852be4addbfd51b03</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.62/threadweaver-5.62.0.tar.xz">threadweaver-5.62.0</a></td>
   <td align="right">1.3MB</td>
   <td><tt>aab624fcdb05805ebe62d2fd6c7a248e38631fce</tt></td>
</tr>

</table>
