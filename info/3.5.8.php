<?php
  $page_title = "KDE 3.5.8 Info Page";
  $site_root = "../";
  include "header.inc";
?>

<p>
KDE 3.5.8 was released on October, 16th, 2007. Read the <a
href="/announcements/announce-3.5.8.php">official announcement</a>.</p>

<?php include "unmaintained.inc" ?>

<h2>Security Issues</h2>

<p>Please report possible problems to <a href="m&#x61;i&#00108;&#x74;o:&#115;ec&#117;&#x72;&#00105;&#x74;&#121;&#x40;kde.&#00111;&#x72;g">&#x73;&#101;&#x63;u&#114;i&#x74;y&#x40;kd&#101;.&#x6f;&#00114;&#103;</a>.</p>

<p>Patches for the issues mentioned below are available from
<a href="ftp://ftp.kde.org/pub/kde/security_patches">ftp://ftp.kde.org/pub/kde/security_patches</a>
unless stated otherwise.</p>

<ul>
<li>None known yet</li>
</ul>

<h2><a name="bugs">Bugs</a></h2>

<p>This is a list of grave bugs and common pitfalls
surfacing after the release was packaged:</p>

<ul>
<li><a href="http://websvn.kde.org/?view=rev&amp;revision=727528">KSelectAction can not be edited</a></li>
<li><a href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kioslave/http/http.cc?r1=717341&amp;r2=726097">HTTP SSL slave regression</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=150809">Bug 150809</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=150904">Bug 150904</a></li>
<li><a href="http://websvn.kde.org/?view=rev&amp;revision=728690">KDevelop user interface lag</a></li>
<li>kde-i18n-fa does not build, <a href="http://cvs.pld-linux.org/cgi-bin/cvsweb/SOURCES/kde-i18n-fa.patch?rev=1.2">patch</a></li>
</ul>

<p>Please check the <a href="http://bugs.kde.org/">bug database</a>
before filing any bug reports. Also check for possible updates on this page
that might describe or fix your problem.</p>

<h2>FAQ</h2>

See the <a href="faq.php">KDE FAQ</a> for any specific
questions you may have.  Questions about Konqueror should be directed
<a href="http://konqueror.kde.org/faq/">to the Konqueror FAQ</a>.

<h2>Download and Installation</h2>

<p>
<u>Library Requirements</u>.
 <a href="/info/requirements/3.5.php">KDE 3.5 requires or benefits</a>
 from the given list of libraries, most of which should be already installed
 on your system or available from your OS CD or your vendor's website.
</p>
<p>
  The complete source code for KDE 3.5.8 is available for download:
</p>

<?php
include "source-3.5.8.inc"
?>

<!-- Comment the following if Konstruct is not up-to-date
<p>The <a href="http://developer.kde.org/build/konstruct/">Konstruct</a> build toolset can help you
downloading and installing these tarballs.</p> -->

<u><a name="binary">Binary packages</a></u>

<p>
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE 3.5.8 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.8/">http</a> or
  <a href="/mirrors/ftp.php">FTP mirrors</a>.
</p>

<p>
  Currently pre-compiled packages are available for:
</p>

<?php
include "binary-3.5.8.inc"
?>

<p>
Additional binary packages might become available in the coming weeks,
as well as updates to the current packages.</p>

<p>Check
<a href="http://www.kde.org/download/distributions.php">this list</a> 
to see which distributions are shipping KDE.
</p>

<h2>Developer Info</h2>

If you need help porting your application to KDE 3.x see the <a
href="http://websvn.kde.org/*checkout*/branches/KDE/3.5/kdelibs/KDE3PORTING.html">
porting guide</a> or subscribe to the
<a href="http://mail.kde.org/mailman/listinfo/kde-devel">KDE Devel Mailinglist</a>
to ask specific questions about porting your applications.

<p>There is also info on the <a
href="http://developer.kde.org/documentation/library/kdeqt/kde3arch/index.html">architecture</a>
and the <a href="http://developer.kde.org/documentation/library/3.5-api.php/">
programming interface of KDE 3.5</a>.
</p>

<!-- END CONTENT -->
<?php
  include "footer.inc";
?>
