KDE Project Security Advisory
=============================

Title:          kconfig: malicious .desktop files (and others) would execute code
Risk Rating:    High
CVE:            CVE-2019-14744
Versions:       KDE Frameworks < 5.61.0
Date:           7 August 2019


Overview
========
The syntax Key[$e]=$(shell command) in *.desktop files, .directory files, and configuration files
(typically found in ~/.config) was an intentional feature of KConfig, to allow flexible configuration.
This could however be abused by malicious people to make the users install such files and get code
executed even without intentional action by the user. A file manager trying to find out the icon for
a file or directory could end up executing code, or any application using KConfig could end up
executing malicious code during its startup phase for instance.

After careful consideration, the entire feature of supporting shell commands in KConfig entries has been removed,
because we couldn't find an actual use case for it. If you do have an existing use for the feature, please
contact us so that we can evaluate whether it would be possible to provide a secure solution.

Note that [$e] remains useful for environment variable expansion.

Solution
========

KDE Frameworks 5 users:
- update to kconfig >= 5.61.0
- or apply the following patch to kconfig:
https://cgit.kde.org/kconfig.git/commit/?id=5d3e71b1d2ecd2cb2f910036e614ffdfc895aa22

kdelibs users: apply the following patch to kdelibs 4.14:
https://cgit.kde.org/kdelibs.git/commit/?id=2c3762feddf7e66cf6b64d9058f625a715694a00

Credits
=======
Thanks to Dominik Penner for finding and documenting this issue (we wish however that he would
have contacted us before making the issue public) and to David Faure for the fix.
