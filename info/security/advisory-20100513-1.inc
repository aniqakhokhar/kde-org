<li>KGet versions up to and including KDE SC 4.4.3 have a directory
traversal flaw that can allow maliciously-crafted metalink files to download
files to unintended directories.
Read the <a href="security/advisory-20100513-1.txt">detailed advisory</a>
for more information.
</li>
