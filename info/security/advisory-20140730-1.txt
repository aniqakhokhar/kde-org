KDE Project Security Advisory
=============================

Title:          KAuth PID Reuse Flaw
Risk Rating:    Low
CVE:            CVE-2014-5033
Platforms:      All
Versions:       kdelibs < 4.14, kauth < 5.1
Author:         Martin Sandsmark <martin.sandsmark@kde.org>
Date:           30 July 2014

Overview
========

The KAuth framework uses polkit-1 API which tries to authenticate using the 
requestors PID. This is prone to PID reuse race conditions.

Impact
======

This potentially allows a malicious application to pose as another for 
authentication purposes when executing privileged actions.

Workaround
==========

Disable polkit-1 integration.

Solution
========

Upgrade to kdelibs 4.14, kauth 5.1 or apply the patches at:
kdelibs: http://quickgit.kde.org/?p=kdelibs.git&a=commit&h=e4e7b53b71e2659adaf52691d4accc3594203b23
kauth: http://quickgit.kde.org/?p=kauth.git&a=commit&h=341b7d84b6d9c03cf56905cb277b47e11c81482a

Credits
=======

Thanks to the SuSE security team and packagers for discovery and notification.
