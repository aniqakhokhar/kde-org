
<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">Sha1 Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/attica-5.29.0.tar.xz">attica-5.29.0</a></td>
   <td align="right">58kB</td>
   <td><tt>c894b693700c8ada5453ff6aa842542de59856e7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/baloo-5.29.0.tar.xz">baloo-5.29.0</a></td>
   <td align="right">196kB</td>
   <td><tt>dcc14c384a18b8a0091a86b639bd4679a48766bd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/bluez-qt-5.29.0.tar.xz">bluez-qt-5.29.0</a></td>
   <td align="right">72kB</td>
   <td><tt>8953655be66aece35f794fabc8eeba74ab1b1625</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/breeze-icons-5.29.0.tar.xz">breeze-icons-5.29.0</a></td>
   <td align="right">1.3MB</td>
   <td><tt>baef35271741ac0fa9d7ae7f9200ca59ed1dc438</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/extra-cmake-modules-5.29.0.tar.xz">extra-cmake-modules-5.29.0</a></td>
   <td align="right">295kB</td>
   <td><tt>6827eb313d6c3cdeedcc7e147ba53c1c8b02bf2e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/frameworkintegration-5.29.0.tar.xz">frameworkintegration-5.29.0</a></td>
   <td align="right">1.6MB</td>
   <td><tt>674e64ca4883d41683323a0fc411f088b6d4a8a8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kactivities-5.29.0.tar.xz">kactivities-5.29.0</a></td>
   <td align="right">59kB</td>
   <td><tt>1ed3a984c653634a59b504ef63237bff8c3b36cf</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kactivities-stats-5.29.0.tar.xz">kactivities-stats-5.29.0</a></td>
   <td align="right">57kB</td>
   <td><tt>1368700916deb3d29e9f192b25cabdd000fad5ba</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kapidox-5.29.0.tar.xz">kapidox-5.29.0</a></td>
   <td align="right">385kB</td>
   <td><tt>763ffe4f517d8f010b5235e049685654b59f633c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/karchive-5.29.0.tar.xz">karchive-5.29.0</a></td>
   <td align="right">109kB</td>
   <td><tt>64a07fd4fa8c2153315e85d0333af061c6fb7828</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kauth-5.29.0.tar.xz">kauth-5.29.0</a></td>
   <td align="right">2.4MB</td>
   <td><tt>58636047a952d2c0a2c2f4f9ea2d21da21a4d2f2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kbookmarks-5.29.0.tar.xz">kbookmarks-5.29.0</a></td>
   <td align="right">1.1MB</td>
   <td><tt>883edde2bea526ef1f860f0df696613d76aeaf70</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kcmutils-5.29.0.tar.xz">kcmutils-5.29.0</a></td>
   <td align="right">2.6MB</td>
   <td><tt>cc24ee3b646fd8e9e8bf602f84be698865868ce2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kcodecs-5.29.0.tar.xz">kcodecs-5.29.0</a></td>
   <td align="right">2.6MB</td>
   <td><tt>92605c0fb0a8697546c3b6774e4e19bd52eb06a0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kcompletion-5.29.0.tar.xz">kcompletion-5.29.0</a></td>
   <td align="right">2.4MB</td>
   <td><tt>b85d92ba1a895109772c5eaec055ef75006ba9c5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kconfig-5.29.0.tar.xz">kconfig-5.29.0</a></td>
   <td align="right">227kB</td>
   <td><tt>208da5a4433360955c7eb113e380f6881b027376</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kconfigwidgets-5.29.0.tar.xz">kconfigwidgets-5.29.0</a></td>
   <td align="right">2.7MB</td>
   <td><tt>195de3d0e2c22fc95fb3be5b640f86303446374e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kcoreaddons-5.29.0.tar.xz">kcoreaddons-5.29.0</a></td>
   <td align="right">2.6MB</td>
   <td><tt>cbbe3edd720dcebcb81dd77e98caa4460002a0c5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kcrash-5.29.0.tar.xz">kcrash-5.29.0</a></td>
   <td align="right">20kB</td>
   <td><tt>056a6e90286ad74c2d16acc580d02df034e4452c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kdbusaddons-5.29.0.tar.xz">kdbusaddons-5.29.0</a></td>
   <td align="right">33kB</td>
   <td><tt>e6995a1626d8415fe353671029adc27a5ad3b7b6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kdeclarative-5.29.0.tar.xz">kdeclarative-5.29.0</a></td>
   <td align="right">2.6MB</td>
   <td><tt>099022ad17c0e869b5d63dcc4adc0072ecf0b583</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kded-5.29.0.tar.xz">kded-5.29.0</a></td>
   <td align="right">35kB</td>
   <td><tt>b538f252a5a939c4690c9f6e3d51d09415877ba5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kdesignerplugin-5.29.0.tar.xz">kdesignerplugin-5.29.0</a></td>
   <td align="right">2.4MB</td>
   <td><tt>6083e347b97fbbb2ddb1a93984e8d855696e0361</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kdesu-5.29.0.tar.xz">kdesu-5.29.0</a></td>
   <td align="right">43kB</td>
   <td><tt>8246c6cb5169c3e3774631c822652cb93863e8c9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kdewebkit-5.29.0.tar.xz">kdewebkit-5.29.0</a></td>
   <td align="right">28kB</td>
   <td><tt>8f26a36587543af27ca7d6b431b1853a79b48174</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kdnssd-5.29.0.tar.xz">kdnssd-5.29.0</a></td>
   <td align="right">2.4MB</td>
   <td><tt>6edafe56c472e85e804aac81cd70dfd9f20d1cb0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kdoctools-5.29.0.tar.xz">kdoctools-5.29.0</a></td>
   <td align="right">427kB</td>
   <td><tt>1d5e8fca51bc7d2f17b94ce13d5008e1ea9bf589</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kemoticons-5.29.0.tar.xz">kemoticons-5.29.0</a></td>
   <td align="right">1.6MB</td>
   <td><tt>a82f9f94c74137183bbb890e6cd3b20464248b56</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kfilemetadata-5.29.0.tar.xz">kfilemetadata-5.29.0</a></td>
   <td align="right">132kB</td>
   <td><tt>4c8dce75576f0310120bec7a404066837c944603</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kglobalaccel-5.29.0.tar.xz">kglobalaccel-5.29.0</a></td>
   <td align="right">2.4MB</td>
   <td><tt>93c545cb1d251940aa1616bfacacce8fa3b47cea</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kguiaddons-5.29.0.tar.xz">kguiaddons-5.29.0</a></td>
   <td align="right">38kB</td>
   <td><tt>f04ed44ca5be0d87c9dcb173ffab2a7ca0cb6ca1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/ki18n-5.29.0.tar.xz">ki18n-5.29.0</a></td>
   <td align="right">2.8MB</td>
   <td><tt>cd76943c8d3f6a2e012b0ea80676a0082f71312c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kiconthemes-5.29.0.tar.xz">kiconthemes-5.29.0</a></td>
   <td align="right">1.3MB</td>
   <td><tt>92e558187127ffad54d6947b401fa0b45075dccc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kidletime-5.29.0.tar.xz">kidletime-5.29.0</a></td>
   <td align="right">25kB</td>
   <td><tt>00bd8af6f5ec6d1a864d806f17abbda66c9f3523</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kimageformats-5.29.0.tar.xz">kimageformats-5.29.0</a></td>
   <td align="right">199kB</td>
   <td><tt>8a3f80efc8c43e45bbcf2fc557163fff683b5274</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kinit-5.29.0.tar.xz">kinit-5.29.0</a></td>
   <td align="right">2.5MB</td>
   <td><tt>e7c2c4070da0e997226a514107256002bbb4c5bd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kio-5.29.0.tar.xz">kio-5.29.0</a></td>
   <td align="right">2.9MB</td>
   <td><tt>f7a0669894f16deffd95fd279acab3d8b180d7f0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kitemmodels-5.29.0.tar.xz">kitemmodels-5.29.0</a></td>
   <td align="right">379kB</td>
   <td><tt>43b674a722213dc20bdf7468591f20f8d20e8d80</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kitemviews-5.29.0.tar.xz">kitemviews-5.29.0</a></td>
   <td align="right">2.3MB</td>
   <td><tt>9dd4e0cc0dd22077e27bd77b7afc124d8593f539</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kjobwidgets-5.29.0.tar.xz">kjobwidgets-5.29.0</a></td>
   <td align="right">2.4MB</td>
   <td><tt>6f7d152857352cdfe231eafbbbf17072de159e7c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/knewstuff-5.29.0.tar.xz">knewstuff-5.29.0</a></td>
   <td align="right">3.1MB</td>
   <td><tt>d0525ff7c129203465c83aca9cbe125ceb9393f2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/knotifications-5.29.0.tar.xz">knotifications-5.29.0</a></td>
   <td align="right">2.4MB</td>
   <td><tt>2635d22d43d9a9f159e5c84b540ef173e128bcfd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/knotifyconfig-5.29.0.tar.xz">knotifyconfig-5.29.0</a></td>
   <td align="right">2.5MB</td>
   <td><tt>a36d5520ab81c8c90726f58d41b91860b1989421</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kpackage-5.29.0.tar.xz">kpackage-5.29.0</a></td>
   <td align="right">162kB</td>
   <td><tt>250a5c37e0299a304b9e8823f8351d39df9f6011</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kparts-5.29.0.tar.xz">kparts-5.29.0</a></td>
   <td align="right">2.6MB</td>
   <td><tt>2ee956a3d24db3d93f56d7e6a4fb07480cb728c0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kpeople-5.29.0.tar.xz">kpeople-5.29.0</a></td>
   <td align="right">58kB</td>
   <td><tt>7af90c764b6c1957875ae7906360f65c7751a514</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kplotting-5.29.0.tar.xz">kplotting-5.29.0</a></td>
   <td align="right">28kB</td>
   <td><tt>afec7b7a0138ed948f07309eb10e5801acfa5577</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kpty-5.29.0.tar.xz">kpty-5.29.0</a></td>
   <td align="right">2.5MB</td>
   <td><tt>7d057f8921dc101a65498a18d94f16882e934962</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/krunner-5.29.0.tar.xz">krunner-5.29.0</a></td>
   <td align="right">65kB</td>
   <td><tt>40a5230055ed4095440e3a052173b70cd55a9273</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kservice-5.29.0.tar.xz">kservice-5.29.0</a></td>
   <td align="right">2.6MB</td>
   <td><tt>1b4fd5dd07d163b576f755a9c2217ec23a114498</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/ktexteditor-5.29.0.tar.xz">ktexteditor-5.29.0</a></td>
   <td align="right">2.3MB</td>
   <td><tt>0602833b1ebd5a604924419c37bd84f3fceadf2f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/ktextwidgets-5.29.0.tar.xz">ktextwidgets-5.29.0</a></td>
   <td align="right">2.6MB</td>
   <td><tt>1f9db0687635d090834d45324b78b126574891d1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kunitconversion-5.29.0.tar.xz">kunitconversion-5.29.0</a></td>
   <td align="right">779kB</td>
   <td><tt>03da9ef5aa215a8a0146d75d794f176b70c38745</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kwallet-5.29.0.tar.xz">kwallet-5.29.0</a></td>
   <td align="right">301kB</td>
   <td><tt>69493d11371f984491370b2a49023e32bcda37c0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kwayland-5.29.0.tar.xz">kwayland-5.29.0</a></td>
   <td align="right">253kB</td>
   <td><tt>255fdf329cfd726d05febc02dbd47870d216af3d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kwidgetsaddons-5.29.0.tar.xz">kwidgetsaddons-5.29.0</a></td>
   <td align="right">4.1MB</td>
   <td><tt>aa0208e645ef0c299474eea1481ad35d11252e57</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kwindowsystem-5.29.0.tar.xz">kwindowsystem-5.29.0</a></td>
   <td align="right">2.5MB</td>
   <td><tt>f11a249ad713de21878f692519e1396f4cebeffb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kxmlgui-5.29.0.tar.xz">kxmlgui-5.29.0</a></td>
   <td align="right">3.0MB</td>
   <td><tt>13c74bd7ae3b553273e21de07656e72f5225c807</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/kxmlrpcclient-5.29.0.tar.xz">kxmlrpcclient-5.29.0</a></td>
   <td align="right">27kB</td>
   <td><tt>86d88f968e926c33f905f206d08d11882863d081</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/modemmanager-qt-5.29.0.tar.xz">modemmanager-qt-5.29.0</a></td>
   <td align="right">95kB</td>
   <td><tt>f34d740c2ac22525fdf2181055f2d5e8ad1eb697</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/networkmanager-qt-5.29.0.tar.xz">networkmanager-qt-5.29.0</a></td>
   <td align="right">153kB</td>
   <td><tt>0c081a8e72832ce18e31696f25f2bc99a708a089</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/oxygen-icons5-5.29.0.tar.xz">oxygen-icons5-5.29.0</a></td>
   <td align="right">223MB</td>
   <td><tt>e453f8264fd54880f030680be96274c4edba6485</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/plasma-framework-5.29.0.tar.xz">plasma-framework-5.29.0</a></td>
   <td align="right">4.4MB</td>
   <td><tt>d4265924dea50341c6dfd9041707b82b73bde094</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/prison-5.29.0.tar.xz">prison-5.29.0</a></td>
   <td align="right">13kB</td>
   <td><tt>597f9aad5d9d2820eded4dd151c78f34e0979797</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/solid-5.29.0.tar.xz">solid-5.29.0</a></td>
   <td align="right">269kB</td>
   <td><tt>2e73c05fb925e2d37ecdf1c128d9420e09befc8e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/sonnet-5.29.0.tar.xz">sonnet-5.29.0</a></td>
   <td align="right">2.5MB</td>
   <td><tt>768cf074d2d6ec8c89040e1d0f18ecfaf79d1f03</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/syntax-highlighting-5.29.0.tar.xz">syntax-highlighting-5.29.0</a></td>
   <td align="right">863kB</td>
   <td><tt>e6a581647c6d6d8c0e353a5749891ce915700930</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.29/threadweaver-5.29.0.tar.xz">threadweaver-5.29.0</a></td>
   <td align="right">1.3MB</td>
   <td><tt>3143edeeae1eafa4bcf6c4305e7cacff9bf2710c</tt></td>
</tr>

</table>
