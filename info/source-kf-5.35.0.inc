
<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">Sha1 Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/attica-5.35.0.tar.xz">attica-5.35.0</a></td>
   <td align="right">58kB</td>
   <td><tt>f20440c0d37205f8fb251c60973103c60687345f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/baloo-5.35.0.tar.xz">baloo-5.35.0</a></td>
   <td align="right">201kB</td>
   <td><tt>b4083b20746b7cb14fd7316dd0061287951877f7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/bluez-qt-5.35.0.tar.xz">bluez-qt-5.35.0</a></td>
   <td align="right">72kB</td>
   <td><tt>3fb56a03f47e1191d8acc2e99ab4988e88b034e0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/breeze-icons-5.35.0.tar.xz">breeze-icons-5.35.0</a></td>
   <td align="right">1.4MB</td>
   <td><tt>8d0b659e19bb62ea66f3c5058dc250c2195af517</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/extra-cmake-modules-5.35.0.tar.xz">extra-cmake-modules-5.35.0</a></td>
   <td align="right">295kB</td>
   <td><tt>7525d4386880852ba9143cb6f98c53bdff3b9f04</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/frameworkintegration-5.35.0.tar.xz">frameworkintegration-5.35.0</a></td>
   <td align="right">1.6MB</td>
   <td><tt>254d4890669a84903d87458c1a50bcf4e39b436f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kactivities-5.35.0.tar.xz">kactivities-5.35.0</a></td>
   <td align="right">59kB</td>
   <td><tt>a10c8f0dcd6ba906adc937350dd9e7fc57c93ab0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kactivities-stats-5.35.0.tar.xz">kactivities-stats-5.35.0</a></td>
   <td align="right">58kB</td>
   <td><tt>74a4ceba9edecdfef7d8d1a0cb0dbc3d2210241c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kapidox-5.35.0.tar.xz">kapidox-5.35.0</a></td>
   <td align="right">386kB</td>
   <td><tt>3925252fee4da8379964b76994c3092e9dc7ca95</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/karchive-5.35.0.tar.xz">karchive-5.35.0</a></td>
   <td align="right">109kB</td>
   <td><tt>2422de4bd47e0437d396c9557ffbd7af04df6cf3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kauth-5.35.0.tar.xz">kauth-5.35.0</a></td>
   <td align="right">2.3MB</td>
   <td><tt>876ab3206438c368db4c02b54bd2328060443cdd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kbookmarks-5.35.0.tar.xz">kbookmarks-5.35.0</a></td>
   <td align="right">1.1MB</td>
   <td><tt>41e92c2bf959a6a59ff350f71f9cf9da86a45f0e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kcmutils-5.35.0.tar.xz">kcmutils-5.35.0</a></td>
   <td align="right">2.5MB</td>
   <td><tt>45e7132133e35b627b62395013614a85ce758ecc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kcodecs-5.35.0.tar.xz">kcodecs-5.35.0</a></td>
   <td align="right">2.5MB</td>
   <td><tt>49edc82c64897d7d2ee854f4f3d2d173eee5feb3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kcompletion-5.35.0.tar.xz">kcompletion-5.35.0</a></td>
   <td align="right">2.4MB</td>
   <td><tt>8f47f2b7c32aeeca9011214728f0622156cb8aa4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kconfig-5.35.0.tar.xz">kconfig-5.35.0</a></td>
   <td align="right">231kB</td>
   <td><tt>bbc1eddba453cfe36e12ed4eb1b33dfa1f16578a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kconfigwidgets-5.35.0.tar.xz">kconfigwidgets-5.35.0</a></td>
   <td align="right">2.6MB</td>
   <td><tt>19b36c694309bcadb6e5144f1f1ef843a4550b9c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kcoreaddons-5.35.0.tar.xz">kcoreaddons-5.35.0</a></td>
   <td align="right">2.5MB</td>
   <td><tt>3fcc85fb0b4507ccac472a95363a33d91786d43a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kcrash-5.35.0.tar.xz">kcrash-5.35.0</a></td>
   <td align="right">20kB</td>
   <td><tt>b4fc1200946c64b5a2cc56386b2b12b9196f6d14</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kdbusaddons-5.35.0.tar.xz">kdbusaddons-5.35.0</a></td>
   <td align="right">34kB</td>
   <td><tt>42400462721481b79b88a06dd16fac9c878cb451</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kdeclarative-5.35.0.tar.xz">kdeclarative-5.35.0</a></td>
   <td align="right">2.5MB</td>
   <td><tt>c5a34c300ad7940d9788d990dc8130b5371329de</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kded-5.35.0.tar.xz">kded-5.35.0</a></td>
   <td align="right">37kB</td>
   <td><tt>7a6c74f6c950fd1a51fbbd0f48c2d1332dca7c48</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kdesignerplugin-5.35.0.tar.xz">kdesignerplugin-5.35.0</a></td>
   <td align="right">2.3MB</td>
   <td><tt>f32b38dadef607f36d932e45604b2c5a5fa80388</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kdesu-5.35.0.tar.xz">kdesu-5.35.0</a></td>
   <td align="right">43kB</td>
   <td><tt>61e4063af85e66694b8b734404a0bbc801092ea1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kdewebkit-5.35.0.tar.xz">kdewebkit-5.35.0</a></td>
   <td align="right">28kB</td>
   <td><tt>ba50bf5295982aba3a1581144b75ee0b66dc758c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kdnssd-5.35.0.tar.xz">kdnssd-5.35.0</a></td>
   <td align="right">2.3MB</td>
   <td><tt>9183d04c7c285740478dbf9a98fef462097d9c95</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kdoctools-5.35.0.tar.xz">kdoctools-5.35.0</a></td>
   <td align="right">429kB</td>
   <td><tt>cc442058174db01660d80420b7d0cf66cf962db7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kemoticons-5.35.0.tar.xz">kemoticons-5.35.0</a></td>
   <td align="right">1.6MB</td>
   <td><tt>7182f14e094b23016adfcfd8ae0b194b0948a517</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kfilemetadata-5.35.0.tar.xz">kfilemetadata-5.35.0</a></td>
   <td align="right">213kB</td>
   <td><tt>0356d1d15b5de112424e6b83e07162ecabf0b6f0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kglobalaccel-5.35.0.tar.xz">kglobalaccel-5.35.0</a></td>
   <td align="right">2.4MB</td>
   <td><tt>7568b0355d554c56552f7dcd755eaaebc81aebbd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kguiaddons-5.35.0.tar.xz">kguiaddons-5.35.0</a></td>
   <td align="right">38kB</td>
   <td><tt>95187f75df497e01903ba83be08b24ae57b356f4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/ki18n-5.35.0.tar.xz">ki18n-5.35.0</a></td>
   <td align="right">2.8MB</td>
   <td><tt>db94b5a69c5029bf7581b40bf5b003da44067ea6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kiconthemes-5.35.0.tar.xz">kiconthemes-5.35.0</a></td>
   <td align="right">1.3MB</td>
   <td><tt>f060cde7be4291eb17659bffdae07c9e26b7dbad</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kidletime-5.35.0.tar.xz">kidletime-5.35.0</a></td>
   <td align="right">25kB</td>
   <td><tt>4dcea1927a90ee120b74cacc4b9e8ae9ce371c6d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kimageformats-5.35.0.tar.xz">kimageformats-5.35.0</a></td>
   <td align="right">199kB</td>
   <td><tt>5bb77b64c622d73b95b344f442a8f5ea53e14952</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kinit-5.35.0.tar.xz">kinit-5.35.0</a></td>
   <td align="right">2.4MB</td>
   <td><tt>143917734c5bb65599d187abc63f1e1e43a2f27f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kio-5.35.0.tar.xz">kio-5.35.0</a></td>
   <td align="right">2.9MB</td>
   <td><tt>4c3f2ad925001448d204e7265335e79d786e5206</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kitemmodels-5.35.0.tar.xz">kitemmodels-5.35.0</a></td>
   <td align="right">380kB</td>
   <td><tt>ccf855e4b274829da8a0a59e672c63b15ac4b158</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kitemviews-5.35.0.tar.xz">kitemviews-5.35.0</a></td>
   <td align="right">2.3MB</td>
   <td><tt>387db75968c1fbc7eb118c165714640534a9a3bc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kjobwidgets-5.35.0.tar.xz">kjobwidgets-5.35.0</a></td>
   <td align="right">2.3MB</td>
   <td><tt>f0c881be2c8d4f4f9de009f05ef0db5c6d9950ae</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/knewstuff-5.35.0.tar.xz">knewstuff-5.35.0</a></td>
   <td align="right">3.1MB</td>
   <td><tt>2cfe320c45bd7d65ac1fa91cc6a7df192e4b3c73</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/knotifications-5.35.0.tar.xz">knotifications-5.35.0</a></td>
   <td align="right">2.3MB</td>
   <td><tt>9330369ef2ecfeaf571037a380f56e64eb77d7c6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/knotifyconfig-5.35.0.tar.xz">knotifyconfig-5.35.0</a></td>
   <td align="right">2.4MB</td>
   <td><tt>062d6a55e387afb282d44e4c713dc21903c0f32b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kpackage-5.35.0.tar.xz">kpackage-5.35.0</a></td>
   <td align="right">166kB</td>
   <td><tt>3b313e4b7844bd8b507d58ee827fe3776978657a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kparts-5.35.0.tar.xz">kparts-5.35.0</a></td>
   <td align="right">151kB</td>
   <td><tt>375e7da5ee3d378483520f2ffce2c9d520905b3d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kpeople-5.35.0.tar.xz">kpeople-5.35.0</a></td>
   <td align="right">57kB</td>
   <td><tt>41f678ff21c48e45820b2c89e47e5f5303993b18</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kplotting-5.35.0.tar.xz">kplotting-5.35.0</a></td>
   <td align="right">28kB</td>
   <td><tt>fd350a6c553e3f9f7eb4cda1fc57f78aa1569d9d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kpty-5.35.0.tar.xz">kpty-5.35.0</a></td>
   <td align="right">56kB</td>
   <td><tt>b9075a7493098c9858903451985ce906d9cdaed2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/krunner-5.35.0.tar.xz">krunner-5.35.0</a></td>
   <td align="right">65kB</td>
   <td><tt>74a5bf5f5fcf0ddd362f233f5f8ba325af9357c6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kservice-5.35.0.tar.xz">kservice-5.35.0</a></td>
   <td align="right">243kB</td>
   <td><tt>02f25e4fac3333d255e03c47632afba9b111da87</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/ktexteditor-5.35.0.tar.xz">ktexteditor-5.35.0</a></td>
   <td align="right">2.1MB</td>
   <td><tt>7e61e77a178401093c1f3d4d9b7502055dd81d9c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/ktextwidgets-5.35.0.tar.xz">ktextwidgets-5.35.0</a></td>
   <td align="right">300kB</td>
   <td><tt>74d49c3d7918729001c08fe3f680b5a633a3018b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kunitconversion-5.35.0.tar.xz">kunitconversion-5.35.0</a></td>
   <td align="right">753kB</td>
   <td><tt>7081e47e4ca50d64a6cc6998d7073f5c99f28669</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kwallet-5.35.0.tar.xz">kwallet-5.35.0</a></td>
   <td align="right">296kB</td>
   <td><tt>cacce10b8152f68f67488bca815ac58f644ac474</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kwayland-5.35.0.tar.xz">kwayland-5.35.0</a></td>
   <td align="right">255kB</td>
   <td><tt>53430d2f24567565e3bddc415bac128d47a32445</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kwidgetsaddons-5.35.0.tar.xz">kwidgetsaddons-5.35.0</a></td>
   <td align="right">2.0MB</td>
   <td><tt>80275625088de37335ee4734355f4e8da6ea8086</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kwindowsystem-5.35.0.tar.xz">kwindowsystem-5.35.0</a></td>
   <td align="right">163kB</td>
   <td><tt>64d2e08fd5e39b1cec21ecf6add6bc33e8c70b5d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kxmlgui-5.35.0.tar.xz">kxmlgui-5.35.0</a></td>
   <td align="right">863kB</td>
   <td><tt>53492e7d3fb6861638e7daf541d6149497f62142</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/kxmlrpcclient-5.35.0.tar.xz">kxmlrpcclient-5.35.0</a></td>
   <td align="right">27kB</td>
   <td><tt>038190176ac71eff270baba6883dd40349f79040</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/modemmanager-qt-5.35.0.tar.xz">modemmanager-qt-5.35.0</a></td>
   <td align="right">95kB</td>
   <td><tt>97c775960dcc0ca58244cc9c5320b74b16b10f6b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/networkmanager-qt-5.35.0.tar.xz">networkmanager-qt-5.35.0</a></td>
   <td align="right">153kB</td>
   <td><tt>ae0978a5c11ba2cc350f9b6ab9cbac1bf1d7a7e9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/oxygen-icons5-5.35.0.tar.xz">oxygen-icons5-5.35.0</a></td>
   <td align="right">223MB</td>
   <td><tt>b605f8228f2610c8e469a1e2cd0773eaf533152c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/plasma-framework-5.35.0.tar.xz">plasma-framework-5.35.0</a></td>
   <td align="right">4.3MB</td>
   <td><tt>e2e3deb96f4154b10c2a58f9544c529040bd772e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/prison-5.35.0.tar.xz">prison-5.35.0</a></td>
   <td align="right">13kB</td>
   <td><tt>5c246d42962b74712f2b46a185cf159e14302997</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/solid-5.35.0.tar.xz">solid-5.35.0</a></td>
   <td align="right">241kB</td>
   <td><tt>80a439787e2701f81ca0a5a92a797b805bb7b8ba</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/sonnet-5.35.0.tar.xz">sonnet-5.35.0</a></td>
   <td align="right">277kB</td>
   <td><tt>631a23a2f775e988feb4c2c8b0057344c2f32446</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/syntax-highlighting-5.35.0.tar.xz">syntax-highlighting-5.35.0</a></td>
   <td align="right">964kB</td>
   <td><tt>7b47ff695d12f70002ad86cbdc7290a52095049b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.35/threadweaver-5.35.0.tar.xz">threadweaver-5.35.0</a></td>
   <td align="right">1.3MB</td>
   <td><tt>59859249c4b4c85cc8bc3ad040d26dd4cf0fab9c</tt></td>
</tr>

</table>
