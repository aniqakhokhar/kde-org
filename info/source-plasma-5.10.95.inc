<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">Sha256 Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/bluedevil-5.10.95.tar.xz">bluedevil-5.10.95</a></td>
   <td align="right">143kB</td>
   <td><tt>67f984ad9274cda3be57e14e315b1422934561d853403e8b4f34595ebcf9b8ef</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/breeze-5.10.95.tar.xz">breeze-5.10.95</a></td>
   <td align="right">21MB</td>
   <td><tt>de22726442b0f13dc7537257a2720316507de56a6d98c55061d5edb0fe915db2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/breeze-grub-5.10.95.tar.xz">breeze-grub-5.10.95</a></td>
   <td align="right">2.9MB</td>
   <td><tt>afc6d6ecad73189586bc43f2160c6dcdb08052ff525a8e9f6d8776a7ae502faa</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/breeze-gtk-5.10.95.tar.xz">breeze-gtk-5.10.95</a></td>
   <td align="right">206kB</td>
   <td><tt>56a17f96f6d68f8074c2dbd0d1934c2d94462b19f316f8d8c834fe93b906ab67</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/breeze-plymouth-5.10.95.tar.xz">breeze-plymouth-5.10.95</a></td>
   <td align="right">102kB</td>
   <td><tt>95b8dc068f86fb99a33076d1500210837f7770b2f603a154ddc528919eb1d39a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/discover-5.10.95.tar.xz">discover-5.10.95</a></td>
   <td align="right">9.7MB</td>
   <td><tt>8ed6a95b7d1c2e1c4ddfc6794d2953b05d3507a75e0550d643312a275242c1fe</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/drkonqi-5.10.95.tar.xz">drkonqi-5.10.95</a></td>
   <td align="right">722kB</td>
   <td><tt>13a453f54936035977e682efab740d07ab6d9c2693c3d9e72628a3a4b8e862e3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/kactivitymanagerd-5.10.95.tar.xz">kactivitymanagerd-5.10.95</a></td>
   <td align="right">82kB</td>
   <td><tt>fb9a3d3e0f05a92d1f37790281ce28ba458ec31bcac6eed9ede085456bee0c9a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/kde-cli-tools-5.10.95.tar.xz">kde-cli-tools-5.10.95</a></td>
   <td align="right">564kB</td>
   <td><tt>24f86cf0f53368a667b518fdc4d2c2a174d07145377284dfd2d2c771e2bf8e53</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/kdecoration-5.10.95.tar.xz">kdecoration-5.10.95</a></td>
   <td align="right">34kB</td>
   <td><tt>27f09b4046a1f3d6b55bcaef3439957246d89b79e1b553571b0c7f2ac4681b79</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/kde-gtk-config-5.10.95.tar.xz">kde-gtk-config-5.10.95</a></td>
   <td align="right">150kB</td>
   <td><tt>f1abd6dc9dc81f4744160c592360d05755e1efe0115689ccb4aa3fdf3bd6ea17</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/kdeplasma-addons-5.10.95.tar.xz">kdeplasma-addons-5.10.95</a></td>
   <td align="right">1.3MB</td>
   <td><tt>98db72e9a8fa1076faa78378f9d82ab11f3daac4b310f6ae22c4c3631c885868</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/kgamma5-5.10.95.tar.xz">kgamma5-5.10.95</a></td>
   <td align="right">58kB</td>
   <td><tt>a47035caae83933cb933eca8bfd4c18849399f950c668f165fb4432708214339</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/khotkeys-5.10.95.tar.xz">khotkeys-5.10.95</a></td>
   <td align="right">1.0MB</td>
   <td><tt>ce586e3da75c622317057f26d0a209a52c860ba45144310859bfd84a305b05cb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/kinfocenter-5.10.95.tar.xz">kinfocenter-5.10.95</a></td>
   <td align="right">1.2MB</td>
   <td><tt>503dba4c24c346da08f4c5bac5688caae36efd006d83a03fcc35ee073913c7ff</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/kmenuedit-5.10.95.tar.xz">kmenuedit-5.10.95</a></td>
   <td align="right">641kB</td>
   <td><tt>3d696451382bd3c905fe5fd2645a97391a4b6d5105b479bb938456733b62619b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/kscreen-5.10.95.tar.xz">kscreen-5.10.95</a></td>
   <td align="right">114kB</td>
   <td><tt>daebc355fa1c6e913ccd150d004b97b1a3c789c46695d02f9fa7404c286a1487</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/kscreenlocker-5.10.95.tar.xz">kscreenlocker-5.10.95</a></td>
   <td align="right">115kB</td>
   <td><tt>d0d716a9b97a940f352abb6a40d677ea78e156330beb027cda440338c88221ca</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/ksshaskpass-5.10.95.tar.xz">ksshaskpass-5.10.95</a></td>
   <td align="right">20kB</td>
   <td><tt>4374ec96f953814d97f4e585c1d8d5538c8b0c294e6e3d8933c436ab10ab4ac4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/ksysguard-5.10.95.tar.xz">ksysguard-5.10.95</a></td>
   <td align="right">480kB</td>
   <td><tt>3e0e0d3d974b800dd3330cb2b3668d5f6c25c6a856c5cc52398edbe104d859ac</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/kwallet-pam-5.10.95.tar.xz">kwallet-pam-5.10.95</a></td>
   <td align="right">18kB</td>
   <td><tt>d715b88be84387edc2421655267451d76a624f8b67b04fb7a2ac19e157d2cf56</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/kwayland-integration-5.10.95.tar.xz">kwayland-integration-5.10.95</a></td>
   <td align="right">18kB</td>
   <td><tt>8369f950ad3877ca2cbb5fc64e4c36c796462f2340e4cd6c3b69e71c765e8fd4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/kwin-5.10.95.tar.xz">kwin-5.10.95</a></td>
   <td align="right">4.6MB</td>
   <td><tt>155b1cb61d939be040a2b126b46a05d4c5fdcdd0f97ff579aad13a6cd0d92e2b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/kwrited-5.10.95.tar.xz">kwrited-5.10.95</a></td>
   <td align="right">19kB</td>
   <td><tt>343af97b80ba54d505f3f95aca83780466dca192bdd3bad83b298bb4008e6215</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/libkscreen-5.10.95.tar.xz">libkscreen-5.10.95</a></td>
   <td align="right">91kB</td>
   <td><tt>ffecb40ec67324b1beb48e12cfcd4e302d13548afb19d7e79159567601d88ded</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/libksysguard-5.10.95.tar.xz">libksysguard-5.10.95</a></td>
   <td align="right">553kB</td>
   <td><tt>3c71dee41303562446b9ff94b822067f598f231cce3a59e389f3779a727dbca4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/milou-5.10.95.tar.xz">milou-5.10.95</a></td>
   <td align="right">54kB</td>
   <td><tt>b2f5e24749105eacdf402cb711b33724b963f07bbc08b619a7d12f2628af8dcc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/oxygen-5.10.95.tar.xz">oxygen-5.10.95</a></td>
   <td align="right">4.2MB</td>
   <td><tt>d50dbf5d5a78b4ba92e15d06ee54af0598076771ccef01f0c6c64e7ffba79f61</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/plasma-desktop-5.10.95.tar.xz">plasma-desktop-5.10.95</a></td>
   <td align="right">8.0MB</td>
   <td><tt>14b747d5810c9357bef8278096fa1e36bdd3255c96377d1308f0e43f0c2387fc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/plasma-integration-5.10.95.tar.xz">plasma-integration-5.10.95</a></td>
   <td align="right">51kB</td>
   <td><tt>783deac27011592cb364c4788e1854841ccb728b984fec01d48966b2e64831a4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/plasma-nm-5.10.95.tar.xz">plasma-nm-5.10.95</a></td>
   <td align="right">680kB</td>
   <td><tt>1ffc2d68ac0be82bf905ed4de6b7410596642fb0b144b941d8ec34018c57efe8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/plasma-pa-5.10.95.tar.xz">plasma-pa-5.10.95</a></td>
   <td align="right">83kB</td>
   <td><tt>34b893f8708a73864055d173f04f0eb3af1b6f2a4312af73821fad2b1d815e66</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/plasma-sdk-5.10.95.tar.xz">plasma-sdk-5.10.95</a></td>
   <td align="right">244kB</td>
   <td><tt>6abfad98058d7e9a8f35fdb832fc96922cb35bea777459bb302b4ec56286d0c4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/plasma-tests-5.10.95.tar.xz">plasma-tests-5.10.95</a></td>
   <td align="right">1kB</td>
   <td><tt>78229e0947f9ba6ea65bc58d0d8894fae72fe12ef0e2c146deae93a1b7d81a8c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/plasma-vault-5.10.95.tar.xz">plasma-vault-5.10.95</a></td>
   <td align="right">85kB</td>
   <td><tt>e08f18d40993098783e73630e0bd02ed350329dfa41b9c382f0d46fba6834482</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/plasma-workspace-5.10.95.tar.xz">plasma-workspace-5.10.95</a></td>
   <td align="right">6.0MB</td>
   <td><tt>f651d454564c880a8edb2bae1c034410df5e27e02f349b081cf666ea494e7131</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/plasma-workspace-wallpapers-5.10.95.tar.xz">plasma-workspace-wallpapers-5.10.95</a></td>
   <td align="right">43MB</td>
   <td><tt>14d78e7e8ac87774c836ebf40604bd565a0df785f38aa48ec599c154f85dc9f3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/plymouth-kcm-5.10.95.tar.xz">plymouth-kcm-5.10.95</a></td>
   <td align="right">34kB</td>
   <td><tt>56a2c4dfe00a80930e19d917052e6ea6073b7308fc5ce649f94ce54ac5647a25</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/polkit-kde-agent-1-5.10.95.tar.xz">polkit-kde-agent-1-5.10.95</a></td>
   <td align="right">40kB</td>
   <td><tt>2558d8402675bfc482b336bbafe1aa033dbcecf37dc811d78bb040c629185c29</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/powerdevil-5.10.95.tar.xz">powerdevil-5.10.95</a></td>
   <td align="right">371kB</td>
   <td><tt>2e073373ee208d6d33bc0520248729c30513168aa5fe64229e9b5259f04476ec</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/sddm-kcm-5.10.95.tar.xz">sddm-kcm-5.10.95</a></td>
   <td align="right">58kB</td>
   <td><tt>1f72164e62317a8576e3b00324e86dfca4f72fac0fe196d2caa94f5992a13701</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/systemsettings-5.10.95.tar.xz">systemsettings-5.10.95</a></td>
   <td align="right">166kB</td>
   <td><tt>2fe2ff3e9a7177be1764e9e97c28a3455720244182cf23363461fa2dbc6f8e58</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/user-manager-5.10.95.tar.xz">user-manager-5.10.95</a></td>
   <td align="right">531kB</td>
   <td><tt>0326798ad4fa1016041b2835c72bd510793f52cdfec69a607fc72b488a8d12c1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.10.95/xdg-desktop-portal-kde-5.10.95.tar.xz">xdg-desktop-portal-kde-5.10.95</a></td>
   <td align="right">29kB</td>
   <td><tt>a57ac2e7ccdcfc1a52e1fcca5e32a68f53c2d0d2923ba54f3f1af5351502db69</tt></td>
</tr>

</table>
