<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">Sha256 Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/bluedevil-5.10.2.tar.xz">bluedevil-5.10.2</a></td>
   <td align="right">141kB</td>
   <td><tt>27fbd7e51420f8900fd63315c4ca80a79e6f7dfa10890ac05b244bbf977c2094</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/breeze-5.10.2.tar.xz">breeze-5.10.2</a></td>
   <td align="right">16MB</td>
   <td><tt>7909034fece1ba17e53ec143924a6b49f240a28b9df09e0add5deb033c52dcf3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/breeze-grub-5.10.2.tar.xz">breeze-grub-5.10.2</a></td>
   <td align="right">3.0MB</td>
   <td><tt>e2ab4db22d12bd0c7bbff32b341de62ea517f9e627b9c0cb7908661fe87e6775</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/breeze-gtk-5.10.2.tar.xz">breeze-gtk-5.10.2</a></td>
   <td align="right">206kB</td>
   <td><tt>dd460a23f3531e9b20565c789702e9e2214a57a8556fe7f729f4a303dc5cef1a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/breeze-plymouth-5.10.2.tar.xz">breeze-plymouth-5.10.2</a></td>
   <td align="right">102kB</td>
   <td><tt>ed22b2dd666d1f46ec743c4836bf64bc70fdd5443dda7edc1d57ef03c7046035</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/discover-5.10.2.tar.xz">discover-5.10.2</a></td>
   <td align="right">9.7MB</td>
   <td><tt>9afa6ca8e0f2202313099a554d831d1b4a43aa89c6570589ef37da69cbf4c54f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/kactivitymanagerd-5.10.2.tar.xz">kactivitymanagerd-5.10.2</a></td>
   <td align="right">82kB</td>
   <td><tt>12a7434640df34c388cdd91bb8bc412429fd946f8909e7f5642ebdafed60eef4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/kde-cli-tools-5.10.2.tar.xz">kde-cli-tools-5.10.2</a></td>
   <td align="right">492kB</td>
   <td><tt>10646f2030c28d756d718f9c1da35d341f1a3256cc869ba37ccf2a105f773b34</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/kdecoration-5.10.2.tar.xz">kdecoration-5.10.2</a></td>
   <td align="right">34kB</td>
   <td><tt>7ca62e41c76d1d3df31c83ea1ac49cf3746e64786679d4eb30dd79c59442af16</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/kde-gtk-config-5.10.2.tar.xz">kde-gtk-config-5.10.2</a></td>
   <td align="right">148kB</td>
   <td><tt>a8b85c6b52373a5afa09f1e5b76c9faeecce16af3a64534935bb986a0b75cd24</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/kdeplasma-addons-5.10.2.tar.xz">kdeplasma-addons-5.10.2</a></td>
   <td align="right">1.3MB</td>
   <td><tt>e0a2b4896681e3ff206e52e98d67e5b170ae81d50e151816d993c9ecc1fff6fb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/kgamma5-5.10.2.tar.xz">kgamma5-5.10.2</a></td>
   <td align="right">59kB</td>
   <td><tt>acb2ff91017e271f9157aefe02b7afd7532c6e63ce6789eefa172f790ab09643</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/khotkeys-5.10.2.tar.xz">khotkeys-5.10.2</a></td>
   <td align="right">684kB</td>
   <td><tt>ba878fbfd8d78a34c8ec502978dff21bacdc669c69944a20d9595610cb6abbcd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/kinfocenter-5.10.2.tar.xz">kinfocenter-5.10.2</a></td>
   <td align="right">1.2MB</td>
   <td><tt>1202b32504925f6746369e5ad63ef7c014e89ea2d3a820387b1dd4b46a4718e4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/kmenuedit-5.10.2.tar.xz">kmenuedit-5.10.2</a></td>
   <td align="right">635kB</td>
   <td><tt>df7231d481c421604183f3fc4d01aeaae15576e78bd58f6775ffb624502e9e35</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/kscreen-5.10.2.tar.xz">kscreen-5.10.2</a></td>
   <td align="right">112kB</td>
   <td><tt>87867c37b5b60095aea90c057d6d10e738f61b39ebadd80c45f7e07e50dc040c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/kscreenlocker-5.10.2.tar.xz">kscreenlocker-5.10.2</a></td>
   <td align="right">108kB</td>
   <td><tt>6545f32e4ba80000bedfa676a05fccd8d636e8a77c128ccbc309149c149b3b1d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/ksshaskpass-5.10.2.tar.xz">ksshaskpass-5.10.2</a></td>
   <td align="right">19kB</td>
   <td><tt>df6fb0855332fafda43f5d9f519a9459a3169007dc625fdc69d9e32195be4bd6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/ksysguard-5.10.2.tar.xz">ksysguard-5.10.2</a></td>
   <td align="right">477kB</td>
   <td><tt>9b0593b217b9ad9b3c9acdab85cee1f0a8de663d85e79eeeb0014316121dbfd9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/kwallet-pam-5.10.2.tar.xz">kwallet-pam-5.10.2</a></td>
   <td align="right">17kB</td>
   <td><tt>aac1cfbf83a1696d103468d7d3d38ebccf94d98c9cd30331aa886b625d167106</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/kwayland-integration-5.10.2.tar.xz">kwayland-integration-5.10.2</a></td>
   <td align="right">18kB</td>
   <td><tt>6a480d97e1dc9ded1fe634b58e5b8b92c5fd44aa23ad5ee1df98d568293dcaba</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/kwin-5.10.2.tar.xz">kwin-5.10.2</a></td>
   <td align="right">4.4MB</td>
   <td><tt>f257f48c1ac2bfef12b6e953565514448c05391c65daaca181561783a6f16bbf</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/kwrited-5.10.2.tar.xz">kwrited-5.10.2</a></td>
   <td align="right">19kB</td>
   <td><tt>c89e0f8074576f1c01047b00337bc3f1c62bb14b76093717122c0d44a3b9e6c3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/libkscreen-5.10.2.tar.xz">libkscreen-5.10.2</a></td>
   <td align="right">91kB</td>
   <td><tt>16eed160dae229991211cadb081c3296ce2e3f2f01f85ed90c518bc1ca6f2a70</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/libksysguard-5.10.2.tar.xz">libksysguard-5.10.2</a></td>
   <td align="right">551kB</td>
   <td><tt>30e63f2ad9b933f5e261048a9c2306c5b5d045f00faac16d89ebafc2c6aa23f4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/milou-5.10.2.tar.xz">milou-5.10.2</a></td>
   <td align="right">54kB</td>
   <td><tt>fc472351d2f7a1cbae77b166027c84bbc43280dc063aa7c37c2b68fac6ba8419</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/oxygen-5.10.2.tar.xz">oxygen-5.10.2</a></td>
   <td align="right">4.2MB</td>
   <td><tt>373842080580abd9bebf16ae1653aaba8e657b1c1cfe4573ee79a95d64cba321</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/plasma-desktop-5.10.2.tar.xz">plasma-desktop-5.10.2</a></td>
   <td align="right">5.8MB</td>
   <td><tt>3677afa111e0d37fa1f222bc3c397b6b1998d34e4f0c72f6883b5469ac285fd6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/plasma-integration-5.10.2.tar.xz">plasma-integration-5.10.2</a></td>
   <td align="right">50kB</td>
   <td><tt>9ea29e476f395d5074b268262e5b0ee9ae3fcc00f6c793f9cc655846efd8f93f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/plasma-nm-5.10.2.tar.xz">plasma-nm-5.10.2</a></td>
   <td align="right">677kB</td>
   <td><tt>81c3030fe9e51532df946ebfeddc04e0dfb375c00c7a01f08374defdbfdf852f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/plasma-pa-5.10.2.tar.xz">plasma-pa-5.10.2</a></td>
   <td align="right">77kB</td>
   <td><tt>37ee85ea5581f812cfa6baf9cb4a1d1b21ffbb94313b732c7b9e4c11cba575ad</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/plasma-sdk-5.10.2.tar.xz">plasma-sdk-5.10.2</a></td>
   <td align="right">239kB</td>
   <td><tt>445efe34450edb6fce6ea87db011f4a20b71b2071117854967a2e5ffc963ec5a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/plasma-tests-5.10.2.tar.xz">plasma-tests-5.10.2</a></td>
   <td align="right">1kB</td>
   <td><tt>b118fc45f1b1e62f3e0cb7a780375fbcedc899a25a2cfaca88a3a3113280cacf</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/plasma-workspace-5.10.2.tar.xz">plasma-workspace-5.10.2</a></td>
   <td align="right">6.6MB</td>
   <td><tt>c2618cebc922efe95d231a7064286c869c0f355378538f2accdc09ede2b80187</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/plasma-workspace-wallpapers-5.10.2.tar.xz">plasma-workspace-wallpapers-5.10.2</a></td>
   <td align="right">43MB</td>
   <td><tt>37cf134527bf4107d28f1596b17e710da0f8e35ae72b77a85d654fe6c23855c3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/plymouth-kcm-5.10.2.tar.xz">plymouth-kcm-5.10.2</a></td>
   <td align="right">31kB</td>
   <td><tt>6a0c001a19060cdd0e8ab2f60e4522154ea318f2b8df8d87373120c07c09bfbb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/polkit-kde-agent-1-5.10.2.tar.xz">polkit-kde-agent-1-5.10.2</a></td>
   <td align="right">40kB</td>
   <td><tt>df76864e55a8a2610d07ef1548ef8aa0c3cbf31601cd7235cef378c4d0184913</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/powerdevil-5.10.2.tar.xz">powerdevil-5.10.2</a></td>
   <td align="right">353kB</td>
   <td><tt>be26a56fb18a9f45673fd37a03580d6ac845e4d17638b9385a4d731131d789bf</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/sddm-kcm-5.10.2.tar.xz">sddm-kcm-5.10.2</a></td>
   <td align="right">56kB</td>
   <td><tt>1e0daf242f8bdc7c428dac40eda2265346bd971b14ce6f48b706f5d64820ef82</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/systemsettings-5.10.2.tar.xz">systemsettings-5.10.2</a></td>
   <td align="right">152kB</td>
   <td><tt>e85f5ebf2e395c4d43c3819a86b5842dc7c5cb20625d1c29f95b27a57678d2dc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/user-manager-5.10.2.tar.xz">user-manager-5.10.2</a></td>
   <td align="right">532kB</td>
   <td><tt>1ca5ce4865ee137ac1dd05b67ea6a7afd369190099008982b1a21d4fc80781a9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.10.2/xdg-desktop-portal-kde-5.10.2.tar.xz">xdg-desktop-portal-kde-5.10.2</a></td>
   <td align="right">27kB</td>
   <td><tt>bb4643d8d2347d511e0a0e7fb297c2da5b994b3622f7bddbd892913974db0d9b</tt></td>
</tr>

</table>
