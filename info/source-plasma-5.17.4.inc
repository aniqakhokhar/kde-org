<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">Sha256 Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/bluedevil-5.17.4.tar.xz">bluedevil-5.17.4</a></td>
   <td align="right">156kB</td>
   <td><tt>e1d8369deb467da0843f68444df565998ff9e5f339f7493931c58e8940c1b694</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/breeze-5.17.4.tar.xz">breeze-5.17.4</a></td>
   <td align="right">15MB</td>
   <td><tt>7fce20b2ab79e4a814a2d117a03738cb0a4402845062adf4b2b04bc5d8c2a67f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/breeze-grub-5.17.4.tar.xz">breeze-grub-5.17.4</a></td>
   <td align="right">2.9MB</td>
   <td><tt>7849769a8464430839a92b90638dae9c49d93612cb9317504569c205ec0bb49f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/breeze-gtk-5.17.4.tar.xz">breeze-gtk-5.17.4</a></td>
   <td align="right">43kB</td>
   <td><tt>fb64b95982b524a5646a9410b4906bf37cdd174162eba3ac6163ee347e27d424</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/breeze-plymouth-5.17.4.tar.xz">breeze-plymouth-5.17.4</a></td>
   <td align="right">103kB</td>
   <td><tt>9fd2f7fe9f14d5a0525ce9284fe82958c19243f83ad3f34904aaa7de8ecee67c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/discover-5.17.4.tar.xz">discover-5.17.4</a></td>
   <td align="right">9.9MB</td>
   <td><tt>530dae9da8e511f190c87c7e81db9b8cdcbd5d0d0cbf2651bb0dd627fb9ed1ba</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/drkonqi-5.17.4.tar.xz">drkonqi-5.17.4</a></td>
   <td align="right">723kB</td>
   <td><tt>b85c63f731e3adc368c5b43e8572e769cd0f89db7bc40014854f2e1ff336c3bd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/kactivitymanagerd-5.17.4.tar.xz">kactivitymanagerd-5.17.4</a></td>
   <td align="right">84kB</td>
   <td><tt>60c3214adf0bd4bbbe558b897fadcf800e9f8b4aaf32bf7f116d43c1c35705b3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/kde-cli-tools-5.17.4.tar.xz">kde-cli-tools-5.17.4</a></td>
   <td align="right">578kB</td>
   <td><tt>e63f5e2f8abeb8e3478174609b1671ff61301bef7b9e50ca5a7c35e7448d3b9e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/kdecoration-5.17.4.tar.xz">kdecoration-5.17.4</a></td>
   <td align="right">42kB</td>
   <td><tt>a20d496705c51d2fa5fcbbe12ef29457b807438f7aaaab7670db6fa5417993d7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/kde-gtk-config-5.17.4.tar.xz">kde-gtk-config-5.17.4</a></td>
   <td align="right">151kB</td>
   <td><tt>a231312cac94354955540efc3d6c28ac1ba793a5e04129007c307352d0cced46</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/kdeplasma-addons-5.17.4.tar.xz">kdeplasma-addons-5.17.4</a></td>
   <td align="right">592kB</td>
   <td><tt>2b790cf0cc48f5a7fdf87579be2ff39da92b47ee3f13ec616c14c866b97392c9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/kgamma5-5.17.4.tar.xz">kgamma5-5.17.4</a></td>
   <td align="right">78kB</td>
   <td><tt>ab8cd07b519816a6ccb316cebece2381232e113bdb22007535950f8324f5f398</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/khotkeys-5.17.4.tar.xz">khotkeys-5.17.4</a></td>
   <td align="right">1.7MB</td>
   <td><tt>2333a6a12cc0e30fd7f449daf1f74ffe22125a5c7cc62f92bcff59b6817a2baf</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/kinfocenter-5.17.4.tar.xz">kinfocenter-5.17.4</a></td>
   <td align="right">1.2MB</td>
   <td><tt>2e6a542870b37e7e36a255205648c957723215f4fa7f1154e2fbdfe78d15d502</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/kmenuedit-5.17.4.tar.xz">kmenuedit-5.17.4</a></td>
   <td align="right">796kB</td>
   <td><tt>4b0cb5cc5c29915def13818670746a018bb760b5fcfa4ee936a1181fc788628a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/kscreen-5.17.4.tar.xz">kscreen-5.17.4</a></td>
   <td align="right">97kB</td>
   <td><tt>7f3c5849975d870ef510f1b0dc819e8d1f0a67e13cca91a849333d90077d6878</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/kscreenlocker-5.17.4.tar.xz">kscreenlocker-5.17.4</a></td>
   <td align="right">120kB</td>
   <td><tt>9c7d0d5efad6a8d28889b75b08f17b074000959f03ed2c78b7f69e0af6c1466c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/ksshaskpass-5.17.4.tar.xz">ksshaskpass-5.17.4</a></td>
   <td align="right">20kB</td>
   <td><tt>ab42898b0dc1250c131ae00c67f8b8a2e7d3d116a22baccf1486161178b8392f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/ksysguard-5.17.4.tar.xz">ksysguard-5.17.4</a></td>
   <td align="right">502kB</td>
   <td><tt>e43a241ad0810fc1abbad9bb5ac631eda9d2127370fbfab52c7b5b3feb0040a6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/kwallet-pam-5.17.4.tar.xz">kwallet-pam-5.17.4</a></td>
   <td align="right">19kB</td>
   <td><tt>43bbe31e0c5f4092b3ba42b73deadb4e28d10fb1566320fa48ba377a1fa1c3db</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/kwayland-integration-5.17.4.tar.xz">kwayland-integration-5.17.4</a></td>
   <td align="right">19kB</td>
   <td><tt>3c6b14bcf7f6f3a927d485afba9ed3051cbe513441ed17f5d484419388da825c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/kwin-5.17.4.tar.xz">kwin-5.17.4</a></td>
   <td align="right">5.9MB</td>
   <td><tt>ffbafbbc34fc791abe3be255615402545519df02cae06973aa54f669f793ae06</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/kwrited-5.17.4.tar.xz">kwrited-5.17.4</a></td>
   <td align="right">19kB</td>
   <td><tt>d772e968be4ad96166f3747da2a252a9978f879b9fcd8a6e84ad86f78baf4d36</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/libkscreen-5.17.4.tar.xz">libkscreen-5.17.4</a></td>
   <td align="right">80kB</td>
   <td><tt>9d9e1871959948b1d4eea67c070c2123e0c9565ab011e43f3489e7a0f4c4ab29</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/libksysguard-5.17.4.tar.xz">libksysguard-5.17.4</a></td>
   <td align="right">592kB</td>
   <td><tt>e88c20e9e0112c00f5dd4fa26fcc5a7f2e682e8672e32913970f138a1c8f1ec6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/milou-5.17.4.tar.xz">milou-5.17.4</a></td>
   <td align="right">61kB</td>
   <td><tt>53eb074ea4dda3172e44eafefac1c1b53aa166b60dfedce4fedb15ee8d5ec5b5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/oxygen-5.17.4.tar.xz">oxygen-5.17.4</a></td>
   <td align="right">4.2MB</td>
   <td><tt>040b6facd9a9d609a882b928e8e0ae9a6e3d57ebbcb3ac513e9c9736afbbf770</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/plasma-browser-integration-5.17.4.tar.xz">plasma-browser-integration-5.17.4</a></td>
   <td align="right">143kB</td>
   <td><tt>312a45447d76ee01fd45c656c256b1b2b2ec9805ebec7d8bcc93ae7cd9e9c628</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/plasma-desktop-5.17.4.tar.xz">plasma-desktop-5.17.4</a></td>
   <td align="right">9.0MB</td>
   <td><tt>c17d1854947c6e647bcf359773da8c8081fb562338b84fdc6739a063348943ff</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/plasma-integration-5.17.4.tar.xz">plasma-integration-5.17.4</a></td>
   <td align="right">55kB</td>
   <td><tt>b632b6fe355c15d825dd46345e83763c6867b7054a31ccd263bfed4674243a7f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/plasma-nm-5.17.4.tar.xz">plasma-nm-5.17.4</a></td>
   <td align="right">794kB</td>
   <td><tt>d7a49d33c40f89b435f43940b37f840aaada86cb20b753995b6641c01ba923a2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/plasma-pa-5.17.4.tar.xz">plasma-pa-5.17.4</a></td>
   <td align="right">101kB</td>
   <td><tt>43a366147323c583dd032c46fab68e4b1ff3a28e4ff1239eebed5b7536f41dde</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/plasma-sdk-5.17.4.tar.xz">plasma-sdk-5.17.4</a></td>
   <td align="right">252kB</td>
   <td><tt>f7766bb713b37881bd1095d1a11d6d90e350e75cca275dabb60dc3db20e127ef</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/plasma-tests-5.17.4.tar.xz">plasma-tests-5.17.4</a></td>
   <td align="right">1kB</td>
   <td><tt>a41653e9730c91d3f8c64cd99b8e7603dc2041cb164146e4467b141c6e632951</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/plasma-thunderbolt-5.17.4.tar.xz">plasma-thunderbolt-5.17.4</a></td>
   <td align="right">46kB</td>
   <td><tt>45325717929bd3d56cafcb5b52b151a68e2b850d4e4fcfd091212d7a42316335</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/plasma-vault-5.17.4.tar.xz">plasma-vault-5.17.4</a></td>
   <td align="right">126kB</td>
   <td><tt>596f1fff1292db1e98277fb61d7146f12ab5a3b487bc760ae1eed97c5c971193</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/plasma-workspace-5.17.4.tar.xz">plasma-workspace-5.17.4</a></td>
   <td align="right">4.7MB</td>
   <td><tt>62ca5bd2c8bc962682f6e2d7ff43aa8fae13bf5e684082dd8b92f2a4ce436c5b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/plasma-workspace-wallpapers-5.17.4.tar.xz">plasma-workspace-wallpapers-5.17.4</a></td>
   <td align="right">32MB</td>
   <td><tt>a74405d1267e88b126e3d6f785424c92297e19426bc185b69bc737a3199e82bd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/plymouth-kcm-5.17.4.tar.xz">plymouth-kcm-5.17.4</a></td>
   <td align="right">38kB</td>
   <td><tt>f34e5bbf334b3cc5bfe2b3bd4937cab0f7cf7bbe6864131eb55ed7a239adb208</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/polkit-kde-agent-1-5.17.4.tar.xz">polkit-kde-agent-1-5.17.4</a></td>
   <td align="right">42kB</td>
   <td><tt>042677cab65ab4c51632398bb9b3204cd09cef5bb4648949a7a3914bba4c6d49</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/powerdevil-5.17.4.tar.xz">powerdevil-5.17.4</a></td>
   <td align="right">580kB</td>
   <td><tt>8bd49fa5d1f5886dbc23f42e35c3e7b1ece6502c2c22cb20c981060ac250de80</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/sddm-kcm-5.17.4.tar.xz">sddm-kcm-5.17.4</a></td>
   <td align="right">57kB</td>
   <td><tt>b2fe7cc5d6aa8d26922aea122ce5b4dbd9621051b7e4c08e0d9ad382917f2e25</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/systemsettings-5.17.4.tar.xz">systemsettings-5.17.4</a></td>
   <td align="right">168kB</td>
   <td><tt>12055008f90f4d53f3b5dccaefe1082032522f17d64e8925b680a5cb97853bd9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/user-manager-5.17.4.tar.xz">user-manager-5.17.4</a></td>
   <td align="right">2.0MB</td>
   <td><tt>1e8021ffeba4eb146c9bc1bf12f0d563d5c0da1bf75d0d63779b1f98b02c0be4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.17.4/xdg-desktop-portal-kde-5.17.4.tar.xz">xdg-desktop-portal-kde-5.17.4</a></td>
   <td align="right">61kB</td>
   <td><tt>58e6e61a95b324f80e5cf70c80841b87a73e613530d0ecfd6eb6446cbefc025e</tt></td>
</tr>

</table>
