<?php 
require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\ItemInterface;

$cache = new FilesystemAdapter();


$cdn = 'https://cdn.kde.org';
$cdnPathPrefix = 'aether-devel';

// carl dev config
//$cdn = 'https://kde.carlschwan.eu';
//$cdnPathPrefix = 'build';

$cdnManifest = $cdn . '/' . $cdnPathPrefix . '/version/manifest.json?cache-force=5';

$cdnCSSFiles = array_merge(['/version/bootstrap.css'], $pageConfig['cdnCSSFiles']);
$cdnJSFiles = array_merge(['/version/bootstrap.js'], $pageConfig['cdnJSFiles']);

//$cache->delete('cdnFiles' . str_replace('/', '', implode('', $cdnCSSFiles) . implode('', $cdnJSFiles)));
ini_set('realpath_cache_size', 0);

$cdnFiles = $cache->get('cdnFiles' . str_replace('/', '', implode('', $cdnCSSFiles) . implode('', $cdnJSFiles)), function (ItemInterface $item) use ($cdnManifest, $cdnPathPrefix, $cdnCSSFiles, $cdnJSFiles) {
    $item->expiresAfter(600);
    $fileContent = file_get_contents($cdnManifest);
    $manifestData = json_decode($fileContent, true);

    $convertPaths = function($cdnCSSFile) use ($cdnPathPrefix, $manifestData)  {
        return $manifestData[$cdnPathPrefix . $cdnCSSFile];
    };
    return [
        'css' => array_map($convertPaths, $cdnCSSFiles),
        'js' => array_map($convertPaths, $cdnJSFiles),
    ];
});

?>
<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">
  <head>
    <!--<meta http-equiv="Content-Security-Policy" content="default-src https: 'unsafe-inline' data: https://stats.kde.org https://cdn.kde.org; script-src: self https://stats.kde.org https://cdn.kde.org;form-action 'self' https://www.paypal.com/en_US/cgi-bin/webscr; frame-ancestors 'none'; object-src 'none'; base-uri 'none'; img-src 'self' data: https://krita.org https://kontact.kde.org https://www.kdevelop.org https://akademy.kde.org/ https://cdn.kde.org">-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?= htmlspecialchars($pageConfig['description']); ?>">
    <meta name="author" content="">
    <title><?= htmlspecialchars($pageConfig['title']); ?> - KDE.org</title>

    <!-- Facebook open graph configuration -->
    <meta property="og:title" content="<?= htmlspecialchars($pageConfig['title']); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://kde.org<?= htmlspecialchars($_SERVER['REQUEST_URI']); ?>" />
    <meta property="og:description" content="<?= htmlspecialchars($pageConfig['description']); ?>" />
    <meta property="og:site_name" content="KDE.org" />
    <?php if ($pageConfig['image'] !== "") { ?>
        <meta property="og:image" content="<?= htmlspecialchars($pageConfig['image']); ?>" />
    <?php } ?>

    <!-- Twitter card configuration -->
    <meta name="twitter:card" content="<?= $pageConfig['image'] === '/stuff/clipart/logo/kde-logo-white-blue-3000x3000.png' ? 'summary' : 'summary_large_image' ?>" />
    <meta name="twitter:site" content="@kdecommunity" />
    <meta name="twitter:title" content="<?= htmlspecialchars($pageConfig['title']); ?>" />
    <meta name="twitter:description" content="<?= htmlspecialchars($pageConfig['description']); ?>" />
    <meta name="twitter:image" content="<?= htmlspecialchars($pageConfig['image']); ?>" />

    <!-- schema.org metadata -->
    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "url": "https://kde.org<?= $_SERVER['REQUEST_URI'] ?>",
        "name": "KDE.org",
        "description": "<?= htmlspecialchars($pageConfig['description']); ?>"
    }
    </script>

	<!-- Icons -->
	<link rel="apple-touch-icon" href="/aether/media/180x180.png">
	<link rel="shortcut icon" href="/aether/media/192x192.png">

	<!-- CDN CSS -->
    <?php foreach ($cdnFiles['css'] as $cssFile) { ?>
        <link href="<?= $cdn . $cssFile ?>" rel="stylesheet" />
    <?php } ?>
	
    <!-- CDN JS -->
    <?php foreach ($cdnFiles['js'] as $jsFile) { ?>
        <script src="<?= $cdn . $jsFile ?>" defer></script>
    <?php } ?>

	<?php
        if (isset($pageConfig['cssFile'])) {
            if (is_array($pageConfig['cssFile'])) {
                foreach ($pageConfig['cssFile'] as $cssFile) {
                    echo ('<link href="'.$cssFile.'" rel="stylesheet" type="text/css" />');
                }

            }
            echo ('<link href="'.$pageConfig['cssFile'].'" rel="stylesheet" type="text/css" />');
        }
    
        if(@include_once ("functions.inc")) {
            setup_site();
            
            if (!isset($site_locale))
            {
                if (isset($_GET['site_locale']))
                {
                    $site_locale = htmlentities($_GET['site_locale']);
                    setcookie('site_locale', $site_locale, time()+31536000, '/', "." . $_SERVER['SERVER_NAME']);
                }
                else
                {
                    if (isset($_COOKIE['site_locale']))
                    {
                        $site_locale = htmlentities($_COOKIE['site_locale']);
                    }
                    else
                    {
                        if (!isset($default_site_locale))
                        {
                            $site_locale = "en";
                        }
                        else
                        {
                            $site_locale = $default_site_locale;
                        }
                    }
                }
            }

            // set php locale
            setlocale(LC_ALL, $site_locale);
    
            // start up our i18n stuff
            startTranslation($site_locale);
        }
    ?>

</head>
<body>
	<header id="KGlobalHeader" class="header clearfix">
		<nav class="navbar navbar-expand-sm" >
			<a aria-label="KDE Homepage" class="kde-logo navbar-brand<?php if ($pageConfig['section'] == "home") { echo ' active'; } ?>" href="/"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <!--
					<li class="nav-item">
						<a class="nav-link<?php if ($pageConfig['section'] == "news") { echo ' active'; } ?>" href="/news">News</a>
					</li>
					-->
					<li class="nav-item">
						<a class="nav-link<?php if ($pageConfig['section'] == "products") { echo ' active'; } ?>" href="/products/">Products</a>
					</li>
					<li class="nav-item">
						<a class="nav-link<?php if ($pageConfig['section'] == "develop") { echo ' active'; } ?>" href="/develop">Develop</a>
					</li>
					<li class="nav-item dropdown">
							<a class="nav-link<?php if ($pageConfig['section'] == "get-involved") { echo ' active'; } ?>" href="https://community.kde.org/Get_Involved">Get Involved</a>
					</li>
					<li class="nav-item">
						<a class="nav-link<?php if ($pageConfig['section'] == "donate") { echo ' active'; } ?>" href="/donations">Donate</a>
					</li>
				</ul>
			</div>
		</nav>
	</header>
	<div id="main">
<!-- better cookie warning set in footer by piwiki code
 <aside id="cookieWarning"><button id="removecookie">Dismiss</button>This website uses cookies to remember your settings, retain your sessions, and learn about our site usage. If your browser is set to &quot;Do not track&quot; mode we will not use statistics cookies.</aside>
-->
