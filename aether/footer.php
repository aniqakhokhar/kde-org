    </div>
    <footer id="kFooter" class="footer">
        <section id="kPatrons" class="container">
            <h3>KDE Patrons</h3>
            <div class="d-flex flex-wrap flex-md-nowrap justify-content-center">
                <span><img src="/aether/media/patrons/canonical.svg" alt="Logo of KDE Patron: Canonical" /></span>
                <span><img src="/aether/media/patrons/google.svg" alt="Logo of KDE Patron: Google" /></span>
                <span><img src="/aether/media/patrons/suse.svg" alt="Logo of KDE Patron: SUSE" /></span>
                <span><img src="/aether/media/patrons/qt-company.svg" alt="Logo of KDE Patron: The Qt Company" /></span>
                <span><img src="/aether/media/patrons/blue-systems.png" alt="Logo of KDE Patron: Blue System" /></span>
                <span><img src="/aether/media/patrons/enioka-haute-couture-logo.svg" alt="Logo of KDE Patron: enioka Haute Couture" /></span>
            </div>
        </section>
        
        
        <section id="kFooterIncome" class="container">
            <div id="kDonateForm">
                <div class="center">
                    <h3>Donate to KDE <a href="/community/donations/index.php#money" target="_blank">Why Donate?</a></h3>
                    <form 
                        action="https://www.paypal.com/en_US/cgi-bin/webscr" 
                        method="post" 
                        onsubmit="return amount.value >= 2 || window.confirm('Your donation is smaller than 2€. This means that most of your donation\nwill end up in processing fees. Do you want to continue?');">
                            <input type="hidden" name="cmd" value="_donations" />
                            <input type="hidden" name="lc" value="GB" />
                            <input type="hidden" name="item_name" value="Development and communication of KDE software" />
                            <input type="hidden" name="custom" value="<?= htmlspecialchars(
                                '//'.
                                $_SERVER['HTTP_HOST'].
                                $_SERVER['REQUEST_URI'].
                                '/donation_box');
                                ?>" />
                            <input type="hidden" name="currency_code" value="EUR" />
                            <input type="hidden" name="cbt" value="Return to kde.org" />
                            <input type="hidden" name="return" value="https://kde.org/community/donations/thanks_paypal" />
                            <input type="hidden" name="notify_url" value="https://kde.org/community/donations/notify.php" />
                            <input type="hidden" name="business" value="kde-ev-paypal@kde.org" />
                            <input aria-label="Donation value" type='text' name="amount" value="20.00" id="donateAmountField" /> €
                            <button type='submit' id="donateSubmit">Donate via PayPal</button>
                    </form>
                    
                    <a href="/community/donations/others" id="otherWaysDonate" target="_blank">Other ways to donate</a>
                </div>
            </div>
            <div id="kMetaStore">
                <div class="center">
                    <h3>Visit the KDE MetaStore</h3>
                    <p>Show others that you love KDE! Purchase books, mugs, and other branded goodies.</p>
                    <a href="/stuff/metastore" class="button">Click here to browse</a>
                </div>
            </div>
        </section>
        
        
        <div id="kLinks" class="container">
            <div class="row">
                <nav class="col-sm">
                    <h3>Products</h3>
                    <a href="/plasma-desktop">Plasma</a>
                    <a href="/applications/">KDE Applications</a>
                    <a href="/products/frameworks/">KDE Frameworks</a>
                    <a href="https://plasma-mobile.org/overview/">Plasma Mobile</a>
                    <a href="https://neon.kde.org/">KDE neon</a>
                    <a href="https://wikitolearn.org/" target="_blank" rel="noreferrer">WikiToLearn</a>
                </nav>
                
                <nav class="col-sm">
                    <h3>Develop</h3>
                    <a href="https://techbase.kde.org/">TechBase Wiki</a>
                    <a href="https://api.kde.org/">API Documentation</a>
                    <a href="https://doc.qt.io/" target="_blank" rel="noreferrer">Qt Documentation</a>
                    <a href="https://inqlude.org/" target="_blank" rel="noreferrer">Inqlude Documentation</a>
                    <a href="/goals">Goals</a>
                </nav>
                
                <nav class="col-sm">
                    <h3>News &amp; Press</h3>
                    <a href="/announcements/">Announcements</a>
                    <a href="https://dot.kde.org/">KDE.news</a>
                    <a href="https://planet.kde.org/">Planet KDE</a>
                    <a href="https://www.kde.org/screenshots">Screenshots</a>
                    <a href="https://www.kde.org/contact/">Press Contact</a>
                </nav>
                
                <nav class="col-sm">
                    <h3>Resources</h3>
                    <a href="https://community.kde.org/Main_Page">Community Wiki</a>
                    <a href="https://userbase.kde.org/">UserBase Wiki</a>
                    <a href="/stuff/">Miscellaneous Stuff</a>
                    <a href="/support/">Support</a>
                    <a href="/support/international.php">International Websites</a>
                    <a href="/download/">Download KDE Software</a>
                    <a href="/code-of-conduct/">Code of Conduct</a>
                    <a href="/privacypolicy">Privacy Policy</a>
                    <a href="/privacypolicy-apps">Applications Privacy Policy</a>
                </nav>
                
                <nav class="col-sm">
                    <h3>Destinations</h3>
                    <a href="https://store.kde.org/">KDE Store</a>
                    <a href="https://ev.kde.org/">KDE e.V.</a>
                    <a href="https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php">KDE Free Qt Foundation</a>
                    <a href="https://timeline.kde.org/">KDE Timeline</a>
                </nav>
            </div>
        </div>
    
        <div id="kSocial" class="container kSocialLinks">
            <!-- <nav style="position: absolute; left: 15px;">
                Browsing in English (Languages coming soon)
            </nav> -->
        
            <a class="shareFacebook" href="https://www.facebook.com/kde/" rel="nofollow">Post on Facebook</a>
            <a class="shareTwitter" href="https://twitter.com/kdecommunity" rel="nofollow">Share on Twitter</a>
            <a class="shareDiaspora" href="https://joindiaspora.com/people/9c3d1a454919ef06" rel="nofollow">Share on Diaspora</a>
            <a class="shareMastodon" href="https://mastodon.technology/@kde" rel="me nofollow">Share on Mastodon</a>
            <a class="shareLinkedIn" href="https://www.linkedin.com/company/29561/" rel="nofollow">Share on LinkedIn</a>
            <a class="shareReddit" href="https://www.reddit.com/r/kde/" rel="nofollow">Share on Reddit</a>
            <a class="shareYouTube" href="https://www.youtube.com/channel/UCF3I1gf7GcbmAb0mR6vxkZQ" rel="nofollow">Share on YouTube</a>
            <a class="sharePeerTube" href="https://peertube.mastodon.host/accounts/kde/videos" rel="nofollow">Share on PeerTube</a>
            <a class="shareVK" href="https://vk.com/kde_ru" rel="nofollow">Share on VK</a>
            <a class="shareInstagram" href="https://www.instagram.com/kdecommunity/" rel="nofollow">Share on Instagram</a>
        </div>
    
        <div id="kLegal" class="container">
            <div class="row">
                <small class="col-4">
                    Maintained by <a href="mailto:kde-webmaster@kde.org">KDE Webmasters</a>
                </small>
                <small class="col-8" style="text-align: right;">
                    KDE<sup>&#174;</sup> and <a href="/media/images/trademark_kde_gear_black_logo.png">the K Desktop Environment<sup>&#174;</sup> logo</a> are registered trademarks of <a href="https://ev.kde.org/" title="Homepage of the KDE non-profit Organization">KDE e.V.</a> |
                    <a href="https://www.kde.org/community/whatiskde/impressum">Legal</a>
                </small>
            </div>
        </div>
    </footer>
    <!--
    WARNING: DO NOT SEND MAIL TO THE FOLLOWING EMAIL ADDRESS! YOU WILL
    BE BLOCKED INSTANTLY AND PERMANENTLY!
    <?php
                    $trapmail = "aaaatrap-";
                    $t = pack('N', time());
                    for($i=0;$i<=3;$i++) {
                                                    $trapmail.=sprintf("%02x",ord(substr($t,$i,1)));
                    }
                    $ip=$_SERVER["REMOTE_ADDR"];
                    sscanf($ip,"%d.%d.%d.%d",$ip1,$ip2,$ip3,$ip4);
                    $trapmail.=sprintf("%02x%02x%02x%02x",$ip1,$ip2,$ip3,$ip4);

                    echo "<a href=\"mailto:$trapmail@kde.org\">Block me</a>\n";
    ?>
    WARNING END
    -->
<script>
  var _paq = window._paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(['disableCookies']);
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="https://stats.kde.org/";
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', '1']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
</body>
</html>
