<?php
	require('aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Explore hardware with Plasma by default",
        'cdnCSSFiles' => ['/version/kde-org/plasma-desktop.css'],
        'description' => "Explore hardware with Plasma by default",
	]);

	require('aether/header.php');

?>
<style>
.laptop-picture {
  width: 700px;
  max-width: 100%;
}
.blue {
  background-color: rgba(0, 115, 184, 0.1);
}
.green {
  background-color: rgba(53, 191, 92, 0.1);
}
.purple {
  background-color: rgba(41, 65, 114, 0.1);
}
</style>

<main>
  <div class="container">
    <h1>Buy hardware with Plasma and KDE Applications</h1>

    <p>Here you can find a list of computers with KDE Plasma pre-installed that you can buy right now:</p>
  </div>

  <section class="diagonal-box bg-blue pt-4 pb-5">
    <div class="container content">
      <div class="row">
        <div class="col-12 col-md-5 col-lg-6">
          <h2 id="kdeslimbookii">KDE Slimbook II</h2>
          <p>
            The Slimbook brand was born in 2015 in Valencia, Spain. The KDE Slimbook II is shiny,
            smart, and sleek, but it is not only good-looking: it is also powerful. The new i5
            and i7 microprocessors and DDR4 RAM make it much faster than its predecessor. Other
            improvements include a touchpad that "clicks" wherever you press it, and larger
            internal WiFi antennas that guarantee you will get a better reception wherever a WiFi
            network is available.
          </p>
        </div>
        <div class="col-12 col-md-7 col-lg-6 align-self-center mt-4">
          <img src="/content/hardware/slimbook.png" alt="KDE Slimbook II Picture" class="laptop-picture">
        </div>
      </div>
      <div>
        <h3>Specifications</h3>
        <p>
          <b>Operating system pre-installed:</b> <a href="https://neon.kde.org">KDE Neon</a><br />
          <b>CPU:</b> Intel i5: 2.5 GHz Turbo Boost 3.1 GHz - 3M Cache, or Intel i7: 2.7 GHz Turbo Boost 3.5 GHz - 4M Cache<br />
          <b>GPU:</b> Intel HD 620 4K<br />
          <b>Memory:</b> DDR4 4GB, 8GB or 16GB<br />
          <b>Display:</b> FullHD 1920x1080 pixels, 13.3'', Matte anti-glare<br />
          <b>Storage:</b> M.2 SSD of 120GB, 250GB, 500GB or 1TB At speeds between 500 MB/s and 2.200 MB/s<br />
          <b>Price:</b> starting at €649
        </p>
        <a href="https://kde.slimbook.es/" class="learn-more button mt-3 mb-3 d-inline-block">Learn more </a><br />
      </div>
    </div>
  </section>

  <section class="diagonal-box bg-green pt-4 pb-5">
    <div class="container content">
      <div class="row">
        <div class="col-12 col-md-5 col-lg-6">
          <h2 id="focus">Kubuntu Focus</h2>
          <p>
            The Kubuntu Focus laptop is a high-powered, workflow-focused laptop which ships with Kubuntu installed. This is the first officially Kubuntu-branded computer. It is designed for work that requires intense GPU computing, fast NVMe storage and optimal cooling to unleash the CPU's maximum potential.<br />
          </p>
        </div>
        <div class="col-12 col-md-7 col-lg-6 align-self-center mt-4">
          <img src="/content/hardware/focus.png" alt="Kubuntu Focus Picture" class="laptop-picture ">
        </div>
      </div>
      <div>
        <h3>Specifications</h3>
        <p>
          <b>Operating system pre-installed:</b> <a href="https://kubuntu.org">Kubuntu</a><br />
          <b>CPU:</b> Core i7-9750H 6c/12t 4.5GHz Turbo<br />
          <b>GPU:</b> NVIDIA GeForce RTX 2060 6 GB GDDR6 with PhysX and CUDA<br />
          <b>Memory</b> 32GB Dual Channel DDR4 2666 MHz<br />
          <b>Display</b> Full HD 16.1” matte 1080p IPS 144Hz<br />
          <b>Storage</b> 1TB Samsung EVO Plus NVMe 3,500MB/s and 2,700MB/s seq. read and write<br />
          <b>Price:</b> starting at $1,795
        </p>
        <a href="https://kfocus.org/" class="learn-more button mt-3 mb-3 d-inline-block">Learn more </a><br />
      </div>
    </div>
  </section>
  
  <section class="diagonal-box bg-purple pt-4 pb-5">
    <div class="container content">
      <div class="row">
        <div class="col-12 col-md-5 col-lg-6">
          <h2 id="pinebook">Pinebook Pro</h2>

          <p>
           The Pinebook Pro is meant to deliver solid day-to-day Linux or *BSD experience and to be a compelling alternative to mid-ranged Chromebooks that people convert into Linux laptops. In contrast to most mid-ranged Chromebooks however, the Pinebook Pro comes with an IPS 1080p 14″ LCD panel, a premium magnesium alloy shell, 64/128GB of eMMC storage, a 10,000 mAh capacity battery and the modularity / hackability that only an open source project can deliver – such as the unpopulated PCIe m.2 NVMe slot. The USB-C port on the Pinebook Pro, apart from being able to transmit data and charge the unit, is also capable of digital video output up-to 4K at 60hz.<br />
          </p>
        </div>
        <div class="col-12 col-md-7 col-lg-6 align-self-center mt-4">
          <img src="/content/hardware/pinebook-pro.png" alt="Pinebook Pro picture" class="laptop-picture">
        </div>
      </div>
      <div>
        <h3>Specifications</h3>
        <p>
          <b>Operating system pre-installed:</b> <a href="https://manjaro.org">Manjaro KDE</a><br />
          <b>CPU:</b> Rockchip RK3399 SOC<br />
          <b>GPU:</b> Mali T860 MP4<br />
          <b>Memory:</b> 4GB LPDDR4 <br />
          <b>Display:</b> 1080p IPS Panel<br />
          <b>Storage:</b> 64GB of eMMC (Upgradable) <br />
          <b>Price:</b> starting at $199
        </p>
          <a href="https://www.pine64.org/pinebook-pro/" class="learn-more button mt-3 mb-3 d-inline-block">Learn more </a><br />
      </div>
    </div>
  </section>

  <div class="container py-5">
    <h1>Hardware sellers with KDE Plasma as an option</h1>

    <p>You can find here a list of hardware sellers who offer Plasma as an option when buying the laptop:</p>
  </div>
  
  <section class="diagonal-box bg-blue pt-4 pb-5">
    <div class="container content">
      <h2 id="slimbook">Slimbook</h2>

      <p>
      SLIMBOOK was born in 2015 with the idea of ​​being the best brand in the computer market with GNU / Linux (although it is also verified the absolute compatibility with Microsoft Windows). They assemble computers searching for excellent quality and warranty service, at a reasonable price. So much so, that in 2018 they were awarded Best Open Source Service / Solution Provider at the OpenExpo Europe 2018.
      </p>

      <a href="https://slimbook.es/en" class="learn-more button mt-3 mb-3 d-inline-block">Learn more </a><br />
      <img src="/content/hardware/slimbook.jpg" alt="Slimbook homepage picture" class="laptop-picture">
    </div>
  </section>

  <section class="diagonal-box bg-yellow pt-4 pb-5">
    <div class="container content">
      <div class="row">
        <div class="col-12 col-md-5 col-lg-6">
          <h2 id="starlabs">Star Labs</h2>
          <p>
            Star Labs offers a range of laptops designed and built specifically for Linux.
            Two version of their laptop are offered: the Star Lite Mk III 11-inch and the 
            Star LabTop Mk IV 13-inch.
          </p>
        </div>
        <div class="col-12 col-md-7 col-lg-6 align-self-center mt-4">
          <img src="/content/hardware/starlabs.png" alt="Star Labs laptop with Plasma" class="laptop-picture">
        </div>
      </div>
      <div>
        <a href="https://starlabs.systems/" class="learn-more button mt-3 mb-3 d-inline-block">Learn more </a><br />
      </div>
    </div>
  </section>
  
  <section class="diagonal-box bg-green pt-4 pb-5">
    <div class="container content">
      <h2 id="tuxedo">Tuxedo</h2>

      <p>
      Tuxedo builds tailor-made hardware and all this with Linux!<br />

      TUXEDO Computers are individually built computers/PCs and notebooks which are fully Linux compatible, i. e. Linux hardware in tailor-made suit.
      All TUXEDO devices are delivered in such a way that you only have to unpack, connect and switch them on. All the computers and notebooks are assembled and installed in-house, not outsourced.<br />
      They provide you with self-programmed driver packages, support, installation scripts and everything around our hardware, so that every hardware component really works.
      </p>

      <a href="https://www.tuxedocomputers.com/en#" class="learn-more button mt-3 mb-3 d-inline-block">Learn more </a><br />
      <img src="/content/hardware/Tuxedo.png" alt="Tuxedo homepage picture" class="laptop-picture">
    </div>
  </section>
  
  <section class="diagonal-box bg-purple pt-4 pb-5">
    <div class="container content">
      <h2 id="zareason">ZaReason</h2>

      <p>
      More than a decade ago, in 2006, ZaReason built their first desktop as part of a volunteer project. In 2007, ZaReason opened for business and their first build went to a PhD Chemistry student at UC Berkeley. Everything grew from there. 
      </p>

      <a href="https://zareason.com/" class="learn-more button mt-3 mb-3 d-inline-block">Learn more </a><br />
      <img src="/content/hardware/zareason.jpg" alt="ZaReason homepage picture" class="laptop-picture">
    </div>
  </section>
</main>

<?php require('aether/footer.php'); ?>

