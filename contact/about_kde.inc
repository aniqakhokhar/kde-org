<?php 
    /*
    This file is for use in announcements. It contains the usual blurb "About KDE",
    along with trademark notices.
    */
?>

<h2>
  <?php i18n("About KDE");?>
</h2>
<p align="justify">
<?php i18n("KDE is an international technology team that creates free 
and open source software for desktop and portable computing. Among 
KDE's products are a modern desktop system for Linux and UNIX platforms, 
comprehensive office productivity and groupware suites and hundreds of 
software titles in many categories including Internet and web 
applications, multimedia, entertainment, educational, graphics and 
software development. KDE software is translated into more than 60 
languages and is built with ease of use and modern accessibility 
principles in mind. KDE's full-featured applications run natively on
Linux, BSD, Solaris, Windows and macOS.");?>
</p>

<hr />

<p align="justify">
  <font size="2">
  <em><?php i18n("Trademark Notices.");?></em>
  <?php i18n("KDE<sup>&#174;</sup> and the K Desktop Environment<sup>&#174;</sup> logo are 
  registered trademarks of KDE e.V.");?>

  <?php i18n("Linux is a registered trademark of Linus Torvalds.");?>

  <?php i18n("UNIX is a registered trademark of The Open Group in the United States and
  other countries.");?>

  <?php i18n("All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.");?>
  </font>
</p>

<hr />
