<?php
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "MetaStore - Buy KDE merchandising, books and more",
        'description' => 'Here you can find links to stores that provide KDE products and merchandising.',
    ]);

    require('../aether/header.php');
    $site_root = "../";
?>

<main class="container">

<h1>MetaStore - Buy KDE merchandising, books and more</h1>

<p>You can show your love for KDE in many ways. Here are a few places where you can buy t-shirts, books, mugs and more.</p>



<h2>KDE Slimbook - Laptop</h2>

<p>
Link: <a href="https://kde.slimbook.es/">https://kde.slimbook.es/</a>
</p>

<p>
<img width="156" height="100" src="https://kde.slimbook.es/assets/img/duo-ok.png">
</p>



<h2>KDE e.V. Cafepress Store - T-shirts, pins, laptop sleeves and more!</h2>

<p>
Link: <a href="https://www.cafepress.com/kde">https://www.cafepress.com/kde</a>
</p>

<p>
<img width="100" height="100" src="https://i3.cpcache.com/merchandise/118_350x350_Front_Color-NA.png?region={%22id%22:105764161,%22name%22:%22FrontCenter%22,%22width%22:3.0,%22height%22:3.0,%22orientation%22:0,%22crop_x%22:0.0,%22crop_y%22:0.0,%22crop_w%22:450.0,%22crop_h%22:450.0,%22scale%22:1.0,%22dpi%22:150,%22template%22:{%22id%22:105764161,%22params%22:{}}}">
<img width="100" height="100" src="https://i3.cpcache.com/merchandise/1241_350x350_Front_Color-White.png?region={%22id%22:100466231,%22name%22:%22FrontCenter%22,%22width%22:15.9981813,%22height%22:12.57,%22orientation%22:0,%22crop_x%22:0.0,%22crop_y%22:0.0,%22crop_w%22:4200.0,%22crop_h%22:3300.0,%22scale%22:1.0,%22dpi%22:300,%22template%22:{%22id%22:100466231,%22params%22:{}}}">
<img width="100" height="100" src="https://i3.cpcache.com/merchandise/111_350x350_Front_Color-Natural.png?region={%22id%22:104695369,%22name%22:%22FrontCenter%22,%22width%22:10.0,%22height%22:10.0,%22orientation%22:0,%22crop_x%22:0.0,%22crop_y%22:0.0,%22crop_w%22:1000.0,%22crop_h%22:1000.0,%22scale%22:1.0,%22dpi%22:100,%22template%22:{%22id%22:104695369,%22params%22:{}}}">
<img width="100" height="100" src="https://i3.cpcache.com/merchandise/100_350x350_Front_Color-NA.png?region={%22id%22:105706193,%22name%22:%22FrontCenter%22,%22width%22:5.5,%22height%22:8.5,%22orientation%22:0,%22crop_x%22:0.0,%22crop_y%22:0.0,%22crop_w%22:825.0,%22crop_h%22:1275.0,%22scale%22:1.0,%22dpi%22:150,%22template%22:{%22id%22:105706193,%22params%22:{}}}">
</p>



<h2>FreeWear.org - T-shirts, caps, mugs and more!</h2>

<p>
Link: <a href="https://www.freewear.org/?org=KDE">https://www.freewear.org/?org=KDE</a>
</p>

<p>
<img width="98" height="100" src="https://www.freewear.org/images/articles/detail/FW0235.png">
<img width="98" height="100" src="https://www.freewear.org/images/articles/detail/FW0234.png">
<img width="98" height="100" src="https://www.freewear.org/images/articles/detail/FW0211.png">
<img width="88" height="100" src="https://www.freewear.org/images/articles/detail/FW0123.png">
</p>



<h2>HELLOTUX - T-shirts, polos and sweatshirts</h2>
<p>
Link: <a href="https://www.hellotux.com/kde">https://www.hellotux.com/kde</a>
</p>

<p>
<img width="133" height="100" src="https://www.hellotux.com/productimages/kde_polo_shirt_black_index.JPG">
<img width="133" height="100" src="https://www.hellotux.com/productimages/kde_womens_tshirt_black_index.JPG">
<img width="133" height="100" src="https://www.hellotux.com/productimages/kde_hoodie_blackgray_index.JPG">
</p>



<h2>Book: 20 Years of KDE: Past, Present and Future</h2>
<p>
Link: <a href="https://20years.kde.org/book/">https://20years.kde.org/book/</a>
</p>

<p>
<img width="71" height="100" src="https://20years.kde.org/book/assets/img/frontcover.png">
</p>

</main>
<?php
  require('../aether/footer.php');
