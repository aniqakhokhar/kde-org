<?php
	require('aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma",
		'section' => 'products',
        'cdnCSSFiles' => ['/version/kde-org/plasma-desktop.css'],
		'description' => "Plasma is KDE's desktop environment. Simple by default, powerful when needed.",
        'image' => 'https://kde.org/announcements/plasma-5.18/customize-layout.png',
	]);

	require('aether/header.php');

?>
<main>
<section class="container">
    <div class="text-center">
        <h1>Plasma is a Desktop</h1>
        <img src="/images/plasma.svg" width="64" height="64" class="mt-3 mb-3" alt="Logo of Plasma desktop">
        <p class="ml-auto mr-auto">Use Plasma to surf the web; keep in touch with colleagues, friends and family; manage your files, enjoy music and videos; and get creative and productive at work. Do it all in a beautiful environment that adapts to your needs, and with the safety, privacy-protection and peace of mind that the best Free Open Source Software has to offer.</p>
        <p>
          <a href="/distributions" class="button">Get Plasma for your device</a>
          <br class="d-md-none">
          or
          <br class="d-md-none">
          <a href="/hardware" class="button">Buy a device with Plasma</a>
        </p>
    </div>
    <div class="embed-responsive embed-responsive-16by9 mt-3 mb-3 ml-auto mr-auto" style="max-width: 800px;">
      <video controls="controls" src="https://cdn.kde.org/promo/Announcements/Plasma/5.19/Video.mp4"></video>
    </div>
</section>

<section id="plasma-last-release">
    <div class="breeze-window">
        <div class="kwin-shadow">
            <div class="headerbar">
                About Plasma 5.19
                <img alt="" src="/content/plasma-desktop/window-minimize.svg" class="window-minimize" />
                <img alt="" src="/content/plasma-desktop/window-maximize.svg" class="window-maximize" />
                <img alt="" src="/content/plasma-desktop/window-close.svg" class="window-close" />
            </div>
            <div class="view">
                <h2>Latest Release: Plasma 5.19</h2>
                <p>Featuring an emoji picker, a global edit mode, user feedback and much more.</p>
                <a href="/announcements/plasma-5.19.0" class="breeze-button">See Announcements</a><br />
            </div>
        </div>
    </div>
</section>

<section id="simple-by-default">
    <div class="container">
        <h2>Simple by default</h2>
        <p>Plasma is designed to be easy to use.</p>
        <ul class="nav justify-content-center nav-features" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" role="tab" href="#simple-by-default-launcher" data-toggle="tab" aria-selected="true" aria-controls="simple-by-default-launcher" id="simple-by-default-launcher-control">Launcher</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" role="tab" href="#simple-by-default-tray" data-toggle="tab" aria-selected="false" aria-controls="simple-by-default-tray" id="simple-by-default-tray-control">System Tray</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" role="tab" href="#simple-by-default-notifications" data-toggle="tab" aria-selected="false" aria-controls="simple-by-default-notifications" id="simple-by-default-notifications-control">Notifications</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" role="tab" href="#simple-by-default-discover" data-toggle="tab" aria-selected="false" aria-controls="simple-by-default-discover" id="simple-by-default-discover-control">Discover</a>
            </li>
        </ul>
    </div>
    <div class="tab-content tab-content-feature pl-4 pr-4">
        <div class="tab-pane active" id="simple-by-default-launcher" role="tabpanel" aria-labelledby="simple-by-default-launcher-control">
            <div class="laptop-with-overlay">
                <img class="laptop img-fluid mb-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
                <div class="laptop-overlay">
                    <img class="img-fluid mb-3" loading="lazy" src="/content/plasma-desktop/plasma-launcher.png" alt="Screenshot Plasma with the launcher open" />
                </div>
            </div>
            <div class="container">
                <p>
                    The Plasma Launcher lets you quickly and easily launch applications, but it can do much more -- convenient tasks like bookmarking applications, searching for documents as you type, or navigating to common places help you getting straight to the point. With a history of recently started programs and opened files, you can return to where you left off. It even remembers previously entered search terms so you don't have to.
                </p>
            </div>
        </div>
        <div class="tab-pane" id="simple-by-default-tray" role="tabpanel" aria-labelledby="simple-by-default-tray-control">
            <div class="laptop-with-overlay bottom-right">
                <img class="laptop img-fluid mb-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
                <div class="laptop-overlay">
                    <img class="img-fluid mb-3" loading="lazy" src="/content/plasma-desktop/plasma-systemtray.png" alt="Screenshot Plasma with the system tray open" />
                </div>
            </div>
            <div class="container">
                <p>Connect to your WiFi network; change the volume; switch to the next song or pause a video; access an external device; change the screen layout. All these and a lot more--directly from the system tray.</p>
                <p>To conserve the focus on what you're currently doing, any icon can be hidden if you like. Inactive icons hide themselves unless you tell them to stay where they are.</p>
            </div>
        </div>
        <div class="tab-pane" id="simple-by-default-notifications" role="tabpanel" aria-labelledby="simple-by-default-notifications-control">
            <div class="laptop-with-overlay bottom-right">
                <img class="laptop img-fluid mb-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
                <div class="laptop-overlay">
                    <img class="img-fluid mb-3" loading="lazy" src="/content/plasma-desktop/plasma-notifications.png" alt="Screenshot Plasma with a notification" />
                </div>
            </div>
            <div class="container">
                <p>See active tasks and recent actions; read new e-mails; quickly reply to messages; see track changes or low battery notice; check for updates; interact with recently moved files or screenshots. Or enter "Do Not Disturb" mode to concentrate on your work.</p>
            </div>
        </div>
        <div class="tab-pane video" id="simple-by-default-discover" role="tabpanel" aria-labelledby="simple-by-default-discover-control">
            <div class="laptop-with-overlay">
                <img class="laptop img-fluid mb-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
                <div class="laptop-overlay">
                    <video class="img-fluid mb-3">
                        <source src="/content/plasma-desktop/discover_1280x900_1500k.webm" />
                    </video>
                </div>
            </div>
            <div class="container">
                <div class="text-center">
                    <button class="button d-none">Replay</button><br />
                </div>
                <p>
                    Discover lets you manage the applications installed in your computer. Updating, removing or installing an application is only one click away. Supports <a href="https://flatpak.org/">Flatpak</a>, <a href="https://snapcraft.io/store">Snaps</a> and the applications available in your Linux distribution.
                </p>
            </div>
        </div>
    </div>
</section>

<section id="powerful-when-needed">
    <div class="container">
        <h2>Powerful when needed</h2>
        <p>Plasma is made to stay out of the way as it helps you get things done. But under its light and intuitive surface, it's a powerhouse. So you're free to choose ways of usage right as you need them and when you need them.</p>
        <ul class="nav justify-content-center nav-features" role="tablist">
            <li class="nav-item">
                <a role="tab" class="nav-link active" href="#powerful-when-needed-krunner" id="powerful-when-needed-control-krunner" data-toggle="tab" aria-selected="true" aria-controls="powerful-when-needed-krunner">KRunner</a>
            </li>
            <li class="nav-item">
                <a role="tab" class="nav-link" href="#powerful-when-needed-systemsettings" id="powerful-when-needed-control-systemsettings" data-toggle="tab" aria-selected="false" aria-controls="powerful-when-needed-systemsettings">System Setting</a>
            </li>
            <li class="nav-item">
                <a role="tab" class="nav-link" href="#powerful-when-needed-apps" id="powerful-when-needed-control-apps" data-toggle="tab" aria-selected="false" aria-controls="powerful-when-needed-apps">Professional Apps</a>
            </li>
        </ul>
    </div>
    <div class="tab-content tab-content-feature pl-4 pr-4">
        <div class="tab-pane show active" id="powerful-when-needed-krunner" role="tabpanel" aria-labelledby="powerful-when-needed-control-krunner">
            <div class="laptop-with-overlay top-center mb-2">
                <img class="laptop img-fluid mb-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
                <div class="laptop-overlay">
                    <img class="img-fluid mb-3" loading="lazy" src="/content/plasma-desktop/krunner.png" alt="Screenshot Plasma with KRunner" />
                </div>
            </div>
            <div class="container">
                <p>
                    KRunner is the launcher built into the Plasma desktop. While its basic function is to launch programs from a sort of mini-command-line, its functionality can be extended by "runners" to assist the user to accomplish a lot of tasks. From opening folders and files to calculations and currency conversions or even controlling your music player application - there are so many things right at your fingertips.
                </p>
            </div>
        </div>
        <div class="tab-pane" id="powerful-when-needed-systemsettings" role="tabpanel" aria-labelledby="powerful-when-needed-control-systemsetting">
            <div class="laptop-with-overlay">
                <img class="laptop img-fluid mb-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
                <div class="laptop-overlay">
                    <img class="img-fluid mb-3" loading="lazy" src="/content/plasma-desktop/systemsettings.png" alt="Screenshot: Plasma system setting" />
                </div>
            </div>
            <div class="container">
                <p>
                    Your Plasma desktop is very flexible and can be configured just how you like it using the System Settings app. Easily manage hardware, software, and workspaces all in one place: Keyboard, Printer, Languages, Desktop Themes, Fonts, Networks...
                </p>
            </div>
        </div>
        <div class="tab-pane" id="powerful-when-needed-apps" role="tabpanel" aria-labelledby="powerful-when-needed-control-apps">
            <div class="laptop-with-overlay">
                <img class="laptop img-fluid mb-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
                <div class="laptop-overlay">
                    <img class="img-fluid mb-3" loading="lazy" src="/content/plasma-desktop/kdenlive-plasma.png" alt="Screenshot: Plasma system setting" />
                </div>
            </div>
            <div class="container">
                <p>
                    <a href="https://krita.org">Krita</a> is an application used by many artists that gives to everyone the opportunity to draw concept art, textures, matte paintings, illustrations and comics. <a href="https://kdenlive.org">Kdenlive</a> is a professional tool that enables everyone to do non-linear video editing. <a href="https://www.digikam.org">DigiKam</a> is an advanced open-source digital photo management application that provides a comprehensive set of tools for importing, managing, editing, and sharing photos and raw files.
                </p>
            </div>
        </div>
    </div>
</section>

<section id="plasma-configuration">
    <div class="container">
        <h2>Customizable</h2>
        <p>With Plasma the user is king. Not happy with the color scheme? Change it! Want to have your panel on the left edge of the screen? Move it! Don't like the font? Use a different one! Download custom widgets in one click and add them to your desktop or panel.</p>
        <ul class="nav justify-content-center nav-features" role="tablist">
            <li class="nav-item">
                <a role="tab" class="nav-link active" href="#plasma-configuration-dark" id="plasma-configuration-dark-control" data-toggle="tab" aria-selected="true" aria-controls="plasma-configuration-dark">Dark Theme</a>
            </li>
            <li class="nav-item">
                <a role="tab" class="nav-link" href="#plasma-configuration-widgets" id="plasma-configuration-widgets-control" data-toggle="tab" aria-selected="false" aria-controls="plasma-configuration-widgets">Widgets</a>
            </li>
            <li class="nav-item">
                <a role="tab" class="nav-link" href="#plasma-configuration-panels" id="plasma-configuration-panels-control" data-toggle="tab" aria-selected="false" aria-controls="plasma-configuration-panels">Panels</a>
            </li>
            <li class="nav-item">
                <a role="tab" class="nav-link" href="#plasma-configuration-community" id="plasma-configuration-community-control" data-toggle="tab" aria-selected="false" aria-controls="plasma-configuration-community">Community Extensions</a>
            </li>
        </ul>
    </div>
    <div class="tab-content tab-content-feature pl-4 pr-4">
        <div class="tab-pane show active" id="plasma-configuration-dark" role="tabpanel" aria-labelledby="plasma-configuration-dark-control">
            <div class="laptop-with-overlay">
                <img class="laptop img-fluid mb-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
                <div class="laptop-overlay">
                    <img class="img-fluid mb-3" loading="lazy" src="/content/plasma-desktop/theme.png" alt="Plasma and Kontact with dark theme enabled" />
                </div>
            </div>
            <div class="container">
                <p>
                    Give your eyes some rest by switching Plasma and its applications to a built-in dark theme.
                </p>
            </div>
        </div>
        <div class="tab-pane" id="plasma-configuration-widgets" role="tabpanel" aria-labelledby="plasma-configuration-widgets-control">
            <div class="laptop-with-overlay">
                <img class="laptop img-fluid mb-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
                <div class="laptop-overlay">
                    <img class="img-fluid mb-3" loading="lazy" src="/content/plasma-desktop/plasma-widgets.png" />
                </div>
            </div>
            <div class="container">
                <p>
                    Add widgets to your desktop or panels; move, rotate or resize them; download custom widgets made by the community in one click.
                </p>
            </div>
        </div>
        <div class="tab-pane" id="plasma-configuration-panels" role="tabpanel" aria-labelledby="plasma-configuration-panels-control">
            <div class="laptop-with-overlay">
                <img class="laptop img-fluid mb-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
                <div class="laptop-overlay">
                    <img class="img-fluid mb-3" loading="lazy" src="/content/plasma-desktop/plasma-panels.png" />
                </div>
            </div>
            <div class="container">
                <p>
                    Take complete control over your panels by moving and resizing them, adding new ones, changing the position of widgets within a panel or even adding new widgets to them.
                </p>
            </div>
        </div>
        <div class="tab-pane" id="plasma-configuration-community" role="tabpanel" aria-labelledby="plasma-configuration-community-control">
            <div class="laptop-with-overlay">
                <img class="laptop img-fluid mb-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
                <div class="laptop-overlay">
                    <img class="img-fluid mb-3" loading="lazy" src="/content/plasma-desktop/plasma-latte.png" />
                </div>
            </div>
            <div class="container">
                <p>
                    KDE has a vast community of people who create application themes, color schemes, widgets, and extensions -- all for free! You can browse their creations in the <a href="https://store.kde.org">KDE Store</a> or download them directly from the relevant page in System Settings.
                </p>
            </div>
        </div>
    </div>
</section>

<section id="kde-connect">
    <div class="container">
        <h2>KDE Connect</h2>
        <p>Send your vacation pictures; see what music is currently playing on  your computer; move your mouse cursor during a presentation; poweroff your computer remotly before going to bed. All these from your Android device.</p>
        <ul class="nav justify-content-center nav-features" role="tablist">
            <li class="nav-item">
                <a role="tab" class="nav-link active" href="#kde-connect-media-sync" id="kde-connect-media-sync-control" data-toggle="tab" aria-selected="true" aria-controls="kde-connect-media-sync">Media Sync</a>
            </li>
            <li class="nav-item">
                <a role="tab" class="nav-link" href="#kde-connect-send-files" id="kde-connect-send-files-control" data-toggle="tab" aria-selected="false" aria-controls="kde-connect-send-files">Send Files</a>
            </li>
            <li class="nav-item">
                <a role="tab" class="nav-link" href="#kde-connect-notifications" id="kde-connect-notifications-control" data-toggle="tab" aria-selected="false" aria-controls="kde-connect-notifications">Notifications</a>
            </li>
        </ul>
        <div class="tab-content tab-content-feature pl-4 pr-4">
            <div class="tab-pane show active" id="kde-connect-media-sync" role="tabpanel" aria-labelledby="kde-connect-media-sync-control">
                <div class="plasma-and-android row align-items-center">
                    <div class="col-4 col-md-3">
                        <div class="phone-with-overlay">
                            <img class="phone img-fluid mb-3" src="/content/plasma-desktop/phone.png" alt="">
                            <div class="phone-overlay">
                                <img class="img-fluid mb-3" loading="lazy" src="/content/plasma-desktop/kde-connect-android-media.jpg" alt="KDE Connect menu" />
                            </div>
                        </div>
                    </div>
                    <div class="col-8 col-md-9">
                        <div class="laptop-with-overlay">
                            <img class="laptop img-fluid mb-3" src="/content/plasma-desktop/laptop.png" alt="">
                            <div class="laptop-overlay">
                                <img class="img-fluid mb-3" loading="lazy" src="/content/plasma-desktop/kde-connect-desktop-media.png" alt="Plasma desktop showing a music player open" />
                            </div>
                        </div>
                    </div>
                </div>
                <p>See what's playing on your computer, be it music, videos or youtube; Pause, play or skip to the next track all from your phone.</p>
            </div>
            <div class="tab-pane two-videos" id="kde-connect-send-files" role="tabpanel" aria-labelledby="kde-connect-send-files-control">
                <div class="plasma-and-android row align-items-center">
                    <div class="col-4 col-md-3">
                        <div class="phone-with-overlay">
                            <img class="phone img-fluid mb-3" src="/content/plasma-desktop/phone.png" alt="">
                            <div class="phone-overlay">
                                <video class="img-fluid mb-3">
                                    <source src="/content/plasma-desktop/kde-connect-android-send-files.webm" />
                                </video>
                            </div>
                        </div>
                    </div>
                    <div class="col-8 col-md-9">
                        <div class="laptop-with-overlay">
                            <img class="laptop img-fluid mb-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
                            <div class="laptop-overlay">
                                <video class="img-fluid mb-3">
                                    <source src="/content/plasma-desktop/kde-connect-laptop-send-files.webm" />
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="text-center">
                        <button class="button d-none">Replay</button><br />
                    </div>
                    <p>Share files from your Android device by simply tapping on the share button and selecting KDE Connect.</p>
                </div>
            </div>
            <div class="tab-pane" id="kde-connect-notifications" role="tabpanel" aria-labelledby="kde-connect-notifications-control">
                <div class="plasma-and-android row align-items-center">
                    <div class="col-4 col-md-3">
                        <div class="phone-with-overlay">
                            <img class="phone img-fluid mb-3" src="/content/plasma-desktop/phone.png" alt="empty phone with an overlay">
                            <div class="phone-overlay">
                                <img class="img-fluid mb-3" loading="lazy" src="/content/plasma-desktop/kde-connect-android-notification.png" alt="KDE Connect notification on android" />
                            </div>
                        </div>
                    </div>
                    <div class="col-8 col-md-9">
                        <div class="laptop-with-overlay">
                            <img class="laptop img-fluid mb-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
                            <div class="laptop-overlay">
                                <img class="img-fluid mb-3" loading="lazy" src="/content/plasma-desktop/kde-connect-laptop-notification.png" alt="KDE Connect notification on desktop"/>
                            </div>
                        </div>
                    </div>
                </div>
                <p>Synchronize notification across your devices and quickly answer to messages from your computer.</p>
            </div>
        </div>
        <h3>Download the android extension</h3>
        <div>
            <a href="https://f-droid.org/en/packages/org.kde.kdeconnect_tp/"><img src="content/plasma-desktop/Get_it_on_F-Droid.svg" alt="Get it on F-Droid" class="download-button-img" /></a>
            <a href='https://play.google.com/store/apps/details?id=org.kde.kdeconnect_tp'><img alt='Get it on Google Play' src='/applications/assets/get-it-from-google-play.png' class="download-button-img playstore" /></a>
        </div>
    </div>
</section>

<section class="container">
    <small>Google Play and the Google Play logo are trademarks of Google LLC.</small>
</section>

<!--
<section id="plasma-browser-integration">
    <div class="container">
        <h2>Browser Integration</h2>
        <p>The time spend in our browser is always increasing, watching cat videos, communicating with our friends</p>
        <ul class="nav justify-content-center nav-features" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="#p-b-i-video" id="plasma-privacy-vaults-control" data-toggle="tab" aria-selected="true" aria-controls="p-b-i-video">Video Control</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#plasma-privacy-microphone" id="plasma-privacy-microphone-control" data-toggle="tab" aria-selected="false" aria-controls="plasma-privacy-microphone">Find Your Tabs</a>
            </li>
        </ul>
    </div>
    <div class="tab-content tab-content-feature pl-4 pr-4">
        <div class="tab-pane show active" id="plasma-privacy-vaults" role="tabpanel" aria-labelledby="#plasma-privacy-vaults-control">
            <div class="laptop-with-overlay">
                <img class="laptop img-fluid mb-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
                <div class="laptop-overlay">
                    <img class="img-fluid mb-3" src="/content/plasma-desktop/theme.png" />
                </div>
            </div>
            <div class="container">
                Ensure your privacy is protected by using a graphical interface to encrypt your data. Create, lock, and unlock vaults directly from the System Tray.
            </div>
        </div>
        <div class="tab-pane" id="plasma-privacy-microphone" role="tabpanel" aria-labelledby="#plasma-privacy-microphone-control">
            <div class="laptop-with-overlay">
                <img class="laptop img-fluid mb-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
                <div class="laptop-overlay">
                    <img class="img-fluid mb-3" src="/content/plasma-desktop/theme.png" />
                </div>
            </div>
            <div class="container">
                Make sure nobody is listening to you: a microphone icon will appear in the System Tray whenever an app is able to record audio.
            </div>
        </div>
        <h3>Download the browser extension</h3>
        <div>
            <a href="https://f-droid.org/en/packages/org.kde.kdeconnect_tp/"><img src="content/plasma-desktop/Get_it_on_F-Droid.svg" alt="Get it on F-Droid" class="download-button-img" /></a>
            <a href='https://play.google.com/store/apps/details?id=org.kde.kdeconnect_tp'><img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png' class="download-button-img playstore" /></a>
        </div>
    </div>
</section>

<section id="plasma-privacy">
    <div class="container">
        <h2>Privacy</h2>
        <p>Privacy is one of KDE's major values. We believe that your data is private and belongs to you, and we take action to make sure that it stays that way!</p>
        <ul class="nav justify-content-center nav-features" role="tablist">
            <li class="nav-item">
                <a role="tab" class="nav-link active" href="#plasma-privacy-vaults" id="plasma-privacy-vaults-control" data-toggle="tab" aria-selected="true" aria-controls="Vault">Vault</a>
            </li>
            <li class="nav-item">
                <a role="tab" class="nav-link" href="#plasma-privacy-microphone" id="plasma-privacy-microphone-control" data-toggle="tab" aria-selected="false" aria-controls="Microphone">Microphone</a>
            </li>
        </ul>
    </div>
    <div class="tab-content tab-content-feature pl-4 pr-4">
        <div class="tab-pane show active" id="plasma-privacy-vaults" role="tabpanel" aria-labelledby="#plasma-privacy-vaults-control">
            <div class="laptop-with-overlay">
                <img class="laptop img-fluid mb-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
                <div class="laptop-overlay">
                    <img class="img-fluid mb-3" src="/content/plasma-desktop/theme.png" />
                </div>
            </div>
            <div class="container">
                <p>
                    Ensure your privacy is protected by using a graphical interface to encrypt your data. Create, lock, and unlock vaults directly from the System Tray.
                </p>
            </div>
        </div>
        <div class="tab-pane" id="plasma-privacy-microphone" role="tabpanel" aria-labelledby="#plasma-privacy-microphone-control">
            <div class="laptop-with-overlay">
                <img class="laptop img-fluid mb-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
                <div class="laptop-overlay">
                    <img class="img-fluid mb-3" src="/content/plasma-desktop/theme.png" />
                </div>
            </div>
            <div class="container">
                <p>
                    Make sure nobody is listening to you: a microphone icon will appear in the System Tray whenever an app is able to record audio.
                <p>
            </div>
        </div>
    </div>
</section>
-->
<script>
    // add autoplay for tabs video
    document.querySelectorAll('.tab-pane.video').forEach(function(tab) {
        tab = tab.id;
        const nav = document.getElementById(`${tab}-control`);
        const video = document.querySelector(`#${tab} video`);
        const button = document.querySelector(`#${tab} button`);

        nav.addEventListener('click', function() {
            if (!button.classList.contains('d-none')) {
                button.classList.add('d-none');
            }
            video.currentTime = 0;
            video.play();
        });

        video.addEventListener('ended', function() {
            if (button.classList.contains('d-none')) {
                button.classList.remove('d-none');
            }
        });

        button.addEventListener('click', function() {
            button.classList.add('d-none');
            video.currentTime = 0;
            video.play();
        });
    });
    
    // add autoplay for tabs with two videos (laptop and mobile)
    document.querySelectorAll('.tab-pane.two-videos').forEach(function(tab) {
        tab = tab.id;
        const nav = document.getElementById(`${tab}-control`);
        const video_mobile = document.querySelector(`#${tab} .phone-overlay video`);
        const video_laptop = document.querySelector(`#${tab} .laptop-overlay video`);
        const button = document.querySelector(`#${tab} button`);

        nav.addEventListener('click', function() {
            if (!button.classList.contains('d-none')) {
                button.classList.add('d-none');
            }
            video_mobile.currentTime = 0;
            video_laptop.currentTime = 0;
            video_mobile.play();
            video_laptop.play();
        });

        video_mobile.addEventListener('ended', function() {
            if (button.classList.contains('d-none')) {
                button.classList.remove('d-none');
            }
        });

        button.addEventListener('click', function() {
            button.classList.add('d-none');
            video_mobile.currentTime = 0;
            video_laptop.currentTime = 0;
            video_mobile.play();
            video_laptop.play();
        });
    });

    // create config object: rootMargin and threshold
    // are two properties exposed by the interface
    const config = {
      rootMargin: '0px 0px 50px 0px',
      threshold: 0
    };

    // register the config object with an instance
    // of intersectionObserver
    let observer = new IntersectionObserver(function(entries, self) {
        // iterate over each entry
        entries.forEach(entry => {
            // process just the images that are intersecting.
            // isIntersecting is a property exposed by the interface
            if(entry.isIntersecting) {
                // custom function that copies the path to the img
                // from data-src to src
                entry.target.src = entry.target.dataset.src;
                // the image is now in place, stop watching
                self.unobserve(entry.target);
            }
        });
    }, config);


    const imgs = document.querySelectorAll('[data-src]');
    imgs.forEach(img => {
        observer.observe(img);
    });

    document.querySelectorAll('.nav-features .nav-link').forEach(nav => {
        nav.addEventListener('mouseover', function(event) {
            const tab_content = document.querySelector(`${event.target.getAttribute("href")} [data-src]`);
            if (tab_content) {
                tab_content.src = tab_content.dataset.src;
            }
        });
    });
</script>

<?php
	require('aether/footer.php');
?>
