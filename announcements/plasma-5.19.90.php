<?php
    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Plasma 5.20 Beta: one absolutely massive release",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = 'plasma-5.19.90'; // for i18n
    $version = "5.19.90";
?>

<script src="/js/use-ekko-lightbox.js" defer="true"></script>


<main class="releaseAnnouncment container">
  <?php include "./announce-i18n-bar.inc"; ?>

  <h1 class="mt-2"><?php print i18n_var("Plasma %1", "5.20 Beta")?></h1>
  <div class="laptop-with-overlay d-block mt-3 mx-auto" style="max-width: 800px">
    <img class="laptop img-fluid mt-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
    <div class="laptop-overlay">
      <img class="img-fluid mt-3" src="/announcements/plasma-5.20/plasma-5.20.png" alt="<?php print i18n_var("KDE Plasma %1", "5.20 Beta")?>" loading="lazy" width="800" height="450" />
    </div>
  </div>

  <p><?php print i18n("Wednesday, 16 September 2020.") ?></p>

  <p><?php i18n("Plasma 5.20 is going to be one absolutely massive release! More features, more fixes for longstanding bugs, more improvements to the user interface! Read on for details:")?></p>

  <p><?php i18n("Read on to discover all the new features and improvements of Plasma 5.20…"); ?></p>

  <h2 id="look-and-feel"><?php i18n("Look and Feel");?></h2>

    <figure class="text-center">
        <a href="plasma-5.20/task-manager.png" data-toggle="lightbox">
            <img src="/announcements/plasma-5.20/task-manager.png"  width="209" style="width: 100%; max-width: 500px; height: auto;" alt="Icon only task manager by default" />
        </a>
        <a href="plasma-5.20/clock.png" data-toggle="lightbox">
            <img src="/announcements/plasma-5.20/clock.png"  width="209" style="width: 100%; max-width: 500px; height: auto;" alt="Clock now with date by default" />
        </a>
    <figcaption><?php i18n("Updated panel default settings");?></figcaption>
  </figure>

  <ul>
    <li><?php i18n("An Icon-Only Task Manager is used by default, and the default panel is now slightly thicker"); ?></li>

    <li><?php i18n("On-screen displays (OSDs) that appear when changing the volume or display brightness (for example) have been redesigned to be less obtrusive. And when using the 'raise maximum volume' setting, the volume OSD will subtly warn you when going over 100% volume"); ?></li>

    <li><?php i18n("Changing the display brightness now gives you a smooth transition"); ?></li>

    <li><?php i18n("The System Tray popup now shows items in a grid rather than a list, and the icon view on your panel can now be configured to scale the icons with the panel thickness if you'd like"); ?></li>

    <li><?php i18n("The Digital Clock applet now shows the current date by default"); ?></li>

    <li><?php i18n("The Digital Clock popup is now more compact and space-efficient"); ?></li>

    <li><?php i18n("Throughout KDE apps, toolbar buttons which display menus when clicked now show downward-pointing arrows to indicate this"); ?></li>
  </ul>
  <h2 id="look-and-feel"><?php i18n("Workspace Behavior");?></h2>

    <figure class="text-center">
        <video width="640" height="360" preload="metadata" controls="controls"><source type="video/mp4" src="/announcements/plasma-5.20/gtk.mp4" />
            <a href="plasma-5.20/gtk.mp4">GTK Window Controls</a>
            </video>
        <figcaption><?php i18n("GTK Window Controls");?></figcaption>
    </figure>
    <figure class="text-center">
        <a href="plasma-5.20/krunner.jpeg" data-toggle="lightbox">
            <img src="/announcements/plasma-5.20/krunner.jpeg"  width="752" style="width: 100%; max-width: 500px; height: auto;" alt="KRunner standalone" />
        </a>
        <figcaption><?php i18n("KRunner standalone");?></figcaption>
    </figure>

  <ul>
    <li><?php i18n("Changed the default shortcut for moving and resizing windows to Meta+drag (instead of Alt+drag) to resolve conflicts with many popular productivity applications"); ?></li>

    <li><?php i18n("Clicking on grouped Task Manager tasks now cycles through each task by default, and you can choose alternative behaviors if you don't like that one"); ?></li>

    <li><?php i18n("The Task Manager can now be optionally configured to not minimize the active task when clicked on"); ?></li>

    <li><?php i18n("On supported laptops, you can now configure a charge limit below 100% to preserve the battery health"); ?></li>

    <li><?php i18n("You can now corner-tile windows by combining left/right/up/down tiling shortcuts. For example, hit Meta+up arrow and then left arrow to tile a window to the upper left corner."); ?></li>

    <li><?php i18n("GTK headerbar apps now respect the appearance you've chosen for your titlebar buttons"); ?></li>

    <li><?php i18n("Widgets now display an 'About' page in the settings window"); ?></li>

    <li><?php i18n("You are now notified when your system is about to run out of space even if your home directory is on a different partition"); ?></li>

    <li><?php i18n("Minimized windows are no longer placed last in the alt-tab task switcher"); ?></li>
    <li><?php i18n("KRunner can now be configured to be a free-floating window, rather than glued to the top of the screen"); ?></li>
    <li><?php i18n("KRunner now remembers the prior search text by default and can be optionally configured to appear as a free-floating window"); ?></li>

    <li><?php i18n("KRunner can search and launch open pages in Falkon"); ?></li>

    <li><?php i18n("Unused audio devices are now filtered out of the audio applet and system settings page by default"); ?></li>

    <li><?php i18n("The Device Notifier applet has been renamed to 'Disks &amp; Devices' and it is now easier to use it to show all disks, not just removable ones"); ?></li>

    <li><?php i18n("You can now middle-click on the Notifications applet or system tray icon to enter Do Not Disturb mode"); ?></li>

    <li><?php i18n("The Web Browser widget now has a user-configurable zoom setting"); ?></li>
  </ul>

  <h2 id="look-and-feel"><?php i18n("System Settings &amp; Info Center");?></h2>

  <figure class="text-center">
        <a href="plasma-5.20/plasma-disks.png" data-toggle="lightbox">
            <img src="/announcements/plasma-5.20/plasma-disks-wee.png"  width="500" style="width: 100%; max-width: 500px; height: auto;" alt="Plasma Disks for S.M.A.R.T disk monitoring" />
        </a>
    <figcaption><?php i18n("Plasma Disks for S.M.A.R.T disk monitoring");?></figcaption>
  </figure>
  <ul>
    <li><?php i18n("There is now a 'highlight changed settings' feature that will show you which settings you've changed from their default values"); ?></li>

    <li><?php i18n("S.M.A.R.T monitoring and failing disk notifications"); ?></li>

    <li><?php i18n("The Autostart, Bluetooth, and User Manager settings pages have been redesigned according to modern user interface standards and rewritten from scratch"); ?></li>

    <li><?php i18n("The Standard Shortcuts and Global Shortcuts pages have been combined into one, which is just called 'Shortcuts' now"); ?></li>

    <li><?php i18n("A new audio balance option has been added to let you adjust the volume of each individual audio channel"); ?></li>

    <li><?php i18n("The touchpad cursor speed can now be configured on a much more granular basis"); ?></li>
  </ul>

  <h2 id="look-and-feel"><?php i18n("Wayland");?></h2>

  <ul>
    <li><?php i18n("Klipper support"); ?></li>

    <li><?php i18n("You can now middle-click to paste (at least in KDE apps; GTK apps do not implement this yet)"); ?></li>

    <li><?php i18n("KRunner appears at the right place when using a top panel"); ?></li>

    <li><?php i18n("Mouse and touchpad scroll speed is now adjustable"); ?></li>

    <li><?php i18n("Screencasting is now supported"); ?></li>

    <li><?php i18n("The Task Manager now shows window thumbnails"); ?></li>

    <li><?php i18n("The whole desktop session no longer crashes if XWayland crashes"); ?></li>
  </ul>

  <a href="plasma-5.19.5-5.19.90-changelog.php"><?php print i18n_var("Full Plasma %1 changelog", "5.20 Beta"); ?></a>

    <!-- // Boilerplate again -->
    <section class="row get-it" id="download">
        <article class="col-md">
            <h2><?php i18n("Live Images");?></h2>
            <p>
                <?php i18n("The easiest way to try it out is with a live image booted off a USB disk. Docker images also provide a quick and easy way to test Plasma.");?>
            </p>
            <a href='https://community.kde.org/Plasma/Live_Images' class="learn-more"><?php i18n("Download live images with Plasma 5");?></a>
            <a href='https://community.kde.org/Plasma/Docker_Images' class="learn-more"><?php i18n("Download Docker images with Plasma 5");?></a>
        </article>

        <article class="col-md">
            <h2><?php i18n("Package Downloads");?></h2>
            <p>
                <?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
            </p>
            <a href='https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro' class="learn-more"><?php i18n("Get KDE Software on Your Linux Distro wiki page");?></a>
        </article>

        <article class="col-md">
            <h2><?php i18n("Source Downloads");?></h2>
            <p>
                <?php i18n("You can install Plasma 5 directly from source.");?>
            </p>
            <a href='https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source' class='learn-more'><?php i18n("Community instructions to compile it");?></a>
            <a href='../info/plasma-5.15.90.php' class='learn-more'><?php i18n("Source Info Page");?></a>
        </article>
    </section>

    <section class="give-feedback">
        <h2><?php i18n("Feedback");?></h2>

        <p class="kSocialLinks">
            <?php i18n("You can give us feedback and get updates on our social media channels:"); ?>
            <?php include($site_root . "/contact/social_link.inc"); ?>
        </p>
        <p>
            <?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?>
        </p>

        <p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>Plasma Matrix chat room</a>, <a href='%2'>Plasma-devel mailing list</a> or report issues via <a href='%3'>bugzilla</a>. If you like what the team is doing, please let them know!", "https://webchat.kde.org/#/room/#plasma:kde.org", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided"); ?>

        <p><?php i18n("Your feedback is greatly appreciated.");?></p>
    </section>

    <h2>
        <?php i18n("Supporting KDE");?>
    </h2>

    <p align="justify">
        <?php print i18n_var("KDE is a <a href='%1'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='%2'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='%3'>Join the Game</a> initiative.", "http://www.gnu.org/philosophy/free-sw.html", "/community/donations/", "https://relate.kde.org/civicrm/contribute/transact?id=5"); ?>
    </p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

</main>
<?php
  require('../aether/footer.php');
