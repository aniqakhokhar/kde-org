<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.19.1 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.19.1";
?>

<style>
main {
	padding-top: 20px;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='bluedevil' href='https://commits.kde.org/bluedevil'>Bluedevil</a> </h3>
<ul id='ulbluedevil' style='display: block'>
<li>[applet] Fix tooltip showing wrong name for connected device. <a href='https://commits.kde.org/bluedevil/29a535b1edfbd46b77b04ae43cd279729f0e6b82'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422691'>#422691</a></li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Appstream: Try harder at de-duplicating. <a href='https://commits.kde.org/discover/2eee9b0bbdba708ec0b5baf37494461ec248caa7'>Commit.</a> </li>
</ul>


<h3><a name='drkonqi' href='https://commits.kde.org/drkonqi'>Dr Konqi</a> </h3>
<ul id='uldrkonqi' style='display: block'>
<li>Map neon in platform guessing. <a href='https://commits.kde.org/drkonqi/4f0544a2e92a9c01a5ba58a01e9c268318dab31f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422556'>#422556</a></li>
</ul>


<h3><a name='kactivitymanagerd' href='https://commits.kde.org/kactivitymanagerd'>kactivitymanagerd</a> </h3>
<ul id='ulkactivitymanagerd' style='display: block'>
<li>Fix removing of icons and activity settings for new and existing setups. <a href='https://commits.kde.org/kactivitymanagerd/54896998ef1249f721a2fe616b61b51b4eda33fd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385814'>#385814</a></li>
<li>Remove too strict Qt/KF5 deprecation rule. <a href='https://commits.kde.org/kactivitymanagerd/7b85bc1d5d4688e1ac5da96b152d52e9a4008933'>Commit.</a> </li>
</ul>


<h3><a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a> </h3>
<ul id='ulkde-gtk-config' style='display: block'>
<li>Fix incorrect palette name. <a href='https://commits.kde.org/kde-gtk-config/4fe8217fc41729125257905fb07417b7519139cc'>Commit.</a> </li>
</ul>


<h3><a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a> </h3>
<ul id='ulkinfocenter' style='display: block'>
<li>Address trailing newline and spaces. <a href='https://commits.kde.org/kinfocenter/d3151145b84330c5e16856138bc04ca5e674ebf1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422968'>#422968</a></li>
<li>Usb: parse version from sysfs entries correctly. <a href='https://commits.kde.org/kinfocenter/1a42afef690644074a97559cbace45d5a256f837'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422610'>#422610</a></li>
</ul>


<h3><a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a> </h3>
<ul id='ulksysguard' style='display: block'>
<li>Add units to disk/all/{read,write}. <a href='https://commits.kde.org/ksysguard/8b57868009eb8cd55ae53da7f7baafb41a98d064'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422643'>#422643</a>. Fixes bug <a href='https://bugs.kde.org/422644'>#422644</a></li>
<li>Use new name for dbus interface too. <a href='https://commits.kde.org/ksysguard/5391f4def701629ffbb2d0b20ef92af2fec7c313'>Commit.</a> </li>
<li>Don't prefix value output with mount point. <a href='https://commits.kde.org/ksysguard/ea0da8491278a27b9238d4660e19abaa9a6c331c'>Commit.</a> </li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>[kcm/kwinrules] Fix slow scrolling through rules list. <a href='https://commits.kde.org/kwin/e307038f84dc0fc0556b2ceb259da0c09b035ce1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421537'>#421537</a></li>
</ul>


<h3><a name='libksysguard' href='https://commits.kde.org/libksysguard'>libksysguard</a> </h3>
<ul id='ullibksysguard' style='display: block'>
<li>Use new name for dbus interface. <a href='https://commits.kde.org/libksysguard/27da06df3abaf707d61686e3a301a84df7b099c8'>Commit.</a> </li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Fix layouting bug for kicker in top panel. <a href='https://commits.kde.org/plasma-desktop/2f5345c3c132ffeba6eedb1cedd69cfddcb7c4f1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399554'>#399554</a></li>
<li>Fix too small result list when favorites are added. <a href='https://commits.kde.org/plasma-desktop/aaba7dbf6eda2293a772ab70ed7825f8536082b1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422398'>#422398</a></li>
<li>Fix unprintable characters. <a href='https://commits.kde.org/plasma-desktop/cf30a7f7083c5cbc804b007b83a0ac2283cd4cd6'>Commit.</a> </li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Openvpn: avoid enabling TCP if the remote has been set on another line. <a href='https://commits.kde.org/plasma-nm/eaf36c0ce4cb4b4cd9cfd597afe7cd816f6856a0'>Commit.</a> </li>
<li>Testing AccessPoint::Privacy flag is not enough to check whether AP is secured. <a href='https://commits.kde.org/plasma-nm/4fbe7b4d19e9208141ccdd245ae68cb63572a3b8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422740'>#422740</a></li>
</ul>


<h3><a name='plasma-thunderbolt' href='https://commits.kde.org/plasma-thunderbolt'>plasma-thunderbolt</a> </h3>
<ul id='ulplasma-thunderbolt' style='display: block'>
<li>Don't set a parent on sharedpointer mangaed object. <a href='https://commits.kde.org/plasma-thunderbolt/8363502d2289524e5336a1928637fb316282163a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423029'>#423029</a></li>
</ul>


<h3><a name='plasma-vault' href='https://commits.kde.org/plasma-vault'>plasma-vault</a> </h3>
<ul id='ulplasma-vault' style='display: block'>
<li>[applet] Restore former default action. <a href='https://commits.kde.org/plasma-vault/9eefb78c95bfe612336bdf8d7474340cea1960c0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422917'>#422917</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Fix race in loading DBus activatable applets. <a href='https://commits.kde.org/plasma-workspace/1c3dc48629d1b27df93244147f0a52a65dd8c0c9'>Commit.</a> </li>
<li>[applets/systemtray] Fix SNI "Always shown" restore. <a href='https://commits.kde.org/plasma-workspace/3367908e976ca5be3ee6d066fd2c278613f5fe50'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422837'>#422837</a></li>
<li>Center align battery icon in compact representation. <a href='https://commits.kde.org/plasma-workspace/0b92a51ee70cc159a1f6ba04905999e0117738b2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421038'>#421038</a></li>
<li>Fix actions for application search results. <a href='https://commits.kde.org/plasma-workspace/090e27b0449ba3ae919a6635e439c6f729e9ea85'>Commit.</a> </li>
<li>Fix desktop actions for recently used applications. <a href='https://commits.kde.org/plasma-workspace/d75bff14b46f02d446c0775ff625736524fa51fa'>Commit.</a> </li>
<li>Fix absolute icon paths. <a href='https://commits.kde.org/plasma-workspace/ba44b69abf82b1236ffab7d8683728c142d30c52'>Commit.</a> </li>
<li>Make hidden plasmoids vertically centered in the System Tray popup applet. <a href='https://commits.kde.org/plasma-workspace/25de25cb77cbe3733bb7ed04a86706911e867ba1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422685'>#422685</a></li>
<li>Make KRunner KCM open in System Settings. <a href='https://commits.kde.org/plasma-workspace/9f058255138b878d9ead77ec4bffbbaef6c0c1bc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421427'>#421427</a></li>
<li>Guard QQmldebuggingEnabled by an env var. <a href='https://commits.kde.org/plasma-workspace/dd24739479b33263c1016ec3938d4c81abdc981a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422228'>#422228</a></li>
<li>Fix confirmLogout setting for SessionManagement. <a href='https://commits.kde.org/plasma-workspace/d49e0a406857287658f205ed8b0aaf8bf31dbb80'>Commit.</a> </li>
</ul>


</main>
<?php
	require('../aether/footer.php');
