<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Hide]</a></h3>
<ul id='ulakonadi' style='display: block'>
<li>Fix aggregation logic for cacheOnly and ignoreErrors. <a href='http://commits.kde.org/akonadi/a646904587fc57fa2536eb6daf68f46ffba08787'>Commit.</a> </li>
<li>Autotests: rename notificationmanagertest to notificationsubscribertest. <a href='http://commits.kde.org/akonadi/5c582e8697342b15be5d16eff9d0b5e209888bdf'>Commit.</a> </li>
<li>Extend tagtest with more checks that the full tag information is sent. <a href='http://commits.kde.org/akonadi/69ec2aff1f3b380f4f3e1fadcb4f198256132ac5'>Commit.</a> </li>
<li>Autotests: remove QEXPECT_FAIL, the mysql bug got fixed. <a href='http://commits.kde.org/akonadi/2c10e118351d19386b897cdb7999ffb7c7176138'>Commit.</a> </li>
<li>Fix tag name/type/gid missing in notifications about tags. <a href='http://commits.kde.org/akonadi/d5ccee37b551df4713bf4239e076e5d3622e4952'>Commit.</a> </li>
<li>Minor: improve QDebug output for Akonadi::Tag. <a href='http://commits.kde.org/akonadi/4c1b12694e4be879e51e82ded93a9b8878a75411'>Commit.</a> </li>
<li>Actually look for mysql binaries in PATH. <a href='http://commits.kde.org/akonadi/a57dd124b3d231ad64f95529f24a3ad07a3c6f63'>Commit.</a> </li>
<li>Allow to exclude unified mailbox from local subscription. <a href='http://commits.kde.org/akonadi/5876535d9170ddbb26b4fd88d58cb39a4491a76b'>Commit.</a> </li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Hide]</a></h3>
<ul id='ulark' style='display: block'>
<li>Remember part URL to delete the tempfile even if the user used save as. <a href='http://commits.kde.org/ark/d412ca6255230d809aa550891f1af9049cf092d9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402529'>#402529</a></li>
</ul>
<h3><a name='cantor' href='https://cgit.kde.org/cantor.git'>cantor</a> <a href='#cantor' onclick='toggle("ulcantor", this)'>[Hide]</a></h3>
<ul id='ulcantor' style='display: block'>
<li>Fix bug with unfocused entry after Cantor start. <a href='http://commits.kde.org/cantor/9840bfb9eb3b374b6b996ae256d893a44ad62071'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395976'>#395976</a></li>
<li>Fix bug with still visible entry cursor after focusing on created entry. <a href='http://commits.kde.org/cantor/ca3270d01d83c55fbeb70f94c0e908ebdacdeab8'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>Fix member initialization. <a href='http://commits.kde.org/dolphin/83912a6de826d4a159fb7b4505f2869cc10b84bb'>Commit.</a> </li>
<li>[versioncontrolobserver] Update working directory on tab activation. <a href='http://commits.kde.org/dolphin/ec29cfff2017b40236edadcae50b8c1a1915d04e'>Commit.</a> </li>
<li>[versioncontrolobserver] Do not use static plugin objects. <a href='http://commits.kde.org/dolphin/9a9fbae988147c5651ce9e2bd2b412d32c1bf01d'>Commit.</a> </li>
</ul>
<h3><a name='dolphin-plugins' href='https://cgit.kde.org/dolphin-plugins.git'>dolphin-plugins</a> <a href='#dolphin-plugins' onclick='toggle("uldolphin-plugins", this)'>[Hide]</a></h3>
<ul id='uldolphin-plugins' style='display: block'>
<li>[gitpushdialog] Check GitWrapper::branches index validity. <a href='http://commits.kde.org/dolphin-plugins/9f8f6c15211688f7d5a7a3d3bff6be6b3df0d8f4'>Commit.</a> </li>
</ul>
<h3><a name='kapptemplate' href='https://cgit.kde.org/kapptemplate.git'>kapptemplate</a> <a href='#kapptemplate' onclick='toggle("ulkapptemplate", this)'>[Hide]</a></h3>
<ul id='ulkapptemplate' style='display: block'>
<li>Set StartupWMClass in desktop file. <a href='http://commits.kde.org/kapptemplate/597434d9c969c939b344681a598db8d8da3893f8'>Commit.</a> </li>
</ul>
<h3><a name='kcachegrind' href='https://cgit.kde.org/kcachegrind.git'>kcachegrind</a> <a href='#kcachegrind' onclick='toggle("ulkcachegrind", this)'>[Hide]</a></h3>
<ul id='ulkcachegrind' style='display: block'>
<li>Fix registration with D-Bus, use service name as announced in desktop file. <a href='http://commits.kde.org/kcachegrind/36d4d6ffd19d862ab8e21d7fddf0772542b21f64'>Commit.</a> </li>
<li>Install the appdata file. <a href='http://commits.kde.org/kcachegrind/1fcd59e585601482cc6c3ffee7e59552769f1ba6'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Update copyright year also. <a href='http://commits.kde.org/kdenlive/196294886d1b8f9f750e84d4f0fcf3120dafb732'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Hide]</a></h3>
<ul id='ulkdepim-addons' style='display: block'>
<li>Fix Bug 403174 - KAddressBook looses contact birthdays when merging two or more contacts. <a href='http://commits.kde.org/kdepim-addons/079955d78d0ba6e1a1cafae91a669253a400acdb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403174'>#403174</a></li>
</ul>
<h3><a name='kidentitymanagement' href='https://cgit.kde.org/kidentitymanagement.git'>kidentitymanagement</a> <a href='#kidentitymanagement' onclick='toggle("ulkidentitymanagement", this)'>[Hide]</a></h3>
<ul id='ulkidentitymanagement' style='display: block'>
<li>Fix show error as utf8. <a href='http://commits.kde.org/kidentitymanagement/47846a5cc682230f251f2c222c9e4cd973ad296c'>Commit.</a> </li>
</ul>
<h3><a name='kjumpingcube' href='https://cgit.kde.org/kjumpingcube.git'>kjumpingcube</a> <a href='#kjumpingcube' onclick='toggle("ulkjumpingcube", this)'>[Hide]</a></h3>
<ul id='ulkjumpingcube' style='display: block'>
<li>Install the appdata file. <a href='http://commits.kde.org/kjumpingcube/a9eed2bb988d3971f3fa3bf6cce157f6dd0f434c'>Commit.</a> </li>
</ul>
<h3><a name='kleopatra' href='https://cgit.kde.org/kleopatra.git'>kleopatra</a> <a href='#kleopatra' onclick='toggle("ulkleopatra", this)'>[Hide]</a></h3>
<ul id='ulkleopatra' style='display: block'>
<li>Remove double margin. <a href='http://commits.kde.org/kleopatra/ada23e7efceea57ef86c0a41436bf661dbb45e16'>Commit.</a> </li>
<li>Remove double margin. <a href='http://commits.kde.org/kleopatra/8217c599bd00e0940d18bcc077d65acdb0f02d9f'>Commit.</a> </li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Fix Bug 403313 - Encoding problem on kmail2 5.10.1 when creating composer from command line. <a href='http://commits.kde.org/kmail/3cebe77c92460505af04827defeb0a9e358ba00b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403313'>#403313</a></li>
</ul>
<h3><a name='kmime' href='https://cgit.kde.org/kmime.git'>kmime</a> <a href='#kmime' onclick='toggle("ulkmime", this)'>[Hide]</a></h3>
<ul id='ulkmime' style='display: block'>
<li>Allow to detect attachment when mail use alternative. <a href='http://commits.kde.org/kmime/ae67e62c614be5f50ac51e9c5d5be66fa7c883db'>Commit.</a> </li>
<li>Add more autotest. <a href='http://commits.kde.org/kmime/0f380cedeb423ad6fb8711d5d1cc8be5f9d707f5'>Commit.</a> </li>
<li>Fix error. <a href='http://commits.kde.org/kmime/3ab1b8f3545390e45d5ddb081b04a3755c6402d5'>Commit.</a> </li>
</ul>
<h3><a name='konqueror' href='https://cgit.kde.org/konqueror.git'>konqueror</a> <a href='#konqueror' onclick='toggle("ulkonqueror", this)'>[Hide]</a></h3>
<ul id='ulkonqueror' style='display: block'>
<li>Fix license. <a href='http://commits.kde.org/konqueror/eb09362336f3e78d8f25adf10aaa6049036c665c'>Commit.</a> </li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Hide]</a></h3>
<ul id='ulkonsole' style='display: block'>
<li>Revert "Fix crash in extendSelection". <a href='http://commits.kde.org/konsole/0236ef3bd3e19c591b166f59608f59cb9c31a105'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403117'>#403117</a></li>
</ul>
<h3><a name='krdc' href='https://cgit.kde.org/krdc.git'>krdc</a> <a href='#krdc' onclick='toggle("ulkrdc", this)'>[Hide]</a></h3>
<ul id='ulkrdc' style='display: block'>
<li>Properly show xfreerdp runtime dependency in cmake summary. <a href='http://commits.kde.org/krdc/f013a85b5a2cafa50250efb396f209afad7fc20e'>Commit.</a> </li>
<li>Style++. <a href='http://commits.kde.org/krdc/c3ba7c0e3eb9fdd2a3d2f1b09551cd27748e269d'>Commit.</a> </li>
</ul>
<h3><a name='ktuberling' href='https://cgit.kde.org/ktuberling.git'>ktuberling</a> <a href='#ktuberling' onclick='toggle("ulktuberling", this)'>[Hide]</a></h3>
<ul id='ulktuberling' style='display: block'>
<li>Fix typo in email address. <a href='http://commits.kde.org/ktuberling/693236b9d54642a1f483e3d0efc1671ec870cae8'>Commit.</a> </li>
</ul>
<h3><a name='libkdepim' href='https://cgit.kde.org/libkdepim.git'>libkdepim</a> <a href='#libkdepim' onclick='toggle("ullibkdepim", this)'>[Hide]</a></h3>
<ul id='ullibkdepim' style='display: block'>
<li>Add an option to skip the AkonadiSearchPIM dependency, to simplify Windows build. <a href='http://commits.kde.org/libkdepim/c185b4f51e6196b82a1289f8d2a1adb8ec3b3770'>Commit.</a> </li>
</ul>
<h3><a name='lokalize' href='https://cgit.kde.org/lokalize.git'>lokalize</a> <a href='#lokalize' onclick='toggle("ullokalize", this)'>[Hide]</a></h3>
<ul id='ullokalize' style='display: block'>
<li>Improve DockWidgets saving and restoring. <a href='http://commits.kde.org/lokalize/cde1083d0cfbb75bdbbed14173758a29fbaf45f5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403234'>#403234</a></li>
<li>Set the proper entry index in catalog after a filter is applied. <a href='http://commits.kde.org/lokalize/689e28142b9e2f50f34a0e9c7a69416335af1e59'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400208'>#400208</a></li>
<li>Initialize KCrash after QApp instance. <a href='http://commits.kde.org/lokalize/6e09147303cfeec7f402934517889bb783e6559a'>Commit.</a> </li>
</ul>
<h3><a name='mailcommon' href='https://cgit.kde.org/mailcommon.git'>mailcommon</a> <a href='#mailcommon' onclick='toggle("ulmailcommon", this)'>[Hide]</a></h3>
<ul id='ulmailcommon' style='display: block'>
<li>Fix bug found by David. <a href='http://commits.kde.org/mailcommon/ce63b3e8696ace726f44cf3cc0961b478010c792'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Fix Bug 403460 - Expand BCC list of emails button is oriented in non-intuitive direction. <a href='http://commits.kde.org/messagelib/0ca70c35f97808bf63288390dc1c1eac0ab4a54a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403460'>#403460</a></li>
<li>Disable code for the moment. <a href='http://commits.kde.org/messagelib/3c49d10526a67acf14d78df5a9d3a8a30f046764'>Commit.</a> </li>
<li>Don't show "Attachment:" when there is a mail embedded. <a href='http://commits.kde.org/messagelib/3d48c6a948a012cd64746dee5ec1b0559bc72281'>Commit.</a> </li>
<li>Avoid to wrap word. <a href='http://commits.kde.org/messagelib/c17d9c7f46ecfd669609bb1b310a7b144c3f41f6'>Commit.</a> </li>
<li>It was fixed in kmime. <a href='http://commits.kde.org/messagelib/dfe760c55100681151e498b11a32ab3976f720fb'>Commit.</a> </li>
<li>Fix show attachment as reported by David. <a href='http://commits.kde.org/messagelib/f9917f65001418166a3fdb54f1dd3caada0b6a21'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Fix MimeType in org.kde.mobile.okular_comicbook.desktop. <a href='http://commits.kde.org/okular/0d958be2900bf0890b660fbbe373e1817f6bace2'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Hide]</a></h3>
<ul id='ulumbrello' style='display: block'>
<li>Compile fix. <a href='http://commits.kde.org/umbrello/1f1e86aeeea292d9aacefbd37bf26699e45bc97f'>Commit.</a> </li>
<li>Fix 'Duplicated text labels in xmi file'. <a href='http://commits.kde.org/umbrello/e4aaa80ff8d2a23d739e8cc05d39e7a11599f700'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403888'>#403888</a></li>
<li>Fix 'Copy and paste of interface in component diagram does not update the displayed name'. <a href='http://commits.kde.org/umbrello/48be64aa24851878e466de7a858887c1cde5aa61'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403853'>#403853</a></li>
<li>Fix 'Displayed interface name in the component diagram is not updated after changing it in the properties dialog'. <a href='http://commits.kde.org/umbrello/ea41f50f515103b9461e78167bebe2deec04267c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403852'>#403852</a></li>
<li>In AssociationWidget::widgetMoved() fix endless loop. <a href='http://commits.kde.org/umbrello/a9ce7bc064a0c0910f046427eb26ee0ff99f4154'>Commit.</a> See bug <a href='https://bugs.kde.org/403761'>#403761</a></li>
<li>Add test file for component diagram elements and associations. <a href='http://commits.kde.org/umbrello/96d20006958d6f337ab4cd34957b1e8e901997b2'>Commit.</a> See bug <a href='https://bugs.kde.org/403763'>#403763</a></li>
<li>Fix 'Displayed name of an interface in a component diagram is not updated'. <a href='http://commits.kde.org/umbrello/0b46604faae48016f8dd7dcb34fdcb8c9857bd64'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403763'>#403763</a></li>
<li>Fixes the problem that the geometry of the interface widget was not updated when the connection was disconnected. <a href='http://commits.kde.org/umbrello/c2f15333696a9fd8b66ef546f5d38d677152eaea'>Commit.</a> See bug <a href='https://bugs.kde.org/403761'>#403761</a></li>
<li>Fix 'incorrect display of interface widgets in the component diagram when adding a second connection'. <a href='http://commits.kde.org/umbrello/d5cb5adb7fe517ea796f08afa0840ebb6507bd4f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403761'>#403761</a></li>
<li>In UMLWidget::mouseMoveEvent() remove duplicated call to adjustUnselectedAssocs(). <a href='http://commits.kde.org/umbrello/c995f32e05f648bfe3800f3c8da0fbfafc41cca8'>Commit.</a> See bug <a href='https://bugs.kde.org/403692'>#403692</a></li>
<li>Fix 'Associations connected to the component port do not move when moving the component'. <a href='http://commits.kde.org/umbrello/a7d6f7dc24f3b52c3deaf55bf3b745652efa2994'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403692'>#403692</a></li>
<li>Fix unused parameter warning. <a href='http://commits.kde.org/umbrello/40c905379aae3e8a29fa3d968023199ea5f85f7a'>Commit.</a> </li>
<li>On loading port widgets from xmi file setup connected state. <a href='http://commits.kde.org/umbrello/2fe1bbce7695afa160645c90b8be7062b24e4d9d'>Commit.</a> See bug <a href='https://bugs.kde.org/403622'>#403622</a></li>
<li>Do not add building tests unconditional. <a href='http://commits.kde.org/umbrello/1c9882e7ad01630049226a1f89a79a39d8f23cee'>Commit.</a> </li>
<li>Fix 'Incorrect port positions when loading old files'. <a href='http://commits.kde.org/umbrello/be70c5a943c10ba6ed9c6974ec99c478e4a81b79'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403622'>#403622</a></li>
<li>Make sure to only remove present ports. <a href='http://commits.kde.org/umbrello/fd91b82bacd1d928362d06440b14e09452df8d2f'>Commit.</a> See bug <a href='https://bugs.kde.org/403515'>#403515</a></li>
<li>Use a better location for deleting UML Port instance. <a href='http://commits.kde.org/umbrello/7f44fe35f4978096176285ffbc0a36aba1d4d0de'>Commit.</a> See bug <a href='https://bugs.kde.org/403515'>#403515</a></li>
<li>Fix 'Port widgets cannot be deleted from the context menu'. <a href='http://commits.kde.org/umbrello/cf5d9e09b14a96dda80ef6177127ed28c729b240'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403618'>#403618</a></li>
<li>Add "show" and "properties" to context menu of tree view item types 'category' and 'artifact'. <a href='http://commits.kde.org/umbrello/3d476963180c3de82827c8e0963fbc03463b2669'>Commit.</a> See bug <a href='https://bugs.kde.org/116354'>#116354</a></li>
<li>Add missing "show" in diagram context menu entry to tree view items. <a href='http://commits.kde.org/umbrello/f9303f4a976c5c4f70fb46e928813f24675856fe'>Commit.</a> See bug <a href='https://bugs.kde.org/116354'>#116354</a></li>
<li>When deleting port widgets, also delete the corresponding UML port. <a href='http://commits.kde.org/umbrello/bc435c590cf357818f99a1e9120a023e1ea5a380'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403515'>#403515</a></li>
<li>Fix center position for showing port widgets. <a href='http://commits.kde.org/umbrello/f0afd735dc1aaf66da208951b96377a421b3653e'>Commit.</a> See bug <a href='https://bugs.kde.org/403515'>#403515</a></li>
<li>Add missing port widgets when loading component diagrams. <a href='http://commits.kde.org/umbrello/4cd475f384c50661b7751d50a91befd5504b7d87'>Commit.</a> See bug <a href='https://bugs.kde.org/403515'>#403515</a></li>
<li>Fix minor Doxygen issues. <a href='http://commits.kde.org/umbrello/2d877cd8ef636daf82a916cbdf879acda4a723f4'>Commit.</a> </li>
</ul>
