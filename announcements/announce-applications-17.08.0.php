<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships KDE Applications 17.08.0");
  $site_root = "../";
  $version = "17.08.0";
  $release = "applications-".$version;
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p align="justify">
<?php print i18n_var("August 17, 2017. KDE Applications 17.08 is here. We have worked to make both the applications and the underlying libraries more stable and easier to use. By ironing out wrinkles and listening to your feedback, we have made the KDE Applications suite less prone to glitches and much friendlier. Enjoy your new apps!");?>
</p>

<h3 style="clear:both;" ><?php print i18n_var("More Porting to KDE Frameworks 5");?></h3>
<p align="justify">
<?php print i18n_var("We're pleased that the following applications which were based on kdelibs4 are now based on KDE Frameworks 5: kmag, kmousetool, kgoldrunner, kigo, konquest, kreversi, ksnakeduel, kspaceduel, ksudoku, kubrick, lskat, and umbrello. Thanks to the hard-working developers who volunteered their time and work to make that happen.");?>
</p>

<h3 style="clear:both;" ><?php print i18n_var("What's new in KDE Applications 17.08");?></h3>

<h4 style="clear:both;" ><a href="https://userbase.kde.org/Dolphin"><?php print i18n_var("Dolphin");?></a></h3>
<p align="justify">
<?php print i18n_var("Dolphin developers report that Dolphin now shows 'Deletion Time' in the Trash, and shows 'Creation Time' if the OS supports it such as on BSDs."); ?>
</p>

<h4 style="clear:both;" ><?php print i18n_var("KIO-Extras");?></h3>
<p align="justify">
<?php print i18n_var("Kio-Extras now provides better support for Samba shares."); ?>
</p>

<h4 style="clear:both;" ><a href="https://www.kde.org/applications/education/kalgebra/"><?php print i18n_var("KAlgebra");?></a></h3>
<p align="justify">
<?php print i18n_var("The KAlgebra developers have worked on improving their Kirigami front-end on the desktop, and implemented code completion."); ?>
</p>

<h4 style="clear:both;" ><a href="https://userbase.kde.org/Kontact"><?php print i18n_var("Kontact");?></a></h3>
<p align="justify">
<ul>
<li><?php print i18n_var("In KMailtransport, developers have reactivated akonadi transport support, created plugins support, and recreated sendmail mail transport support."); ?></li>
<li><?php print i18n_var("In SieveEditor, a lot of bugs in autocreate scripts have been fixed and closed. Along with general bug-fixing, a regexp editor line-editor has been added."); ?></li>
<li><?php print i18n_var("In KMail the ability to use an external editor has been recreated as a plugin."); ?></li>
<li><?php print i18n_var("The Akonadi-import-wizard now has the 'convert all converter' as a plugin, so that developers can create new converters easily."); ?></li>
<li><?php print i18n_var("Applications now depend on Qt 5.7. Developers have fixed a lot of compile errors on Windows. All of kdepim does not compile on Windows yet but the developers have made big progress. To begin, the developers created a craft-recipe for it. Much bug fixing has been done to modernize code (C++11). Wayland support on Qt 5.9. Kdepim-runtime adds a facebook resource."); ?></li>
</ul>
</p>

<h4 style="clear:both;" ><a href="https://kdenlive.org/"><?php print i18n_var("Kdenlive");?></a></h3>
<p align="justify">
<?php print i18n_var("In Kdenlive, the team fixed the broken 'Freeze effect'. In recent versions, it was impossible to change the frozen frame for the freeze effect. Now a keyboard shortcut for Extract Frame feature is allowed. Now the user can save screenshots of their timeline with a keyboard shortcut, and a name is now suggested based on frame number <a href='https://bugs.kde.org/show_bug.cgi?id=381325'>https://bugs.kde.org/show_bug.cgi?id=381325</a>. Fix downloaded transition lumas do not appear in interface: <a href='https://bugs.kde.org/show_bug.cgi?id=382451'>https://bugs.kde.org/show_bug.cgi?id=382451</a>. Fix audio clicks issue (for now, requires building the dependency MLT from git until a MLT release): <a href='https://bugs.kde.org/show_bug.cgi?id=371849'>https://bugs.kde.org/show_bug.cgi?id=371849</a>."); ?>
</p>

<h4 style="clear:both;" ><a href="https://www.kde.org/applications/system/krfb/"><?php print i18n_var("Krfb");?></a></h3>
<p align="justify">
<?php print i18n_var("Developers have finished porting the X11 plugin to Qt5, and krfb is working again using a X11 backend that is much faster than the Qt plugin. There's a new Settings page, allowing the user to change preferred framebuffer plugin."); ?>
</p>

<h4 style="clear:both;" ><a href="https://www.kde.org/applications/system/konsole/"><?php print i18n_var("Konsole");?></a></h3>
<p align="justify">
<?php print i18n_var("Konsole now allows unlimited scrollback to extend past 2GB (32bit) limit. Now Konsole allows users to enter any location to store scrollback files. Also, a regression was fixed, Konsole can once again allow KonsolePart to call the Manage Profile dialog."); ?>
</p>

<h4 style="clear:both;" ><a href="https://www.kde.org/applications/development/kapptemplate/"><?php print i18n_var("KAppTemplate");?></a></h3>
<p align="justify">
<?php print i18n_var("In KAppTemplate there is now an option to install new templates from the filesystem. More templates have been removed from KAppTemplate and instead integrated  into related products; ktexteditor plugin template and kpartsapp template (ported to Qt5/KF5 now) have become part of KDE Frameworks KTextEditor and KParts since 5.37.0. These changes should simplify creation of templates in KDE applications."); ?>
</p>

<h3 style="clear:both;" ><?php print i18n_var("Bug Stomping");?></h3>
<p align="justify">
<?php print i18n_var("More than 80 bugs have been resolved in applications including the Kontact Suite, Ark, Dolphin, K3b, Kdenlive, KGpg, Konsole and more!");?>
</p>

<h3 style="clear:both;"><?php print i18n_var("Full Changelog");?></h3>

<p align="justify">
<?php print i18n_var("If you would like to read more about the changes in this release, <a href='%1'>head over to the complete changelog</a>. Although a bit intimidating due to its breadth, the changelog can be an excellent way to learn about KDE's internal workings and discover apps and features you never knew you had.", "fulllog_applications.php?version=".$version);?>
</p>

<h4>
<?php i18n("Spread the Word");?>
</h4>
<p align="justify">
<?php print i18n_var("Non-technical contributors play an important part in KDE's success. While proprietary software companies have huge advertising budgets for new software releases, KDE often depends on word of mouth."); ?>
</p>

<p align="justify">
<?php print i18n_var("There are many ways to support the KDE Applications 17.08 release: you can report bugs, encourage others to join the KDE Community, or <a href='%1'>support the non-profit organization behind the KDE Community</a>.", "https://relate.kde.org/civicrm/contribute/transact?reset=1&id=5"); ?>
</p>

<p align="justify">
<?php i18n("Help us spread the word on the social web. You can submit stories to news sites like Reddit, Facebook and Twitter; upload screenshots of your new set-up to services like Snapchat, Instagram and Google+; or create screencasts and upload them to YouTube, Blip.tv, and Vimeo, or stream them live over Twitch!"); ?>
</p>

<p align="justify">
<?php i18n("Remember to tag your posts and uploaded materials with the <i>KDE</i> moniker, as this makes them easier to find and gives the KDE Promo Team a way to analyze coverage for the KDE Applications 17.08 release."); ?>
</p>

<!-- // Boilerplate again -->

<h4>
  <?php i18n("Installing KDE Applications 17.08 Binary Packages");?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php i18n("Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 17.08 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.");?>
</p>

<p align="justify">
  <a name="package_locations"></a><em><?php i18n("Package Locations");?></em>.
  <?php i18n("For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_Applications/Binary_Packages'>Community Wiki</a>.");?>
</p>

<h4>
  <?php i18n("Compiling KDE Applications 17.08");?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php i18n("The complete source code for KDE Applications 17.08 may be <a href='http://download.kde.org/stable/applications/17.08.0/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications-17.08.0.php'>KDE Applications 17.08.0 Info Page</a>.");?>
</p>

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
