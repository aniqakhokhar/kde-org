<?php
  $page_title = "KDE 1.1.1 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<TABLE WIDTH="600"><TR><TD>
<br><br>

                  <h1 align="center">K Desktop Environment 1.1.1 Ships</h1>

<pre>


FOR IMMEDIATE RELEASE


                  K DESKTOP ENVIRONMENT 1.1.1 SHIPS
  New Release of Award-Winning GNU/Linux Desktop GUI Increases Stability and
                          Ease -of-Use

 New York - May 3, 1999 - The K Desktop Environment&reg; (KDE) Team
(<a href="http://www.kde.org">http://www.kde.org</a>) today announced the shipment of KDE 1.1.1, the
latest stable  release of GNU/Linux's  most advanced  open source desktop
environment.

By offering an elegant, intuitive and familiar interface marked by
ease-of-use comparable to the popular  -  but  proprietary  -  Windows
interfaces, KDE 1.1.1 unleashes the power, freedom and potential of
GNU/Linux for the typical home or office user.  In recognition of KDE's
usability, KDE was recently awarded the Ziff-Davis&reg; "Innovation of the
year 1998/1999" award in the Software category.

<i>"Congratulations to the KDE team on a super job with the new release,"</i>
said Dr. Michael Cowpland, president and CEO of Corel&reg; Corporation
(NASDAQ: COSFF, TSE:COS).  <i>"The KDE project is doing a terrific job of
bringing GNU/Linux to the desktop which is why we're basing the GUI for our
desktop Linux distribution on KDE. We look forward to the improvements
in KDE 1.1.1."</i>

<i>"KDE 1.1.1 continues KDE's leadership and widens its lead over other
GNU/Linux desktop alternatives by establishing new desktop standards in
stability, ease-of-use and customization,"</i> said Ransom Love, President
of Caldera&reg; Systems, Inc.  <i>"We chose KDE for OpenLinux&reg; because it's
suitable for business and mainstream use, and the Ziff-Davis innovation
award underscores that choice."</i>

Virtually all Linux distributors include KDE in their distributions,
making it the de facto desktop standard for GNU/Linux.

KDE 1.1.1 builds on the well-received release of KDE 1.1. providing its
users with increased stability and ease-of-use .It incorporates many
user suggestions and provides a number of enhancements, including:

        *  improved integration between korganizer, a sophisticated
            calendaring and scheduling program, and kpilot, a Palm-pilot
           	synchronization tool;
        *  improved Internet connectivity; and
        *  increased stability and additional features in a range of
           desktop components.


------------------------------------------------------------------------


KDE 1.1.1 is available for free download  from numerous mirrors (see
<a href="http://www.kde.org/mirrors.html">http://www.kde.org/mirrors.html</a>) and also from KDE's primary ftp server
(<a href="ftp://ftp.kde.org/pub/kde/stable/1.1.1/distribution/">ftp://ftp.kde.org/pub/kde/stable/1.1.1/distribution/</a>).  It will also be
available in most future GNU/Linux and *BSD distributions.

User support for KDE is provided by Linux vendors who include KDE in
their distributions.  Users may also receive free support from an
international group of KDE aficionados by subscribing to the KDE mailing
lists. Please visit <a href="http://www.kde.org/contact.html">http://www.kde.org/contact.html</a> for more
information.

ABOUT KDE

KDE is a collaborative project by hundreds of developers worldwide to
create a sophisticated, customizable and stable desktop environment
employing a network-transparent, intuitive user interface.  In addition,
many KDE users assisted in the preparation of the 1.1.1 release by
providing constructive feedback, suggestions and software patches.  KDE
is working proof of the power of the open source software development
model.
For more information about KDE, please visit
<a href="http://www.kde.org/whatiskde/">http://www.kde.org/whatiskde/</a>.

Editor's note
To contact those quoted in the article:
Caldera Systems, Inc.
Ransom Love
(801) 765-4999 ext. 304

Corel
Judith O'Brien
(613) 728-0826 ext. 5405

Press ONLY Contacts:

United States:
Andreas Pour
(718) 456-1165
<a href="ma&#x69;&#x6c;t&#111;&#58;p&#111;ur&#x40;kd&#x65;&#46;&#x6f;r&#x67;">po&#0117;&#x72;&#64;&#x6b;d&#x65;&#46;&#111;&#x72;&#103;</a>

Europe:
Martin Konold
49.7071.2978653
<a href="mailto:konold@alpha.tat.physik.uni-tuebingen.de">konold@alpha.tat.physik.uni-tuebingen.de</a>



</pre>

</TD></TR></TABLE>

<?php include "footer.inc" ?>
