<?php
  $page_title = "KDE 3.0-alpha1 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>
<p>FOR IMMEDIATE RELEASE</p>
<h3 align="center">Third Generation KDE Desktop Ready for Developers</h3>
<p><strong>KDE Ships Alpha of Third Generation of the Leading Linux Desktop for Developers</strong></p>
<p>October 5, 2001 (The INTERNET).
The <a href="http://www.kde.org/">KDE
Project</a> today announced the immediate release of KDE 3.0alpha1,
the third generation of KDE's free, powerful and
easy-to-use free Internet-enabled desktop for Linux and other UNIXes.
KDE 3.0 is scheduled for its first beta release
this December and for final release in late February 2002.
</p>
<p>
This inaugural release of the KDE 3, which follows two weeks after the
stable release of KDE 2.2.1 series, is based on
<a href="http://www.trolltech.com">Trolltech</a>'s Qt 3.0.0beta6.
It ships with the core KDE libraries, the core
desktop environment, and over 100 applications from the other
base KDE packages (administration, multimedia, network, PIM,
utilities, etc.).
</p>
<p>
The primary goal of the 3.0alpha1 release is to provide a framework for
developers to start porting their KDE 2 applications to KDE 3 and to
solicit developer feature contributions and feature requests before the
KDE 3 API is frozen for binary compatibility.
In addition, experimental KDE users who would like to try this
release can set up a KDE 3 system side-by-site with a KDE 2 system.
Instructions for doing so are
<a href="http://www.kde.org/kde2-and-kde3.html">available here</a>.
</p>
<p>
Additional information about KDE 3 is available at the KDE <a href="http://www.kde.org/">website</a>, including a tentative
<a href="http://developer.kde.org/development-versions/kde-3.0-release-plan.html">release
plan</a>, a KDE 3 <a href="http://www.kde.org/info/3.0.html">info page</a>,
and a list of <a href="http://developer.kde.org/development-versions/kde-3.0-features.html">planned features</a>.
</p>
<h5>Improvements</h5>
<p>
For both developers and users, KDE 3 offers substantial improvements
and additions compared to KDE 2 (the great bulk of which are, at this
juncture, due to the switch to Qt 3):
</p>
<p>
For the developer:
</p>
<dl>
<dt><em>Database access</em>.</dt>
<dd>KDE 3 provides a database-independent
API for accessing SQL databases. It provides support for ODBC as well as direct
support for Oracle, PostgreSQL and MySQL databases (custom drivers may
be added as well).</dd>
<dt><em>Data-aware widgets</em>.</dt>
<dd>New database-aware controls provide
automatic synchronization between the GUI and the database.</dd>
<dt><em>RAD Development</em>.</dt>
<dd>A greatly improved Qt Designer
now supports interactive construction of the application main windows
with menus and tool bars in addition to dialogs.  It supports KDE, Qt and
custom widgets, including preview, and can be used in conjunction
with KDevelop.</dd>
<dt><em>Regular expressions</em>.</dt>
<dd>KDE 3 features a new and
powerful regular expression engine.  While compatible with, and as powerful
as, Perl regular expressions, the Qt regular expression classes additionally
provide full support for international (Unicode) character sets.</dd>
<dt><em>Internationalization</em>.</dt>
<dd>The addition of Qt Linguist
as an alternative to KBabel.  Qt Linguist allows users to convert KDE-based
programs from one language to another seamlessly, simply and intelligently.
Qt Linguist helps with the translation of all visible text in a program,
to and from any language supported by Unicode (including Unicode 3),
and can be used in conjunction with KDevelop.</dd>
</dl>
<p>
For everyone:
</p>
<dl>
<dt><em>International text support</em>.</dt>
<dd>KDE 3 offers radically
improved support for displaying non-Latin alphabets.  In addition, characters
of different character sets may be freely mixed in the same text, even
without Unicode fonts installed.</dd>
<dt><em>Bidirectional language support</em>.</dt>
<dd>KDE 3 provides full
support for right-to-left and bidirectional languages, such as Arabic and
Hebrew.</dd>
<dt><em>Multi-monitor support</em>.</dt>
<dd>KDE 3 provides support for
both Xinerama and the traditional multi-screen technology.</dd>
<dt><em>KDE/Qt Integration</em>.</dt>
<dd>KDE 3 improves the integration
of pure Qt applications into KDE by applying the KDE 3.x widget style plugins
to pure Qt applications.  Pure Qt applications thus largely achieve the
KDE look and feel.  In addition, the Qt style engine has been extended
to support a wider range of standard widgets, including progress bars,
spin boxes, and table headers.</dd>
<dt><em>Hardware accelerated alpha blending</em>.</dt>
<dd>This features, among other things, makes disabled icons look nice.</dd>
<dt><em>HTTP improvements</em>.</dt>
<dd>The HTTP kio-slave is going to support HTTP pipelining, which provides much faster
downloading of web sites containing numerous images.</dd>
</dl>
<p>
Most of these improvements result directly from the switch to Qt 3, which
has been the focus of KDE 3 code development so far.
Improvements to the KDE libraries and applications themselves are planned for
the successive beta releases leading to the first stable KDE 3.  A list
of these planned features is available
<a href="http://developer.kde.org/development-versions/kde-3.0-features.html">here</a>.
</p>
<h5>Porting to KDE 3</h5>
<p>
Since KDE 3 is mostly source compatible with KDE 2, porting applications
from KDE 2 to KDE 3 can usually be done surprisingly quickly.
The process is <em>substantially</em> easier than it was for porting
from KDE 1 to KDE 2, and even very complicated applications can be ported
in a matter of a few hours.
</p>
<p>
Instructions for porting KDE 2 applications to KDE 3 are available
separately for the
<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE3PORTING.html">KDE
libraries</a> and the
<a href="http://doc.trolltech.com/3.0/porting.html">Qt libraries</a>.
Most of the changes required for the port applications pertain to changes
in the Qt API.  Although neither the KDE 3 nor the Qt 3
APIs are frozen, few changes are anticipated for the final releases
of KDE 3.0 and Qt 3.0.0, respectively. The KDE team will provide
a mailinglist for porting questions soon.
</p>
<h5>Downloading and Compiling KDE 3.0alpha1</h5>
<p>
KDE and all its components (including
<a href="http://www.kdevelop.org/">KDevelop</a> and
<a href="http://www.koffice.org/">KOffice</a>) are available
<em><strong>for free</strong></em> under Open Source licenses from the KDE
<a href="http://download.kde.org/">ftp server</a>
and its <a href="http://www.kde.org/ftpmirrors.html">mirrors</a> and can
also be obtained on <a href="http://www.kde.org/cdrom.html">CD-ROM</a>.
</p>
<p>
<a name="source_code-library_requirements"></a><em>Library
Requirements</em>.
KDE 3.0alpha1 requires qt-3.0.0beta6, which is available in source code
from Trolltech as
<a href="ftp://ftp.trolltech.com/qt/source/qt-x11-free-3.0.0-beta6.tar.gz">qt-x11-free-3.0.0-beta6.tar.gz</a>, as well as libxml2 &gt;= 2.3.13,
available <a href="ftp://speakeasy.rpmfind.net/pub/libxml/">here</a>.
</p>
<p>
<em>Compiler Requirements</em>.
Please note that some components of
KDE 3.0alpha1 will not compile with older versions of
<a href="http://gcc.gnu.org/">gcc/egcs</a>, such as egcs-1.1.2 or
gcc-2.7.2.  At a minimum gcc-2.95-* is required.  In addition, some
components of KDE 3.0alpha1 (such as the multimedia backbone of KDE,
<a href="http://www.arts-project.org/">aRts</a>) will not compile with
<a href="http://gcc.gnu.org/gcc-3.0/gcc-3.0.html">gcc</a> 3.0 or 3.0.1,
though the forthcoming gcc 3.0.2 release will most likely work.
</p>
<p>
<a name="source_code"></a><em>Source Code</em>.
The souce code for KDE 3.0alpha1 is no longer available. Please consider
using a newer KDE release. 
</p>
<p>
<em>Further Information</em>.  For further
instructions on compiling and installing KDE 3.0alpha1, please consult
the <a href="http://www.kde.org/install-source.html">installation
instructions</a> and, if you should encounter problems, the
<a href="http://www.kde.org/compilationfaq.html">compilation FAQ</a>.
</p>
<h5>About KDE</h5>
<p>
KDE is an independent, collaborative project by hundreds of developers
worldwide working over the Internet to create a sophisticated,
customizable and stable desktop environment employing a component-based,
network-transparent architecture.  KDE provides a stable, mature desktop,
an office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
set of networking and administration tools, and an efficient and intuitive
development environment, including an excellent IDE
(<a href="http://www.kdevelop.org/">KDevelop</a>).
KDE is working proof of the power of
the Open Source "Bazaar-style" software development model to create
first-rate technologies on par with and superior to even the most complex
commercial software.
</p>
<p>
Please visit the KDE family of web sites for the
<a href="http://www.kde.org/faq.html">KDE FAQ</a>,
<!-- <a href="http://www.kde.org/screenshots/kde2shots.html">screenshots</a>,-->
<a href="http://www.koffice.org/">KOffice information</a> and
<a href="http://developer.kde.org/documentation/kde3arch.html">developer
information</a>.
Much more <a href="http://www.kde.org/whatiskde/">information</a>
about KDE is available from KDE's
<a href="http://www.kde.org/family.html">family of web sites</a>.
</p>
<h5>Corporate KDE Sponsors</h5>
<p>
Besides the valuable and excellent efforts by the
<a href="http://www.kde.org/gallery/index.html">KDE developers</a>
themselves, significant support for KDE development has been provided by
<a href="http://www.mandrakesoft.com/">MandrakeSoft</a> and
<a href="http://www.suse.com/">SuSE</a>.  Thanks!
</p>
<hr noshade="noshade" size="1" width="90%" align="center" />
<em>Trademarks Notices.</em>
<p>
KDE, K Desktop Environment and KOffice are trademarks of KDE e.V.
Linux is a registered trademark of Linus Torvalds.
Unix and Motif are registered trademarks of The Open Group.
Trolltech and Qt are trademarks of Trolltech AS.
Netscape Communicator is a trademark or registered trademark of Netscape Communications Corporation in the United States and other countries.
Java is a trademark of Sun Microsystems, Inc.
All other trademarks and copyrights referred to in this announcement are the property of their respective owners.
</p>
<hr noshade="noshade" size="1" width="90%" align="center"/>
<table border="0" cellpadding="8" cellspacing="0">
<tr><th colspan="2" align="left">
Press Contacts:
</th></tr>
<tr valign="top"><td align="right" nowrap="nowrap">
United&nbsp;States:
</td><td nowrap="nowrap">
Eunice Kim<br />
The Terpin Group<br />
ekim@terpin.com<br />
(1) 650 344 4944 ext. 105<br />&nbsp;<br />
Kurt Granroth<br />
&#103;&#x72;an&#114;o&#x74;h&#x40;&#00107;d&#x65;&#x2e;&#x6f;r&#0103;<br />
(1) 480 732 1752<br />&nbsp;<br />
Andreas Pour<br />
KDE League, Inc.<br />
p&#111;&#00117;r&#64;&#107;&#100;&#0101;.org<br />
(1) 917 312 3122
</td></tr>
<tr valign="top"><td align="right" nowrap="nowrap">
Europe (French and English):
</td><td nowrap="nowrap">
David Faure<br />
f&#97;&#0117;&#x72;e&#0064;&#107;&#x64;e&#46;org<br />
(44) 1225 837409
</td></tr>
<tr valign="top"><td align="right" nowrap="nowrap">
Europe (English and German):
</td><td>
Ralf Nolden<br />
n&#x6f;&#x6c;&#0100;e&#110;&#x40;kd&#101;.o&#x72;g<br />
(49) 2421 502758
</td></tr>
</table>
<!-- $Id: announce-3.0alpha1.php 523083 2006-03-27 11:22:27Z scripty $ -->
<?php
  include "footer.inc"
?>
