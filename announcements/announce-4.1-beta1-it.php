<?php<?php
  $page_title = "Annuncio rilascio KDE 4.1 beta1";
  $site_root = "../";
  include "header.inc";
?>

<p>PER IL RILASCIO IMMEDIATO</p>

Pagina disponibile in:
<?php
  $release = '4.1-beta1';
  include "announce-i18n-bar.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
  Disponibile la prima versione Beta di KDE 4.1
</h3>

<p align="justify">
  <strong>

La comunità KDE è lieta di annunciare il primo rilascio Beta per KDE 4.1</strong>
</p>

<p align="justify">
27 Maggio 2008 (INTERNET).  
Il progetto <a href="http://www.kde.org/">KDE</a> è lieto di annunciare il rilascio della prima versione Beta per KDE 4.1. Questa Beta 1 è indirizzata a coloro i quali desiderino provare in anteprima KDE 4.1 e a chi volesse contribuire aiutando a scovare bug, regressioni e altri problemi in modo che la versione 4.1 possa finalmente sostituire la 3.5 sui Desktop degli utenti. KDE 4.1 Beta 1 è disponibile in formato binario per una gran numero di piattaforme e, naturalmente anche sotto forma di codice sorgente. La versione finale di KDE 4.1 è attesa per Luglio 2008.
</p>


<h4>
  <a name="changes">I dettagli di KDE 4.1 Beta 1</a>
</h4>

<p align="justify">
<ul>
    <li>Miglioramento generale delle funzionalità e della configurabilità del Desktop
    </li>
    <li>L'insieme dei programmi "KDE Personal Information Management" introdotta nel rilascio 
    </li>
    <li>Molte nuove applicazioni o nuove traduzioni dalla vecchia infrastruttura
    </li>
</ul>
</p>

<h4>
  Plasma cresce
</h4>
<p align="justify">
Plasma, l'innovativa infrastruttura di menu, pannelli e del Desktop stesso, cresce rapidamente. Ora supporta molteplici, configurabili, pannelli che permettono di impostare il Desktop a seconda dei propri gusti. Il menu delle applicazioni, Kickoff, è stato ripulito e migliorato. La finestra per l'esecuzione rapida di programmi è stata rivista e migliorata, permettendo agli utenti avanzati di eseguire applicazioni, aprire documenti ed indirizzi di rete. Le prestazioni degli effetti grafici delle finestre sono state migliorate e sono stati aggiunti effetti per migliorare l'usabilità incluso il selettore di finestre "Cover Switch" e l'obbligatorio "Finestre Gommose".
</p>
<div>
		<div style='float:left;'>
				<a href="announce_4.1-beta1/plasma-krunner.png">
						<img width="310" height="235" src="announce_4.1-beta1/plasma-krunner-small.png" title="il nuovo KRunner" alt="The new KRunner alt-F2 dialog"/>
				</a>
		</div>
		<div style='float:left;'>
				<a href="announce_4.1-beta1/plasma-panelcontroller.png">
						<img width="462" height="84" src="announce_4.1-beta1/plasma-panelcontroller-small.png" title="La gestione dei pannelli ritorna" alt="Panel management returns"/>
				</a>
		</div>
		<div style='float:left;'>
				<a href="announce_4.1-beta1/kwin-coverswitch.png">
						<img width="462" height="347" src="announce_4.1-beta1/kwin-coverswitch-small.png" title="L'effetto Cover Switch" alt="KWin's Cover Switch effect"/>
				</a>
		</div>
		<div style='float:left;'>
				<a href="announce_4.1-beta1/kwin-wobbly1.png">
						<img width="462" height="347" src="announce_4.1-beta1/kwin-wobbly1-small.png" title="Finestre Gommose" alt="Windows that wobble"/>
				</a>
		</div>
		<div style='float:left;'>
				<a href="announce_4.1-beta1/kwin-wobbly2.png">
						<img width="462" height="347" src="announce_4.1-beta1/kwin-wobbly2-small.png" title="Altre finestre gommose" alt="More wobbling windows"/>
				</a>
		</div>
		<div style="clear:both;"></div>
</div>
<h4>
  Il ritorno di Kontact
</h4>
<p align="justify">
Kontact, il gestore delle informazioni personali di KDE, e tutti gli strumenti correlati sono ora disponibili nella versione per KDE 4.1. Molte delle funzioni delle precedenti versioni enterprise sono state incorporate, rendendo Kontact adatto anche alle esigenze dell'ufficio. I miglioramenti includono nuovi componenti come KtimeTracker e KJots, un'applicazione per raccogliere le note. Un nuovo e gradevole aspetto, miglior supporto per più calendari e fusi orari ed una ancora più robusta gesitione delle email.</p>
<center>
<a href="announce_4.1-beta1/kontact-calendar.png">
<img width="351" height="275" src="announce_4.1-beta1/kontact-calendar-small.png" title="Molteplici calendari in uso" alt="Molteplici calendari in uso"/>
</a>
<a href="announce_4.1-beta1/kontact-kjots.png">
<img width="351" height="271" src="announce_4.1-beta1/kontact-kjots-small.png" title="KJots outliner" alt="The KJots outliner"/>
</a>
</center>

<h4>
Aumento delle applicazioni disponibili per KDE 4
</h4>
<p align="justify">
Grazie alla comunità KDE molte applicazioni sono state riscritte per la nuova infrastruttura di KDE 4, molte altre sono state migliorate da quando kde 4 è stato rilasciato. Dragon Player, il nuovo, leggerissimo lettore multimediale, è al suo debutto. Torna il lettore CD di KDE. Una nuova interfaccia di stampa fornisce nuove capacità di stampa e flessibilità per il Desktop Libero. Konqueror ha ottenuto la capacità di salvare delle sessioni per la navigazione, una modalità di annullamento, un avanzamento di pagina più lineare. Una modalità a schermo intero di navigazione delle immagini arriva su Gwenview. Dolphin, il gestore di file, ottiene la visualizzazione a schede e altre funzionalità molto apprezzate dagli utenti di KDE 3, incluso "Copia in.." e una visualizzazione migliorata della gerarchia delle cartelle. Molte applicazioni, inclusi il Desktop e le applicazioni per l'apprendimento, mettono a disposizione novità come icone, temi, mappe e materiale per le lezioni attraverso la nuova infrastruttura "Scarica le novità", tramite la sua nuova interfaccia. Zeroconf è stato aggiunto a molti giochi e programmi rendendo possibile configurare i giochi e l'accesso remoto in maniera indolore.
</p>
<div>
		<div style='float:left;'>
				<a href="announce_4.1-beta1/dolphin-treeview.png">
						<img width="265" height="234" src="announce_4.1-beta1/dolphin-treeview-small.png" title="L'albero delle cartelle in Dolphin" alt="L'albero delle cartelle in Dolphin"/>
				</a>
		</div>
		<div style='float:left;'>
				<a href="announce_4.1-beta1/dragonplayer.png">
						<img width="442" height="317" src="announce_4.1-beta1/dragonplayer-small.png" title="Dragon media player" alt="Dragon media player"/>
				</a>
		</div>
		<div style='float:left;'>
				<a href="announce_4.1-beta1/games-kdiamond.png">
						<img width="264" height="305" src="announce_4.1-beta1/games-kdiamond-small.png" title="Il rompicapo KDiamond" alt="Il rompicapo KDiamond"/>
				</a>
		</div>
		<div style='float:left;'>
				<a href="announce_4.1-beta1/games-kubrick.png">
						<img width="215" height="200" src="announce_4.1-beta1/games-kubrick-small.png" title="Gli anni 80 sul tuo Desktop!" alt="Gli anni 80 sul tuo Desktop!"/>
				</a>
		</div>
		<div style='float:left;'>
				<a href="announce_4.1-beta1/konqueror.png">
						<img width="302" height="294" src="announce_4.1-beta1/konqueror-small.png" title="Konqueror, il browser" alt="Konqueror, il browser"/>
				</a>
		</div>
		<div style='float:left;'>
				<a href="announce_4.1-beta1/marble-openstreetmap.png">
						<img width="324" height="281" src="announce_4.1-beta1/marble-openstreetmap-small.png" title="Marble in visualizzazione OpenStreetMaps" alt="Marble in visualizzazione OpenStreetMaps"/>
				</a>
		</div>
		<div style="clear:both;"></div>
</div>
<h4>
Miglioramenti dell'infrastruttura
</h4>
<p align="justify">
Gli sviluppatori sono stati molto impegnati nell'arricchire le librerie e l'infrastruttura alla base di KDE.  KHTML ora è molto più veloce grazie al precaricamento delle risorse, WebKit, un suo derivato, è stato aggiunto a Plasma per dargli la capacità di poter caricare i widget di Mac OS X. L'uso della nuova funzionalità Widget on Canvas di Qt 4.4 ha donato a Plasma stabilità e leggerezza. La classica interfaccia a click singolo di KDE ha ora un nuovo metodo di selezione che promette accessibilità e velocità. Phonon, l'infrastruttura multimediale indipendente dalla piattaforma, ha ora il supporto ai sottotitoli e il supporto ai motori GStreamer, DirectShow 9 e QuickTime. La gestione della rete è stata ampliata per essere compatibile con più versioni di NetworkManager. E consapevoli delle molte diversità presenti nei Desktop liberi sono stati fatti degli sforzi per iniziare a migliorare l'integrazione  come il supporto alle specifiche per le notifiche attraverso i popup e i segnalibri per il Desktop di freedesktop.org, così che le applicaioni scritte per altri ambienti Desktop possano integrarsi al meglio in una sessione di KDE 4.1.
</p>

<h4>
  KDE 4.1 - Rilascio Finale
</h4>
<p align="justify">
KDE 4.1 è pianificato per essere rilasciato il 29 Luglio 2008. Questo rilascio avviene esattamente sei mesi dopo il rilascio di KDE 4.0.
</p>

<h4>
  Scaricalo, avvialo, provalo
</h4>
<p align="justify">
I volontari della comunità e i distributori di sistemi Linux/UNIX hanno gentilmente messo a disposizione pacchetti precompilati per KDE 4.0.80 (4.1 Beta 1) per molte distribuzioni Linux, Mac OS x e Windows. Considerate il fatto che questi pacchetti non sono da ritenersi per un utilizzo quotidiano. Consultate il gestore di pacchetti della vostra distribuzione oppure seguite i link di seguito per istruzioni dettagliate:</p>

<ul>
<li><a href="http://fedoraproject.org/wiki/Releases/Rawhide"></a>Fedora</li>
<li><em>Debian</em> ha KDE 4.1beta1 in <em>experimental</em>.</li>
<li><em>Kubuntu</em> è in fase di preparazione dei pacchetti.</li>
<li><a href="http://wiki.mandriva.com/en/2008.1_Notes#Testing_KDE_4">Mandriva</a></li>
<li><a href="http://en.opensuse.org/KDE4#KDE_4_UNSTABLE_Repository_--_Bleeding_Edge">openSUSE</a></li>
<li><a href="http://techbase.kde.org/Projects/KDE_on_Windows/Installation">Windows</a></li>
<li><a href="http://mac.kde.org/">Mac OS X</a></li>
</ul>

<h4>
  Compiliare KDE 4.1 Beta 1 (4.0.80)
</h4>
<p align="justify">
  <a name="source_code"></a><em>Source Code</em>.
  il codice sorgente completo di KDE 4.0.80 può essere<a
  href="http://www.kde.org/info/4.0.80.php">liberamente scaricato</a>.
Istruzioni per la compilazione e l'installazione di KDE 4.0.80
si trovano nella <a href="/info/4.0.80.php">KDE 4.0.80 Info
  Page</a>, oppure su <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>.
</p>

<h4>
 Supporta KDE
</h4>
<p align="justify">
 KDE è un progetto di <a href="http://www.gnu.org/philosophy/free-sw.html">Software Libero</a> che esiste e cresce solamente grazie all'aiuto di molti volontari che donano il loro tempo e impegno. KDE è sempre alla ricerca di nuovi volontari contribuenti che possano dare una mano in settori come la programmazione, la ricerca di bachi, la scrittura della documentazione, la traduzione, la promozione, con donazioni, etc. Tutti i contributi sono apprezzati e accettati. Per favore leggi la pagina<a href="http://www.kde.org/community/donations/">Supporting KDE</a> per informazioni. </p>

<p align="justify">
Non vediamo l'ora di avere tue notizie!
</p>

<h2>A proposito di KDE 4</h2>
<p>
KDE 4.0 è un innovativo desktop di Software Libero che contiene molte applicazioni per un utilizzo quotidiano come anche per applicazioni specifiche. Plasma è un nuovo gestore per il desktop sviluppato per KDE 4, fornisce un'interfaccia intuitiva per l'interazione con il desktop e con le applicazioni. Konqueror, il web browser, integra il web con il Desktop. Dolphin, il gestore file, Okular, il visualizzatore dei documenti, e le Impostazioni di Sistema forniscono un desktop di base.
<br />
KDE è sviluppato sulle omonime librerie, che forniscono facilità di accesso alle risorse sulla rete con KIO e avanzate capacità grafiche grazie alle Qt4. Phonon e Solid, che sono ugualmente parte delle librerie di KDE, aggiungono un'infrastruttura multimediale e una migliore integrazione dell'hardware con le applicazioni scritte per KDE.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
