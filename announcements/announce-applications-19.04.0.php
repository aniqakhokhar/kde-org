<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  require('../aether/config.php');

  $pageConfig = array_merge($pageConfig, [
      'title' => i18n_noop("KDE Ships KDE Applications 19.04.0"),
      'cssFile' => '/css/announce.css'
  ]);

  require('../aether/header.php');
  $site_root = "../";
  $release = 'applications-19.04.0';
  $version_text = "19.04";
  $version_number = "19.04.0";
?>

<script src="/js/use-ekko-lightbox.js" defer></script>

<main class="releaseAnnouncment container">

<h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("Applications %1", $version_text)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<figure class="videoBlock">
      <iframe width="560" height="315" src="https://www.youtube.com/embed/1keUASEvIvE" allowfullscreen='true'></iframe>
</figure>

<p>
<?php i18n("April 18, 2019.")?>
<br />
<?php print i18n_var("The KDE community is happy to announce the release of KDE Applications %1.", $version_text);?>
</p>

<p>
<?php print i18n_var("Our community continuously works on improving the software included in our KDE Application series. Along with new features, we improve the design, usability and stability of all our utilities, games, and creativity tools. Our aim is to make your life easier by making KDE software more enjoyable to use. We hope you like all the new enhancements and bug fixes you'll find in %1!", $version_text);?>
</p>

<h2><?php print i18n_var("What's new in KDE Applications %1", $version_text);?></h2>

<p>
<?php i18n("More than 150 bugs have been resolved. These fixes re-implement disabled features, normalize shortcuts, and solve crashes, making KDE Applications friendlier and allowing you to be more productive.");?>
</p>
<p>
<?php print i18n_var("If you would like to read more about the changes in this release, <a href='%1'>head over to the complete changelog</a>, which is an excellent way to learn about KDE's internal workings and discover apps and features you never knew you had.", "fulllog_applications-aether.php?version=".$version_number);?>
</p>

<h3 style="clear:both;"><?php i18n("File Management")?></h3>

<figure style="float: right; margin: 0px"><a href="app1904_dolphin01.png" data-toggle="lightbox"><img src="app1904_dolphin01_small.png" width="250" /></a></figure>
<p>
<?php print i18n_var("<a href='%1'>Dolphin</a> is KDE's file manager. It also connects to network services, such as SSH, FTP, and Samba servers, and comes with advanced tools to find and organize your data.", "https://www.kde.org/applications/system/dolphin/");?>
</p>
<p>
<?php i18n("New features:")?>
<ul>
<li><?php print i18n_var("We have expanded thumbnail support, so Dolphin can now display thumbnails for several new file types: <a href='%1'>Microsoft Office</a> files, <a href='%2'>.epub and .fb2 eBook</a> files, <a href='%3'>Blender</a> files, and <a href='%4'>PCX</a> files. Additionally, thumbnails for text files now show <a href='%5'>syntax highlighting</a> for the text inside the thumbnail.", "https://phabricator.kde.org/D18768", "https://phabricator.kde.org/D18738", "https://bugs.kde.org/show_bug.cgi?id=246050", "https://phabricator.kde.org/D19679", "https://phabricator.kde.org/D19432");?></li>
<li><?php print i18n_var("You can now choose <a href='%1'>which split view pane to close</a> when clicking the 'Close split' button.", "https://bugs.kde.org/show_bug.cgi?id=312834");?></li>
<li><?php print i18n_var("This version of Dolphin introduces <a href='%1'>smarter tab placement</a>. When you open a folder in a new tab, the new tab will now be placed immediately to the right of the current one, instead of always at the end of the tab bar.", "https://bugs.kde.org/show_bug.cgi?id=403690");?></li>
<li><?php print i18n_var("<a href='%1'>Tagging items</a> is now much more practical, as tags can be added or removed using the context menu.", "https://phabricator.kde.org/D16872");?></li>
<li><?php print i18n_var("We have <a href='%1'>improved the default sorting</a> parameters for some commonly-used folders. By default, the Downloads folder is now sorted by date with grouping enabled, and the Recent Documents view (accessible by navigating to recentdocuments:/) is sorted by date with a list view selected.", "https://phabricator.kde.org/D18697");?></li>
</ul>
</p>
<p>
<?php i18n("Bug fixes include:")?>
<ul>
<li><?php print i18n_var("When using a modern version of the SMB protocol, you can now <a href='%1'>discover Samba shares</a> for Mac and Linux machines.", "https://phabricator.kde.org/D16299");?></li>
<li><?php print i18n_var("<a href='%1'>Re-arranging items in the Places panel</a> once again works properly when some items are hidden.", "https://bugs.kde.org/show_bug.cgi?id=399430");?></li>
<li><?php print i18n_var("After opening a new tab in Dolphin, that new tab's view now automatically gains <a href='%1'>keyboard focus</a>.", "https://bugs.kde.org/show_bug.cgi?id=401899");?></li>
<li><?php print i18n_var("Dolphin now warns you if you try to quit while the <a href='%1'>terminal panel is open</a> with a program running in it.", "https://bugs.kde.org/show_bug.cgi?id=304816");?></li>
<li><?php print i18n_var("We fixed many memory leaks, improving Dolphin's overall performance.");?></li>
</ul>
</p>
<p>
<?php print i18n_var("The <a href='%1'>AudioCD-KIO</a> allows other KDE applications to read audio from CDs and automatically convert it into other formats.", "https://cgit.kde.org/audiocd-kio.git/");?>
</p>
<p>
<ul>
<li><?php print i18n_var("The AudioCD-KIO now supports ripping into <a href='%1'>Opus</a>.", "https://bugs.kde.org/show_bug.cgi?id=313768");?></li>
<li><?php print i18n_var("We made <a href='%1'>CD info text</a> really transparent for viewing.", "https://bugs.kde.org/show_bug.cgi?id=400849");?></li>
</ul>
</p>

<h3 style="clear:both;"><?php i18n("Video Editing")?></h3>

<figure style="float: right; margin: 16px"><a href="app1904_kdenlive.png" data-toggle="lightbox"><img src="app1904_kdenlive_small.jpeg" width="250" /></a></figure>
<p>
<?php print i18n_var("This is a landmark version for KDE's video editor. <a href='%1'>Kdenlive</a> has gone through an extensive re-write of its core code as more than 60% of its internals has changed, improving its overall architecture.", "https://kde.org/applications/multimedia/kdenlive/");?>
</p>
<p>
<?php i18n("Improvements include:")?>
<ul>
<li><?php print i18n_var("The timeline has been rewritten to make use of QML.");?></li>
<li><?php print i18n_var("When you put a clip on the timeline, audio and video always go to separate tracks.");?></li>
<li><?php print i18n_var("The timeline now supports keyboard navigation: clips, compositions and keyframes can be moved with the keyboard. Also, the height of the tracks themselves is adjustable.");?></li>
<li><?php print i18n_var("In this version of Kdenlive, the in-track audio recording comes with a new <a href='%1'>voice-over</a> feature.", "https://bugs.kde.org/show_bug.cgi?id=367676");?></li>
<li><?php print i18n_var("We have improved copy/paste: it works between different project windows. The proxy clip management has also been enhanced, as clips can now be individually deleted.");?></li>
<li><?php print i18n_var("Version 19.04 sees the return of support for external BlackMagic monitor displays and there are also new on-monitor preset guides.");?></li>
<li><?php print i18n_var("We have improved the keyframe handling, giving it a more consistent look and workflow. The titler has also been improved by making the align buttons snap to safe zones, adding configurable guides and background colors, and displaying missing items.");?></li>
<li><?php print i18n_var("We fixed the timeline corruption bug that misplaced or missed clips which was triggered when you moved a group of clips.");?></li>
<li><?php print i18n_var("We fixed the JPG image bug that rendered images as white screens on Windows. We also fixed the bugs affecting screen capture on Windows.");?></li>
<li><?php print i18n_var("Apart from all of the above, we have added many small usability enhancements that will make using Kdenlive easier and smoother.  ");?></li>
</ul>
</p>

<h3 style="clear:both;"><?php i18n("Office")?></h3>

<figure style="float: right; margin: 8px"><a href="app1904_okular.png" data-toggle="lightbox"><img src="app1904_okular_small.png" width="250" /></a></figure>
<p>
<?php print i18n_var("<a href='%1'>Okular</a> is KDE's multipurpose document viewer. Ideal for reading and annotating PDFs, it can also open ODF files (as used by LibreOffice and OpenOffice), ebooks published as ePub files, most common comic book files, PostScript files, and many more.", "https://kde.org/applications/graphics/okular/");?>
<!--Okular-->
</p>
<p>
<?php i18n("Improvements include:")?>
<ul>
<li><?php print i18n_var("To help you ensure your documents are always a perfect fit, we've added <a href='%1'>scaling options</a> to Okular's Print dialog.", "https://bugs.kde.org/show_bug.cgi?id=348172");?></li>
<li><?php print i18n_var("Okular now supports viewing and verifying <a href='%1'>digital signatures</a> in PDF files.", "https://cgit.kde.org/okular.git/commit/?id=a234a902dcfff249d8f1d41dfbc257496c81d84e");?></li>
<li><?php print i18n_var("Thanks to improved cross-application integration, Okular now supports editing LaTeX documents in <a href='%1'>TexStudio</a>.", "https://bugs.kde.org/show_bug.cgi?id=404120");?></li>
<li><?php print i18n_var("Improved support for <a href='%1'>touchscreen navigation</a> means you will be able to move backwards and forwards using a touchscreen when in Presentation mode.", "https://phabricator.kde.org/D18118");?></li>
<li><?php print i18n_var("Users who prefer manipulating documents from the command-line will be able to perform smart <a href='%1'>text search</a> with the new command-line flag that lets you open a document and highlight all occurrences of a given piece of text.", "https://bugs.kde.org/show_bug.cgi?id=362038");?></li>
<li><?php print i18n_var("Okular now properly displays links in <a href='%1'>Markdown documents</a> that span more than one line.", "https://bugs.kde.org/show_bug.cgi?id=403247");?></li>
<li><?php print i18n_var("The <a href='%1'>trim tools</a> have fancy new icons.", "https://bugs.kde.org/show_bug.cgi?id=397768");?></li>
</ul>
</p>

<figure style="float: right; margin: 8px"><a href="app1904_kmail.png" data-toggle="lightbox"><img src="app1904_kmail_small.jpeg" width="250" /></a></figure>
<p>
<?php print i18n_var("<a href='%1'>KMail</a> is KDE's privacy-protecting email client. Part of the <a href='%2'>Kontact groupware suite</a>, KMail supports all email systems and allows you to organize your messages into a shared virtual inbox or into separate accounts (your choice). It supports all kinds of message encryption and signing, and lets you share data such as contacts, meeting dates, and travel information with other Kontact applications.", "https://kde.org/applications/internet/kmail/", "https://kde.org/applications/office/kontact/");?>
</p>
<p>
<?php i18n("Improvements include:")?>
<ul>
<li><?php print i18n_var("Move over, Grammarly! This version of KMail comes with support for languagetools (grammar checker) and grammalecte (French-only grammar checker).");?></li>
<li><?php print i18n_var("Phone numbers in emails are now detected and can be dialed directly via <a href='%1'>KDE Connect</a>.", "https://community.kde.org/KDEConnect");?></li>
<li><?php print i18n_var("KMail now has an option to start directly in system tray without opening the main window.", "https://phabricator.kde.org/D19189");?></li>
<li><?php print i18n_var("We have improved the Markdown plugin support.");?></li>
<li><?php print i18n_var("Fetching mails via IMAP no longer gets stuck when login fails.");?></li>
<li><?php print i18n_var("We also made numerous fixes in KMail's backend Akonadi to improve reliability and performance.");?></li>
</ul>
</p>
<p>
<?php print i18n_var("<a href='%1'>KOrganizer</a> is Kontact's calendar component, managing all your events.", "https://kde.org/applications/office/korganizer/");?>
</p>
<p>
<ul>
<li><?php print i18n_var("Recurrent events from <a href='%1'>Google Calendar</a> are again synchronized correctly.", "https://bugs.kde.org/show_bug.cgi?id=334569");?></li>
<li><?php print i18n_var("The event reminder window now remembers to show on all desktops.", "https://phabricator.kde.org/D16247");?></li>
<li><?php print i18n_var("We modernized the look of the <a href='%1'>event views</a>.", "https://phabricator.kde.org/T9420");?></li>
</ul>
</p>
<p>
<?php print i18n_var("<a href='%1'>Kitinerary</a> is Kontact's brand new travel assistant that will help you get to your location and advise you on your way.", "https://cgit.kde.org/kitinerary.git/");?>
</p>
<p>
<ul>
<li><?php print i18n_var("There is a new generic extractor for RCT2 tickets (e.g. used by railway companies such as DSB, ÖBB, SBB, NS).");?></li>
<li><?php print i18n_var("Airport name detection and disambiguation have been greatly improved.");?></li>
<li><?php print i18n_var("We added new custom extractors for previously unsupported providers (e.g. BCD Travel, NH Group), and improved format/language variations of already supported providers (e.g. SNCF, Easyjet, Booking.com, Hertz).");?></li>
</ul>
</p>

<h3 style="clear:both;"><?php i18n("Development")?></h3>

<figure style="float: right; margin: 8px"><a href="app1904_kate.png" data-toggle="lightbox"><img src="app1904_kate_small.png" width="250" /></a></figure>
<p>
<?php print i18n_var("<a href='%1'>Kate</a> is KDE's full-featured text editor, ideal for programming thanks to features such as tabs, split-view mode, syntax highlighting, a built-in terminal panel, word completion, regular expressions search and substitution, and many more via the flexible plugin infrastructure.", "https://kde.org/applications/utilities/kate/");?>
</p>
<p>
<?php i18n("Improvements include:")?>
<ul>
<li><?php print i18n_var("Kate can now show all invisible whitespace characters, not just some.");?></li>
<li><?php print i18n_var("You can easily enable and disable Static Word Wrap by using its own menu entry on a per-document basis, without having to change the global default setting.");?></li>
<li><?php print i18n_var("The file and tab context menus now include a bunch of useful new actions, such as Rename, Delete, Open Containing Folder, Copy File Path, Compare [with another open file], and Properties.");?></li>
<li><?php print i18n_var("This version of Kate ships with more plugins enabled by default, including the popular and useful inline Terminal feature.");?></li>
<li><?php print i18n_var("When quitting, Kate no longer prompts you to acknowledge files that were modified on the disk by some other process, such as a source control change.");?></li>
<li><?php print i18n_var("The plugin’s tree view now correctly displays all menu items for git entries that have umlauts in their names.");?></li>
<li><?php print i18n_var("When opening multiple files using the command line, the files are opened in new tabs in the same order as specified in the command line.");?></li>
</ul>
</p>

<figure style="float: right; margin: 8px"><a href="app1904_konsole.png" data-toggle="lightbox"><img src="app1904_konsole_small.png" width="250" /></a></figure>
<p>
<?php print i18n_var("<a href='%1'>Konsole</a> is KDE's terminal emulator. It supports tabs, translucent backgrounds, split-view mode, customizable color schemes and keyboard shortcuts, directory and SSH bookmarking, and many other features.", "https://kde.org/applications/system/konsole/");?>
</p>
<p>
<?php i18n("Improvements include:")?>
<ul>
<li><?php print i18n_var("Tab management has seen a number of improvements that will help you be more productive. New tabs can be created by middle-clicking on empty parts of the tab bar, and there's also an option that allows you to close tabs by middle-clicking on them. Close buttons are displayed on tabs by default, and icons will be displayed only when using a profile with a custom icon. Last but not least, the Ctrl+Tab shortcut allows you to quickly switch between the current and previous tab.");?></li>
<li><?php print i18n_var("The Edit Profile dialog received a huge <a href='%1'>user interface overhaul</a>.", "https://phabricator.kde.org/D17244");?></li>
<li><?php print i18n_var("The Breeze color scheme is now used as the default Konsole color scheme, and we have improved its contrast and consistency with the system-wide Breeze theme.");?></li>
<li><?php print i18n_var("We have resolved the issues when displaying bold text.");?></li>
<li><?php print i18n_var("Konsole now correctly displays the underline-style cursor.");?></li>
<li><?php print i18n_var("We have improved the display of <a href='%1'>box and line characters</a>, as well as of <a href='%2'>Emoji characters</a>.", "https://bugs.kde.org/show_bug.cgi?id=402415", "https://bugs.kde.org/show_bug.cgi?id=401298");?></li>
<li><?php print i18n_var("Profile switching shortcuts now switch the current tab’s profile instead of opening a new tab with the other profile.");?></li>
<li><?php print i18n_var("Inactive tabs that receive a notification and have their title text color changed now again reset the color to the normal tab title text color when the tab is activated.");?></li>
<li><?php print i18n_var("The 'Vary the background for each tab' feature now works when the base background color is very dark or black.");?></li>
</ul>
</p>

<p>
<?php print i18n_var("<a href='%1'>Lokalize</a> is a computer-aided translation system that focuses on productivity and quality assurance. It is targeted at software translation, but also integrates external conversion tools for translating office documents.", "https://kde.org/applications/development/lokalize/");?>
</p>
<p>
<?php i18n("Improvements include:")?>
<ul>
<li><?php print i18n_var("Lokalize now supports viewing the translation source with a custom editor.");?></li>
<li><?php print i18n_var("We improved the DockWidgets location, and the way settings are saved and restored.");?></li>
<li><?php print i18n_var("The position in .po files is now preserved when filtering messages.");?></li>
<li><?php print i18n_var("We fixed a number of UI bugs (developer comments, RegExp toggle in mass replace, fuzzy empty message count, …).");?></li>
</ul>
</p>

<h3 style="clear:both;"><?php i18n("Utilities")?></h3>

<figure style="float: right; margin: 16px"><a href="app1904_gwenview.png" data-toggle="lightbox"><img src="app1904_gwenview_small.jpeg" width="250" /></a></figure>
<p>
<?php print i18n_var("<a href='%1'>Gwenview</a> is an advanced image viewer and organizer with intuitive and easy-to-use editing tools.", "https://kde.org/applications/graphics/gwenview/");?>
</p>
<p>
<?php i18n("Improvements include:")?>
<ul>
<li><?php print i18n_var("The version of Gwenview that ships with Applications 19.04 includes full <a href='%1'>touchscreen support</a>, with gestures for swiping, zooming, panning, and more.", "https://phabricator.kde.org/D13901");?></li>
<li><?php print i18n_var("Another enhancement added to Gwenview is full <a href='%1'>High DPI support</a>, which will make images look great on high-resolution screens.", "https://bugs.kde.org/show_bug.cgi?id=373178");?></li>
<li><?php print i18n_var("Improved support for <a href='%1'>back and forward mouse buttons</a> allows you to navigate between images by pressing those buttons.", "https://phabricator.kde.org/D14583");?></li>
<li><?php print i18n_var("You can now use Gwenview to open image files created with <a href='%1'>Krita</a> – everyone’s favorite digital painting tool.",  "https://krita.org/");?></li>
<li><?php print i18n_var("Gwenview now supports large <a href='%1'>512 px thumbnails</a>, allowing you to preview your images more easily.", "https://phabricator.kde.org/D6083");?></li>
<li><?php print i18n_var("Gwenview now uses the <a href='%1'>standard Ctrl+L keyboard shortcut</a> to move focus to the URL field.", "https://bugs.kde.org/show_bug.cgi?id=395184");?></li>
<li><?php print i18n_var("You can now use the <a href='%1'>Filter-by-name feature</a> with the Ctrl+I shortcut, just like in Dolphin.", "https://bugs.kde.org/show_bug.cgi?id=386531");?></li>
</ul>
</p>

<figure style="float: right; margin: 8px"><a href="app1904_spectacle.jpg" data-toggle="lightbox"><img src="app1904_spectacle.jpg" width="250" /></a></figure>
<p>
<?php print i18n_var("<a href='%1'>Spectacle</a> is Plasma's screenshot application. You can grab full desktops spanning several screens, individual screens, windows, sections of windows, or custom regions using the rectangle selection feature.", "https://kde.org/applications/graphics/spectacle/");?>
</p>
<p>
<?php i18n("Improvements include:")?>
<ul>
<li><?php print i18n_var("We have extended the Rectangular Region mode with a few new options. It can be configured to <a href='%1'>auto-accept</a> the dragged box instead of asking you to adjust it first. There is also a new default option to remember the current <a href='%2'>rectangular region</a> selection box, but only until the program is closed.", "https://bugs.kde.org/show_bug.cgi?id=404829", "https://phabricator.kde.org/D19117");?></li>
<li><?php print i18n_var("You can configure what happens when the screenshot shortcut is pressed <a href='%1'>while Spectacle is already running</a>.", "https://phabricator.kde.org/T9855");?></li>
<li><?php print i18n_var("Spectacle allows you to change the <a href='%1'>compression level</a> for lossy image formats.", "https://bugs.kde.org/show_bug.cgi?id=63151");?></li>
<li><?php print i18n_var("Save settings shows you what the <href='%1'>filename of a screenshot</a> will look like. You can also easily tweak the <a href='%2'>filename template</a> to your preferences by simply clicking on placeholders.", "https://bugs.kde.org/show_bug.cgi?id=381175", "https://bugs.kde.org/show_bug.cgi?id=390856");?></li>
<li><?php print i18n_var("Spectacle no longer displays both “Full Screen (All Monitors)” and “Current Screen” options when your computer only has one screen.");?></li>
<li><?php print i18n_var("The help text in Rectangular Region mode now shows up in the middle of the primary display, rather than split between the screens.");?></li>
<li><?php print i18n_var("When run on Wayland, Spectacle only includes the features that work.");?></li>
</ul>
</p>

<h3 style="clear:both;"><?php i18n("Games and Education")?></h3>

<figure style="float: right; margin: 16px"><a href="app1904_kmplot.png" data-toggle="lightbox"><img src="app1904_kmplot_small.png" width="250" /></a></figure>
<p>
<?php print i18n_var("Our application series includes numerous <a href='%1'>games</a> and <a href='%2'>educational applications</a>.", "https://games.kde.org/", "https://edu.kde.org/");?>
</p>
<p>
<?php print i18n_var("<a href='%1'>KmPlot</a> is a mathematical function plotter. It has a powerful built-in parser. The graphs can be colorized and the view is scalable, allowing you to zoom to the level you need. Users can plot different functions simultaneously and combine them to build new functions.", "https://kde.org/applications/education/kmplot");?>
<ul>
<li><?php print i18n_var("You can now zoom in by holding down Ctrl and using the <a href='%1'>mouse wheel</a>.", "https://bugs.kde.org/show_bug.cgi?id=159772");?></li>
<li><?php print i18n_var("This version of Kmplot introduces the <a href='%1'>print preview</a> option.", "https://phabricator.kde.org/D17626");?></li>
<li><?php print i18n_var("Root value or (x,y) pair can now be copied to <a href='%1'>clipboard</a>.", "https://bugs.kde.org/show_bug.cgi?id=308168");?></li>
</ul>
</p>

<p>
<?php print i18n_var("<a href='%1'>Kolf</a> is a miniature golf game.", "https://kde.org/applications/games/kolf/");?>
<ul>
<li><?php print i18n_var("We have restored <a href='%1'>sound support</a>.", "https://phabricator.kde.org/D16978");?></li>
<li><?php print i18n_var("Kolf has been successfully ported away from kdelibs4.");?></li>
</ul>
</p>

<!-- Boilerplate -->

<section class="row get-it">
    <article class="col-md">
        <h2><?php i18n("Package Downloads");?></h2>
        <p>
            <?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
        </p>
        <p><a href='https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro' class="learn-more"><?php i18n("Package download wiki page");?></a></p>
    </article>

    <article class="col-md">
        <h2><?php i18n("Linux App Stores");?></h2>
        <p>
            <?php print str_replace('https://snapcraft.io/search?q=kde', 'https://snapcraft.io/publisher/kde', i18n("The Snap store contains <a href='https://snapcraft.io/search?q=kde'>44 packages from KDE applications</a> updated at release time.
"));?>
        </p>
        <a href="https://snapcraft.io/publisher/kde">   <img alt="<?php i18n("Get it from the Snap Store");?>" src="https://snapcraft.io/static/images/badges/en/snap-store-black.svg" /> </a>
    </article>

    <article class="col-md">
        <h2><?php i18n("Source Downloads");?></h2>
        <p><?php print i18n_var("The complete source code for KDE Applications %1 may be <a href='https://download.kde.org/stable/applications/%2/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications-%3.php'>KDE Applications %4 Info Page</a>.", $version_text, $version_number, $version_number, $version_text);?></p>
    </article>
</section>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p>
 <?php i18n("KDE is a <a href='https://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.");?>
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
?>
