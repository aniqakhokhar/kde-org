<?php

  $page_title = "Anuncio de lanzamiento de KDE 4.0.3";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

También disponible en:
<a href="announce-4.0.3.php">English</a>
<a href="announce-4.0.3-it.php">Italiano</a>
<a href="http://fr.kde.org/announcements/announce-4.0.3.php">Francés</a>
<a href="announce-4.0.3-pt_BR.php">Portuguese (Brazilian)</a>
<a href="announce-4.0.3-sv.php">Sueco</a>

<!--
<a href="announce-4.0.3-bn_IN.php">Bengali (India)</a>
<a href="announce-4.0.3-ca.php">Catalan</a>
<a href="http://www.kdecn.org/announcements/announce-4.0.3.php">Chinese</a>
<a href="announce-4.0.3-cz.php">Czech</a>
<a href="announce-4.0.3-nl.php">Dutch</a>
<a href="announce-4.0.3.php">English</a>
<a href="http://fr.kde.org/announcements/announce-4.0.3.php">Francés</a>
<a href="announce-4.0.3-de.php">Alemán</a>
<a href="announce-4.0.3-gu.php">Gujarati</a>
<a href="announce-4.0.3-he.php">Hebrew</a>
<a href="announce-4.0.3-hi.php">Hindi</a>
<a href="announce-4.0.3-it.php">Italiano</a>
<a href="announce-4.0.3-lv.php">Latvian</a>
<a href="announce-4.0.3-ml.php">Malayalam</a>
<a href="announce-4.0.3-mr.php">Marathi</a>
<a href="announce-4.0.3-fa.php">Persian</a>
<a href="announce-4.0.3-pl.php">Polish</a>
<a href="announce-4.0.3-pa.php">Punjabi</a>
<a href="announce-4.0.3-pt_BR.php">Portuguese (Brazilian)</a>
<a href="announce-4.0.3-ro.php">Rumano</a>
<a href="announce-4.0.3-ru.php">Russian</a>
<a href="announce-4.0.3-sl.php">Slovenian</a>
<a href="announce-4.0.3-es.php">Spanish</a>
<a href="announce-4.0.3-sv.php">Sueco</a>
<a href="announce-4.0.3-ta.php">Tamil</a>
-->

<!-- // Boilerplate -->

<h3 align="center">
El proyecto KDE lanza la tercera versión que mejora las traducciones y servicios del escritorio líder de software libre.
</h3>

<p align="justify">
  <strong>
La Comunidad de KDE lanza la la tercera versión que mejora las traducciones y servicios
del Escritorio Libre 4.0, incluyendo numerosas correcciones de fallos, mejoras de rendimiento 
y actualizaciones de traducción.
</strong>
</p>

<p align="justify">
2 de abril de 2008 (INTERNET). La <a href="http://www.kde.org/">Comunidad de KDE</a> 
ha anunciado hoy la inmediata disponibilidad de KDE 4.0.3, la tercera versión de mantenimiento
para la última generación del más avanzado y potente escritorio libre. KDE 4.0.3 es la
tercera actualización mensual de <a href="4.0/">KDE 4.0</a>. Incorpora
un escritorio básico y muchos otros paquetes como administración, redes, educación,
utilidades, multimedia, juegos, recursos gráficos, desarrollo web y demás. Las
premiadas herramientas y aplicaciones de KDE están disponibles en 49 idiomas.
</p>
<p align="justify">
KDE, incluyendo todas sus bibliotecas y aplicaciones, está disponible gratuitamente
bajo licencias de Código Abierto. Se puede conseguir KDE en código fuente y varios
formatos binarios desde <a
href="http://download.kde.org/stable/4.0.3/">http://download.kde.org</a> y además
en <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a> o junto con alguno de los
<a href="http://www.kde.org/download/distributions.php">principales sistemas GNU/Linux
y UNIX</a> existentes hoy en día.
</p>

<h4>
  <a name="changes">Mejoras</a>
</h4>
<p align="justify">
KDE 4.0.3 incluye una impresionante cantidad de mejoras y correcciones de errores. La mayoria
de ellas están descritas en el
<a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php">registro de cambios</a>.
KDE continuará lanzando mensualmente actualizaciones para el escritorio 4.0. KDE 4.1, que
traerá <a href="http://techbase.kde.org/index.php?title=Schedules/KDE4/4.1_Feature_Plan">grandes mejoras</a>
al escritorio KDE y sus aplicaciones, será lanzado en julio de este año.

<br />
Las mejoras de KDE 4.0.3 giran en torno a una gran cantidad de correcciones de
fallos y actualizaciones de traducción. Las correcciones se han realizado de
tal forma que haya un mínimo riesgo de regresiones. Para KDE es también una
manera de proporcionar rápidamente correcciones de fallos a los usuarios.

Un extracto del registro de cambios revela que casi todos los módulos de KDE
han sufrido muchas mejoras. De nuevo, el equipo de KHTML ha hecho un
impresionante trabajo mejorando la experiencia del usuario con el navegador
web Konqueror.

<ul>
  <li>Optimizaciones del desplazamiento en KHTML, el motor de renderizado HTML de KDE.</li>
  <li>Manejo mejorado de ventanas de diálogo en KWin, el gestor de ventanas de KDE.</li>
  <li>Varias mejoras de renderizado en Okular, el visor de documentos de KDE.</li>
</ul>

<h4>Extragear</h4>
<p align="justify">
Desde KDE 4.0.0, las aplicaciones <a href="http://extragear.kde.org">Extragear</a>
son también parte de las versiones regulares de KDE.
Las aplicaciones extragear son aplicaciones KDE que están maduras, pero no son parte de
alguno de los otros paquetes de KDE. El paquete extragear que se incluye en
KDE 4.0.3 contiene los siguientes programas:

<ul>
    <li><a href="http://en.wikipedia.org/wiki/KColorEdit">KColoredit</a> -  Un editor 
        para ficheros de paletas de color que soporta los formatos de paletas de color de KDE y GIMP.</li>
    <li>KFax - Un visor de fax de escritorio </li>
    <li><a href="http://www.kde-apps.org/content/show.php/KGrab?content=74086">KGrab</a> - 
        Una herramienta de captura de pantalla más avanzada </li>
    <li><a href="http://extragear.kde.org/apps/kgraphviewer/">KGraphviewer</a> - A 
        Visor de grafos de GraphViz </li>
    <li><a href="http://w1.1358.telia.com/~u135800018/prog.html#KICONEDIT">KIconedit</a> - 
        Un programa de dibujo de iconos </li>
    <li><a href="http://kmldonkey.org/">KMldonkey</a> - Un cliente gráfico para la red EDonkey </li>
    <li><a href="http://www.kpovmodeler.org/">KPovmodeler</a> - Un modelador 3D </li>
    <li>Libksane - Una biblioteca de escaneado de imágenes </li>
    <li><a href="http://www.rsibreak.org">RSIbreak</a> - Un programa que te salva de tener
		RSI (Lesión por Esfuerzo Repetitivo) obligándote a tomar descansos. </li>
</ul>

Una novedad en esta versión de aplicaciones Extragear es el KIO slave de Gopher, una extensión
que añade soporte para el <a href="http://en.wikipedia.org/wiki/Gopher_(protocol)">protocolo
Gopher</a> a todas las aplicaciones KDE.
</p>

<h4>
Conjunto de aplicaciones educativas de KDE
</h4>
<p align="justify">
KDE 4.0.3 incorpora un conjunto de <a href="http://edu.kde.org">software educativo</a> de alta calidad.
Las aplicaciones abarcan desde <a href="http://edu.kde.org/marble/">Marble</a>, el versátil globo terráqueo de
escritorio, a juegos pequeños para niños más jóvenes.
<p align="justify">
Kalzium es una tabla periódica de los elementos. Visualiza conceptos abstractos, tales como
los átomos, de forma atractiva. También ofrece numerosas maneras de mostrar información
detallada acerca de los elementos. Kalzium se ha construido como una herramienta que hace la
química fácil de entender para estudiantes de secundaria, pero también es divertido para que jueguen
personas mayores.
<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
  <a href="announce_4.0.3/kalzium.png">
    <img src="announce_4.0.3/kalzium_thumb.png" align="center"  height="261"  />
  </a>
  <br /><em>Experimente la química con Kalzium</em>
</div>
</p>
<p align="justify">

Parley es un programa que ayuda a memorizar vocabulario. Parley soporta varias funcionalidades
específicas para idiomas, pero también puede ser usado para aprender simplemente tareas. Usa el 
método de aprendizaje de repaso espaciado, también conocido como de fichas de aprendizaje.
Crear nuevas colecciones de vocabulario con Parley es fácil, pero por supuesto es incluso
mejor si se usan algunos de los archivos ya hechos que se pueden descargar desde Internet.
<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
  <a href="announce_4.0.3/parley.png">
    <img src="announce_4.0.3/parley_thumb.png" align="center"  height="225"  />
  </a>
  <br /><em>Practique vocabulario con Parley</em>
</div>
</p>
<p align="justify">

Kmplot es un trazador de funciones matemáticas que proporciona una herramienta fácil para entender
las matemáticas. Se pueden introducir fácilmente funciones matemáticas en él y ver las gráficas
que dichas funciones describen.
<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
  <a href="announce_4.0.3/kmplot.png">
    <img src="announce_4.0.3/kmplot_thumb.png" align="center"  height="341"  />
  </a>
  <br /><em>Haz los cálculos con Kmplot</em>
</div>

Aquellos que quieran más información acerca de las aplicaciones educativas de KDE deberían
realizar la <a href="http://edu.kde.org/tour_kde4.0/">Visita guiada del Proyecto de Educación de KDE</a>.
</p>


<h4>
  Cómo instalar los paquetes binarios de KDE 4.0.3
</h4>
<p align="justify">
  <em>Creadores de paquetes</em>.
Algunos proveedores de Linux/UNIX han proporcionado generosamente
paquetes binarios de KDE 4.0.3 para algunas versiones de su distribución, y en
otros casos comunidades de voluntarios lo han hecho.
Algunos de estos paquetes binarios están disponibles para su libre descarga
en el servidor de descargas de KDE en 
<a href="http://download.kde.org/binarydownload.html?url=/stable/4.0.3/">http://download.kde.org</a>.
Paquetes binarios adicionales, así como actualizaciones de los paquetes ahora
disponibles, pueden estar disponibles en las próximas semanas.

</p>

<p align="justify">
<a name="package_locations"><em>Localizaciones de paquetes</em></a>.
Para ver una lista actualizada de paquetes binarios disponibles de los que el Proyecto
KDE ha sido informado, por favor visite la <a href="/info/4.0.3.php">página
de información de KDE 4.0.3</a>.
</p>


<h4>
  Cómo compilar KDE 4.0.3
</h4>
<p align="justify">
  <a name="source_code"></a><em>Código fuente</em>.
  El código fuente completo de KDE 4.0.3 puede ser
  <a href="http://download.kde.org/stable/4.0.3/src/">descargado libremente</a>.
  Hay disponibles instrucciones acerca de compilar e instalar KDE 4.0.3
  en la <a href="/info/4.0.3.php">página de información de KDE 4.0.3</a>.

</p>

<h4>
Ayudar a KDE
</h4>
<p align="justify">
KDE es un proyecto de <a href="http://www.gnu.org/philosophy/free-sw.html">Software Libre</a>
que existe y crece sólo mediante la ayuda de muchos voluntarios que donan su tiempo y esfuerzo. KDE siempre está buscando nuevos voluntarios y colaboradores, bien para ayudar programando, arreglando fallos o informando de ellos, escribiendo documentación, traducciones, promocionándolo, donando dinero, etc. Todos los colaboradores son gratamente apreciados y esperados con mucho entusiasmo. Por favor, lea la página <a href="/community/donations/">Ayudar a KDE</a> para más información.
</p>

<p align="justify">
¡Esperamos noticias suyas pronto!
</p>

<h2>Acerca de KDE 4</h2>
<p>
KDE 4.0 es un innovador escritorio de Software Libre que contiene muchas aplicaciones
de uso diario, así como para propósitos específicos. Plasma es un nuevo interfaz de escritorio
desarrollado para KDE 4, proporcionando una forma intuitiva de interactuar con el escritorio y
las aplicaciones. El navegador web Konqueror integra la web en el escritorio. El gestor de
archivos Dolphin, el lector de documentos Okular y el centro de control System Settings completan
el conjunto de escritorio básico. 

<br />
KDE está construido sobre las Bibliotecas KDE, las cuales proporcionan un acceso sencillo a los recursos
de la red mediante KIO y capacidades visuales avanzadas mediante Qt4. Phonon y Solid, que también son
parte de las Bibliotecas KDE, añaden un <i>framework</i> multimedia y mejor integración de hardware a
todas las aplicaciones KDE.

</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Contactos de prensa</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
