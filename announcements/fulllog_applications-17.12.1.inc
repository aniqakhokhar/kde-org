<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Hide]</a></h3>
<ul id='ulakonadi' style='display: block'>
<li>Give more details in the LLCONFLICT error. <a href='http://commits.kde.org/akonadi/22009f36a7a1cf50a76b191b491da8e678eb96a0'>Commit.</a> </li>
<li>Make conflict dialog much bigger, it's unusable otherwise. <a href='http://commits.kde.org/akonadi/c71aed7cc0d55bcfbd0c31a7fcbfd994301c9063'>Commit.</a> </li>
<li>ErrorOverlay: don't show by mistake if called with Running twice. <a href='http://commits.kde.org/akonadi/10db44a34c4fa0d0907bdc1abfec674fed920dcf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379997'>#379997</a></li>
</ul>
<h3><a name='akonadi-import-wizard' href='https://cgit.kde.org/akonadi-import-wizard.git'>akonadi-import-wizard</a> <a href='#akonadi-import-wizard' onclick='toggle("ulakonadi-import-wizard", this)'>[Hide]</a></h3>
<ul id='ulakonadi-import-wizard' style='display: block'>
<li>Fix rename categories. <a href='http://commits.kde.org/akonadi-import-wizard/6ad9bc0b7cad09309cef0256329bd2182b32eee2'>Commit.</a> </li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Hide]</a></h3>
<ul id='ulark' style='display: block'>
<li>Set transient parent to extraction dialog. <a href='http://commits.kde.org/ark/e909a198ec6facd62c25314e53893855161574bc'>Commit.</a> </li>
</ul>
<h3><a name='baloo-widgets' href='https://cgit.kde.org/baloo-widgets.git'>baloo-widgets</a> <a href='#baloo-widgets' onclick='toggle("ulbaloo-widgets", this)'>[Hide]</a></h3>
<ul id='ulbaloo-widgets' style='display: block'>
<li>Added 'Comment' option to  metadata config-dialog. <a href='http://commits.kde.org/baloo-widgets/caf155114b3b3b41a26eebd46a938104c2eea9b3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365620'>#365620</a></li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>Infopanel fully resizable. <a href='http://commits.kde.org/dolphin/65c0997164bd6f770c4286f7be83bcd2d3d145bd'>Commit.</a> </li>
<li>Revive folderpanel when outside $HOME. <a href='http://commits.kde.org/dolphin/7b595b3387cdb002c3e9a11e98a95d8801ca078d'>Commit.</a> </li>
<li>Ignore baloo urls created from new KIO model. <a href='http://commits.kde.org/dolphin/9d3a019445d7a7fdf3177bca9eeef4c44599e706'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387888'>#387888</a></li>
</ul>
<h3><a name='eventviews' href='https://cgit.kde.org/eventviews.git'>eventviews</a> <a href='#eventviews' onclick='toggle("uleventviews", this)'>[Hide]</a></h3>
<ul id='uleventviews' style='display: block'>
<li>Agendaview.cpp - in loadDecorations(), don't add failed decos to the list. <a href='http://commits.kde.org/eventviews/04875981a0efbd25cc0fbd97254a6f916239a415'>Commit.</a> </li>
</ul>
<h3><a name='filelight' href='https://cgit.kde.org/filelight.git'>filelight</a> <a href='#filelight' onclick='toggle("ulfilelight", this)'>[Hide]</a></h3>
<ul id='ulfilelight' style='display: block'>
<li>Use correct native separators when prettifying a local path. <a href='http://commits.kde.org/filelight/f761342084a41f91f74f3e059f5f7136da957e42'>Commit.</a> </li>
<li>Substantially refine the path x-platform path displaying. <a href='http://commits.kde.org/filelight/85ad152e4dd3e6e5827968442e069434ab2b433b'>Commit.</a> </li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Hide]</a></h3>
<ul id='ulgwenview' style='display: block'>
<li>Fix failing PlaceTreeModelTest autotest. <a href='http://commits.kde.org/gwenview/e8d799c9b522af299f721dbbfcc650526ba78940'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387824'>#387824</a></li>
<li>Respect custom filename for Copy/Move/Link To operations. <a href='http://commits.kde.org/gwenview/119ac4186e4150467f4c3833d8756345b4a95191'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388144'>#388144</a></li>
<li>Fix displaying RW2 files with shared-mime-info 1.9. <a href='http://commits.kde.org/gwenview/484a8df5e435ff1587a58cd1e3ac1a6788d8a4ef'>Commit.</a> </li>
<li>Fix expanding of baloo URLs in folders sidebar tree view. <a href='http://commits.kde.org/gwenview/50e6fa3ffc490eca33b8e2025120ec041b333fee'>Commit.</a> See bug <a href='https://bugs.kde.org/387824'>#387824</a></li>
<li>Revert changes caused by mistake from previous commit. <a href='http://commits.kde.org/gwenview/b8ff0e19ad69932077c1611e22182b6686c8af9e'>Commit.</a> </li>
<li>Fixed places side panel for baloo urls. <a href='http://commits.kde.org/gwenview/86d754546bd1d6054cc87d3030a1f29d9995bd94'>Commit.</a> See bug <a href='https://bugs.kde.org/387824'>#387824</a></li>
<li>Update titlebar after image modification. <a href='http://commits.kde.org/gwenview/5a7d105ae9dead6214953bfcb3f184c0399be42b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/345980'>#345980</a></li>
<li>Correct margin for left sidebar. <a href='http://commits.kde.org/gwenview/92e63330ecdcb1569100036a9a7f5245522fe720'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381535'>#381535</a></li>
</ul>
<h3><a name='k3b' href='https://cgit.kde.org/k3b.git'>k3b</a> <a href='#k3b' onclick='toggle("ulk3b", this)'>[Hide]</a></h3>
<ul id='ulk3b' style='display: block'>
<li>Revert "Fix Settings dialog resizes itself issue". <a href='http://commits.kde.org/k3b/c5ccf6d64658caece7fe18743fa6639d91154ea0'>Commit.</a> </li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Hide]</a></h3>
<ul id='ulkate' style='display: block'>
<li>Clear konsole command-line before directory change. <a href='http://commits.kde.org/kate/732f690534d8818af9f893bd567fe99f2d9999b1'>Commit.</a> </li>
</ul>
<h3><a name='kcalcore' href='https://cgit.kde.org/kcalcore.git'>kcalcore</a> <a href='#kcalcore' onclick='toggle("ulkcalcore", this)'>[Hide]</a></h3>
<ul id='ulkcalcore' style='display: block'>
<li>Fix a porting bug when persing timezones. <a href='http://commits.kde.org/kcalcore/24ef9a938e23d32f8c0044ad91e39bbf310b2081'>Commit.</a> </li>
</ul>
<h3><a name='kdebugsettings' href='https://cgit.kde.org/kdebugsettings.git'>kdebugsettings</a> <a href='#kdebugsettings' onclick='toggle("ulkdebugsettings", this)'>[Hide]</a></h3>
<ul id='ulkdebugsettings' style='display: block'>
<li>Fix save custom rules when value is false. <a href='http://commits.kde.org/kdebugsettings/e07c5b40224865a3038706a24f83b4d6bc01efd5'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Fix translation install. <a href='http://commits.kde.org/kdenlive/60916dd1febc574f61c16f235d6e7261c86e062a'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Hide]</a></h3>
<ul id='ulkdepim-addons' style='display: block'>
<li>We still need it as a slot. <a href='http://commits.kde.org/kdepim-addons/1e438182a77464dfd536a9805990bd89801b7370'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Hide]</a></h3>
<ul id='ulkdepim-runtime' style='display: block'>
<li>DAV: fix Next button not enabled when typing password. <a href='http://commits.kde.org/kdepim-runtime/57b24f6a033b018600a345ba4e86b20e332114c2'>Commit.</a> </li>
<li>Add Sasl2_INCLUDE_DIRS to the inlclude directories of kdexoauth2. <a href='http://commits.kde.org/kdepim-runtime/4c8026fe73fe6518798afc8dd3b1f20822e8f540'>Commit.</a> </li>
<li>Add Sasl2_INCLUDE_DIRS to the inlclude directories of kdexoauth2. <a href='http://commits.kde.org/kdepim-runtime/21c46e941e445da5513229ccffeb84c3400a999a'>Commit.</a> </li>
<li>Cmake: Strip comments from else()/endif(). <a href='http://commits.kde.org/kdepim-runtime/0abb495af8ff40921731a3633fd8f320f1b52922'>Commit.</a> </li>
<li>Use ecm_install_icons instead of a loop. <a href='http://commits.kde.org/kdepim-runtime/bd2a620dd3bf432fb30a5ff2d263f6570b30adb3'>Commit.</a> </li>
<li>Use KDE_INSTALL_ICONDIR instead of CMAKE_INSTALL_PREFIX. <a href='http://commits.kde.org/kdepim-runtime/4ffa3a77ce91427eba71013900dd96a6636e1a46'>Commit.</a> </li>
</ul>
<h3><a name='kget' href='https://cgit.kde.org/kget.git'>kget</a> <a href='#kget' onclick='toggle("ulkget", this)'>[Hide]</a></h3>
<ul id='ulkget' style='display: block'>
<li>Simplify and fix "Download again?" dialog. <a href='http://commits.kde.org/kget/ce318c52c5f643364fdb7594ffc8f04915b68294'>Commit.</a> </li>
<li>Fix path/Url issues in checksumsearch plugin. <a href='http://commits.kde.org/kget/7aa96c890e721b35778a7588af7c293919b2b65d'>Commit.</a> </li>
<li>[metalink] Don't duplicate slash. <a href='http://commits.kde.org/kget/7746a28100b584547bcecf5c5f9bebccb9efa598'>Commit.</a> </li>
<li>Fix Transfer::directory(). <a href='http://commits.kde.org/kget/0dc6351f7f368190a8297a18f8ff9ace65e7f8a3'>Commit.</a> </li>
<li>Fix MMS downloads. <a href='http://commits.kde.org/kget/a0be949d206af8e12ea3cb8171416a824b94c60d'>Commit.</a> </li>
<li>[newtransferdialog] Use "Save Mode" for the destination file dialog. <a href='http://commits.kde.org/kget/acd005d314f8d3e899f225b035c12129c859b8e5'>Commit.</a> </li>
<li>Simplify transfer history dialog and save correct its size. <a href='http://commits.kde.org/kget/17b40fc2415aea1a425a355cd2a4fb934e82173a'>Commit.</a> </li>
</ul>
<h3><a name='kleopatra' href='https://cgit.kde.org/kleopatra.git'>kleopatra</a> <a href='#kleopatra' onclick='toggle("ulkleopatra", this)'>[Hide]</a></h3>
<ul id='ulkleopatra' style='display: block'>
<li>Fix signature guessing. <a href='http://commits.kde.org/kleopatra/066c6e4ca82d064437f1902838d09fa903147e40'>Commit.</a> </li>
<li>Fix S/MIME CSR Creation. <a href='http://commits.kde.org/kleopatra/faa62ae4f13839e30d2fbbd050f536ada8a06f16'>Commit.</a> </li>
</ul>
<h3><a name='kmahjongg' href='https://cgit.kde.org/kmahjongg.git'>kmahjongg</a> <a href='#kmahjongg' onclick='toggle("ulkmahjongg", this)'>[Hide]</a></h3>
<ul id='ulkmahjongg' style='display: block'>
<li>Update action state properly after undo/redo. <a href='http://commits.kde.org/kmahjongg/d23bab4f9a06c08abdd40cf1fae2ac3b03c5f8b9'>Commit.</a> </li>
</ul>
<h3><a name='kmix' href='https://cgit.kde.org/kmix.git'>kmix</a> <a href='#kmix' onclick='toggle("ulkmix", this)'>[Hide]</a></h3>
<ul id='ulkmix' style='display: block'>
<li>Fix initial dialog's positioning when it's done by Qt. <a href='http://commits.kde.org/kmix/b7caba5b9c0c68bcac553d89e85fd25a07829141'>Commit.</a> </li>
</ul>
<h3><a name='kpimtextedit' href='https://cgit.kde.org/kpimtextedit.git'>kpimtextedit</a> <a href='#kpimtextedit' onclick='toggle("ulkpimtextedit", this)'>[Hide]</a></h3>
<ul id='ulkpimtextedit' style='display: block'>
<li>Remove duplicate line. <a href='http://commits.kde.org/kpimtextedit/d94b0fc622b9ea64dc21b355194d94f4066dbc9f'>Commit.</a> </li>
</ul>
<h3><a name='kqtquickcharts' href='https://cgit.kde.org/kqtquickcharts.git'>kqtquickcharts</a> <a href='#kqtquickcharts' onclick='toggle("ulkqtquickcharts", this)'>[Hide]</a></h3>
<ul id='ulkqtquickcharts' style='display: block'>
<li>Mark Label and LineLabel types as internal. <a href='http://commits.kde.org/kqtquickcharts/09f6df15b6636b8cf9443244df1efb5afa6e9bb1'>Commit.</a> </li>
<li>Add missing types to qmldir. <a href='http://commits.kde.org/kqtquickcharts/4e90e87e7522230387c1bdf7b95d8177ea3a7486'>Commit.</a> </li>
</ul>
<h3><a name='krdc' href='https://cgit.kde.org/krdc.git'>krdc</a> <a href='#krdc' onclick='toggle("ulkrdc", this)'>[Hide]</a></h3>
<ul id='ulkrdc' style='display: block'>
<li>Don't use qCritical for debug output, that's what qCDebug is for. <a href='http://commits.kde.org/krdc/ecea7149d423038a01c4c68369384b84849ed64e'>Commit.</a> </li>
</ul>
<h3><a name='ksmtp' href='https://cgit.kde.org/ksmtp.git'>ksmtp</a> <a href='#ksmtp' onclick='toggle("ulksmtp", this)'>[Hide]</a></h3>
<ul id='ulksmtp' style='display: block'>
<li>Send the correct hostname with the HELO/EHLO command. <a href='http://commits.kde.org/ksmtp/5199ed07428a03f1aa340da3ae99fcfa62ba2751'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387926'>#387926</a></li>
<li>Fix duplicate authentication. <a href='http://commits.kde.org/ksmtp/ec2afd27c790fbde63a9c2bdd1f97a59fe04b18e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387926'>#387926</a>. Fixes bug <a href='https://bugs.kde.org/388068'>#388068</a></li>
<li>Add .arcconfig. <a href='http://commits.kde.org/ksmtp/02d4368d33ee06431d2757f732106914d72fe44d'>Commit.</a> </li>
<li>Show error string instead error number. <a href='http://commits.kde.org/ksmtp/1ae4f0e39b942ecb7929e9060550e150fa1734e1'>Commit.</a> </li>
<li>Do not cancel send job after 10s timeout. <a href='http://commits.kde.org/ksmtp/400942fde2e90c79dca9d60ce94293cffaccde38'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387613'>#387613</a></li>
</ul>
<h3><a name='libkleo' href='https://cgit.kde.org/libkleo.git'>libkleo</a> <a href='#libkleo' onclick='toggle("ullibkleo", this)'>[Hide]</a></h3>
<ul id='ullibkleo' style='display: block'>
<li>Fix sorting in KeyRearrangeColumnsProxyModel. <a href='http://commits.kde.org/libkleo/3203921aed49539158891a3a05d2cc93895d9cdd'>Commit.</a> </li>
</ul>
<h3><a name='libksieve' href='https://cgit.kde.org/libksieve.git'>libksieve</a> <a href='#libksieve' onclick='toggle("ullibksieve", this)'>[Hide]</a></h3>
<ul id='ullibksieve' style='display: block'>
<li>We just need address header not all. <a href='http://commits.kde.org/libksieve/c3243420ffae7f126d75fc630bc53db96e9d5c70'>Commit.</a> </li>
<li>Fix Bug 381714 - Cancelling sieve management operation has no effect. <a href='http://commits.kde.org/libksieve/bb78894acfa86d09b8274c193bfe23a5a02e5749'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381714'>#381714</a></li>
</ul>
<h3><a name='lskat' href='https://cgit.kde.org/lskat.git'>lskat</a> <a href='#lskat' onclick='toggle("ullskat", this)'>[Hide]</a></h3>
<ul id='ullskat' style='display: block'>
<li>Add missing categories files. <a href='http://commits.kde.org/lskat/1918d750c96bef893b2bf439aa53aca485951037'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Repair Page Down with Qt 5.9, there documentElement.scrollTop doesn't work. <a href='http://commits.kde.org/messagelib/155647759daf413aba208011004a23ecbda7de60'>Commit.</a> </li>
<li>Fix Bug 388440 - scrolling in KMail: "page down" only works once. <a href='http://commits.kde.org/messagelib/ddbdf9b33c6279b0a2ff29a7b67b0044762a258d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388440'>#388440</a></li>
<li>Add the values of the Inotify find call to the appropriate lists. <a href='http://commits.kde.org/messagelib/4e42726de7f2f7923298c6788edbe0916ec4b128'>Commit.</a> </li>
<li>Not necessary to use private class here. <a href='http://commits.kde.org/messagelib/14c393bc7591f63f50ca6e7f054ff9dcb35cb705'>Commit.</a> </li>
<li>Hide widget when we change message. <a href='http://commits.kde.org/messagelib/650db1a4fd3e26679e62467f087ff10c543ac85d'>Commit.</a> </li>
</ul>
<h3><a name='okteta' href='https://cgit.kde.org/okteta.git'>okteta</a> <a href='#okteta' onclick='toggle("ulokteta", this)'>[Hide]</a></h3>
<ul id='ulokteta' style='display: block'>
<li>Fix StringDataInformation::setEncoding() logic on UTF16 endianess change. <a href='http://commits.kde.org/okteta/c62fdc8b88c4490f814ca9c5fcdbaab64637c089'>Commit.</a> </li>
<li>Fix crash on shutdown due to mTerminalPart trying to recover. <a href='http://commits.kde.org/okteta/ba63f5114006ebcc8b94b5986521d8f99f13407e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/384993'>#384993</a></li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Remove deprecated Encoding key from desktop files. <a href='http://commits.kde.org/okular/9008803b32c23f5433e4a340268229a9a823a2fe'>Commit.</a> </li>
<li>Fix testSaveAsUndoStackAnnotations autotest. <a href='http://commits.kde.org/okular/454a53ff72d06f6c2e2b7af2447d374e73cf56bb'>Commit.</a> </li>
<li>Don't call m_formsMessage->setVisible( true ) on unsetDummyMode. <a href='http://commits.kde.org/okular/9504d91c6111e5f8c9ace8c9f059521f8c5a44ac'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Hide]</a></h3>
<ul id='ulumbrello' style='display: block'>
<li>Keep calls to Import_Utils::remapUMLEnum() in Ada, IDL and Pascal import in sync with in java and csharp import. <a href='http://commits.kde.org/umbrello/c1b2a9ca18bcbbf4c658bab694564be529ad473e'>Commit.</a> </li>
<li>Fixup of b4a20d4fb. <a href='http://commits.kde.org/umbrello/95115e7067609c2df435cf7dd8b3628ae86767fd'>Commit.</a> See bug <a href='https://bugs.kde.org/388549'>#388549</a></li>
<li>Fix bug not saving "show stereotype" class widget setting to xmi file. <a href='http://commits.kde.org/umbrello/e66f65297a0820b1159d9d5657a93fe4149d826a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388560'>#388560</a></li>
<li>Fix 'Cannot start umbrello from applications menu'. <a href='http://commits.kde.org/umbrello/b4a20d4fb517033533244f14d8370e87f3c6483b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388549'>#388549</a></li>
<li>Catch configuring KDE4 variant of umbrello with installed KF5 kdevplatform development package. <a href='http://commits.kde.org/umbrello/01a8c26a52bdfafe178b9881141416b30443b906'>Commit.</a> </li>
<li>Complete java import fix. <a href='http://commits.kde.org/umbrello/be3b42f9cf186cb24a9fb3865adc63537dc82444'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386479'>#386479</a></li>
<li>Spelling fix. <a href='http://commits.kde.org/umbrello/b76ec0da7d476b34107eb3a1f4a8e2642a38be9d'>Commit.</a> </li>
<li>Fix 'Imported classes are not placeable on diagrams after java import'. <a href='http://commits.kde.org/umbrello/ac222d4abc99c5e844604aebfd6b6a2239c69d93'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386479'>#386479</a></li>
<li>Fix implementation of Import_Utils::createUMLObject() parameter searchInParentPackageOnly. <a href='http://commits.kde.org/umbrello/c11df4aa1fd0f182c0c4b0797aa7035303b61eb9'>Commit.</a> See bug <a href='https://bugs.kde.org/386479'>#386479</a></li>
<li>Add additional java test files. <a href='http://commits.kde.org/umbrello/459fe36a6eca91d34becea3ae19fdba3848fbdd8'>Commit.</a> See bug <a href='https://bugs.kde.org/386479'>#386479</a></li>
<li>Fix 'umbrello5 does not display any files using default file pattern'. <a href='http://commits.kde.org/umbrello/465272f9c08fbf12b6c1c01865e66c75c0ec3c40'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388171'>#388171</a></li>
<li>Fix 'Umbrello 2.2.3 crashes when deleting three text labels'. <a href='http://commits.kde.org/umbrello/008b19ece3847435f5b2f229d2e528f8a6a71771'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387939'>#387939</a></li>
<li>Remove faulty '\n'. <a href='http://commits.kde.org/umbrello/1d335e07ba8578a746c23718f3ce01c25062f432'>Commit.</a> </li>
<li>Fix 'Broken link to omg.org/spec in "About->Umbrello" dialog'. <a href='http://commits.kde.org/umbrello/c74304cb27f71bedf7d5505f0abb64e8a5a5e6d0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387912'>#387912</a></li>
<li>Fix autoscroll for Logger. <a href='http://commits.kde.org/umbrello/7662598f246fda131c564564290f00105dbb16c7'>Commit.</a> </li>
</ul>
