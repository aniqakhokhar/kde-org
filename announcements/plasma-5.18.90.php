<?php
    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Plasma 5.19 Beta: Consistency, Control and Usability",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = 'plasma-5.18.90'; // for i18n
    $version = "5.18.90";
?>

<script src="/js/use-ekko-lightbox.js" defer="true"></script>

<main class="releaseAnnouncment container">
  <?php include "./announce-i18n-bar.inc"; ?>

  <h1 class="mt-2"><?php print i18n_var("Plasma %1", "5.19 Beta")?></h1>
  <div class="laptop-with-overlay d-block mt-3 mx-auto" style="max-width: 800px">
    <img class="laptop img-fluid mt-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
    <div class="laptop-overlay">
      <img class="img-fluid mt-3" src="plasma-5.19/plasma-5.19-2.png" alt="<?php print i18n_var("KDE Plasma %1", "5.19 Beta")?>" loading="lazy" width="1200" height="675" />
    </div>
  </div>

  <p><?php print i18n("Thursday, 14 May 2020.") ?></p>

  <p><?php i18n("It's time to test the beta release for Plasma 5.19!")?></p>

  <p><?php i18n("In this release, we have prioritized making Plasma more consistent, correcting and unifying designs of widgets and desktop elements; worked on giving you more control over your desktop by adding configuration options to the System Settings; and improved usability, making Plasma and its components easier to use and an overall more pleasurable experience.")?></p>

  <p><?php i18n("Read on to discover all the new features and improvements of Plasma 5.19…"); ?></p>

  <h2 id="desktop"><?php i18n("Plasma Desktop and Widgets");?></h2>

  <ul>
    <li><?php i18n("We have improved the panel spacer so that it can automatically center widgets."); ?></li>
    <li><?php i18n("The System Monitor widgets have been rewritten from scratch."); ?></li>
  </ul>
  <figure class="text-center">
    <img src="plasma-5.19/sensors.png" class="border-0 img-fluid" width="700"  alt="<?php i18n("Rewritten System Monitor Widgets");?>" />
    <figcaption><?php i18n("Rewritten System Monitor Widgets");?></figcaption>
  </figure>

  <ul><li><?php i18n("Plasma now has a consistent design and header area in system tray applets as well as notifications."); ?></li></ul>
  <figure class="text-center">
    <img src="plasma-5.19/system-tray-applets.png" class="border-0 img-fluid" width="600"  alt="<?php i18n("Consistent System Tray Applets");?>" />
    <figcaption><?php i18n("Consistent System Tray Applets");?></figcaption>
  </figure>
  <ul>
    <li><?php i18n("We have refreshed the look of the media playback applet in the System Tray and of Task Manager tooltips."); ?></li>
    <li><?php i18n("There are completely new photographic avatars to choose from."); ?></li>
  </ul>
  <figure class="text-center">
    <img src="plasma-5.19/user-avatars.png" class="border-0 img-fluid" width="500"  alt="<?php i18n("Completely New User Avatars");?>" />
    <figcaption><?php i18n("Completely New User Avatars");?></figcaption>
  </figure>
  <ul>
    <li><?php i18n("You can now see the name of the creator of a desktop wallpaper when you go to pick one."); ?></li>
    <li><?php i18n("Sticky notes get usability improvements."); ?></li>
    <li><?php i18n("You now have more control over the visibility of volume OSDs during certain situations."); ?></li>
    <li><?php i18n("GTK 3 applications immediately apply a newly selected color scheme and GTK 2 applications no longer have broken colors."); ?></li>
    <li><?php i18n("We have increased the default fixed-width font size from 9 to 10."); ?></li>
    <li><?php i18n("The audio widget shows a more consistent appearance with a more attractive interface for switching the current audio device."); ?></li>
  </ul>
  <figure class="text-center">
    <img src="plasma-5.19/multi-device-switching-ui.png" class="border-0 img-fluid" width="400"  alt="<?php i18n("More consistent appearance for switching the current audio device");?>" />
  </figure>

  <h2 id="systemsettings"><?php i18n("System Settings");?></h2>

  <ul>
    <li><?php i18n("Default Applications, Online Accounts, Global Shortcuts, KWin Rules and Background Services settings pages have all been overhauled."); ?></li>
  </ul>
  <figure class="text-center">
    <img src="plasma-5.19/redesigned-kcms.png" class="border-0 img-fluid" width="900"  alt="<?php i18n("Redesigned Settings Pages");?>" />
    <figcaption><?php i18n("Redesigned Settings Pages");?></figcaption>
  </figure>
  <ul>
    <li><?php i18n("When launching System Settings modules from within KRunner or the application launcher, the complete System Settings application launches on the page you asked for."); ?></li>
  </ul>
  <figure class="text-center">
    <div class="embed-responsive embed-responsive-16by9">
    <video width="700" height="504" controls>
      <source src="plasma-5.19/krunner.webm" type="video/webm" />
    </video>
    </div>
    <figcaption><?php i18n("Full System Settings App Is Now Launching");?></figcaption>
  </figure>
  <ul>
    <li><?php i18n("The Display settings page now shows the aspect ratio for each available screen resolution."); ?></li>
    <li><?php i18n("You now have more granular control over Plasma's animation speed."); ?></li>
    <li><?php i18n("We have added configurable file indexing for individual directories and you can now disable indexing for hidden files."); ?></li>
    <li><?php i18n("There is now an option that lets you configure the mouse and touchpad scroll speed under Wayland."); ?></li>
    <li><?php i18n("We have made lots of small improvements to the font configuration."); ?></li>
  </ul>

  <h2 id="kinfocenter"><?php i18n("Info Center");?></h2>

  <ul>
    <li><?php i18n("The Info Center application has been redesigned with a look and feel that is consistent with the System Settings."); ?></li>
    <li><?php i18n("It is now possible to see information about your graphics hardware."); ?></li>
  </ul>
  <figure class="text-center">
    <img src="plasma-5.19/kinfocenter.png" class="border-0 img-fluid" width="600"  alt="<?php i18n("Redesigned Info Center");?>" />
    <figcaption><?php i18n("Redesigned Info Center");?></figcaption>
  </figure>

  <h2 id="kwin"><?php i18n("KWin Window Manager");?></h2>

  <ul>
    <li><?php i18n("The new subsurface clipping for Wayland greatly reduces the flickering in many applications."); ?></li>
    <li><?php i18n("Icons in titlebars are now recolored to fit the color scheme instead of sometimes being hard to see."); ?></li>
    <li><?php i18n("Screen rotation for tablets and convertable laptops now works on Wayland."); ?></li>
  </ul>
  <figure class="text-center">
    <img src="plasma-5.19/recoloured-icons.png" class="border-0 img-fluid" width="350" alt="<?php i18n("Icon Recoloring in the Titlebar");?>" />
    <figcaption><?php i18n("Icon Recoloring in the Titlebar");?></figcaption>
  </figure>

  <h2 id="discover"><?php i18n("Discover");?></h2>

  <ul>
    <li><?php i18n("Flatpak repositories in use are easier to remove now."); ?></li>
    <li><?php i18n("Discover displays the application version for reviews."); ?></li>
    <li><?php i18n("Discover improved its visual consistency and usability."); ?></li>
  </ul>
  <figure class="text-center">
    <img src="plasma-5.19/discover.png" class="border-0 img-fluid" width="500"  alt="<?php i18n("Flatpak Repository Removal in Discover");?>" />
    <figcaption><?php i18n("Flatpak Repository Removal in Discover");?></figcaption>
  </figure>

  <h2 id="ksysguard"><?php i18n("KSysGuard");?></h2>
  <ul>
    <li><?php i18n("Our system monitor KSysGuard has gained support for systems with more than 12 CPU cores."); ?></li>
  </ul>

  <a href="plasma-5.18.5-5.18.90-changelog.php"><?php print i18n_var("Full Plasma %1 changelog", "5.19 Beta"); ?></a>

    <!-- // Boilerplate again -->
    <section class="row get-it" id="download">
        <article class="col-md">
            <h2><?php i18n("Live Images");?></h2>
            <p>
                <?php i18n("The easiest way to try it out is with a live image booted off a USB disk. Docker images also provide a quick and easy way to test Plasma.");?>
            </p>
            <a href='https://community.kde.org/Plasma/Live_Images' class="learn-more"><?php i18n("Download live images with Plasma 5");?></a>
            <a href='https://community.kde.org/Plasma/Docker_Images' class="learn-more"><?php i18n("Download Docker images with Plasma 5");?></a>
        </article>

        <article class="col-md">
            <h2><?php i18n("Package Downloads");?></h2>
            <p>
                <?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
            </p>
            <a href='https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro' class="learn-more"><?php i18n("Get KDE Software on Your Linux Distro wiki page");?></a>
        </article>

        <article class="col-md">
            <h2><?php i18n("Source Downloads");?></h2>
            <p>
                <?php i18n("You can install Plasma 5 directly from source.");?>
            </p>
            <a href='https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source' class='learn-more'><?php i18n("Community instructions to compile it");?></a>
            <a href='../info/plasma-5.15.90.php' class='learn-more'><?php i18n("Source Info Page");?></a>
        </article>
    </section>

    <section class="give-feedback">
        <h2><?php i18n("Feedback");?></h2>

        <p class="kSocialLinks">
            <?php i18n("You can give us feedback and get updates on our social media channels:"); ?>
            <?php include($site_root . "/contact/social_link.inc"); ?>
        </p>
        <p>
            <?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?>
        </p>

        <p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>Plasma Matrix chat room</a>, <a href='%2'>Plasma-devel mailing list</a> or report issues via <a href='%3'>bugzilla</a>. If you like what the team is doing, please let them know!", "https://webchat.kde.org/#/room/#plasma:kde.org", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided"); ?>

        <p><?php i18n("Your feedback is greatly appreciated.");?></p>
    </section>

    <h2>
        <?php i18n("Supporting KDE");?>
    </h2>

    <p align="justify">
        <?php print i18n_var("KDE is a <a href='%1'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='%2'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='%3'>Join the Game</a> initiative.", "http://www.gnu.org/philosophy/free-sw.html", "/community/donations/", "https://relate.kde.org/civicrm/contribute/transact?id=5"); ?>
    </p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

</main>
<?php
  require('../aether/footer.php');
