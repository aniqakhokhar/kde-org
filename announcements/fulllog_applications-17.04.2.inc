<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Show]</a></h3>
<ul id='ulakonadi' style='display: none'>
<li>Fix crash when Connection is terminated before init(). <a href='http://commits.kde.org/akonadi/76fac925362e55c0919d9ffaf688bdf77865d7a4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379809'>#379809</a></li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Show]</a></h3>
<ul id='ulark' style='display: none'>
<li>Don't crash when dropping twice invalid URLs on empty archive view. <a href='http://commits.kde.org/ark/bf24b7a80b9d144b53acd933cf56b3d58d932e53'>Commit.</a> </li>
<li>Part: update actions when settings change. <a href='http://commits.kde.org/ark/dfb9823d0a1849098d6b6cb39629d9f4bbe239b6'>Commit.</a> </li>
<li>Respect file size limit when double cliclking. <a href='http://commits.kde.org/ark/b32236826c9f98d2b313de66c32c38aa4d9fe147'>Commit.</a> </li>
<li>Fix crash when renaming top-level entries. <a href='http://commits.kde.org/ark/ed46c0d66e82ee33c7342052a91d1ebf0e465bef'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379802'>#379802</a></li>
</ul>
<h3><a name='calendarsupport' href='https://cgit.kde.org/calendarsupport.git'>calendarsupport</a> <a href='#calendarsupport' onclick='toggle("ulcalendarsupport", this)'>[Show]</a></h3>
<ul id='ulcalendarsupport' style='display: none'>
<li>Fix potential crash on wayland. <a href='http://commits.kde.org/calendarsupport/c4421ba68b15d1f209d2fde60891329c238ed1f7'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Show]</a></h3>
<ul id='uldolphin' style='display: none'>
<li>Revert "Increase smooth scrolling animation duration from 100 to 300 ms and set easing curve to InOutQuart". <a href='http://commits.kde.org/dolphin/883b908b4b26b428e5609d3e7ddd46a9ad884b92'>Commit.</a> </li>
<li>Correct searchbox, split view transitions between tabs. <a href='http://commits.kde.org/dolphin/43da84eefc7d68ce86cda2d353216dbe7552fc2c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379135'>#379135</a>. Fixes bug <a href='https://bugs.kde.org/380032'>#380032</a></li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Show]</a></h3>
<ul id='ulgwenview' style='display: none'>
<li>Fix Enter and Escape keys in tool dialogs (crop, red eye). <a href='http://commits.kde.org/gwenview/f2a611e0c184dc30b66102c51c3bac71653af224'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/313806'>#313806</a></li>
<li>Fixed persistence of "Show Status Bar" action status. <a href='http://commits.kde.org/gwenview/f94529934bb9e526a19be10b5af77ca51d62ed94'>Commit.</a> </li>
<li>Add keyboard shortcut for statusbar toggling option. <a href='http://commits.kde.org/gwenview/567fd7deb6ccfd200dbb25d70d48e8e05230a049'>Commit.</a> </li>
<li>Re-enable all tests. <a href='http://commits.kde.org/gwenview/59227541997d2862c930e90b142ee74b4e8a87cb'>Commit.</a> </li>
<li>Fix crop region size. <a href='http://commits.kde.org/gwenview/5b0749f6d60f40e91ae930bac604c7e77074c11f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/340459'>#340459</a></li>
<li>Re-enable importertest. <a href='http://commits.kde.org/gwenview/44a5f4a4e9057318ec2439ad5e2299ad1a90a303'>Commit.</a> </li>
<li>Avoid data loss when importing pictures. <a href='http://commits.kde.org/gwenview/6ce5d2f8d82f83c5a3d726ee5807ebaf7a6e3c6c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379615'>#379615</a></li>
</ul>
<h3><a name='kapptemplate' href='https://cgit.kde.org/kapptemplate.git'>kapptemplate</a> <a href='#kapptemplate' onclick='toggle("ulkapptemplate", this)'>[Show]</a></h3>
<ul id='ulkapptemplate' style='display: none'>
<li>Brush up and fix KF5-based app templates some more. <a href='http://commits.kde.org/kapptemplate/53e306fba1c377a8983a90b2035b4bcb6931473d'>Commit.</a> </li>
<li>Use FDL 1.2 for kde-frameworks5 template. <a href='http://commits.kde.org/kapptemplate/a7208eb16f89014da76611285bad760f3f73b95e'>Commit.</a> </li>
</ul>
<h3><a name='kcalutils' href='https://cgit.kde.org/kcalutils.git'>kcalutils</a> <a href='#kcalutils' onclick='toggle("ulkcalutils", this)'>[Show]</a></h3>
<ul id='ulkcalutils' style='display: none'>
<li>Fix potential crash. <a href='http://commits.kde.org/kcalutils/86dc94dce4e311f675f2fdd28de370a756e46d22'>Commit.</a> </li>
<li>Src/icaldrag.cpp - don't crash if QMimeData is null. <a href='http://commits.kde.org/kcalutils/613c4947fe7289ee6444d20103d38b0177256e54'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379534'>#379534</a></li>
</ul>
<h3><a name='kcontacts' href='https://cgit.kde.org/kcontacts.git'>kcontacts</a> <a href='#kcontacts' onclick='toggle("ulkcontacts", this)'>[Show]</a></h3>
<ul id='ulkcontacts' style='display: none'>
<li>Fix potential crash. <a href='http://commits.kde.org/kcontacts/4df827a98ed0fd0e0e6868addbfc6cbb44b216c1'>Commit.</a> </li>
</ul>
<h3><a name='kdav' href='https://cgit.kde.org/kdav.git'>kdav</a> <a href='#kdav' onclick='toggle("ulkdav", this)'>[Show]</a></h3>
<ul id='ulkdav' style='display: none'>
<li>Add missing include. <a href='http://commits.kde.org/kdav/c16692e69d6d815dfd19182db18534646a8787fa'>Commit.</a> </li>
</ul>
<h3><a name='kde-dev-scripts' href='https://cgit.kde.org/kde-dev-scripts.git'>kde-dev-scripts</a> <a href='#kde-dev-scripts' onclick='toggle("ulkde-dev-scripts", this)'>[Show]</a></h3>
<ul id='ulkde-dev-scripts' style='display: none'>
<li>Fix regex in convert-kurl.pl. <a href='http://commits.kde.org/kde-dev-scripts/77e13b76ff518233cc89a84f02dafcd57d7ee4c3'>Commit.</a> </li>
<li>Use absolute imports in GDB scripts. <a href='http://commits.kde.org/kde-dev-scripts/ef2cf266c9862c77b77f757f0f061bc8341874a8'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Show]</a></h3>
<ul id='ulkdenlive' style='display: none'>
<li>Forgot this file in previous commit :(. <a href='http://commits.kde.org/kdenlive/aa80c88cc4d8c5c3f7cda426d294a8b5d32252a1'>Commit.</a> </li>
<li>Fix crash changing title font for 2nd time. <a href='http://commits.kde.org/kdenlive/94b1b432ecd06d6ec71ebf3ee69942b13815b8d3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379606'>#379606</a></li>
<li>More Windows rendering fixes. <a href='http://commits.kde.org/kdenlive/8f788c1d13e08fe8bfed8858ee70933f8d42d1fe'>Commit.</a> </li>
<li>Fix init of geometryWidget. <a href='http://commits.kde.org/kdenlive/3ab863f0d58594802b53a36d0002d9415b9622ba'>Commit.</a> See bug <a href='https://bugs.kde.org/379274'>#379274</a></li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Show]</a></h3>
<ul id='ulkdepim-addons' style='display: none'>
<li>Fix Bug 379988 - TNEF attachments not properly represented in UI. <a href='http://commits.kde.org/kdepim-addons/648a48afbb50187c18d9452433549ae660f12b08'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379988'>#379988</a></li>
<li>Make sure that it's not empty. <a href='http://commits.kde.org/kdepim-addons/59af5eda10bf4ba970cf1efc6caf8b750da75246'>Commit.</a> </li>
<li>Make sure that we don't add empty str. <a href='http://commits.kde.org/kdepim-addons/bf606b8271551de655248204f21ba5c66cd0005a'>Commit.</a> </li>
<li>Add autotests. <a href='http://commits.kde.org/kdepim-addons/aa9a7e5b285b90d6f0c2da19ece7416732b701b9'>Commit.</a> </li>
<li>Fix list of emails. <a href='http://commits.kde.org/kdepim-addons/4d9769452bd5d64fb94e500eb15281c384d7e645'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-apps-libs' href='https://cgit.kde.org/kdepim-apps-libs.git'>kdepim-apps-libs</a> <a href='#kdepim-apps-libs' onclick='toggle("ulkdepim-apps-libs", this)'>[Show]</a></h3>
<ul id='ulkdepim-apps-libs' style='display: none'>
<li>Lintcmake include find dependency before using it http://build.neon.kde.org/job/xenial_unstable_applications_kdepim-apps-libs_lintcmake/4/testReport/. <a href='http://commits.kde.org/kdepim-apps-libs/36d2f64db4ef01d39c70ca9fd4e9f28ad92c0982'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Show]</a></h3>
<ul id='ulkdepim-runtime' style='display: none'>
<li>IMAP: allow creating top-level folders it User namespace is empty. <a href='http://commits.kde.org/kdepim-runtime/580ef3441cdd9ca2555a2a00c54aaa97a26b43a1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/339567'>#339567</a></li>
<li>Fix Bug 379155 - Imap resources crash when trying to open settings. <a href='http://commits.kde.org/kdepim-runtime/611510d0a005bc93102aa4b9f1a5b5f9905c4179'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379155'>#379155</a></li>
<li>Add missing include. <a href='http://commits.kde.org/kdepim-runtime/e8b131fe179f474af9de12f1289ea70cd99e90e4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379155'>#379155</a></li>
<li>Enable by default store locally emails. <a href='http://commits.kde.org/kdepim-runtime/73bd68ef63631b8507591d4ae6a02893edf57bf5'>Commit.</a> </li>
</ul>
<h3><a name='kfourinline' href='https://cgit.kde.org/kfourinline.git'>kfourinline</a> <a href='#kfourinline' onclick='toggle("ulkfourinline", this)'>[Show]</a></h3>
<ul id='ulkfourinline' style='display: none'>
<li>Fix theme change not being acted upon. <a href='http://commits.kde.org/kfourinline/969ced8ee36d711c35680a5383e873a4aa5f78ee'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365945'>#365945</a></li>
</ul>
<h3><a name='kgeography' href='https://cgit.kde.org/kgeography.git'>kgeography</a> <a href='#kgeography' onclick='toggle("ulkgeography", this)'>[Show]</a></h3>
<ul id='ulkgeography' style='display: none'>
<li>Duns and not Berwick was county town of Berwickshire. <a href='http://commits.kde.org/kgeography/291789adcdd29c221d86b525b2ffe1bbe1dab4c4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380320'>#380320</a></li>
</ul>
<h3><a name='kholidays' href='https://cgit.kde.org/kholidays.git'>kholidays</a> <a href='#kholidays' onclick='toggle("ulkholidays", this)'>[Show]</a></h3>
<ul id='ulkholidays' style='display: none'>
<li>Update Turkish Holiday File (EN and TR). <a href='http://commits.kde.org/kholidays/867fb26176ecf8a99c52577f718bc7397d807352'>Commit.</a> </li>
</ul>
<h3><a name='kidentitymanagement' href='https://cgit.kde.org/kidentitymanagement.git'>kidentitymanagement</a> <a href='#kidentitymanagement' onclick='toggle("ulkidentitymanagement", this)'>[Show]</a></h3>
<ul id='ulkidentitymanagement' style='display: none'>
<li>Fix potential crash. <a href='http://commits.kde.org/kidentitymanagement/fb5219aaf2d2a3e45cb2a703526aa61cf028edbb'>Commit.</a> </li>
</ul>
<h3><a name='kimap' href='https://cgit.kde.org/kimap.git'>kimap</a> <a href='#kimap' onclick='toggle("ulkimap", this)'>[Show]</a></h3>
<ul id='ulkimap' style='display: none'>
<li>Add sasl include (need by windows). <a href='http://commits.kde.org/kimap/e9af028a807fdf700e16faa1b8c0690b4392fa4b'>Commit.</a> </li>
<li>Define getPid on windows too. <a href='http://commits.kde.org/kimap/abefcd5c71926ff89d2b5e23f243b8bed9a5639c'>Commit.</a> </li>
<li>Fix sasl include dir. <a href='http://commits.kde.org/kimap/7e9011c1e138d6cd2e64d1c19eab6b6ae97938d5'>Commit.</a> </li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Show]</a></h3>
<ul id='ulkmail' style='display: none'>
<li>Make sure that we use plugin when we use sendlater feature. <a href='http://commits.kde.org/kmail/78c5552be2f00a4ac25bd77ca39386522fca70a8'>Commit.</a> </li>
<li>Remove old option. <a href='http://commits.kde.org/kmail/26c015800a13ae4ea0aaae0e77f26918446a234e'>Commit.</a> </li>
<li>Add missing helper_p include. <a href='http://commits.kde.org/kmail/8f8f402e376d38d5a34517d3738f3e1cdf747b67'>Commit.</a> </li>
<li>Fix list when we have empty string. <a href='http://commits.kde.org/kmail/4f9069871c324566763a5be543a77e1f21777b36'>Commit.</a> </li>
<li>Fix create list of emails. <a href='http://commits.kde.org/kmail/8924235c4dc844724f89e4929e1740dd62d07e72'>Commit.</a> </li>
<li>Fix typo. <a href='http://commits.kde.org/kmail/cfd7c3c129d0b7883a37bba2ba216e7416d08327'>Commit.</a> </li>
<li>Rename methods. <a href='http://commits.kde.org/kmail/57e22a4f902aee39bbcdeba1a542b85d94cca75e'>Commit.</a> </li>
</ul>
<h3><a name='kmime' href='https://cgit.kde.org/kmime.git'>kmime</a> <a href='#kmime' onclick='toggle("ulkmime", this)'>[Show]</a></h3>
<ul id='ulkmime' style='display: none'>
<li>Make it compile on windows. <a href='http://commits.kde.org/kmime/f9a93b779e0611fd325afc13a089f76cbe2355f6'>Commit.</a> </li>
<li>Look at to make it compile on windows. <a href='http://commits.kde.org/kmime/60c149828690a83f09b6b2d4760a16a6ca3fdd82'>Commit.</a> </li>
</ul>
<h3><a name='kolourpaint' href='https://cgit.kde.org/kolourpaint.git'>kolourpaint</a> <a href='#kolourpaint' onclick='toggle("ulkolourpaint", this)'>[Show]</a></h3>
<ul id='ulkolourpaint' style='display: none'>
<li>Adjust license to follow the rest of the headers. <a href='http://commits.kde.org/kolourpaint/36f297a9c9c9f5323273bdc57f5ee3a4e8e00743'>Commit.</a> </li>
</ul>
<h3><a name='libkdepim' href='https://cgit.kde.org/libkdepim.git'>libkdepim</a> <a href='#libkdepim' onclick='toggle("ullibkdepim", this)'>[Show]</a></h3>
<ul id='ullibkdepim' style='display: none'>
<li>Fix potential crash. <a href='http://commits.kde.org/libkdepim/a1a485d3d52313fb1569250acb332dad41c7f91c'>Commit.</a> </li>
</ul>
<h3><a name='libkleo' href='https://cgit.kde.org/libkleo.git'>libkleo</a> <a href='#libkleo' onclick='toggle("ullibkleo", this)'>[Show]</a></h3>
<ul id='ullibkleo' style='display: none'>
<li>Fix compilation with GCC 7. <a href='http://commits.kde.org/libkleo/675ce908a33d16f3b78d3fc741b0ff45790e4770'>Commit.</a> </li>
</ul>
<h3><a name='libksieve' href='https://cgit.kde.org/libksieve.git'>libksieve</a> <a href='#libksieve' onclick='toggle("ullibksieve", this)'>[Show]</a></h3>
<ul id='ullibksieve' style='display: none'>
<li>Fix order of attributes. <a href='http://commits.kde.org/libksieve/7800365248b9bc135a8b17b73bdc4a170f541529'>Commit.</a> </li>
<li>Add newline after each error message. <a href='http://commits.kde.org/libksieve/be8ae8680a8e61acc62cd1deebf02f00273509c4'>Commit.</a> </li>
<li>Don't add empty requires. <a href='http://commits.kde.org/libksieve/0608c5580946e0395606a144b7bb1fada2fab12c'>Commit.</a> </li>
<li>Don't double quote when we have a list. <a href='http://commits.kde.org/libksieve/982f765a924b4f9abae2e10e93a7e7bb44db8e47'>Commit.</a> </li>
<li>Fix generate body element when we have a list. <a href='http://commits.kde.org/libksieve/3001eb7dcec54ba005dae4130400e8c1e017babb'>Commit.</a> </li>
<li>Body supports list too. <a href='http://commits.kde.org/libksieve/1f8e83a697828c3250549a9df64c8d2eb18daafc'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Show]</a></h3>
<ul id='ulmessagelib' style='display: none'>
<li>Make sure to sign/encrypt message when we send later. <a href='http://commits.kde.org/messagelib/c54706e990bbd6498e7b1597ec7900bc809e8197'>Commit.</a> </li>
<li>Change encryption too. <a href='http://commits.kde.org/messagelib/12ab56430613da18e3872c3e041a3b6d7dcb9afe'>Commit.</a> </li>
<li>Fix potential crash as david reported. <a href='http://commits.kde.org/messagelib/7e52763adf3fcc4a3f98f9dffdfb22e8cab5189c'>Commit.</a> </li>
<li>Workaround assert in QtWebEngine 5.9 (ASSERT: "m_updateHttpCache"). <a href='http://commits.kde.org/messagelib/579a09e1bba70c90b710bfc92c13004aa959934e'>Commit.</a> </li>
<li>Fix runtime warning about application/x-pkcs7-signature. <a href='http://commits.kde.org/messagelib/23d0007f67813d44af48c461fe67beb2b7cece43'>Commit.</a> </li>
<li>Use QTemporaryDir as discussed with David. <a href='http://commits.kde.org/messagelib/34e89ff1c14689bdb925ed8258a7e2da038b466d'>Commit.</a> </li>
<li>Fix Bug 378526 - KMail 5.4.3: Attachment displayed with wrong filename. <a href='http://commits.kde.org/messagelib/cbbe13d470caa4b20505009f27888fcde6cd22b5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378526'>#378526</a></li>
</ul>
<h3><a name='okteta' href='https://cgit.kde.org/okteta.git'>okteta</a> <a href='#okteta' onclick='toggle("ulokteta", this)'>[Show]</a></h3>
<ul id='ulokteta' style='display: none'>
<li>Use explicit constructor call for QFlags-based types instead of blank 0. <a href='http://commits.kde.org/okteta/5ff403f48b747ef09e944ed029a8f33489daa3c5'>Commit.</a> </li>
<li>Use Qt::NoItemFlags instead of blank 0. <a href='http://commits.kde.org/okteta/2f0ece3f2b38891e7f5d73b0327c8e5b0ba5bf08'>Commit.</a> </li>
<li>Only register to D-Bus after inital commandline parsing has been done. <a href='http://commits.kde.org/okteta/fb9f49ff543fcb5e97b3678b2c871cc76a0a8370'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Show]</a></h3>
<ul id='ulokular' style='display: none'>
<li>Initialize cursor in PageView scrollbars with ArrowCursor. <a href='http://commits.kde.org/okular/574fad92c8bed04e8b0c0fcbb84a9ecbd8cf685b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/334798'>#334798</a>. Code review <a href='https://git.reviewboard.kde.org/r/130131'>#130131</a></li>
</ul>
<h3><a name='pimcommon' href='https://cgit.kde.org/pimcommon.git'>pimcommon</a> <a href='#pimcommon' onclick='toggle("ulpimcommon", this)'>[Show]</a></h3>
<ul id='ulpimcommon' style='display: none'>
<li>Add missing "break". <a href='http://commits.kde.org/pimcommon/c752a01107e8f5959fb2eed3b087de387b102bcf'>Commit.</a> </li>
</ul>
<h3><a name='print-manager' href='https://cgit.kde.org/print-manager.git'>print-manager</a> <a href='#print-manager' onclick='toggle("ulprint-manager", this)'>[Show]</a></h3>
<ul id='ulprint-manager' style='display: none'>
<li>Fix moving print job to another printer. <a href='http://commits.kde.org/print-manager/7776714c332e4794f5cdcd53ccbc5eb767e89d0e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378446'>#378446</a>. Code review <a href='https://git.reviewboard.kde.org/r/130063'>#130063</a></li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Show]</a></h3>
<ul id='ulumbrello' style='display: none'>
<li>Fix 'Building Umbrello 2.21.90.89e79b8 with optional LLVM fails'. <a href='http://commits.kde.org/umbrello/46763682e9fda91681fec507ff5345f234f1aeb7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379293'>#379293</a></li>
<li>Fix 'umbrello/KF5 does not show the KF5 crash dialog'. <a href='http://commits.kde.org/umbrello/badcf99a56f2e2d06b0ac64f98b6f1b0624b0cc9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374415'>#374415</a></li>
<li>Add some error message to debug output in case of file saving fails. <a href='http://commits.kde.org/umbrello/4def5639ebd6344173c301d53c22c3db438ee919'>Commit.</a> See bug <a href='https://bugs.kde.org/376305'>#376305</a></li>
</ul>
