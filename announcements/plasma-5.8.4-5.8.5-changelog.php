<?php
include_once ("functions.inc");
$translation_file = "kde-org";
$page_title = i18n_noop("Plasma 5.8.5 Complete Changelog");
$site_root = "../";
$release = 'plasma-5.8.5';
include "header.inc";
?>
<p><a href="plasma-5.8.5.php">Plasma 5.8.5</a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>Add top_left_arrow cursor symlinks. <a href='https://commits.kde.org/breeze/6316df30a995812003df9c1f31f9162e28c2bbcc'>Commit.</a> </li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Make sure we report changes in the size once advertised. <a href='https://commits.kde.org/discover/1cb77eeac7122a018f6e98d61690adf37aac8905'>Commit.</a> </li>
<li>AppStream: Take stock icons into account. <a href='https://commits.kde.org/discover/369ce7b623008753e8e65c7378fdc4be1fa870bc'>Commit.</a> </li>
<li>Enable full screenshot smoothness. <a href='https://commits.kde.org/discover/6ee9a59c4c4ae8f6eae004426b80227e0bee5445'>Commit.</a> </li>
<li>Add missing tests. <a href='https://commits.kde.org/discover/548f53c3bd8a472f1a964e4280fea098e4cb64fe'>Commit.</a> </li>
<li>Don't close if the input is wrong. <a href='https://commits.kde.org/discover/3f3cddb46bf0708f82b8a0b1cc352339432d66f1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372277'>#372277</a></li>
<li>Display an error if input URL was malformed. <a href='https://commits.kde.org/discover/6dd31d014fa00839791d730b9fdb9c29cabe3518'>Commit.</a> </li>
<li>Expose software-properties-kde if present. <a href='https://commits.kde.org/discover/3e7dd188f9d0e452bdcba583ac46ef992a918e01'>Commit.</a> </li>
</ul>


<h3><a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a> </h3>
<ul id='ulkde-gtk-config' style='display: block'>
<li>Make sure we check theme tarballs when they're set. <a href='https://commits.kde.org/kde-gtk-config/28c5ac5ed337dc4b9c8d59d3ef5c0264ffa15822'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372927'>#372927</a></li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>Check on current comic to be valid. <a href='https://commits.kde.org/kdeplasma-addons/7195311a1cd8f50686cacbbb5fd5ff6821bfdbb9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373031'>#373031</a></li>
</ul>


<h3><a name='kscreen' href='https://commits.kde.org/kscreen'>KScreen</a> </h3>
<ul id='ulkscreen' style='display: block'>
<li>Guard against nullptr-access to the OutputPtr. <a href='https://commits.kde.org/kscreen/d79dc2b8ca4f8c9daf6c6e6f5f3e40580f6d224c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372945'>#372945</a></li>
<li>Apply config change after correcting invalid mode. <a href='https://commits.kde.org/kscreen/d5b9d37767adbf2a035281fbeb5f38a309160413'>Commit.</a> See bug <a href='https://bugs.kde.org/356864'>#356864</a></li>
<li>No use in setting modes on disabled outputs. <a href='https://commits.kde.org/kscreen/b4b504696b638691ad96c75560e6599c44bea88c'>Commit.</a> </li>
<li>Correct possibly invalid current mode. <a href='https://commits.kde.org/kscreen/96db1a9a1aedcc3c5816af6fe69e7a9039daeda1'>Commit.</a> </li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Align configure button consistently across delegates. <a href='https://commits.kde.org/kwin/d0e0f6c88cdd032d3bdb9dc7b0c85902fb3d859a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372685'>#372685</a></li>
</ul>


<h3><a name='libkscreen' href='https://commits.kde.org/libkscreen'>libkscreen</a> </h3>
<ul id='ullibkscreen' style='display: block'>
<li>Allow changing an output's modelist at runtime. <a href='https://commits.kde.org/libkscreen/7367e55b7c172d54d068eb09f308e92368c294e9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356864'>#356864</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Title of Folder View plasmoid popup is now correctly shown. <a href='https://commits.kde.org/plasma-desktop/980f3aafbaed335c17e477c510fa84b9d7a18f7a'>Commit.</a> </li>
<li>[Kicker] Support drag from bookmarks runner. <a href='https://commits.kde.org/plasma-desktop/47ce959f6f10e6231f3cb78a7f7662610428161c'>Commit.</a> </li>
<li>Larger icons and stack windows above label item. <a href='https://commits.kde.org/plasma-desktop/9d45aa5a9584fa285802c4d96806abd902633f8c'>Commit.</a> </li>
<li>Ensure results are sorted by relevance. <a href='https://commits.kde.org/plasma-desktop/7c00be121eebfdfc350302ff9bf7c09064a021bd'>Commit.</a> </li>
<li>[taskmanager] Limit GroupDialog size, stop highlightwindow effect when it shown, increase items readability. <a href='https://commits.kde.org/plasma-desktop/aeec4ae8869b487fe516a920b49a1fde4ea48c06'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/128038'>#128038</a></li>
<li>Fix 2 RTL bugs. <a href='https://commits.kde.org/plasma-desktop/4fada5bdb0325c3952650f05ca7af1caa5f4e5a3'>Commit.</a> </li>
<li>[Kicker] Hide "Edit Applications..." context menu entry if system immutable. <a href='https://commits.kde.org/plasma-desktop/00d160aab9291ae2eb4b21aeddf95ec54a434c23'>Commit.</a> </li>
<li>Fix running of recent docs actions for runner match entries. <a href='https://commits.kde.org/plasma-desktop/0314f666649318557942e15bf9c980b8d7aa1fba'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373173'>#373173</a></li>
<li>[Folder View] Clear hover state when mouse leaves view. <a href='https://commits.kde.org/plasma-desktop/7585f295363532eff51f7e11ef6b62925117aba4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373255'>#373255</a></li>
<li>Select correct index without timer. <a href='https://commits.kde.org/plasma-desktop/d9d52a37c6ba267f7da936c0436b254788f7cb87'>Commit.</a> </li>
<li>Select correct index without timer. <a href='https://commits.kde.org/plasma-desktop/ee2ec66a415580a5c4ccfdb34e5f6e68a7739652'>Commit.</a> </li>
<li>Fix "Move to Current Desktop" action moving to all desktops instead. <a href='https://commits.kde.org/plasma-desktop/039ab6aad9b0c0e51e84bc5104cdfb00d93b3c97'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372873'>#372873</a></li>
<li>Check whether the KServe is still valid before running. <a href='https://commits.kde.org/plasma-desktop/4f3510bc5550d236db47b9bb98cd505a27b49645'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372810'>#372810</a></li>
<li>Publish delegate geometry when ChildCount increases. <a href='https://commits.kde.org/plasma-desktop/04a758912b906c8d793361956dfb7193da86d982'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372699'>#372699</a></li>
<li>Fix config dialogs in ltr mode. <a href='https://commits.kde.org/plasma-desktop/e18109e3c7646dd7319a8eb55da6dc010eed515d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372721'>#372721</a></li>
</ul>


<h3><a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a> </h3>
<ul id='ulplasma-integration' style='display: block'>
<li>Fix compilation with Qt 5.8. <a href='https://commits.kde.org/plasma-integration/6b405fead515df417514c9aa9bb72cfa5372d2e7'>Commit.</a> </li>
<li>Fix Plasma-QPA filedialog to show wrong directory with QFileDialog::selectUrl(). <a href='https://commits.kde.org/plasma-integration/7bbbd93cd3fc0abdffd3fa7f144cb50a33fafad9'>Commit.</a> </li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Some captive portals seems to have problem redirecting from https. <a href='https://commits.kde.org/plasma-nm/7defc68b673b0e0718081f828cf9f6a06b6b9f33'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374026'>#374026</a></li>
<li>Revert "Pass protocol type to openconnect". <a href='https://commits.kde.org/plasma-nm/2180f3582a0844404a8edad56daa6d0b5a4b151e'>Commit.</a> </li>
<li>Pass protocol type to openconnect. <a href='https://commits.kde.org/plasma-nm/a51e9c5c2a2c32ef8d2f354af33532cbc35bfa9e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363917'>#363917</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Notice when the only screen changes. <a href='https://commits.kde.org/plasma-workspace/f7b170de9fd9c4075fee324d33212b0d69909ba4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373880'>#373880</a></li>
<li>Make sure  applet is removed from stack when destroyed. <a href='https://commits.kde.org/plasma-workspace/c7dda807318670b1d9ec3cedca116c448ae5c907'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373812'>#373812</a></li>
<li>[Lock Screen] Wrap OSD in its own ColorScope. <a href='https://commits.kde.org/plasma-workspace/c575baacc34e818f632aba5e3569658b08f1fae3'>Commit.</a> </li>
<li>Move updating of primary screen in screenpool after fetching its id. <a href='https://commits.kde.org/plasma-workspace/bdfa0f3fb726f4c639d2ea86d829c25c89779025'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372963'>#372963</a></li>
<li>Calendar: clear selection on opening. <a href='https://commits.kde.org/plasma-workspace/c30edd9bedfd13459eecdf110fcb61d8f465523a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129308'>#129308</a></li>
<li>Fix regression in last commit, don't show battery life as 100 when it's 0. <a href='https://commits.kde.org/plasma-workspace/1507afa007a668f7797a89031efe241ce78322b7'>Commit.</a> </li>
<li>Fix "Unable to assign [undefined] to int" log. <a href='https://commits.kde.org/plasma-workspace/f4b5dfbc36d707d8dd76c4dd8b90a1b3877a393d'>Commit.</a> </li>
<li>Refresh the ScreenGeometry data role whenever a screen is added or changes its geometry. <a href='https://commits.kde.org/plasma-workspace/0b0d14639a1a0eb2766d74daa3358c0939b6d8b3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373075'>#373075</a></li>
<li>Systemtray: Don't propagate unhandled wheel events. <a href='https://commits.kde.org/plasma-workspace/d1ea67c1fcf03dc6365eaf62b3d815533267292f'>Commit.</a> </li>
<li>[Lock Screen] Add keyboard icon for keyboard layout switcher. <a href='https://commits.kde.org/plasma-workspace/b34601f0ef1a341bea940adff001fd2f41acc88e'>Commit.</a> </li>
<li>Remove categories not present on server anymore. <a href='https://commits.kde.org/plasma-workspace/59c66b71ce814cb61fb15423f2edff47024df0ea'>Commit.</a> </li>
<li>Select correct index without timer. <a href='https://commits.kde.org/plasma-workspace/5434e4bc014181841df0e24979e03288584b8696'>Commit.</a> </li>
<li>Expose rowCount as data role in TaskGroupingProxyModel. <a href='https://commits.kde.org/plasma-workspace/781bb3426978661112e9ac7fef3574ca7d73bae7'>Commit.</a> See bug <a href='https://bugs.kde.org/372699'>#372699</a></li>
<li>[taskmanagerrulesrc] Add Rewrite Rule for chromium. <a href='https://commits.kde.org/plasma-workspace/97ff71b5efe0ec8518204312f455137d64a3a284'>Commit.</a> </li>
<li>[SDDM Theme] Fix background in qmlscene by providing proper config dummy data. <a href='https://commits.kde.org/plasma-workspace/2f1efff8516abe32351f9ef4d0991acdbb778353'>Commit.</a> </li>
<li>[SDDM Theme] Add "lastUser" and "disableAvatarsThreshold" properties to dummydata. <a href='https://commits.kde.org/plasma-workspace/3deb4cd6234ee934c47e714dd492029bf24b19f1'>Commit.</a> </li>
<li>Login screen now remembers the last user name for domain logins where the user list is unavailable. <a href='https://commits.kde.org/plasma-workspace/83fac04b7d92fec586c07ee0be5a02d9842d3364'>Commit.</a> </li>
<li>Chromium WebApp windows are no longer treated as Chromium browser windows in task manager. <a href='https://commits.kde.org/plasma-workspace/6f4b4c26a8f654f1680c43358831ef11cdd8fcf4'>Commit.</a> </li>
<li>Fix config dialogs in ltr mode. <a href='https://commits.kde.org/plasma-workspace/7355b783c28a56715594697b2b90efc3810f1e41'>Commit.</a> See bug <a href='https://bugs.kde.org/372721'>#372721</a></li>
</ul>


<h3><a name='user-manager' href='https://commits.kde.org/user-manager'>User Manager</a> </h3>
<ul id='uluser-manager' style='display: block'>
<li>Hide "automatic login" button in UserAccounts since is does absolutely nothing. <a href='https://commits.kde.org/user-manager/4761ae39b60d6bb1923d78c2e4333b477cb3c240'>Commit.</a> See bug <a href='https://bugs.kde.org/363058'>#363058</a></li>
<li>Revert "Do not ask for root permissions when it's unnecessary". <a href='https://commits.kde.org/user-manager/f2c69db182fb20453e671359e90a3bc6de40c7b0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373276'>#373276</a></li>
</ul>


<?php
  include("footer.inc");
?>
