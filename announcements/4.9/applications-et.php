<?php

  $release = '4.9';
  $release_full = '4.9.0';
  $page_title = "KDE rakendused 4.9 on viimistletumad ja stabiilsemad";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>Teistes keeltes:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>

<p>
KDE teatab rõõmuga paljude populaarsete rakenduste uute versioonide väljalaskmisest. Nende seas on mitu olulist ja kõigile vajalikku tööriista ja rakendust, näiteks Okular, Kopete, KDE PIM, õpirakendused ja mängud.
</p>
<p>
KDE dokumendinäitaja Okular võib nüüd salvestada ja trükkida PDF-dokumentide märkusi ehk annotatsioone. Täiustatud on otsimist, järjehoidjaid ja teksti valimist. Okulari saab seadistada nii, et sülearvuti ei lähe unne ega lülita ekraani välja esitluse ajal, samuti suudab Okular nüüd esitada PDF-failidesse põimitud videoid. Pildinäitaja Gwenview pakub uuena piltide täisekraanirežiimis sirvimise võimalust, rääkimata tervest reast veaparandustest ja väiksematest täiustustest.
</p>
<p>
KDE vaikimisi muusikamängija Juk toetab nüüd last.fm-i koos tagasiside ja kaanepiltide tõmbamisega, samuti on toetatud MPRIS2. Võimalik on ära tunda ja kasutada MP4- ja AAC-failidesse põimitud kaanepilte. Ka KDE videomängija Dragon toetab nüüdsest MPRIS2.
</p>
<p>
Mitmekülgne kiirsuhtlusklient Kopete võib rühmitada kõik kasutajad, keda pole võrgus, ühtsesse rühma ja näidata kontakti oleku muutumist vestlusaknas. Kontaktide nime muutmise võimalust on uuendatud, näidatavat nime saab muuta ka otse.
</p>
<p>
Tõlkerakendusel Lokalize on senisest etem pooleli olevate tõlgete otsing ning paremini korraldatud failiotsingu kaart. Samuti on nüüd võimalik töötada .TS-failidega. Umbrello võib skeemidele automaatselt kujunduse luua ning eksportida graphvizi dot-jooniseid. Okteta võttis kasutusele erinevad vaateprofiilid.
</p>
<h2>Rakendustekogum Kontact</h2>
<p>
Maailma kõige mitmekülgsem PIM-rakenduste kogum Kontact on saanud rohkelt veaparandusi ja ka jõudlus on paranenud. Käesolevas väljalaskes on uuteks võimalusteks importimisnõustaja, mis lubab Thunderbirdist ja Evolutionist tuua KDE PIM-i üle seadistused, kirjad, kalendri ja aadressiraamatu. Uus on ka tööriist, millega saab kirju, seadistusi ja metaandmeid varundada ja taastada. Tagasi on ilmunud KDE 3 aegadest tuntud iseseisv TNEF-manuste näitaja KTnef. KDE PIM võimaldab lõimida ka Google'i ressursse, nii et kasutaja pääseb hõlpsalt ligi oma Google'i kontaktidele ja kalendrile.
</p>
<h2>KDE õpirakendused</h2>

<p>
KDE-Edu tõi välja uue mälu nõudva ja parandava mängu Pairs. Mitmeid täiendusi sai nii õppuritele kui ka õpetajatele mõeldud graafiteooria rakendus Rocs. Algoritme saab nüüd täita samm-sammult, tagasivõtmise ja loobumise võimalused töötavad paremini ning toetatud on ka üksteist katvad graafid. Kstarsis on täiustatud sortimist taevameridiaani ületamise või vaatlusaja järgi ning Digital Sky Survey <a href="http://en.wikipedia.org/wiki/Digitized_Sky_Survey">piltide hankimist</a>.
<div align="center" class="screenshot">
<a href="screenshots/kde49-pairs.png"><img src="screenshots/kde49-pairs-thumb.png" /></a></div>
</p>
<p>
Marble kiirust optimeeriti ja lisati lõimede kasutamise toetus; lihvi sai ka kasutajaliides. Marble marsruudilaienduste hulgast leiab nüüd OSRM-i (Open Source Routing Machine), toetatud on jalgratta- ja jalakäijamarsruudid ning võrguväline andmemudel, mis lubab hallata marsruute ja otsida andmeid ka siis, kui võrguühendust pole. Samuti võib Marble nüüd näidata õhusõidukite asukohti FlightGeari simulaatori vahendusel.
</p>
<h2>KDE mängud</h2>
<p>
KDE mängud said mitmeid uuendusi. Tublisti lihviti KDE mahjonggimängu Kajongg, millel on nüüd mänguvihjed kohtspikritena, täiustatud mängurobot ja isegi võimalus vestelda, kui mängijad on ühe serveri taga (nüüd on oma server ka kajongg.org-il!). KGoldrunner sai mitu uut taset (Gabriel Miltschitzky panus) ning KPatience suudab salvestamisel meelde jätta mängu ajaloo. Väiksemaid uuendusi sai KSudoku, näiteks paremad vihjed, aga ka seitse uut tasapinnalist ja kolm uut ruumilist mänguvarianti.
<div align="center" class="screenshot">
<a href="screenshots/kde49-ksudoku-3d-samurai.png"><img src="screenshots/kde49-ksudoku-3d-samurai-thumb.png" /></a></div>
</p>

<h4>KDE rakenduste paigaldamine</h4>
<?php
  include("boilerplate-et.inc");
?>

<h2>Täna ilmusid veel:</h2>
<h2><a href="plasma-et.php"><img src="images/plasma.png" class="app-icon" alt="KDE Plasma töötsoonid 4.9" width="64" height="64" /> Plasma töötsoonid 4.9 – põhikomponentide täiustused</a></h2>
<p>
Plasma töötsoonide peamistest uuendustest tasub esile tõsta olulisi täiustusi failihalduris Dolphin, X'i terminali emulaatoris Konsole, tegevustes ja aknahalduris Kwin. Neist kõneleb lähemalt <a href="plasma-et.php">'Plasma töötsoonide teadanne'.</a>
</p>
<h2><a href="platform-et.php"><img src="images/platform.png" class="app-icon" alt="KDE arendusplatvorm 4.9"/> KDE platvorm 4.9</a></h2>
<p>
Tänane KDE platvormi väljalase sisaldab veaparandusi, muulaadseid kvaliteediparandusi, võrgutäiustusi ja valmistumist üleminekuks raamistikule 5
</p>
<?php
  include("footer.inc");
?>