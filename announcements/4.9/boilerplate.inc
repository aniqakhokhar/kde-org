<!-- header goes into the specific page -->
<p align="justify">
KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations and CPU architectures such as ARM and x86, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href="http://windows.kde.org">KDE software on Windows</a> site and Apple Mac OS X versions on the <a href="http://mac.kde.org/">KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported. <a href="http://plasma-active.org">Plasma Active</a> is a user experience for a wider spectrum of devices, such as tablet computers and other mobile hardware.
<br />
KDE software can be obtained in source and various binary formats from <a
href="http://download.kde.org/stable/<?php echo $release_full;?>/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions.php">major
GNU/Linux and UNIX systems</a> shipping today.
</p>
<p align="justify">
  <a name="packages"><em>Packages</em></a>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of <?php echo $release_full;?> 
for some versions of their distribution, and in other cases community volunteers
have done so. <br />
<a name="package_locations"><em>Package Locations</em></a>.
For a current list of available binary packages of which the KDE's Release Team has
been informed, please visit the <a href="/info/<?php echo $release_full;?>.php"><?php echo $release;?> Info
Page</a>.
</p>
<p align="justify">
  <a name="source_code"></a>
  The complete source code for <?php echo $release_full;?> may be <a href="/info/<?php echo $release_full;?>.php">freely downloaded</a>.
Instructions on compiling and installing KDE software <?php echo $release_full;?>
  are available from the <a href="/info/<?php echo $release_full;?>.php#binary"><?php echo $release_full;?> Info Page</a>.
</p>

<h4>
System Requirements
</h4>
<p align="justify">
In order to get the most out of these releases, we recommend to use a recent version of Qt, either 4.7.4 or 4.8.0. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>
