<?php

  $release = '4.9';
  $release_full = '4.9.0';
  $page_title = "KDE platvorm 4.9 – parem ühilduvus, lihtsam arendus";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>Teistes keeltes:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>
<p>
KDE annab uhkelt teada KDE platvormi 4.9 väljalaskmisest. KDE platvorm on Plasma töötsoonide ja KDE rakenduste alus. KDE meeskond töötab praegu KDE arendusplatvormi järgmise põlvkonna kallal, mis kannab nimetust "raamistik 5". Ehkki raamistik 5 on valdavalt tagasiulatuvalt ühilduv, on selle baasiks siiski Qt5. Sellega suureneb jaotatavus, mis võimaldab kasutada valikuliselt kindlaid platvormi osi ja vähendada tunduvalt kõikvõimalikke sõltuvusi. Seepärast on KDE platvorm praegu üsna külmutatud olekus: enamik selle kallal nähtud vaevast kulus vigade ja jõudluse parandamisele.
</p>
<p>
Edenenud on QGraphicsView lahutamine Plasmast, millega sillutatakse teed puhtale QML-põhisele Plasmale, samuti on paranenud Qt Quicki toetus KDE teekides ja Plasma töötsoonides.
</p>
<p>
KDE teekides on tehtud süvatasemel üht-teist võrguühenduse vallas. Kõik KDE rakendused suudavad nüüd palju kiiremini ligi pääseda võrgus välja jagatud ressurssidele. NFS-i, Samba ja SSHFS ühendused ning KIO ei loe enam sisse kõiki kataloogi elemente, mis muudab nad palju kiiremaks. Ebavajalike serveripöörduste vähendamisega on kiirust lisatud ka HTTP-protokollile. See on eriti täheldatav loomu poolest võrgus töötavates rakendustes, näiteks Korganizer, mis töötab nende paranduste tagajärjel ligemale 20% kiiremini. Võrguressursside puhul on täiustatud ka parooli salvestamist.
</p>
<p>
Nepomukis ja Sopranos parandati hulk kriitilise tähtsusega vigu, mis muudab töölauaotsingu kiiremaks ja usaldusväärsemaks nii KDE platvormile 4.9 tuginevates rakendustes kui ka töötsoonides. Just jõudluse parandamine ja suurem stabiilsus olidki nende projektide puhul käesolevaks väljalaskeks valmistudes kõige olulisemad eesmärgid.
</p>

<h4>KDE arendusplatvormi paigaldamine</h4>
<?php
  include("boilerplate-et.inc");
?>

<h2>Täna ilmusid veel:</h2>
<h2><a href="plasma-et.php"><img src="images/plasma.png" class="app-icon" alt="KDE Plasma töötsoonid 4.9" width="64" height="64" /> Plasma töötsoonid 4.9 – põhikomponentide täiustused</a></h2>
<p>
Plasma töötsoonide peamistest uuendustest tasub esile tõsta olulisi täiustusi failihalduris Dolphin, X'i terminali emulaatoris Konsole, tegevustes ja aknahalduris Kwin. Neist kõneleb lähemalt <a href="plasma-et.php">'Plasma töötsoonide teadaanne'.</a>
</p>
<h2><a href="applications-et.php"><img src="images/applications.png" class="app-icon" alt="KDE rakendused 4.9"/>Uued ja täiustatud KDE rakendused 4.9</a></h2>
<p>
Täna ilmunud uute ja täiustatud KDE rakenduste seast väärivad märkimist Okular, Kopete, KDE PIM, õpirakendused ja mängud. Neist kõneleb lähemalt <a href="applications-et.php">'KDE rakenduste teadaanne'</a>
</p>
<?php
  include("footer.inc");
?>