<!-- header goes into the specific page -->
<p align="justify">
El software de KDE, incluidas todas sus bibliotecas y aplicaciones, está libremente disponible bajo licencias de Código Abierto. El software de KDE funciona sobre diversas configuraciones de hardware y arquitecturas de CPU, como ARM y x86, sistemas operativos y funciona con cualquier tipo de gestor de ventanas o entorno de escritorio. Además de Linux y otros sistemas operativos basados en UNIX, puede encontrar versiones para Microsoft Windows de la mayoría de las aplicaciones de KDE en el sitio web <a href="http://windows.kde.org">KDE software on Windows</a> y versiones para Apple Mac OS X en el sitio web <a href="http://mac.kde.org/">KDE software on Mac</a>. En la web puede encontrar compilaciones experimentales de aplicaciones de KDE para diversas plataformas móviles como MeeGo, MS Windows Mobile y Symbian, aunque en la actualidad no tienen soporte. <a href="http://plasma-active.org">Plasma Active</a> es una experiencia de usuario para un amplio abanico de dispositivos, como tabletas y otro tipo de hardware 
móvil.
<br />
Puede obtener el software de KDE en forma de código fuente y distintos formatos binarios en <a
href="http://download.kde.org/stable/<?php echo $release_full;?>/">http://download.kde.org</a>, 
y también en <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>
o con cualquiera de los <a href="http://www.kde.org/download/distributions.php">principales
sistemas GNU/Linux y UNIX</a> de la actualidad.
</p>
<p align="justify">
  <a name="packages"><em>Paquetes</em></a>.
  Algunos distribuidores del SO Linux/UNIX tienen la gentileza de proporcionar paquetes binarios de <?php echo $release_full;?> 
para algunas versiones de sus distribuciones, y en otros casos han sido voluntarios de la comunidad
los que lo han hecho. <br />
  Algunos de estos paquetes binarios están disponibles para su libre descarga en el sitio web de KDE <a
href="http://download.kde.org/binarydownload.html?url=/stable/<?php echo $release_full;?>/">http://download.kde.org</a>.
  Paquetes binarios adicionales, así como actualizaciones de los paquetes ya disponibles,
estarán disponibles durante las próximas semanas.
<a name="package_locations"><em>Ubicación de los paquetes</em></a>.
Para una lista actual de los paquetes binarios disponibles de los que el Equipo de Lanzamiento de KDE ha
sido notificado, visite la <a href="/info/<?php echo $release_full;?>.php">Página de información sobre <?php echo $release;?></a>.
</p>
<p align="justify">
  <a name="source_code"></a>
  La totalidad del código fuente de <?php echo $release_full;?> se puede <a href="/info/<?php echo $release_full;?>.php">descargar libremente</a>.
Dispone de instrucciones sobre cómo compilar e instalar el software de KDE <?php echo $release_full;?>
  en la <a href="/info/<?php echo $release_full;?>.php#binary">Página de información sobre <?php echo $release_full;?></a>.
</p>

<h4>
Requisitos del sistema
</h4>
<p align="justify">
Para obtener lo máximo de estos lanzamientos, le recomendamos que use una versión reciente de Qt, ya sea la 4.7.4 o la 4.8.0. Esto es necesario para asegurar una experiencia estable y con rendimiento, ya que algunas de las mejoras realizadas en el software de KDE están hechas realmente en la infraestructura subyacente Qt.<br />
Para hacer un uso completo de las funcionalidades del software de KDE, también le recomendamos que use en su sistema los últimos controladores gráficos, ya que pueden mejorar sustancialmente la experiencia del usuario, tanto en las funcionalidades opcionales como en el rendimiento y la estabilidad general.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Notas de prensa</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>
