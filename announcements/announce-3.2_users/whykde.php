<?php
  $page_title = "The User's Perspective: Why KDE?";
  $site_root = "../../";
  include "header.inc";
?>

<p>There are many reasons why people pick KDE as their desktop
of choice. Some of them include:</p>

<h3>Freedom</h3>

<p>As an Open Source product, KDE grants you freedoms that are not usually afforded the users
of other desktops. There is no single vendor (or group of vendors) that controls KDE. This means
that when you use KDE you are free from vendor lock-in. You can easily switch from one provider
of KDE and KDE-related services to another and know that you will get the same great software you've come to
rely on. You can even &quot;roll your own&quot; by compiling source code released by the KDE Project if you so desire!</p>

<p>You are also free to change, redistribute and use KDE as you see fit without royalties
or burdonsome restrictions. The only stipulation most KDE software has when it comes to making
changes is that your changes must be redistributed under similar Open Source licenses.</p>

<h3>Cost (As in None)</h3>

<p>The freedom from onerous licensing means you can put KDE on as many systems as you would like, and even modify
it to your heart's content at no cost. You might
expect to pay thousands of dollars per seat for all the software that comes with KDE, and
if you purchased commercial closed-source software you probably would. But KDE has no
price tags attached. You can, of course, find companies who would be more than happy
to accept your money in return for support and integration services, and the KDE project
itself would gladly accept your donations. But all such transactions are up to you.</p>

<p>Finally, technology that you are in control of right down to the cost!</p>

<h3>Features</h3>

<p>One of the primary reasons KDE is so rich in
features is that it is Open Source software. Anyone can built on top of KDE, and the best of those additions
are often pulled into KDE itself. This cooperative manner of development has allowed thousands of
people to create an impressive mountain of software. With <a href="http://kde-apps.org">thousands of
third party applications</a> to augment the hundreds of applications that come with KDE, there
is very little that you can't do with KDE. And the vast majority of it is available for free
online!<p>

<p>If you do happen to find yourself needing to do something
that isn't available as a native KDE application, KDE is compatible with all
Linux and UNIX software available today. You can run non-KDE applications in KDE (and vice versa!)
usually with no extra effort on your part. You can even run many Microsoft Windows applications
in KDE by using available <a href="http://www.winehq.com/">Windows compatibility tools</a>.</p>

<h3>Consistency</h3>

<p>With standardized menu layouts, toolbars, dialogs and behaviors KDE applications tend to
be very consistent. This makes KDE an easy to learn, friendly and usable solution
that appeals to many users.</p>

<h3>Customizability</h3>

<p>KDE is highly customizable and can be modified to suit just about any tastes or needs. In
corporate or public settings where control over the desktop is important, KDE's Kiosk system allows
for fine grained management of the desktop and what is allowed. In more relaxed settings,
KDE offers a wide array of easily accessable configuration panels which allow KDE to be tweaked
to perfection. Whatever that happens to mean for you.</p>

<h3>Internationalization</h3>

<p>Sporting translations in almost 70 languages, support for various calendaring systems,
the ability to follow regional customs for weights and measures and options for different international
keyboard layouts, KDE is about as global-friendly as it gets. For many people, KDE has been
the first desktop that they have been able to use in their own language that respects their
region's or country's local policies.</p>

<h3>Ubiquity</h3>

<p>KDE is one of the most widely available desktop systems today. Besides being available on most
desktop Linux distributions and many flavors of UNIX, it is available on MacOS X and even to a
limited extent on Microsoft Windows. The ubiquitous nature of KDE means that when you install
or use a Linux or UNIX operating system, chances are that KDE will be available to you.
This allows you to comfortably take your knowledge of KDE from one machine to another.</p>

<h3>Community</h3>

<p>A large global community of users has grown around KDE. You may never
actively engage this community, but the software you are using is a direct result of it. Should
you ever have a KDE question, or need a program that does something in specific, or just want to tailor
KDE to your preferences the user community is a large and wealthy resource just
waiting to be tapped.</p>

<p>A <a href="http://www.kde.org/family/">constellation of web sites</a>, online chat
forums, email lists and user contributed documentation is a gold mine of information that
springs from the community of users. Best of all, you are able and encouraged to participate
as well.</p>

<p>A special subset of the user community is the developer community. This segment of users
helps create and maintain KDE, and there are several ways to communicate with
them. Many developers maintain public <a href="http://kdedevelopers.org/blog">web logs</a>
and the <a href="http://bugs.kde.org">KDE Bug Tracking System</a> provides a way to offer feedback
 on KDE (including feature requests) to the developers. You'll often find KDE developers participating
in discussions on web boards and in user-oriented mailing lists.</p>

<p>This vibrant and lively community gives KDE something that most technologies lack: a human
aspect.</p>

<h3>Support</h3>

<p>While the KDE community is a great resource, often times a business partner that can be
contracted to provide support is desirable. This is especially true in business settings. Fortunately, as the most
widely available Open Source desktop, it is easy to find technical support for KDE. <a href="http://www.kde.org/download/distributions.php">Operating systems that ship with KDE</a> also
provide support for KDE, as do <a href="http://enterprise.kde.org/bizdir/">many independent
consulting firms</a>. These companies can provide services such as custom KDE development, training and deployment expertise.</p>

<table style="border: solid 1px;" align="center" cellpadding="6" cellspacing="0">
<tr>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">Users</th>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">Developers</th>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">About KDE</th>
</tr>
<tr>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap">
<a href="whykde.php">Why KDE?</a><br/>
<a href="deploying.php">Deploying KDE</a><br/>
<a href="supporting.php">Supporting KDE</a>
</td>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap">
<a href="../announce-3.2_developers/whykde.php">Why KDE?</a><br/>
<a href="../announce-3.2_developers/developing.php">Developing With KDE</a><br/>
<a href="../announce-3.2_developers/supporting.php">Supporting KDE</a>
</td>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap"><a href="http://www.kde.org">The KDE Project</a><br/>
<a href="http://www.kde.org/areas/kde-ev/">KDE e.V.</a><br/>

</td>
</tr>
</table>

<?php
  include("footer.inc");
?>
