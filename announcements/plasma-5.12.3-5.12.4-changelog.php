<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.12.4 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.12.4";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>- Moved shadowSize from anonymous namespace to static member function. <a href='https://commits.kde.org/breeze/f5ad0557c4d4dd2d7fd60d94d4f6e3f627c8abf0'>Commit.</a> </li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Simplify the tasks view. <a href='https://commits.kde.org/discover/1d7b96ec879eb59107b915e52f31941d7480ef0e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391760'>#391760</a></li>
<li>Remove unneeded includes. <a href='https://commits.kde.org/discover/125b56ce874115fe59ea3a1745f3fa669bde8703'>Commit.</a> </li>
<li>Fix dbus path. <a href='https://commits.kde.org/discover/6e1ca8e0518a6d7e3d36af77796d1040aee03ba7'>Commit.</a> </li>
<li>Include a test for cancelling flatpak installations. <a href='https://commits.kde.org/discover/b73e08da262316a2f08f07d4b4d68024fb687064'>Commit.</a> See bug <a href='https://bugs.kde.org/391941'>#391941</a></li>
<li>Improve status of FlatpakTransaction. <a href='https://commits.kde.org/discover/f908c57b9f401ed3d04ade374c02f0ea3cee6ca7'>Commit.</a> </li>
<li>Remove pointless i18n call. <a href='https://commits.kde.org/discover/41737108aaa9d5a5eb7777cf71ab7ac724366cfb'>Commit.</a> </li>
<li>KNS: Only indicate that it's fetching if already fetching reviews. <a href='https://commits.kde.org/discover/476cb92d2369c523bb7a4f0a43b81e30bb3a3cfd'>Commit.</a> </li>
<li>Include a test that makes sure we can list origins. <a href='https://commits.kde.org/discover/b15639cbba4b285301dd0359bfbd9725165eeab1'>Commit.</a> See bug <a href='https://bugs.kde.org/391126'>#391126</a></li>
<li>Solve some inconsistencies found when testing flatpak. <a href='https://commits.kde.org/discover/e546dcc591cccf3fbc61d9344a78c63fe1b82a18'>Commit.</a> </li>
<li>Introduce a flatpak test. <a href='https://commits.kde.org/discover/340380772aaa414de6ad2b1421791dfa2ee938b8'>Commit.</a> </li>
<li>No need to pass the extra arguments on the Update Action. <a href='https://commits.kde.org/discover/cc92bedbfa6bb99d9d819d3b6c5b478c1fc2ea83'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391801'>#391801</a></li>
<li>Add Qt5::Xml. <a href='https://commits.kde.org/discover/f81c9a79d036c4f3a81ed75711f2aea9cbecc888'>Commit.</a> </li>
<li>Simplify KNSTransaction creation. <a href='https://commits.kde.org/discover/622b94e370894161258178357400f07be0eac6be'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390236'>#390236</a></li>
<li>Fix test. <a href='https://commits.kde.org/discover/1e4ecc8c06c9caaae0ffe73c86eeeb1ac0d0aa3d'>Commit.</a> </li>
<li>Add a test to make sure we don't add resources with the wrong URL. <a href='https://commits.kde.org/discover/0aea4dc634ac9668aae46b4e762a4d96ba27f852'>Commit.</a> </li>
<li>Kns: Fix logic for adding screenshots. <a href='https://commits.kde.org/discover/81f801a69aaa456a9f4288f76dc74b3f6fccc4f8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391190'>#391190</a></li>
<li>Add debug information when the searched URI is wrong. <a href='https://commits.kde.org/discover/62f6b54326f54a61a00b5950fde1a50e6ad56d41'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>[konsoleprofiles applet] Do not try to set plasmoid.aspectRatioMode. <a href='https://commits.kde.org/kdeplasma-addons/14447a1fc1e6d3191aa6313fce30b704fdaf57a1'>Commit.</a> </li>
<li>Fix default colour used by Swap monitor. <a href='https://commits.kde.org/kdeplasma-addons/dad98acf635d18be762f8ef55945e89511454a3e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11559'>D11559</a></li>
<li>Drop unused X-Plasma-Requires-* entries from applet metadata. <a href='https://commits.kde.org/kdeplasma-addons/02432d255a6591f12c360d3b133bbcc4ffb8c187'>Commit.</a> </li>
<li>Remove long-time deprecated Encoding=UTF-8 from desktop format files. <a href='https://commits.kde.org/kdeplasma-addons/bf0f188013c444c91b5907f348822cd3423e4276'>Commit.</a> </li>
</ul>


<h3><a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a> </h3>
<ul id='ulkinfocenter' style='display: block'>
<li>Kcm_energyinfo: Allow changing of the timespan if no data available. <a href='https://commits.kde.org/kinfocenter/fadf1008e452982cad431d8008930ca7fe97d395'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11712'>D11712</a></li>
</ul>


<h3><a name='kmenuedit' href='https://commits.kde.org/kmenuedit'>KMenuEdit</a> </h3>
<ul id='ulkmenuedit' style='display: block'>
	<li>KMenuEdit: Fix pixelated icon scaling for HiDPI screens. <a href='https://commits.kde.org/kmenuedit/e8e3c0f8e4a122ffc13386ab55b408aba2d29e14'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390737'>#390737</a>. Phabricator Code review <a href='https://phabricator.kde.org/D11631'>D11631</a></li>
	
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>[colorcorrection] Apply current color temperature on screen hot plug. <a href='https://commits.kde.org/kwin/99fec3d24597acaef16b3e2b11b24bf647789919'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391623'>#391623</a>. Phabricator Code review <a href='https://phabricator.kde.org/D11654'>D11654</a></li>
<li>[effects/coverswitch] Fix broken reflection on multi-monitor setup. <a href='https://commits.kde.org/kwin/2eded918977f8c8cefdd5c938d11fc57dc9ce1fe'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378688'>#378688</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10465'>D10465</a></li>
<li>[AppMenu] Ignore show request when application menu isn't configured. <a href='https://commits.kde.org/kwin/c99d329125ac745d4c764c5262a8010e9b565fde'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/392012'>#392012</a>. Phabricator Code review <a href='https://phabricator.kde.org/D11580'>D11580</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>[Folder View] Fix selecting text with Shift+Home/End. <a href='https://commits.kde.org/plasma-desktop/5d719c1bb374914952eeedeb2b5a931881a9a1d0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/392318'>#392318</a>. Phabricator Code review <a href='https://phabricator.kde.org/D11708'>D11708</a></li>
<li>Make Select/Deselect All operate on the filtered rows. <a href='https://commits.kde.org/plasma-desktop/8e15d7883ee6567ef28f1b5f57e7dee3aeae2291'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11665'>D11665</a></li>
<li>Remove long-time deprecated Encoding=UTF-8 from desktop format files. <a href='https://commits.kde.org/plasma-desktop/e908d99c2fe62d8dc6858241d0aa4b35ea7bccdd'>Commit.</a> </li>
<li>[touchpad kded] Work around kded fail on X-KDE-OnlyShowOnQtPlatforms lists. <a href='https://commits.kde.org/plasma-desktop/830ff8deb4772a0042a4878f9dc529661c0555a0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11126'>D11126</a></li>
<li>[Touchpad KCM] Sync values to UI on Defaults in Wayland session. <a href='https://commits.kde.org/plasma-desktop/bac84005811927891bc1dcaf1b8f68123ea7dccc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/392029'>#392029</a>. Phabricator Code review <a href='https://phabricator.kde.org/D11471'>D11471</a></li>
<li>[Workspace Options KCM] sync() after writing settings. <a href='https://commits.kde.org/plasma-desktop/06c837e274ed38de16ae86b41b31ce9e5300c4b0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11327'>D11327</a></li>
<li>Bad textarea width for rename file if filename too short. <a href='https://commits.kde.org/plasma-desktop/d6818eac6fbe495facf558b41cd570604bb7cbcb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386546'>#386546</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10837'>D10837</a></li>
<li>Fix scrollbar on empty search results. <a href='https://commits.kde.org/plasma-desktop/7ddd20cfd35dff528463b04e6e7d0e1db7f9eb94'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391644'>#391644</a>. Phabricator Code review <a href='https://phabricator.kde.org/D11254'>D11254</a></li>
<li>Kimpanel: Fix the window positioning when plasma uses native-qt scaling. <a href='https://commits.kde.org/plasma-desktop/7d3ffa0400ed8444512915c3b7906a98619175c5'>Commit.</a> </li>
<li>Load correct wallpaper plugin configuration in the dialog. <a href='https://commits.kde.org/plasma-desktop/b0304a2ef312a7f61dc7896521a6f54e93043a16'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11194'>D11194</a></li>
<li>Don't skip certain recent documents in kicker and taskmanager. <a href='https://commits.kde.org/plasma-desktop/ca96f850efbf290a9c7252160eae12f0dd45b2cf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10890'>D10890</a></li>
<li>Improve multi-desktop folderview behavior. <a href='https://commits.kde.org/plasma-desktop/8f1ddca3cfcafbc604bb97fb0676ae877a6c14a8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390676'>#390676</a>. See bug <a href='https://bugs.kde.org/389745'>#389745</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10728'>D10728</a></li>
<li>Use QUrl in the ScreenMapper API. <a href='https://commits.kde.org/plasma-desktop/cb1a2f8e70a0ea49e38792e3771397014936f7d1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9325'>D9325</a></li>
<li>Use opacity instead of visible. <a href='https://commits.kde.org/plasma-desktop/eff339237986bec675303bf512be2bc2d631b90b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391432'>#391432</a>. Phabricator Code review <a href='https://phabricator.kde.org/D11098'>D11098</a></li>
</ul>


<h3><a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>Call beginRemoveRows before removing the data. <a href='https://commits.kde.org/plasma-pa/722ee38399a1938c5d8e97eeda2e54e3235b3311'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11613'>D11613</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[notifications applet] Fix two qml warnings about assigning [undefined]. <a href='https://commits.kde.org/plasma-workspace/5d65fcbf8bf95cb1a814b81aa26d5b477a8549db'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11743'>D11743</a></li>
<li>Remove long-time deprecated Encoding=UTF-8 from desktop format files. <a href='https://commits.kde.org/plasma-workspace/45a19fa16c45f3439e302a77b8418f0926e6f5d9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11563'>D11563</a></li>
<li>[Run Command] Pass "event" to runCurrentIndex. <a href='https://commits.kde.org/plasma-workspace/68dd0f46143406e926bbd7990af50fda733e9e92'>Commit.</a> </li>
<li>Move to KRunner's second results item with a single keypress. <a href='https://commits.kde.org/plasma-workspace/b8ffa755ed2df6bf6a47b9268e7da93355a4d856'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/392197'>#392197</a>. Phabricator Code review <a href='https://phabricator.kde.org/D11611'>D11611</a></li>
<li>Pass proper click coordinates to SNI ContextMenu calls. <a href='https://commits.kde.org/plasma-workspace/a4a0d5bfd0dabf6be5651244ebe3459b4a257ce2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11600'>D11600</a></li>
<li>Libdbusmenu-qt: Remove nonexistant actions directly from the menu. <a href='https://commits.kde.org/plasma-workspace/8827bb38806e8266bec6e0ec23fedf8c1955c842'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11586'>D11586</a></li>
<li>Set a transient parent for SNI context menus. <a href='https://commits.kde.org/plasma-workspace/cf2d64fa9718c27f5f36101c9d2eb8dda79eeb77'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11584'>D11584</a></li>
<li>[DeviceNotifications Engine] Never show safely remove message when mounting. <a href='https://commits.kde.org/plasma-workspace/f449adc1993626f1dc7e9216068ab6778e397782'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391180'>#391180</a>. Phabricator Code review <a href='https://phabricator.kde.org/D11504'>D11504</a></li>
<li>Use dbus-run-session instead of dbus-launch --exit-with-session. <a href='https://commits.kde.org/plasma-workspace/19bee4105feb3e8de0fc1e170bf36260d6c2b3b2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11478'>D11478</a></li>
<li>[System Tray] Fix hiding applets with HiddenStatus. <a href='https://commits.kde.org/plasma-workspace/de715e8c5d95e1cbb8a23465fa4bd9bb932753ef'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11335'>D11335</a></li>
<li>[Notifications] Remove NoAccessNetworkAccessManagerFactory. <a href='https://commits.kde.org/plasma-workspace/71f16881843915187af4dee0a7ce2ec85bfb400c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11109'>D11109</a></li>
<li>Improve By Desktop sorting for task groups. <a href='https://commits.kde.org/plasma-workspace/a9df1e20f527dadcc43bcbbad48a9de52f83d9dd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/384747'>#384747</a>. Phabricator Code review <a href='https://phabricator.kde.org/D11257'>D11257</a></li>
<li>Use separate config group for each wallpaper plugin. <a href='https://commits.kde.org/plasma-workspace/bb5645d8c6e7ac0572462220869d02d42fc74525'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11195'>D11195</a></li>
<li>Fix duplicated [Desktop Entry] sections in two menu directory files. <a href='https://commits.kde.org/plasma-workspace/a167e25092a7f2d3c21e1d46118915ad19133ac9'>Commit.</a> </li>
</ul>


</main>
<?php
	require('../aether/footer.php');
