<?php
  $release = '4.5';
  $release_full = '4.5.0';
  $page_title = "La plataforma de desarrollo de KDE 4.5.0 mejora su rendimiento, su estabilidad e introduce una nueva cach&eacute; de alta velocidad y el uso de WebKit";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<p align="justify">
KDE, una comunidad internacional de software libre, tiene el orgullo de publicar la plataforma KDE 4.5. Esta base desarrollada por KDE para el espacio de trabajo Plasma y para las aplicaciones ha experimentado mejoras significativas en muchas &aacute;reas. Adem&aacute;s de la introducci&oacute;n de nueva tecnolog&iacute;a, se ha trabajado mucho para mejorar el rendimiento y la estabilidad de los pilares fundamentales del desarrollo de KDE. La plataforma KDE ha progresado en muchas &aacute;reas, tanto en t&eacute;rminos de infraestructura como de herramientas de desarrollo.
</p>

<p align="justify">
<ul>
    <li>Tras a&ntilde;os de trabajo, la integraci&oacute;n de <b>WebKit</b> ya forma parte de nuestras bibliotecas. Ahora, WebKit proporciona una integraci&oacute;n con el almacenamiento de contrase&ntilde;as y otras caracter&iacute;sticas de Konqueror, el navegador de KDE. Otras aplicaciones pueden hacer uso de WebKit para mostrar contenidos con el mismo nivel de integraci&oacute;n que KHTML.</li>

    <li>KHTML contin&uacute;a su desarrollo con la compatibilidad con las consultas XPath.</li>

    <li>El rendimiento de los contenidos web se ha mejorado mediante una nueva pol&iacute;tica de planificaci&oacute;n que admite mayor n&uacute;mero de descargas de contenidos en paralelo de diferentes servidores, permitiendo que las p&aacute;ginas se generen m&aacute;s r&aacute;pidamente ya que sus componentes se cargan juntos. La velocidad de generaci&oacute;n de p&aacute;ginas de los motores de KHTML y de WebKit tambi&eacute;n se benefician de estos cambios.</li>

    <li>Ahora, <b>el espacio de trabajo Plasma</b> se puede configurar mediante plantillas de JavaScript que se pueden distribuir en forma de peque&ntilde;os paquetes aparte. Esto permite que los administradores del sistema y los integradores preparen configuraciones personalizadas de Plasma para los usuarios o que cambien la configuraci&oacute;n predeterminada de un escritorio Plasma est&aacute;ndar.</li>

    <li><b>KSharedDataCache</b> es un mecanismo de cach&eacute; para aplicaciones. Esta nueva cach&eacute; en memoria ya se utiliza para los iconos y ha demostrado notables aumentos de velocidad en esa &aacute;rea.</li>

    <li>Ahora, los desarrolladores de aplicaciones que hagan uso de la <b>interfaz SmartRange de Kate</b> para sus componentes de editor tienen asu disposici&oacute;n varias interfaces nuevas, de las cuales se pueden consultar m&aacute;s detalles en <a href="http://kate-editor.org/2010/07/13/kde-4-5-smartrange-movingrange/">esta entrada de blog.</a>.</li>

    <li>Como KDE Games da la bienvenida a su primera aplicaci&oacute;n completamente escrita en Python, los desarrolladores de bindings de KDE han a&ntilde;adido bindings de Perl al conjunto del lenguaje implementado oficialmente. Tambi&eacute;n se ha <a href="http://www.arnorehn.de/blog/2010/07/and-the-bindings-keep-rocking-writing-ruby-kio-slaves/">mejorado significativamente</a>. el uso de Ruby en la plataforma KDE.</li>

    <li>Ahora, la biblioteca Phonon Multimedia puede utilizar PulseAudio de manera opcional.</li>

</ul>
Estos cambios constiyuyen los fundamentos para muchas de las mejoras en <a href="plasma-es.php">el escritorio Plasma y en Plasma Netbook</a> as&iacute; como en las<a href="applications-es.php">aplicaciones de KDE 4.5</a> que se han publicado de manera conjunta con la plataforma de desarrollo KDE 4.5.0.
</p>

<h2>Planes y trabajo en procceso</h2>
<p align="justify">
El equipo de la plataforma KDE est&aacute; trabajando para hacerla m&aacute;s adecuada para dispositivos port&aacute;tiles. Con la introducci&oacute;n de los perfiles de la plataforma KDE, es posible compilar KDE con configuraciones de caracter&iacute;sticas limitadas con el fin de minimizar su huella. Dichos perfiles tambi&eacute;n se est&aacute;n desarrollando para satisfacer las necesidades de las aplicaciones m&oacute;viles. Los planes incluyen su uso para las versiones m&oacute;viles de Kontact, KOffice y el espacio de trabajo Plasma Mobile. El marco de trabajo BlueDevil a&ntilde;adir&aacute; una mejor compatibilidad para numerosos dispositivos Bluetooth en versiones posteriores. BlueDevil, que est&aacute; basado en BlueZ y que se encuentra en muchos sistemas Linux, est&aacute; madurando r&aacute;pidamente y en la actualidad est&aacute; pasando por una serie de versiones beta. Sus desarrolladores planean publicar una nueva versi&oacute;n con la plataforma KDE 4.6 en enero de 2011.
</p>

<h4>Instalaci&oacute;n de la plataforma de desarrollo de KDE</h4>
<?php
  include("boilerplate-es.inc");
?>

<h2>Tambi&eacute;n publicados hoy:</h2>


<a href="plasma-es.php">
<img src="images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.5.0" width="64" height="64" />
</a>

<h2>Plasma para escritorio y netbooks 4.5.0: experiencia de usuario mejorada</h2>
<p align="justify">
10 de agosto de 2010. Hoy, KDE publica los espacios de trabajo del escritorio Plasma y Plasma para netbooks 4.5.0. El escritorio Plasma ha refinado mucho su usabilidad. Se ha mejorado la eficiencia de los flujos de gesti&oacute;n de notificaciones y trabajos, el &aacute;rea de notificaciones se ha "limpiado" a nivel visual, y la gesti&oacute;n de entrada de datos entre aplicaciones es m&aacute;s consistente al utilizar el protocolo de notificaciones de Freedesktop.org ya introducido en la versi&oacute;n anterior de Plasma. <a href="plasma-es.php"><b>Leer el anuncio completo</b></a>
</p>


<a href="applications-es.php">
<img src="images/applications.png" class="app-icon" alt="The KDE Applications 4.5.0"/>
</a>
<h2>Las aplicaciones de KDE 4.5.0 mejoran su usabilidad e incluyen rutas de mapas</h2>
<p align="justify">
10 de agosto de 2010. Hoy, el equipo de KDE publica una nueva versi&oacute;n de las aplicaciones de KDE. Se han mejorado muchos t&iacute;tulos educativos, herramientas, juegos y utilidades gr&aacute;ficas a nivel funcional y de usabilidad. Marble, el globo virtual, incluye rutas bas&aacute;ndose en OpenRouteService. Konqueror, el navegador web de KDE se puede configurar para utilizar WebKit mediante el componente KWebKit disponible en el repositorio Extragear. <a href="applications-es.php"><b>Leer el anuncio completo</b></a>
</p>

<?php
  include("footer.inc");
?>
