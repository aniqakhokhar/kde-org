<?php

# This file (should) contain an up to date list of our global press contacts. It is included in
# for example the release announcements and the http://www.kde.org/contact/representative.php page.

?>

<table cellpadding="10"><tr valign="top">
<td>

<b>Африка</b><br />

AJ Venter<br />
Unit 7B Beauvallon Village<br />
13 Sandown Road<br />
West Beach<br />
Cape Town<br />
7441<br />
South-Africa<br />
Phone: +27 83 455 9978<br />
<a href="mai&#108;&#116;&#111;&#58;&#105;nf&#111;-&#97;f&#114;i&#99;a&#64;&#107;de.o&#114;&#103;">info-af&#114;&#105;&#99;a&#64;&#107;d&#101;&#46;&#111;&#114;&#103;</a><br />
</td>

<td>
<b>Азия</b><br />
     Pradeepto Bhattacharya<br/>
     A-4 Sonal Coop. Hsg. Society<br/>
     Plot-4, Sector-3,<br/>
     New Panvel,<br/>
     Maharashtra.<br/>
     India 410206<br/>
     Phone : +91-9821033168<br/>
<a href="ma&#0105;&#108;to&#00058;inf&#00111;-&#97;&#115;&#x69;a&#x40;kde.or&#x67;">inf&#00111;-&#97;si&#97;&#64;kde.or&#x67;</a>
</td>

<td>
<b>Европа</b><br />
Sebastian K&uuml;gler<br />
Wolfstraat 154<br />
6531 LR Nijmegen<br />
The Netherlands<br />
Phone: +31-6-48370928<br />
<a href="&#109;a&#105;l&#116;&#111;&#58;&#105;&#110;f&#111;&#45;eu&#114;ope&#64;&#107;d&#101;.&#111;&#114;g">info-&#101;&#117;&#114;o&#112;e&#64;&#107;de.o&#114;g</a>
</td>


</tr>
<tr valign="top">

<td>
<b>Северная Америка</b><br />
Jeff Mitchell <br />
21 Kinross Rd. #2 <br />
Brighton, MA 02135 <br />
U.S.A <br />
Phone: +1 (762) 233-4KDE (4533) <br />
<a href="mail&#116;&#111;:&#105;&#110;&#102;o-nort&#104;&#97;m&#101;rica&#64;k&#100;e.&#111;&#114;&#103;">info-n&#111;rt&#104;a&#109;&#101;&#114;i&#99;&#97;&#64;&#107;&#100;e.o&#114;&#103;</a><br />
</td>

<td>
<b>Океания</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="m&#97;&#105;&#108;t&#111;:&#105;&#110;f&#111;&#45;oc&#101;a&#110;ia&#64;&#107;&#100;e&#46;&#111;&#114;g">info-&#111;c&#101;&#97;&#110;ia&#64;&#107;&#100;e&#46;org</a><br />
</td>

<td>
<b>Южная Америка</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)8808-1519<br />
<a href="mai&#108;&#116;o&#58;&#105;&#110;fo-&#115;out&#104;&#97;meric&#97;&#64;&#107;d&#101;&#46;or&#103;">info-s&#111;ut&#104;&#97;mer&#105;&#99;a&#64;&#107;&#100;e.&#111;&#114;g</a><br />
</td>
</tr></table>

