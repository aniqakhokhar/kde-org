<?php
  $release = '4.5';
  $release_full = '4.5.0';
  $page_title = "KDE publica la versi&oacute;n 4.5.0 de la plataforma de desarrollo, las aplicaciones y el espacio de trabajo Plasma";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>
<!--
<p>Also available in:
-->
<?php
  include "../announce-i18n-bar.inc";
?>
<!--</p>
</h3>
-->
<div style="float: right; padding: 1ex; margin: 1ex; border: 0px"> 
<p><a href="https://www.kde.org/announcements/4.5/"><img src="images/kde45-small.png" /></a></p> 
</div> 
<p align="justify">
Hoy, con la versi&oacute;n 4.5.0 se publican nuevas versiones de la plataforma de desarrollo de KDE, los espacios de trabajo del escritorio Plasma y Plasma para netbooks, adem&aacute;s de muchas aplicaciones. El equipo de KDE se centrado en la usabilidad, el rendimiento y la estabilidad de muchas caracter&iacute;sticas y tecnolog&iacute;as introducidas anteriormente. A continuaci&oacute;n encontrar&aacute; los 3 anuncios independientes de cada producto de KDE: la plataforma de desarrollo, la compilaci&oacute;n de aplicaciones y el entorno de trabajo Plasma.

<h2>La plataforma de desarrollo de KDE 4.5.0 mejora su rendimiento, estabilidad, introduce una nueva cach&eacute; de alta velocidad y el uso de WebKit</h2>

<a href="platform-es.php">
<img src="images/platform.png" class="app-icon" alt="La plataforma de desarrollo de KDE 4.5.0"/>
</a>

<p align="justify">
10 de agosto de 2010. Hoy, KDE publica la plataforma de desarrollo de KDE 4.5.0. Esta versi&oacute;n incluye muchas mejoras de rendimiento y estabilidad. La nueva <b>KSharedDataCache</b> est&aacute; optimizada para un acceso r&aacute;pido a los recursos almacenados en disco, como los iconos. La nueva biblioteca <b>KDE WebKit</b> proporciona integraci&oacute;n con las preferencias de red, almacenamiento de contrase&ntilde;as y muchas otras caracter&iacute;sticas de Konqueror. <a href="platform-es.php"><b>Leer el anuncio completo</b></a>
</p>



<h2>Plasma para escritorio y netbooks 4.5.0: experiencia de usuario mejorada</h2>
<p align="justify">

<a href="plasma-es.php">
<img src="images/plasma.png" class="app-icon" alt="Los espacios de trabajo de Plasma 4.5.0" width="64" height="64" />
</a>

10 de agosto de 2010. Hoy, KDE publica los espacios de trabajo del escritorio Plasma y Plasma para netbooks 4.5.0. El escritorio Plasma ha refinado mucho su usabilidad. Se ha mejorado la eficiencia de los flujos de gesti&oacute;n de notificaciones y trabajos, el &aacute;rea de notificaciones se ha &quot;limpiado&quot; a nivel visual, y la gesti&oacute;n de entrada de datos entre aplicaciones es m&aacute;s consistente al utilizar el protocolo de notificaciones de Freedesktop.org ya introducido en la versi&oacute;n anterior de Plasma. <a href="plasma-es.php"><b>Leer el anuncio completo</b></a>
</p>


<h2>Aplicaciones de KDE 4.5.0: usabilidad mejorada y rutas de mapas</h2>
<p align="justify">

<a href="applications-es.php">
<img src="images/applications.png" class="app-icon" alt="Las aplicaciones de KDE 4.5.0"/>
</a>

10 de agosto de 2010. Hoy, el equipo de KDE publica una nueva versi&oacute;n de las aplicaciones de KDE. Se han mejorado muchos t&iacute;tulos educativos, herramientas, juegos y utilidades gr&aacute;ficas a nivel funcional y de usabilidad. Marble, el globo virtual, incluye rutas bas&aacute;ndose en OpenRouteService. Konqueror, el navegador web de KDE se puede configurar para utilizar WebKit mediante el componente KWebKit disponible en el repositorio Extragear. <a href="applications-es.php"><b>Leer el anuncio completo</b></a>
</p>



<?php
    showScreenshot("kde-general45.png", "La plataforma de desarrollo, las aplicaciones y el escritorio Plasma y Plasma para netbooks de KDE en su versi&oacute;n".$release);
?>


<h4>
    Corra la voz y vea lo que ocurre: Utilice la etiqueta "KDE"
</h4>
<p align="justify">
La comunidad de KDE anima a todo el mundo a <strong>correr la voz</strong> en la web social. Env&iacute;e art&iacute;culos a sitios web, use canales como delicious, digg, reddit, twitter o identi.ca. Suba capturas de pantalla a servicios como Facebook, Flickr, ipernity o Picasa y publ&iacute;quelas en los grupos adecuados. Cree demostraciones en v&iacute;deo y env&iacute;elas a YouTube, Blip.tv, Vimeo y otros sitios. No olvide etiquetar el material que env&iacute;e con la <em>etiqueta <strong>kde</strong></em> para que sea m&aacute;s f&aacute;cil para todo el mundo encontrar el material, y para el equipo de KDE compilar recortes del anuncio de KDE SC <?php echo $release;?>. <strong>�Ay&uacute;denos a correr la voz, sea parte del proyecto!</strong></p>

<p align="justify">
Puede seguir en directo lo que ocurre en torno a la liberaci&oacute;n de <?php echo $release?> en la web social a trav&eacute;s del nuevo <a href="http://buzz.kde.org"><strong>live feed de la Comunidad de KDE</a>. Este sitio agrega en tiempo real lo que ocurre en identi.ca, twitter, youtube, flickr, picasaweb, blogs y en muchas otras redes sociales. Puede encontrar el livefeed en <strong><a href="http://buzz.kde.org">buzz.kde.org</a></strong>.
</p>

<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/search?s=kde45"><img src="buttons/digg.gif" alt="Digg" title="Digg" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/search?q=kde45"><img src="buttons/reddit.gif" alt="Reddit" title="Reddit" /></a>
    </td>
    <td>
        <a href="http://twitter.com/#search?q=kde45"><img src="buttons/twitter.gif" alt="Twitter" title="Twitter" /></a>
    </td>
    <td>
        <a href="http://identi.ca/search/notice?q=kde45"><img src="buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde45"><img src="buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde45"><img src="buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com/#!/pages/K-Desktop-Environment/6344818917?ref=ts"><img src="buttons/facebook.gif" alt="Facebook" title="Facebook" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde45"><img src="buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
    </td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbotones</a></span>
</div>

<h4>Apoye a KDE</h4>

<a href="http://jointhegame.kde.org/"><img src="images/join-the-game.png" width="231" height="120"
alt="Join the Game" align="left"/> </a>
<p align="justify">Ya est&aacute; abierto el nuevo <a
href="http://jointhegame.kde.org/">programa de Miembros de Apoyo</a> de KDE e.V. Por 25 &euro; trimestrales asegurar&aacute; que la comunidad internacional de KDE siga desarrollando Software Libre de uso a nivel mundial.</p>
<br clear="all" />
<p>&nbsp;</p>
<?php
  include("footer.inc");
?>