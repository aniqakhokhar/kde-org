<?php
  $release = '4.5';
  $release_full = '4.5.0';

  $page_title = "Las aplicaciones de KDE 4.5.0 mejoran su usabilidad e incluyen rutas de mapas";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<!--
<h2>KDE Applications 4.5 Released</h2>

<table width="100%">
    <tr>
        <td width="50%">
                <a href="plasma.php">
                <img src="images/desktop-32.png" />
                Previous Page: KDE Workspaces
                </a>
        </td>
        <td align="right" width="50%">
                <a href="platform-es.php">Next page: Network and Personal Information Management
                <img src="images/internet-32.png" />
		</a>
        </td>
    </tr>
</table>
-->
<p>
KDE, una comunidad de Software Libre internacional, se complace en anunciar la disponibilidad inmediata de las aplicaciones de KDE 4.5. Ya se trate de los juegos de alta calidad, el software educativo y de producci&oacute;n o las &uacute;tiles herramientas, todas las aplicaciones han mejorado y se ha facilitado su uso.
</p>

<?php
    showScreenshot("apps-overview.png", "Aplicaciones matem&aacute;ticas, de edici&oacute;n de texto y de entretenimiento");
?>

<p>
Las aplicaciones de KDE han mejorado en muchos aspectos. A continuaci&oacute;n se destacan algunos de ellos.
</p>
    <ul>

   <li>El equipo de juegos de KDE ha estado trabajando en nuevas caracter&iacute;sticas y en un nuevo juego: <b>Kajongg</b>, el <a href="http://en.wikipedia.org/wiki/Mahjong">Mahjongg original para 4 jugadores</a>. Dicho juego est&aacute; escrito totalmente en Python, convirti&eacute;ndose en la primera gran aplicaci&oacute;n en Python que forma parte de la familia de juegos de KDE. El juego <a href="http://www.kde.org/applications/games/konquest/">Konquest</a>, donde el jugador debe conquistar la galaxia, ahora permite al usuario crear y personalizar sus propios mapas. Tambi&eacute;n se ha facilitado la configuraci&oacute;n del <a href="http://www.kde.org/applications/games/palapeli">juego de puzzles Palapeli</a>, se ha incluido un nuevo conjunto de niveles de KGoldRunner (&quot;Demolition&quot;), y mejorado la integraci&oacute;n con archivos <a href="http://en.wikipedia.org/wiki/Smart_Game_Format">SGF</a> de <a href="http://www.kde.org/applications/games/kigo/">Kigo</a>.
    </li>

    <li>Las <b>aplicaciones educativas</b> tambi&eacute;n se han mejorado mucho. La mayor corresponde al ayudante al aprendizaje <a href="http://www.kde.org/applications/education/parley/">Parley</a>, con una nueva interfaz de pr&aacute;ctica y el soporte a conjugaciones.
    </li>

    <li><a href="http://www.kde.org/applications/education/marble/"><b>Marble</b></a>, la aplicaci&oacute;n de globo terr&aacute;queo virtual, ahora permite planificar rutas y le ofrece la opci&oacute;n de descargar los datos de un mapa antes de salir de viaje para que no necesite conectarse a Internet para utilizar Marble ni sus funciones basadas en OpenStreetMap u OpenRouteService. Puede consultar m&aacute;s informaci&oacute;n sobre <a href="http://nienhueser.de/blog/?p=137">usar las rutas mundiales de Marble mientras est&aacute; desconectado</a>.
    </li>

</ul>

<?php
    showScreenshot("marble-routing.png", "El globo terr&aacute;queo de escritorio Marble ahora tambi&eacute;n se puede utilizar para establecer rutas");
?>

<p align="justify">

En Konqueror, las listas de Adblock se pueden actualizar autom&aacute;ticamente. Los usuarios que prefieran utilizar WebKit como motor de renderizado pueden delegar en el KPart de KDE-WebKit. Gwenview ahora se ejecuta con suavidad y sin bloquearse incluso al aplicar efectos de uso intensivo del procesador. Sus operaciones de edici&oacute;n de imagen ahora conservan correctamente todos los metadatos EXIF. Gwenview tambi&eacute;n ha mejorado su usabilidad y aumentado las posibilidades de configuraci&oacute;n. Finalmente, se ha trabajado mucho en KInfoCenter, que ofrece al usuario un resumen de la configuraci&oacute;n del hardware y software de su sistema, mejorando la usabilidad de su ventana principal y la estabilidad.
</p>

<h3>M&aacute;s capturas de pantalla...</h3>

<?php
    showScreenshot("plasma-infocenter.png", "El apartado visual de KInfoCenter, que muestra informaci&oacute;n sobre su sistema, ha sido modernizado");

    showScreenshot("dolphin-metadata.png", "Las funciones del Escritorio Sem&aacute;ntico de Dolphin proporcionan metadatos &uacute;tiles");

    showScreenshot("gwenview-flickrexport.png", "Con Gwenview puede compartir de forma sencilla sus im&aacute;genes en la web usando su servicio de im&aacute;genes favorito");


?>

<h4>Instalaci&oacute;n de las aplicaciones de KDE</h4>
<?php
  include("boilerplate-es.inc");
?>

<h2>Tambi&eacute;n publicados hoy:</h2>


<a href="platform-es.php">
<img src="images/platform.png" class="app-icon" alt="La plataforma de desarrollo de KDE 4.5.0"/>
</a>

<h2>La plataforma de desarrollo de KDE 4.5.0 mejora su rendimiento, estabilidad, introduce una nueva cach&eacute; de alta velocidad y el uso de WebKit</h2>
<p align="justify">
10 de agosto de 2010. Hoy, KDE publica la plataforma de desarrollo de KDE 4.5.0. Esta versi&oacute;n incluye muchas mejoras de rendimiento y estabilidad. La nueva <b>KSharedDataCache</b> est&aacute; optimizada para un acceso r&aacute;pido a los recursos almacenados en disco, como los iconos. La nueva biblioteca <b>KDE WebKit</b> proporciona integraci&oacute;n con las preferencias de red, almacenamiento de contrase&ntilde;as y muchas otras caracter&iacute;sticas de Konqueror. <a href="platform-es.php"><b>Leer el anuncio completo</b></a>
</p>



<a href="plasma-es.php">
<img src="images/plasma.png" class="app-icon" alt="Los espacios de trabajo de Plasma 4.5.0" width="64" height="64" />
</a>

<h2>Plasma para escritorio y netbooks 4.5.0: experiencia de usuario mejorada</h2>
<p align="justify">
10 de agosto de 2010. Hoy, KDE publica los espacios de trabajo del escritorio Plasma y Plasma para netbooks 4.5.0. El escritorio Plasma ha refinado mucho su usabilidad. Se ha mejorado la eficiencia de los flujos de gesti&oacute;n de notificaciones y trabajos, el &aacute;rea de notificaciones se ha &quot;limpiado&quot; a nivel visual, y la gesti&oacute;n de entrada de datos entre aplicaciones es m&aacute;s consistente al utilizar el protocolo de notificaciones de Freedesktop.org ya introducido en la versi&oacute;n anterior de Plasma. <a href="plasma-es.php"><b>Leer el anuncio completo</b></a>
</p>


<?php
  include("footer.inc");
?>