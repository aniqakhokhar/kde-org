<?php
  $release = '4.5';
  $release_full = '4.5.0';

  $page_title = "KDE Applications 4.5.0 enhance usability and bring map routing";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<!--
<h2>KDE Applications 4.5 Released</h2>

<table width="100%">
    <tr>
        <td width="50%">
                <a href="plasma.php">
                <img src="images/desktop-32.png" />
                Previous Page: KDE Workspaces
                </a>
        </td>
        <td align="right" width="50%">
                <a href="platform.php">Next page: Network and Personal Information Management
                <img src="images/internet-32.png" />
		</a>
        </td>
    </tr>
</table>
-->
<p>
KDE, an international Free Software community, is happy to announce the immediate availability of the KDE Applications 4.5. Be it the high-quality games, educational and productivity software or the useful tools, these applications have become more powerful, yet easier to use.
</p>

<?php
    showScreenshot("apps-overview.png", "Mathematical, text-editing and entertainment applications");
?>

<p>
The KDE Applications have been improved in many different areas. Let us highlight a few of the changes which were made.
</p>
    <ul>

   <li>The KDE Games team has worked on new features and introduced a new game: <b>Kajongg</b>, the <a href="http://en.wikipedia.org/wiki/Mahjong">original four-player Mahjongg</a>. This game is written entirely in Python, making it the first major Python KDE Application as part of the KDE Games family. The game <a href="http://www.kde.org/applications/games/konquest/">Konquest</a>, in which the player is tasked to conquer the galaxy, now lets users create and customize their own maps. Other improvements are easier configuration in the <a href="http://www.kde.org/applications/games/palapeli">jigsaw puzzle game Palapeli</a>, a new KGoldRunner level set (“Demolition”), and better <a href="http://en.wikipedia.org/wiki/Smart_Game_Format">SGF</a> file integration for <a href="http://www.kde.org/applications/games/kigo/">Kigo</a>.
    </li>

    <li>The <b>educational applications</b> have also seen several improvements. The biggest were for the learning aid, <a href="http://www.kde.org/applications/education/parley/">Parley</a>, which has gained a new practice interface and support for conjugations.
    </li>

    <li><a href="http://www.kde.org/applications/education/marble/"><b>Marble</b></a>, our virtual globe application, received a route planning feature and the option to download map data before you go on a trip so you do not need access to the Internet to make use of Marble and its mapping features based on OpenStreetmap and OpenRouteService data. Read more about <a href="http://nienhueser.de/blog/?p=137">worldwide and offline map routing in Marble</a>.
    </li>

</ul>

<?php
    showScreenshot("marble-routing.png", "The Marble Desktop Globe can now be used for routing as well");
?>

<p align="justify">

In Konqueror, Adblock lists can now be automatically updated, making for a cleaner web-browsing experience. Users who prefer to use WebKit as the rendering engine can now rely on the KDE-WebKit KPart. Gwenview now runs smoothly without blocking even while applying processor-intensive effects. Its image editing operations now properly retain all EXIF metadata. Gwenview also sees many usability improvements and increased configurability. Finally, KInfoCenter, which gives the user an overview of the hardware and software configuration of his or her system, has seen some major work this release, with usability improvements to its main window and better stability.
</p>

<h3>More Screenshots...</h3>

<?php
    showScreenshot("plasma-infocenter.png", "KInfoCenter, which shows information about your system has been visually revamped");

    showScreenshot("dolphin-metadata.png", "Semantic Desktop features in Dolphin provide useful metadata");

    showScreenshot("gwenview-flickrexport.png", "With Gwenview you can easily share your pictures on the web using your favourite picture service");


?>

<h4>Installing KDE Applications</h4>
<?php
  include("boilerplate.inc");
?>

<h2>Also Released Today:</h2>


<a href="platform.php">
<img src="images/platform.png" class="app-icon" alt="The KDE Development Platform 4.5.0"/>
</a>

<h2>KDE Development Platform 4.5.0 gains performance, stability, new high-speed cache and support for WebKit</h2>
<p align="justify">
10th August, 2010. KDE today releases the KDE Development Platform 4.5.0. This release brings many performance  and stability improvements. The new <b>KSharedDataCache</b> is optimized for fast access to resources stored on disk, such as icons. The new <b>WebKit</b> library provides integration with network settings, password-storage and many other features found in Konqueror. <a href="platform.php"><b>Read The Full Announcement</b></a>
</p>



<a href="plasma.php">
<img src="images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.5.0" width="64" height="64" />
</a>

<h2>Plasma Desktop and Netbook 4.5.0: improved user experience</h2>
<p align="justify">
10th August, 2010. KDE today releases the Plasma Desktop and Plasma Netbook Workspaces 4.5.0. Plasma Desktop received many usability refinements. The Notification and Job handling workflows have been streamlined. The notification area has been cleaned up visually, and its input handling across applications is now made more consistent by the extended use of the Freedesktop.org notification protocol first introduced in Plasma's previous version. <a href="plasma.php"><b>Read The Full Announcement</b></a>
</p>


<?php
  include("footer.inc");
?>
