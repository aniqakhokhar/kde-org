<?php
  $page_title = "KDE 3.0-beta1 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>
<p>DATELINE DECEMBER 19, 2001</p>
<p>FOR IMMEDIATE RELEASE</p>
<h3 align="center">Third Generation KDE Desktop Ready for Beta Testing and KDE 2 Apps Porting</h3>
<p><strong>The KDE Project Ships Beta of the Leading Desktop for Linux/UNIX,
Plans LinuxWorld Expo Demonstrations</strong></p>
<p>December 19, 2001 (The INTERNET).
The <a href="http://www.kde.org/">KDE
Project</a> today announced the immediate release of KDE 3.0beta1,
the third generation of KDE's free, powerful,
easy-to-use, Internet-enabled desktop for Linux and other UNIXes.
This second pre-release of KDE 3.0, which follows two months after the
release of KDE 3.0alpha, ships with the core KDE libraries, the core
desktop environment, and over 100 applications from the other
base KDE packages (administration, multimedia, network, PIM,
utilities, development, etc.).
KDE 3.0 is scheduled for final release in the first half of 2002, with one
or more intervening "RC" releases preceding the final release.
The KDE Project will demonstrate the latest pre-release of KDE 3 at
the <a href="http://www.linuxworldexpo.com/">LinuxWorld Expo in New York</a>
(Jan. 29 - Feb. 1, 2002).
</p>
<p>
"Beta1 is a stabilized snapshot of the current KDE 3 development branch
and is meant for testers, developers and translators," explained
<a href="http://www.kde.org/people/dirk.html">Dirk Mueller</a>, the KDE 3
release coordinator.  "Users can assist in the development effort
by testing this release and providing constructive feedback or helping
to update or complete documentation.  We're
especially interested in feedback about the support for RTL languages
and about the new KDE 3 features.  For developers, now is a great time
to complete <a href="#porting">porting</a> your applications to KDE 3, or
to pitch in and help implement the still-incomplete
<a href="http://developer.kde.org/development-versions/kde-3.0-features.html">planned
features</a>.  In addition, the
<a href="http://i18n.kde.org/">KDE translation
project</a> needs new contributors who can translate KDE 3 into their
native language. Translation is another great way for non-programmers
to contribute to the continuing success and rapid improvement of KDE."
</p>
<p>
The primary goals of the 3.0beta1 release are to:
</p>
<ul>
<li>enlist volunteers to help
find bugs and provide constructive feedback through the
KDE <a href="http://bugs.kde.org/">bugs database</a>, and to help
complete or update KDE documentation;</li>
<li>provide a stable API so developers can <a href="#porting">port</a>
their KDE 2 applications to KDE 3;</li>
<li>solicit developer feature requests before the KDE 3 API is finally
frozen for binary compatibility, as well as developer assistance in completing
the planned features; and</li>
<li>provide a consistent snapshot for the
<a href="http://i18n.kde.org/teams/index.shtml">translation teams</a>
(this release already provides some support for
<a href="http://i18n.kde.org/teams/distributed.html"><strong>57</strong>
languages</a>).</li>
</ul>
<p>
Additional information about KDE 3, including
<a href="http://www.kde.org/kde2-and-kde3.html">instructions</a> for
setting up a KDE 3 system side-by-side with a KDE 2 system,
a tentative
<a href="http://developer.kde.org/development-versions/kde-3.0-release-plan.html">release
plan</a>,
a KDE 3 <a href="http://www.kde.org/info/3.0.html">info page</a>, a list of
<a href="http://developer.kde.org/development-versions/kde-3.0-features.html">planned
features</a>, as well as a
<a href="http://www.kde.org/jobs/jobs-open.html">list of open
tasks</a> containing interesting projects for both users and developers
who wish to contribute to KDE,
is available at the KDE websites.
Please use the <a href="http://bugs.kde.org/">KDE bugs database</a> to
report bugs or make feature requests.
</p>
<p>
KDE and all its components (including the IDE
<a href="http://www.kdevelop.org/">KDevelop</a>) are available
<em><strong>for free</strong></em> under Open Source licenses from the KDE
<a href="http://download.kde.org/">ftp server</a>
and its <a href="http://www.kde.org/ftpmirrors.html">mirrors</a>.
<!--and can
also be obtained on <a href="http://www.kde.org/cdrom.html">CD-ROM</a>.
-->
</p>
<h4>Improvements</h4>
<p>
Besides the improvements to the underlying Qt library noted in the
<a href="http://www.kde.org/announcements/announce-3.0alpha1.html">KDE
3.0alpha1 announcement</a>, this release offers the following additional
improvements compared to the KDE 2 series:
</p>
<ul>
<li>a new clipboard system to satisfy the preferences of all users:
<ul>
  <li>KDE continues to offer the standard X-type clipboard; selected text is
  copied to this clipboard, and clicking the middle mouse button pastes the
  contents of this clipboard; and</li>
  <li>KDE also offers a complementary, independent Windows/Mac-type clipboard;
  text is inserted in the clipboard using an application's cut/copy
  (or generally <code>Ctrl-x</code>, <code>Ctrl-c</code>) function,
  and the application's paste (or generally <code>Ctrl-v</code>) function
  pastes the contents of this clipboard;</li>
</ul>
(note that <a name="clipboard">the clipboard in Qt 3.0.1 hangs
sometimes</a>; using <em>qt-copy</em>
or upgrading to Qt 3.0.2 when it is released should cure this problem);</li>
<li>many new <a href="http://kmail.kde.org/">KMail</a>
features, including:
<ul>
  <li>maildir support;</li>
  <li>support for distribution lists (groups) and aliases;</li>
  <li>SMTP authentication;</li>
  <li>SMTP over SSL/TLS;</li>
  <li>pipelining for POP3 (increases download speed on slow networks);</li>
  <li>support for both on-demand downloading and deletion without downloading;</li>
  <li>IMAP improvements (header caching, faster header fetching,
  creating/removing folders, drafts/sent/trash folders on the server,
  and mail checking in all folders);</li>
  <li>auto-configuration of IMAP/POP3/SMTP security features;</li>
  <li>automatic encoding selection for outgoing mails;</li>
  <li>DIGEST-MD5 authentication;</li>
  <li>keyboard shortcuts for switching between mono-spaced/proportional fonts;</li>
  <li>UTF-7 support; and</li>
  <li>enhanced status reports for encrypted/signed messages;</li>
</ul></li>
<li>important feature additions for
<a href="http://www.kdevelop.org/">KDevelop</a>, including:
<ul>
  <li>full cross-compiling support, with the ability to specify a
  compiler, compiler flags, target architecture, etc.; and</li>
  <li>support for Qt/Embedded projects (such as the
  <a href="http://developer.sharpsec.com/">Zaurus</a> and
  <a href="http://www.compaq.com/products/iPAQ/">iPAQ</a>);</li>
</ul>
(note that a release of KDevelop for KDE 2.2 with these new features is
included in most of the download directories listed below);</li>
<li>SSL certificate and CA management tools;</li>
<li>a new URL speed bar in file dialogs;</li>
<li>support for actions in the <a href="http://konqueror.kde.org/">Konqueror</a>
sidebar; and</li>
<li>numerous feature enhancements to Konsole, including:
<ul>
  <li>cluster management (sending input to all active sessions);</li>
  <li>clearing, searching and saving of session histories;</li>
  <li>specification of a working directory per session type;</li>
  <li>easier renaming of sessions with a keyboard shortcut;</li>
  <li>pausing scrolling while selecting text; and</li>
  <li>writing utmp entries (requires the utempter library).</li>
</ul></li>
</ul>
<p>
Additional improvements to the KDE libraries and applications are planned for
the successive RC releases leading to the first stable KDE 3.0.  A
<a href="http://developer.kde.org/development-versions/kde-3.0-features.html">partial list</a> of these planned features is available at KDE's
<a href="http://developer.kde.org/">developer website</a>.
</p>
<a name="porting"></a><h4>Porting to KDE 3</h4>
<p>
Since KDE 3 is mostly source compatible with KDE 2, porting applications
from KDE 2 to KDE 3 can usually be done with relative ease and comfort.
The process is <em>substantially</em> easier than the one for porting
KDE 1 applications to KDE 2; even very complicated applications have been
ported in a matter of hours.
</p>
<p>
Instructions for porting KDE 2 applications to KDE 3 are available
separately for the
<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE3PORTING.html">KDE
libraries</a> and the
<a href="http://doc.trolltech.com/3.0/porting.html">Qt libraries</a>.
Most of the changes required for the port applications pertain to changes
in the Qt API.  Although the KDE 3 API is not yet frozen, few changes are
anticipated for the final release of KDE 3.0.
</p>
<h4>Installing KDE 3.0beta1 Binary Packages</h4>
<p>
<em>Binary Packages</em>.
The 3.0beta1 release is outdated, its binary packages are no longer
available. Please consider using a newer release instead. 
</p>
<p>
Please note that the KDE team makes these packages available from the
KDE web site as a convenience to KDE users.  The KDE project is not
responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution.  If you cannot find a binary package for your distribution,
please read the <a href="http://www.kde.org/packagepolicy.html">KDE Binary
Package Policy</a>.
</p>
<p>
<em>Library Requirements</em>.
The library requirements for a particular binary package vary with the
system on which the package was compiled.  Please bear in mind that
some binary packages may require a newer version of Qt and other libraries
than was included with the applicable distribution (e.g., LinuxDistro 8.0
may have shipped with Qt-2.2.3 but the packages below require
Qt-3.x).  For general library requirements for KDE, please see the text at
<a href="#source_code-library_requirements">Source Code - Library
Requirements</a> below.
</p>
<h4>Downloading and Compiling KDE 3.0beta1</h4>
<p>
<a name="source_code-library_requirements"></a><em>Library
Requirements</em>.
KDE 3.0beta1 requires the following libraries:
</p>
<ul>
<li>qt-3.0.1, which is available in source code
from Trolltech as
<a href="ftp://ftp.trolltech.com/qt/source/qt-x11-free-3.0.1.tar.gz">qt-x11-free-3.0.1.tar.gz</a>,
though due to a <a href="#clipboard">problem with the clipboard</a>
<em>qt-copy</em> (or qt-3.0.2, when released) is recommended;</li>
<li>for reading help pages and KDE documentation,
<a href="ftp://speakeasy.rpmfind.net/pub/libxml/">libxml2</a> &gt;= 2.3.13
and <a href="http://xmlsoft.org/XSLT/">libxslt</a> &gt;= 1.0.7;</li>
<li>for JavaScript/ECMAScript regular expression support,
<a href="http://www.pcre.org/">PCRE</a> &gt;= 3.5;</li>
<li>for SSL support,
<a href="http://www.openssl.org/">OpenSSL</a> &gt;= 0.9.6x
(versions 0.9.5x are no longer supported);</li>
<li>for Java support, a JVM &gt;= 1.3;</li>
<li>for Netscape Communicator plugin support, KDE requires a recent version of
<a href="http://www.lesstif.org/">Lesstif</a> or Motif;</li>
<li>for searching local documentation,
<a href="http://www.htdig.org/">ht://dig</a>; and</li>
<li>for other special features, such as drag'n'drop audio CD ripping,
certain other packages.</li>
</ul>

<p><em>Compiler Requirements</em>.
Please note that some components of
KDE 3.0beta1 will not compile with older versions of
<a href="http://gcc.gnu.org/">gcc/egcs</a>, such as egcs-1.1.2 or
gcc-2.7.2.  At a minimum gcc-2.95-* is required.  In addition, some
components of KDE 3.0beta1 (such as the multimedia backbone of KDE,
<a href="http://www.arts-project.org/">aRts</a>) will not compile with
<a href="http://gcc.gnu.org/gcc-3.0/gcc-3.0.html">gcc</a> 3.0.x
(this problem is being addressed but no time frame is available).
</p>
<p>
<a name="source_code"></a><em>Source Code/SRPMs</em>.
The complete source code for KDE 3.0beta1 is no longer available. 
Consider using a newer release instead. 
</p>
<p>
<em>Further Information</em>.  For further
instructions on compiling and installing KDE 3.0beta1, please consult
the <a href="http://www.kde.org/install-source.html">installation
instructions</a> and, if you should encounter problems, the
<a href="http://www.kde.org/compilationfaq.html">compilation FAQ</a>.
</p>
<h4>About KDE</h4>
<p>
KDE is an independent, collaborative project by hundreds of developers
worldwide working over the Internet to create a sophisticated,
customizable and stable desktop environment employing a component-based,
network-transparent architecture.  KDE provides a stable, mature desktop,
an office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
set of networking and administration tools, and an efficient and intuitive
development environment, including an excellent IDE
(<a href="http://www.kdevelop.org/">KDevelop</a>).
KDE is working proof of the power of
the Open Source "Bazaar-style" software development model to create
first-rate technologies on par with and superior to even the most complex
commercial software.
</p>
<p>
Please visit the KDE family of web sites for the
<a href="http://www.kde.org/faq.html">KDE FAQ</a>, screenshots
(<a href="http://www.kde.org/screenshots/kde2shots.html">KDE 2</a>,
<a href="http://www.kde.org/screenshots/kde3shots.html">KDE 3</a>),
<a href="http://www.koffice.org/">KOffice information</a> and
<a href="http://developer.kde.org/documentation/kde3arch.html">developer
information</a>.
Much more <a href="http://www.kde.org/whatiskde/">information</a>
about KDE is available from KDE's
<a href="http://www.kde.org/family.html">family of web sites</a>.
</p>
<h4>Corporate KDE Sponsors</h4>
<p>
Besides the valuable and excellent efforts by the
<a href="http://www.kde.org/gallery/index.html">KDE developers</a>
themselves, significant support for KDE development has been provided by
<a href="http://www.mandrakesoft.com/">MandrakeSoft</a> and
<a href="http://www.suse.com/">SuSE</a>.  In addition,
the members of the <a href="http://www.kdeleague.org/">KDE League</a> provide
significant support for promoting KDE and KOffice.  Thanks!
</p>
<hr noshade="noshade" size="1" width="98%" align="center" />
<font size="2">
<em>Trademarks Notices.</em>
KDE, K Desktop Environment, KDevelop and KOffice are trademarks of KDE e.V.

Compaq, Alpha, iPAQ and Tru64 are either trademarks and/or service marks or
registered trademarks and/or service marks of Compaq Computer Corporation.

HP is a registered trademark of Hewlett-Packard Company.

IBM and PowerPC are registered trademarks of IBM Corporation.

Intel, i386 and i586 are trademarks or registered trademarks of Intel
Corporation or its subsidiaries in the United States and other countries.

Linux is a registered trademark of Linus Torvalds.

Netscape Communicator is a trademark or registered trademark of
Netscape Communications Corporation in the United States and other countries.

Sun is a trademark or registered trademark of Sun Microsystems, Inc. in
the United States and other countries.

Trolltech and Qt are trademarks of Trolltech AS.

UNIX and Motif are registered trademarks of The Open Group.

Zaurus is a trademark of Sharp Electronics Corporation in the United
States and/or other countries.

All other trademarks and copyrights referred to in this announcement are the property of their respective owners.
<br />
</font>
<hr noshade="noshade" size="1" width="98%" align="center" />
<table border="0" cellpadding="8" cellspacing="0">
<tr><th colspan="2" align="left">
Press Contacts:
</th></tr>
<tr valign="top"><td align="right" nowrap="nowrap">
United&nbsp;States:
</td><td nowrap="nowrap">
Eunice Kim<br />
The Terpin Group<br />
ekim@terpin.com<br />
(1) 650 344 4944 ext. 105<br />&nbsp;<br />
Kurt Granroth<br />
g&#114;&#0097;&#110;&#x72;&#x6f;&#x74;h&#x40;&#x6b;d&#x65;.or&#103;<br />
(1) 480 732 1752<br />&nbsp;<br />
Andreas Pour<br />
KDE League, Inc.<br />
&#x70;&#111;u&#x72;&#0064;&#107;d&#x65;.o&#114;&#103;<br />
(1) 917 312 3122
</td></tr>
<tr valign="top"><td align="right" nowrap="nowrap">
Europe (French and English):
</td><td nowrap="nowrap">
David Faure<br />
&#0102;&#x61;ure&#x40;&#00107;&#x64;&#x65;&#046;org<br />
(33) 4 3250 1445
</td></tr>
<tr valign="top"><td align="right" nowrap="nowrap">
Europe (German and English):
</td><td nowrap="nowrap">
Ralf Nolden<br />
n&#00111;&#108;&#x64;&#0101;n&#00064;kde&#00046;&#x6f;&#00114;&#103;<br />
(49) 2421 502758
</td></tr>
</table>
<!-- $Id: announce-3.0beta1.php 523083 2006-03-27 11:22:27Z scripty $ -->
<?php
  include "footer.inc"
?>
