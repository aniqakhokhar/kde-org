<?php
// SPDX-FileCopyrightText: 2020 David Faure <faure@kde.org>
// SPDX-FileCopyrightText: 2020 Jonathan Riddell <jr@jriddell.org>
//
// SPDX-License-Identifier: CC-BY-4.0

    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.75.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.75.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<h1>NOT OUT YET</h1>

<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
October 05, 2020. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.75.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("[AdvancedQueryParser] Relax parsing of string ending with parentheses");?></li>
<li><?php i18n("[AdvancedQueryParser] Relax parsing of string ending with comparator");?></li>
<li><?php i18n("[AdvancedQueryParser] Fix out-of-bound access if last character is a comparator");?></li>
<li><?php i18n("[Term] Replace Term::Private constructor with default values");?></li>
<li><?php i18n("[BasicIndexingJob] Shortcut XAttr retrieval for files without attributes");?></li>
<li><?php i18n("[extractor] Fix document type in extraction result");?></li>
<li><?php i18n("Relicense file to LGPL-2.0-or-later");?></li>
</ul>

<h3><?php i18n("BluezQt");?></h3>

<ul>
<li><?php i18n("Add rfkill property to manager");?></li>
<li><?php i18n("Add status property to rfkill");?></li>
<li><?php i18n("Register Rfkill for QML");?></li>
<li><?php i18n("Export Rfkill");?></li>
<li><?php i18n("Support providing service data values for LE advertisements");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Add new generic \"behavior\" icon");?></li>
<li><?php i18n("Make icon validation depend on icon generation only if enabled");?></li>
<li><?php i18n("Replace 24px icon bash script with python script");?></li>
<li><?php i18n("Use flag style iconography for view-calendar-holiday");?></li>
<li><?php i18n("Add Plasma Nano logo");?></li>
<li><?php i18n("Add application-x-kmymoney");?></li>
<li><?php i18n("Add KMyMoney icon");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("fix fetch-translations for invent urls");?></li>
<li><?php i18n("Include FeatureSummary and find modules");?></li>
<li><?php i18n("Introduce plausibility check for outbound licenes");?></li>
<li><?php i18n("Add CheckAtomic.cmake");?></li>
<li><?php i18n("Fix configuring with pthread on Android 32 bit");?></li>
<li><?php i18n("add RENAME parameter to ecm_generate_dbus_service_file");?></li>
<li><?php i18n("Fix find_library on Android with NDK &lt; 22");?></li>
<li><?php i18n("Explicitly sort Android version lists");?></li>
<li><?php i18n("Store Android {min,target,compile}Sdk in variables");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Licensing improvements");?></li>
<li><?php i18n("Fix api.kde.org on mobile");?></li>
<li><?php i18n("Make api.kde.org a PWA");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Relicense file to LGPL-2.0-or-later");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("use new install var (bug 415938)");?></li>
<li><?php i18n("Mark David Edmundson as maintainer for KAuth");?></li>
</ul>

<h3><?php i18n("KCalendarCore");?></h3>

<ul>
<li><?php i18n("Relicense file to LGPL-2.0-or-later");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("Remove handling for inside events from tab hack (bug 423080)");?></li>
</ul>

<h3><?php i18n("KCompletion");?></h3>

<ul>
<li><?php i18n("Relicense files to LGPL-2.0-or-later");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("CMake: Also set SKIP_AUTOUIC on generated files");?></li>
<li><?php i18n("Use reverse order in KDesktopFile::locateLocal to iterate over generic config paths");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Fix isDefault that cause the KCModule to not properly update its default state");?></li>
<li><?php i18n("[kcolorscheme]: Add isColorSetSupported to check if a colour scheme has a given color set");?></li>
<li><?php i18n("[kcolorscheme] Properly read custom Inactive colors for the backgrounds");?></li>
</ul>

<h3><?php i18n("KContacts");?></h3>

<ul>
<li><?php i18n("Remove obsolete license file for LGPL-2.0-only");?></li>
<li><?php i18n("Relicense files to LGPL-2.0-or-later");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("KJob: emit result() and finished() at most once");?></li>
<li><?php i18n("Add protected KJob::isFinished() getter");?></li>
<li><?php i18n("Deprecate KRandomSequence in favour of QRandomGenerator");?></li>
<li><?php i18n("Initialize variable in header class + const'ify variable/pointer");?></li>
<li><?php i18n("harden message-based tests against environment (bug 387006)");?></li>
<li><?php i18n("simplify qrc watching test (bug 387006)");?></li>
<li><?php i18n("refcount and delete KDirWatchPrivate instances (bug 423928)");?></li>
<li><?php i18n("Deprecate KBackup::backupFile() due to lost functionality");?></li>
<li><?php i18n("Deprecate KBackup::rcsBackupFile(...) due to no known users");?></li>
</ul>

<h3><?php i18n("KDBusAddons");?></h3>

<ul>
<li><?php i18n("Relicense files to LGPL-2.0-or-later");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("QML for I18n are added in KF 5.17");?></li>
<li><?php i18n("Relicense files to LGPL-2.0-or-later");?></li>
<li><?php i18n("Block shortcuts when recording key sequences (bug 425979)");?></li>
<li><?php i18n("Add SettingHighlighter as a manual version of the highlighting done by SettingStateBinding");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("KStandardDirs: always resolve symlinks for config files");?></li>
</ul>

<h3><?php i18n("KHolidays");?></h3>

<ul>
<li><?php i18n("Uncommented description fields for mt_* holiday files");?></li>
<li><?php i18n("Add national holidays for Malta in both English (en-gb) and Maltese (mt)");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Relicense file to LGPL-2.0-or-later");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("KUrlNavigator: always use \"desktop:/\" not \"desktop:\"");?></li>
<li><?php i18n("Support DuckDuckGo bang syntax in Webshortcuts (bug 374637)");?></li>
<li><?php i18n("KNewFileMenu: KIO::mostLocalUrl is useful with :local protocols only");?></li>
<li><?php i18n("Deprecate KIO::pixmapForUrl");?></li>
<li><?php i18n("kio_trash: remove unnecessarily strict permission check (bug 76380)");?></li>
<li><?php i18n("OpenUrlJob: handle all text scripts consistently (bug 425177)");?></li>
<li><?php i18n("KProcessRunner: more systemd metadata");?></li>
<li><?php i18n("KDirOperator: don't call setCurrentItem on an empty url (bug 425163)");?></li>
<li><?php i18n("KNewFileMenu: fix creating new dir with name starting with ':' (bug 425396)");?></li>
<li><?php i18n("StatJob: make it clearer that mostLocalUrl works only with :local protocols");?></li>
<li><?php i18n("Document how to add new \"random\" roles in kfileplacesmodel.h");?></li>
<li><?php i18n("Remove old kio_fonts hack in KCoreDirLister, hostname was stripped incorrectly");?></li>
<li><?php i18n("KUrlCompletion: accommodate \":local\" protocols that use hostname in url");?></li>
<li><?php i18n("Split code of addServiceActionsTo method into smaller methods");?></li>
<li><?php i18n("[kio] BUG: Allowing double-quotes in open/save dialog (bug 185433)");?></li>
<li><?php i18n("StatJob: cancel job if url is invalid (bug 426367)");?></li>
<li><?php i18n("Connect slots explicitly instead of using auto-connections");?></li>
<li><?php i18n("Make filesharingpage API actually work");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("AbstractApplicationHeader: anchors.fill instead of position-dependent anchoring");?></li>
<li><?php i18n("Improve look and consistency of GlobalDrawerActionItem");?></li>
<li><?php i18n("Remove form indent for narrow layouts");?></li>
<li><?php i18n("Revert \"allow customize the header colors\"");?></li>
<li><?php i18n("allow customize the header colors");?></li>
<li><?php i18n("Add missing @since for the painted area properties");?></li>
<li><?php i18n("Introduce QtQuick Image style paintedWidth/paintedHeight properties");?></li>
<li><?php i18n("Add a placeholder image property to icon (in the style of fallback)");?></li>
<li><?php i18n("Remove Icon's custom implicitWidth/implicitHeight behavior");?></li>
<li><?php i18n("Guard potentially-null pointer (turns out to be surprisingly common)");?></li>
<li><?php i18n("protected setStaus");?></li>
<li><?php i18n("Introduce status property");?></li>
<li><?php i18n("Support ImageResponse and Texture type image providers in Kirigami::Icon");?></li>
<li><?php i18n("Warn people not to use ScrollView in ScrollablePage");?></li>
<li><?php i18n("Revert \"always show separator\"");?></li>
<li><?php i18n("make mobilemode support custom title delegates");?></li>
<li><?php i18n("Hide breadcrumbs separator line if buttons layout is visible but 0 width (bug 426738)");?></li>
<li><?php i18n("[icon] Consider icon invalid when source is an empty URL");?></li>
<li><?php i18n("Change Units.fontMetrics to actually use FontMetrics");?></li>
<li><?php i18n("Add Kirigami.FormData.labelAlignment property");?></li>
<li><?php i18n("always show separator");?></li>
<li><?php i18n("Use Header colors for desktop style AbstractApplicationHeader");?></li>
<li><?php i18n("Use the context of the component when creating delegates for ToolBarLayout");?></li>
<li><?php i18n("Abort and delete incubators when deleting ToolBarLayoutDelegate");?></li>
<li><?php i18n("Remove actions and delegates from ToolBarLayout when they get destroyed (bug 425670)");?></li>
<li><?php i18n("Replace use of c-style pointer cast in sizegroup");?></li>
<li><?php i18n("binary constants are a C++14 extension");?></li>
<li><?php i18n("sizegroup: Fix enum not handled warnings");?></li>
<li><?php i18n("sizegroup: Fix 3arg connects");?></li>
<li><?php i18n("sizegroup: Add CONSTANT to signal");?></li>
<li><?php i18n("Fix a few cases of using range loops on non-const Qt containers");?></li>
<li><?php i18n("Constrain button height in global toolbar");?></li>
<li><?php i18n("Display a separator between breadcrumbs and the icons to the left");?></li>
<li><?php i18n("Use KDE_INSTALL_TARGETS_DEFAULT_ARGS");?></li>
<li><?php i18n("Size ApplicationHeaders using the SizeGroup");?></li>
<li><?php i18n("Introduce SizeGroup");?></li>
<li><?php i18n("Fix: make refresh indicator appear above list headers");?></li>
<li><?php i18n("put overlaysheets over drawers");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("ignore sourceDataChanged on invalid indexes");?></li>
<li><?php i18n("Support for KDescendantProxyModel \"collapsing\" nodes");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Manually track the life cycle of our kpackage runner internals");?></li>
<li><?php i18n("Update versions for \"fake\" kpackage updates (bug 427201)");?></li>
<li><?php i18n("Do not use default parameter when not needed");?></li>
<li><?php i18n("Fix crash when installing kpackages (bug 426732)");?></li>
<li><?php i18n("Detect when the cache changes and react accordingly");?></li>
<li><?php i18n("Fix updating of entry if version number is empty (bug 417510)");?></li>
<li><?php i18n("Const'ify pointer + initialize variable in header");?></li>
<li><?php i18n("Relicense file to LGPL-2.0-or-later");?></li>
<li><?php i18n("Accept suggest takeover of maintainership");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Lower Android plugin until new Gradle is available");?></li>
<li><?php i18n("Use Android SDK versions from ECM");?></li>
<li><?php i18n("Deprecate KNotification constructor taking widget parameter");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("Fix DBus notifications when installed/updated");?></li>
<li><?php i18n("Relicense file to LGPL-2.0-or-later");?></li>
</ul>

<h3><?php i18n("KParts");?></h3>

<ul>
<li><?php i18n("Install krop &amp; krwp servicetype definition files by file name matching type");?></li>
</ul>

<h3><?php i18n("KQuickCharts");?></h3>

<ul>
<li><?php i18n("Avoid binding loop inside Legend");?></li>
<li><?php i18n("Remove check for GLES3 in SDFShader (bug 426458)");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("Add matchRegex property to prevent unnecessary thread spawning");?></li>
<li><?php i18n("Allow to set actions in QueryMatch");?></li>
<li><?php i18n("Allow to specify individual actions for D-Bus runner matches");?></li>
<li><?php i18n("Relicense files to LGPL-2.0-or-later");?></li>
<li><?php i18n("Add min letter count property");?></li>
<li><?php i18n("Consider XDG_DATA_HOME env variable for template install dirs");?></li>
<li><?php i18n("Improve error messages for D-Bus runners");?></li>
<li><?php i18n("Start to emit metadata porting warnings at runtime");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("bring back disableAutoRebuild from the brink (bug 423931)");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("[kateprinter] Portaway from deprecated QPrinter methods");?></li>
<li><?php i18n("Don't create temporary buffer to detect mimetype for saved local file");?></li>
<li><?php i18n("avoid hang due to dictionary and trigrams loading on first typing");?></li>
<li><?php i18n("[Vimode]Always show  a-z buffers in lower case");?></li>
<li><?php i18n("[KateFadeEffect]emit hideAnimationFinished() when a fade out is interrupted by a fade in");?></li>
<li><?php i18n("ensure pixel perfect border even for scaled rendering");?></li>
<li><?php i18n("ensure we overpaint the border separator over all other stuff like folding highlights");?></li>
<li><?php i18n("move separator from between icon border and line numbers to between bar and text");?></li>
<li><?php i18n("[Vimode]Fix behavior of numbered registers");?></li>
<li><?php i18n("[Vimode]Put deleted text to the proper register");?></li>
<li><?php i18n("Restore behavior of find selected when no selection is available");?></li>
<li><?php i18n("no further LGPL-2.1-only or LGPL-3.0-only files");?></li>
<li><?php i18n("re-license files to LGPL-2.0-or-later");?></li>
<li><?php i18n("use not needed set method for some theme colors");?></li>
<li><?php i18n("5.75 will be once incompatible, default to 'Auto Color Theme Selection' for themes");?></li>
<li><?php i18n("alter scheme =&gt; theme in the code to avoid confusion");?></li>
<li><?php i18n("shorten proposed license header to current state");?></li>
<li><?php i18n("ensure we always end up with some valid theme");?></li>
<li><?php i18n("improve Copy... dialog");?></li>
<li><?php i18n("fix more new =&gt; copy naming");?></li>
<li><?php i18n("add some KMessageWidget that hints for read-only themes to copy them, rename new =&gt; copy");?></li>
<li><?php i18n("disable editing of read-only themes");?></li>
<li><?php i18n("saving of highlighting specific style overrides works, only diffs are saved");?></li>
<li><?php i18n("simplify attribute creation, transparent colors are now properly handled in Format");?></li>
<li><?php i18n("try to limit export to changes attributes, this halfway works, but we still export the wrong names for included definitions");?></li>
<li><?php i18n("start to compute the 'real' defaults based on current theme and formats without style overrides for the highlighting");?></li>
<li><?php i18n("fix reset action");?></li>
<li><?php i18n("storing of syntax specific overrides, at the moment just all stuff that got loaded into the tree view is stored");?></li>
<li><?php i18n("start to work on syntax highlighting specific overrides, at the moment, show just the styles a highlighting really has itself");?></li>
<li><?php i18n("allow default style changes changes to be saved");?></li>
<li><?php i18n("allow color changes to be saved");?></li>
<li><?php i18n("implement theme export: simple file copy");?></li>
<li><?php i18n("no highlighting specific import/export, makes no sense with new .theme format");?></li>
<li><?php i18n("implement .theme file import");?></li>
<li><?php i18n("theme new &amp; delete work, new will copy the current theme as start point");?></li>
<li><?php i18n("use theme colors everywhere");?></li>
<li><?php i18n("rip out more old schema code in favor of KSyntaxHighlighting::Theme");?></li>
<li><?php i18n("start to use the colors as set by the theme, without own logic around this");?></li>
<li><?php i18n("initialize m_pasteSelection and increase UI file version");?></li>
<li><?php i18n("add shortcut for paste mouse selection");?></li>
<li><?php i18n("avoid setTheme, we just can pass our theme to the helper functions");?></li>
<li><?php i18n("fix tooltip, this will just reset to theme default");?></li>
<li><?php i18n("export default styles configuration to json theme");?></li>
<li><?php i18n("start to work on theme json export, activated by using the .theme extension in the export dialog");?></li>
<li><?php i18n("rename 'Use KDE Color Theme' to 'Use Default Colors', that is the actual effect");?></li>
<li><?php i18n("don't ship empty 'KDE' theme by default");?></li>
<li><?php i18n("support automatic selection of right theme for current Qt/KDE color theme");?></li>
<li><?php i18n("convert old theme names to new ones, use new config file, transfer data once");?></li>
<li><?php i18n("move font config to appearance, rename scheme =&gt; color theme");?></li>
<li><?php i18n("remove hardcoded default theme name, use KSyntaxHighlighting accessors");?></li>
<li><?php i18n("load fallback colors from theme");?></li>
<li><?php i18n("don't bundle embedded colors at all");?></li>
<li><?php i18n("use right function to lookup theme");?></li>
<li><?php i18n("editor colors are now used from Theme");?></li>
<li><?php i18n("use the KSyntaxHighlighting::Theme::EditorColorRole enum");?></li>
<li><?php i18n("handle Normal =&gt; Default transition");?></li>
<li><?php i18n("first step: load theme list from KSyntaxHighlighting, now we have them already as known schemes in KTextEditor");?></li>
</ul>

<h3><?php i18n("KUnitConversion");?></h3>

<ul>
<li><?php i18n("Use uppercase for Fuel Efficiency");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Don't cache QList::end() iterator if erase() is used");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("kviewstateserializer.cpp - crash guards in restoreScrollBarState()");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Relicense files to be compatible with LGPL-2.1");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("[kmainwindow] Don't delete entries from an invalid kconfiggroup (bug 427236)");?></li>
<li><?php i18n("Don't overlap main windows when opening additional instances (bug 426725)");?></li>
<li><?php i18n("[kmainwindow] Don't create native windows for non-toplevel windows (bug 424024)");?></li>
<li><?php i18n("KAboutApplicationDialog: avoid empty \"Libraries\" tab if HideKdeVersion is set");?></li>
<li><?php i18n("Show language code in addition to (translated) language name in switch application language dialog");?></li>
<li><?php i18n("Deprecate KShortcutsEditor::undoChanges() in favour of new undo()");?></li>
<li><?php i18n("Handle double close in main window (bug 416728)");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Fix plasmoidheading.svgz being installed to the wrong place (bug 426537)");?></li>
<li><?php i18n("Provide a lastModified value in ThemeTest");?></li>
<li><?php i18n("Detect that we are looking for an empty element and quit early");?></li>
<li><?php i18n("Make PlasmaComponents3 Tooltips use the typical tooltip style (bug 424506)");?></li>
<li><?php i18n("Use Header colors in PlasmoidHeading headers");?></li>
<li><?php i18n("Change PC2 TabBar highlight movement animation easing type to OutCubic");?></li>
<li><?php i18n("Add support for Tooltip color set");?></li>
<li><?php i18n("Fix PC3 Button/ToolButton icons not always having the right color set (bug 426556)");?></li>
<li><?php i18n("Ensure FrameSvg uses lastModified timestamp when using cache (bug 426674)");?></li>
<li><?php i18n("Ensure we always have a valid lastModified timestamp when setImagePath is called");?></li>
<li><?php i18n("Deprecate a lastModified timestamp of 0 in Theme::findInCache (bug 426674)");?></li>
<li><?php i18n("Adapt QQC2 import to new versioning scheme");?></li>
<li><?php i18n("[windowthumbnail] Verify that the relevant GLContext exists, not any");?></li>
<li><?php i18n("Add missing PlasmaCore import to ButtonRow.qml");?></li>
<li><?php i18n("Fix a few more reference errors in PlasmaExtras.ListItem");?></li>
<li><?php i18n("Fix error for implicitBackgroundWidth in PlasmaExtras.ListItem");?></li>
<li><?php i18n("Call edit mode \"Edit Mode\"");?></li>
<li><?php i18n("Fix a TypeError in QueryDialog.qml");?></li>
<li><?php i18n("Fix ReferenceError to PlasmaCore in Button.qml");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Also use highlight text color when checkDelegate is highlighted (bug 427022)");?></li>
<li><?php i18n("Respect scrollbar click to jump to position setting (bug 412685)");?></li>
<li><?php i18n("Don't inherit colors in desktop toolbar style by default");?></li>
<li><?php i18n("Relicense file to LGPL-2.0-or-later");?></li>
<li><?php i18n("Use header colors only for header toolbars");?></li>
<li><?php i18n("Move color set declaration to a place where it can be overridden");?></li>
<li><?php i18n("Use Header colors for Desktop style ToolBar");?></li>
<li><?php i18n("add the missing isItem property necessary for trees");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Downgrade trigrams output");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("AppArmor: fix regexp of paths detection");?></li>
<li><?php i18n("AppArmor: update highlighting for AppArmor 3.0");?></li>
<li><?php i18n("color cache for rgb to ansi256colors conversions (speeds up markdown loading)");?></li>
<li><?php i18n("SELinux: use include keywords");?></li>
<li><?php i18n("SubRip Subtitles &amp; Logcat: small improvements");?></li>
<li><?php i18n("generator for doxygenlua.xml");?></li>
<li><?php i18n("Fix doxygen latex formulas (bug 426466)");?></li>
<li><?php i18n("use Q_ASSERT like in remaining framework + fix asserts");?></li>
<li><?php i18n("rename --format-trace to --syntax-trace");?></li>
<li><?php i18n("apply a style to regions");?></li>
<li><?php i18n("trace contexts and regions");?></li>
<li><?php i18n("use editor background color by default");?></li>
<li><?php i18n("ANSI highlighter");?></li>
<li><?php i18n("Add copyright and update separator color in Radical theme");?></li>
<li><?php i18n("Update separator color in the Solarized themes");?></li>
<li><?php i18n("Improve color of separator and icon border for Ayu, Nord and Vim Dark themes");?></li>
<li><?php i18n("make separator color less intrusive");?></li>
<li><?php i18n("import Kate schema to theme converter by Juraj Oravec");?></li>
<li><?php i18n("more prominent section about licensing, link our MIT.txt copy");?></li>
<li><?php i18n("first template for base16 generator, https://github.com/chriskempson/base16");?></li>
<li><?php i18n("add proper license information to all themes");?></li>
<li><?php i18n("Add Radical color theme");?></li>
<li><?php i18n("Add Nord color theme");?></li>
<li><?php i18n("improve themes showcase to show more styles");?></li>
<li><?php i18n("adding gruvbox light and dark themes, MIT licensed");?></li>
<li><?php i18n("Add ayu color theme (with light, dark and mirage variants)");?></li>
<li><?php i18n("Add POSIX alternate for simple variable assignment");?></li>
<li><?php i18n("tools to generate a graph from a syntax file");?></li>
<li><?php i18n("fix auto-conversion of unset QRgb == 0 color to \"black\" instead of \"transparent\"");?></li>
<li><?php i18n("Add Debian changelog and control example files");?></li>
<li><?php i18n("add the 'Dracula' color theme");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.75");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.12");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://invent.kde.org/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://invent.kde.org/groups/frameworks/-/merge_requests", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
