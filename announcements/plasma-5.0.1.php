<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships First Bugfix Release of Plasma 5");
  $site_root = "../";
  $release = 'plasma-5.0.1';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<a href="plasma-5.0.1.png">
<img src="plasma-5.0.1-wee.png" style="float: right; padding: 1ex; margin: 1ex; border: 0; background-image: none; " width="400" height="300" alt="<?php i18n("Plasma 5");?>" />
</a>

<p>
<?php i18n("August 12, 2014.
Today KDE releases the first bugfix update to Plasma 5.  <a
href='http://kde.org/announcements/plasma5.0/index.php'>Plasma 5</a>
was released a month ago with many feature refinements and
streamlining the existing codebase of KDE's popular desktop for
developers to work on for the years to come.
");?>
</p>

<p>
<?php i18n("
This release, versioned 5.0.1, adds a month's worth of new
translations and fixes from KDE's contributors.  The bugfixes are
typically small but important such as fixing text which couldn't be
translated, using the correct icons and fixing overlapping files with
KDELibs 4 software.
");?>
</p>

<!-- // Boilerplate again -->

<h2><?php i18n("Live Images");?></h2>

<p><?php i18n("
The easiest way to try it out is the with a live image booted off a
USB disk.  Images are available for development versions of <a
href='http://cdimage.ubuntu.com/kubuntu-plasma5/'>Kubuntu Plasma 5</a>.
");?></p>

<h2><?php i18n("Package Downloads");?></h2>

<p><?php i18n("Some distributions have created, or are in the process
of creating, packages listed on our wiki page.
");?></p>

<ul>
<li>
<?php print i18n_var("<a
href='https://community.kde.org/Plasma/Packages'>Package
download wiki page</a>"
, $release);?>
</li>
</ul>

<h2><?php i18n("Source Downloads");?></h2>

<p><?php i18n("You can install Plasma 5 directly from source. KDE's
community wiki has <a
href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>.
Note that Plasma 5 does not co-install with Plasma 4, you will need
to uninstall older versions or install into a separate prefix.
");?>
</p>

<ul>
<li>
<?php print i18n_var("
<a href='../info/%1.php'>Source
Info Page</a>
", $release);?>
</li>
</ul>

<h2><?php i18n("Feedback");?></h2>

<p><?php print i18n_var("You can provide feedback either via the <a
href='%1'>#Plasma IRC channel</a>, <a
href='%2'>Plasma-devel
mailing list</a> or report issues via <a
href='%3'>bugzilla</a>. Plasma
5 is also <a
href='%4'>discussed on the KDE
Forums</a>. Your feedback is greatly appreciated. If you like what the
team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided",  "http://forum.kde.org/viewforum.php?f=289");?></p>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
