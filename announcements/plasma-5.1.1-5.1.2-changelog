<h3>Plasma Desktop</h3><ul>
<li>Fix vertical aligment.</li>
<li>Fix test by removing old test install directory</li>
<li>Read/Write ColorScheme to configGroup General instead of KDE</li>
<li>Remove KGlobal check and KComponentData object that overrode applications KAboutData when showing dialogs.</li>
<li>Ignore bottom margin unless view is overflowing.</li>
<li>Highlight first entry when searching</li>
<li>Fix 'Forget App' in Recent Apps not working immediately.</li>
<li>[desktop/views] Set right margin properly</li>
<li>Don't duplicate entries when the Rever button is clicked</li>
<li>Bouncing cursor is the default</li>
<li>Fix installing cursor themes from GHNS</li>
<li>Fix target install dir</li>
<li>Fix crash on Defaults</li>
<li>Fix install target dir</li>
<li>Disable checkbox buddies when unchecked.</li>
<li>Yank duplicate group header.</li>
<li>X-KDE-Keywords separator is ,</li>
<li>Fix regression in event handler causing favorites to launch on right-click release.</li>
<li>Don't put scripts from ~/.config/autostart in autostart kcm list.</li>
<li>Don't parse window titles as rich text.</li>
</ul>
<h3>Oxygen</h3><ul>
<li>Fixed color role for accelerator</li>
<li>Added inputWidgetEngine, for abstract item views, line editors, spinboxes and comboboxes it is needed to avoid animation conflicts between lists and buttons (when, e.g. there is a checkbox inside a list)</li>
<li>Removed support for Q3ListView</li>
<li>Manually calculate viewport update rect on header hover change</li>
<li>Remove mnemonic underlines when alt-tabbing</li>
<li>CMake: use the kde4workspace include dir</li>
<li>CMake: fix searching for pkg-config</li>
<li>Fixed member initialization removed unused headers backported from breeze</li>
</ul>
<h3>Breeze </h3><ul>
<li>Fix ui files</li>
<li>Fixed color role for accelerator</li>
<li>Added inputWidgetEngine, for abstract item views, line editors, spinboxes and comboboxes it is needed to avoid animation conflicts between lists and buttons (when, e.g. there is a checkbox inside a list)</li>
<li>Removed Q3ListView support</li>
<li>Add LGPL 3 for breeze icons with clairification as in Oxygen</li>
<li>Manually calculate viewport update rect on header hover change BUG: 340343</li>
<li>Fixed KDE4 compilation CCBUG: 341006</li>
<li>Remove mnemonic underlines when alt-tabbing</li>
<li>Removed unused members CCMAIL: staniek@kde.org</li>
<li>Fixed uninitialized member _value CCMAIL: staniek@kde.org</li>
</ul>
<h3>Powerdevil</h3><ul>
<li>Fix battery remaining time update with upower >= 0.99</li>
</ul>
<h3>KDE CLI Tools</h3><ul>
<li>Use QFile::decodeName for command in KDEsuDialog to fix encoding</li>
<li>port away from Q_WS_X11</li>
</ul>
<h3>System Settings</h3><ul>
<li>Restore KStandardGuiItems</li>
</ul>
<h3>KWin</h3><ul>
<li>[kcmkwin/screenedges] Drop check whether Compositing is enabled</li>
<li>Cleanup electric border handling in leaveMoveResize</li>
<li>Fixuifiles</li>
<li>Require OpenGL 2.0 in the SceneOpenGL2 constructor</li>
</ul>
<h3>Plasma Workspace </h3><ul>
<li>[lookandfeel/osd] Make the OSD timeout shorter</li>
<li>Fix ui files</li>
<li>Fix lockscreen theme fallback</li>
<li>Add the next wallpaper action when necessary</li>
<li>[applets/notifications] Revert i18n changes cherry-picked from master</li>
<li>Only play indeterminate animation when plasmoid is expanded</li>
<li>Refactor JobDelegate</li>
<li>Don't leave a gap when label1 is not visible</li>
<li>Don't emit a Job finished notification if the message would be empty</li>
<li>Finish cleanup and remove unused config.ui remnant</li>
<li>When no percentage is exposed over the dataengine make the progress bar indeterminate</li>
<li>Cleanup Jobs code and remove dead code</li>
<li>Alleviate the annoyance of Job Finished notifications</li>
<li>Escape ampersands in notifications</li>
<li>[dataengines/notifications] Replace \n with <br/></li>
<li>Fix dialog minimum height (always add the margins afterwards)</li>
<li>Also take the title label into account when calculating the dialog size</li>
<li>Enforce StyledText in notifications</li>
<li>Hide popup when opening configure dialog</li>
<li>Fix notification configure button</li>
<li>Fix binding loops and make popup even more compact</li>
<li>Cleanup Loaders</li>
<li>Cleanup Notifications code</li>
<li>Move duplicated code from NotificationDelegate and NotificationPopup into a new NotificationItem</li>
<li>Make sure lock screen package name does not overlap highlight rectangle</li>
<li>Actually save the kscreensaver config</li>
<li>fix resetting and reverting to default values</li>
<li>Fix qml warnings in splash kcm</li>
<li>Separator for Keywords is ;</li>
<li>Hopefully fix shell resizing in VirtualBox</li>
<li>Take into account the primary screen can be redundant</li>
<li>Delayed primaryOutput processing needs to be delayed</li>
<li>[digital-clock] Also set minHeight in vertical panels</li>
<li>BUG: 337742 REVIEW: 120431. Fix and future-proof security in Dr Konqi.</li>
<li>[freespacenotifier] Fix hiding the SNI when avail space goes up again</li>
<li>[freespacenotifier] Make the SNI just Active when free space raised</li>
<li>Find messages in QML as well</li>
</ul>
<h3>Plasma Addons</h3><ul>
<li>Handle onExternalData event in notes applet</li>
<li>Properly wrap to the next hour</li>
</ul>
<h3>KIO Extras </h3><ul>
<li>Sanitize path</li>
</ul>
