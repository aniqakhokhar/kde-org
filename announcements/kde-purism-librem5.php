<?php
	include_once ("functions.inc");
	$translation_file = "kde-org";
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Purism and KDE to Work Together on World's First Truly Free Smartphone",
		'cssFile' => '/content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = 'plasma-5.10.95'; // for i18n
	$version = "5.10.95";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}

figure {
    position: relative;
    z-index: 2;
    font-size: smaller;
    /* text-shadow: 2px 2px 5px grey; */
    font-style: italic;
    color: #545454;
    text-align: right;
}
</style>
<main class="releaseAnnouncment container">
<p align="justify">FOR IMMEDIATE RELEASE</p>


<h1>
  Purism and KDE to Work Together on World's First Truly Free Smartphone
</h1>


<figure style="float: right; width: 420px;">
<a href="kde-purism-librem5/phone-logo.png">
<img src="kde-purism-librem5/phone-logo.png" width="400" height="455" />
</a>
<figcaption><?php i18n("Plasma Mobile");?></figcaption>
</figure>
<p>
<i>Berlin/San Francisco, September 14, 2017</i>
</p>
<p align="justify">
<a href="https://puri.sm/">Purism</a> and <a href="https://www.kde.org">KDE</a> are partnering to help KDE adapt <a href="https://plasma-mobile.org/">Plasma Mobile</a> to <a href="https://puri.sm/shop/librem-5/">Purism's Librem 5 smartphone</a>.
</p>
<p align="justify">
KDE develops Plasma Mobile, a free, open and full-featured graphical environment for mobile devices. Plasma Mobile has been tested on several off-the-shelf devices. However, most smartphones include hardware that requires proprietary software to work. This clashes with KDE's principles of <b>freedom and openness</b>. It also makes building difficult, since many details of the hardware are kept secret, preventing complete access to all the components.
</p>

<p align="justify">
Purism, the manufacturer that builds high-quality, top of the range and freedom-respecting devices, is currently running <a href="https://puri.sm/shop/librem-5/">a crowdfunding campaign</a> which will allow the company to build the first fully free and open smartphone: The Librem 5.
</p>

<p align="justify">
The shared vision of <b>freedom, openness and personal control for end users</b> has brought KDE and Purism together in a common venture. Both organisations agree that cooperating will help bring a truly free and open source smartphone to the market. KDE and Purism will work together to make this happen.
</p>

<p align="justify">
"<em>Building a Free Software and privacy-focused smartphone has been a dream of the KDE community for a long time. We created Plasma to not just run on desktops and laptops, but for the whole spectrum of devices.</em>" says Lydia Pintscher, President of KDE e.V.. "<em>Partnering with Purism will allow us to ready Plasma Mobile for the real world and integrate it seamlessly with a commercial device for the first time. The Librem 5 will make Plasma Mobile shine the way it deserves.</em>"
</p>

<p align="justify">
"<em>Having full access to Purism's hardware platform is a dream for the KDE community,</em>" says Lydia Pintscher, President of KDE e.V. "<em>Partnering with Purism will allow us to integrate Plasma Mobile seamlessly with a commercial device for the first time. The Librem 5 will make Plasma Mobile shine the way it deserves.</em>"
</p>

<p align="justify">
"<em>KDE has created an evolved, completely free platform in Plasma Mobile,</em>" says Todd Weaver, CEO of Purism. "<em>We feel that Plasma Mobile will become a serious contender that may break the current duopoly and bring a full-featured, fully free/libre and open source mobile operating system to the market. We look forward to trying out Plasma Mobile on our test hardware and working with KDE's community.</em>"
</p>

<h2>About KDE</h2>
<p align="justify">
KDE is an international community of developers, designers, writers, translators and users that work together to achieve a world in which everyone has control over their digital life and enjoys freedom and privacy. KDE produces <a href="https://www.kde.org/plasma-desktop">Plasma</a>, an advanced and friendly desktop and a graphical environment for mobile devices. KDE also fosters and sponsors the creation of <a href="https://www.kde.org/applications/">hundreds of apps</a> for Linux, Windows, MacOS, Android and many other platforms; as well as <a href="https://api.kde.org/frameworks/">frameworks</a>, libraries and utilities that help developers create applications faster and easier.
</p>


<h2>About Purism</h2>
<p align="justify">
<a href="https://puri.sm/">Purism</a> is a <a href="https://puri.sm/about/social-purpose/">Social Purpose</a> Corporation devoted to bringing security, privacy, <a href="https://en.wikipedia.org/wiki/Free_software">software freedom</a>, and digital independence to everyone’s personal computing experience. With operations based in San Francisco (California) and around the world, Purism manufactures <a href="https://puri.sm/products/">premium-quality laptops and tablets</a>, creating beautiful and powerful devices meant to protect users’ digital lives without requiring a compromise on ease of use. Purism designs and assembles its hardware in the United States, carefully selecting internationally sourced components to be privacy-respecting and fully Free-Software-compliant. Security and privacy-centric features come built-in with every product Purism makes, making security and privacy the simpler, logical choice for individuals and businesses.
</p>


<h2>Further Information...</h2>
<p align="justify">
<ul>
    <li><a href="kde-purism-librem5/kde-purism-presskit.zip">Press-kit with more images</a></li>
    <li><a href="https://youtu.be/auuQA0Q8qpM">Video showing a prototype of Plasma Mobile</a></li>
    <li><a href="https://plasma-mobile.org/">Plasma Mobile website</a></li>
</ul>
</p>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

</main>
<?php
  require('../aether/footer.php');
?>
