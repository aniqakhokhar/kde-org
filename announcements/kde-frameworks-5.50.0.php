<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.50.0");
  $site_root = "../";
  $release = '5.50.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
September 08, 2018. KDE today announces the release
of KDE Frameworks 5.50.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Attica");?></h3>

<ul>
<li><?php i18n("Add support for proposed tags addition in OCS 1.7");?></li>
</ul>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Fixed a typo in the index size output (bug 397843)");?></li>
<li><?php i18n("Remove src not dest url when a url is newly unindexable");?></li>
<li><?php i18n("[tags_kio] Simplify the filename path query matching by using a capture group");?></li>
<li><?php i18n("Revert \"Skip queueing newly unindexable files and remove them from the index immediately.\"");?></li>
<li><?php i18n("[tags_kio] Simplify file path lookup while loop");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Add LabPlot project file icon");?></li>
<li><?php i18n("ScalableTest, add \"scalable\" plasma-browser-integration (bug 393999)");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Bindings: Check if bindings can be generated for a specific python version");?></li>
<li><?php i18n("Bindings: Make generator forward compatible with Python 3");?></li>
<li><?php i18n("Disable alteration of QT_PLUGIN_PATH by ECM when running tests");?></li>
<li><?php i18n("Bindings: Add support for scoped enums (bug 397154)");?></li>
<li><?php i18n("Make it possible for ECM to detect po files at configure time");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("[KStyle] Use dialog-question for question icon");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("handle non-ASCII encodings of file names in tar archives (bug 266141)");?></li>
<li><?php i18n("KCompressionDevice: don't call write after WriteError (bug 397545)");?></li>
<li><?php i18n("Add missing Q_OBJECT macros for QIODevice subclasses");?></li>
<li><?php i18n("KCompressionDevice: propagate errors from QIODevice::close() (bug 397545)");?></li>
<li><?php i18n("Fix bzip main page");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("Use custom QScrollArea with size hint not limited by font size (bug 389585)");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Remove warning about old kiosk feature that no longer applies");?></li>
<li><?php i18n("Set system default shortcut Ctrl+0 for \"Actual Size\" action");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Don't remove space between two url when line start with \" (kmail bug)");?></li>
<li><?php i18n("KPluginLoader: use '/' even on Windows, libraryPaths() returns paths with '/'");?></li>
<li><?php i18n("KPluginMetaData: convert empty string to empty stringlist");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Revert \"ensure we are always writing in the engine's root context\"");?></li>
<li><?php i18n("Attach property to \"delegate\" (bug 397367)");?></li>
<li><?php i18n("[KCM GridDelegate] Use layer effect only on OpenGL backend (bug 397220)");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("add acronym ASCII to general.entities");?></li>
<li><?php i18n("add JSON to general.entities");?></li>
<li><?php i18n("Let meinproc5 be more verbose in 'install' auto test");?></li>
<li><?php i18n("In kdoctools_install test use absolute paths to find installed files");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Add enum alias Property::Language for typo Property::Langauge");?></li>
</ul>

<h3><?php i18n("KHolidays");?></h3>

<ul>
<li><?php i18n("Implement proper equinox and solstice calculation algorithm (bug 396750)");?></li>
<li><?php i18n("src/CMakeLists.txt - install headers the frameworks-way");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Assert if trying to use a KCatalog without a QCoreApplication");?></li>
<li><?php i18n("Port ki18n from QtScript to QtQml");?></li>
<li><?php i18n("Check the build directory for po/ as well");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Set breeze as fallback icon theme");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("[KSambaShareData] Accept spaces in ACL host name");?></li>
<li><?php i18n("[KFileItemListProperties] Use mostLocalUrl for capabilities");?></li>
<li><?php i18n("[KMountPoint] Also check \"smb-share\" for whether it's an SMB mount (bug 344146)");?></li>
<li><?php i18n("[KMountPoint] Resolve gvfsd mounts (bug 344146)");?></li>
<li><?php i18n("[KMountPoint] Remove traces of supermount");?></li>
<li><?php i18n("[KMountPoint] Remove AIX and Windows CE support");?></li>
<li><?php i18n("Display mounted file system type and mounted from fields in properties dialog (bug 220976)");?></li>
<li><?php i18n("kdirlistertest doesn't fail at random");?></li>
<li><?php i18n("[KUrlComboBox] Fix KIcon porting error");?></li>
<li><?php i18n("Port KPATH_SEPARATOR \"hack\" to QDir::listSeparator, added in Qt 5.6");?></li>
<li><?php i18n("Fixes memory leak in KUrlComboBox::setUrl");?></li>
<li><?php i18n("[KFileItem] Don't read directory comment on slow mounts");?></li>
<li><?php i18n("Use QDir::canonicalPath instead");?></li>
<li><?php i18n("Ignore NTFS hidden flag for root volume (bug 392913)");?></li>
<li><?php i18n("Give the \"invalid directory name\" dialog a cancel button");?></li>
<li><?php i18n("KPropertiesDialog: switch to label in setFileNameReadOnly(true)");?></li>
<li><?php i18n("Refine wording when a folder with an invalid name could not be created");?></li>
<li><?php i18n("Use appropriate icon for a cancel button that will ask for a new name");?></li>
<li><?php i18n("Make read-only filenames selectable");?></li>
<li><?php i18n("Use title case for some button labels");?></li>
<li><?php i18n("Use KLineEdit for folder name if folder has write access, else use QLabel");?></li>
<li><?php i18n("KCookieJar: fix wrong timezone conversion");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("support fillWidth for items");?></li>
<li><?php i18n("guard against external deletion of pages");?></li>
<li><?php i18n("always show the header when we are in collapsible mode");?></li>
<li><?php i18n("fix showContentWhenCollapsed behavior");?></li>
<li><?php i18n("fix holes in menus in Material style");?></li>
<li><?php i18n("standard actionsmenu for the page contextmenu");?></li>
<li><?php i18n("Explicitly request Qt 5.7's QtQuick to make use of Connections.enabled");?></li>
<li><?php i18n("use Window color instead of a background item");?></li>
<li><?php i18n("make sure the drawer closes even when pushing a new");?></li>
<li><?php i18n("export separatorvisible to the globaltoolbar");?></li>
<li><?php i18n("Fix the Kirigami QRC static plugin generation");?></li>
<li><?php i18n("Fix the build in LTO static mode");?></li>
<li><?php i18n("Ensure drawerOpen property is synced correctly (bug 394867)");?></li>
<li><?php i18n("drawer: Display the content widget when dragging");?></li>
<li><?php i18n("Allow qrc assets to be used in Actions icons");?></li>
<li><?php i18n("ld on old gcc (bug 395156)");?></li>
</ul>

<h3><?php i18n("KItemViews");?></h3>

<ul>
<li><?php i18n("Deprecate KFilterProxySearchLine");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Cache providerId");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Support libcanberra for audio notification");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("KBuildSycoca: always process application desktop files");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Turn enum Kate::ScriptType into an enum class");?></li>
<li><?php i18n("correct error handling for QFileDevice and KCompressedDevice");?></li>
<li><?php i18n("InlineNotes: Do not print inline notes");?></li>
<li><?php i18n("Remove QSaveFile in favor of plain old file saving");?></li>
<li><?php i18n("InlineNotes: Use screen global coordinates everywhere");?></li>
<li><?php i18n("InlineNote: Initialize position with Cursor::invalid()");?></li>
<li><?php i18n("InlineNote: Pimpl inline note data without allocs");?></li>
<li><?php i18n("Add inline note interface");?></li>
<li><?php i18n("Show text preview only if main window is active (bug 392396)");?></li>
<li><?php i18n("Fix crash when hiding the TextPreview widget (bug 397266)");?></li>
<li><?php i18n("Merge ssh://git.kde.org/ktexteditor");?></li>
<li><?php i18n("improve hidpi rendering of icon border");?></li>
<li><?php i18n("Improve vim color theme (bug 361127)");?></li>
<li><?php i18n("Search: Add workaround for missing icons in Gnome icon-theme");?></li>
<li><?php i18n("fix overpainting for _ or letters like j in the last line (bug 390665)");?></li>
<li><?php i18n("Extend Scripting API to allow executing commands");?></li>
<li><?php i18n("Indentation script for R");?></li>
<li><?php i18n("Fix crash when replacing \\n around empty lines (bug 381080)");?></li>
<li><?php i18n("remove highlighting download dialog");?></li>
<li><?php i18n("no need to new/delete hash on each doHighlight, clearing it is good enough");?></li>
<li><?php i18n("ensure we can handle invalid attribute indices that can happen as left overs after HL switch for a document");?></li>
<li><?php i18n("let smart pointer handle deletion of objects, less manual stuff to do");?></li>
<li><?php i18n("remove map to lookup additional hl properties");?></li>
<li><?php i18n("KTextEditor uses the KSyntaxHighlighting framework for all");?></li>
<li><?php i18n("use character encodings as provided by the definitions");?></li>
<li><?php i18n("Merge branch 'master' into syntax-highlighting");?></li>
<li><?php i18n("non-bold text no longer renders with font weight thin but (bug 393861)");?></li>
<li><?php i18n("use foldingEnabled");?></li>
<li><?php i18n("remove EncodedCharaterInsertionPolicy");?></li>
<li><?php i18n("Printing: Respect footer font, fix footer vertical position, make header/footer separator line visually lighter");?></li>
<li><?php i18n("Merge branch 'master' into syntax-highlighting");?></li>
<li><?php i18n("let syntax-highlighting framework handle all definition management now that there is the None definition around in the repo");?></li>
<li><?php i18n("completion widget: fix minimum header section size");?></li>
<li><?php i18n("Fix: Scroll view lines instead of real lines for wheel and touchpad scrolling (bug 256561)");?></li>
<li><?php i18n("remove syntax test, that is now tested in the syntax-highlighting framework itself");?></li>
<li><?php i18n("KTextEditor configuration is now application local again, the old global configuration will be imported on first use");?></li>
<li><?php i18n("Use KSyntaxHighlighting::CommentPosition instead of KateHighlighting::CSLPos");?></li>
<li><?php i18n("Use isWordWrapDelimiter() from KSyntaxHighlighting");?></li>
<li><?php i18n("Rename isDelimiter() to isWordDelimiter()");?></li>
<li><?php i18n("implement more lookup stuff via format -&gt; definition link");?></li>
<li><?php i18n("we now always get valid formats");?></li>
<li><?php i18n("nicer way to get attribute name");?></li>
<li><?php i18n("fix python indentation test, safer accessor to property bags");?></li>
<li><?php i18n("add right definition prefixes again");?></li>
<li><?php i18n("Merge branch 'syntax-highlighting' of git://anongit.kde.org/ktexteditor into syntax-highlighting");?></li>
<li><?php i18n("try to bring back lists needed to do the configuration per scheme");?></li>
<li><?php i18n("Use KSyntaxHighlighting::Definition::isDelimiter()");?></li>
<li><?php i18n("make can break bit more like in word code");?></li>
<li><?php i18n("no linked list without any reason");?></li>
<li><?php i18n("cleanup properties init");?></li>
<li><?php i18n("fix order of formats, remember definition in highlighting bag");?></li>
<li><?php i18n("handle invalid formats / zero length formats");?></li>
<li><?php i18n("remove more old implementation parts, fixup some accessors to use the format stuff");?></li>
<li><?php i18n("fix indentation based folding");?></li>
<li><?php i18n("remove exposure of context stack in doHighlight + fix ctxChanged");?></li>
<li><?php i18n("start to store folding stuff");?></li>
<li><?php i18n("rip out highlighting helpers, no longer needed");?></li>
<li><?php i18n("remove need to contextNum, add FIXME-SYNTAX marker to stuff that needs to be fixed up properly");?></li>
<li><?php i18n("adapt to includedDefinitions changes, remove contextForLocation, one only needs either keywords for location or spellchecking for location, can be implemented later");?></li>
<li><?php i18n("remove more no longer used syntax highlighting things");?></li>
<li><?php i18n("fixup the m_additionalData and the mapping for it a bit, should work for attributes, not for context");?></li>
<li><?php i18n("create initial attributes, still without real attribute values, just a list of something");?></li>
<li><?php i18n("call the highlighting");?></li>
<li><?php i18n("derive from abstract highlighter, set definition");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("Move example from techbase to own repo");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Sync set/send/update methods");?></li>
<li><?php i18n("Add serial number and EISA ID to OutputDevice interface");?></li>
<li><?php i18n("Output device color curves correction");?></li>
<li><?php i18n("Fix memory management in WaylandOutputManagement");?></li>
<li><?php i18n("Isolate every test within WaylandOutputManagement");?></li>
<li><?php i18n("OutputManagement fractional scaling");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Create a first example of the use of KMessageBox");?></li>
<li><?php i18n("Fix two bugs in KMessageWidget");?></li>
<li><?php i18n("[KMessageBox] Call style for icon");?></li>
<li><?php i18n("Add workaround for labels with word-wrapping (bug 396450)");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Make Konqi look good in HiDPI");?></li>
<li><?php i18n("Add missing parentheses");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Require NetworkManager 1.4.0 and newer");?></li>
<li><?php i18n("manager: add support to R/W the GlobalDnsConfiguration property");?></li>
<li><?php i18n("Actually allow to set the refresh rate for device statistics");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Workaround bug with native rendering and opacity in TextField text (bug 396813)");?></li>
<li><?php i18n("[Icon Item] Watch KIconLoader icon change when using QIcon (bug 397109)");?></li>
<li><?php i18n("[Icon Item] Use ItemEnabledHasChanged");?></li>
<li><?php i18n("Get rid of deprecated QWeakPointer usage");?></li>
<li><?php i18n("Fix style sheet for 22-22-system-suspend (bug 397441)");?></li>
<li><?php i18n("Improve Widgets' removal and configure text");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("solid/udisks2: Add support for categorized logging");?></li>
<li><?php i18n("[Windows Device] Show device label only if there is one");?></li>
<li><?php i18n("Force reevaluation of Predicates if interfaces are removed (bug 394348)");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("hunspell: Restore build with hunspell &lt;=v1.5.0");?></li>
<li><?php i18n("Include hunspell headers as system includes");?></li>
</ul>

<h3><?php i18n("syndication");?></h3>

<p>New module</p>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("highlight 20000 lines per testcase");?></li>
<li><?php i18n("make highlighting benchmark more reproducible, we anyways want to measure this execution with e.g. perf from the outside");?></li>
<li><?php i18n("Tune KeywordList lookup &amp; avoid allocations for implicit capture group");?></li>
<li><?php i18n("remove captures for Int, never implemented");?></li>
<li><?php i18n("deterministic iteration of tests for better result comparison");?></li>
<li><?php i18n("handle nested include attributes");?></li>
<li><?php i18n("update Modula-2 highlighting (bug 397801)");?></li>
<li><?php i18n("precompute attribute format for context &amp; rules");?></li>
<li><?php i18n("avoid word delimiter check at start of keyword (bug 397719)");?></li>
<li><?php i18n("Add syntax highlighting for SELinux kernel policy language");?></li>
<li><?php i18n("hide bestCandidate, can be static function inside file");?></li>
<li><?php i18n("Add some improvements to kate-syntax-highlighter for use in scripting");?></li>
<li><?php i18n("added := as a valid part of an identifier");?></li>
<li><?php i18n("use our own input data for benchmarking");?></li>
<li><?php i18n("try to fix line ending issue in compare of results");?></li>
<li><?php i18n("try trivial diff output for Windows");?></li>
<li><?php i18n("add defData again for valid state check");?></li>
<li><?php i18n("decrease StateData space by more than 50% and half the number of needed mallocs");?></li>
<li><?php i18n("improve performance of Rule::isWordDelimiter and KeywordListRule::doMatch");?></li>
<li><?php i18n("Improve skip offset handling, allow to skip full line on no match");?></li>
<li><?php i18n("check extensions wildcard list");?></li>
<li><?php i18n("more asterisk hl, I tried some asterisk configs, they are just ini style, use .conf as ini ending");?></li>
<li><?php i18n("fix highlighting for #ifdef _xxx stuff (bug 397766)");?></li>
<li><?php i18n("fix wildcards in files");?></li>
<li><?php i18n("MIT relicensing of KSyntaxHighlighting done");?></li>
<li><?php i18n("JavaScript: add binaries, fix octals, improve escapes &amp; allow Non-ASCII identifiers (bug 393633)");?></li>
<li><?php i18n("Allow to turn of the QStandardPaths lookups");?></li>
<li><?php i18n("Allow to install syntax files instead of having them in a resource");?></li>
<li><?php i18n("handle context switch attributes of the contexts themselves");?></li>
<li><?php i18n("change from static lib to object lib with right pic setting, should work for shared + static builds");?></li>
<li><?php i18n("avoid any heap allocation for default constructed Format() as used as \"invalid\"");?></li>
<li><?php i18n("honor cmake variable for static vs. dynamic lib, like e.g. karchive");?></li>
<li><?php i18n("MIT relicensing, https://phabricator.kde.org/T9455");?></li>
<li><?php i18n("remove old add_license script, no longer needed");?></li>
<li><?php i18n("Fix includedDefinitions, handle definition change in context switch (bug 397659)");?></li>
<li><?php i18n("SQL: various improvements and fix if/case/loop/end detection with SQL (Oracle)");?></li>
<li><?php i18n("fix reference files");?></li>
<li><?php i18n("SCSS: update syntax. CSS: fix Operator and Selector Tag highlighting");?></li>
<li><?php i18n("debchangelog: add Bookworm");?></li>
<li><?php i18n("Relicense Dockerfile to MIT license");?></li>
<li><?php i18n("remove the no longer supported configuration part of the spellchecking that always had just one mode we now hardcode");?></li>
<li><?php i18n("Add syntax highlighting support for Stan");?></li>
<li><?php i18n("add back indenter");?></li>
<li><?php i18n("Optimize many syntax highlighting files and fix the '/' char of SQL");?></li>
<li><?php i18n("Modelines: add byte-order-mark &amp; small fixes");?></li>
<li><?php i18n("Relicense modelines.xml to MIT license (bug 198540)");?></li>
<li><?php i18n("Add QVector&lt;QPair&lt;QChar, QString&gt;&gt; Definition::characterEncodings() const");?></li>
<li><?php i18n("Add bool Definition::foldingEnabled() const");?></li>
<li><?php i18n("Add \"None\" highlighting to repository per default");?></li>
<li><?php i18n("Update Logtalk language syntax support");?></li>
<li><?php i18n("Add Autodesk EAGLE sch and brd file format to the XML category");?></li>
<li><?php i18n("C# highlighting: Prefer C-Style indenter");?></li>
<li><?php i18n("AppArmor: update syntax and various improvements/fixes");?></li>
<li><?php i18n("Java: add binaries &amp; hex-float, and support underscores in numbers (bug 386391)");?></li>
<li><?php i18n("Cleanup: indentation was moved from general to language section");?></li>
<li><?php i18n("Definition: Expose command markers");?></li>
<li><?php i18n("Add highlighting of JavaScript React");?></li>
<li><?php i18n("YAML: fix keys, add numbers and other improvements (bug 389636)");?></li>
<li><?php i18n("Add bool Definition::isWordWrapDelimiter(QChar)");?></li>
<li><?php i18n("Definition: Rename isDelimiter() to isWordDelimiter()");?></li>
<li><?php i18n("Note KF6 API improvement ideas from the KTE porting");?></li>
<li><?php i18n("Provide a valid format for empty lines too");?></li>
<li><?php i18n("Make Definition::isDelimiter() also work for invalid definitions");?></li>
<li><?php i18n("Definition: Expose bool isDelimiter() const");?></li>
<li><?php i18n("Sort returned formats in Definition::formats() by id");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.50");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.8");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
