<?php
  $page_title = "KDE 3.2.3 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>DATELINE June 9, 2004</p>
<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE Project Ships Third Translation and Service Release for Leading Open Source Desktop
</h3>

<p align="justify">
  <strong>
    KDE Project Ships Third Translation and Service Release of the 3.2 Generation
    GNU/Linux - UNIX Desktop, Offering Enterprises and Governments a Compelling
    Free and Open Desktop Solution
  </strong>
</p>

<p align="justify">
  June 9, 2004 (The INTERNET).  The <a href="http://www.kde.org/">KDE
  Project</a> today announced the immediate availability of KDE 3.2.3,
  a maintenance release for the latest generation of the most advanced and
  powerful <em>free</em> desktop for GNU/Linux and other UNIXes.  KDE 3.2.3
  ships with a basic desktop and eighteen other packages
  (PIM, administration, network, edutainment, utilities, multimedia, games,
  artwork, web development and more).  KDE's award-winning tools and
  applications are available in <strong>51 languages</strong> (now including
  Arabic, Croatian and Upper Sorbian compared to KDE 3.2.2).
</p>

<p align="justify">
  KDE, including all its libraries and its applications, is
  available <em><strong>for free</strong></em> under Open Source licenses.
  KDE can be obtained in source and numerous binary formats from
  <a href="http://download.kde.org/stable/3.2.3/">http://download.kde.org</a> and can
  also be obtained on <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>
  or with any of the major GNU/Linux - UNIX systems shipping today.
</p>

<h4>
  <a name="changes">Enhancements</a>
</h4>
<p align="justify">
  KDE 3.2.3 is a maintenance release which provides corrections of problems
  reported using the KDE <a href="http://bugs.kde.org/">bug tracking system</a>
  and enhanced support for existing translations.
</p>
<p align="justify">
  For a more detailed list of improvements since the KDE 3.2.2 release in April,
  please refer to the
  <a href="changelogs/changelog3_2_2_to_3_2_3.php">KDE 3.2.3 Changelog</a>.
</p>
<p>
  Additional information about the enhancements of the KDE 3.2.x release
  series is available in the
  <a href="announce-3.2.php">KDE 3.2 Announcement</a>.
</p>

<h4>
  Installing KDE 3.2.3 Binary Packages
</h4>
<p align="justify">
  <em>Packaging Policies</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE 3.2.3 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.3/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now
  available, may become available over the coming weeks.
</p>

<p align="justify">
  <a name="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE
  Project has been informed, please visit the
  <a href="http://www.kde.org/info/3.2.3.php">KDE 3.2.3 Info Page</a>.
</p>

<h4>
  Compiling KDE 3.2.3
</h4>
<p align="justify">
  <a name="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 3.2.3 may be
  <a href="http://download.kde.org/stable/3.2.3/src/">freely
  downloaded</a>.  Instructions on compiling and installing KDE 3.2.3
  are available from the <a href="/info/3.2.3.php#binary">KDE
  3.2.3 Info Page</a>.
</p>

<h4>
  KDE Sponsorship
</h4>
<p align="justify">
  Besides the superb and invaluable efforts by the
  <a href="http://www.kde.org/people/">KDE developers</a>
  themselves, significant support for KDE development has been provided by
  <a href="http://www.mandrakesoft.com/">MandrakeSoft</a>,
  <a href="http://www.trolltech.com/">Trolltech</a> and
  <a href="http://www.suse.com/">SuSE</a>.  In addition,
  the members of the <a href="http://www.kdeleague.org/">KDE
  League</a> provide significant support for KDE promotion,
  <a href="http://www.ibm.com/">IBM</a> has donated significant hardware
  to the KDE Project, and the
  <a href="http://www.uni-tuebingen.de/">University of T&uuml;bingen</a>
  and the <a href="http://www.uni-kl.de/">University of Kaiserslautern</a>
  provide most of the Internet bandwidth for the KDE project.  Thanks!
</p>

<h4>
  About KDE
</h4>
<p align="justify">
  KDE is an independent project of hundreds of developers, translators,
  artists and other professionals worldwide collaborating over the Internet
  to create and freely distribute a sophisticated, customizable and stable
  desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform.  KDE provides a stable, mature desktop, a full, component-based
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking and administration tools and utilities, and an
  efficient, intuitive development environment featuring the excellent IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.  KDE is working proof
  that the Open Source "Bazaar-style" software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software.
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<p align="justify">
  <font size="2">
  <em>Trademark Notices.</em>
  KDE and K Desktop Environment are trademarks of KDE e.V.

  Linux is a registered trademark of Linus Torvalds.

  UNIX is a registered trademark of The Open Group in the United States and
  other countries.

  All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.
  </font>
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<h4>Press Contacts</h4>
<table cellpadding="10"><tr valign="top">
<td>

<b>Africa</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="ma&#x69;l&#0116;o&#58;&#0105;&#0110;&#102;&#0111;-&#x61;fric&#097;&#00064;&#x6b;de.&#x6f;&#x72;g">in&#x66;o&#x2d;&#97;&#x66;&#0114;i&#0099;a&#x40;&#107;&#100;e.&#00111;&#114;&#00103;</a><br />
</td>

<td>
<b>Asia</b><br />
Sirtaj S. Kang <br />
C-324 Defence Colony <br />
New Delhi <br />
India 110024 <br />
Phone: +91-981807-8372 <br />
<a href="&#109;&#97;&#x69;l&#116;o:&#x69;nf&#111;&#x2d;a&#x73;i&#x61;&#64;kde&#x2e;&#x6f;r&#0103;">i&#x6e;&#102;o&#00045;as&#105;&#97;&#064;k&#x64;e.o&#114;g</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europe</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="mail&#116;&#0111;:&#x69;&#x6e;&#102;o-e&#117;&#x72;o&#112;e&#64;&#107;&#100;&#x65;.&#0111;r&#103;">i&#x6e;&#102;&#111;&#0045;&#0101;&#x75;r&#x6f;pe&#064;kde&#46;o&#00114;g</a>
</td>

<td>
<b>North America</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Phone: (416)-925-4030 <br />
<a href="mai&#108;&#00116;o:i&#0110;&#102;&#111;&#00045;&#x6e;o&#114;&#116;&#x68;a&#0109;er&#x69;c&#97;&#64;kd&#0101;.&#111;&#00114;g">&#x69;nf&#111;-&#x6e;o&#x72;th&#097;&#x6d;&#101;r&#x69;&#0099;&#97;&#64;k&#x64;&#x65;&#x2e;&#111;&#00114;&#x67;</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="&#109;&#x61;il&#x74;&#111;&#x3a;&#x69;n&#x66;o-&#00111;&#00099;e&#97;n&#105;a&#0064;&#107;&#100;&#x65;.&#0111;rg">info&#045;oce&#97;nia&#x40;&#107;de.&#111;&#114;g</a><br />
</td>

<td>
<b>South America</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="&#109;a&#x69;&#x6c;t&#0111;:i&#0110;f&#x6f;-s&#111;&#x75;tham&#00101;ri&#x63;a&#x40;&#107;de&#46;&#x6f;r&#x67;">&#x69;nf&#x6f;&#045;sou&#x74;ha&#0109;&#x65;&#x72;ic&#97;&#x40;&#x6b;&#100;e&#0046;or&#x67;</a><br />
</td>


</tr></table>

<?php

  include("footer.inc");
?>
