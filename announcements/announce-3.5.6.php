<?php

  $page_title = "KDE 3.5.6 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

<!-- Other languages translations -->
Also available in:
<a href="announce-3.5.6-ca.php">Catalan</a>
<a href="http://www.kdecn.org/announcements/announce-3.5.6.php">Chinese (simplified)</a>
<a href="announce-3.5.6-de.php">German</a>
<a href="announce-3.5.6-es.php">Spanish</a>
<a href="http://fr.kde.org/announcements/announce-3.5.6-fr.php">French</a>
<a href="http://www.is.kde.org/announcements/announce-3.5.6.php">Icelandic</a>
<a href="announce-3.5.6-it.php">Italian</a>
<a href="announce-3.5.6-nl.php">Dutch</a>
<a href="announce-3.5.6-pt.php">Portuguese</a>
<a href="announce-3.5.6-pt-br.php">Brazilian Portuguese</a>
<a href="announce-3.5.6-sl.php">Slovenian</a>
<a href="announce-3.5.6-sv.php">Swedish</a>

<h3 align="center">
   KDE Project Ships Sixth Translation and Service Release for Leading Free
   Software Desktop
</h3>

<p align="justify">
  <strong>
    KDE 3.5.6 features translations in 65 languages, improvements in the
    the HTML rendering engine (KHTML) and other applications.
  </strong>
</p>

<p align="justify">
  January 25, 2007 (The INTERNET).  The <a href="http://www.kde.org/">KDE
  Project</a> today announced the immediate availability of KDE 3.5.6,
  a maintenance release for the latest generation of the most advanced and
  powerful <em>free</em> desktop for GNU/Linux and other UNIXes. KDE now
  supports 65 languages, making it available to more people than most non-free
  software and can be easily extended to support others by communities who wish
  to contribute to the open source project.
</p>

<p align="justify">
  This release includes a number of bugfixes for KHTML, <a href="http://kate.kde.org">Kate</a>, 
  the kicker, ksysguard and lots of other applications. Significant features include additional support for compiz as a window manager with kicker,
  session management browser tabs for <a href="http://akregator.kde.org">Akregator</a>,
  templating for <a href="http://kontact.kde.org/kmail/">KMail</a> messages, and new summary menus for 
  <a href="http://kontact.kde.org">Kontact</a> making it easier to work with your appointments and to-do's. Translations continue as well, with <a href="http://l10n.kde.org/team-infos.php?teamcode=gl">Galician</a> translations nearly doubling to 78%.
</p>

<p align="justify">
  For a more detailed list of improvements since
  <a href="http://www.kde.org/announcements/announce-3.5.5.php">the KDE 3.5.5 release</a>
  on the 11th October 2006, please refer to the
  <a href="http://www.kde.org/announcements/changelogs/changelog3_5_5to3_5_6.php">KDE 3.5.6 Changelog</a>.
</p>

<p align="justify">
  KDE 3.5.6 ships with a basic desktop and fifteen other packages (PIM,
  administration, network, edutainment, utilities, multimedia, games,
  artwork, web development and more). KDE's award-winning tools and
  applications are available in <strong>65 languages</strong>.
</p>

<h4>
  Distributions shipping KDE
</h4>
<p align="justify">
  Most of the Linux distributions and UNIX operating systems do not immediately
  incorporate new KDE releases, but they will integrate KDE 3.5.6 packages in
  their next releases. Check
  <a href="http://www.kde.org/download/distributions.php">this list</a> to see
  which distributions are shipping KDE.
</p>

<h4>
  Installing KDE 3.5.6 Binary Packages
</h4>
<p align="justify">
  <em>Package Creators</em>.
  Some operating system vendors have kindly provided binary packages of
  KDE 3.5.6 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  download server at
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.6/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now
  available, may become available over the coming weeks.
</p>

<p align="justify">
  <a name="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE
  Project has been informed, please visit the
  <a href="/info/3.5.6.php">KDE 3.5.6 Info Page</a>.
</p>

<h4>
  Compiling KDE 3.5.6
</h4>
<p align="justify">
  <a name="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 3.5.6 may be
  <a href="http://download.kde.org/stable/3.5.6/src/">freely
  downloaded</a>.  Instructions on compiling and installing KDE 3.5.6
  are available from the <a href="/info/3.5.6.php">KDE
  3.5.6 Info Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a> project that exists and grows only because of the
help of many volunteers that donate their time and effort. KDE
is always looking for new volunteers and contributions, whether its
help with coding, bug fixing or reporting, writing documentation,
translations, promotion, money, etc. All contributions are gratefully
appreciated and eagerly accepted. Please read through the <a href="/community/donations/">Supporting
KDE page</a> for further information. </p>

<p align="justify">
We look forward to hearing from you soon!
</p>


<?php
  include("../contact/about_kde.inc");
?>

<h4>Press Contacts</h4>
<?php
  include("../contact/press_contacts.inc");
  include("footer.inc");
?>
