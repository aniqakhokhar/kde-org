<?php
  $page_title = "Plasma Active 3 Improves Performance, Brings New Apps";
  $site_root = "../";
  include "header.inc";
?>


<p align="justify">
KDE has released the 3rd stable version of Plasma Active, KDE's device-independent user experience. The Plasma Active user interface is touch-friendly and works well across a range of devices. Its Activities function gives users a natural way to organize and access their applications, files and information. Plasma Active Three noticeably improves the user experience with its enhanced and expanded set of apps, improved performance and a new virtual keyboard.
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="images/1-plasma-active-welcome.png"><img src="images/thumbs/1-plasma-active-welcome.png" align="center" width="600" alt="Plasma Active's Friendly Welcome Activity" title="Plasma Active's Friendly Welcome Activity" /></a>
<br />
<em>Plasma Active's Friendly Welcome Activity</em>
</div>

<p align="justify">A video introduction to Plasma Active Three can be viewed below or <a href="http://share.basyskom.com/contour/UIDesign/RC2_plasma_active_two-with_Audio.ogv">downloaded</a>.</p>

<div align="center"><iframe style="margin-top: 4em" width="400" height="240" src="http://www.youtube.com/embed/ulcizzAj-N4" frameborder="0" allowfullscreen></iframe></div>


<h2>New Apps</h2>
<p align="justify"><strong>Files</strong>  is a new default application in Plasma Active. It is a file manager, but, unlike most others, it isn't based on folders; rather users can search for documents by file type, creation time and semantic information such as Tags. Files does not use the file system directly; it organizes documents with Nepomuk, Plasma Active's underlying semantic engine.
</p>

<p align="justify"><strong>Okular Active</strong> is Plasma Active's new Ebook Reader. Okular Active is built on the technology which also drives the desktop version of the popular Document Viewer, and is optimized for reading documents on a touch device. Okular Active supports a wide range of file formats, such as PDF, EPub, Open Document, and many others.
</p>
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="images/9-plasma-active-readertoc.png"><img src="images/thumbs/9-plasma-active-readertoc.png" align="center" width="600" alt="Plasma Active's Reader" title="Plasma Active's Reader" /></a>
<br />
<em>Plasma Active's Reader</em>
</div>
<p align="justify">Through Plasma Active's <strong>Add Ons</strong>, thousands of ebooks are available for free, with paid applications and content coming soon.
</p>

<h2>Champions: Kontact and Calligra</h2>
<p align="justify">With Calligra Active and Kontact Touch, Plasma Active delivers scalable and proven applications for office and groupware tasks, with a focus on interoperability. Kontact Touch supports many groupware solutions, and brings calendaring, email, contact management among other features. Calligra Active has excellent support for most common office file formats such as OpenDocument and Microsoft's .doc, .docx, .xls and .xlsx.
</p>
<p align="justify">The KDE developers have put a lot of effort into improving the performance of the user experience, its applications and underlying libraries. These improvements bring a noticeably snappier and more visually coherent user interface, and make using Plasma Active a more enjoyable experience compared to previous versions.
</p>
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="images/4-plasma-active-files.png"><img src="images/thumbs/4-plasma-active-files.png" align="center" width="600" alt="Plasma Active's Semantic File Browser" title="Plasma Active's Semantic File Browser" /></a>
<br />
<em>Plasma Active's Semantic File Browser</em>
</div>

<h2>Plasma Active runs on Mer</h2>
<p align="justify">Mer is the leading openly developed Linux-based OS for mobile devices. It is based on work from the MeeGo Project. With the size of the Core OS reduced to the bare essentials, Mer delivers performance. It is also a fully open development project, and thus supports fundamental Plasma Active design principles—openness, user freedom.
</p>
<h2>Improved Text Input</h2>
<p align="justify">Thanks to a new virtual keyboard based on Maliit, the input method used on devices such as Nokia's N9, Plasma Active Three improves ease of text input. The new virtual keyboard is faster and more convenient to use and offers great flexibility for system integrators.
</p>
<h2>Roadmap: More Apps, Qt5 and File Synchronization</h2>
<p align="justify">In future releases, users can expect support for a wider range of devices, easier synchronization of data across devices, an improved user experience through the use of Qt5 and KDE Frameworks 5, and more applications for popular uses.
<br />The KDE team is currently working to bring a Plasma Active-based tablet to the market to demonstrate its capabilities fully and offer users a fresh and more open alternative to existing mobile operating systems. 
</p>
<h2>About Plasma Active</h2>
<p align="justify">Plasma Active is openly developed Free software. Its reference implementation builds on the Mer operating system and runs on a number of devices, including popular x86-based tablet computers and ARM-based hardware such as the Archos G9 series. Plasma Active can be used as a base for creating custom user experiences, and for a variety of purposes on existing and  emerging touch-enabled devices. Participation in Plasma Active is welcomed, either through individual contributions or by joining the Make * Play * Live partner network as an official support or distribution partner.
</p>


<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
