<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.13.5 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.13.5";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='breeze-gtk' href='https://commits.kde.org/breeze-gtk'>Breeze GTK</a> </h3>
<ul id='ulbreeze-gtk' style='display: block'>
<li>Fix scrollbars in LibreOffice. <a href='https://commits.kde.org/breeze-gtk/43f2cb7c2334f5438d20ebf1ac04432a18ce5edf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389709'>#389709</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15110'>D15110</a></li>
<li>[kconf_update] Add "gtk-primary-button-warps-slider=0" to generated GTK3 config. <a href='https://commits.kde.org/breeze-gtk/f5121ba89abf9eb50b42ab9c95f1f4c7e57bd044'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379773'>#379773</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14863'>D14863</a></li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Don't try to trigger an empty transaction. <a href='https://commits.kde.org/discover/31ea5cc11526d454e880337cc904ad6361d7eac4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397759'>#397759</a></li>
<li>PK: wait until we have fetched the packages that dont' come from appstream. <a href='https://commits.kde.org/discover/2155003d054e75d173307360c81fea41890b590f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397192'>#397192</a></li>
<li>Fix navigation after search. <a href='https://commits.kde.org/discover/e41df0e3b0b836c5dea3f0988ba713711127582b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391931'>#391931</a></li>
<li>Remove wrong assert. <a href='https://commits.kde.org/discover/310685b3db21d03aeca91c6df1753a2f9851287f'>Commit.</a> </li>
</ul>


<h3><a name='kactivitymanagerd' href='https://commits.kde.org/kactivitymanagerd'>kactivitymanagerd</a> </h3>
<ul id='ulkactivitymanagerd' style='display: block'>
<li>If a query failure is bad enough for an assert, use qCWarning... <a href='https://commits.kde.org/kactivitymanagerd/04e2d96e4b68f9df6ddd17b63e1f959976a9ade6'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>[POTD] Actually update the image every day. <a href='https://commits.kde.org/kdeplasma-addons/81e89f1ea830f278fdb6f086baa4741c9606892f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397914'>#397914</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15124'>D15124</a></li>
</ul>


<h3><a name='kscreenlocker' href='https://commits.kde.org/kscreenlocker'>KScreenlocker</a> </h3>
<ul id='ulkscreenlocker' style='display: block'>
<li>Prevent paste in screen locker. <a href='https://commits.kde.org/kscreenlocker/1638db3fefcae76f27f889b3709521b608aa67ad'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388049'>#388049</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14924'>D14924</a></li>
<li>Load QtQuickSettings for software rendering. <a href='https://commits.kde.org/kscreenlocker/c25251a7eb051c7e6914e188f39773d654cb7358'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14708'>D14708</a></li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Avoid global static for effects. <a href='https://commits.kde.org/kwin/3cfb7a30f00517e2d3a43dd422c7e72c749ea911'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15163'>D15163</a></li>
<li>[effects] Use more effectData() in BuiltInEffects. <a href='https://commits.kde.org/kwin/fa5242b3eefd02683d98b773f1c64da6c4379fbe'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13587'>D13587</a></li>
<li>[kcmkwin] Use QtQuick.Controls 2.0 for Label and TextField. <a href='https://commits.kde.org/kwin/22d898399b38cadfd72474a2b50f1d41db7bd28e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366451'>#366451</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14495'>D14495</a></li>
<li>Update kwindecoration docbook. <a href='https://commits.kde.org/kwin/06fb902e14cc2fd4419857f6dfc007d62adaf556'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11453'>D11453</a></li>
</ul>


<h3><a name='milou' href='https://commits.kde.org/milou'>Milou</a> </h3>
<ul id='ulmilou' style='display: block'>
<li>Fixed drag pixmap not showing when dragging results off KRunner's window. <a href='https://commits.kde.org/milou/816a923f7c05047391834fc115c5da25563978f5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14545'>D14545</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>[plasma-changeicons] Call sync() after writing. <a href='https://commits.kde.org/plasma-desktop/716deea83daec267ebc97f0e0ed950b6c6da08fb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14672'>D14672</a></li>
<li>Revert "Fix bad model hygiene in Positioner::move()". <a href='https://commits.kde.org/plasma-desktop/f23b9dc0d57df80863e988208e60135a7f42ad12'>Commit.</a> </li>
<li>Kicker: Reset re-used menu width to default one. <a href='https://commits.kde.org/plasma-desktop/5dd480208afc33e526ed25f0478fa424e518366e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397206'>#397206</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14836'>D14836</a></li>
<li>[Style KCM] Use "configure" icon. <a href='https://commits.kde.org/plasma-desktop/8ed77e53af334ab22834cc4a4740cdb9e46d99f1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397358'>#397358</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14746'>D14746</a></li>
<li>Fontinst quits after KJob is done. <a href='https://commits.kde.org/plasma-desktop/690570a4cefd786db5113ca237e9bdb48cd50812'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379524'>#379524</a>. Fixes bug <a href='https://bugs.kde.org/379324'>#379324</a>. Fixes bug <a href='https://bugs.kde.org/349673'>#349673</a>. Fixes bug <a href='https://bugs.kde.org/361960'>#361960</a>. Fixes bug <a href='https://bugs.kde.org/392267'>#392267</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14493'>D14493</a></li>
<li>[Desktop Toolbox] Emit contextualActionsAboutToShow before opening. <a href='https://commits.kde.org/plasma-desktop/3485b7d30efa2fadf695391bd7947aac0cded819'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/384862'>#384862</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14535'>D14535</a></li>
<li>Fix bad model hygiene in Positioner::move(). <a href='https://commits.kde.org/plasma-desktop/1cb71a2cca8a46512f5ac0cb03da268dde8d9c7d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396666'>#396666</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14243'>D14243</a></li>
</ul>


<h3><a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a> </h3>
<ul id='ulplasma-integration' style='display: block'>
<li>Fix QFileDialog not remembering the last visited directory. <a href='https://commits.kde.org/plasma-integration/b269980db1d7201a0619f3f5c6606c96b8e59d7d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14437'>D14437</a></li>
<li>File Dialog: fix testSelectUrl() again, i.e. selectUrl() should set the directory too. <a href='https://commits.kde.org/plasma-integration/22da1e661afdf7f05dbbc0ed0d59a49987e2a32a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14440'>D14440</a></li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Fix a text label in the password dialog. <a href='https://commits.kde.org/plasma-nm/af9f12b2909dc244197c32f241d1535d00d9a8e5'>Commit.</a> </li>
<li>Add additional checks for device existence. <a href='https://commits.kde.org/plasma-nm/9a06a8e033a6f4dbfb690324cbf767b7cb3fa5ee'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396957'>#396957</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[MPRIS Data Engine] Avoid blocking calls in media shortcuts handling. <a href='https://commits.kde.org/plasma-workspace/eac5d2e40e1edaf258d9ce8181666326c333c412'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397685'>#397685</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14970'>D14970</a></li>
<li>[Solid Device Engine] Don't update free space info for inaccessible storage devices. <a href='https://commits.kde.org/plasma-workspace/e29ac5c3ff38f48d8b48bddf2e308dcc68dc9ba8'>Commit.</a> See bug <a href='https://bugs.kde.org/355258'>#355258</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14532'>D14532</a></li>
<li>[Notifications Engine] Guard "this". <a href='https://commits.kde.org/plasma-workspace/7a98c7d5e58a3afef1f85eafa92aed5ce1f30de9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397105'>#397105</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14651'>D14651</a></li>
<li>[Service Runner] Look up relative entryPaths. <a href='https://commits.kde.org/plasma-workspace/cf7381d7fbff0c966b93b7c2df4cd748011689e2'>Commit.</a> See bug <a href='https://bugs.kde.org/397070'>#397070</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14546'>D14546</a></li>
<li>[Desktop Scripting ConfigGroup] Add more nullptr checks. <a href='https://commits.kde.org/plasma-workspace/79ac08108966f1e978203de71149861a18d6bae8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14576'>D14576</a></li>
<li>Use 1 instead of true for gtk-shell-shows-menubar. <a href='https://commits.kde.org/plasma-workspace/2829367e2fba15ec3822df1cf5fca85c0410d014'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397087'>#397087</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14556'>D14556</a></li>
</ul>


<h3><a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a> </h3>
<ul id='ulxdg-desktop-portal-kde' style='display: block'>
<li>Fix build condition for screencast portal. <a href='https://commits.kde.org/xdg-desktop-portal-kde/77c9ec54a612625d799f7b84358dfd81bcaee8d5'>Commit.</a> </li>
<li>We don't need to link against libspa. <a href='https://commits.kde.org/xdg-desktop-portal-kde/23e139b7e8d4fbdba7f8e1eba1442c3be9d84d35'>Commit.</a> </li>
<li>Search also for libpipewire-0.2. <a href='https://commits.kde.org/xdg-desktop-portal-kde/a96074bd1bc2cb1d5795b242595616cc0d4718e4'>Commit.</a> </li>
</ul>


</main>
<?php
	require('../aether/footer.php');
