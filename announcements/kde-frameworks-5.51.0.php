<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.51.0");
  $site_root = "../";
  $release = '5.51.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
October 15, 2018. KDE today announces the release
of KDE Frameworks 5.51.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Add calls to KIO::UDSEntry::reserve in timeline/tags ioslaves");?></li>
<li><?php i18n("[balooctl] Flush buffered \"Indexing &lt;file&gt;\" line when indexing starts");?></li>
<li><?php i18n("[FileContentIndexer] Connect finished signal from extractor process");?></li>
<li><?php i18n("[PositionCodec] Avoid crash in case of corrupt data (bug 367480)");?></li>
<li><?php i18n("Fix invalid char constant");?></li>
<li><?php i18n("[Balooctl] remove directory parent check (bug 396535)");?></li>
<li><?php i18n("Allow removing non-existent folders from include and exclude lists (bug 375370)");?></li>
<li><?php i18n("Use String to store UDS_USER and UDS_GROUP of String type (bug 398867)");?></li>
<li><?php i18n("[tags_kio] Fix parenthesis. Somehow this got by my code checker");?></li>
<li><?php i18n("Exclude genome files from indexing");?></li>
</ul>

<h3><?php i18n("BluezQt");?></h3>

<ul>
<li><?php i18n("Implement Media and MediaEndpoint API");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Fix \"stack-use-after-scope\" detected by ASAN in CI");?></li>
<li><?php i18n("Fix monochrome icons missing stylesheets");?></li>
<li><?php i18n("Change drive-harddisk to more adaptable style");?></li>
<li><?php i18n("Add firewall-config and firewall-applet icons");?></li>
<li><?php i18n("Make lock on plasmavault icon visible with breeze-dark");?></li>
<li><?php i18n("Add plus symbol to document-new.svg (bug 398850)");?></li>
<li><?php i18n("Provide icons for 2x scaling");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Compile python bindings with the same sip flags used by PyQt");?></li>
<li><?php i18n("Android: Allow passing a relative path as the apk dir");?></li>
<li><?php i18n("Android: Properly offer a fallback to applications that don't have a manifest");?></li>
<li><?php i18n("Android: Make sure Qm translations get loaded");?></li>
<li><?php i18n("Fix Android builds using cmake 3.12.1");?></li>
<li><?php i18n("l10n: Fix matching digits in the repository name");?></li>
<li><?php i18n("Add QT_NO_NARROWING_CONVERSIONS_IN_CONNECT as default compile flags");?></li>
<li><?php i18n("Bindings: Correct handling of sources containing utf-8");?></li>
<li><?php i18n("Actually iterate over CF_GENERATED, rather than checking item 0 all the time");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Fix dangling reference with \"auto\" becoming \"QStringBuilder\"");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("manage return events");?></li>
<li><?php i18n("Manually resize KCMUtilDialog to sizeHint() (bug 389585)");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Fix issue when reading path lists");?></li>
<li><?php i18n("kcfg_compiler now documents valid inputs for its 'Color' type");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("remove own implementation of QString to TString conversion for taglibwriter");?></li>
<li><?php i18n("increase test coverage of taglibwriter");?></li>
<li><?php i18n("implement more basic tags for taglibwriter");?></li>
<li><?php i18n("remove usage of own TString to QString conversion function");?></li>
<li><?php i18n("bump required taglib version to 1.11.1");?></li>
<li><?php i18n("implement reading of the replaygain tags");?></li>
</ul>

<h3><?php i18n("KHolidays");?></h3>

<ul>
<li><?php i18n("add Ivory Coast holidays (French) (bug 398161)");?></li>
<li><?php i18n("holiday_hk_* - fix date for Tuen Ng Festival in 2019 (bug 398670)");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Properly scope CMAKE_REQUIRED_LIBRARIES change");?></li>
<li><?php i18n("Android: Make sure we're looking for .mo files in the right path");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Start drawing emblems in the bottom-right corner");?></li>
</ul>

<h3><?php i18n("KImageFormats");?></h3>

<ul>
<li><?php i18n("kimg_rgb: optimize away QRegExp and QString::fromLocal8Bit");?></li>
<li><?php i18n("[EPS] Fix crash at app shutdown (being tried to persist clipboard image) (bug 397040)");?></li>
</ul>

<h3><?php i18n("KInit");?></h3>

<ul>
<li><?php i18n("Lessen log spam by not checking for existence of file with empty name (bug 388611)");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("allow non-local file:// redirect to a Windows WebDav URL");?></li>
<li><?php i18n("[KFilePlacesView] Change icon for the 'Edit' context menu entry in Places panel");?></li>
<li><?php i18n("[Places panel] use more appropriate network icon");?></li>
<li><?php i18n("[KPropertiesDialog] Show mount information for folders in / (root)");?></li>
<li><?php i18n("Fix deletion of files from DAV (bug 355441)");?></li>
<li><?php i18n("Avoid QByteArray::remove in AccessManagerReply::readData (bug 375765)");?></li>
<li><?php i18n("Don't try to restore invalid user places");?></li>
<li><?php i18n("Make it possible to change directory up even with trailing slashes in the url");?></li>
<li><?php i18n("KIO slave crashes are now handled by KCrash instead of subpar custom code");?></li>
<li><?php i18n("Fixed a file being created from pasted clipboard contents showing up only after a delay");?></li>
<li><?php i18n("[PreviewJob] Send enabled thumbnail plugins to the thumbnail slave (bug 388303)");?></li>
<li><?php i18n("Improve \"insufficient disk space\" error message");?></li>
<li><?php i18n("IKWS: use non-deprecated \"X-KDE-ServiceTypes\" in desktop file generation");?></li>
<li><?php i18n("Fix WebDAV destination header on COPY and MOVE operations");?></li>
<li><?php i18n("Warn user before copy/move operation if available space is not enough (bug 243160)");?></li>
<li><?php i18n("Move SMB KCM to Network Settings category");?></li>
<li><?php i18n("trash: Fix directorysizes cache parsing");?></li>
<li><?php i18n("kioexecd: watch for creations or modifications of the temporary files (bug 397742)");?></li>
<li><?php i18n("Don't draw frames and shadows around images with transparency (bug 258514)");?></li>
<li><?php i18n("Fixed file type icon in file properties dialog rendered blurry on high dpi screens");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("properly open the drawer when dragged by handle");?></li>
<li><?php i18n("extra margin when the pagerow globaltoolbar is ToolBar");?></li>
<li><?php i18n("support also Layout.preferredWidth for sheet size");?></li>
<li><?php i18n("get rid of last controls1 remains");?></li>
<li><?php i18n("Allow creation of separator Actions");?></li>
<li><?php i18n("consent an arbitrary # of columns in CardsGridview");?></li>
<li><?php i18n("Don't actively destroy menu items (bug 397863)");?></li>
<li><?php i18n("icons in actionButton are monochrome");?></li>
<li><?php i18n("don't make icons monochrome when they shouldn't");?></li>
<li><?php i18n("restore the arbitrary *1.5 sizing of icons on mobile");?></li>
<li><?php i18n("delegate recycler: Do not request the context object twice");?></li>
<li><?php i18n("use the internal material ripple implementation");?></li>
<li><?php i18n("control header width by sourcesize if horizontal");?></li>
<li><?php i18n("expose all properties of BannerImage in Cards");?></li>
<li><?php i18n("use DesktopIcon even on plasma");?></li>
<li><?php i18n("correctly load file:// paths");?></li>
<li><?php i18n("Revert \"Start looking for the context from the delegate itself\"");?></li>
<li><?php i18n("Add test case that outlines scoping issue in DelegateRecycler");?></li>
<li><?php i18n("explicitly set an height for overlayDrawers (bug 398163)");?></li>
<li><?php i18n("Start looking for the context from the delegate itself");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("Use reference in for loop for type with non-trivial copy constructor");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Add support for Attica tags support (bug 398412)");?></li>
<li><?php i18n("[KMoreTools] give the \"Configure...\" menu item an appropriate icon (bug 398390)");?></li>
<li><?php i18n("[KMoreTools] Reduce menu hierarchy");?></li>
<li><?php i18n("Fix 'Impossible to use knsrc file for uploads from non standard location' (bug 397958)");?></li>
<li><?php i18n("Make test tools link on Windows");?></li>
<li><?php i18n("Unbreak build with Qt 5.9");?></li>
<li><?php i18n("Add support for Attica tags support");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Fixed a crash caused by bad lifetime management of canberra-based audio notification (bug 398695)");?></li>
</ul>

<h3><?php i18n("KNotifyConfig");?></h3>

<ul>
<li><?php i18n("Fix UI file hint: KUrlRequester now has QWidget as base class");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("Use reference in for loop for type with non-trivial copy constructor");?></li>
<li><?php i18n("Move Qt5::DBus to the 'PRIVATE' link targets");?></li>
<li><?php i18n("Emit signals when a package is installed/uninstalled");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("Fix signals not being emitted when merging two persons");?></li>
<li><?php i18n("Don't crash if person gets removed");?></li>
<li><?php i18n("Define PersonActionsPrivate as class, as declared before");?></li>
<li><?php i18n("Make PersonPluginManager API public");?></li>
</ul>

<h3><?php i18n("Kross");?></h3>

<ul>
<li><?php i18n("core: handle better comments for actions");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Paint code folding marker only for multiline code folding regions");?></li>
<li><?php i18n("Intialize m_lastPosition");?></li>
<li><?php i18n("Scripting: isCode() returns false for dsAlert text (bug 398393)");?></li>
<li><?php i18n("use R Script hl for R indent tests");?></li>
<li><?php i18n("Update of the R indent script");?></li>
<li><?php i18n("Fix Solarized Light and Dark color schemes (bug 382075)");?></li>
<li><?php i18n("Don't require Qt5::XmlPatterns");?></li>
</ul>

<h3><?php i18n("KTextWidgets");?></h3>

<ul>
<li><?php i18n("ktextedit: lazy load the QTextToSpeech object");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("Log wallet open failure errors");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Don't silently error if damage is sent before buffer (bug 397834)");?></li>
<li><?php i18n("[server] Do not return early on fail in touchDown fall back code");?></li>
<li><?php i18n("[server] Fix remote access buffer handling when output not bound");?></li>
<li><?php i18n("[server] Do not try to create data offers without source");?></li>
<li><?php i18n("[server] Abort drag start on correct conditions and without posting error");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("[KCollapsibleGroupBox] Respect style's widget animation duration (bug 397103)");?></li>
<li><?php i18n("Remove obsolete Qt version check");?></li>
<li><?php i18n("Compile");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Use _NET_WM_WINDOW_TYPE_COMBO instead of _NET_WM_WINDOW_TYPE_COMBOBOX");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Fix OCS provider URL in about dialog");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Use matching enum value AuthEapMethodUnknown to compare a AuthEapMethod");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Bump theme version strings because there are new icons in 5.51");?></li>
<li><?php i18n("Also raise configuration window when reusing it");?></li>
<li><?php i18n("Add missing component: RoundButton");?></li>
<li><?php i18n("Combine display OSD icon files and move to plasma icon theme (bug 395714)");?></li>
<li><?php i18n("[Plasma Components 3 Slider] Fix implicit size of handle");?></li>
<li><?php i18n("[Plasma Components 3 ComboBox] Switch entries with mouse wheel");?></li>
<li><?php i18n("Support button icons when present");?></li>
<li><?php i18n("Fixed week names not showing properly in calendar when week starts with a day other than Monday or Sunday (bug 390330)");?></li>
<li><?php i18n("[DialogShadows] Use 0 offset for disabled borders on Wayland");?></li>
</ul>

<h3><?php i18n("Prison");?></h3>

<ul>
<li><?php i18n("Fix rendering Aztec codes with an aspect ratio != 1");?></li>
<li><?php i18n("Remove assumption about the barcode aspect ratio from the QML integration");?></li>
<li><?php i18n("Fix rendering glitches caused by rounding errors in Code 128");?></li>
<li><?php i18n("Add support for Code 128 barcodes");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("Make cmake 3.0 the minimum cmake version");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Small default padding when there is a background");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Don't show an emblem for mounted disks, only unmounted disks");?></li>
<li><?php i18n("[Fstab] Remove AIX support");?></li>
<li><?php i18n("[Fstab] Remove Tru64 (__osf__) support");?></li>
<li><?php i18n("[Fstab] Show non-empty share name in case root fs is exported (bug 395562)");?></li>
<li><?php i18n("Prefer provided drive label for loop devices as well");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Fix breakage of language guessing");?></li>
<li><?php i18n("Prevent highlighter from erasing selected text (bug 398661)");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("i18n: fix extraction of theme names");?></li>
<li><?php i18n("Fortran: Highlight alerts in comments (bug 349014)");?></li>
<li><?php i18n("avoid that the main context can be #poped");?></li>
<li><?php i18n("Endless state transition guard");?></li>
<li><?php i18n("YAML: add literal &amp; folded block styles (bug 398314)");?></li>
<li><?php i18n("Logcat &amp; SELinux: improvements for the new Solarized schemes");?></li>
<li><?php i18n("AppArmor: fix crashes in open rules (in KF5.50) and improvements for the new Solarized schemes");?></li>
<li><?php i18n("Merge git://anongit.kde.org/syntax-highlighting");?></li>
<li><?php i18n("Update git ignore stuff");?></li>
<li><?php i18n("Use reference in for loop for type with non-trivial copy constructor");?></li>
<li><?php i18n("Fix: Email highlighting for unclosed parenthesis in Subject header (bug 398717)");?></li>
<li><?php i18n("Perl: fix brackets, variables, string references and others (bug 391577)");?></li>
<li><?php i18n("Bash: fix parameter &amp; brace expansion (bug 387915)");?></li>
<li><?php i18n("Add Solarized Light and Dark themes");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.51");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.8");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
