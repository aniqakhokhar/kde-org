<?php
  $page_title = "KDE 3.0.2 to 3.0.3 Changelog";
  $site_root = "../../";
  include "header.inc";
?>

<p>
This page tries to present as much as possible of the problem
corrections that occurred in KDE between the 3.0.2 and 3.0.3 releases.
</p>
<p>
Please see the <a href="../changelog3_0_1to3_0_2.html">3.0.1 to 3.0.2 changelog</a> for further information.
</p>

<h3>arts</h3>
<ul>
</ul>

<h3>kdelibs</h3>
<ul>
<li>kprocess / kprocctrl: fix problem with hanging kdevelop.</li>
<li>Javascript (core): more robust stack overflow check</li>
<li>Javascript (html): allow to set a JS (non-CSS) property to a style object</li>
<li>Javascript (html): improved result of navigator.platform using uname()</li>
<li>Javascript (html): more aggressive garbage collection on clear, fixing many crashes</li>
<li>Javascript (html): global object (window) has a correct object prototype now</li>
<li>khtml: several crashes and bugs fixed</li>
<li>kssl: Always verify the basic constraints on certificates (SECURITY) 
<li>kdefx: Make the blending routine used by transparent menus work properly on big-endian platforms</li>
<li>kdeui: Fix KDialog::setPlainCaption()</li>
</ul>

<h3>kdeaddons</h3>
<!-- <ul>
</ul> -->

<h3>kdeadmin</h3>
<ul>
</ul>

<h3>kdeartwork</h3>
<ul>
</ul>

<h3>kdebase</h3>
<ul>
<li>Build fixes for IRIX / MipsPro (kcontrol, konqueror, konsole).</li>
<li>Kicker: Quick browser escapes ampersand characters in file names.</li>
<li>Konsole: Don't prepend ESC if Meta is pressed if key definition is for "+Alt".</li>
<li>Konsole: Fixed crashes at startup related to broken font installations.</li>
<li>Konsole: Fixed crashes when selecting in history buffer.</li>
<li>krdb: Smarter handling of multiple and symlinked KDE and Qt installs when exporting plugin path to Qt.</li>
</ul>

<h3>kdebindings</h3>
<ul>
</ul>

<h3>kdegames</h3>
<!-- <ul>
</ul> -->

<h3>kdegraphics</h3>
<ul>
</ul>

<h3>kdemultimedia</h3>
<ul>
</ul>

<h3>kdenetwork</h3>
<ul>
<li>KMail: allow an ampersand as last char of an URL.</li>
<li>KMail: Don't crash when custom drafts or sent-mail folders have been deleted or renamed.</li>
<li>KMail: Fix auto charset detection for empty strings.</li>
<li>KMail/KNode: Correctly handle quotes in user ids of OpenPGP keys.</li>
<li>KMail: Make custom (global) Bcc headers work correctly with encryption and distribution list expansion.</li>
</ul>

<h3>kdepim</h3>
<ul>
<li>KAlarm: Fix session restoration often not occurring at login.</li>
<li>KAlarm: Adjust wrong summer times stored by KDE 3.0.0 version of KAlarm.</li>
<li>KAlarm: Make Close button on message window not the default button to reduce chance of accidental acknowledgement.</li>
<li>KAlarm: Make Help button in configuration dialog display KAlarm handbook.</li>
<li>KNotes: Compile fix for Qt 3.0.5</li>
<li>KOrganizer: Fixed context menu of todo list/view and event list which disappeared when using Qt 3.0.5</li>
</ul>

<h3>kdesdk</h3>
<ul>
</ul>

<h3>kdetoys</h3>
<ul>
</ul>

<h3>kdeutils</h3>
<ul>
</ul>

<h3>KDevelop (version 2.1.3)</h3>
<ul>
<li>considers also new behaviour of Qt version 3.0.5. (proper text formatting in output view)</li>
<li>some memory leaks fixed</li>
<li>TabPage user interface mode got a close button (stable backport from HEAD)</li>
<li>again improved editor indention</li>
<li>compile fixes for FreeBSD</li>
<li>some Debian-specific updates</li>
<li>FYI: 'Add files to project' doesn't hang anymore because of a fix in the latest kdelibs:kdecore library</li>
</ul>


<?php include "footer.inc" ?>
