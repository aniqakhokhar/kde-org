<?php
  $page_title = "KDE 3.2.2 to 3.2.3 Changelog";
  $site_root = "../../";
  include "header.inc";
?>

<p>
This page tries to present as much as possible of the problem
corrections that occurred in KDE between the 3.2.2 and 3.2.3 releases.
Nevertheless it should be considered as incomplete.
</p>

<a name="arts"><h3>arts</h3>
<ul>
</ul>

<a name="kdelibs"><h3>kdelibs</h3>
<ul>
<li>general: More and better usage of icons in different widgets</li>
<li>kiosk: Support for
<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdelibs/kdecore/README.user_profiles?rev=1.2.2.1&content-type=text/x-cvsweb-markup&only_with_tag=KDE_3_2_BRANCH">
user profiles</a></li>
<li>kfile: directory chooser dialog now hides hidden directories, added context option to show</li>
<li>uiserver: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=75244">KIO: Progress Dialog - 'Keep open' + 'Open File' - Problems</a></li>
<li>kwallet: Keep KWallet's password dialog above other windows</li>
</ul>

<a name="kdeaccessibility"><h3>kdeaccessibility</h3>
<ul>
</ul>

<a name="kdeaddons"><h3>kdeaddons</h3>
<ul>
</ul>

<a name="kdeadmin"><h3>kdeadmin</h3>
<ul>
</ul>

<a name="kdeartwork"><h3>kdeartwork</h3>
<ul>
<li>Euphoria screensaver: Fixed "white bg" bug, some mem leaks, more precise timing</li>
<li>Plastik: Ported changes from the development branch back to KDE 3.2.x. They include various bug fixes and small visible changes.</li>
<li>Plastik: Adopt horizontal menu geometries from Keramik [<a href="http://bugs.kde.org/show_bug.cgi?id=79674">#79674</a>]</li>
<li>Plastik: Use the correct background color for translucent menus [<a href="http://bugs.kde.org/show_bug.cgi?id=81249">#81249</a>]</li>
</ul>

<a name="kdebase"><h3>kdebase</h3>
<ul>
<li>general: More and better usage of icons in different applications</li>
<li>general: Fix link order for IRIX [<a href="http://bugs.kde.org/show_bug.cgi?id=78228">#78228</a>], [<a href="http://bugs.kde.org/show_bug.cgi?id=76174">#76174</a>]</li>
<li>kappfinder: Fixed menu structure and added icons of several applications</li>
<li>kicker: Don't show unimplemented "About" and "Preferences" for universal sidebar</li>
<li>kicker: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=72186">two panels on different xinerama screens affecting their placement</a>.</li>
<li>konqueror/kdesktop: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=43172">konqueror is ignoring umask when creating new files</a>
<li>konqueror/kdesktop: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=80880">filename suggestion when moving files applies to all subsequent files</a>
<li>konqueror: Made all items in the "Go" menu open in the current Konqueror window</li>
<li>konqueror: Now Ctrl+Enter opens a tab with default background setting</li>
<li>konqueror: Holding Shift while opening a tab inverses the "open tabs in background" setting</li>
<li>konqueror: "[FMSettings] TabPosition=Bottom" in konquerorrc lets the tabbar appear at bottom</li>
<li>konqueror: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=68975">konqueror listview rightclick always selects file</a></li>
<li>konsole: Added the ability to fetch the shell PID per session via DCOP</li>
<li>konsole: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=78506">terminal size display is covered by kwin size display</a></li>
<li>konsole: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=73750">Default rendition should be used when clearing screen areas</a></li>
<li>konsole: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=80326">can't change/save Keytabs in the Session Editor</a></li>
<li>konsole: Fix for crashes on exit.</li>
<li>konsole: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=12215">transparant background updates</a></li>
<li>kio_fish: Fixes for copying into a symlink to a folder, and for copying over a directory that already exists</li>
<li>kwin: Keep properly splashscreens above their mainwindows.</li>
<li>kwin: Restore focus when a broken application sets it to nowhere.</li>
<li>kwin: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=77341">dialog placement problems with Konqueror</a> with Qt-3.2.x .</li>
<li>kwin: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=66868">problem with minimizing xmms</a>.</li>
<li>kwin: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=79252">active desktop borders while moving improper initialization</a>.</li>
<li>kwin: Fix Shift+middle mouse button action in Konqueror.</li>
<li>klipper: Fix Klipper reacting to its own clipboard changes.</li>
<li>khotkeys: Mouse gestures are globally disabled by default.</li>
<li>drkonqi: Fix valid backtraces sometimes being reported as useless.</li>
<li>kcontrol: Fix colorsheme file name conflict [<a href="http://bugs.kde.org/show_bug.cgi?id=74687">#74687</a>]</li>
</ul>

<a name="kdebinginds"><h3>kdebindings</h3>
<ul>
</ul>

<a name="kdeedu"><h3>kdeedu</h3>
<ul>
<li>kalzium: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=76417">#76417:  i18n problems in kalzium</a></li>
<li>kig: Save window settings on exit</li>
<li>kbruch: Make sure task settings could not be invalid</li>
</ul>


<a name="kdegames"><h3>kdegames</h3>
<ul>
<li>atlantik: avoid crash when unresolvable hosts are in the meta server list [<a href="http://bugs.kde.org/show_bug.cgi?id=80225">#80225</a>]</li>
<li>atlantik: fix crash when network core is reset while readbuffer isn't empty</li>
<li>atlantik: chat view can be cleared [<a href="http://bugs.kde.org/show_bug.cgi?id=69044">#69044</a>]</li>
</ul>

<a name="kdegraphics"><h3>kdegraphics</h3>
<ul>
<li>kruler: When move rule window, use "cross arrows" cursor</li>
<li>kfile-plugins/jpeg: Compile fix for IRIX / MipsPro [<a href="http://bugs.kde.org/show_bug.cgi?id=78244">#78244</a>]</li>
</ul>

<a name="kdemultimedia"><h3>kdemultimedia</h3>
<ul>
</ul>

<a name="kdenetwork"><h3>kdenetwork</h3>
<ul>
<li>kppp: Fix <a href="http://bugs.kde.org/73646">flow control</a> for non-English users</li>
<li>kppp: Added /dev/ttyS4 device. <a href="http://bugs.kde.org/82345">Custom devices</a> still cannot be set unfortunately.</li>
<li>kget: Fix filesize display for files &gt; 2GB</li>
<li>kopete: Fix <a href="http://bugs.kde.org/79468">bug 79468</a>: Don't add temporary Yahoo! contacts to the serverside list.</li>
<li>kopete: Fix <a href="http://bugs.kde.org/81143">bug 81143</a>: Kopete doesn't save settings when exiting KDE</li>
<li>kopete: Fix <a href="http://bugs.kde.org/81809">bug 81809</a>: Close button doesn't close application when the system tray is disabled</li>
<li>kopete: Fix <a href="http://bugs.kde.org/81823">bug 81823</a>: Ignoring incoming IRC conversations crashes Kopete</li>
<li>kopete: Fix <a href="http://bugs.kde.org/77100">bug 77100</a>: Avoid using libpng if no image is present</li>
<li>kopete: Fix <a href="http://bugs.kde.org/80158">bug 80158</a>: When I type more than one space in a row, Kopete sends the characters "&amp;nbsp"</li>
<li>kopete: Fix <a href="http://bugs.kde.org/79981">bug 79981</a>: Yahoo won't connect with correct password on AMD64</li>
</ul>

<a name="kdepim"><h3>kdepim</h3></a>
<ul>
<li>KMail:
<ul>
<li>More icons on buttons</li>
<li>Make sure that child processes don't inherit kmail's file descriptors</li>
<li>Fix wrong initialization of View->Unread Count</li>
<li>Warn the user if they try to send a message without a sender address. This fixes several related bugs, e.g. <a href="http://bugs.kde.org/53448">#53448</a>, <a href="http://bugs.kde.org/78895">#78895</a>, <a href="http://bugs.kde.org/80817">#80817</a>, <a href="http://bugs.kde.org/81611">#81611</a>.</li>
</ul>
</li>
<li>KNotes:
<ul>
<li>Shortcuts work again</li>
<li>Fixed being able to uncheck all text format buttons in RT mode</li>
<li>Fixed possibility of some global settings not being saved</li>
</ul>
</li>
<li>KNode: Fix coloring of quoted text (<a href="http://bugs.kde.org/75333">#75333</a>)</li>
<li>KNode: Prevent multiple configure shortcuts actions when running in Kontact (<a href="http://bugs.kde.org/79000">#79000</a>)</li>
</ul>

<a name="kdesdk"><h3>kdesdk</h3>
<ul>
<li>kuiviewer: Fixed loading of menu layout definition</li>
<li>cervisia: Fixed several bugs in resolve dialog [<a href="http://bugs.kde.org/show_bug.cgi?id=46871">#46871</a>,&nbsp;
<a href="http://bugs.kde.org/show_bug.cgi?id=74903">#74903</a>]</li>
<li>cervisia: Don't ignore the CVS_RSH setting during checkout [<a href="http://bugs.kde.org/show_bug.cgi?id=80291">#80291</a>]</li>
<li>cervisia: Handle spaces in the working folder name correctly [<a href="http://bugs.kde.org/show_bug.cgi?id=80291">#81498</a>]</li>
<li>cervisia: Honor option "Hide Non-CVS Files" when opening a branch in the file tree 
[<a href="http://bugs.kde.org/show_bug.cgi?id=58254">#58254</a>]</li>
</ul>

<a name="kdetoys"><h3>kdetoys</h3>
<ul>
</ul>

<a name="kdeutils"><h3>kdeutils</h3>
<ul>
<li>kdf: Fix icon size of the Kicker applet [<a href="http://bugs.kde.org/show_bug.cgi?id=79385">#79385</a>]</li>
<li>klaptopdaemon: Fix sysfs parsing for CPUFreq</li>
<li>klaptopdaemon: Better handling of invalid remaining time values of ACPI batteries</li>
<li>klaptopdaemon: Toshiba LCD fix (<a href="http://bugs.kde.org/show_bug.cgi?id=69154">#69154</a>)</li>
</ul>

<a name="quanta"><h3>quanta</h3>
<ul>
<li><strong>VPL</strong>: (once again) don't lose the comment text from inside a comment [<a href="http://bugs.kde.org/show_bug.cgi?id=81162">#81162</a>]</li>
<li>don't crash when source/VPL buttons are pressed while a plugin is visible [<a href="http://bugs.kde.org/show_bug.cgi?id=76616">#76616</a>]</li>
<li>don't crash when canceling a failed upload</li>
<li>don't crash when saving files while the structure tree is visible [<a href="http://bugs.kde.org/show_bug.cgi?id=79803">#79803</a>]</li>
<li>don't crash when inserting a dot in an empty CSS file [<a href="http://bugs.kde.org/show_bug.cgi?id=82143">#82143</a>]</li>
<li>fix a major memory leak, cause of many crashes and instability</li>
<li>don't be confused by quotation marks inside a script area which is inside a tag value (like <em>&#060;a href="&#060;? echo "foo" ?>"></em>) [<a href="http://bugs.kde.org/show_bug.cgi?id=80683">#80683</a>]</li>
<li>fix search problems when Preview is set to appear in the bottom area [<a href="http://bugs.kde.org/show_bug.cgi?id=76832">#76832</a>]</li>
<li>fix restoration of the toolbar visibility setting [<a href="http://bugs.kde.org/show_bug.cgi?id=79082">#79082</a>]</li>
<li>hide/show the plugin toolbar as well when Show Toolbar is unchecked/checked [<a href="http://bugs.kde.org/show_bug.cgi?id=79082">#79082</a>]</li>
<li>fix the layout of the Document Properties Dialog</li>
<li>don't ask for copying a file to the project if it is under the project directory (happened with symlinked files and projects)</li>
<li>color with bold in the Files Tree the opened files, even if they are under a symlinked directory</li>
<li>don't save the shortcuts as localized strings [<a href="http://bugs.kde.org/show_bug.cgi?id=80115">#80115</a>]</li>
<li>remove the backup files in every case when the document was saved and don't warn about their presence on the next startup</li>
<li><em>&#060;fieldset></em> is not a single tag [<a href="http://bugs.kde.org/show_bug.cgi?id=79926">#79926</a>]</li>
<li>unconditionally hide the splash screen after 10 seconds [<a href="http://bugs.kde.org/show_bug.cgi?id=80086">#80086</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=82013">#82013</a>]</li>
<li>make uploading of empty folders possible [<a href="http://bugs.kde.org/show_bug.cgi?id=82127">#82127</a>]</li>
<li>don't show the file changed dialog when previewing after Save All or Project Upload was called </li>
<li>fix removal of action containing &#038;</li>
<li>fix toolbar configuration and removal for non-English versions</li>
<li><strong>improvement</strong>: allow two plugins named differently, but using of the same part</li>
<li><strong>performance improvement</strong>: faster parsing and tree building</li>
</ul>

<a name="kdevelop"><h3>kdevelop</h3>
<ul>
<li>satisfy new haskel compilers and do not return random data</li>
<li>GUI translations: Hindi and Slovenian</li>
<li>don't crash when user attempts "add method" without selecting a class [<a href="http://bugs.kde.org/show_bug.cgi?id=81579">#81579</a>]</li>
<li>make the importer acknowledge more header suffixes than .h [<a href="http://bugs.kde.org/show_bug.cgi?id=71099">#71099</a>]</li>
<li>don't crash when there is no preferred handler for application/x-designer [<a href="http://bugs.kde.org/show_bug.cgi?id=79794">#79794</a>]</li>
</ul>

<?php include "footer.inc" ?>
