<?php
  $page_title = "KDE 3.0.4 to 3.0.5 Changelog";
  $site_root = "../../";
  include "header.inc";
?>

<p>
This page tries to present as much as possible of the problem
corrections that occurred in KDE between the 3.0.4 and 3.0.5 releases.
</p>
<p>
Please see the <a href="changelog3_0_3to3_0_4.html">3.0.3 to 3.0.4 changelog</a> for further information.
</p>

<h3>arts</h3>
<ul>
</ul>

<h3>kdelibs</h3>
<ul>
        <li>KHTML : Fixed several crashes and misrenderings.</li>
        <li><a href="http://www.kde.org/info/security/advisory-20021111-2.txt">rlogin.protocol</a>:
            fixed command execution in specially crafted urls</li>
</ul>

<h3>kdeaddons</h3>
<ul>
</ul>

<h3>kdeadmin</h3>
<ul>
</ul>

<h3>kdeartwork</h3>
<ul>
</ul>

<h3>kdebase</h3>
<ul>
	<li>KAddressbook: Don't squeeze columns on startup/properly restore column width settings</li>
</ul>

<h3>kdebindings</h3>
<ul>
</ul>

<h3>kdegames</h3>
<ul>
</ul>

<h3>kdegraphics</h3>
<ul>
</ul>

<h3>kdemultimedia</h3>
<ul>
</ul>

<h3>kdenetwork</h3>
<ul>
<li>lisa, reslisa: Fixed several <a href="http://www.kde.org/info/security/advisory-20021111-2.txt">security vulnerabilities</a></li>
</ul>

<h3>kdepim</h3>
<ul>
</ul>

<h3>kdesdk</h3>
<ul>
	<li>Cervisia: Show long lines in diff view always completely.</li>
</ul>

<h3>kdetoys</h3>
<ul>
</ul>

<h3>kdeutils</h3>
<ul>
</ul>

<h3>KDevelop (2.1.4)</h3>
<ul>
<li>bugfix in output tool-view for Red Hat Linux 8.0: jump-to-error feature works again</li>
<li>another bugfix in output tool-view for SuSE 8.1 (German translation): jump-to-error feature works again</li>
</ul>


<?php include "footer.inc" ?>
