<?php
  $page_title = "KDE 3.0.3 to 3.0.4 Changelog";
  $site_root = "../../";
  include "header.inc";
?>

<p>
This page tries to present as much as possible of the problem
corrections that occurred in KDE between the 3.0.3 and 3.0.4 releases.
</p>
<p>
Please see the <a href="changelog3_0_2to3_0_3.html">3.0.2 to 3.0.3 changelog</a> for further information.
</p>

<h3>arts</h3>
<ul>
	<li>Compile fix for IRIX64</li>
</ul>

<h3>kdelibs</h3>
<ul>
	<li>KHTML : Fixed progress bar.</li>
        <li>KHTML : Fixed several crashes and misrenderings.</li>
	<li>KHTML : Fixed "HTML source displayed in text viewer" after viewing text.</li>
	<li>KHTML : Only ask "download plugins?" once per requested mimetype in the same page.</li>
	<li>Javascript : several fixes (row.cells(), tablecaption.*, heading.*, event.x/y) .</li>
	<li>libkscreensaver : Fixed bug that in rare cases prevented the screensaver from showing anything.</li>
	<li>kio_file : Proper "disk full" error message in mkdir</li>
	<li>KIO : Dropping links to webpages onto the desktop works in all cases now</li>
	<li>Drag-and-drop of URLs : Improved compatibility with non-KDE apps</li>
	<li>KToolBar : fix for oversized comboboxes in toolbars, e.g. in KOffice</li>
</ul>

<h3>kdeaddons</h3>
<!-- <ul>
</ul> -->

<h3>kdeadmin</h3>
<ul>
</ul>

<h3>kdeartwork</h3>
<ul>
</ul>

<h3>kdebase</h3>
<ul>
	<li>NSPlugins : Compatibility with gcc-3.x</li>
	<li>Konqueror configuration : fixed default font size value</li>
	<li>KDesktop : better determination of icon heights, for "lineup Icons"</li>
	<li>KDesktop : Don't rearrange all icons when changing the font size</li>
	<li>KControl : Fixed proxy configuration dialog layout</li>
	<li>Fix a lot of mem leak</li>
	<li>Kaddressbook : fix crash when we export CSV list</li>
	<li>KDM<ul>
		<li>Security: proper temp dir creation for Qt and disabled crash dialog</li>
		<li>Made chooser work at all</li>
		<li>Fixed compilation with older glibc</li>
		<li>Fixed bad lilo interaction wrt. warnings</li>
	    </ul></li>
</ul>

<h3>kdebindings</h3>
<ul>
</ul>

<h3>kdegames</h3>
<!-- <ul>
</ul> -->

<h3>kdegraphics</h3>
<ul>
	<li>Kamera: Fixed crash when we didn't select camera in kcmkamera</li>
             <li>KGhostview: <a href="http://www.kde.org/info/security/advisory-20021008-1.txt">Security related fixes</a></li>
</ul>

<h3>kdemultimedia</h3>
<ul>
	<li>KMix : Fixed compilation on Solaris</li>
	<li>Kaboodle : fixed when it was embedded in konqueror</li>
	<li>Fix a lot of memory leak</li>
</ul>

<h3>kdenetwork</h3>
<ul>
<li>kpf: <a href="http://www.kde.org/info/security/advisory-20021008-2.txt">Security related fixes</a></li>
</ul>

<h3>kdepim</h3>
<ul>
	<li>KAlarm: Fix right-to-left character sets not being displayed in message edit control</li>
	<li>KAlarm: Make "Help -> Report Bug" use the KDE bug system (bug #43250)</li>
  <li>KNotes: Fixed session management (bug #22844)</li>
  <li>KNotes: Fixed mouse behaviour to be like kwin's (raise and lower already on mouse press)</li>
  <li>KNotes: Fixed (in)famous bug that caused the big black windows and lost data (bugs #22062,
              #44870, #45084, #45386, #45451, #46570, #47352)</li>
  <li>KNotes: Fixed drag'n drop of links and plain text</li>
</ul>

<h3>kdesdk</h3>
<ul>
	<li>Disabled kbugbuster compilation because bug system changed. Use CVS version if you need it.</li>
</ul>

<h3>kdetoys</h3>
<ul>
</ul>

<h3>kdeutils</h3>
<ul>
</ul>


<?php include "footer.inc" ?>
