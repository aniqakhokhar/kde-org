------------------------------------------------------------------------
r933011 | gallinari | 2009-02-27 19:56:35 +0000 (Fri, 27 Feb 2009) | 1 line

Temp fix for the bugs reported with the latest Qt release candidate
------------------------------------------------------------------------
r935440 | scripty | 2009-03-05 07:49:33 +0000 (Thu, 05 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r937023 | lueck | 2009-03-08 21:46:31 +0000 (Sun, 08 Mar 2009) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r938311 | reed | 2009-03-11 15:51:45 +0000 (Wed, 11 Mar 2009) | 1 line

fix for bug #184400, kggz build issue on 10.5
------------------------------------------------------------------------
r939754 | toma | 2009-03-15 16:38:49 +0000 (Sun, 15 Mar 2009) | 2 lines

Backport docbook fix

------------------------------------------------------------------------
r939755 | toma | 2009-03-15 16:39:36 +0000 (Sun, 15 Mar 2009) | 2 lines

Backport docbook fix

------------------------------------------------------------------------
r939860 | aacid | 2009-03-15 21:17:11 +0000 (Sun, 15 Mar 2009) | 5 lines

backport r939857 | aacid | 2009-03-15 22:16:00 +0100 (Sun, 15 Mar 2009) | 2 lines

Fix usage of KScoreDialog


------------------------------------------------------------------------
r942095 | piacentini | 2009-03-21 02:48:23 +0000 (Sat, 21 Mar 2009) | 2 lines

Backporting fix for bug 184592

------------------------------------------------------------------------
r942463 | mfuchs | 2009-03-21 19:50:33 +0000 (Sat, 21 Mar 2009) | 5 lines

Redeal button disabled when no redeals are possible anymore. Also "Hint" and "Demo" are disabled when the game is lost.

BUG:185439


------------------------------------------------------------------------
r943008 | scripty | 2009-03-23 07:45:26 +0000 (Mon, 23 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r943675 | scripty | 2009-03-24 08:19:47 +0000 (Tue, 24 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r943986 | aacid | 2009-03-24 19:29:35 +0000 (Tue, 24 Mar 2009) | 4 lines

Fix knetwalk not working when in rtl mode
Patch by alsadi@ojuba.org
BUGS: 187870

------------------------------------------------------------------------
r944171 | scripty | 2009-03-25 08:46:03 +0000 (Wed, 25 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
