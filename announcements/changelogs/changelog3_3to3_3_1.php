<?php
  $page_title = "KDE 3.3 to KDE 3.3.1 Changelog";
  $site_root = "../../";
  include "header.inc";
?>

<p>
This page tries to present as much as possible the additions
and corrections that occurred in KDE between the 3.3 and 3.3.1 releases.
Nevertheless it should be considered as incomplete.
</p>

<a name="arts"><h3>arts</h3>
<ul>
</ul>

<a name="kdelibs"><h3>kdelibs</h3>
<ul>
<li>KHTML: fixed spurious deletion of textarea contents
(<a href="http://bugs.kde.org/show_bug.cgi?id=49828">#49828</a>,
<a href="http://bugs.kde.org/show_bug.cgi?id=48879">#48879</a>,
<a href="http://bugs.kde.org/show_bug.cgi?id=86916">#86916</a>)</li>
<li>KHTML: fixed innerHTML getter to not behave like outerHTML
(<a href="http://bugs.kde.org/show_bug.cgi?id=88242">#88242</a>)
</li>
<li>KHTML: fix crash in caret mode</li>
<li>KHTML: added missing &lt;marquee&gt; ECMAScript bindings</li>
<li>KHTML: fixed missing Javascript-support for HTMLDocument.compatMode</li>
<li>KHTML: fixed off-by-one error which caused decorations to exceed the
selection-end by one pixel</li>
<li>KHTML: fixed wrong positioning of text-decorations in selections</li>
<li>KHTML: small paint optimization for render texts</li>
<li>KHTML: fixed spurious scrollbars on the canvas when overflow of clipped children exceeds the viewport boundaries
(<a href="http://bugs.kde.org/show_bug.cgi?id=57080">#57080</a>)</li>
<li>KHTML: fixed unreachable relatively positioned content on viewport</li>
<li>KHTML: fixed unreachable positioned content in clipped blocks</li>
<li>KHTML: fixed selection not being redrawn in overflow-area</li>
<li>KHTML: jump-to-anchor fixes
(<a href="http://bugs.kde.org/show_bug.cgi?id=57360">#57360</a>)</li>
<!-- KHTML-Template
<li>KHTML:
(<a href="http://bugs.kde.org/show_bug.cgi?id=">#</a>)
</li>
-->
<li>KHTML: Don't warn that a form was not submitted after pressing back button
(<a href="http://bugs.kde.org/show_bug.cgi?id=88073">#88073</a>)</li>
<li>KHTML: fix crash on illegal CSS (<a href="http://bugs.kde.org/show_bug.cgi?id=86192">#86192</a>)</li>
<li>KHTML: On popular demand, double-click between two words selects both again
as in KDE 3.2.</li>
<li>kfile: Added "New Folder..." in context menu within KDirSelectDialog (<a href="http://bugs.kde.org/show_bug.cgi?id=74614">#74614</a>)</li>
<li>kfile: Enabled dragging of directories within KDirSelectDialog</li>
<li>kfile: KURLRequester now popups KDirSelectDialog when a directory is requested (<a href="http://bugs.kde.org/show_bug.cgi?id=85074">#85074</a>)</li>
<li>kfile: Toggling hidden files didn't refresh the file selection list (<a href="http://bugs.kde.org/show_bug.cgi?id=80103">#80103</a>)</li>
<li>kio: Separate general from meta info in filetips (<a href="http://bugs.kde.org/show_bug.cgi?id=85252">#85252</a>)</li>
<li>don't crash when right clicking on a toolbar in some applications that are using KParts</li>
</ul>

<a name="kdeaccessibility"><h3>kdeaccessibility</h3>
<ul>
</ul>

<a name="kdeaddons"><h3>kdeaddons</h3>
<ul>
<li>sidebar newsticker: Better error handling in case no connection to the DCOP service is possible (<a href="http://bugs.kde.org/show_bug.cgi?id=87920">#87920</a>)</li>
<li>sidebar newsticker: Remove newsticker from the default sidebar configuraton (usability fix)</li>
<li>sidebar mediaplayer: Remove mediaplayer from the default sidebar configuration (usability fix)</li>
</ul>

<a name="kdeadmin"><h3>kdeadmin</h3>
<ul>
</ul>

<a name="kdeartwork"><h3>kdeartwork</h3>
<ul>
<li>Plastik: Performance optimizations</li>
<li>Plastik: Honor QButtonGroup::setLineWidth( 0 ) (<a href="http://bugs.kde.org/show_bug.cgi?id=89342">#89342</a>)</li>
<li>Plastik/dotNet: Fix flicker in scrollbar (<a href="http://bugs.kde.org/show_bug.cgi?id=76107">#76107</a>)</li>
</ul>

<a name="kdebase"><h3>kdebase</h3>
<ul>
<li>kinfocenter: fixed crash in USB viewer
(<a href="http://bugs.kde.org/show_bug.cgi?id=87807">#87807</a>)</li>
<li>konqueror: "Advanced Add Bookmark" didn't affect right-click bookmarking (<a href="http://bugs.kde.org/show_bug.cgi?id=86025">#86025</a>)</li>
<li>konqueror: "Duplicate Tab" now respects "After Current Tab" setting (<a href="http://bugs.kde.org/show_bug.cgi?id=89245">#89245</a>)</li>
<li>konqueror: Only let linked views in same tab follow changes (<a href="http://bugs.kde.org/show_bug.cgi?id=86748">#86748</a>)</li>
<li>konqueror: Automatically switch tab when a html page pops up a message box</li>
<li>konsole: Added AppScreen support to keytab (<a href="http://bugs.kde.org/show_bug.cgi?id=76976">#76976</a>)</li>
<li>konsole: Cycling trough tabs with keyboard caused terminal size to be printed (<a href="http://bugs.kde.org/show_bug.cgi?id=87274">#87274</a>)</li>
<li>konsole: Don't crash when showing tabbar with centered background (<a href="http://bugs.kde.org/show_bug.cgi?id=89629">#89629</a>)</li>
</ul>

<a name="kdebindings"><h3>kdebindings</h3>
<ul>
</ul>

<a name="kdeedu"><h3>kdeedu</h3>
<ul>
<li>kbruch: Fix color settings (apply them to all widgets)</li>
<li>kbruch: fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=87858">87858</a></li>
<li>khangman: fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=87853">87853</a> (level bug)</li>
<li>kmplot: Avoid an endless loop when a calculation doesn't return a value</li>
<li>kmplot: Fix areaUnderGraph-drawing: an invald y-value led to y&gt;10e10 or  y&lt;-10e10</li>
<li>kstars: bug <a href="http://bugs.kde.org/show_bug.cgi?id=89095">89095</a></li>
<li>kstars: LX200 GPS fixes</li>
<li>kstars: Fixes compilation on gcc-2.95</li>
<li>kstars: Fixes to the Solar Sysytem tab of the options dialog</li>
<li>kstars: Disable stars-related widgets in the options dialog (Catalogs tab) if the 
"Hipparcos Star Catalog" box is unchecked</li>
<li>kstars: Make it impossible to set the "zoomed out" mag limit to a fainter value 
than the "zoomed in" limit</li>
<li>kstars: Fix horribly blocky images in printed image</li>
<li>kstars: Printing fix: When original color scheme is restored, restore the 
original star color intensity as well</li>
<li>kstars: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=88100">88100</a></li>
<li>ktouch: Typos fixes in german2.ktouch</li>
<li>ktouch: utf8 fixes for bugs <a href="http://bugs.kde.org/show_bug.cgi?id=86080">86080</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=88704">88704</a></li>
<li>ktouch: Fix for bug <a href="http://bugs.kde.org/show_bug.cgi?id=86477">86477</a></li>
<li>kturtle: Fix crash in Settings</li>
<li>kturtle: Fix the endless loop bug that occurs when drawing extremely long lines</li>
<li>libkdeedu: 2 Fixes to ExtDateTime and the test program 'test_extdate'</li>
</ul>

<a name="kdegames"><h3>kdegames</h3>
<ul>
<li>atlantik: Don't show warning dialog when exiting a game that ended. (<a href="http://bugs.kde.org/show_bug.cgi?id=88617">#88617</a>)</li>
<li>kpoker: Fixed misevaluation of poker hands (<a href="http://bugs.kde.org/show_bug.cgi?id=30272">#30272</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=70903">#70903</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=71694">#71693</a>)</li>
<li>kpoker: Fixed unnecessary saves (<a href="http://bugs.kde.org/show_bug.cgi?id=88548">#88548</a>)</li>
</li>
<li>kreversi: Fix for bug where the color of the players are not saved (<a href="http://bugs.kde.org/show_bug.cgi?id=89829">#89829</a>)</li>
<li>kreversi: Fix for bug in undo handling after side switch (<a href="http://bugs.kde.org/show_bug.cgi?id=90190">#90190</a>)</li>
<li>kreversi: Fix against possible cheat (<a href="http://bugs.kde.org/show_bug.cgi?id=90195">#90195</a>)</li>
</ul>

<a name="kdegraphics"><h3>kdegraphics</h3>
<ul>
<li>kpdf: Use xpdf 3.0 rendering engine</li>
<li>kpdf: Fixed crash on pdf with 0 pages <a href="http://bugs.kde.org/show_bug.cgi?id=83590">#83590</a></li>
<li>kpdf: Fixed crash when using various kpdf parts <a href="http://bugs.kde.org/show_bug.cgi?id=81442">#81442</a></li>
<li>kpdf: Fixed crash when using zoom values with . on a locale with , as decimal separator <a href="http://bugs.kde.org/show_bug.cgi?id=82877">#82877</a></li>
<li>kpdf: Show "Show menubar" option on RMB menu when menubar is hidden <a href="http://bugs.kde.org/show_bug.cgi?id=89673">#89673</a></li>
<li>kpdf: Generate thumbnails in a thread so the GUI is not blocked <a href="http://bugs.kde.org/show_bug.cgi?id=88661">#88661</a></li>
<li>kolourpaint: Fix multiple selection bugs</li>
<li>kolourpaint: Speed up renderer (esp. drag-scrolling at high zoom)</li>
<li>kolourpaint: Fix remaining untranslatable strings <a href="http://bugs.kde.org/show_bug.cgi?id=85785">#85785</a></li>
</ul>

<a name="kdemultimedia"><h3>kdemultimedia</h3>
<ul>
<li>akodelib: Fixed decoding of Ogg/Vorbis on big-endian machines (<a href="http://bugs.kde.org/show_bug.cgi?id=88992">#88992</a>)</li>
<li>akode-artsplugin: Fixed artsd-crash when playing in Konqueror (<a href="http://bugs.kde.org/show_bug.cgi?id=89265">#89265</a>)</li>
<li>juk: Fixed a bug where JuK sometimes crashes on KDE logout. (<a href="http://bugs.kde.org/show_bug.cgi?id=87244">#87244</a>)</li>
<li>juk: Fixed the removeTrack() DCOP call, which caused an infinite memory-leaking loop. (<a href="http://bugs.kde.org/show_bug.cgi?id=89785">#89785</a>)</li>
<li>juk: JuK now automatically saves your playlists every 10 minutes. (<a href="http://bugs.kde.org/show_bug.cgi?id=67392">#67392</a>)</li>
<li>juk: The tree view mode uses a case-insensitive search to fill its dynamic playlist now, matching the internal method of operation. (<a href="http://bugs.kde.org/show_bug.cgi?id=89339">#89339</a>)</li>
<li>juk: The tag editor no longer loses changes when you hide the tag editor. (<a href="http://bugs.kde.org/show_bug.cgi?id=86892">#86892</a>)</li>
<li>juk: Added checks for the return value of opendir(), which could fail if you don't have read permission for the directory. (<a href="http://bugs.kde.org/show_bug.cgi?id=87491">#87491</a>)</li>
<li>juk: Update status bar after changing the tags on the currently playing item. (<a href="http://bugs.kde.org/show_bug.cgi?id=87332">#87332</a>)</li>
<li>juk: Fixed a bug where a tree view mode playlist could be deleted out from under an item that needed to access it.</li>
<li>juk: Fixed the GUI layout to also work in right-to-left languages.</li>
<li>juk: Update the status bar when a different playlist is selected.</li>
<li>juk: Always show the playing playlist in the status bar, fixing a regression from KDE 3.2.</li>
<li>kscd: Fix saving of cd-information (<a href="http://bugs.kde.org/show_bug.cgi?id=88165">#88165</a>)</li>
</ul>

<a name="kdenetwork"><h3>kdenetwork</h3>
<ul>
<li>ksirc: fixed duplicated channel into the list (<a href="http://bugs.kde.org/show_bug.cgi?id=80912">#80912</a>)</li>
<li>kopete: fix parse errors when URLs are sent from gaim via yahoo (<a href="http://bugs.kde.org/show_bug.cgi?id=87190">#87190</a>)</li>
<li>kopete: fix AIM and ICQ outgoing message encoding (<a href="http://bugs.kde.org/show_bug.cgi?id=89200">#89200</a>)</li>
<li>kopete: fix crash in ICQ rich text parsing (<a href="http://bugs.kde.org/show_bug.cgi?id=87390">#87390</a>)</li>
<li>kopete: don't show offline in the systray if we're invisible (<a href="http://bugs.kde.org/show_bug.cgi?id=86790">#86790</a>)</li>
<li>kopete: fix contact list not displayed on startup (<a href="http://bugs.kde.org/show_bug.cgi?id=87727">#87727</a>)</li>
<li>kopete: fix crash after font change (<a href="http://bugs.kde.org/show_bug.cgi?id=86860">#86860</a>)</li>
<li>kopete: change the default shortcut to Enter from Ctrl+Enter</li>
<li>kopete: fix the format toolbar not staying off after being turned off (<a href="http://bugs.kde.org/show_bug.cgi?id=59080">#59080</a>)</li>
<li>kopete: fix the contact list not resorting after renaming contact (<a href="http://bugs.kde.org/show_bug.cgi?id=86783">#86783</a>)</li>
<li>kopete: fix the graphical error in the contact properties for contacts with address book associations (<a href="http://bugs.kde.org/show_bug.cgi?id=85870">#85870</a></li>
<li>kopete: hide the header in the contact list (<a href="http://bugs.kde.org/show_bug.cgi?id=87974">#87974</a>)</li>
<li>kopete: fix highlighted contacts not using the inverse text color (<a href="http://bugs.kde.org/show_bug.cgi?id=88495">#88495</a>)</li>
<li>kopete: fix the position of the popup menu on contact items after removing the header from the contact list</li>
<li>kopete: fix stale chat member lists appearing in chat window titles</li>
<li>kopete: fix URL parsing for URLs containing "!"</li>
<li>kopete: fix crash on connection for people using gcc 2.95. Affects AIM, ICQ, and MSN</li>
<li>kopete: fix the move contact context menu not updating (<a href="http://bugs.kde.org/show_bug.cgi?id=88751">#88751</a>)</li>
<li>kopete: fix multiple formulas in one paragraph confusing Kopete's latex plugin (<a href="http://bugs.kde.org/show_bug.cgi?id=88759">#88759</a>)</li>
<li>kopete: fix the non-escaping of bash wildcards in the latex plugin (<a href="http://bugs.kde.org/show_bug.cgi?id=89384">#89384</a>)</li>
<li>kopete: fix the cutting off of long nicknames (<a href="http://bugs.kde.org/show_bug.cgi?id=73901">#73901</a>)</li>
<li>kopete: don't request the display picture in MSN group chats</li>
</ul>

<a name="kdepim"><h3>kdepim</h3></a>
<ul>
<li>kaddressbook: Update the button bar when adding contacts from the ldap query</li>
<li>kalarm: Prevent crash if kalarmui.rc is missing (<a href="http://bugs.kde.org/show_bug.cgi?id=89610">#89610</a>)</li>
<li>kalarm: Prevent Quit option from becoming useless if "Don't ask again" is selected along with Cancel in confirmation dialog</li>
<li>kalarm: Fix scheduling of weekly/monthly/yearly recurrences from the command line</li>
<li>kalarm: Fix errors when altering or cancelling deferrals of expired recurrences</li>
<li>kalarm: Fix message window size not fitting message</li>
<li>kalarm: Prevent blind copy to self of email alarms via KMail when bcc is deselected</li>

<li>kmail: Fixed a couple of bugs which can lead to mail loss with disconnected IMAP</li>
<li>kmail: Fixed "Switch to html mode when using bullets in composer" (<a href="http://bugs.kde.org/show_bug.cgi?id=88255">#88255</a>)</li>
<li>kmail: Fixed "Clicking bold/italic in composer sometimes doesn't do anything"</li>
<li>kmail: Fixed the mailing list address importing (<a href="http://bugs.kde.org/show_bug.cgi?id=78626">#78626</a>)</li>
<li>kmail: Fixed folder tree flicker</li>
<li>kmail: Fixed "attaching files with long lines causes message loss" (<a href="http://bugs.kde.org/show_bug.cgi?id=74254">#74254</a>)</li>
<li>kmail: Filter dialog: create at least one new filter where there is one (usability improvement)</li>
<li>kmail: Fixed crash on folder creation under certain conditions</li>
<li>kmail: AV-filter scripts: Make antivir search in archives</li>
<li>kmail: Numerous crash fixes (xml-gui, anti-spam wizard)</li>
<li>kmail: Added icon for agenda resource folder</li>
<li>kmail: Don't move files if source and target folder are the same</li>
<li>kmail: Let address completion keep working after reconfiguring ldap servers</li>
<li>kmail: Fixed crash when expiring mail with the menu item (<a href="http://bugs.kde.org/show_bug.cgi?id=88508">#88508</a>)</li>
<li>kmail: Made it possible to use disconnected IMAP folders as draft folders</li>
<li>kmail: Allow the use of distribution list in BCC again (regression, <a href="http://bugs.kde.org/show_bug.cgi?id=87607">#87607</a>)</li>
<li>kmail: ACL dialog: make sure user cannot remove his own folder admin rights</li>
<li>kpilot: Added missing icons</li>
<li>kpilot: Various fixes</li>
<li>korganizer: Fix typeahead for new events in agendaview</li>
<li>korganizer: Don't crash on undo when no resource is available (<a href="http://bugs.kde.org/show_bug.cgi?id=87798">#87798</a>)</li>
<li>korganizer: Fixes for freebuy view: if appointment is more than 15 days in the future; made "pick a date" button work"</li>
<li>korganizer: Don't crash when cancelling a drop on the daymatrix (<a href="http://bugs.kde.org/show_bug.cgi?id=77852">#77852</a>)</li>
<li>korganizer: Various fixes in the invitation editor dialog</li>
<li>korganizer: Various timezone handling fixes</li>
<li>korganizer: Fix for "directory for local dir resource was never automatically created, and all data of that resource was just lost" (<a href="http://bugs.kde.org/show_bug.cgi?id=87327">#87327</a>)</li>
<li>korganizer: Don't try to save read-only resources, which lead to an error. (<a href="http://bugs.kde.org/show_bug.cgi?id=87116">#87116</a>)</li>
<li>konsolekalender: various fixes</li>
<li>kontact: "open in addressbook" when kaddressbook-part is not yet loaded showed only the addressbook part (<a href="http://bugs.kde.org/show_bug.cgi?id=87233">#87233</a>)</li>
<li>kontact: Completely rewritten knotes plugin</li>
<li>kontact: Summary view: new default plugins</li>
<li>kontact: Summary view: various layouting fixes</li>
<li>kontact: Sidebar: do not raise part on certain drag'n drop actions</li>
</ul>

<a name="kdesdk"><h3>kdesdk</h3>
<ul>
<li>umbrello: Remove need for flex library (<a href="http://bugs.kde.org/show_bug.cgi?id=89582">#89582</a>)</li>
<li>umbrello: Fix various crashes (e.g. <a href="http://bugs.kde.org/show_bug.cgi?id=77645">#77645</a>)</li>
<li>umbrello: Fix deletion of message in sequence diagram (<a href="http://bugs.kde.org/show_bug.cgi?id=87995">#87995</a>)</li>
<li>umbrello: Fix deletion of association name label (<a href="http://bugs.kde.org/show_bug.cgi?id=89579">#89579</a>)</li>
<li>umbrello: Fix C++ import of code with comments (<a href="http://bugs.kde.org/show_bug.cgi?id=86958">#86958</a>)</li>
<li>umbrello: Improve quality of export to EPS (<a href="http://bugs.kde.org/show_bug.cgi?id=89553">#89553</a>)</li>
<li>umbrello: Allow deletion of enum literals</li>
<li>umbrello: Improve perl code generation (POD)</li>
</ul>

<a name="kdetoys"><h3>kdetoys</h3>
<ul>
</ul>

<a name="kdeutils"><h3>kdeutils</h3>
<ul>
<li>Try to make KFloppy usable again.</li>
</ul>

<a name="kdevelop"><h3>kdevelop</h3>
<ul>
</ul>

<a name="kdewebdev"><h3>kdewebdev</h3>
<ul>
<li><h4>Quanta Plus</h4>
<ul>
<li><strong>VPL:</strong> enable VPL on KDE 3.3.x</li>
<li>show (again) the full filename in a tooltip</li>
<li>don't crash if the preview widget is closed with a JavaScript command from the code itself (<a href="http://bugs.kde.org/show_bug.cgi?id=87533">#87533</a>)</li>
<li>possible crash on startup fixed</li>
<li>don't try to autofill a closing tag for non-xml tags (<a href="http://bugs.kde.org/show_bug.cgi?id=89212">#89212</a>)</li>
<li>when opening a Quanta 3.2 project set the upload status of the files to "When Modified" not to "Never" (<a href="http://bugs.kde.org/show_bug.cgi?id=88232">#88232</a>)</li>
<li>when adding files to a project, use the upload status of the parent directory for the newly added file</li>
<li>fix the Save As.. behavior (it defaulted to some strange directories, depending on the active treeview, selected directory, etc.)</li>
<li>update the modified status text/icon when using Save All (<a href="http://bugs.kde.org/show_bug.cgi?id=87196">#87196</a>)</li>
<li>always find the right action to edit, even if there are more actions with the same user visible name</li>
<li>don't change the template description if writing to the .dirinfo file fails (usually for global templates)</li>
<li>fix creation of new template directories (template type was stored incorrectly)</li>
<li>display the user-readable template type in every dialog</li>
<li>fix the Konqueror launch in meinproc.kmdr</li>
<li>fix open dialog in checkxml.kmdr: use the the folder selection dialog to select folders</li>

<li><strong>improvement:</strong> don't show the project toolbar when no project is loaded</li>
<li><strong>improvement:</strong> support loading of more than one toolbar at a time</li>
<li><strong>improvement:</strong> don't ask for toolbar saving if the toolbar names was modified by Quanta to add (1), (2), etc. at the end
</li>
<li><strong>improvement:</strong> disable the Quanta Template page in properties if you don't have writing rights to the directory</li>
<li><strong>improvement:</strong> show the user-readable template description for every template file, not just the directories.</li>
<li><strong>improvement:</strong> don't allow to change the template type in the properties of a file as it's valid per-directory.</li>
</ul>
</li>
<li>
<h4>Kommander</h4>
<ul>
<li>output from ExecButton wasn't sent to standard output</li>
</ul>
</li>
<li>
<h4>KLinkStatus</h4>
<ul>
<li>enable the hide toolbar menu item in the toolbar context menu (and don't crash with KDE 3.3.0 when you right click on the toolbar)</li>
</ul>
</li>
</ul>
<?php include "footer.inc" ?>
