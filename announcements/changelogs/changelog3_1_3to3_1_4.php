<?php
  $page_title = "KDE 3.1.3 to 3.1.4 Changelog";
  $site_root = "../../";
  include "header.inc";
?>

<p>
This page tries to present as much as possible of the problem
corrections that occurred in KDE between the 3.1.3 and 3.1.4 releases.
</p>
<p>
Please see the <a href="changelog3_1_2to3_1_3.php">3.1.2 to 3.1.3 changelog</a> for further information.
</p>

<h3>arts</h3>
<ul>
<li>Fix artsbuilder crash with changed construction order in gcc 3.3.2 release.</li>
<li>Fix stereo volume control.</li>
<li>Fix compilation problems.</li>
<li>Crash fixes.</li>
</ul>

<h3>kdelibs</h3>
<ul>
<li><b>Allows compilation against Qt 3.2.x</b></li>
<li>improvements to utf8-filename handling in KIO</li>
<li>fix nonlating-filenames in http form upload (#61721)</li>
<li>ignoring X11 errors during startup to avoid startup notification to stop early</li>
<li>Preformance improvement to KGuiItem::plainText()</li>
<li>kdesu: implement session management for kdesu-started applications.</li>
<li>khtml: fix form widget sizing when code is compiled with gcc 3.4+.</li>
<li>kio: fix bug that caused gzip-encoded webpages to fail rendering
when compiled with Qt 3.2.</li>
<li>keramik widgets: fix merging mistake that caused drawing errors
  in comboboxes.</li>
<li>keramik widgets: Fix pixmap cachine keying, correcting odd artifacts with some colorscheme changes.</li>
<li>dcop: honor $DCOPAUTHORITY which allows to specify an alternative
location than $HOME for .DCOPserver_xxx</li>
<li>kcookiejar: fix cookie handling cornercase</li>
<li>kdeinit: support read-only home directories</li>
<li>artsd: fix restart</li>
<li>khtml: code fixes to avoid miscompilation of SunProCC under Solaris</li>
<li>kate: fix offering of default encoding when saving a file.</li>
<li>netwm: fix massive memory leak (showing up e.g. in kicker).</li>
<li>kate: fix compilation under aCC.</li>
<li>khtml: several crash fixes. Qt 3.2.x compatibility fixes. rendering improvements.</li>
<li>khtml: merging bug that caused the famous scrollbar problem fixed.</li>
<li>klistview: massive performance improvement in alternative color background
handling</li>
<li>khtml/java: improved handling of slightly malformed urls.</li>
<li>kio: only cache successful passwords, otherwise its impossible to
re-enter a password when it failed authorisation the first time.</li>
</ul>

<h3>kdeaddons</h3>
<ul>
<li>gcc 3.4+ compile fixes.</li>
<li>Debian packaging updates.</li>
</ul>

<h3>kdeadmin</h3>
<ul>
<li>kxconfig: add mouse wheels support to USB mice.</li>
<li>kxconfig: fix solaris compile.</li>
<li>ksysv: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=49265">Bug 49265: inactive hyperlinks in Help and About</a></li>
</ul>

<h3>kdeartwork</h3>
<ul>
<li>Debian packaging updates.</li>
</ul>

<h3>kdebase</h3>
<ul>
<li>kdialog: Fixed "--warningcontinuecancel".</li>
<li>Konqueror: Don't crash when opening a directory in tab from the navigation panel</li>
<li>KDM: <a href="/info/security/advisory-20030916-1.txt">Fix weak cookie generation / local root</a></li>
<li>smb:// slave: improve error handling.</li>
<li>khotkeys: allow usage of global config file for site-wide configuration.</li>
<li>kwin: fix mouse grabbing problem with _NET_WM_MOVERESIZE.</li>
<li>krdb: improve exported colorsheme for Adobe acroread.</li>
<li>fonts kcontrol: improve configure check to handle newer fontconfig releases which use pkg-config.</li>
<li>kwin: fix "left click into active window in background does not raise" bug</li>
<li>KDM: implement parsing of display names that contain dashes.</li>
<li>kicker: memory leak fixes.</li>
<li>khotkeys: crash fix.</li>
<li>konsole: ported grantpty to FreeBSD.</li>
<li>ksmserver: support read-only home directory.</li>
<li>Improve FreeBSD support in fish://.</li>
<li>kdesktop: accept empty password if account has no password set.</li>
<li>konqueror: ensure that we don't change the servicetype, in case
the mimetype is ambiguous.</li>
<li>konqueror: Fix handling of wildcard file selection / unselection.</li>
<li>konqueror: fix crash when trying to drag the location label.</li>
<li>konqueror's listview: selected items don't get unselected anymore during DnD operation</li>
<li>thumbnail creator: fix filedescriptor leak.</li>
<li>thumbnail creator: fix Postscript thumbnail rendering.</li>
</ul>

<h3>kdebindings</h3>
<ul>
<li>Package / build updates.</li>
</ul>

<h3>kdeedu</h3>
<ul>
<li>kstars: Fix bug which essentially made all Southern Hemisphere
locations to report that Daylight savings time was always active.</li>
<li>kvoctrain: fix crash on inline editing.</li>
<li>kstars: Implement altitude shift caused by atmospheric refraction.</li>
</ul>

<h3>kdegames</h3>
<ul>
<li>ksokoban: fix rare crash on startup.</li>
<li>katomic: fix compile warnings.</li>
<li>kasteroids: Fix checked/unchecked "pause" action</li>
<li>kasteroids: Don't connect the cancel button twice to reject slot.</li>
<li>gcc 3.4+ compile fixes.</li>
</ul>

<h3>kdegraphics</h3>
<ul>
<li>kiconedit: Fix 'Paste As New' action.</li>
<li>kiconedit: make configuration dialog modal.</li>
<li>kiconedit: icon preview area uses the background settings now.</li>
<li>kiconedit: improve "image is modified" handling.</li>
<li>kiconedit: improve mouse cursor pixmaps.</li>
<li>kiconedit: fix loading/saving logic to be more robust.</li>
<li>kiconedit: correctly display icons with an alpha channel.</li>
<li>kamera: fix handling of PTP based cameras.</li>
<li>kghostview: fix stopping of download indicator when download is cancelled.</li>
<li>kghostview: fix CTRL-M accelerator clash.</li>
</ul>

<h3>kdemultimedia</h3>
<ul>
<li>compile fixes with newer gcc 3.3 releases.</li>
<li>compile fixes for upcoming gcc 3.4 release.</li>
<li>noatun: memory leak fix.</li>
<li>kscd: massive memory leak fix.</li>
<li>kaboodle: fix segfault during exit.</li>
<li>noatun: fix rare crash when playlist entries are deleted.</li>
</ul>


<h3><a name="kdenetwork">kdenetwork</a></h3>
<ul>
<li>kpf: Generate proper links in the HTML output for directories with spaces and umlauts.</li>
<li>knewsticker: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=49114">Bug 49114: KNewsticker news download still buggy?</a></li>
<li>knewsticker: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=52642">Bug 52642: does not update an rdf feed when the new feed has no entries</a></li>
<li>knewsticker: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=62129">Bug 62129: Suggest button is not cancellable."</a></li>
<li>knewsticker: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=63224">Bug 63224: knewsticker doesn't update news</a></li>
<li>knewsticker: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=63265">Bug 63265: Ampersands in news source is shown as underline</a></li>
<li>knewsticker: Filters now work properly with original (non-custom) newsfeed names.</li>
<li>knewsticker: Scrolltext now eats less CPU, scrolling speed slider influences speed linearly.</li>
<li>kmail: Fix crypto plugin loading.</li>
<li>kmail: Fix decoding of subjected in embedded mime parts.</li>
<li>kmail: Fix crash during configuring signatures.</li>
<li>kmail: Fix portability issue in header field decoding.</li>
<li>kmail: Fix bug that caused mailman to mangle headers.</li>
<li>kmail: Fix mangling of multiline, quoted-printable encoded subject header.</li>
<li>kmail: Roaming User Support.</li>
<li>krfb: multihead fix.</li>
<li>kget: Roaming User Support.</li>
<li>knode: Roaming User Support.</li>
</ul>

<h3>kdepim</h3>
<ul>
<li>kpilot: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=50560">Bug 50560: address not synced correct with new address information</a></li>
<li>kpilot: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=60659">Bug 60659: Always using at least one of the addresses from kaddressbook</a></li>
<li>kpilot: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=60691">Bug 60691: KPilot vcal conduit changes day when syncing repeating events</a></li>
<li>kpilot: fix errors in build system.</li>
</ul>

<h3>kdesdk</h3>
<ul>
<li>Debian packaging updates.</li>
</ul>

<h3>kdetoys</h3>
<ul>
<li>amor: Fix <a href="http://bugs.kde.org/show_bug.cgi?id=62000">Bug 62000: error in amor's config dialog cancel button</a></li>
</ul>

<h3>kdeutils</h3>
<ul>
<li>compile fixes for upcoming gcc 3.4 release</li>
<li>kdf: Fixed memory leak</li>
<li>ksim: Fixed memory leak</li>
<li>kedit: Made Undo/Redo possible when inserting files</li>
</ul>

<h3>quanta</h3>
<ul>
  <li>resolve symlinks before opening a file [<a href="http://bugs.kde.org/show_bug.cgi?id=60860">#60860</a>
]</li>
   <li>don't insert the "&lt;meta http-equiv="Content-Type" content="text/html; charset=..." line when using the Quick Start dialog [<a href="http://bugs.kde.org/show_bug.cgi?id=61500">#61500</a>
]</li>
   <li>fix CTRL-C behavior [<a href="http://bugs.kde.org/show_bug.cgi?id=62624">#62624</a>]</li>
   <li>fix message window handling</li>
   <li>fix script action error output handling</li>
   <li>honour the "Do not load the modified version from disk." setting in the dirty file dialog</li>
</ul>

<?php include "footer.inc" ?>
