------------------------------------------------------------------------
r1226900 | lvsouza | 2011-04-03 09:51:13 +1200 (Sun, 03 Apr 2011) | 2 lines

Sorting here is not necessary.

------------------------------------------------------------------------
r1226904 | lvsouza | 2011-04-03 10:18:03 +1200 (Sun, 03 Apr 2011) | 7 lines

Update accounts' status message when NowPlaying plugin changes
the global status message and not only when user manually set
the global status message.

BUG: 155865
FIXED-IN: 4.6.2

------------------------------------------------------------------------
r1227262 | lvsouza | 2011-04-07 13:32:15 +1200 (Thu, 07 Apr 2011) | 8 lines

Backporting r1227261 to 4.6 branch: 

Using a more restrictive regular expression to retrieve
the Now Listening data from status message.

BUG: 222147, 261989, 121097
FIXED-IN: 4.6.3

------------------------------------------------------------------------
r1227471 | mfuchs | 2011-04-09 21:26:37 +1200 (Sat, 09 Apr 2011) | 2 lines

Further addresses CVE-2010-1000.
The file name of Metalink File is checked a better way, making it work under more conditions.
------------------------------------------------------------------------
r1227534 | lvsouza | 2011-04-10 17:16:12 +1200 (Sun, 10 Apr 2011) | 17 lines

Backporting 1227533 to 4.6 branch:

Per metacontact statistics DB is created only when contact goes online
or when statistics for an offline metacontact is requested.
This improves plugin startup and shutdown times and also helps with UI
responsiveness.

Now shutdown time is proportional to the number of metacontacts that went
online since Kopete has started plus the number of offline metacontacts
that the user requested statistics from. That is better then being
proportional to the number of all metacontacts registered but is not that
good yet.

BUG: 117989, 138903
CCBUG: 246785
FIXED-IN: 4.6.3

------------------------------------------------------------------------
r1227572 | scripty | 2011-04-11 01:00:37 +1200 (Mon, 11 Apr 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1227608 | lvsouza | 2011-04-11 10:15:30 +1200 (Mon, 11 Apr 2011) | 2 lines

This is not really needed.

------------------------------------------------------------------------
r1227624 | lvsouza | 2011-04-11 14:24:18 +1200 (Mon, 11 Apr 2011) | 7 lines

Backporting r1227623:

Make Yahoo/Wlm mail notifications persistent.

BUG: 240595
FIXED-IN: 4.6.3

------------------------------------------------------------------------
r1227629 | lvsouza | 2011-04-11 15:10:55 +1200 (Mon, 11 Apr 2011) | 8 lines

Backporting r1227628:

Re-apply fix for bug 131951. Somehow the fix got lost during KDE 3.x -> 4.x
transition.

BUG: 187977
FIXED-IN: 4.6.3

------------------------------------------------------------------------
r1227831 | lvsouza | 2011-04-13 12:36:45 +1200 (Wed, 13 Apr 2011) | 2 lines

Initializing private variable.

------------------------------------------------------------------------
r1228259 | pali | 2011-04-17 00:34:26 +1200 (Sun, 17 Apr 2011) | 2 lines

Do not start Skype if user want to change status message of offline skype account

------------------------------------------------------------------------
r1228309 | lvsouza | 2011-04-17 22:22:15 +1200 (Sun, 17 Apr 2011) | 6 lines

Backporting r1228308 to 4.6 branch:

Set the status message when using the away toggle shortcut.

FIXED-IN: 4.6.3

------------------------------------------------------------------------
r1228391 | lvsouza | 2011-04-18 14:57:12 +1200 (Mon, 18 Apr 2011) | 7 lines

Backporting r1228390 to 4.6 branch:

Provides close button for tabs in chat windows.

BUG: 224388
FIXED-IN: 4.6.3

------------------------------------------------------------------------
r1228835 | scripty | 2011-04-23 02:35:59 +1200 (Sat, 23 Apr 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1228915 | mblumenstingl | 2011-04-24 01:25:19 +1200 (Sun, 24 Apr 2011) | 6 lines

Backport of 1228913
Fix "last seen" property when quitting kopete.

CCBUG: 266697


------------------------------------------------------------------------
r1229010 | scripty | 2011-04-25 01:45:47 +1200 (Mon, 25 Apr 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1229133 | alexmerry | 2011-04-26 12:46:29 +1200 (Tue, 26 Apr 2011) | 3 lines

Fix the build for GCC 4.6.0


------------------------------------------------------------------------
r1229550 | scripty | 2011-04-28 04:42:30 +1200 (Thu, 28 Apr 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
