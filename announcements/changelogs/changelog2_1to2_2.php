<?php
  $page_title = "KDE 2.1 to 2.2 Changelog";
  $site_root = "../../";
  include "header.inc";
?>

<p>This page tries to present as much as possible of the problem
corrections and feature additions that occurred in KDE between the 2.1
and 2.2 releases.</p>

<p><strong>Note:</strong> gcc 3.0 is <em>not recommended</em> for compilation of KDE 2.2. 
There are several known miscompilations of production C++ code with this
compiler, like i.e. virtual inheritance which is used for example in arts.
We know that you will experience a lot of runtime difficulties when you compile
with gcc 3.0, so using this particular compiler version is at your own risk,
and please do not report bugs to us if you use this compiler. The
problems are mostly known and we're working with the gcc team to get them fixed.</p>


<h2>KDE Libraries</h2>

<h3>General</h3>
<ul>
  <li>Classes KTipDialog and KTipDatabase added, providing a easy-to-use "tip of the day" dialog. Tips
        are collected in a tipdatabase in XML format. Used by KDevelop and KOrganizer.</li>
  <li>Alternating color-changing in listviews (used in Konqueror and KFileDialog)</li>
  <li>File previews in KFileDialog</li>
  <li>General file preview classes for mime-type specific file preview implementation</li>
  <li>Fixed support for QIconSets in QPushButtons in several widget styles.</li>
  <li>Fixed support for toggle-items with icons in QPopupMenus in several widget styles.</li>
  <li>Fixed several styles not displaying an arrow on QPushButtons with a popup 
menu assigned (QPushButton::setPopup()).</li>
  <li>Improved retrieval of taskbar / window manager icons for windows that
do not supply their own icons</li>
  <li>KBugReport dialog now determines OS specific parameters at run time instead
of build time</li>
  <li>Generic host/domain based global configuration of io-slaves.</li>
  <li>Applications can now override global io-slave settings.</li>
  <li>New class 'KArrowButton' used for buttons which point in a certain direction.</li>
  <li>KProcIO added. KProcIO extends KProcess and eases the
      processing of stdin/stdout of the child process.</li>
  <li>Improved application startup notification added</li>
  <li>for KHML fixes, see the Konqueror section in the KDE Base Package part of this document.</li>
  <li>new print library including a KPrinter interface class built on top of
   the KDE print framework, and an enhanced print dialog</li>
  <li>Meta key &amp; AltGr key can now be used for shortcuts</li>
  <li>Shortcuts for moving &amp; resizing windows now use Meta key instead of
      Alt key (when Meta key is present)</li>
  <li>New widget styles added:
  <ul>
    <li>MegaGradient highcolor (transparent menus)</li>
    <li>Web style</li>
  </ul></li>
</ul>

<h3>KDE Print System</h3>
<ul>
  <li>based on a plugin architecture, currently supporting: CUPS, LPR, RLPR,
   external command (Netscape-like), generic LPD</li>
  <li>complete control on the printer and print options (if possible) via the
   print dialog</li>
  <li>print management through a KControl module to manage:
        <ul>
          <li>printers (add/remove/enable/disable/configure)</li>
          <li>server (only for CUPS currently)</li>
          <li>print jobs (cancel/hold/move)</li>
        </ul></li>
  <li>configurable filter mechanism: for example to change the number of pages
   per sheet</li>
  <li>configurable pseudo printers that send the print data to an external
   program (PDF writing, faxing, mailing, ...)</li>
  <li>print job viewer automatically started and docked in kicker</li>
</ul>

<h3>aRts</h3>
<ul>
  <li>applications like noatun or Brahms can easily show guis for configuring
      effects now</li>
  <li>support for decoding streams (kio enabled), that is, you can play mp3s
      off the net (kaboodle is a player that uses this interface)</li>
  <li>structures designed by artsbuilder can be used as modules again</li>
  <li>support for changing the playing speed/pitch of PlayObjects</li>
  <li>Endianness fixes for big endian platforms such as Solaris/SPARC</li>
  <li>"out-of-the-box" support for Solaris and AIX</li>
  <li>Glib2.0 mainloop support via libgmcop</li>
  <li>Threading support via libmcop_mt</li>
  <li>MIDI timing and performance improvements</li>
  <li>New <tt>artsd -s</tt> option that allows setting the auto-suspend time</li>
  <li><tt>artsshell status</tt> now displays all of the server audio parameters</li>
  <li>New <tt>artsshell autosuspend</tt> option to change auto-suspend time</li>
  <li>New <tt>artsd</tt> -m option that passes reporting of sound server errors to an external program</li>
  <li>New <tt>artsmessage</tt> program which displays <tt>artsd</tt> errors using a KDE dialog</li>
  <li>KDE sound control panel now allows setting the auto-suspend timeout and the message display program. The <tt>artsmessage</tt> command is enabled by default.</li>
  <li>New <tt>artsd -N</tt> and <tt>-w</tt> options for setting larger network buffers</li>
</ul>

<h3>Icons</h3>
<ul>
  <li>Icon highlighting (gamma effect) by default enabled for Desktop/Filemanager and Panel groups</li>
  <li>New set of 64x64 for mimi-type icons added</li>
  <li>General polishing of icons</li>
  <li>Dithered icon is used instead of low-color icon when no low-color icon is available</li>
</ul>


<h2>KDE Base Package</h2>

<h3>General</h3>
<ul>
  <li>NumLock turn on during startup now possible</li>
  <li>Improved application startup notification by a busy cursor displayed by the starting application's icon that can additionally blink. KControl
  module for configuration added (see KControl)</li>
  <li>new filepreviews for KDesktop and Konqueror: pdf/ps, sound files (web-archives see kdeaddons package)</li>
  <li>New option to refresh desktop</li>
  <li>New keyboard shortcuts for Window and Desktop Screenshot</li>
</ul>

<h3>KHelpCenter</h3>
<ul>
  <li>KHelpCenter is an application of its own again unlike the former Konqueror plugin</li>
  <li>The Helpsystem is now completely based on XML and converted to HTML via libxml2</li>
  <li>Added a glossary which explains various KDE-, X- and Unix-related terms.</li>
</ul>

<h3>Kate</h3>
<ul>
  <li>Kate is a new, enhanced Editor application in the kdebase package based on the kwrite part</li>
  <li>plugin architecture</li>
  <li>split-view capable</li>
  <li>includes filebrowser for simple project management</li>
</ul>

<h3>KDM</h3>
<ul>
  <li>KDM now consists of three programs: kdm, kdm_config and kdm_greet.
  You needn't to care for the latter two, just don't be surprised.</li>
  <li>The configuration changed a lot. KDM does not any longer share
  its configuration with a possibly installed XDM. KDM's config files
  are now all located in the kdm/ subdirectory of your KDE config directory.
  All settings from the former xdm-config have moved to kdmrc. You may use
  some of the files from the XDM configuration directory (usually
  /usr/X11R6/lib/X11/xdm) as templates for KDM's config. Don't blindly
  copy everything, as it might cause unexpected problems. Note that
  subsequent "make install"s won't overwrite your customized configuration
  any more.</li>
</ul>

<h3>Kicker</h3>
<ul>
  <li>taskbar grouping of windows</li>
  <li>More configuration options for extension panels</li>
  <li>Auto-hide time can be set to zero</li>
  <li>Redesign of control module</li>
  <li>size of kicker can now be set in percent of the screen width</li>
  <li>text-fade out effects in taskbar</li>
  <li>Panel is now shown when switching desktop</li>
  <li>Clock Applet: now has plain mode</li>
  <li>System Tray: lock/logout icons now have RMB menu</li>
  <li>Many new applets like kworldclock, fifteen pieces, character picker and more</li>
  <li>New keyboard shortcut "Toggle Showing Desktop"</li>
  <li>Pinnable menus</li>
  <li>KAppfinder: Terminal applications are put into sub-menus</li>
</ul>

<h3>AbBrowser/KAddressBook</h3>
<ul>
  <li>AbBrowser has been renamed to KAddressBook</li>
  <li>Import of arbitrary lists of comma-seperated values</li>
  <li>Export of addressbook as list of comma-seperated values</li>
</ul>

<h3>Konsole</h3>
<ul>
  <li>Size of history can be limited to prevent filling up of disk.</li>
  <li>schema editor</li>
  <li>New keyboard shortcut to start a new session (Ctrl-Alt-n), definable as "newSession" in Keytabs files</li>
</ul>

<h3>Konqueror</h3>
<ul>
  <li>Properties dialog now shows free disk space on current partition, e.g. "39.7MB/968.3MB (90% used)"</li>
  <li>New configuration for whether to start a new process for new windows or reuse an existing one</li>
  <li>New configuration for user-defined CSS stylesheets</li>
  <li>Proxy support re-engineered, socks and IPv6 support added (in kdelibs)</li>
  <li>Save toolbar layout in profile</li>
  <li>Fixes for full-screen mode</li>
  <li>Fixes for reload, detects mimetype changes</li>
  <li>"Enter" button in the toolbar</li>
  <li>"Most Often Visited" URL list in the Go menu</li>
  <li>New sidebar added as Extended Sidebar that has single/multiple view switching and uses dockwidgets.
        The categories in the old sidebar can be used as single windows and new windows added by URL.</li>
  <li>Close button for vertical sidebars</li>
  <li>"Send Link", "Send File" and "Open With Mozilla" functionality (File menu)</li>
  <li>New option to stop animated gifs in RMB context-menu.</li>
  <li>Konqueror introduction can be restarted at any time with Help/Konqueror Introduction.</li>
  <li>Many improvements to the completion (and including bookmarks into it)</li>
  <li>KHTML: rewritten Event Handling for more precise DOM2 conformance.</li>
  <li>KHTML: as usual a huge pile of bugfixes and workarounds for legacy HTML, some small performance improvements.</li>
  <li>KHTML: improved ECMA bindings, better floating and inline boxing layouting for improved CSS2 conformance and compatibility.</li>
  <li>KHTML: A small number of memory leaks and quite a few causes for crashes removed.</li>
  <li>KHTML: Extended the DOM bindings for CSS and parts of DOM3 recommendation.</li>
  <li>KHTML: theming support (currently colors only) for form widgets.</li>
  <li>KHTML: extended compatibility with IE's parsing and tokenisation fallbacks for really malformed HTML.</li>
  <li>Javascript: Support for history navigation (back/forward/go...)</li>
  <li>Javascript: Garbage collector fixes</li>
  <li>Javascript: LiveScript backward compatibility</li>
  <li>Bookmark Editor: Use UTF-8 for Mozilla bookmarks</li>
  <li>Bookmark Editor: Some useability improvements, added Create Bookmark</li>
  <li>Find File component: Save and restore the found items into the history</li>
  <li>List View and Tree View: many fixes and improvements</li>
</ul>


<h3>KPersonalizer</h3>
<ul>
<li>KPersonalizer (Desktop Settings Wizard) is a new application in KDE 2.2</li>
  <li>KPersonalizer configures KDE to fit your personal tastes and habits</li>
  <li>On the first KDE start, KPersonalizer comes up allowing to set your
      preferred language, keybindings, color scheme, theme and the amount of
      graphical effects. It especially aims to make  it easy and safe for new
      users to set up KDE to fit their basic needs and habits to ensure maximal
      productivity from the very beginning.</li>
  <li>The personalizer can be started any time via K-&gt;System-&gt;Desktop Settings Wizard</li>
  <li>in favor of the maximum functionality, a new soundscheme has been added for KWin events (e.g.maximizing window)</li>
  <li>a default wallpaper for KDE has been added (default_blue.jpg)</li>
</ul>

<h3>KWin (KDE Window Manager)</h3>
<ul>
  <li>support for xinerama (multi-head) added</li>
  <li>new keybindings added for various functions (e.g.Alt+Tab, Ctrl+Tab are now configurable), see kcontrol</li>
  <li>no flickering during virtual desktop switches</li>
  <li>started applications appear on the virtual desktop that was active when
they were started</li>
  <li> fix : SkipTaskbar flag now works correctly</li>
  <li>kwin has a new Window Decoration kcontrol module including:
  <ul>
    <li>style selector</li>
    <li>drag and drop button position selector</li>
    <li>support for kwin style configuration plugins</li>
  </ul></li>
  <li>New KWin decoration styles added:
  <ul>
    <li>quartz</li>
    <li>IceWM (native IceWM theme reader)</li>
    <li>MWM</li>
    <li>Web</li>
  </ul></li>
</ul>

<h3>KControl (KDE Control Center)</h3>
<ul>
  <li>About module information added. When a module is loaded, the information can be retrieved by Help-&gt;About module..</li>
  <li>Each group has an information page listing all modules of the group plus short description as a link, clicking on the link
        loads the module.</li>
  <li>New Modules:
  <ul>
    <li>Launch feedback: application startup notification configuration module (kicker notification, busy cursor)</li>
    <li>USB info module: displays information about the currently connected USB devices</li>
    <li>KWin decoration module: due to the growing amount of window decorations, a kcontrol module configures the
    window decorations. Includes customization and theme support for iceWM themes.</li>
    <li>Configuration of user defined stylesheets (CSS) module</li>
    <li>Konsole configuration module</li>
    <li>Audio CD ripping configuration module (MP3, Ogg Vorbis format options)</li>
  </ul></li>
  <li>Changed/rewritten modules:
  <ul>
    <li>Kicker</li>
    <li>Mouse module</li>
    <li>Keybindings module has undergone new keybinding schemes plus configuration options</li>
    <li>allmost all other modules have been revised for usability and geometry management fixes</li>
    <li>Integrated "GTK Theme Importer"</li>
    <li>User Agent setup</li>
  </ul></li>
</ul>

<h3>KScreensaver</h3>
<ul>
  <li>Added libkscreensaver to ease development of KDE screensavers</li>
</ul>

<h2>KDE Network Package</h2>

<h3>Kdict</h3>
<ul>
  <li>Kdict, a dictionary program, has been added to kdenetwork.</li>
  <li>Kdict is a graphical client for the DICT protocol. It enables
  you to search through dictionary-like databases for a word or
  phrase, then displays suitable definitions. Kdict has support for
  all protocol features, a separate list of matching words for
  advanced queries, configurable database sets, a browser-like user
  interface, convenient interaction with the X-clipboard, configurable
  HTML output and printing.</li>
</ul>


<h3><a name="kmail">KMail</a></h3>
<ul>
   <li>IMAP support including SSL and TLS</li>
   <li>SSL and TLS support for POP3</li>
   <li>Configuration for SASL and APOP authentication</li>
   <li>Non-blocking sending</li>
   <li>Performance improvements for huge folders</li>
   <li>Message scoring</li>
   <li>Improved filter dialog</li>
   <li>Automatic filter creation</li>
   <li>Only the selected part of a mail will be quoted on reply</li>
   <li>Forwarding of mails as attachment</li>
   <li>Delete only <em>old</em> messages from the trash folder on exit</li>
   <li>Collapsable threads</li>
   <li>Multiple PGP identities</li>
   <li>Bind an SMTP server to an identity</li>
   <li>Better procmail support via the local account</li>
   <li>Messages can be flagged</li>
   <li>Read the new messages by only hitting the space key</li>
   <li>Automatic popup address completion (similar to Konqueror)</li>
</ul>

<h3>KNode</h3>
<ul>
<li>New scoring system:
  <ul>
    <li>User definable scoring rules with conditions based on the
    overview data and configurable actions (currently score value
    adjustment and notification)</li>
    <li>Full-fledged regular expressions which can be used in scoring
    and filter conditions.</li>
  </ul>
</li>
<li>Reduced memory consumption. Its now configurable how much RAM is
used to cache article bodies and headers.</li>
<li>KNode now uses dock widgets for its views, the user can arrange
them in any way he wants. Added the shortcuts 'g', 'h' and 'j' to switch
to group, header and article view.</li>
<li>Improved article handling:
  <ul>
    <li>User-defined folders</li>
    <li>DnD between folders and groups</li>
    <li>Convenient mbox-folder import/export</li>
    <li>Nice in-place renaming of accounts, folders and groups</li>
    <li>New menu option that expires all groups of the current account
    and a option that compacts all folders on demand.</li>
    <li>New expire-feature: delete article headers that aren't
    available on the server</li>
    <li>Implemented "Smart Scrolling" (TM) in the listviews and in the
    groupbrowser</li>
    <li>added option to expand all threads by default</li>
    <li>The configuration dialog has a new page "Navigation" which features the following new options:
      <ul>
        <li>emulation of the cursor key behaviour of KMail</li>
        <li>the "mark all read", "mark thread as read" and "ignore thread"
        actions can now trigger additional actions.(close thread,
        go to next thread/group)</li>
      </ul>
    </li>
  </ul>
</li>
<li>Reading news articles:
  <ul>
    <li>Proper handling of crossposted articles, they are marked as read simutaneously in all groups.</li>
    <li>Ignored articles are now automatically marked as read</li>
    <li>Added option to retreive the article source</li>
    <li>New menu option: fetch an article by message-id</li>
    <li>Added option to retreive an article from groups.google.com if it's unavailable on the local server</li>
    <li>Trailing empty lines can be stripped from incoming articles</li>
    <li>Highlighting of "news:" and "mailto:" urls, all email-addresses and msgids</li>
    <li>It's now configurable what characters are recognized as quote signs</li>
    <li>It's now possible to disable the fancy header decorations, saves
    space on small displays</li>
    <li>The word wrap behaviour of the article widget is now configurable</li>
    <li>Added a menuoption and a keyboard shortcut that switches
    from proportional to fixed font in the article viewer</li>
    <li>Its now possible to deactivate the tree view in the group subscription dialog.</li>
  </ul>
</li>
<li>Enhancements of the article composer
  <ul>
    <li>Optional integration of KMail and other external mail programs</li>
    <li>Full support for the Mail-Copies-To header</li>
    <li>Added placeholder for the group name in the attribution line</li>
    <li>Support for dynamically generated signatures</li>
    <li>Added option to place the cursor below the attribution line,
    not above the signature (off by default)</li>
    <li>Implemented forwarding of articles with attachments</li>
    <li>Files can now be attached to an article with Drag&amp;Drop</li>
  </ul>
</li>
<li>Improved article filters and search function:
  <ul>
    <li>Filter rules for numerical values are more flexible now, its
    possible to filter something like "x &lt; value" or "x &gt;
    value", not just ranges.</li>
    <li>Its now possible to filter on the message-id and references header</li>
    <li>The search function can show complete threads now (optionally)</li>
    <li>Its now possible to search in folders</li>
  </ul>
</li>
<li>Network related improvements:
  <ul>
    <li>Articles are no fetched via article number instead of
    article-id to avoid problems with broken newsservers</li>
    <li>Enhanced dupe protection, utilizing the "recommended id"
    feature of newer inn's</li>
    <li>Shows a password-dialog when the server requests authentication</li>
    <li>Support for IPv6 and SOCKS-proxies</li>
  </ul>
</li>
</ul>

<h3>KNewsTicker</h3>
<ul>
  <li>Added many more news sites to KNewsTicker's default list of news feeds.</li>
  <li>The configuration dialog got complete redesigned.</li>
  <li>It's now possible to specify filters to make KNewsTicker scroll only certain headlines along.</li>
  <li>You can now associate icons to news sources (and have those icons shown in the scroller) to make it easier to associate a headline with a news source.</li>
  <li>Dragging a headline from the news ticker to a Konqueror window will open the corresponding article in that Konqueror window.</li>
  <li>It's possible to make the scroller slow down by 50% as soon as the mouse hovers over it to make it easier to click on a headline.</li>
  <li>There are two new scrolling modes which rotate the headlines by 90 or 270 degree, those are especially useful if the news ticker is used in a vertical panel.</li>
  <li>The DCOP interface got vastly improved, it's now possible to influence pretty much every aspect of the look and feel of the news ticker via DCOP.</li>
  <li>You can now arrange the newsfeeds into categories such as "Arts", "Science" or "Computers" to make it easier to handle large lists of news feeds.</li>
  <li>KNewsTicker now extends Konqueror's file properties dialog, right-clicking on any RDF or RSS file will show an additional tab which lists various interesting information.</li>
  <li>Added precommand support, you can use various scripts (like those in kdeaddons/knewsticker-scripts) which transform arbitrary input data into RSS with KNewsTicker transparently.</li>
</ul>

<h3>Kit</h3>
<ul>
  <li>Password changing is available</li>
  <li>Changing buddy list doesn't make you appear to blink offline anymore</li>
  <li>*Not* in violation of AOL trademark</li>
</ul>

<h2>KDE Admin Package</h2>
<ul>
  <li>Redesigned user interface for KUser</li>
  <li>Configurable source files for NIS support resolving a bug
   caused by hardcoded filenames in KUser</li>
  <li>KDat has been removed due to lack of maintainership and user demand. Please notify if this application is still needed to
  make a separate package and provide that seperately.</li>
  <li>KCron has been generally improved</li>
</ul>

<h2>KDE Graphics Package</h2>

<h3>Kooka + libkscan (kdegraphics)</h3>
<ul>
  <li>KDE wide scanner support added</li>
  <li>Kooka - Scanning Application added</li>
</ul>

<h3>Pixie removed</h3>
<ul>
  <li>Due to Daniel M. Duley's stopping of active contribution inside the KDE Project, the graphics viewer
  Pixie has been removed and is now maintained by Daniel outside the KDE CVS and kdegraphics package. See <a href="http://www.mosfet.org/">www.mosfet.org</a>
  for updates on Pixie. </li>
</ul>

<h2>KDE PIM Package (KDE Personal Information Manager)</h2>

<h3>Kandy</h3>
<ul>
  <li>Kandy is a new application, which has been added to the kdepim
  module.</li>
  <li>Synchronizes phonebook data from mobile phones with the KDE
  address book.</li>
  <li>Provides a terminal application for communication with the
  mobile phone.</li>
  <li>Supports phones with GSM compatible modem</li>
</ul>

<h3>KOrganizer</h3>
<ul>
  <li>"What's next" view</li>
  <li>Journal feature</li>
  <li>Switch to iCalendar as default file format</li>
  <li>Active remote calendar</li>
  <li>Sending events via KMail</li>
  <li>Tip of the Day</li>
  <li>HTML export of month view</li>
  <li>Experimental project view (Gantt diagram)</li>
  <li>"Percent Completed" setting for todos</li>
  <li>Filter facility (select categories, hide recurring, hide completed)</li>
</ul>

<h3>KPilot</h3>
<ul>
  <li>Synchronization of address book</li>
</ul>

<h2>KDE Utilities Package</h2>

<h3>Kasbar</h3>
<ul>
  <li>Switched to new code base, Kasbar TNG</li>
  <li>Now uses the task manager API</li>
  <li>Startup notifier support</li>
  <li>Dynamically updated window thumbnails</li>
  <li>Drag-over task switching</li>
  <li>Added configuration and about dialogs</li>
  <li>Support for large, medium and small sizes</li>
  <li>Modified window indicator</li>
  <li>Better icon handling</li>
  <li>Support for grouping related tasks together</li>
</ul>

<h2>KDE Multimedia Package</h2>

<h3>Noatun (KDE Media Player)</h3>
<ul>
  <li>Equalizer Presets and support for preamplification</li>
  <li>An improved Plugin API</li>
  <li>Support for restarting aRts when required</li>
  <li>Hardware mixer support is now optionally available</li>
  <li>The KJofol skin loader is much more robust</li>
  <li>An improved plugin configuration page</li>
  <li>External visualizations allowing other applications to bind to noatun</li>
  <li>Tron playlist is aware of tag information</li>
  <li>Tron playlist can remember the volume level for files</li>
  <li>Tron playlist has Add Directory action</li>
  <li>Standard interface (Excellent) now remembers its state and toolbars</li>
  <li>see the KDE Addons module for more plugins.</li>
</ul>

<h2>KDE Games Package</h2>
<ul>
  <li>Two new games added: KBattleship and KLines</li>
  <li>Most games share a common highscore mechanism - KHighscore. This leads to
  further enhancements in several games.</li>
  <li>KMines even provides world wide highscores at <a href="http://kmines.sourceforge.net">http://kmines.sourceforge.net</a></li>
  <li>Most games use the default KDE shortcuts and menu entries using
  KAction/KStdAction/KStdGameAction</li>
  <li>The game menu has been split into categories</li>
  <li>A new set of cards for the card games</li>
  <li>Some new classes for libkdegames added</li>
  <li>Bugfixes</li>
</ul>

<h2>KDE Bindings</h2>
kdebindings provides 'Koala', a full set of Qt and KDE Java api bindings:
<ul>
  <li>JNI based wrappers for over 700 Qt and KDE classes</li>
  <li>Full Java support for Qt features such as slots/signals, subclassing event
handlers, and event filtering.</li>
  <li>KDE Java apps look and behave identically to their C++ equivalents</li>
</ul>

<h2>KDE Addons module</h2>
<ul>
  <li>kdeaddons is a new module in KDE's CVS that collects plugins and parts for various KDE applications</li>
  <li>Kicker applets:
        <ul>
          <li>kolourpicker is an applet that lets the user choose a color </li>
          <li>ktimemon is a system monitor applet that was formerly in the kdebase package</li>
        </ul></li>
  <li>Konqueror plugins/parts:
        <ul>
          <li>Babelfish plugin: translates a webpage via AltaVista's Babelfish. Places itself into Konqueror's toolbar and allows the selection of the destination and target language</li>
          <li>Directory filter: shows a filter for the mime-types of the current directory displayed in Konqueror, so the user can e.g. filter the current view for all images</li>
          <li>DOM Treeviewer: a plugin that displays the DOM tree of a webpage</li>
          <li>KHTML Settings plugin: makes the settings for configuring the KHTML part of Konqueror easier as it offers enabling/disabling things like Java or Javascript available in a popup menu in the toolbar</li>
          <li>Imagegallery plugin: creates an imagegallery HTML page in any folder with images. Large collections of images can be easily handled, especially if the gallery is named index.html and
                Konqueror prefers displaying an index.html file over the directory contents.</li>
          <li>User Agent changer: provides the same functionality as the user agent settings in Konqueror's configuration but with a simple popup menu, so the user can change
                the user agent setting on a webpage immediately.</li>
          <li>HTML Validator: checks the current webpage for correct usage of HTML through W3C check</li>
          <li>Webarchiver: Archives webpages on demand in a .war file that can be used later in offline mode. It simply not only saves the HTML page but also all the images on the page as well as other contents.</li>
        </ul></li>
  <li>Noatun (KDE Media Player) plugins
        <ul>
          <li>Alarm plugin: plays the music at a certain time</li>
          <li>Blurscope plugin: A blurring SDL-based monoscope</li>
          <li>Luckytag plugin: Guesses titles based on filename</li>
          <li>Noatun Madness: moves your windows in the rythm of the music played by Noatun</li>
          <li>Synaescope: another impressive SDL base visualization, based on the program Synaesthesia</li>
          <li>Tippecanoe: a slightly different version of the Synaescope</li>
          <li>Tyler: A port of XMMS's Infinity</li>
        </ul></li>
  <li>Kate (the new editor in KDE based on Kwrite) plugins</li>
  <li>KNewsTicker scripts
        <ul>
                <li>fyensget.py - makes it possible to monitor news from the Danish news site &quot;>Fyens Stiftstidende&quot; with KNewsTicker.</li>
                <li>stock.pl - turns KNewsTicker into a realtime stock ticker.</li>
                <li>newsrss.pl - allows you to monitor arbitrary USENET newsgroups with KNewsTicker as it can scroll the most recent postings along your ticker with this script.</li>
        </ul></li>
</ul>

<h2>KDE Artwork module</h2>
<ul>
  <li>kdeartwork is a new module in KDE's CVS that provides extra artwork 
      like wallpapers, widget styles, window decorations, themes and more.</li>
  <li>low-color icons have been moved to kdeartwork</li>
</ul>

<h2>KDevelop Package</h2>
<ul>
  <li>version number is now 2.0</li>
  <li>KDevelop's user interface has been enhanced with the usage of the multiple document interface architecture,
  allowing to view several windows. Also several views for the same file can be used. </li>
  <li>the view mode can be switched between childview mode, toplevel mode and tabview mode</li>
  <li>a new configuration wizard (based on QWizard) replaces the old, messagebox based one.</li>
  <li>dockwidget usage has been extended to dock/undock the tabulator windows in the Treeview and Outputview</li>
  <li>new templates for implementing a KDE/Qt style library and KControl modules have been added.</li>
  <li>the kde-common/admin copy (admin.tar.gz) has been updated to the latest version in KDE CVS. Users of versions
  prior to 2.0 are advised to update their project's admin directory with that to support newer versions of autoconf and
  automake.</li>
  <li>KDevelop now uses the new tip of day dialog from kdelibs</li>
  <li>the user manual has been extended to match the new GUI layout plus adds a chapter for using Qt Designer with
  KDevelop projects</li>
</ul>

<h2>KDE SDK Package</h2>
<ul>
  <li>KSpy: a utility library to examine the internal state of a Qt/KDE application. Graphically displays all
        QObjects in use and alllows browsing their properties.</li>
</ul>
<!-- END CONTENT -->
<?php
  include "footer.inc";
?>
