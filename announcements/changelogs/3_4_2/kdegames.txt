2005-06-20 13:42 +0000 [r427382-427380]  lanius

	* libkdegames/highscore/kconfigrawbackend.cpp,
	  libkdegames/highscore/kexthighscore_internal.cpp,
	  klickety/Makefile.am,
	  libkdegames/highscore/kexthighscore_gui.cpp,
	  kfouleggs/Makefile.am, kmines/Makefile.am,
	  libkdegames/configure.in.in, ksirtet/ksirtet/Makefile.am,
	  libkdegames/highscore/kexthighscore_gui.h: fix the system wide
	  highscore system to work again fix bug #103116 (usernames are
	  converted to lowercase) fix bug #90680 (use the username from
	  kemailsettings as default instead of anonymous) BUG: 103116 BUG:
	  90680 BUG: 76437 BUG: 78341

	* kbounce/game.h, kbounce/main.cpp, kbounce/kbounce.cpp,
	  kbounce/Makefile.am, kbounce/game.cpp, kbounce/configure.in.in:
	  don't require arts in kbounce

2005-06-23 11:17 +0000 [r428165]  mutz

	* libkdegames/highscore/kconfigrawbackend.cpp,
	  libkdegames/highscore/kexthighscore_internal.cpp,
	  libkdegames/highscore/kfilelock.cpp,
	  libkdegames/highscore/kexthighscore_item.cpp,
	  libkdegames/highscore/kscoredialog.cpp,
	  libkdegames/highscore/kexthighscore.cpp,
	  libkdegames/highscore/kexthighscore_gui.cpp,
	  libkdegames/highscore/kexthighscore_tab.cpp: If you include
	  config.h, do it first and everywhere. Fix compilation for when
	  HIGHSCORE_DIRECTORY is undef'ed (for whatever reason, configure
	  didn't warn about it, so it's probably non-fatal).

2005-06-27 15:18 +0000 [r429401]  binner

	* kdegames.lsm: 3.4.2 preparations

2005-06-30 19:09 +0000 [r430289]  lanius

	* libkdegames/highscore/kexthighscore_internal.cpp: small fix

2005-06-30 19:15 +0000 [r430292-430291]  lanius

	* libkdegames/highscore/kexthighscore_internal.cpp: fix compilation
	  if HIGHSCORE_DIRECTORY is not set

	* libkdegames/highscore/kexthighscore_internal.cpp: fix compilation
	  if HIGHSCORE_DIRECTORY is not set

2005-07-17 00:49 +0000 [r435456]  weidendo

	* kenolaba/Board.cpp, kenolaba/Move.h: Backport fix for move
	  generator: do not ignore some moves when there are more than 84
	  moves possible.

