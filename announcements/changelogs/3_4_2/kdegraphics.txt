2005-05-28 11:57 +0000 [r419034]  aacid

	* kpdf/ui/toc.cpp: backport fix for 106323

2005-05-30 13:09 +0000 [r419720]  mlaurent

	* kpovmodeler/pmpart.cpp: Backport fix

2005-06-01 09:16 +0000 [r420740]  wgreven

	* kghostview/dscparse.h, kghostview/displayoptions.cpp,
	  kghostview/dscparse.cpp: Revert to GPL'ed DSC parser.

2005-06-02 17:25 +0000 [r421281]  aacid

	* kpdf/ui/pageview.cpp: backport fix for 106546

2005-06-02 17:59 +0000 [r421298]  aacid

	* kpdf/ui/toc.cpp: backport 103433 bug fix

2005-06-03 17:39 +0000 [r421674]  aacid

	* kpdf/ui/pageview.cpp: revert fix for 106546

2005-06-04 10:32 +0000 [r422025]  aacid

	* kpdf/ui/pageview.cpp: backport fix for 106767

2005-06-06 21:07 +0000 [r422908]  aacid

	* kpdf/part.h, kpdf/part.cpp: Backport fix bug 106771

2005-06-21 20:03 +0000 [r427776]  aacid

	* kpdf/shell/main.cpp, kpdf/shell/shell.cpp, kpdf/shell/shell.h,
	  kpdf/VERSION: This seems to fix completely #94851, let's commit
	  it to the stable branch to get more testers version++

2005-06-22 19:29 +0000 [r428029]  aacid

	* kpdf/part.h, kpdf/part.cpp: Backport of fix for comment #4 of
	  106771

2005-06-27 15:18 +0000 [r429401]  binner

	* kdegraphics.lsm: 3.4.2 preparations

2005-06-27 21:36 +0000 [r429495]  aacid

	* kpdf/ui/toc.cpp, kpdf/core/generator_pdf/generator_pdf.cpp:
	  Backport the fix tokoe did to make tackat's pdf work

2005-06-27 21:50 +0000 [r429504]  aacid

	* kpdf/ui/pageviewutils.cpp: Patch i got from Thomas Luebking
	  <thomas.luebking@web.de> to fix rendering for brainded styles
	  like baghira, tested it and woked, thanks :-)

2005-06-30 11:35 +0000 [r430165]  mueller

	* kiconedit/kiconconfig.h: compile

2005-07-04 09:23 +0000 [r431422]  kebekus

	* kdvi/dviwin_draw.cpp: adds temporary woraround for bug in
	  C-compiler that RedHat uses for Fedora Core 4

2005-07-04 21:54 +0000 [r431651]  aacid

	* kpdf/xpdf/xpdf/PDFDoc.cc: The same "fix" i've commited to trunk
	  for #98891

2005-07-04 22:25 +0000 [r431663]  aacid

	* kpdf/xpdf/xpdf/PDFDoc.cc: typo fix

2005-07-05 14:32 +0000 [r431905]  hasso

	* kpovmodeler/pmlibraryobjectsearch.cpp: Backport of i18n("Search")
	  usage audit. No new strings, "&Search" is already in the
	  kdelibs.po. CCMAIL: kde-et@linux.ee

2005-07-13 22:48 +0000 [r434394]  aacid

	* kpdf/xpdf/xpdf/Makefile.am: "Easy" fix for crash #109015 Better
	  solutions are accepted BUGS: 109015

2005-07-14 11:33 +0000 [r434500]  aacid

	* kpdf/configure.in.in, kpdf/xpdf/xpdf/Makefile.am: Only use the
	  option when the compilers supports it

2005-07-16 16:08 +0000 [r435336]  aacid

	* kpdf/xpdf/xpdf/GlobalParams.h,
	  kpdf/xpdf/splash/SplashFTFontFile.h,
	  kpdf/xpdf/fofi/FoFiTrueType.cc, kpdf/xpdf/fofi/FoFiTrueType.h,
	  kpdf/xpdf/xpdf/SplashOutputDev.cc, kpdf/xpdf/xpdf/Catalog.cc,
	  kpdf/xpdf/splash/SplashFontFile.cc, kpdf/xpdf/xpdf/GfxFont.cc,
	  kpdf/xpdf/xpdf/Catalog.h, kpdf/xpdf/splash/SplashFontFile.h,
	  kpdf/xpdf/xpdf/GfxFont.h, kpdf/xpdf/xpdf/Array.cc,
	  kpdf/xpdf/splash/SplashT1FontFile.cc, kpdf/xpdf/xpdf/Array.h,
	  kpdf/xpdf/splash/SplashT1FontFile.h,
	  kpdf/xpdf/xpdf/CharCodeToUnicode.h, kpdf/xpdf/xpdf/Makefile.am,
	  kpdf/xpdf/splash/SplashFTFontEngine.cc, kpdf/xpdf/xpdf/Stream.cc,
	  kpdf/xpdf/splash/SplashFTFontEngine.h,
	  kpdf/xpdf/fofi/FoFiType1C.h, kpdf/xpdf/xpdf/Stream.h,
	  kpdf/xpdf/splash/SplashFontEngine.cc,
	  kpdf/xpdf/splash/SplashFontEngine.h,
	  kpdf/xpdf/xpdf/PSOutputDev.cc,
	  kpdf/xpdf/splash/SplashFTFontFile.cc,
	  kpdf/xpdf/splash/SplashFTFont.cc, kpdf/xpdf/xpdf/GlobalParams.cc,
	  kpdf/xpdf/xpdf/PSOutputDev.h,
	  kpdf/xpdf/splash/SplashT1FontEngine.cc: sync trunk kpdf/xpdf to
	  KDE 3.4 branch, that means some rendering fixes. I know this is
	  maybe too close to the 3.4.2 release date, so blame dirk if it
	  fails, as he's the instigator of this commit

2005-07-16 16:22 +0000 [r435340]  aacid

	* kpdf/configure.in.in, kpdf/xpdf/xpdf/Makefile.am: Avoid automake
	  conditional as told in kde-commits list

2005-07-18 17:01 +0000 [r435922]  aacid

	* kpdf/ui/pagepainter.cpp, kpdf/ui/presentationwidget.cpp,
	  kpdf/ui/thumbnaillist.cpp, kpdf/conf/settings.kcfgc,
	  kpdf/ui/pageview.cpp, kpdf/ui/pageviewutils.cpp,
	  kpdf/core/document.cpp,
	  kpdf/core/generator_pdf/generator_pdf.cpp, kpdf/part.cpp:
	  Settings -> KpdfSettings

2005-07-19 10:05 +0000 [r436212]  mueller

	* kpdf/xpdf/xpdf/DCTStream.cc (added),
	  kpdf/xpdf/xpdf/FlateStream.cc (added), kpdf/xpdf/xpdf/DCTStream.h
	  (added), kpdf/xpdf/xpdf/FlateStream.h (added): unbreak
	  compilation

2005-07-19 12:23 +0000 [r436245]  dang

	* kolourpaint/ChangeLog: point to websvn, not webcvs

2005-07-19 12:27 +0000 [r436251]  dang

	* kolourpaint/kpmainwindow_tools.cpp: fix warning

2005-07-19 12:33 +0000 [r436256]  dang

	* kolourpaint/widgets/kptoolwidgetbase.cpp: Don't permit selection
	  of tool options with RMB - pass it to the toolbar for the toolba
	  r context menu. BACKPORT of trunk/.

2005-07-19 12:37 +0000 [r436260]  dang

	* kolourpaint/widgets/kptooltoolbar.cpp: Permit right clicks in the
	  empty part of the Tool Box (for the RMB Menu). BACKPORT of trunk/

2005-07-19 12:41 +0000 [r436265]  dang

	* kolourpaint/widgets/kpcolorsimilaritydialog.cpp: Ensure Color
	  Similarity maximum is 30, not 29 due to gcc4 double -> int cast
	  behaviour BACKPORT of trunk/

2005-07-19 12:49 +0000 [r436270]  dang

	* kolourpaint/tools/kptool.cpp, kolourpaint/tools/kptoolaction.h,
	  kolourpaint/tools/kptool.h, kolourpaint/tools/kptoolaction.cpp:
	  Tool Actions placed outside the Tool Box are no longer fixed in
	  size and resize with their toolbars - pass icon name string, not
	  QIconSet, to action ctor. BACKPORT of trunk/

2005-07-19 12:57 +0000 [r436275]  dang

	* kolourpaint/tools/kptool.cpp: Return and numpad 5 key now draw as
	  would be expected since Enter already does. BACKPORT of trunk/

2005-07-19 13:04 +0000 [r436283]  dang

	* kolourpaint/kpselection.cpp: Click on right half of text
	  character now positions text cursor on next text character
	  (instead of the one under the mouse) to be consistent with all
	  other text editors (really noticeable with big fonts). BACKPORT
	  of trunk/

2005-07-19 13:13 +0000 [r436292]  dang

	* kolourpaint/kpselection.cpp: QPainter::drawText() increases line
	  heights to >QFontMetrics::height() if you type Chinese characters
	  (!) so the cursor gets out of sync. To fix: hand craft
	  kpselection.cpp:drawTextLines() which draws one line at a time.
	  BACKPORT of trunk/

2005-07-19 13:20 +0000 [r436301]  dang

	* kolourpaint/scripts (removed): Remove scripts that shouldn't be
	  part of KDE releases.

2005-07-19 13:33 +0000 [r436309]  dang

	* kolourpaint/TODO (removed): should be in
	  branches/kolourpaint/control/ because it's independent of version

2005-07-19 14:03 +0000 [r436325]  dang

	* kolourpaint/BUGS: * "the 1.4.1_light release" -> "this version"
	  to reduce maintenance hassle. * Text Tool does antialias since
	  1.4_light * Adding unreliable keyboard drawing bug * Remove _kde3
	  backport info TODO: Figure out what to do with this BUGS file
	  pain in all branches. CCMAIL: dang@kde.org

2005-07-19 14:25 +0000 [r436338-436333]  dang

	* kolourpaint/NEWS: * Document 1.4.2_light * CVS -> SVN branch
	  names * Correct 1.4.1_light entry to reflect what actually got
	  into it * 1.4_light: Mark features recently backported to KDE 3.3
	  and 1.2_kde3 branches (text antialias, crop, InputMethod); add
	  Bug # from BUGS to text antialias entry * Remove extra line
	  between 1.4_light and 1.2 branch sections

	* kolourpaint/README, kolourpaint/VERSION,
	  kolourpaint/kolourpaint.cpp: Welcome to KolourPaint 1.4.2_light
	  (KDE 3.4.2).

