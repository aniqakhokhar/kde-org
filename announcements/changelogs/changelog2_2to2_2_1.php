<?php
  $page_title = "KDE 2.2 to 2.2.1 Changelog";
  $site_root = "../../";
  include "header.inc";
?>

<p>
This page tries to present as much as possible of the problem
corrections that occurred in KDE between the 2.2 and 2.2.1 releases.
The primary goals of the 2.2.1 release being more complete documentation
and translations, the amount of code change is quite minimal - only the
most critical bugs have been fixed, the rest of the development is
currently being done in the branch that will lead to the KDE 3.0 release.
</p>

<h3>General</h3>
<ul>
  <li>Many improvements to translations and documentation</li>
</ul>

<h3>kdelibs</h3>
<ul>
<li>arts: compile fixes, single threaded option.</li>
<li>KRFCDate: fix for eastern timezones.</li>
<li>KTempFile: fix for problem when fileExtension contained "X".</li>
<li>Improved checking for IPv6 support.</li>
<li>KPasswordEdit: slightly better in understanding non-ASCII characters.</li>
<li>KXMLGUI: fix for problem with reading xml file completely.</li>
<li>KFile: file preview fixes.</li>
<li>KHTML: Various html &amp; CSS fixes.</li>
<li>Improved large file support.</li>
<li>HTTP: Better support for https through proxy.</li>
<li>HTTP: Improved handling of HTTP-cookies.</li>
<li>HTTP: Fixed security problem with abuse of HTTP POST.</li>
<li>Optimisations to improve KDE startup time.</li>
<li>KDED: All tasks can now be disabled through config file.</li>
<li>KSpell: support for Lithuanian and Belarusian.</li>
<li>KSSL: OpenBSD fixes.</li>
</ul>

<h3>kdeaddons</h3>
<ul>
<li>User Agent plugin: various fixes.</li>
<li>Kate Plugins: made translatable.</li>
</ul>

<h3>kdeadmin</h3>
<ul>
<li>KPackage: various fixes.</li>
<li>Kwuftpd: various fixes.</li>
</ul>

<h3>kdeartwork</h3>
No changes

<h3>kdebase</h3>
<ul>
<li>Kate: corrected bug wrt last line.</li>
<li>Desktop Wallpaper: small fixes.</li>
<li>KControl: fixes for NetBSD &amp; OpenBSD info.</li>
<li>KDesktop: fix problem with icon positions on desktop.</li>
<li>KDesktop: fixed sever bug where renaming a file on the desktop would destroy it.</li>
<li>Kicker: mini-pager can be used to switch desktop during drag&amp;drop.</li>
<li>AudioCD: Vorbis fixes.</li>
<li>IMAP: Handle folders that contain # or ? in their names correctely.</li>
<li>SMB: Better error reporting.</li>
<li>Thumbnail io-slave: fixes.</li>
<li>klipper: changes in configuration file format.</li>
<li>Konqueror: compile fixes.</li>
<li>Konqueror:minor fixes.</li>
<li>Konsole: many fixes.</li>
<li>Konsole: added --noxft option to disable AA.</li>
<li>Konsole: added "tripple click" support.</li>
<li>KPersonalizer: Fixed bug that inadvertently turned "shade hover" on.
  (This makes the window roll up till only the title bar is left when the
   mouse is over the title bar for some time)</li>
<li>ScreenSavers: show screen saver in front of the desktop instead of behind it.</li>
<li>KSMServer: Provide error diagnostics for common KDE instalation problems.</li>
<li>KSysGuard: various fixes.</li>
<li>KWin B2 decration: fixes.</li>
<li>KWin: Fix for freeze with CDE-style alt-tab.</li>
<li>Netscape Plugin Scan: Don't crash on incompatible libs.</li>
</ul>

<h3>kdebindings</h3>
<ul>
<li>Many updates.</li>
</ul>

<h3>kdegames</h3>
<ul>
<li>KAtomic: Fixed various incorrect chemical names.</li>
</ul>

<h3>kdegraphics</h3>
<ul>
<li>kdvi: various fixes.</li>
<li>kgv: small fixes.</li>
<li>kview: various fixes.</li>
</ul>

<h3>kdemultimedia</h3>
<ul>
<li>Some compile fixes.</li>
<li>noaun: small fix.</li>
</ul>

<h3>kdenetwork</h3>
<ul>
<li>KMail: various fixes.</li>
<li>KNode: Fix 1e9 problem.</li>
<li>KPgp: various fixes.</li>
</ul>

<h3>KDEPIM</h3>
<ul>
<li>korganizer: small fix.</li>
<li>kpilot: fix syncing problems.</li>
</ul>

<h3>KDESDK</h3>
<ul>
<li>kbabel: small fix.</li>
</ul>

<h3>KDEToys</h3>
<ul>
<li>KScore: small fix.</li>
</ul>

<h3>KDEUtils</h3>
<ul>
<li>Kab: compile fix.</li>
<li>Kpm: fix for Linux 2.4.</li>
</ul>

<h3>KDdevelop</h3>
<ul>
<li>kdevelop: avoid khtml crash on restoring files also with KDE-2.2.0</li>
<li>kdevelop: gcc 3.0 compile fix.</li>
<li>kdevelop: Fix find dialog.</li>
<li>kdevelop: Various fixes.</li>
</ul>

<h3>KDoc</h3>
No changes.


<?php include "footer.inc" ?>
