<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.19.3 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.19.3";
?>

<style>
main {
	padding-top: 20px;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>Port task switchers to use standard sizes for everything else. <a href='https://commits.kde.org/kdeplasma-addons/4959a81b6db0aec857d081db8de0823fdf62e471'>Commit.</a> </li>
<li>Port task switchers to use units.iconSizes. <a href='https://commits.kde.org/kdeplasma-addons/c3aa0010099a20defe7f931e438280cb2e9dac4c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422547'>#422547</a></li>
</ul>


<h3><a name='ksshaskpass' href='https://commits.kde.org/ksshaskpass'>KSSHAskPass</a> </h3>
<ul id='ulksshaskpass' style='display: block'>
<li>Migrate away from singe quote identifiers as well. <a href='https://commits.kde.org/ksshaskpass/90d164f67febab7f5fc4b97099cf246ffeac31f6'>Commit.</a> </li>
<li>Bring back for saving usernames as well. <a href='https://commits.kde.org/ksshaskpass/b590b9dfeac75ef9f32ced476a294b00f918141a'>Commit.</a> </li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Make sure tablet coordinates take decorations into account. <a href='https://commits.kde.org/kwin/311094ad8be4c76df2a4655fe71e7085fa9c4e14'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423833'>#423833</a></li>
<li>Provide a mask for flag-type properties (window types). <a href='https://commits.kde.org/kwin/ad3d2f5acf1e66276a5b7946f70cbbe96100d141'>Commit.</a> </li>
<li>[kcm/kwinrules] Fix types property for NET::AllTypesMask. <a href='https://commits.kde.org/kwin/8b9472e0bfcff8cb7467ee3055282a133a808349'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423214'>#423214</a></li>
<li>[x11] Force FocusIn events for already focused windows. <a href='https://commits.kde.org/kwin/a0c4a8e766a2160213838daf6f71c7ae6c3705df'>Commit.</a> </li>
<li>[kcm/kwinrules] Fix detection of wmclass property. <a href='https://commits.kde.org/kwin/3bfc750a79f967a93612385def636dced1ed1337'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423138'>#423138</a></li>
<li>[x11] Hold a passive grab on buttons only when needed. <a href='https://commits.kde.org/kwin/ec5a0249e26acdf12677df535df3d3faa6a18b49'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394772'>#394772</a></li>
<li>KCM KWin Decoration : kcfgc File attribute should point to kcfg file. <a href='https://commits.kde.org/kwin/170741c499a13a8e9e71da54064cffeb899a31cf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423398'>#423398</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Translate kcfg files. <a href='https://commits.kde.org/plasma-desktop/f437fdee10ace7e27f0474c419554556fe960428'>Commit.</a> </li>
<li>Fixuifiles. <a href='https://commits.kde.org/plasma-desktop/01c922c5e3474cd074cb1d525ccb3a5ca66f2292'>Commit.</a> </li>
<li>Fix message extraction a bit. <a href='https://commits.kde.org/plasma-desktop/e3f15bf05956f4e3b16ad009986fd769d8119d39'>Commit.</a> </li>
<li>Extract from ui files as well. <a href='https://commits.kde.org/plasma-desktop/d2a39317b1419b5964af27c91fd64e2c204cbdb5'>Commit.</a> </li>
<li>Fix a further potential null pointer deref in file manager chooser. <a href='https://commits.kde.org/plasma-desktop/00424d0537ee32e64951ac99907ce3ac9902d1eb'>Commit.</a> </li>
<li>Fix a KCM crash when no file manager is installed. <a href='https://commits.kde.org/plasma-desktop/74753128180a36c9fba154914b3a2384025c4893'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422819'>#422819</a></li>
<li>Do not modify vector while iterating over it in range-based for loop. <a href='https://commits.kde.org/plasma-desktop/b460e5b14458e550dcee55c4f4bc5dc8014a2670'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423224'>#423224</a>. Fixes bug <a href='https://bugs.kde.org/423441'>#423441</a></li>
<li>KCM component chooser: use the category FileManager to filter filemanager apps. <a href='https://commits.kde.org/plasma-desktop/6fa8a6b1b4ec9cc1125e1bc96f51586e0a1c87ab'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423313'>#423313</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[wallpaper] Save the package name in configuration not a URL. <a href='https://commits.kde.org/plasma-workspace/d18d901b67e602b9345cd5a9beae232bd852c28a'>Commit.</a> </li>
<li>Theme shouldn't inherit. <a href='https://commits.kde.org/plasma-workspace/1a19f67c5f3bcac0196d2315efa61704f5ab54fa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423747'>#423747</a></li>
<li>[applet/systemtray] Load config defaults from main.xml. <a href='https://commits.kde.org/plasma-workspace/14695957a18af530d0b07026d0058b858264f3a3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423561'>#423561</a></li>
<li>[applets/systemtray] Fix context menu misplaced. <a href='https://commits.kde.org/plasma-workspace/35ae7712d40a937e6e03e88376e5a1436ebdf60e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421275'>#421275</a></li>
<li>[startkde] Block plasma-session exiting until startup sound completes. <a href='https://commits.kde.org/plasma-workspace/e76a1395a84307125033f46ac6efe3b1109be528'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422948'>#422948</a></li>
<li>[mediacontroller] Use half the samples for better performance with swrast. <a href='https://commits.kde.org/plasma-workspace/c62cc5a9642a9da893f28804a6e06496a1878521'>Commit.</a> </li>
<li>[applets/mediaplayer] Make size of the standalone plasmoid similar to that of the system tray. <a href='https://commits.kde.org/plasma-workspace/e9fa9bf8174c72b3d33afdbd4e969ff40a288551'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422736'>#422736</a></li>
<li>Fix broken ENV variables for detailed settings. <a href='https://commits.kde.org/plasma-workspace/edc64d04a1e569d7032c41e6ee0ebf59833c26f2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417070'>#417070</a>. Fixes bug <a href='https://bugs.kde.org/176650'>#176650</a>. See bug <a href='https://bugs.kde.org/327757'>#327757</a>. See bug <a href='https://bugs.kde.org/176650'>#176650</a></li>
<li>Set proper formFactors to faces. <a href='https://commits.kde.org/plasma-workspace/08babd516a07c0113e9d890def148ae6e89b1900'>Commit.</a> </li>
<li>Better propagate size hints. <a href='https://commits.kde.org/plasma-workspace/b7d7bfb0b88195e2b973b2ddc09eedf2810da88c'>Commit.</a> See bug <a href='https://bugs.kde.org/422669'>#422669</a>. See bug <a href='https://bugs.kde.org/422888'>#422888</a></li>
<li>Fix DBus service used in shutdown interface. <a href='https://commits.kde.org/plasma-workspace/fcf4aabe4209fa5a797953e36546394739f6fa75'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423391'>#423391</a></li>
</ul>


<h3><a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>Merge plasma/5.18. <a href='https://commits.kde.org/powerdevil/2f21446bb13347f9e6dd5ab4f70106a51081bbd0'>Commit.</a> </li>
<li>Watch DBus service right away to discard pending inhibitions reliably. <a href='https://commits.kde.org/powerdevil/d21102cc6c7a4db204a29f376ce5eb316ef57a6e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423131'>#423131</a></li>
<li>Fix compilation with Qt 5.15, this hit the time bomb too. <a href='https://commits.kde.org/powerdevil/0fa63b8685f82e5f626058dfc0f9461ae158599b'>Commit.</a> </li>
</ul>


</main>
<?php
	require('../aether/footer.php');
