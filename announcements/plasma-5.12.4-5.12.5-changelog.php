<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.12.5 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.12.5";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Improve display of Progress element. <a href='https://commits.kde.org/discover/ff6f7b86a94c25b0a322dd5ad1d86709d54a9cd5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393323'>#393323</a></li>
<li>More elegant syntax for qrc files in cmake. <a href='https://commits.kde.org/discover/e0d9bad8ee770a4354aa0592af7c793282446f5b'>Commit.</a> </li>
<li>Make sure we update the checkability of all delegates when one changes. <a href='https://commits.kde.org/discover/a905b42c8c56b878e694bbd409f9b4994d3477f7'>Commit.</a> </li>
<li>PK: Improve information on resources that extend others. <a href='https://commits.kde.org/discover/df726e253328a420697c3d0b7659cfb4f1c7ec6c'>Commit.</a> </li>
<li>Report 0% update progress when not progressing. <a href='https://commits.kde.org/discover/1b640feb28e8493915c9bf9256df38d0f6fa4e16'>Commit.</a> </li>
<li>Infer cancellability of the StandardBackendUpdater. <a href='https://commits.kde.org/discover/b3a85d494cf9283fbac4b65f6e179e11e4e46067'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390319'>#390319</a></li>
<li>Forward action enabled state into the button. <a href='https://commits.kde.org/discover/c690772105a1f3b662293742564051596851f271'>Commit.</a> See bug <a href='https://bugs.kde.org/390319'>#390319</a></li>
<li>Don't change the resources while updating. <a href='https://commits.kde.org/discover/be9c3a3363d4d159c2a67ffa442afd07ca0332f1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391262'>#391262</a></li>
<li>Remove unused variable. <a href='https://commits.kde.org/discover/74d08abdf58c10dd126dc66f36fca4d52fcff720'>Commit.</a> </li>
<li>Fix typo. <a href='https://commits.kde.org/discover/31d53bc9e857d90f87b240b548f1b2a1f8278cda'>Commit.</a> </li>
<li>Fix notifier button placement. <a href='https://commits.kde.org/discover/a0aab16bec8a9705463e7c5af3eab88df06c2264'>Commit.</a> </li>
<li>Fix warning on Qt 5.11. <a href='https://commits.kde.org/discover/afafea6f9084a963a20c0e7f134dc0884a88931d'>Commit.</a> </li>
<li>Remove wrongly commented code. <a href='https://commits.kde.org/discover/be32ae1e8b5abe82ff8015ccd6c9fd91ada0c1de'>Commit.</a> </li>
<li>Report when root categories change. <a href='https://commits.kde.org/discover/ad0ccff5a2e4123f093fd4ed72e8d8fa34ce3391'>Commit.</a> </li>
<li>Readability. <a href='https://commits.kde.org/discover/e576bbcd31084f8c5084b4cb1d98591878dd9f71'>Commit.</a> </li>
<li>Properly concatenate strings. <a href='https://commits.kde.org/discover/4ff40ea8d62c82304a51e2f225657eaf85aeba6c'>Commit.</a> </li>
<li>Fix crash when installing flatpak files. <a href='https://commits.kde.org/discover/f16a72064e7fbe9bc95e1784527fc9455f1342f5'>Commit.</a> </li>
<li>Remove unneeded dependency. <a href='https://commits.kde.org/discover/24cd117c1e67339abb46f34d74fed3f6c0e6f4d5'>Commit.</a> </li>
<li>Don't crash when cancelling flatpak jobs. <a href='https://commits.kde.org/discover/6ec2f2cf22285acd662e6afc772515f11a70168f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391941'>#391941</a></li>
<li>Remove unused imports. <a href='https://commits.kde.org/discover/938da4ecc3261bc3bfd0e51891a09f9f117a2c5c'>Commit.</a> </li>
<li>--debug. <a href='https://commits.kde.org/discover/dfe5cc4f6be52859013c012dfaa1822b5302c720'>Commit.</a> </li>
<li>Make sure we don't crash automatically. <a href='https://commits.kde.org/discover/d98f2c9551666f7c06eafc4c806a34d9b7c4b09e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/392082'>#392082</a></li>
<li>Fix sorting when the actual comparison matches. <a href='https://commits.kde.org/discover/96b62a4f8806e60c25106d1a2725525ac6c97214'>Commit.</a> </li>
<li>Fix warning in Qt 5.11. <a href='https://commits.kde.org/discover/c86a99d75b3b14bbb39642d1e0242e617bf043cf'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>Fix run-time warnings on quick-share. <a href='https://commits.kde.org/kdeplasma-addons/45a25f05ad3edf879b620073ec480ccd6a552814'>Commit.</a> </li>
<li>[mediaframe applet] Enable wrapping of fill mode description in config UI. <a href='https://commits.kde.org/kdeplasma-addons/b43a463230d0fe53bc6cce410cb7b66c019274ff'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393232'>#393232</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12307'>D12307</a></li>
<li>[quickshare applet] Fix sizing of complete color config page. <a href='https://commits.kde.org/kdeplasma-addons/934e0e2da77339763aa705c5ea826d20ff3c5b93'>Commit.</a> </li>
<li>[quickshare applet] Fix layout of elements in config page. <a href='https://commits.kde.org/kdeplasma-addons/fac0670754b4f07c8b43efc4fbf6f44fbdd8c80e'>Commit.</a> </li>
<li>[mediaframe applet] Fix defunct adding of new files/folder per DnD. <a href='https://commits.kde.org/kdeplasma-addons/7a0b14bf13137b925be3a35c50889b7fef0faf77'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12287'>D12287</a></li>
<li>[mediaframe applet] Fix broken selection of fill mode. <a href='https://commits.kde.org/kdeplasma-addons/faa26503b68f29d2ce4d0f4c25116a7904d8f8c9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12134'>D12134</a></li>
<li>[notes applet] Remove unused/unneeded import of private.notifications. <a href='https://commits.kde.org/kdeplasma-addons/653d63560c4afdadfd1e08efb8583de27bc8dd14'>Commit.</a> </li>
<li>[potd dataengine] Fix Wikimedia Picture of the Day provider. <a href='https://commits.kde.org/kdeplasma-addons/9eeba3dc85166cc377011a2c5822ef787619681b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11978'>D11978</a></li>
<li>[Media Frame] Fix config pages to fit themselves into dialog. <a href='https://commits.kde.org/kdeplasma-addons/d678a4c88473ecef8174d7c8e0a3f3cee03d2289'>Commit.</a> </li>
<li>[weather applet] Fix wind direction arrows to adapt to color theme. <a href='https://commits.kde.org/kdeplasma-addons/eed649e733982a62098c61252553767051e891e1'>Commit.</a> </li>
<li>[potd wallpaper] Add missing Messages.sh, use wallpaper catalog name. <a href='https://commits.kde.org/kdeplasma-addons/79377f8a206f372f6cd91b0f5b58aef1b01a34c9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11986'>D11986</a></li>
<li>[potd wallpaper] Config UI: fix spacing between UI elements. <a href='https://commits.kde.org/kdeplasma-addons/458f4387f8259bd62ba7aacaedb7381e2b7761dc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11983'>D11983</a></li>
<li>[potd dataengine] Do not leak pimpl object of PotdProvider class. <a href='https://commits.kde.org/kdeplasma-addons/2113b047da4527b1dc48832951a2c86c62e5cc96'>Commit.</a> </li>
</ul>


<h3><a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a> </h3>
<ul id='ulkinfocenter' style='display: block'>
<li>Kcm_opengl: Fix EGL info retrieval. <a href='https://commits.kde.org/kinfocenter/776386dc3a4fa53bd57c288ae29d2bb81566ba12'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12455'>D12455</a></li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Convert JS files to UTF-8 (from ISO-8859-15). <a href='https://commits.kde.org/kwin/86af84f13ece07600721070da7aa421cf4a06538'>Commit.</a> </li>
<li>[KScreen Effect] Fix fade to black. <a href='https://commits.kde.org/kwin/437a36a2bb86222f1c3d086c15a2f60eef898c1a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388384'>#388384</a>. Phabricator Code review <a href='https://phabricator.kde.org/D9608'>D9608</a></li>
<li>Reparse the input configuration when we are notified that it changed. <a href='https://commits.kde.org/kwin/6e84645e355a2556bebb3bce2d0b75823c402e35'>Commit.</a> </li>
<li>Don't try to filter null key combinations. <a href='https://commits.kde.org/kwin/bec8493459d5e15272a4fd07a84b35896b47ce69'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390110'>#390110</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12416'>D12416</a></li>
<li>Drm backend: choose correct EGL config with mesa-18. <a href='https://commits.kde.org/kwin/0ccecbc4275783e217d3c1ab1d3acd6988368757'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11758'>D11758</a></li>
<li>Ensure _NET_CURRENT_DESKTOP is set on startup. <a href='https://commits.kde.org/kwin/4205496033728c82643e78340b4ddae3079c6280'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391034'>#391034</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10836'>D10836</a></li>
</ul>


<h3><a name='libksysguard' href='https://commits.kde.org/libksysguard'>libksysguard</a> </h3>
<ul id='ullibksysguard' style='display: block'>
<li>[ProcessModel] Return invalid QVariant() for when window is not known. <a href='https://commits.kde.org/libksysguard/e3ea3c840d6f7f0b7e23490c766045f093361ada'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12000'>D12000</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>[Desktop Theme KCM] Workaround bug in FileDialog. <a href='https://commits.kde.org/plasma-desktop/c5c756623a3495183f5b3390520ee599dd0f43c1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12495'>D12495</a></li>
<li>FunnelModel: fix invalid model API usage. <a href='https://commits.kde.org/plasma-desktop/38403268aad327f658616feeecfa78f305471e2b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12275'>D12275</a></li>
<li>[Window List] Show pin icon only when in panel. <a href='https://commits.kde.org/plasma-desktop/eaa5b4364f8bd59869a947f1bcf3edd6a06d01c0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391145'>#391145</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12146'>D12146</a></li>
<li>Make automounting work even if StorageAccess is ignored. <a href='https://commits.kde.org/plasma-desktop/2da93ae0a3099021646e4a743fca3ca47ee36376'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389479'>#389479</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12050'>D12050</a></li>
<li>[Folder View] Set shortcut on plasmoid.action. <a href='https://commits.kde.org/plasma-desktop/94aee349c346270093d93a060b14e8486f7590b1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/392730'>#392730</a>. Phabricator Code review <a href='https://phabricator.kde.org/D11953'>D11953</a></li>
<li>[Folder View] Use KStandardAction for rename, delete, and trash. <a href='https://commits.kde.org/plasma-desktop/eab41217209bda4e5890e54ce30ce3f2598d5fb6'>Commit.</a> See bug <a href='https://bugs.kde.org/392730'>#392730</a>. Phabricator Code review <a href='https://phabricator.kde.org/D11947'>D11947</a></li>
<li>[Folder View] Simplify clearing of lists. <a href='https://commits.kde.org/plasma-desktop/dadb306d42998e65793fdf02c75d218024533011'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11946'>D11946</a></li>
<li>[Folder View] Update "Empty Trash" enabled state for files inside Trash and links to Trash. <a href='https://commits.kde.org/plasma-desktop/7e98c5d737eb953c8a2692b7eeae1b4cb0f32e5f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/392738'>#392738</a>. Phabricator Code review <a href='https://phabricator.kde.org/D11944'>D11944</a></li>
<li>[System Model] Support actions. <a href='https://commits.kde.org/plasma-desktop/bf12647c60024545e367b160e13a7d1618c3acc1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11927'>D11927</a></li>
<li>Do clamping in the model and add missing lower bound check. <a href='https://commits.kde.org/plasma-desktop/20bcc8f0501bb3cd368b03797c0bdea54de9f84b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390888'>#390888</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10736'>D10736</a></li>
</ul>


<h3><a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a> </h3>
<ul id='ulplasma-integration' style='display: block'>
<li>KDEPlatformFileDialog: Fix initial directory selection for remote files. <a href='https://commits.kde.org/plasma-integration/cc064e81c6ed51f3b4422c2f2347e5f4e090e628'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374913'>#374913</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4193'>D4193</a></li>
</ul>


<h3><a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>AbstractModel: Check if index is valid in setData. <a href='https://commits.kde.org/plasma-pa/0284a425f3db86f02d66430f77202ac44e8b00af'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11807'>D11807</a></li>
</ul>


<h3><a name='plasma-vault' href='https://commits.kde.org/plasma-vault'>plasma-vault</a> </h3>
<ul id='ulplasma-vault' style='display: block'>
<li>Add missing includes to fix compilation with Qt 5.11. <a href='https://commits.kde.org/plasma-vault/4ace32df8345f3a4b64837f242543d14e504b85c'>Commit.</a> </li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[ksmserver] Use QUrl::fromUserInput to construct sound url. <a href='https://commits.kde.org/plasma-workspace/608204765110724f420c978499411e46db1d086e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/392725'>#392725</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12606'>D12606</a></li>
<li>[Image Wallpaper] Fix auto transform for blurred fill. <a href='https://commits.kde.org/plasma-workspace/90e0a7b16529c30631f11d0fc5d8474175345d6c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393498'>#393498</a></li>
<li>Sddm-theme: Focus the password field if enter pressed in the username field. <a href='https://commits.kde.org/plasma-workspace/1456549026633384747805c975511a4517a96e41'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12510'>D12510</a></li>
<li>[Notifications] Always scroll to the top when opening. <a href='https://commits.kde.org/plasma-workspace/19332567dce613b0d6ad59497a614d397c31ba91'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391646'>#391646</a>. Phabricator Code review <a href='https://phabricator.kde.org/D11989'>D11989</a></li>
<li>[ContextMenu Containment Action] Fix checking for KIOSK. <a href='https://commits.kde.org/plasma-workspace/cfd77db90cf604e2f30afa5a0a1466cddfeeb9ac'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393329'>#393329</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12376'>D12376</a></li>
<li>[Power Management Engine] Fix kiosk restriction for lockscreen. <a href='https://commits.kde.org/plasma-workspace/4932c6e2cbc949b6566d0672dfd1ba15ba944d2d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393331'>#393331</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12375'>D12375</a></li>
<li>[weather dataengine] envcan: fix forecast terms to match "ice pellets". <a href='https://commits.kde.org/plasma-workspace/c5414f0d31f9c6dff783542ec6ed56960b629372'>Commit.</a> </li>
<li>[weather dataengine] bbc: handle and ignore condition "Not Available". <a href='https://commits.kde.org/plasma-workspace/39593a337e546887d4c505194c6a9f4080b9fab4'>Commit.</a> </li>
<li>[weather dataengine] bbc: handle and skip visbility "--". <a href='https://commits.kde.org/plasma-workspace/96150f5f37f879c01bf05a5f13c9d4cf099ed084'>Commit.</a> </li>
<li>[OSD] Enforce plain text. <a href='https://commits.kde.org/plasma-workspace/d5a98be7b64238116767a3bddf14935314aad2ef'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12086'>D12086</a></li>
<li>[weather] BBCUKMET: add missing "thundery showers". <a href='https://commits.kde.org/plasma-workspace/96cc6a68a05bde5b203d65bc1272251a741d4dd0'>Commit.</a> </li>
<li>[weather] BBCUKMET: add missing "light rain showers"/"heavy rain showers". <a href='https://commits.kde.org/plasma-workspace/715870b35227ceac9dc3f180e429d7d105fd7bcf'>Commit.</a> </li>
<li>[weather dataengine] Fix BBC provider to adapt to change RSS feed. <a href='https://commits.kde.org/plasma-workspace/558a29efc4c9f055799d23ee6c75464e24489e5a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/392510'>#392510</a>. Phabricator Code review <a href='https://phabricator.kde.org/D11808'>D11808</a></li>
<li>KDE logout screen background color fix. <a href='https://commits.kde.org/plasma-workspace/fc0033ede6b0bb88d737752b8db359194cb4e41f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382264'>#382264</a>. Phabricator Code review <a href='https://phabricator.kde.org/D11262'>D11262</a></li>
<li>Fix: Klipper notifications visually broken since plasma 5.12. <a href='https://commits.kde.org/plasma-workspace/896950a819ea76800007cad7393adacac7f9bf20'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390375'>#390375</a>. Phabricator Code review <a href='https://phabricator.kde.org/D11754'>D11754</a></li>
<li>[notifications applet] Fix two qml warnings about assigning [undefined]. <a href='https://commits.kde.org/plasma-workspace/5d65fcbf8bf95cb1a814b81aa26d5b477a8549db'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11743'>D11743</a></li>
</ul>


<h3><a name='polkit-kde-agent-1' href='https://commits.kde.org/polkit-kde-agent-1'>polkit-kde-agent-1</a> </h3>
<ul id='ulpolkit-kde-agent-1' style='display: block'>
<li>Add a window icon for authentication dialog under Wayland. <a href='https://commits.kde.org/polkit-kde-agent-1/92b6a3459d4e5ea327e90f5f673594a0472ddb7d'>Commit.</a> </li>
<li>Remove traces of polkit-kde-authorization tool. <a href='https://commits.kde.org/polkit-kde-agent-1/3791346b2843c2cd7f178b3a05056c6ac988c005'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11950'>D11950</a></li>
</ul>


<h3><a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>Fixed bug that caused power management system to not work on a fresh install. <a href='https://commits.kde.org/powerdevil/be91abe7fc8cc731b57bec4cf2c004c07b0fd79b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391782'>#391782</a></li>
</ul>


<h3><a name='sddm-kcm' href='https://commits.kde.org/sddm-kcm'>SDDM KCM</a> </h3>
<ul id='ulsddm-kcm' style='display: block'>
<li>Also read sddm.conf.d config directories. <a href='https://commits.kde.org/sddm-kcm/c879e4fa438f4cb029f1e73784489869b0d48314'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12359'>D12359</a></li>
<li>Use XCursorTheme on wayland as well. <a href='https://commits.kde.org/sddm-kcm/4af192630606a14a6850fcb6a50c709508d7ce05'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12361'>D12361</a></li>
<li>Read theme.conf of themes. <a href='https://commits.kde.org/sddm-kcm/ebdfab7e08671b5165f52118cabdf4d1d6c14bb2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12323'>D12323</a></li>
</ul>


<h3><a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a> </h3>
<ul id='ulsystemsettings' style='display: block'>
<li>Polish sidebar header appearance. <a href='https://commits.kde.org/systemsettings/5de538d0df406973f130b3d1aa3c2555effbe59c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/392340'>#392340</a>. Phabricator Code review <a href='https://phabricator.kde.org/D11757'>D11757</a></li>
<li>Use font.weight instead of font.bold. <a href='https://commits.kde.org/systemsettings/541598fe398a5b21987af63c4d73a7725fe13633'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11764'>D11764</a></li>
</ul>


</main>
<?php
	require('../aether/footer.php');
