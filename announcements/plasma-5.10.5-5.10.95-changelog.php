<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.10.95 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.10.95";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='bluedevil' href='https://commits.kde.org/bluedevil'>Bluedevil</a> </h3>
<ul id='ulbluedevil' style='display: block'>
<li>Fix spdx licence for appstream happyness. <a href='https://commits.kde.org/bluedevil/a39206f5adcba51678acbef5b56032c838e0e302'>Commit.</a> </li>
<li>Fix spdx licence for appstream happyness. <a href='https://commits.kde.org/bluedevil/66a95fb6c17c0e7141919792d1de472c349f398d'>Commit.</a> </li>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/bluedevil/7d28f59d5e5e1ae15f80d5456fe4cf8a2bbf663e'>Commit.</a> </li>
</ul>


<h3><a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>Renamed breeze specific KCMs. <a href='https://commits.kde.org/breeze/b4b0c8af3d910dd619e8f35eec8168c0f410dd01'>Commit.</a> See bug <a href='https://bugs.kde.org/383283'>#383283</a></li>
<li>Moved inclusion of QQuickWindow and QQuickItem outside of X11 block. <a href='https://commits.kde.org/breeze/148c4c65af5dd309a58f6f74f74613d9d92409b6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377209'>#377209</a></li>
<li>Partial update to c++11. <a href='https://commits.kde.org/breeze/8a61b4184fffa527ff2bc222132ae4f9b070c3a4'>Commit.</a> </li>
<li>Properly change buttons hit area to enforce Fitts law when windows are quick-tiled and or vertically/horizontally maximiezed. At the same time, do not draw the window round corners. <a href='https://commits.kde.org/breeze/629b0698535a7210a49ab2825b44e156e1eae8a5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7355'>D7355</a></li>
<li>Use std::function. <a href='https://commits.kde.org/breeze/76a9241f309ce88173b39eb7d7f5d37e5a056354'>Commit.</a> </li>
<li>Adjustments to the wallpaper based on community feedback. <a href='https://commits.kde.org/breeze/a0fe0817e55c7a513a2ef25a38b991ed40669b78'>Commit.</a> </li>
<li>Fix licence spdx http://metadata.neon.kde.org/appstream/html/xenial/main/issues/plasma-look-and-feel-org-kde-breezedark-desktop.html. <a href='https://commits.kde.org/breeze/3900eaf19044c197b8615320a3590bb436d7fea0'>Commit.</a> </li>
<li>Removed unnecessary 1pixel margin around menubaritem focus/hover rect. <a href='https://commits.kde.org/breeze/7de150a993ddb6e5026007d0cebae5e139b217cb'>Commit.</a> </li>
<li>Added missing "break" statement. <a href='https://commits.kde.org/breeze/9bf4f840ae8cad1f01ed7b96ebb283aa60d0447e'>Commit.</a> </li>
<li>Plasma 5.11 "Opal" wallpaper. <a href='https://commits.kde.org/breeze/d46ecf71fc69fcdd52907832ad63ab007c24c7ac'>Commit.</a> </li>
<li>[Breeze Style] Fix flickering during KPageTreeView animation. <a href='https://commits.kde.org/breeze/09e09a9cd88535a28b7ecbf7201bc0ecc17d6869'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7089'>D7089</a></li>
<li>Fix app icon button in window decos with high DPI in previews. <a href='https://commits.kde.org/breeze/e827fa0801370b9241f6456d228d4db268482e7c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6763'>D6763</a></li>
<li>[kstyle] Do not delete the Surface for a QWindow. <a href='https://commits.kde.org/breeze/e02fef0883af3d3b33ce8a9ea8677c82c1973975'>Commit.</a> </li>
<li>[kstyle] Change ownership of Wayland objects so that KWin can delete them. <a href='https://commits.kde.org/breeze/5328a253ccdccadd94ac1f18ed7540547ee7493d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6572'>D6572</a></li>
<li>Optimize if condition by checking state first and then using qobject_cast. <a href='https://commits.kde.org/breeze/6906919371ff92ff0dc7f6ae4e4e14310c5e1e53'>Commit.</a> </li>
<li>Don't draw focus line on selected list item. <a href='https://commits.kde.org/breeze/89b91603938738a65f2ebc17d8c661346daa587f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6544'>D6544</a></li>
<li>Added some comments. <a href='https://commits.kde.org/breeze/af04e80c7b15d130b08613b5a10b84b627e6e2f1'>Commit.</a> </li>
<li>- hide shadow when mask is empty. <a href='https://commits.kde.org/breeze/da757f6683a2bd6cae348a83921ffa318acec569'>Commit.</a> See bug <a href='https://bugs.kde.org/379790'>#379790</a></li>
<li>Cosmetics. <a href='https://commits.kde.org/breeze/2012d71b8544dbef4284e7d73f4f827f2891db19'>Commit.</a> </li>
<li>Fixed compilation when KWayland is not there. <a href='https://commits.kde.org/breeze/7db2aa9fcddd7d06c48d392bff05c1033d8241e3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5910'>D5910</a></li>
<li>Use fontMetrics.height() rather than boundingRect(...).height(). <a href='https://commits.kde.org/breeze/2cc0f8ba2da50ca3efa500ebdcc3655c8d0e47f8'>Commit.</a> See bug <a href='https://bugs.kde.org/380391'>#380391</a></li>
<li>Added some 'auto'. <a href='https://commits.kde.org/breeze/1fe4c5edf86fee5eba466e6287703f95ecee19c2'>Commit.</a> </li>
<li>Make shadows work on wayland. <a href='https://commits.kde.org/breeze/d4940fe692c7be10025bfcb6a118c2cb750039d6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5910'>D5910</a></li>
<li>Q_DECL_OVERRIDE -> override. <a href='https://commits.kde.org/breeze/610b6f13c31eb593d9dbe4893ec29e5267c04aaf'>Commit.</a> </li>
<li>Partially revert "Use Q_DECL_OVERRIDE". <a href='https://commits.kde.org/breeze/30245abae856a476025eaa2c3f16aafe219919be'>Commit.</a> </li>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/breeze/13859b08f7e86557010587b10c1480231b902e99'>Commit.</a> </li>
<li>Set a mask to shadow widget to make sure that it does not overlap with the mdi window. <a href='https://commits.kde.org/breeze/760cc2b71d544c8084f278bc2d6eb7cfa576b864'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379790'>#379790</a></li>
<li>- 0L -> nullptr. <a href='https://commits.kde.org/breeze/afcd633575f6fa357c9640586338455a9150570b'>Commit.</a> </li>
<li>0L -> nullptr. <a href='https://commits.kde.org/breeze/7841dedf34280bf1a85f3ed8d3614ac1a73c0c5a'>Commit.</a> </li>
<li>Do not draw focus indicator for checkable groupbox labels: it is drawn already by the checkbox. <a href='https://commits.kde.org/breeze/7f6c07742a0ed3ee82b179cb9a181f37e9120173'>Commit.</a> </li>
</ul>


<h3><a name='breeze-gtk' href='https://commits.kde.org/breeze-gtk'>Breeze GTK</a> </h3>
<ul id='ulbreeze-gtk' style='display: block'>
<li>Adjusted Scrollbars so now they fit the Qt theme. <a href='https://commits.kde.org/breeze-gtk/01a86601804222929441c0c1c8bb0db6d4ee2769'>Commit.</a> </li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Include a fetchChangelog test. <a href='https://commits.kde.org/discover/ad78c785c226d865b0ad9435e298094d5b6e80f8'>Commit.</a> </li>
<li>Make methods const when possible. <a href='https://commits.kde.org/discover/9a1a124e2576669a0a641524b4079d02c77a5e76'>Commit.</a> </li>
<li>Don't make requests if we didn't manage to connect. <a href='https://commits.kde.org/discover/8280fa0c802b6f7ed540fa9aa266d21fd26e7aaf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7785'>D7785</a></li>
<li>Fix display of some flatpakref files. <a href='https://commits.kde.org/discover/fd16396d44bcf8ad1dee3081f16db5a454ac0808'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/384455'>#384455</a></li>
<li>Make sure we don't call m_updates if it's null. <a href='https://commits.kde.org/discover/fe4216245d9575db44e5dbaf97313541ef9d2c50'>Commit.</a> </li>
<li>Fix tests. <a href='https://commits.kde.org/discover/73d460c22e86d3111c2625cee2c809d4beb226bd'>Commit.</a> </li>
<li>Improve progress bar in some cases. <a href='https://commits.kde.org/discover/b092b3b60b6c3175bb0901130b6493c0166c8cd5'>Commit.</a> See bug <a href='https://bugs.kde.org/383473'>#383473</a></li>
<li>Do not hardcode path to appstream header. <a href='https://commits.kde.org/discover/c6d064634f0f41fe4f30b6496e764600ee8ea7af'>Commit.</a> </li>
<li>Initially populate the installed snaps. <a href='https://commits.kde.org/discover/864b8152cf908cf5a915044bc6e6217c88aa9bca'>Commit.</a> </li>
<li>Install required runtime for flatpakref if it's specified. <a href='https://commits.kde.org/discover/224012fabcf2cf61ec2bc6422bf09f1c35bb1323'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7201'>D7201</a></li>
<li>Search in all app backends in parallel rather than just the preferred. <a href='https://commits.kde.org/discover/0230adc59593ed1e0ada6ad23e2c4a4148bfd5b7'>Commit.</a> </li>
<li>Show KNS issues as passive notifications. <a href='https://commits.kde.org/discover/e423e9237a010f71458f96696ae601c330f4febe'>Commit.</a> </li>
<li>--debug. <a href='https://commits.kde.org/discover/b082360c7dbd6ff13db38ed34068303a9e71e015'>Commit.</a> </li>
<li>Do not refresh appstream metadata everytime. <a href='https://commits.kde.org/discover/64ad5fc8d6c70bd4fdaf8b3b0d26278243faf213'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7177'>D7177</a></li>
<li>Fallback for cached icons. <a href='https://commits.kde.org/discover/852973152c6cd0d206236f244f013cba33866958'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7178'>D7178</a></li>
<li>Remove boilerplate code. <a href='https://commits.kde.org/discover/e567c34669dff215d9645d139411c5dda63fa4a2'>Commit.</a> </li>
<li>Initialize attribute. <a href='https://commits.kde.org/discover/c30ff4510574f8fbee7d23f34f2e104cc8704110'>Commit.</a> </li>
<li>Refresh the FeaturedModel when the backend stops fetching. <a href='https://commits.kde.org/discover/b3ffe6027a0235d734486dfb00678ee66a799571'>Commit.</a> </li>
<li>Sometimes flatpak doesn't seem to return an error despite failing. <a href='https://commits.kde.org/discover/204f16622288fc1bce9f0e94fbb806f2c6bf184d'>Commit.</a> </li>
<li>FlapakBackend: refresh appstream metadata before listing apps. <a href='https://commits.kde.org/discover/276eaa650ebf698700884c7d8a27ee3edce949dc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383092'>#383092</a></li>
<li>Fix warning. <a href='https://commits.kde.org/discover/df6ec024e7c0e8b87a9b4765b5f7cb211445d9a8'>Commit.</a> </li>
<li>Make it possible to pass remote flatpakref and flatpakrepo files. <a href='https://commits.kde.org/discover/4b755e941fba2303a648e1283a5086dc021d19ba'>Commit.</a> </li>
<li>Implement adding flatpak repositories from the sources page. <a href='https://commits.kde.org/discover/36095ea676111de72e006cec81be07a7b2b14dca'>Commit.</a> </li>
<li>Pass a string to grab the error message. <a href='https://commits.kde.org/discover/3a718124d45d60c49bb586e14d348f233178b34b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382916'>#382916</a></li>
<li>Add 'and' to category names where it was missing. <a href='https://commits.kde.org/discover/3592fedaf9f1425eb4a89398b50040039dd0bffc'>Commit.</a> </li>
<li>Automatically install/remove/update related refs. <a href='https://commits.kde.org/discover/a9a018589de58115124e60abd72a048cd34ef14e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6677'>D6677</a></li>
<li>Last merge was broken. <a href='https://commits.kde.org/discover/f440cf983b1adf852ec5cdf5604c86ccd2ccb3cc'>Commit.</a> </li>
<li>Don't change the title of the Installed section. <a href='https://commits.kde.org/discover/ce54589cc393410dfd42fdb6c4c4b9b309d8c0e7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378565'>#378565</a></li>
<li>Make sure the UpdateModel is initialized after switching the view. <a href='https://commits.kde.org/discover/33000d5409e5f6fc17e28776558de5974db77ee5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382192'>#382192</a></li>
<li>Remove unused code. <a href='https://commits.kde.org/discover/65cbbbe6c9eca6644b8db34cd5a7f60ae0313050'>Commit.</a> </li>
<li>Don't show the drawer immediately when on compact mode. <a href='https://commits.kde.org/discover/53651467b98af75e1929ff33325c6a176dcd9c96'>Commit.</a> </li>
<li>Don't reset the menu unless there's something to change. <a href='https://commits.kde.org/discover/218aff4597912e516fe0c066b264ed0829a99951'>Commit.</a> </li>
<li>Remove unused file. <a href='https://commits.kde.org/discover/dd49191c6f0b6cdd54475e831273a7f670ed9855'>Commit.</a> </li>
<li>Use "reviews" rather than "comments" for consistency. <a href='https://commits.kde.org/discover/e3eb4dd77df2542fc24b0e2b492712ee7b795543'>Commit.</a> See bug <a href='https://bugs.kde.org/380514'>#380514</a></li>
<li>Don't override paddings. <a href='https://commits.kde.org/discover/c73a0f5ee54399afff369e4240cf00d5897aef51'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6610'>D6610</a></li>
<li>Fix qml module versioning. <a href='https://commits.kde.org/discover/ed6f66ec001a14ca05226246d7b5055e9897c946'>Commit.</a> </li>
<li>Don't show the progress view if there's nothing to show. <a href='https://commits.kde.org/discover/cc28a65b6335d120ccc751172fb33d3af58c43fe'>Commit.</a> </li>
<li>Use the right API. <a href='https://commits.kde.org/discover/c9c3b71245d33a98fca603c7fe7e4385d7516eae'>Commit.</a> </li>
<li>Forgot to commit AppStreamUtils.h. <a href='https://commits.kde.org/discover/43573ccded06615bf898a6b8d2d7bf025fd5c418'>Commit.</a> </li>
<li>Make sure we never use the local plugin instead. <a href='https://commits.kde.org/discover/a1ddd8aa24282d964d52de4b71d88d6d7be4ca56'>Commit.</a> </li>
<li>Flatpak: Reduce path puzzles and duplicated code. <a href='https://commits.kde.org/discover/5cbcb4ca31d67c15acb1d0a98ac1c178c5946b55'>Commit.</a> </li>
<li>Set icon's size so the proper size is chosen. <a href='https://commits.kde.org/discover/bc937a1868d0be2c6663afa2a648410416a33e4c'>Commit.</a> </li>
<li>Share appstream screenshots code. <a href='https://commits.kde.org/discover/47d6d14364ec4fd1f7bbe09726b240f638e564a1'>Commit.</a> </li>
<li>Properly convert from QString to char*. <a href='https://commits.kde.org/discover/b53ad168a589575ea5f2f7a7ee87353f7cea6d13'>Commit.</a> </li>
<li>Use correct path where appstream metadata are stored. <a href='https://commits.kde.org/discover/88c9ac30067a39ccc14b432335aa9057b5de6d34'>Commit.</a> </li>
<li>Call QCLP parse rather than process, as documented in KDBusService. <a href='https://commits.kde.org/discover/3114021e572e63004bce2cc2e453d93b40f68ce8'>Commit.</a> </li>
<li>Fix build. <a href='https://commits.kde.org/discover/9865e89083e711012b51be65f7eb2eda7f840b3d'>Commit.</a> </li>
<li>Fix test. <a href='https://commits.kde.org/discover/b9a1051c4192a2eefd2179a5671a2616bbe60e88'>Commit.</a> </li>
<li>Improve the macaroon dialog error reporting. <a href='https://commits.kde.org/discover/b748b7b29193e3ba9a22cd1b758aad70a4646372'>Commit.</a> </li>
<li>Remove unused import. <a href='https://commits.kde.org/discover/11734f14ea0fef87851d55a3e309d95d2fe79596'>Commit.</a> </li>
<li>Fix on older Qt versions. <a href='https://commits.kde.org/discover/228b216e5f681553c67f2df913f173a93ca862d9'>Commit.</a> </li>
<li>Make sure the plugin is available before setting up the view. <a href='https://commits.kde.org/discover/cbe05ff60be3a2d17f53a83d6eb9969b7bbf445a'>Commit.</a> </li>
<li>Reduce leaks. <a href='https://commits.kde.org/discover/795ecf018ca66f4cb922565f49d79959315db449'>Commit.</a> </li>
<li>Make it easier to reuse the CachedNetworkAccessManager. <a href='https://commits.kde.org/discover/ff25b3dfcd0495eaab0c75f551ec6242bb2a731b'>Commit.</a> </li>
<li>Remove undefined variable usage. <a href='https://commits.kde.org/discover/f67710cd06b457cd72d4a181176e13c6bfbd85f7'>Commit.</a> </li>
<li>Simplify listing code. <a href='https://commits.kde.org/discover/266239be948ca67d242e134996d87bea3edd2c41'>Commit.</a> </li>
<li>Don't continue fetching when looking for installed resources. <a href='https://commits.kde.org/discover/f9c820d6efc3589124143a5e2ccc13806f28654a'>Commit.</a> </li>
<li>Revert "Revert "Use appstream-qt insted of glib based one"". <a href='https://commits.kde.org/discover/2187ede781f62e2667a1d8f401af3eff10244258'>Commit.</a> </li>
<li>Block less on start. <a href='https://commits.kde.org/discover/7a73f34c1856350cb1225f9b79f367215185c1bd'>Commit.</a> </li>
<li>Remove unused logic. <a href='https://commits.kde.org/discover/6a606076604172cf077b573ec5c2d532893c3822'>Commit.</a> </li>
<li>Fix build. <a href='https://commits.kde.org/discover/aceb852884c024ba092eee348934806b48fef86d'>Commit.</a> </li>
<li>Don't load asynchronously images that come from the qrc. <a href='https://commits.kde.org/discover/ef076acb4c811980b1d5019bc9cb45ef4f3c73d6'>Commit.</a> </li>
<li>Remove the declarative plugin and just instantiate it from the main object. <a href='https://commits.kde.org/discover/178bbdcd73537b80066a9a237befccd0d1045f3e'>Commit.</a> </li>
<li>Don't try to list firmwares from PackageKit. <a href='https://commits.kde.org/discover/f400d42697c4bc7f457032e3bf6a831efbc800e7'>Commit.</a> </li>
<li>Improve integration of the Application source link button. <a href='https://commits.kde.org/discover/f50e0ff11b4810066b7e63f8568dc0eb32bc1f64'>Commit.</a> </li>
<li>Some initial category support for Snap. <a href='https://commits.kde.org/discover/86bc21d6a7297a622c2afb94499749dcb174799a'>Commit.</a> </li>
<li>All resources should have a URL. <a href='https://commits.kde.org/discover/20eb8edff7c93967294ebbcd45b312b4d45d7a32'>Commit.</a> </li>
<li>Coding style. <a href='https://commits.kde.org/discover/2b5158082626263089c36102a27e93d5c4f88eef'>Commit.</a> </li>
<li>Properly fetch icons. <a href='https://commits.kde.org/discover/a53f48ac69e6c80b1a211304c1391e1f92ab7bab'>Commit.</a> </li>
<li>Implement screenshots for snap. <a href='https://commits.kde.org/discover/0c91e20d48d7b506e76801938299a67f510a8e06'>Commit.</a> </li>
<li>Remove a bunch of unused API. <a href='https://commits.kde.org/discover/dc74ef75fde23f5690c92deb27df9e0b4d9ee6d6'>Commit.</a> </li>
<li>Actually provide icons for snaps. <a href='https://commits.kde.org/discover/e829908a04fb4d1ada3745c7766be7f4a7f1fba7'>Commit.</a> </li>
<li>Properly filter urls. <a href='https://commits.kde.org/discover/c166ec5780244d25cb96f55d712cab45843e9e2a'>Commit.</a> </li>
<li>Properly filter application snaps. <a href='https://commits.kde.org/discover/3d23e959881ed51b5742177f75935eb959053630'>Commit.</a> </li>
<li>When apps backend isn't found use whatever application backend we have. <a href='https://commits.kde.org/discover/5b1e5301a1b18986d59e4b03ebf0d353d4775a91'>Commit.</a> </li>
<li>Don't look up Snapd if it's not going to be used. <a href='https://commits.kde.org/discover/1ff711c54630b665988a728d6318f06004d598ed'>Commit.</a> </li>
<li>Cleaner filtering. <a href='https://commits.kde.org/discover/94c6317838f08a7511fa25508c0c74e24ed4e1c5'>Commit.</a> </li>
<li>Improve uri look-up. <a href='https://commits.kde.org/discover/9f2a1b716d390225cde7c7a07d3ec3c04576d5ff'>Commit.</a> </li>
<li>Disable the snap-backend if it failed to initialize. <a href='https://commits.kde.org/discover/690d62fc3f23ea6b8476af47ffec77443066b9a4'>Commit.</a> </li>
<li>Unneeded include. <a href='https://commits.kde.org/discover/5438ec204d4cc380778d9f416667b03582efc9cb'>Commit.</a> </li>
<li>The macaroon dialog will only interact with snap through policykit. <a href='https://commits.kde.org/discover/b1d6ab8a9b3fcfb86f46167d6cb502794026f79d'>Commit.</a> </li>
<li>Initial port to snapd-glib library. <a href='https://commits.kde.org/discover/f364bf807e2c7bda168aeade933f875dc752b4c6'>Commit.</a> </li>
<li>Revert "Use appstream-qt insted of glib based one". <a href='https://commits.kde.org/discover/ae25ff805e7204543743f9fc9a36e5f47fbc8756'>Commit.</a> </li>
<li>Use appstream-qt insted of glib based one. <a href='https://commits.kde.org/discover/dd26366277ea93230f26e214fe6dddd3c004c69d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6267'>D6267</a></li>
<li>Make sure we display at least some origin if there is non for flatpak bundles. <a href='https://commits.kde.org/discover/4dc58c58bbb156a01af5b5022141a5676570833b'>Commit.</a> </li>
<li>Fix comparison between appstream ids. <a href='https://commits.kde.org/discover/e273bd5db1b2c699ca7a75a5abb1e88d58c1ddbe'>Commit.</a> </li>
<li>Make sure the update progress is as smooth as possible. <a href='https://commits.kde.org/discover/8c50c5f9e951d367be65226c12ceb520586415ff'>Commit.</a> </li>
<li>Add status message for failed transactions as well. <a href='https://commits.kde.org/discover/f01cd4b03063a2f4e9ea1c46e5da602a5859b82b'>Commit.</a> </li>
<li>Relax the Tasks view during updates that contain StandardUpdaters. <a href='https://commits.kde.org/discover/cef117db413e164c5af756873c6b28960b8e000d'>Commit.</a> </li>
<li>Make it possible for the ProgressView to include resource-less transactions. <a href='https://commits.kde.org/discover/ad0c705aef1ee6cede46a93ce60945e07e9e4536'>Commit.</a> </li>
<li>Don't assert when a transaction is deleted too early. <a href='https://commits.kde.org/discover/a4332eba0f678819e843a532436c56fcbe463b1d'>Commit.</a> </li>
<li>Improve tests. <a href='https://commits.kde.org/discover/d2738d318b87587f2356ee218e8b71a8079b6308'>Commit.</a> </li>
<li>Make sure we don't set the status until everything is in place. <a href='https://commits.kde.org/discover/8b83df514ccba0196b488019b639d4094cfeeeed'>Commit.</a> </li>
<li>Make it possible to use the AddonsList and PackageState with QDebug. <a href='https://commits.kde.org/discover/2857f080af56d54d3e2de7c6204db453f4d65418'>Commit.</a> </li>
<li>Change how we deal with Transactions. <a href='https://commits.kde.org/discover/fd01fb97a73ec4c9a24bb4a7d40f31c0a59acb8b'>Commit.</a> </li>
<li>Move applications source menu to the left of the toolbar. <a href='https://commits.kde.org/discover/dbca623869fa38311abdbabfa1c2134e3104175d'>Commit.</a> </li>
<li>Extend tests. <a href='https://commits.kde.org/discover/d278a00364fbe4009e2594e34f061e3710c119cf'>Commit.</a> See bug <a href='https://bugs.kde.org/370906'>#370906</a></li>
<li>Remove redundant code. <a href='https://commits.kde.org/discover/94a3424d29ea7a2a8c1b01fd41cd602b0f4de4d1'>Commit.</a> </li>
<li>Open information about the software license by making the license clickable. <a href='https://commits.kde.org/discover/7de6fbd5ce889ba592b449a69e228c8b8917ddd1'>Commit.</a> </li>
<li>Simplify KNSBackend fetch logic. <a href='https://commits.kde.org/discover/efaa09a20543583323cdc8f7841a0879faed0d37'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380138'>#380138</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6191'>D6191</a></li>
<li>Simplify logic. <a href='https://commits.kde.org/discover/d5b12a6b46fb4a83710dddb4d87434498584b7a8'>Commit.</a> </li>
<li>Remove unused declaration. <a href='https://commits.kde.org/discover/2fd0b0475d3ad064e5f80c8007db14cdf5dd1640'>Commit.</a> </li>
<li>Make sure the created component has an id. <a href='https://commits.kde.org/discover/8c459cfa628cbf6aaf1097ccd9e3badfd74b3fa5'>Commit.</a> </li>
<li>Checkable doesn't seem to be necessary for BasicListItem. <a href='https://commits.kde.org/discover/82e745a98cf89ef0de1c54059a9d3872c63af0a8'>Commit.</a> </li>
<li>Make it possible to change the source as well from the description. <a href='https://commits.kde.org/discover/a8939cbb0f0587e9bb0c02baac927a6368f5420b'>Commit.</a> </li>
<li>Don't force a space when layouts are already spaced. <a href='https://commits.kde.org/discover/cb1779d128bd96e56bdfa8b7a8d3ce1c6f957cb6'>Commit.</a> </li>
<li>Don't make screenshots constrained to the page. <a href='https://commits.kde.org/discover/7b40c3040248cafb174ab43ca74da0338eeed15e'>Commit.</a> </li>
<li>Leaner approach for switching sources. <a href='https://commits.kde.org/discover/4d8f1914070d41b144838d4a2dff0ac33a9b498a'>Commit.</a> </li>
<li>We also want to list installed packages of the non-default app source. <a href='https://commits.kde.org/discover/16ec05152106daa13bbdf85be890901cc975774a'>Commit.</a> </li>
<li>Make it possible to specify in the query if all backends will be queried. <a href='https://commits.kde.org/discover/7e431a224efe2084761ef475a82763079593589b'>Commit.</a> </li>
<li>Use the resources model to search from the featured model. <a href='https://commits.kde.org/discover/c796ffb266a03f38d0e0af07c86b33a132f20f2b'>Commit.</a> </li>
<li>Delay deletion of the AggregatedResultsStreams when there's no streams. <a href='https://commits.kde.org/discover/b9bcae0233e2876f61069f42b077c7fbe289557b'>Commit.</a> </li>
<li>Application Backend selection is exclusive. <a href='https://commits.kde.org/discover/611bd66b519ee7eef4e63eb6532dd683504b5dc9'>Commit.</a> </li>
<li>Make tests more flexible, hopefully fixing them on build.kde.org. <a href='https://commits.kde.org/discover/a6d90ac435a67eeb89916f896dd5993103bdfbe4'>Commit.</a> </li>
<li>Adapt the dummy bakcend to changes. <a href='https://commits.kde.org/discover/b0fc2ab00306b75a4dc6ee721c720f5dd80ba0ec'>Commit.</a> </li>
<li>Make it possible to change the source we're dealing with. <a href='https://commits.kde.org/discover/4bbab6b242bafa784849ad1711ffbdb2c6252ae8'>Commit.</a> </li>
<li>When opening by url, show a list rather than a resource directly. <a href='https://commits.kde.org/discover/95ef0d1cee2e14e3c61dd4e365b23e71a8ede89e'>Commit.</a> </li>
<li>Allow the user to set which applications backend is preferred. <a href='https://commits.kde.org/discover/eca66b8f9e3d7a924eaa6c660b3a129f79d7187f'>Commit.</a> </li>
<li>Declare override method just once. <a href='https://commits.kde.org/discover/9c62168fdd393188d46325f8a590bfbf74d128e5'>Commit.</a> </li>
<li>Show the origin in the ApplicationPage. <a href='https://commits.kde.org/discover/f48ca75814287c51573777d61b45b6a300075083'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380976'>#380976</a></li>
<li>Add method to figure out the kns: url of an item. <a href='https://commits.kde.org/discover/bc64fab6eaaa5e2756399652004cf90d8ab66b0d'>Commit.</a> </li>
<li>Fix url, didn't exist anymore. <a href='https://commits.kde.org/discover/3606c9324be6f215cd7d0e802a4ad14e6b913ef6'>Commit.</a> </li>
<li>Use the first thumbnail as icon for KNS resources. <a href='https://commits.kde.org/discover/05e780a32f210695ec2b3efb7a8d04ae27e96863'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360675'>#360675</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5768'>D5768</a></li>
</ul>


<h3><a name='drkonqi' href='https://commits.kde.org/drkonqi'>Dr Konqi</a></h3>
<ul id='uldrkonqi' style='display: block'><li>New in this release</li></ul>
<h3><a name='kactivitymanagerd' href='https://commits.kde.org/kactivitymanagerd'>kactivitymanagerd</a> </h3>
<ul id='ulkactivitymanagerd' style='display: block'>
<li>Separating queries for removing from a specific activity and from all activities. <a href='https://commits.kde.org/kactivitymanagerd/2a978d3647f1e56b78fb87c0913c35eae10f295f'>Commit.</a> </li>
</ul>


<h3><a name='kde-cli-tools' href='https://commits.kde.org/kde-cli-tools'>kde-cli-tools</a> </h3>
<ul id='ulkde-cli-tools' style='display: block'>
<li>Always install kdesu docs, the translations are always installed so this has minimal effect. <a href='https://commits.kde.org/kde-cli-tools/4feeaa41c3edb800114dd0b72c0f7fcd8b5e2170'>Commit.</a> </li>
<li>Update kcm statistics in kcmshell too. <a href='https://commits.kde.org/kde-cli-tools/e003e86628beebb4794848ae34ca7aae5b01c6f0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6112'>D6112</a></li>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/kde-cli-tools/492df95c51dae024fba414231922b25036f5cea4'>Commit.</a> </li>
</ul>


<h3><a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a> </h3>
<ul id='ulkde-gtk-config' style='display: block'>
<li>Add some keywords so it's more easily found in System Settings search. <a href='https://commits.kde.org/kde-gtk-config/4e31ca3f05a0ad9f513b4e0f2da197f4116e4ae0'>Commit.</a> See bug <a href='https://bugs.kde.org/383287'>#383287</a></li>
<li>Fix look-up of gtk preview modules. <a href='https://commits.kde.org/kde-gtk-config/fdb0df568637358d27e2dd315eda2aa32d2d551b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383198'>#383198</a></li>
<li>Remove unused KNS file. <a href='https://commits.kde.org/kde-gtk-config/836486fbfd3f8256e9749c15d0874a9a780a0471'>Commit.</a> </li>
<li>Make the kde-gtk-config kcm better at checking global gtk settings. <a href='https://commits.kde.org/kde-gtk-config/34357f74ee2d98128ff423b0ec6ddcbf4232c475'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378013'>#378013</a></li>
<li>Fix test. <a href='https://commits.kde.org/kde-gtk-config/a3fc95750af50a83b16f8672e8400a110e800cfa'>Commit.</a> </li>
<li>Add a checkbox to enable dark GTK3 Themes. <a href='https://commits.kde.org/kde-gtk-config/2155e62af5a8466ea9632d01e16a08d8b40f7ba8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346469'>#346469</a></li>
<li>Use override. <a href='https://commits.kde.org/kde-gtk-config/0a637d4d779788885f4c530dae1a9ed7fbabcce3'>Commit.</a> </li>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/kde-gtk-config/fbac76e4af46ac21f2561f10b941c96cae8cda64'>Commit.</a> </li>
</ul>


<h3><a name='kdecoration' href='https://commits.kde.org/kdecoration'>KDE Window Decoration Library</a> </h3>
<ul id='ulkdecoration' style='display: block'>
<li>[autotests] Fix DecorationButtonTest::testPressAndHold with Qt 5.9. <a href='https://commits.kde.org/kdecoration/da28d8a4eb5582c35617564939cd7ad865a269d1'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>Try to get higher quality image for flickr potd. <a href='https://commits.kde.org/kdeplasma-addons/819ed65e6f8ba83c7cc10ac278197319e53f2e7e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377917'>#377917</a></li>
<li>Spdx syntax fix http://metadata.neon.kde.org/appstream/html/xenial/main/issues/plasma-dataengines-addons.html. <a href='https://commits.kde.org/kdeplasma-addons/a8b30c5bb480ab264ab8f32b49c0563044af6e96'>Commit.</a> </li>
<li>Fix .desktop files for appstream compliance http://metadata.neon.kde.org/appstream/html/xenial/main/issues/plasma-widgets-addons.html. <a href='https://commits.kde.org/kdeplasma-addons/1d4e69db40f478bf20b06c954d4eb0f9e9034cf3'>Commit.</a> </li>
<li>Fixes for appstream compliance http://metadata.neon.kde.org/appstream/html/xenial/main/issues/plasma-wallpapers-addons.html. <a href='https://commits.kde.org/kdeplasma-addons/f6e65068ccaac1245ffce4c574560a456aab283d'>Commit.</a> </li>
<li>[Color Picker] Add drag pixmap for color. <a href='https://commits.kde.org/kdeplasma-addons/3e63fa62e6296315054fa3f063302b978d74021d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7184'>D7184</a></li>
<li>Color Picker applet can now be found and launched through KRunner. <a href='https://commits.kde.org/kdeplasma-addons/8da9cb72fc73eaee339e9899708e8b78bef49b2a'>Commit.</a> </li>
<li>[Notes applet] Wrap in FocusScope. <a href='https://commits.kde.org/kdeplasma-addons/300771464b5aab6b16eac728e702e830e4d0960c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6575'>D6575</a></li>
<li>Remove wrong X-Ubuntu-Gettext-Domain key. <a href='https://commits.kde.org/kdeplasma-addons/d0a07b2ee812a4b9bb6c1510bced4856a50e8601'>Commit.</a> </li>
<li>Restore original formatButtonsRow.opacity. <a href='https://commits.kde.org/kdeplasma-addons/0e1e35e4607f7225efbeaf7eb0771f04f46d1b41'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6150'>D6150</a></li>
<li>[Notes Applet] Show formatting options when it has focus. <a href='https://commits.kde.org/kdeplasma-addons/4c2f9a61804715e83825f582a0d9b8a72d9dcdb8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6150'>D6150</a></li>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/kdeplasma-addons/12224edf6954f63e06ceccc65c8ad8e9e5f9aa42'>Commit.</a> </li>
<li>Fix fifteen puzzle solveability. <a href='https://commits.kde.org/kdeplasma-addons/3516eda8107bda0c91fa8fa5ef7e6da34d995f6b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5913'>D5913</a>. Fixes bug <a href='https://bugs.kde.org/358940'>#358940</a></li>
</ul>


<h3><a name='kgamma5' href='https://commits.kde.org/kgamma5'>Gamma Monitor Calibration Tool</a> </h3>
<ul id='ulkgamma5' style='display: block'>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/kgamma5/db0d8b2ee485cc64926d4b217acad5dcbb14aba8'>Commit.</a> </li>
<li>Use QFormLayout for sliders. <a href='https://commits.kde.org/kgamma5/f5905ee018cd37c63ea4bcf057dcb0f2995621ab'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5886'>D5886</a></li>
<li>Don't fix a height of sliders. <a href='https://commits.kde.org/kgamma5/160938ffb3f277ad94a4138b5442ad8d2e1c2389'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5885'>D5885</a></li>
<li>Fix spacing in "Select test picture:" row. <a href='https://commits.kde.org/kgamma5/df98599155db833e545e007f37a78d2f6f7d18f6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5882'>D5882</a></li>
<li>Remove .png extension from icon name in the desktop file. <a href='https://commits.kde.org/kgamma5/879e031097bd537397d9ee60b6f32b2d9da9c3de'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5881'>D5881</a></li>
</ul>


<h3><a name='khotkeys' href='https://commits.kde.org/khotkeys'>KDE Hotkeys</a> </h3>
<ul id='ulkhotkeys' style='display: block'>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/khotkeys/ac0ab6317bcebd5874421ba754d1f38e6f313600'>Commit.</a> </li>
</ul>


<h3><a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a> </h3>
<ul id='ulkinfocenter' style='display: block'>
<li>Do not leak XVisualInfo (X11 EGL path). <a href='https://commits.kde.org/kinfocenter/27e0c81db9121974737fa1d6032a12b391fc2d2d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6566'>D6566</a></li>
<li>Do not mark {variantLabel} as translatable. <a href='https://commits.kde.org/kinfocenter/3222a95c50d7767d16d3b3f563d8c6de3f08bf99'>Commit.</a> </li>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/kinfocenter/a3340d39ff7cad6b92bb91080221b29549c3f53f'>Commit.</a> </li>
</ul>


<h3><a name='kmenuedit' href='https://commits.kde.org/kmenuedit'>KMenuEdit</a> </h3>
<ul id='ulkmenuedit' style='display: block'>
<li>Remove obsolete add_dependencies. <a href='https://commits.kde.org/kmenuedit/dab2e843793d3432ccef11ad9332dc87707cb3a1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5699'>D5699</a></li>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/kmenuedit/967e39a48af26c720be29c6ae4a5bc5f79ec0365'>Commit.</a> </li>
</ul>


<h3><a name='kscreen' href='https://commits.kde.org/kscreen'>KScreen</a> </h3>
<ul id='ulkscreen' style='display: block'>
<li>Also generate default output scale on first run not just new monitors. <a href='https://commits.kde.org/kscreen/22a0012950c4727de4377f974a142cd189e3960a'>Commit.</a> </li>
<li>[OutputConfig] Locale-format refresh rate. <a href='https://commits.kde.org/kscreen/7a768f9fa571562a3c6c5d98958eba67a5599937'>Commit.</a> </li>
<li>Automatic scaling selection. <a href='https://commits.kde.org/kscreen/2c1303a0f1a7244aa77b11419539a398f79c23ea'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7482'>D7482</a></li>
<li>Fix deprecated usage of ecm_install_icons. <a href='https://commits.kde.org/kscreen/46ab5b1bbe622bf49b90e03d485824dc692812b7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7488'>D7488</a></li>
<li>Show UI for per screen scaling options on supported platforms. <a href='https://commits.kde.org/kscreen/b85cbd494f22d7cde44b764552896682929eaa1b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7331'>D7331</a></li>
<li>Port OutputConfig away from blockSignals. <a href='https://commits.kde.org/kscreen/98a0345dd67782568b5e1a6490d6586f017f4ef5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7333'>D7333</a></li>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/kscreen/2dda869263eeedea34f174f1aa104b7ae338db9e'>Commit.</a> </li>
<li>Get rid of extra margins. <a href='https://commits.kde.org/kscreen/96f3e96d3c45fd54ff5fd4372b36d53fda96b242'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5888'>D5888</a></li>
</ul>


<h3><a name='kscreenlocker' href='https://commits.kde.org/kscreenlocker'>KScreenlocker</a> </h3>
<ul id='ulkscreenlocker' style='display: block'>
<li>Seccomp filter: Handle openat as well. <a href='https://commits.kde.org/kscreenlocker/1d70ac4096acbd7e68fb776086f66b70aebc3261'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/384651'>#384651</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7806'>D7806</a></li>
<li>Don't dissallow open with write flag syscall on NVIDIA. <a href='https://commits.kde.org/kscreenlocker/2136a38d88c8d0d7be67fe0c6c0870e53a6f7bc6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/384005'>#384005</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7616'>D7616</a></li>
<li>Tell user to unlock his session only. <a href='https://commits.kde.org/kscreenlocker/7bbafe5f80c558fbcebd081a9733f37d8753f983'>Commit.</a> </li>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/kscreenlocker/b56157c61a98941b10116a81b41206c98a1e8260'>Commit.</a> </li>
<li>Fix detection of sys/event.h on FreeBSD < 12. <a href='https://commits.kde.org/kscreenlocker/d1b2761a3b48aa133501386476433d190d7369f5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6024'>D6024</a></li>
<li>Include <signal.h> for kill(2). <a href='https://commits.kde.org/kscreenlocker/62e6f59c0bc7e345a92b6e03ab3a7250b554c261'>Commit.</a> </li>
</ul>


<h3><a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a> </h3>
<ul id='ulksysguard' style='display: block'>
<li>Softraid: Remove dead code and associated warning. <a href='https://commits.kde.org/ksysguard/3588e85cd8a67b99f779e33819b38752d319db95'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6658'>D6658</a></li>
<li>Properly check if mdraid array is active. <a href='https://commits.kde.org/ksysguard/09d8fdf914656af798385239a6639e605a7ddfda'>Commit.</a> </li>
<li>FreeBSD build fix, v4. <a href='https://commits.kde.org/ksysguard/7c551f82de4b3031f82480d1b669ff7ec4a31f7f'>Commit.</a> </li>
<li>FreeBSD compile fix, try 3. <a href='https://commits.kde.org/ksysguard/5bef0ecaa07147984182ab291d63b537731ae833'>Commit.</a> </li>
<li>Fix FreeBSD build, try 2. <a href='https://commits.kde.org/ksysguard/8c650abc851c899fce38b446361d437a1781b073'>Commit.</a> </li>
<li>Fix build regression on FreeBSD. <a href='https://commits.kde.org/ksysguard/6db11670e94244a1b088e884881ebfb2efe78df8'>Commit.</a> </li>
<li>Fix compilation with strict libc (such as musl). <a href='https://commits.kde.org/ksysguard/257591cf462d2d1db7a5c606c316e8e39c84cdfd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6631'>D6631</a></li>
<li>Remove extra <nlist.h> include on FreeBSD. <a href='https://commits.kde.org/ksysguard/7d7cafefc23de371cfa08c724dc7ae47bd35f349'>Commit.</a> </li>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/ksysguard/364691380fd4bf8efbeacc855d6667841fc15d21'>Commit.</a> </li>
</ul>


<h3><a name='kwallet-pam' href='https://commits.kde.org/kwallet-pam'>kwallet-pam</a> </h3>
<ul id='ulkwallet-pam' style='display: block'>
<li>Avoid dropping privileges by initializing gcrypt secmem. <a href='https://commits.kde.org/kwallet-pam/1a01e1eb870e1ab1d96a8641f1f3500af646c974'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7124'>D7124</a></li>
<li>Check for a graphical session. <a href='https://commits.kde.org/kwallet-pam/f3b230f7f3bf39dc46b97a216aa7c28595d20a7a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7125'>D7125</a></li>
<li>Several cleanups. <a href='https://commits.kde.org/kwallet-pam/a33ec22b96e837899528b05963eae8ea6b01171a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7123'>D7123</a></li>
<li>Handle differences in PAM headers. <a href='https://commits.kde.org/kwallet-pam/38ff660bb81ce464d923338bcd10d08d96509935'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6000'>D6000</a></li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Try fixing build failure on CI. <a href='https://commits.kde.org/kwin/a9270f4232b7834b5ba5994fb9ee9754c45a6673'>Commit.</a> </li>
<li>CMake 3.1 is the actually required version. <a href='https://commits.kde.org/kwin/10381b10f899f644a30826f535dabc9e037463e6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7784'>D7784</a></li>
<li>[effects] Support xcbConnectionChanged for support properties. <a href='https://commits.kde.org/kwin/90e77a939a4d96702ef3fdd862ede781bdc2a0c5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7761'>D7761</a></li>
<li>Move XRandR event filter into XRandRScreens. <a href='https://commits.kde.org/kwin/4fa41165d1902c988b99448c0e7b7e4a7bfadf0b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7654'>D7654</a></li>
<li>Add virtual method to Scene to get the EGL/GLX extensions. <a href='https://commits.kde.org/kwin/8015e4e84ece3d8b120a896ebaab2595c5d646ef'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7734'>D7734</a></li>
<li>Do not hard runtime depend on X11 in RuleBook. <a href='https://commits.kde.org/kwin/8522ef17eea0daf913eaaf0ee2c2d02d43c9d475'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7653'>D7653</a></li>
<li>[autotests] Drop cast to SceneOpenGL GenericSceneOpenGLTest. <a href='https://commits.kde.org/kwin/50470e97c6268dcfb661d6886bc325d87829356a'>Commit.</a> </li>
<li>Make AbstractEglBackend a QObject. <a href='https://commits.kde.org/kwin/01ddbe7d751ab2f31a2698fc236713530c1631ce'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7669'>D7669</a></li>
<li>Pass EGL information from AbstractEglBackend to Platform instead of query. <a href='https://commits.kde.org/kwin/9381411b9112fe06439164866fdbcd4696a07ee9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7698'>D7698</a></li>
<li>[tabbox] Properly check whether two windows belong to same application. <a href='https://commits.kde.org/kwin/5d9027b110be7510a75daa789402b83c106a4164'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7713'>D7713</a></li>
<li>Delay syncing internal window geometry to end of cycle. <a href='https://commits.kde.org/kwin/e0f95fd913b6e7c9106ca153c0dd1e3d46c9e297'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/384441'>#384441</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7712'>D7712</a></li>
<li>[platforms/hwcomposer] Include the android-config.h in the hwcomposer_backend. <a href='https://commits.kde.org/kwin/d1e0c6f9d728e7f36af2341042629d6d3141f2c0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7719'>D7719</a></li>
<li>Protect readProperty and deleteProperty in case of no X11. <a href='https://commits.kde.org/kwin/4db4fa42f7ed30c4fb5cd46f3e6831c86efe715a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7651'>D7651</a></li>
<li>Drop XRandR dependency from Options's currentRefreshRate. <a href='https://commits.kde.org/kwin/536739f09510093e2bc9415d294de4c462ca320a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7650'>D7650</a></li>
<li>Deprecate global KWin::displayWidth and KWin::displayHeight. <a href='https://commits.kde.org/kwin/20e22ec26a4a8033b45b0e5a5ff241792a04107d'>Commit.</a> </li>
<li>Fix Platform::createDecorationRenderer. <a href='https://commits.kde.org/kwin/6168638cf4f7cc3f26ec78c8c565e58e67ed1582'>Commit.</a> </li>
<li>Create SyncManager only when using X11. <a href='https://commits.kde.org/kwin/3e0e26204f4c4dc28fadef9eac557145d45f5137'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7514'>D7514</a></li>
<li>Do not hard runtime depend on X11 in composite startup. <a href='https://commits.kde.org/kwin/2892fad5b6c5d3eccd946a6aa24d0f77b40f0058'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7504'>D7504</a></li>
<li>Move screen inversion through XRandr into X11 standalone platform. <a href='https://commits.kde.org/kwin/51561052ec0342a6afe821c2a8c6074e2d59a914'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7447'>D7447</a></li>
<li>Move the X11 Decoration Renderer into the X11 standalone platform. <a href='https://commits.kde.org/kwin/23ef40e638095a6bcd1eb30dd0bb2756df83be3e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7444'>D7444</a></li>
<li>Move QPainter compositor into plugin. <a href='https://commits.kde.org/kwin/535b107969ca15021d6758a4c5877882f8cf1784'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7259'>D7259</a></li>
<li>Provide a virtual Scene::qpainterRenderBuffer() -> QImage* method. <a href='https://commits.kde.org/kwin/c398db3c459101fa8f2927912766a71501f6aca3'>Commit.</a> </li>
<li>[autotests] Remove not needed includes for scene_qpainter.h. <a href='https://commits.kde.org/kwin/546d603182170656a3390fb5344d1747dd28a1ba'>Commit.</a> </li>
<li>Move SceneXRender into a plugin. <a href='https://commits.kde.org/kwin/054d9234112ca4faa0f563be5012c74286b9c806'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7232'>D7232</a></li>
<li>Port some displayWidth/displayHeight usages to Screens::size(). <a href='https://commits.kde.org/kwin/1e13deaa1d5687482bfe87741659467226b4c331'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D1798'>D1798</a></li>
<li>Move NonComposited Outline into the X11 standalone platform. <a href='https://commits.kde.org/kwin/a3dc2ad5a26eb036dc18b77f2e35f26d9837fda1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7450'>D7450</a></li>
<li>Drop boolean parameter from AbstractClient::caption. <a href='https://commits.kde.org/kwin/f0652970f4f0a22ad0e90fc7e012e8c2997dc8e5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7427'>D7427</a></li>
<li>Make AbstractClient::caption no longer a virtual method. <a href='https://commits.kde.org/kwin/a7b29e09cefe50f6ef6e9e603997da0c1753885a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7426'>D7426</a></li>
<li>Add <number> to Wayland captions if the caption is the same. <a href='https://commits.kde.org/kwin/6685288d486925eeac5a2c14424812aa3c243902'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7425'>D7425</a></li>
<li>Move X11 specific event filtering for ScreenEdges into x11 standalone platform. <a href='https://commits.kde.org/kwin/833f933c5c6aa1bedfb57fa65bf7d9b6e2f9516a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7406'>D7406</a></li>
<li>Don't create QWhatsThis when user presses showContextHelp button. <a href='https://commits.kde.org/kwin/76ee47151a2ce7f879f63d9715aebd440aebc889'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7398'>D7398</a></li>
<li>[tabBox] Move X11 specific event filtering into a dedicated event filter. <a href='https://commits.kde.org/kwin/9c74be1256cc710d74addd536d62d921c5d373b9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7395'>D7395</a></li>
<li>Move X11 movingClient handling into a dedicated X11EventFilter. <a href='https://commits.kde.org/kwin/bd5f5e0915b3178b753dc90f7cc893b46f13bef6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7374'>D7374</a></li>
<li>ARGB buffers are premultiplied. <a href='https://commits.kde.org/kwin/b57a525ef7b4ca30f955ccd4eff9fdbf8774b09e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7607'>D7607</a></li>
<li>Disable synced resizing for X11 clients on Xwayland. <a href='https://commits.kde.org/kwin/6f4c02ce10186fe966bce1ead63144e845bc74fe'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374881'>#374881</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7524'>D7524</a></li>
<li>[autotests] Fix testX11TimestampUpdate after latest change. <a href='https://commits.kde.org/kwin/28534e3664520a9d0282e2873a549b64ce65f30e'>Commit.</a> </li>
<li>Move updateXTime into the X11 standalone platform. <a href='https://commits.kde.org/kwin/1cc38c929a100337547abd42e0c7ccc133878877'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7515'>D7515</a></li>
<li>Handle buffer scales changing dynamically. <a href='https://commits.kde.org/kwin/95c983e6d858afb3ba69b30aaf36db0d32600475'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7486'>D7486</a></li>
<li>Send output enter/leave events to surfaces. <a href='https://commits.kde.org/kwin/f0971532c90b9b04bf4ac084cef0ecf4fe61b7d2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7359'>D7359</a></li>
<li>[effects] Don't register touch edges which don't exist. <a href='https://commits.kde.org/kwin/981662a6b5e00a4fc01e264e79437a63344e5432'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383797'>#383797</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7508'>D7508</a></li>
<li>Use App's x11 rootWindow and connection in PointerInputRedirection::warpXcbOnSurfaceLeft. <a href='https://commits.kde.org/kwin/80c3fdd684b3bfebf8d19294c21f3beea0f0eed5'>Commit.</a> </li>
<li>Make EffectsHandlerImpl::announceSupportProperty work without X11. <a href='https://commits.kde.org/kwin/651ea26f3c6393ebaf86d099eb9ae3fc04ef95b3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7475'>D7475</a></li>
<li>Remove hard X11 runtime dependency from DBusInterface. <a href='https://commits.kde.org/kwin/793624b9a73ddf9394e7ffa88126bc0bb6edc3d7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7474'>D7474</a></li>
<li>Port Compositor::deleteUnusedSupportProperties away from global x11 connection. <a href='https://commits.kde.org/kwin/12cb1c108e5b543d2bb0a421534e00c7598f8d95'>Commit.</a> </li>
<li>Use xcb-icccm to read the name property. <a href='https://commits.kde.org/kwin/c87230c3a5feb6b31938189bd3dd69eaee490def'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382789'>#382789</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7046'>D7046</a></li>
<li>Only send active window changes to X11 root window if the X11 window changed. <a href='https://commits.kde.org/kwin/0455fa9ef98966af9979b2fbc36affd5b34ea853'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7096'>D7096</a></li>
<li>Forward showing desktop only to rootInfo if it changed. <a href='https://commits.kde.org/kwin/778b7d037f63bbbf9336810fc6f927411ac94211'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7097'>D7097</a></li>
<li>Drop unused private field GlobalShortcut::m_axis. <a href='https://commits.kde.org/kwin/311fe315e0978f33382eb719b528fabf904ca294'>Commit.</a> </li>
<li>Don't move local object in return statement. <a href='https://commits.kde.org/kwin/95a6f1ac2af4f68fd41e062aa68d7463f7ea820c'>Commit.</a> </li>
<li>Send QKeyEvent with Qt::Key as expected by Qt to internal windows. <a href='https://commits.kde.org/kwin/70bc9524d95b56c8ab569b26f596640214839070'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6828'>D6828</a></li>
<li>[platforms/x11] Use a GlxContextAttributeBuilder. <a href='https://commits.kde.org/kwin/f88c322a3b8388d58a3c4f63fd654198600d151e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6411'>D6411</a></li>
<li>Create a dedicated X11EventFilter for recognizing first user interaction. <a href='https://commits.kde.org/kwin/36a31898631601d07374b0ffbbad4f65aaf77156'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7371'>D7371</a></li>
<li>Track outputs in kwin integration tests. <a href='https://commits.kde.org/kwin/34de2c2b5c8b08643a4ccf4bb2a88c89b1931b03'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7377'>D7377</a></li>
<li>Migrate Xkb::toQtKey away from KKeyServer. <a href='https://commits.kde.org/kwin/5d101ce2977b3f1cf5670b6ef4916a903d53ff26'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7336'>D7336</a></li>
<li>[autotests] Add test case for Xkb::toQtKey. <a href='https://commits.kde.org/kwin/801fe41013dda92d3d2e7d8e24e1b869aa1be885'>Commit.</a> </li>
<li>Dependency inject KWayland::Server::SeatInterface into Xkb. <a href='https://commits.kde.org/kwin/dbb951b4e23abd7616d5a5c83429e05a73507e5a'>Commit.</a> </li>
<li>Turn Xkb into a QObject. <a href='https://commits.kde.org/kwin/08ae17e26560ce62d8233122062b9a3b66d09680'>Commit.</a> </li>
<li>Always emit Client::captionChanged when the caption changed. <a href='https://commits.kde.org/kwin/f50fbde1a9edae2eb9dd2947e457687dc94b41a7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383444'>#383444</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7290'>D7290</a></li>
<li>[autotests] Try making KWinBindingsTest::testSwitchWindowScript more robust. <a href='https://commits.kde.org/kwin/f9e0e8a980b8f237115e69a8da7d382164be0ead'>Commit.</a> </li>
<li>[autotests] Add test case for updating X11 client captions. <a href='https://commits.kde.org/kwin/d0c2c1ed09e5ea15dd89c6385f7a099c41c2d841'>Commit.</a> See bug <a href='https://bugs.kde.org/383444'>#383444</a></li>
<li>Remove Keyboard modifiers from Qt::Key created in Xkb::toQtKey. <a href='https://commits.kde.org/kwin/d56b75f636390e981c040d8a438a918c1bdabc87'>Commit.</a> </li>
<li>Move event filtering for overlay window into an X11EventFilter. <a href='https://commits.kde.org/kwin/a65b2c062cb0c02497d593aa535150fd23b5651c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7197'>D7197</a></li>
<li>Move the X11 specific OverlayWindow into the platform/x11. <a href='https://commits.kde.org/kwin/b4a79d30e64f56d9638a465ae199042c82f87ec6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7193'>D7193</a></li>
<li>Implement the shortcut caption suffix for Wayland windows. <a href='https://commits.kde.org/kwin/bbca8c6677feb5dc4ad91bd296ce5a7e5a4d36f2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7093'>D7093</a></li>
<li>[autotests] Try making QuickTilingTest::testShortcut more robust. <a href='https://commits.kde.org/kwin/5118d9470cb6eaf63f7175042250eeb98222549d'>Commit.</a> </li>
<li>[autotests] Try making StrutsTest::testWindowMoveWithPanelBetweenScreens more robust. <a href='https://commits.kde.org/kwin/5ff9aa7cf5c0f0fb8a1d53f7ada1ec794612ac74'>Commit.</a> </li>
<li>Add virtual Scene::scenePainter method. <a href='https://commits.kde.org/kwin/ad4a3107d5489770bb397935b54e1441647b10f3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7214'>D7214</a></li>
<li>Add virtual Scene::xrenderBufferPicture method. <a href='https://commits.kde.org/kwin/c1892e6c0fd4ccf2309aed8e2c194b7b8ba70aa3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7207'>D7207</a></li>
<li>[autotests] Try to make translucencyTest more robust for CI. <a href='https://commits.kde.org/kwin/83790306334a51c152d94b2ec1e425ba226861e0'>Commit.</a> </li>
<li>[autotests] Disable Outline in StrutsTest. <a href='https://commits.kde.org/kwin/af9f833a1d64771e4b24d0f95e291a503e033ed7'>Commit.</a> </li>
<li>Guard every remaining access to rootInfo. <a href='https://commits.kde.org/kwin/8794fe548a0b7ec29b346748f8899face6b06f71'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7173'>D7173</a></li>
<li>Drop Client::cap_deco as it's nowhere used. <a href='https://commits.kde.org/kwin/ddf3536f19a4243670e5fc87f5fbaf9e44031542'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7077'>D7077</a></li>
<li>Handle processId being 0 in ShellClient::killWindow. <a href='https://commits.kde.org/kwin/00281711d9c47a07c8c54fe6d11fa8562fd8dea1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7168'>D7168</a></li>
<li>Properly render XWayland windows in SceneQPainter. <a href='https://commits.kde.org/kwin/20e314d151e002b85419a284a4dced9214426018'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382748'>#382748</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6952'>D6952</a></li>
<li>Drop the stripped parameter from AbstractClient::caption. <a href='https://commits.kde.org/kwin/afd52c188ab0ddb2feda7517200d91dfcd0d12fc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7059'>D7059</a></li>
<li>Process all clients when updating fullscreen for active client. <a href='https://commits.kde.org/kwin/4a0787c0582bb555aa501ea476d603aee034ec0f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6892'>D6892</a></li>
<li>Do not move temporary objects. <a href='https://commits.kde.org/kwin/9bdc7b7d4b45e4cb30da258a37c0fe62e7e6906c'>Commit.</a> </li>
<li>Drop unused variable UdevMonitor::m_udev. <a href='https://commits.kde.org/kwin/f15ceaffe6e0ac4ebd762b2ac9ab6691aae5ac58'>Commit.</a> </li>
<li>Drop unused SceneXrender::Window::alpha_cached_opacity. <a href='https://commits.kde.org/kwin/bca8777acd389c07312ec842a3014bc80fa22d46'>Commit.</a> </li>
<li>Drop unused static variables in client.cpp. <a href='https://commits.kde.org/kwin/2858e30882b230184f2e361b9071c6b25648ea95'>Commit.</a> </li>
<li>[plugins/qpa] Drop unused variable AbstractPlatformContext::m_integration. <a href='https://commits.kde.org/kwin/e713df18861d2fd4a46f71e16fb1c567b53713ed'>Commit.</a> </li>
<li>[plugins/qpa] Properly ifdef everything with HAVE_WAYLAND_EGL. <a href='https://commits.kde.org/kwin/f5845fec02f44aaf1e39d744bc39a26d67fd46fd'>Commit.</a> </li>
<li>[autotests] Drop PlasmaSurfaceTest::m_connection. <a href='https://commits.kde.org/kwin/b3a52e39998bd3d8c5ebdebaec3404ec019d959b'>Commit.</a> </li>
<li>[effects/backgroundcontrast] Remove ContrastShader::pixelSizeLocation. <a href='https://commits.kde.org/kwin/063e796b18d12a9121c5a40f4ff2c239b77797a0'>Commit.</a> </li>
<li>[effects/wobblywindows] Move computeVectorBounds into ifdef section. <a href='https://commits.kde.org/kwin/3f238470c80835eb65aa43549a0f14f1b3fe939c'>Commit.</a> </li>
<li>[kcmkwin/desktop] Use KAboutData::programLogo instead of programIconName. <a href='https://commits.kde.org/kwin/8b2b11196b5e2c66413516644715590655e682b7'>Commit.</a> </li>
<li>Check for include file before configuring config-kwin.h. <a href='https://commits.kde.org/kwin/ca84a81e0de84e53a68873856db6c91fcb0c594b'>Commit.</a> </li>
<li>Improve testWindowScaled test. <a href='https://commits.kde.org/kwin/0809a357c13ff1297d6bceee5ab666c2dedd17da'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6982'>D6982</a></li>
<li>Add cmake check whether sys/sysmacros.h exists. <a href='https://commits.kde.org/kwin/bc003c022846b7bdcb8df29097801fd9f5b914e1'>Commit.</a> </li>
<li>Turn around include order for major/minor. <a href='https://commits.kde.org/kwin/9ada8baca65da11a355970d02df8511f1b649d5a'>Commit.</a> </li>
<li>[qpa] Fix overloaded-virtual warning. <a href='https://commits.kde.org/kwin/acedee21a595303d43cb570da2d4a0ff46d6b70e'>Commit.</a> </li>
<li>[platforms/x11] Silence warning caused by eglCreateImageKHR taking an xpixmap for a pointer. <a href='https://commits.kde.org/kwin/31e43878522a31e0d250eb5a06603879a39cfd65'>Commit.</a> </li>
<li>[libinput] Fix enumeral and non-enumeral type in conditional expression warning. <a href='https://commits.kde.org/kwin/859709f10a37d4c7896013e917fe00bbd1fe7ced'>Commit.</a> </li>
<li>[libinput] Fix reorder warnings. <a href='https://commits.kde.org/kwin/656b3c2db5faca4aca887748dd71a85ac1a1603f'>Commit.</a> </li>
<li>[platforms/x11] Fix int-in-bool-context warning. <a href='https://commits.kde.org/kwin/3eb3e64d9121506de584c470c5395ef60a6cdcf5'>Commit.</a> </li>
<li>Fix unuxed-but-set-variable warning. <a href='https://commits.kde.org/kwin/f47cc621807428af119df270ca2949b291a20130'>Commit.</a> </li>
<li>[autotests] Fix unused function warnings. <a href='https://commits.kde.org/kwin/1447f4641cc9e27b2b4235c2ee5fe5826392e628'>Commit.</a> </li>
<li>Move bitCount from utils to GlxBackend. <a href='https://commits.kde.org/kwin/349618c21473e5ef04d01d42142d40271c02ea76'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6784'>D6784</a></li>
<li>Nullptr check for rootInfo when setting active client. <a href='https://commits.kde.org/kwin/da036098cfd043b117583d9df4813bb3b554d47d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6908'>D6908</a></li>
<li>Replace delegate slots for window shortcut by std::bind expressions. <a href='https://commits.kde.org/kwin/150a0357b4a6a7f40672af0ca27de0d7a84b98fd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6801'>D6801</a></li>
<li>Use std::bind expression for Workspace::slotWindowtoDesktop. <a href='https://commits.kde.org/kwin/c8c15f00579938ce3589d44663c32415baefdd0f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6811'>D6811</a></li>
<li>Drop the Workspace::slotSwitchWindowFoo methods. <a href='https://commits.kde.org/kwin/7d3517060f89040e36a6eeea1c8c64fae1c48541'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6791'>D6791</a></li>
<li>Replace Workspace::slotWindowQuickTileFoo by a quickTileWindow with argument. <a href='https://commits.kde.org/kwin/64da6c8d1e10941089c575c28609b8bd36adfd9c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6783'>D6783</a></li>
<li>Fix unused-parameter warnings as reported by GCC. <a href='https://commits.kde.org/kwin/8527ac30dc717c8f1f222a56f1a877df888aed92'>Commit.</a> </li>
<li>[autotests] Perform complete QImage comparison in SceneQPainterTest::testX11Window. <a href='https://commits.kde.org/kwin/e8a92361ae9f7328a4647c14b763b973a0aac10d'>Commit.</a> </li>
<li>Emit connectionDied on all Wayland connections of plugins on teardown. <a href='https://commits.kde.org/kwin/06433997e8d9dc9a9276182411de13ee7bc169ff'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6571'>D6571</a></li>
<li>-Wno-inconsistent-missing-override on Clang. <a href='https://commits.kde.org/kwin/719d09d0cd392fcd19e04844df200895d3cfa70d'>Commit.</a> </li>
<li>[autotests] Another change for SceneQPainterTest::testX11Window to make it pass CI. <a href='https://commits.kde.org/kwin/f6c964a32bbf8dc8e8d254cd7b37236bb6ca8a7e'>Commit.</a> </li>
<li>Make Q_FALLTHROUGH code compile with Qt < 5.8. <a href='https://commits.kde.org/kwin/eee2ace6a107bf4872b29e8b8519abf30ffa8feb'>Commit.</a> </li>
<li>[autotests] Try to make SceneQPainterTest::testX11Window more robust. <a href='https://commits.kde.org/kwin/a13ecb1cacddf447f8b30b76f78353a068f20426'>Commit.</a> </li>
<li>Fix (incorrect) implicit-fallthrough warnings in gcc7. <a href='https://commits.kde.org/kwin/4b89011099a919b516b951e6111b30584adc0ef3'>Commit.</a> </li>
<li>[autotests] Try to make SceneQPainterTest::testX11Window more robust. <a href='https://commits.kde.org/kwin/d39fea1b6744c84e76d31e980a9bf87c3e9f5cd9'>Commit.</a> </li>
<li>[autotests] Add test case for incorrect rendering of XWayland on SceneQPainter. <a href='https://commits.kde.org/kwin/54ca2bba25a30b2ff151521bcf5f8135d7bdd69d'>Commit.</a> See bug <a href='https://bugs.kde.org/382748'>#382748</a></li>
<li>[autotests] Add test cases for verifying that Toplevel::window() is only set for X11. <a href='https://commits.kde.org/kwin/1557f102f438fa3be778e3f12441926325307121'>Commit.</a> </li>
<li>Fix build with future glibc (major/minor macros), BSD compatible. <a href='https://commits.kde.org/kwin/35c278e525377723028a1417a705d28517fa183f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376505'>#376505</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6175'>D6175</a></li>
<li>[autotests] Try to make PlasmaWindowTest::testCreateDestroyX11PlasmaWindow more robust. <a href='https://commits.kde.org/kwin/f5f14475b568aff2251afc531648f2af207a8ac4'>Commit.</a> </li>
<li>Implement support for window shortcuts for Wayland windows. <a href='https://commits.kde.org/kwin/c29d6093ba9afc2ec93d24a6854c81bac1cd3563'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6818'>D6818</a></li>
<li>Hide the Linux specific parts behind check for headers. <a href='https://commits.kde.org/kwin/a512f549249df0403f6dafea4c3a0e3774c3eae1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6847'>D6847</a></li>
<li>[autotests] Extend X11ClientTest::testFullscreenLayerWithActiveWaylandWindow. <a href='https://commits.kde.org/kwin/a1af59e9dd8e3312db8f6843951c6e61e6dcfa01'>Commit.</a> </li>
<li>Reset most_recently_raised when removing ShellClient. <a href='https://commits.kde.org/kwin/7eb05552ca0d551f4a06292c47686c1cf50d7524'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6853'>D6853</a></li>
<li>Reset last_active_client when a ShellClient is removed. <a href='https://commits.kde.org/kwin/bd158a632100ae8186abdcc5853bb888d02105de'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6852'>D6852</a></li>
<li>[autotest] Add a test case for X11 fullscreen windows when an Wayland window is there. <a href='https://commits.kde.org/kwin/dcbfa0869b3604d0636e4b470f03ac9bcca6da77'>Commit.</a> See bug <a href='https://bugs.kde.org/375759'>#375759</a></li>
<li>[autotests] Add test case for sending window to desktop shortcuts. <a href='https://commits.kde.org/kwin/8cd95b56e3f38af69196ec7a37ffcfcf2101393d'>Commit.</a> </li>
<li>[autotest] Test opposite direction in switchWindow. <a href='https://commits.kde.org/kwin/b1efdbaaf1bcf88530f71522630fd2da44820f97'>Commit.</a> </li>
<li>[autotests] Add test cases for switchWindow. <a href='https://commits.kde.org/kwin/84b0578a717375f094d0d1f2d29a3dac5bb922e8'>Commit.</a> </li>
<li>[autotests] Add test case for Client window shortcut. <a href='https://commits.kde.org/kwin/c9fa43e9db1329c04e5af2e6fde298c2a528d8c0'>Commit.</a> </li>
<li>[autotests] Try to make new QuickTilingTest addition more robust on CI. <a href='https://commits.kde.org/kwin/81c59a86ddd8c146cf7ca90a81ffac89f3e13c5d'>Commit.</a> </li>
<li>[autotests/integration] Extend QuickTilingTest for keyboard shortcut and scripting. <a href='https://commits.kde.org/kwin/bbbd2f6a24bb193eeddfb60ce2646301a2c99107'>Commit.</a> </li>
<li>[qpa] Use the new OpenGLContextAttributeBuilder to create Qt's OpenGL context. <a href='https://commits.kde.org/kwin/73fa7b21a1872500a70ba32f2c4b06dafc84c087'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6734'>D6734</a></li>
<li>Require C++14. <a href='https://commits.kde.org/kwin/ea5d611de1bc33869c13c27d75a7827201a5139d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6634'>D6634</a></li>
<li>Fix s_ck2ActiveProperty value. <a href='https://commits.kde.org/kwin/58322c277892942f5ab5128d2e864fb8547157c9'>Commit.</a> </li>
<li>[platforms/drm] Delete buffer on all errors in present. <a href='https://commits.kde.org/kwin/d4423186b98ff2c1015d3811fe57eff3b599c039'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6660'>D6660</a></li>
<li>[logind] Correct property name to fix logind session. <a href='https://commits.kde.org/kwin/a49ba5054f81009b4aff859334f352f7ed7396cd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6659'>D6659</a></li>
<li>Implement support for restricted move area on Wayland. <a href='https://commits.kde.org/kwin/672cae9a7d367f04f673748ddef4d557c087de86'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6611'>D6611</a></li>
<li>Restrict move resize area only on the screen the strut window is on. <a href='https://commits.kde.org/kwin/14c8440f11f9af48e63ca0fcf05ee7988f445b9a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371199'>#371199</a>. See bug <a href='https://bugs.kde.org/370510'>#370510</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6562'>D6562</a></li>
<li>Add ConsoleKit2 support for launching Wayland sessions. <a href='https://commits.kde.org/kwin/6ebc3d65f032ef79f2ce7db0fa86016aadbbf18f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6291'>D6291</a></li>
<li>[autotests] Ensure TestDontCrashUseractionsMenu uses breeze. <a href='https://commits.kde.org/kwin/aa4d12a9062bbd7804ce8539f78d8dc797dabe21'>Commit.</a> </li>
<li>Raise minimum required Mesa version to 10.0. <a href='https://commits.kde.org/kwin/88e56f630b7ee6db8c07e34387c9ebbdb6de5535'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6462'>D6462</a></li>
<li>Remove roundtrip to XServer from Workspace::xStackingOrder. <a href='https://commits.kde.org/kwin/630514d52ab7211f7857a49a907dcde94523495f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6323'>D6323</a></li>
<li>Introduce a method Workspace::markXStackingOrderAsDirty. <a href='https://commits.kde.org/kwin/0d8f11405e1aba37ae144ae8a5e7e24cba85cb74'>Commit.</a> </li>
<li>[platforms/x11] Request OpenGL 2.1 instead of 1.2. <a href='https://commits.kde.org/kwin/be89c16b3884cbf96049d7c2749b90211af482ea'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6401'>D6401</a></li>
<li>Refactor the specification of OpenGL context attributes. <a href='https://commits.kde.org/kwin/3f4995fb9b4fd753a304ea65020bbafdd174936b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6396'>D6396</a></li>
<li>Implement software cursor in OpenGL backend. <a href='https://commits.kde.org/kwin/247ef43f683b4a397bb34ac9e2576cbaae303a48'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6186'>D6186</a></li>
<li>[autotests] Test creating core context profile. <a href='https://commits.kde.org/kwin/004c0f3892bff5ac48b2000a3211c2f7ae7605cb'>Commit.</a> </li>
<li>[autotests] Introduce a SceneOpenGL ES test. <a href='https://commits.kde.org/kwin/66f08399258f91693c71b9a399076307df24ea25'>Commit.</a> </li>
<li>[autotests] Extend pointer constraints test for closing window of locked pointer. <a href='https://commits.kde.org/kwin/e2682c840575ab263bb954059b0f890ea30ea163'>Commit.</a> </li>
<li>Make Workspace::switchWindow operate on AbstractClient. <a href='https://commits.kde.org/kwin/763fbf5fafd01ef7ecf0bc69818da74060d08ce8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6275'>D6275</a></li>
<li>Use AbstractClient instead of Client in placement. <a href='https://commits.kde.org/kwin/92207be904aa92f057c0b65bdef408ce52d32a68'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6274'>D6274</a></li>
<li>[autotest] Disable default TabBox on touch screen edge in ScreenEdgeClientShowTest. <a href='https://commits.kde.org/kwin/56e681823b509c7a5796ff4fc1656881d8f16188'>Commit.</a> </li>
<li>[autotests] Fix ScreenEdgeClientShowTest::testScreenEdgeShowHideX11 on new CI. <a href='https://commits.kde.org/kwin/0364fae4b9930fe3d089a81805745551dfb12f4a'>Commit.</a> </li>
<li>Geometry shape changes also with no resize. <a href='https://commits.kde.org/kwin/8190927eafa8662e4a357204a1bab99e07b21dc2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6229'>D6229</a></li>
<li>Bump version number again. <a href='https://commits.kde.org/kwin/7dabf223009b87f93dba88d8999ec267f4b9ee39'>Commit.</a> </li>
<li>[platforms/drm] Make Atomic Mode Setting the new default. <a href='https://commits.kde.org/kwin/2f8cc2aa752af98dcb33ca1995a5d93d734dd553'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5934'>D5934</a></li>
<li>[DRM plugin] Remove dead code. <a href='https://commits.kde.org/kwin/b3247539e4454c457d43bed94d95387faa133801'>Commit.</a> </li>
<li>Set pid on the ClientConnection backing the PlasmaWindow surface. <a href='https://commits.kde.org/kwin/e7e79124aa1de025599809946daefdc1ac5a0b0f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5756'>D5756</a></li>
<li>Make kwin_wayland remotely debuggable. <a href='https://commits.kde.org/kwin/d8ec2e04663ecf0f5de6cd7a2740a741d35a75ec'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6160'>D6160</a></li>
</ul>


<h3><a name='libkscreen' href='https://commits.kde.org/libkscreen'>libkscreen</a> </h3>
<ul id='ullibkscreen' style='display: block'>
<li>When cloning a config also clone supportedFeatures. <a href='https://commits.kde.org/libkscreen/05498b80525945a21f4cc356a174e9540ca5b695'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7807'>D7807</a></li>
<li>Warnings--;. <a href='https://commits.kde.org/libkscreen/7da742ddd7d05349ec5c84c64600637678cf9f1d'>Commit.</a> </li>
<li>USe Q_DECL_OVERRIDE. <a href='https://commits.kde.org/libkscreen/6a7ed5b586426e49c6092f445edca77bd6be3d6e'>Commit.</a> </li>
<li>Delete registry before connection. <a href='https://commits.kde.org/libkscreen/7e04d0de3beb64caf4674678210f8e0f571bd552'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5814'>D5814</a></li>
<li>Update unit test to match scaling. <a href='https://commits.kde.org/libkscreen/2bafd15756a12417d84f6041cbd759813738cc58'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5813'>D5813</a></li>
</ul>


<h3><a name='libksysguard' href='https://commits.kde.org/libksysguard'>libksysguard</a> </h3>
<ul id='ullibksysguard' style='display: block'>
<li>Libksysguard does not appear to use QtScript, but just includes it. <a href='https://commits.kde.org/libksysguard/b6ed1afd8a8a4104a9fcf09c3322bf6d69231622'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7036'>D7036</a></li>
<li>Inject custom style sheet with system colors. <a href='https://commits.kde.org/libksysguard/3c2eff3dcb6e35bb280fc02a2c4f3421884f80b4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360214'>#360214</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6402'>D6402</a></li>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/libksysguard/24c0b5fd4f7c5aefc3152f1104b3dc2e4934a839'>Commit.</a> </li>
<li>Fix compilation on CentOS 6. <a href='https://commits.kde.org/libksysguard/bb556d8cea2fb77e66970ebc02303becd1c8e8e3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5955'>D5955</a></li>
</ul>


<h3><a name='milou' href='https://commits.kde.org/milou'>Milou</a> </h3>
<ul id='ulmilou' style='display: block'>
<li>Make krunner accessible. <a href='https://commits.kde.org/milou/5946e684f8afbd1d1015bf6a5027c6cdd02bb349'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7766'>D7766</a></li>
<li>Spdx validation. <a href='https://commits.kde.org/milou/da4262a7e31b9fa9605d7be0626b23c96634c183'>Commit.</a> </li>
<li>Remove unused import. <a href='https://commits.kde.org/milou/71b986eae200fa3993cd67b561e5743b861d88de'>Commit.</a> </li>
<li>Remove unused import. <a href='https://commits.kde.org/milou/b0ee7ad990628de8c1fcb1b3d843fac70f1d9f97'>Commit.</a> </li>
<li>[ResultDelegate] Enforce PlainText. <a href='https://commits.kde.org/milou/00bdd9a58abe71191e60ee330f6b83a124f3da2e'>Commit.</a> </li>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/milou/c53338af10b766c25cd13f4659c2f4c6d7585d85'>Commit.</a> </li>
</ul>


<h3><a name='oxygen' href='https://commits.kde.org/oxygen'>Oxygen</a> </h3>
<ul id='uloxygen' style='display: block'>
<li>Renamed oxygen specific KCMs. <a href='https://commits.kde.org/oxygen/bf1fa4a8ba0978e053dd067aabfd2553f48ffe93'>Commit.</a> See bug <a href='https://bugs.kde.org/383283'>#383283</a></li>
<li>- hide shadow when mask is empty. <a href='https://commits.kde.org/oxygen/9d0123305d7d3d4916c0b4f3b1cf3d60c95489b7'>Commit.</a> See bug <a href='https://bugs.kde.org/379790'>#379790</a></li>
<li>Fixed warning about unused variable. <a href='https://commits.kde.org/oxygen/201044101dc9702f951d1cb91c7939420570099e'>Commit.</a> </li>
<li>Removed double ;;. <a href='https://commits.kde.org/oxygen/b6d3cd29d4e4c99e87b4a7c2df3fd206343e2aa1'>Commit.</a> </li>
<li>Fixed calculation of top border. <a href='https://commits.kde.org/oxygen/632c993542a1d966f098ef7c5bf3ca52828eca6f'>Commit.</a> </li>
<li>Partially revert "Use Q_DECL_OVERRIDE". <a href='https://commits.kde.org/oxygen/cbc5fdbd4654118c6ec37776b060420e520c216b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380452'>#380452</a></li>
<li>Q_DECL_OVERRIDE -> override. <a href='https://commits.kde.org/oxygen/8a4e31bc99ed5ada95f2ebf1eb8151c52a1d5420'>Commit.</a> See bug <a href='https://bugs.kde.org/380452'>#380452</a></li>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/oxygen/1bf982f43470c9441287f3fd6a8a3dee834e1483'>Commit.</a> </li>
<li>Set a mask to shadow widget to make sure that it does not overlap with the mdi window. <a href='https://commits.kde.org/oxygen/ac3136236da1854262387d2f24409b3bf6aeaca9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379790'>#379790</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>[Folder View] Fix supported drag actions. <a href='https://commits.kde.org/plasma-desktop/6b8fa112f47d6ed5794167b737007e35a7817746'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382797'>#382797</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7798'>D7798</a></li>
<li>[Folder View Icons Config] Fix initial index of ComboBox. <a href='https://commits.kde.org/plasma-desktop/4bf0358af9b86d96780a944e1510ed08d7959156'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7733'>D7733</a></li>
<li>Fix minor typo. <a href='https://commits.kde.org/plasma-desktop/4f81e514b7625583159b7bdbdb82e9a5fd618a86'>Commit.</a> </li>
<li>Add a button to launch orca configuration in kcm. <a href='https://commits.kde.org/plasma-desktop/d88a3026e37108da4d9e4d2f4869c6d7c921574a'>Commit.</a> </li>
<li>Try harder to launch orca by calling it with --replace. <a href='https://commits.kde.org/plasma-desktop/df0a010918f647d6f465c6497ac9a15ea7fd4731'>Commit.</a> </li>
<li>Do not use A as accellerator since that should be apply. <a href='https://commits.kde.org/plasma-desktop/33bff8ce66e2a6053681e8baff09c6b38e46d7e7'>Commit.</a> </li>
<li>Add missing accelerator for Modifier keys. <a href='https://commits.kde.org/plasma-desktop/bbe71381e475f48a19dee59f5fd23a346b287510'>Commit.</a> </li>
<li>Per-activity favorites (Final, again?). <a href='https://commits.kde.org/plasma-desktop/4ba9b01e2d6acc361f4ac0c7e4c21066eb575146'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D3805'>D3805</a></li>
<li>[Containment Actions Config] Cleanup layout and support multiple modifiers. <a href='https://commits.kde.org/plasma-desktop/b20d5297ed49381af44bb412820038eeebe251aa'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7624'>D7624</a></li>
<li>[Task Manager] Strip applications: scheme from launcher URL in MPris source. <a href='https://commits.kde.org/plasma-desktop/d711ff0c357a86ae86596abf0aa9b60a3739cafd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7705'>D7705</a></li>
<li>[Task Manager] Honor "kde:pid" Metadata for when window PID isn't player PID. <a href='https://commits.kde.org/plasma-desktop/d094bd37ea90a7b6db09450030616d393ee3691c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6359'>D6359</a></li>
<li>[Task Manager] Decode applications URL before dragging. <a href='https://commits.kde.org/plasma-desktop/b293137ed955e076d72f47c86ea1a8539632bf5a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7639'>D7639</a></li>
<li>[Folder View] Accept and reject Escape event appropriately. <a href='https://commits.kde.org/plasma-desktop/9646aec02c3b72d1bb74d89ed4ed86d9e93ce452'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352988'>#352988</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7631'>D7631</a></li>
<li>[Folder View] Also handle Enter key (on numpad) to run files and cd into folders. <a href='https://commits.kde.org/plasma-desktop/eca3cb1fecf44b2d6383398630483c8a3f331a10'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7632'>D7632</a></li>
<li>Support edit and appstream actions also for application search results. <a href='https://commits.kde.org/plasma-desktop/ee7078864b750b91812269b7cbc43bd4544c98f8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7567'>D7567</a></li>
<li>[Folder View] Delay delegate creation until plasmoid expands. <a href='https://commits.kde.org/plasma-desktop/3a5369e86f33006e04d3d21d42c22cc37602db1f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7586'>D7586</a></li>
<li>[Folder Model] Introduce status enum. <a href='https://commits.kde.org/plasma-desktop/c77e9542005f83fe1c9a2840af1bdb0c8f9d9c7a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7598'>D7598</a></li>
<li>Use correct spdx syntax for appstream happyness. <a href='https://commits.kde.org/plasma-desktop/be558d2c8c9514731a2e2c30e9d87e3e83befd6f'>Commit.</a> </li>
<li>Revert "set nodisplay to stop appstream complaining that it's not valid". <a href='https://commits.kde.org/plasma-desktop/75198a80255c916faf0dc2a738af88718f65b43a'>Commit.</a> </li>
<li>Revert "set nodisplay to stop appstream trying to extract it". <a href='https://commits.kde.org/plasma-desktop/52fd93af6e126d659a45042844dc29d8f6ea1733'>Commit.</a> </li>
<li>Update appimage file for valid screenshot and URL. <a href='https://commits.kde.org/plasma-desktop/74f054ccc5809e3aebe51efee036c6a4b8070730'>Commit.</a> </li>
<li>Set nodisplay to stop appstream trying to extract it. <a href='https://commits.kde.org/plasma-desktop/c39c0182b774da65c1797b63029819d5f41c928e'>Commit.</a> </li>
<li>Set nodisplay to stop appstream complaining that it's not valid. <a href='https://commits.kde.org/plasma-desktop/680de17a6fa3e416b11c3655d0e70d979085bacc'>Commit.</a> </li>
<li>Fix license tags for spdx complianse to keep appstream happy. <a href='https://commits.kde.org/plasma-desktop/05c230733a1b9d16c0e52eb54955c62d340b94e3'>Commit.</a> </li>
<li>Remove unused part of an expression. <a href='https://commits.kde.org/plasma-desktop/702e59ee6bee0d3fe1a91da59fff5ae08e6785b2'>Commit.</a> </li>
<li>Understand KAStats-style applications: launcher URLs. <a href='https://commits.kde.org/plasma-desktop/01095327f0142feda47831b8c5ec6dcef5d3841c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7562'>D7562</a></li>
<li>[Folder View] Implement Select All shortcut. <a href='https://commits.kde.org/plasma-desktop/ab3ede4f79a09323b850a46061f01ecff509bc69'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6881'>D6881</a></li>
<li>Don't special case file URLs when processing .desktop files. <a href='https://commits.kde.org/plasma-desktop/09624c713e63563b740385ade86414b9782263ea'>Commit.</a> See bug <a href='https://bugs.kde.org/379994'>#379994</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7535'>D7535</a></li>
<li>Use icon chooser in Kicker and Dashboard configuration to select custom icon. <a href='https://commits.kde.org/plasma-desktop/b46cd1ed167c0bc69bdcef45877b3e4727767354'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7469'>D7469</a></li>
<li>[Folder View] Clear selection on Escape. <a href='https://commits.kde.org/plasma-desktop/07c2bf0c53d5b783bd16e5e202f9b74c601b7174'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7511'>D7511</a></li>
<li>Kimpanel: change highlight color. <a href='https://commits.kde.org/plasma-desktop/5377b6b03b8d31dc40323b2071fc0b7176819849'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7436'>D7436</a></li>
<li>Keep default toolbar font in fonts KCM in sync with plasma-integration. <a href='https://commits.kde.org/plasma-desktop/bc16cc4059664c69dc16b53daacf2a25770ecd36'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6698'>D6698</a></li>
<li>[Style KCM] Remove message box about changes applying only to newly started applications. <a href='https://commits.kde.org/plasma-desktop/51b8518c5e377403c8f18bb456adb747f61dea18'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7345'>D7345</a></li>
<li>Collapse favorites sidebar when empty. <a href='https://commits.kde.org/plasma-desktop/eedeb3c2a6b407423deac0cb71da84f75f33a662'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383353'>#383353</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7321'>D7321</a></li>
<li>Use new libtm API to enable DND reordering within group dialogs. <a href='https://commits.kde.org/plasma-desktop/7db2a6fbe394326777a771b9461b989606a6a2fa'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7268'>D7268</a></li>
<li>Don't show appstream action if we can't find an app to open appstream:// URLs. <a href='https://commits.kde.org/plasma-desktop/4ff8801450c63d6efccad57e6b958dc1d2e7d70b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7227'>D7227</a></li>
<li>Move a KIO KDirLister test for kio_fonts here, where it belongs. <a href='https://commits.kde.org/plasma-desktop/7ecafcf272527fa317c645d3e88ce46426b8b916'>Commit.</a> </li>
<li>Don't require X11 to save font DPI to config. <a href='https://commits.kde.org/plasma-desktop/f97930a8cc3b620a2b780ebf0df685ba36188cfa'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7205'>D7205</a></li>
<li>[Task Manager] Remove unused import. <a href='https://commits.kde.org/plasma-desktop/a962ae5d39b01180daa6b99ce17f18c5b48e6b0c'>Commit.</a> </li>
<li>Terminology fixes in KCMs: KDE-as-the-desktop -> Plasma. <a href='https://commits.kde.org/plasma-desktop/7f9e7c8549368211a3227f57f23518376fa2c34a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6469'>D6469</a></li>
<li>[Window List Plasmoid] Use plasmoid icon and text from metadata. <a href='https://commits.kde.org/plasma-desktop/7148ad43551d664c556f2a579fd8d5e6801bfed7'>Commit.</a> </li>
<li>[Folder View Config] Replace FilterableMimeTypesModel by Plasma SortFilterModel. <a href='https://commits.kde.org/plasma-desktop/103c19119d67d5306ea22c723ef4e73e8e366c5c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7064'>D7064</a></li>
<li>[Folder View Config] Make model writable and drop custom setRowChecked method. <a href='https://commits.kde.org/plasma-desktop/76af2694e937d81ae736aa8a5d85e3654ab21f10'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6938'>D6938</a></li>
<li>Stop unnecessarily killing IBus when applying keyboard layouts config. <a href='https://commits.kde.org/plasma-desktop/999568ada13bc99ad360a4eaa03999821c2c8cc2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6956'>D6956</a></li>
<li>Add link about additional configuration option to baloo kcm docbook. <a href='https://commits.kde.org/plasma-desktop/01e8035b4593d60a10a37a1ee5834c6e9539a4aa'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6957'>D6957</a></li>
<li>Update tooltip in pager when name changes. <a href='https://commits.kde.org/plasma-desktop/690db6bc671b4cd6e94653c4660074a3dd160964'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382743'>#382743</a></li>
<li>[Component Chooser] Update http(s) scheme handler when changing default browser. <a href='https://commits.kde.org/plasma-desktop/1193971e9a270d37970cf7799444773be68576c6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/332817'>#332817</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6933'>D6933</a></li>
<li>Fix minor typo. <a href='https://commits.kde.org/plasma-desktop/027186f9278c9ad156bc5355fe83b82a06e27e66'>Commit.</a> </li>
<li>Add section about screen reader to kcmaccess kcm docbook. <a href='https://commits.kde.org/plasma-desktop/577a208b03e884fdec348d0fcf284a8e22c06302'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382207'>#382207</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6886'>D6886</a></li>
<li>[Component Chooser] Add ComboBox with browsers. <a href='https://commits.kde.org/plasma-desktop/4cd9302762c6958ebf94bd90d0548344eba277e7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6917'>D6917</a></li>
<li>[Task Manager] Remove unused import. <a href='https://commits.kde.org/plasma-desktop/6b1c0be0665dc3636489b5d04535dd1fcc153537'>Commit.</a> </li>
<li>[Pager] Remove unused utils.js. <a href='https://commits.kde.org/plasma-desktop/4bcbecd2e913e56a73494201eaf4b3e7372fe1a1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6898'>D6898</a></li>
<li>Some clazy level 0+1 fixes. <a href='https://commits.kde.org/plasma-desktop/fb22c23d0a596ad852d21869e93ac87890cd04ad'>Commit.</a> </li>
<li>Fix and normalize license in .desktop files. <a href='https://commits.kde.org/plasma-desktop/782cb4d27e7b71912bbbe0ca363dade6bdd82976'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6628'>D6628</a></li>
<li>[ConfigurationContainmentActions] Pass Configure button as context for showConfiguration. <a href='https://commits.kde.org/plasma-desktop/334c5aa4902b351a84726d4c06b745ae444e2927'>Commit.</a> </li>
<li>[Folder View] Replay mouse click so left button containment actions work. <a href='https://commits.kde.org/plasma-desktop/6f19967d6078472f3d41347089bbd2fe0dce24f4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357367'>#357367</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6552'>D6552</a></li>
<li>[Task Manager] Also respect visible property of configure action. <a href='https://commits.kde.org/plasma-desktop/95efcd389e0e4f3a16ff33420e0e8c504c19ab5f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6551'>D6551</a></li>
<li>Fix setting natural/inverted scroll mode for touchpad with libinput. <a href='https://commits.kde.org/plasma-desktop/8132e02bb8e8c206820ad17fc45d39fccb3c6f75'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6488'>D6488</a></li>
<li>Add standard keyboard shortcut for refresh. <a href='https://commits.kde.org/plasma-desktop/1aa12f1d042b6937d4cb81d8183f8a11ab86a50d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6447'>D6447</a></li>
<li>Fix Mac config (see commit 184cf399). <a href='https://commits.kde.org/plasma-desktop/a7b8fec293d496f32d94fcefc43a665638686013'>Commit.</a> </li>
<li>[ConfigurationContainmentActions] Anchor left "Add Action" button. <a href='https://commits.kde.org/plasma-desktop/a654ee620836b31ad1cfba249903bd9d6ba8a3af'>Commit.</a> </li>
<li>[KRDB] Read kdeglobals font and fallback to hardcoded default if neccessary. <a href='https://commits.kde.org/plasma-desktop/978700d9b65b6118e93a87bc7fbe9c113ca7d2ab'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378262'>#378262</a></li>
<li>Fix warning 'returning reference to temporary'. <a href='https://commits.kde.org/plasma-desktop/838777eaa6f04f87daf59451bf0cdf9725001527'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6332'>D6332</a></li>
<li>[FolderView] Don't calculate extra spacing when we don't need to. <a href='https://commits.kde.org/plasma-desktop/33793e9e2f5675cb3b632b011321c264db5504ba'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6322'>D6322</a></li>
<li>Don't busy-loop in preview popups. <a href='https://commits.kde.org/plasma-desktop/f49ca6ee277142539e0302f2848c79ff41d319cf'>Commit.</a> </li>
<li>[Folder View] Pad cellWidth/cellHeight with the extra space. <a href='https://commits.kde.org/plasma-desktop/20ba491cb6bdb0e136ba0573d0a7dc3a8ac06d62'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6188'>D6188</a></li>
<li>The session is not called KDE. <a href='https://commits.kde.org/plasma-desktop/c1da4a8dce02d30cb7b22152392125c5dd852137'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6137'>D6137</a></li>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/plasma-desktop/44051d03232e11ceb7e2b0567662a638caf16156'>Commit.</a> </li>
<li>Use right QProcess API. <a href='https://commits.kde.org/plasma-desktop/7a4fdbbd6124757ccd9d07fabd8e82ac62bc8995'>Commit.</a> </li>
<li>Implement the new panel icon size ceiling also for Input Method Panel. <a href='https://commits.kde.org/plasma-desktop/a0dee59dcf28a530741a0ff6c71aef782feba9e4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378443'>#378443</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5962'>D5962</a></li>
<li>Fix typo {L->l}ookupTableVisible. <a href='https://commits.kde.org/plasma-desktop/e31896432a2233207bb2eea470cfb47bb73194ff'>Commit.</a> </li>
<li>[kimpanel] correctly hide candidate when LookupTableVisible is false. <a href='https://commits.kde.org/plasma-desktop/f91d2b30f5ea52b9fdcc457981c10502fc613855'>Commit.</a> </li>
<li>Only defer initial refresh when actually instanciated from QML. <a href='https://commits.kde.org/plasma-desktop/6a76ce0b4fbdf3c17c37967add7a1f22c35d5059'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379783'>#379783</a></li>
<li>Update ungrab mouse hack for Qt5.8. <a href='https://commits.kde.org/plasma-desktop/03006fd66ac1c79421a51751fa32dc86bc79bd33'>Commit.</a> </li>
</ul>


<h3><a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a> </h3>
<ul id='ulplasma-integration' style='display: block'>
<li>Add ~/.local/share/icons to icons search paths. <a href='https://commits.kde.org/plasma-integration/f9e5b852faa78f6d67615ea3c435b10b393cdaa5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5093'>D5093</a></li>
<li>Group find_package(KF5*) calls together. <a href='https://commits.kde.org/plasma-integration/a12e9ea504c67368bf9411dd91a5d32b6af3c59d'>Commit.</a> </li>
<li>Fix faulty merge. <a href='https://commits.kde.org/plasma-integration/1e083f8822f2c12f12796749295103cb5ada154a'>Commit.</a> </li>
<li>Allow to change toolbar font separately again. <a href='https://commits.kde.org/plasma-integration/86be8d49d8988fb6c35e847ed9e0aad3e8514208'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358254'>#358254</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6697'>D6697</a></li>
<li>Changing the "Show icons in menus" setting will now immediately be applied to running applications. <a href='https://commits.kde.org/plasma-integration/2434bafcb168467f6763a20226918d27afa26744'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7344'>D7344</a></li>
<li>Also specify a default StyleName for fonts. <a href='https://commits.kde.org/plasma-integration/1b21b5977c2068c5bd30c9f9f641f60bdba9ea8e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383191'>#383191</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7160'>D7160</a></li>
<li>Middle-click on QSystemTrayIcon сauses context menu. <a href='https://commits.kde.org/plasma-integration/b0059b1c8342b5ef95b054a0a5081b5db7e1c7bb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382855'>#382855</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7013'>D7013</a></li>
<li>Introduce KDE_NO_GLOBAL_MENU env variable to disable global menu. <a href='https://commits.kde.org/plasma-integration/e912ec46eaf620eacfbff37a0d85e408350427d5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6310'>D6310</a></li>
<li>Check window visibility at expose event. <a href='https://commits.kde.org/plasma-integration/7ace568d23745f796289841abe7cbe7e24a55cd3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6214'>D6214</a></li>
<li>Allow to disable blinking cursor completely. <a href='https://commits.kde.org/plasma-integration/4be9b478e5296914258b77eb02c2583ee0e84c7c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5791'>D5791</a></li>
<li>Fix deprecation warnings. setSelection -> setSelectedUrl ui -> uiDelegate. <a href='https://commits.kde.org/plasma-integration/f25c5e10d0235c2d383e998bddc39fb35517ce9e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5743'>D5743</a></li>
<li>Replace Q_DECL_OVERRIDE with override. <a href='https://commits.kde.org/plasma-integration/4aef17e64f564331c79d5307dd6aeded828bf382'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5742'>D5742</a></li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Drop the old legacy connection editor. <a href='https://commits.kde.org/plasma-nm/f997b4512eda6946e2ff04ff5c02ab1d1ee888bb'>Commit.</a> </li>
<li>Add option to allow managing virtual connections. <a href='https://commits.kde.org/plasma-nm/4634aa6e68e2e4d5a1177a3ee90a6461a9e3f44a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376664'>#376664</a></li>
<li>Summary: L2TP: pre-sharedkey should be mask. <a href='https://commits.kde.org/plasma-nm/c86bb0d58380ffd71df47837b1363c6afbdbae25'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6520'>D6520</a></li>
<li>Add support for fortisslvpn. <a href='https://commits.kde.org/plasma-nm/78b78cf3dedf7423ddd1c13388cfd381a768f42d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6189'>D6189</a></li>
<li>UI updates for NetworkManager-l2tp 1.2.6. <a href='https://commits.kde.org/plasma-nm/1adb364664753f342c7b221687ca68ce4a5ec79e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6111'>D6111</a></li>
<li>Add missing file with UI for configuration. <a href='https://commits.kde.org/plasma-nm/844b3983754543e4c2565c06560ac26738136526'>Commit.</a> </li>
<li>Allow to have wider password dialog while preffering minimum size. <a href='https://commits.kde.org/plasma-nm/aba4b9c4ed6216d49299084037100716422a0103'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380399'>#380399</a></li>
<li>Openconnect: make sure we accept the dialog. <a href='https://commits.kde.org/plasma-nm/ebfbe93510d7e5a10992a482efe38bc934dd19c5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380299'>#380299</a></li>
<li>Openconnect: Add option to select protocol. <a href='https://commits.kde.org/plasma-nm/0355bee0ac241a20095d56288d1d2604cca9a155'>Commit.</a> </li>
<li>Properly pass specific vpn type when selecting new connection by double click. <a href='https://commits.kde.org/plasma-nm/480a9e2bdfb2043f3af685944274d694e08c9ed8'>Commit.</a> </li>
<li>Openconnect (juniper): Properly make sure we are compatible with the rest of nm tools. <a href='https://commits.kde.org/plasma-nm/eadad496c4e33b0071f8816255a44cfc9a62b976'>Commit.</a> </li>
<li>Openconnect (juniper): Make sure we are compatible with the rest of nm tools. <a href='https://commits.kde.org/plasma-nm/7fe2a9622b24667cd5ad1c78e85fae9b4e14e264'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380244'>#380244</a></li>
<li>Add option to disable unlocking modem on detection. <a href='https://commits.kde.org/plasma-nm/743fec5a84c9dc6b62dcbdb15d14ecf3b3c4214d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380150'>#380150</a></li>
</ul>


<h3><a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>[ListItemBase] Use section instead of disabled menu item. <a href='https://commits.kde.org/plasma-pa/31fed95b4de6d6f3323f11a38abf799c1dcf1440'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7723'>D7723</a></li>
<li>StreamRestore: Always set channel count to 1. <a href='https://commits.kde.org/plasma-pa/792248092d25512ef2fe37ff4c79c02242832626'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383787'>#383787</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7485'>D7485</a></li>
</ul>


<h3><a name='plasma-sdk' href='https://commits.kde.org/plasma-sdk'>Plasma SDK</a> </h3>
<ul id='ulplasma-sdk' style='display: block'>
<li>Spdx validation for appstream. <a href='https://commits.kde.org/plasma-sdk/4b54a83404ab7c9e5041bb2769059e6da62aebd4'>Commit.</a> </li>
<li>Drop unused dependencies KF5WindowSystem and KF5XmlGui. <a href='https://commits.kde.org/plasma-sdk/4b710be6c5b9e0535099518aa6e017fb131ef6ff'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6494'>D6494</a></li>
<li>Restore KCompletion dependency. <a href='https://commits.kde.org/plasma-sdk/807bfc2d7006f8b3ad3c8ee0f4669e6f90848fbb'>Commit.</a> </li>
<li>Drop unused dependencies. <a href='https://commits.kde.org/plasma-sdk/d63149e27cafea8fb81f9271fb11c71de3821ea5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6221'>D6221</a></li>
</ul>


<h3><a name='plasma-tests' href='https://commits.kde.org/plasma-tests'>plasma-tests</a> </h3>
<ul id='ulplasma-tests' style='display: block'>
<li>Test every plasmoid. <a href='https://commits.kde.org/plasma-tests/f00fdd4299ef4fd564b88011a7a760a288d85a98'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6134'>D6134</a></li>
<li>Test every plasmoid. <a href='https://commits.kde.org/plasma-tests/31c85ca49872f2f87db31efdc327e7db9e766ccd'>Commit.</a> </li>
<li>Add a notifications plasmoid test. <a href='https://commits.kde.org/plasma-tests/64b939e8e7c4f39037a04ba5255b356da79f6e73'>Commit.</a> </li>
<li>Remove unneeded check, we already depend on 5.6.1. <a href='https://commits.kde.org/plasma-tests/a8decf762af247a2863575f80e9bee758dbfdaa2'>Commit.</a> </li>
</ul>


<h3><a name='plasma-vault' href='https://commits.kde.org/plasma-vault'>Plasma Vault</a></h3>
<ul id='ulplasma-vault' style='display: block'><li>New in this release</li></ul>
<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Accessibility in lock screen UI. <a href='https://commits.kde.org/plasma-workspace/d8cb98e76900ca61eea00e545fdef369d1a787a1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7789'>D7789</a></li>
<li>Make krunner accessible. <a href='https://commits.kde.org/plasma-workspace/0619c45422ad331c90cfb242bc563114cafb5fbd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7765'>D7765</a></li>
<li>Make sure the plasma interface is created before going further. <a href='https://commits.kde.org/plasma-workspace/dea32eb18e9c7f4797b0fa7149c8f4cf660a8d93'>Commit.</a> </li>
<li>Don't set QT_SCREEN_SCALE_FACTORS on wayland. <a href='https://commits.kde.org/plasma-workspace/be7f7b0772a4922ac95b2ef64ff16ae8d2782996'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7781'>D7781</a></li>
<li>[Digital Clock] Compact and move to the end font setup. <a href='https://commits.kde.org/plasma-workspace/a8ae90067183ae5174c3e18aea566fce5d9f1b07'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7633'>D7633</a></li>
<li>Add readme in startkde folder. <a href='https://commits.kde.org/plasma-workspace/2a5ffa9cded04c8a0f9c160f35803325a72ec1bc'>Commit.</a> </li>
<li>Register objectPath before service. <a href='https://commits.kde.org/plasma-workspace/7f270efe9c4aeec63ca1b5dacf13e395732b38af'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7596'>D7596</a></li>
<li>[SolidDeviceEngine] Use QDateTime::currentTimeUtc. <a href='https://commits.kde.org/plasma-workspace/f37c17674e6540a8b8a2cf06720596ba9f3b7c4d'>Commit.</a> </li>
<li>[CurrentContainmentActionsModel] Emit configurationChanged when action is added/removed/edited. <a href='https://commits.kde.org/plasma-workspace/3f093c395b1da2316037e514f424d305fc4cf779'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7626'>D7626</a></li>
<li>[DesktopView] Move cancelling showing desktop into keyPressEvent. <a href='https://commits.kde.org/plasma-workspace/55d74c68219781c8770b1a22ecd5155e9908ccb8'>Commit.</a> See bug <a href='https://bugs.kde.org/352988'>#352988</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7630'>D7630</a></li>
<li>[CurrentContainmentActionsModel] Add configurationChanged signal. <a href='https://commits.kde.org/plasma-workspace/3257f5f923733127af1cbdf108ac2d7cf2b9749b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6443'>D6443</a></li>
<li>[Digital Clock] Show event plugin configurations only if enabled. <a href='https://commits.kde.org/plasma-workspace/03b37c94bd3bd615e43c88dfdb74928b2d06c396'>Commit.</a> See bug <a href='https://bugs.kde.org/372090'>#372090</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7603'>D7603</a></li>
<li>[KRunner] Remove copy of dialog.h. <a href='https://commits.kde.org/plasma-workspace/5eaef24532061d6b2d399fdee6940dc7be60517a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7599'>D7599</a></li>
<li>SNI KDED: register DBus object before service name. <a href='https://commits.kde.org/plasma-workspace/d53889eca2586809c2aaaa823948c09b11a40f62'>Commit.</a> </li>
<li>Register DBus object before registering the service name. <a href='https://commits.kde.org/plasma-workspace/629e534fe7edc63fe1b84d7d8455a954c7619ff2'>Commit.</a> </li>
<li>Guard Applet->property("graphicsitem") in corona tests. <a href='https://commits.kde.org/plasma-workspace/9f6db26dccc98abf2d500ec02eddf2e96d6471b1'>Commit.</a> </li>
<li>Register klipper DBus interface when in non-standalone mode. <a href='https://commits.kde.org/plasma-workspace/6fb7ed2384dbee0f4da34ac200c9ea38fc25b0eb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7536'>D7536</a></li>
<li>Port Appmenu model to QDBusServiceWatcher. <a href='https://commits.kde.org/plasma-workspace/c453e9b5fb7740716fe146c6b00feba7439c159a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7611'>D7611</a></li>
<li>Avoid absolute paths to .desktop files in launcher URLs. <a href='https://commits.kde.org/plasma-workspace/a47d1f19e1e0bda300ff799dfc6f377802a2a678'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7561'>D7561</a></li>
<li>Fix build without libqalculate. <a href='https://commits.kde.org/plasma-workspace/2aa504340e68ac11ac68a7b9573354ad431f2995'>Commit.</a> </li>
<li>Don't search for and link to libcln when using libqalculate>=2.0. <a href='https://commits.kde.org/plasma-workspace/b915e5b4bab10af938441ac5cae8e62cf6bbb843'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7590'>D7590</a></li>
<li>Fix SPDX syntax http://metadata.neon.kde.org/appstream/html/xenial/main/issues/plasma-workspace.html. <a href='https://commits.kde.org/plasma-workspace/b0e8b75b85d6008c88282df58c3341a3f9e7e2dc'>Commit.</a> </li>
<li>[Digital Clock] Use invalidateFilter instead of invalidate in TimeZoneModel. <a href='https://commits.kde.org/plasma-workspace/273c265cd7c01f8b76f0d3032befb2bfdf5bad11'>Commit.</a> </li>
<li>[MPris Engine] Don't construct QDBusObjectPath in Metadata update. <a href='https://commits.kde.org/plasma-workspace/1a95e7c7e14643dfc297208dd8ad1601cc71d538'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383557'>#383557</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7538'>D7538</a></li>
<li>[Icon Applet] Set busy to false when copying failed. <a href='https://commits.kde.org/plasma-workspace/a243eb87674a07338e9317486edda6746adeb001'>Commit.</a> </li>
<li>[Notifications Applet] Don't explicitly set Plasmoid.icon. <a href='https://commits.kde.org/plasma-workspace/1b56c4b7a925d94bcb2d0535d7a98ea34368d45d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375093'>#375093</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7542'>D7542</a></li>
<li>[Recent Documents Runner] Avoid duplicate results. <a href='https://commits.kde.org/plasma-workspace/9583c7df0492411eb697e5e5babb08abdb16c4a2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381665'>#381665</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7476'>D7476</a></li>
<li>Drop unnecessary dependency on KF5XmlRpcClient. <a href='https://commits.kde.org/plasma-workspace/d545538e5a9dd83ab5ac35f0b440694b21e7ebdf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7506'>D7506</a></li>
<li>[PanelShadows] Update m_windows in setEnabledBorders. <a href='https://commits.kde.org/plasma-workspace/4ec4a739a34504a811ce6b9f9f34cb145c067dcd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7505'>D7505</a></li>
<li>[Device Notifier] Set preferred size for ActionItem. <a href='https://commits.kde.org/plasma-workspace/b8f263cd9939530a6634a1ddb9533bf473a67805'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382541'>#382541</a></li>
<li>Don't elide UserDelegate text when there's only one item. <a href='https://commits.kde.org/plasma-workspace/f7cf3c2a68e609afebada4c781a116d2a5d2740a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7472'>D7472</a></li>
<li>Remove pointless data copy in lockscreen delegate. <a href='https://commits.kde.org/plasma-workspace/cc8647481095f5631ef8136945ee5d2d06981fd6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7473'>D7473</a></li>
<li>Run the Baloo runner out of process. <a href='https://commits.kde.org/plasma-workspace/76d41e0831cf1cc9755a0ff0326b992796480d62'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7404'>D7404</a></li>
<li>Notifications: Correctly display the file in jobs. <a href='https://commits.kde.org/plasma-workspace/403b7348e1687c4fd633df5ee4d57d2728f33d7f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4879'>D4879</a></li>
<li>Workaround bug in QT_QUICK_CONTROLS_1_STYLE usage in Qt. <a href='https://commits.kde.org/plasma-workspace/dda29726a0fe9fdd87bc11d110da68ce4833c887'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7381'>D7381</a></li>
<li>[Plasma Windowed] Use setSize instead of setWidth + setHeight. <a href='https://commits.kde.org/plasma-workspace/30cd11c7e2857d87c873eebbaad64bdcc7d2a219'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7365'>D7365</a></li>
<li>Remove application directory from QCoreApplication::libraryPaths(). <a href='https://commits.kde.org/plasma-workspace/cf572c3b5224b1edd849e7c71fde470287cda6ba'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7255'>D7255</a></li>
<li>[Image Wallpaper] Allow dropping images and folders into config dialog. <a href='https://commits.kde.org/plasma-workspace/e73ae4f188da47fbfb0abef0766f7a50b60ed742'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6568'>D6568</a></li>
<li>Optionally show a history of notifications. <a href='https://commits.kde.org/plasma-workspace/28d86ab4356737260e054009795e595fbb069158'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7271'>D7271</a></li>
<li>[Notifications] Remove unused notificationIcon property. <a href='https://commits.kde.org/plasma-workspace/4b16249657077a54430fe60ccda9fd46b68f4ef3'>Commit.</a> </li>
<li>Set the QtQuick Controls 1 style name explicitly. <a href='https://commits.kde.org/plasma-workspace/053d54488cdd4c6735998fb3a231b144f69ecf66'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7257'>D7257</a></li>
<li>Add support for moving group members within a group. <a href='https://commits.kde.org/plasma-workspace/592dc9e0bc79e11664c880eb9385ab1b76188af5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7267'>D7267</a></li>
<li>Remove drkonqi from p-w. <a href='https://commits.kde.org/plasma-workspace/cd63a2ade3ae299c417db63a73fb6f9fcfa16c42'>Commit.</a> </li>
<li>Give wallpaper packages a comment. <a href='https://commits.kde.org/plasma-workspace/1e9591ae400ac8c7ad12d095ca34c81514bb21c2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7070'>D7070</a></li>
<li>Summary: change user visible text to use modern branding. <a href='https://commits.kde.org/plasma-workspace/8e7e96db2683cd6989ffa61d5f09b6216c2ec81a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7202'>D7202</a></li>
<li>Set a default font DPI if not set. <a href='https://commits.kde.org/plasma-workspace/7cfdd9a5260b8bc6140a85270a6d6bb0cc98e009'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374978'>#374978</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7185'>D7185</a></li>
<li>Determine if we should use Qt scaling based on env var. <a href='https://commits.kde.org/plasma-workspace/0fa58f0abc716484e0420401aa5b6087b03c73f2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7200'>D7200</a></li>
<li>Add missing include. <a href='https://commits.kde.org/plasma-workspace/bbdcf72ab8d60335529434e8bba83fb4c76bc77c'>Commit.</a> </li>
<li>Use Qt scaling in Plamsa. <a href='https://commits.kde.org/plasma-workspace/bbf7aaf3448532b38819456fbe88ed866fb6fec0'>Commit.</a> See bug <a href='https://bugs.kde.org/356446'>#356446</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7188'>D7188</a></li>
<li>[MPRIS Data Engine] Remove duplicate capFromName. <a href='https://commits.kde.org/plasma-workspace/86e5422beeaee2c6d61918e5e1fea8cd0969a84b'>Commit.</a> </li>
<li>Fix QSortFilterProxyModelPrivate::updateChildrenMapping crash in libtaskmanager. <a href='https://commits.kde.org/plasma-workspace/3990ec2358106875bd1d58ad65bd2a55ff4f1d73'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381006'>#381006</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7139'>D7139</a></li>
<li>Keep fallback icon updated. <a href='https://commits.kde.org/plasma-workspace/75b7f5c2fa63bc1ef13ac6fb5f82c7d52bfabd6c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383017'>#383017</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7092'>D7092</a></li>
<li>[Wallpaper Configuration] Make Get New Stuff dialog modal to config window. <a href='https://commits.kde.org/plasma-workspace/3558afa9579edffb5f86d36973cf42c560126569'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7073'>D7073</a></li>
<li>[Wallpaper Configuration] Hide Get New Stuff if disabled by KIOSK. <a href='https://commits.kde.org/plasma-workspace/bd468dced14e2aa32889fdbf9b3b87b736a5e681'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7086'>D7086</a></li>
<li>Always install policykit-kde docs as the translations are always installed anyway and build is now fixed. <a href='https://commits.kde.org/plasma-workspace/b23416612848d5e8e1d7c2c6c8206b09003887d8'>Commit.</a> </li>
<li>[Widget Explorer] Hide "Download New Widgets" if GHNS is disabled by KIOSK restriction. <a href='https://commits.kde.org/plasma-workspace/6d3f29161c523aef2db2aa439ec48a5e8ac0f6a8'>Commit.</a> </li>
<li>Warning--. <a href='https://commits.kde.org/plasma-workspace/1bd3145694b6d3ee8cf61902c803f64cf8baeab0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7065'>D7065</a></li>
<li>Add missing SUBDIR for PolicyKit-kde documentation. <a href='https://commits.kde.org/plasma-workspace/44a3a193c3b99189687b8362b78be1a82bf1ff8a'>Commit.</a> </li>
<li>Revert "remove conditional installation of docs, it has no effect on translations". <a href='https://commits.kde.org/plasma-workspace/91f47c81b572e808046361b02cb824ab378c3841'>Commit.</a> </li>
<li>Remove conditional installation of docs, it has no effect on translations. <a href='https://commits.kde.org/plasma-workspace/9383159c970ec3ca6f7124d4712afc623c16b482'>Commit.</a> </li>
<li>Digital-clock: Update iso-3166 codes in timezones. <a href='https://commits.kde.org/plasma-workspace/5668ff05ad8cfc027bbd9fe7692fc2be518649c6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129691'>#129691</a></li>
<li>AppMenu: Don't call AboutToShow on LayoutUpdated. <a href='https://commits.kde.org/plasma-workspace/dba0f80288b41e385f6a358f325b6b05d8ae11c4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375053'>#375053</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5705'>D5705</a></li>
<li>[Battery Monitor] Remove unused import. <a href='https://commits.kde.org/plasma-workspace/7bab7b635dfb46cbd6c6bcfefae2d990619f60c3'>Commit.</a> </li>
<li>[krunner] First set panel behavior than as panel. <a href='https://commits.kde.org/plasma-workspace/e967385b0a584fdca5480654892591d2e7198fc2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6657'>D6657</a></li>
<li>[Icon Applet] Enforce PlainText. <a href='https://commits.kde.org/plasma-workspace/be413f2ca749e072c0074131ee12a8da2b2ccaf1'>Commit.</a> </li>
<li>[Analog Clock] Get rid of unused logic.js. <a href='https://commits.kde.org/plasma-workspace/05466ff36ef9d24e824871855a730ea0a05e989f'>Commit.</a> </li>
<li>Don't autohide panel if a child window is open. <a href='https://commits.kde.org/plasma-workspace/2d8b4e1dec26c5976dd75c238c3ae8a4700b8dd9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352459'>#352459</a>. Fixes bug <a href='https://bugs.kde.org/347855'>#347855</a>. Fixes bug <a href='https://bugs.kde.org/351823'>#351823</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6577'>D6577</a></li>
<li>Port analog clock to PlasmaComponents3. <a href='https://commits.kde.org/plasma-workspace/930e8f9609a064b865592c235cc33ee75fe69242'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6649'>D6649</a></li>
<li>Remove unused references to forceWindowed. <a href='https://commits.kde.org/plasma-workspace/af666ef770accbc6c20512be793c04bab723a6c5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6648'>D6648</a></li>
<li>Show alternatives dialog immediately. <a href='https://commits.kde.org/plasma-workspace/74fe83cbaf4f3c3de8183559bf0b3fbc59f56047'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6647'>D6647</a></li>
<li>Disable klipper debug by default. <a href='https://commits.kde.org/plasma-workspace/e9401081544227baacfa5a9db05aa563318082dc'>Commit.</a> </li>
<li>Disable http cleaning, it'd dangle after the test and hold up ctest. <a href='https://commits.kde.org/plasma-workspace/a25e3e36f7f110bd749173c20f364dc14cb50760'>Commit.</a> </li>
<li>Fix indentation. <a href='https://commits.kde.org/plasma-workspace/70ceebd3ce468b3c665480410226fa72fd8713a6'>Commit.</a> </li>
<li>Simplify Alternatives Dialog code. <a href='https://commits.kde.org/plasma-workspace/c32548441d97dcb7dbed5a0a0dffaecdbb45f657'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6615'>D6615</a></li>
<li>Fix license in metadata.desktop files. <a href='https://commits.kde.org/plasma-workspace/e3bbe82ebfb3e1270a1537372e09bb9ae1db73aa'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6627'>D6627</a></li>
<li>Lower the test timeout to 2.5 minutes to reduce the time impact it has. <a href='https://commits.kde.org/plasma-workspace/c93de093772d3d43231baadb24b46868a7a4a507'>Commit.</a> </li>
<li>Test is still not terminating properly, debug processes again. <a href='https://commits.kde.org/plasma-workspace/b9e5abd657be65420530b56e7a5dd1f0fc162254'>Commit.</a> </li>
<li>Don't list plasmashell in menus. <a href='https://commits.kde.org/plasma-workspace/6f3f0d85085c399bf1003a1fe2c4a9c8f9e91589'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6498'>D6498</a></li>
<li>[CurrentContainmentActionsModel] Allow making the action about dialog modal. <a href='https://commits.kde.org/plasma-workspace/e5e0e91daa52812f18739f7d4c9ba0e9482708d0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6440'>D6440</a></li>
<li>Do not access applications array through desktops. <a href='https://commits.kde.org/plasma-workspace/1aa2044760729ddfad6a4945aa3c401da3742f54'>Commit.</a> </li>
<li>Force a11y and manually manage atspi helpers. <a href='https://commits.kde.org/plasma-workspace/018108dca4227a8deafa70d05e7bab754d1112c9'>Commit.</a> </li>
<li>[Logout Dialog] Honor "Offer shutdown options". <a href='https://commits.kde.org/plasma-workspace/913142c565ec7cb51f9d5406193d3c9a2562dbb0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6574'>D6574</a></li>
<li>Revert "add a new isolater permutation for CI use". <a href='https://commits.kde.org/plasma-workspace/e0d163bbc767a8dd3c6ee450b1fbc52b77c29d32'>Commit.</a> </li>
<li>Add a new isolater permutation for CI use. <a href='https://commits.kde.org/plasma-workspace/d607a1a29ead1a15af49669c6630c8d70dac1601'>Commit.</a> </li>
<li>Drop dangling end keyword. <a href='https://commits.kde.org/plasma-workspace/9d503d397cb367fb1d8b3f9913e97ae162e5c3e7'>Commit.</a> </li>
<li>Remove proc debugging again no more useful. <a href='https://commits.kde.org/plasma-workspace/fae71c7403bc973f3f10f03c39af7752321cacef'>Commit.</a> </li>
<li>Replace static sleep with a retry mechanic. <a href='https://commits.kde.org/plasma-workspace/b636324b4beb0ccedf0d65964fd6e16972407152'>Commit.</a> </li>
<li>Make sure a11y gets enabled in in Qt. <a href='https://commits.kde.org/plasma-workspace/c2ae4c75859d6bbf151c7191cc462cefe52f7ad6'>Commit.</a> </li>
<li>Dump processes tables in setup and teardown. <a href='https://commits.kde.org/plasma-workspace/bc82997225a5270ed0bc32325c721c85a638b2ba'>Commit.</a> </li>
<li>Force synced output. <a href='https://commits.kde.org/plasma-workspace/b3a035d7ab96bf0ca34fbbc2b7e13c26ef3cc5ae'>Commit.</a> </li>
<li>Reduce overriding yet more when working with no isolation. <a href='https://commits.kde.org/plasma-workspace/8da9e08e91367cb082efe52f7f7327ccdfa5d79f'>Commit.</a> </li>
<li>When drkonqi isn't running complain in a more comprehensible manner. <a href='https://commits.kde.org/plasma-workspace/467582390437b6eb5391da72fcdc3c9c661b62fb'>Commit.</a> </li>
<li>Introduce debug branch if tests failed. <a href='https://commits.kde.org/plasma-workspace/8217d6625c36c56e24b5505915422d9d65decf18'>Commit.</a> </li>
<li>Style++. <a href='https://commits.kde.org/plasma-workspace/fc7fb731d12797c0a38cac1a89f8e0186da10634'>Commit.</a> </li>
<li>Remove deprecated plasmashell options. <a href='https://commits.kde.org/plasma-workspace/cb2eaef77eefcaac1fb36accef56152df95cd1cb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6429'>D6429</a></li>
<li>Kio_desktop: don't use QDesktopServices::storageLocation. <a href='https://commits.kde.org/plasma-workspace/a5a18f5ac709d03c63902b0e3c1d6b362b1551fa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381566'>#381566</a></li>
<li>Move test-global variables to the suite runner. <a href='https://commits.kde.org/plasma-workspace/637f2f79b9d2c8aa7ec2de0ee8d7cc7a4102bc95'>Commit.</a> </li>
<li>Search for gdb as integration test requirement as well. <a href='https://commits.kde.org/plasma-workspace/37e37c71e5e7cac80011987ac43891244888a41d'>Commit.</a> </li>
<li>Disable all test isolation for build.kde builds. <a href='https://commits.kde.org/plasma-workspace/76407e9a9a274908ae12ecc22206f9e14358fdcb'>Commit.</a> </li>
<li>Do not try to isolate x11 on build.kde. <a href='https://commits.kde.org/plasma-workspace/1e42adf730b837403a37ed25f10b0d4a915bc34c'>Commit.</a> </li>
<li>Reduce post-test forced sleep. <a href='https://commits.kde.org/plasma-workspace/4cc63ecd0c79dc61d602ee504d2b887f503168ad'>Commit.</a> </li>
<li>Spawn without going through sh first. <a href='https://commits.kde.org/plasma-workspace/1f9711e2e78f6341115afd41a88055b6669b42b1'>Commit.</a> </li>
<li>Typo--. <a href='https://commits.kde.org/plasma-workspace/b61305899c5cc3bd2c7c3d7544988ecf6730a377'>Commit.</a> </li>
<li>Get rid of manual window flags setting on controller. <a href='https://commits.kde.org/plasma-workspace/13aa418c2772c619bfde64c25474dc7f8c9c7bd3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6403'>D6403</a></li>
<li>Remove Hidden=true from the .desktop file. <a href='https://commits.kde.org/plasma-workspace/5f4333feb815ae45ca03a8e6db4d56b5d9b558da'>Commit.</a> </li>
<li>[Image Wallpaper] Add "Open Containing Folder" button. <a href='https://commits.kde.org/plasma-workspace/0a86c4de3ed9efb0aa7b8eb295a0a18fbd3b7424'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371247'>#371247</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6450'>D6450</a></li>
<li>Application launcher mouse action can now be configured to show application names instead of their description. <a href='https://commits.kde.org/plasma-workspace/bdaaaedbfbbe72673c288a5acc8a8366d3f6bc1e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358423'>#358423</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6441'>D6441</a></li>
<li>Remove unused includes to QGraphicsScene. <a href='https://commits.kde.org/plasma-workspace/c75e91f89719560638d7ce938e4ce37454335bb5'>Commit.</a> </li>
<li>Quote file path. <a href='https://commits.kde.org/plasma-workspace/7d816e340261cad58d23bf83d8ab2e465c52c00b'>Commit.</a> </li>
<li>Debug xvfb launching as there's a rather weird error coming out of jenkins. <a href='https://commits.kde.org/plasma-workspace/8976d73a80958af4327eb59dc88f2a40e10e30fa'>Commit.</a> </li>
<li>Find bus launcher on suse. <a href='https://commits.kde.org/plasma-workspace/50749f70677d2e6acef626842b39cc0d67824609'>Commit.</a> </li>
<li>Set correct icons for plasma config windows on wayland. <a href='https://commits.kde.org/plasma-workspace/650f478dd5b59f525df51dffd355911b8ce830a6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6400'>D6400</a></li>
<li>[Scripting] Throw a error from the Containment when the widget is not found. <a href='https://commits.kde.org/plasma-workspace/94aa4152937f6eff7c60c2f310d0fc848c644353'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6424'>D6424</a></li>
<li>Create shell surface only at the right moment. <a href='https://commits.kde.org/plasma-workspace/4c2eb55636fb4485deaebf67f01ae4d07d29ce6d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6299'>D6299</a></li>
<li>Simplify positioning code. <a href='https://commits.kde.org/plasma-workspace/415b6d81a6967b29c11dda6b6bed13f2bb9db18a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6216'>D6216</a></li>
<li>Add a integration test suite and fix a signal connect bug when attaching reports. <a href='https://commits.kde.org/plasma-workspace/295af397fb2ea977e5669b83d0c32c2e1c11ad91'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6161'>D6161</a></li>
<li>Make --test mode run without spawning kactivitymanagerd. <a href='https://commits.kde.org/plasma-workspace/5bd54084cf41bcd6f1b7fb5e4da0ebc7b3a9e4ba'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6133'>D6133</a></li>
<li>Don't ignore return value. <a href='https://commits.kde.org/plasma-workspace/3c8d7552d79a29958707b029cd400207480567d4'>Commit.</a> </li>
<li>[Notifications] Hide job info label for successive jobs. <a href='https://commits.kde.org/plasma-workspace/149212e79bc460b1ac0282ef77e2399b427947a3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6109'>D6109</a></li>
<li>[Notifications Data Engine] Expose desktop entry and application service name icon. <a href='https://commits.kde.org/plasma-workspace/83ed4da7e38392dce04eed540848800bb0007715'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6106'>D6106</a></li>
<li>[Notification Job Delegate] Slightly optimize summary label. <a href='https://commits.kde.org/plasma-workspace/3d85454fef8da0d13318bb1f88a691c746bc6ab5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6105'>D6105</a></li>
<li>Add a test that adds a notification and closes it. <a href='https://commits.kde.org/plasma-workspace/ec31a507275c16aecd91e1e47adb8b21b1acac43'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6059'>D6059</a></li>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/plasma-workspace/1a63f5eb2a7ea31c242ee06405e174fee4aaeba3'>Commit.</a> </li>
<li>Include locale.h for setlocale & co. <a href='https://commits.kde.org/plasma-workspace/d0b3617767c4f4aeb68082dcaa9cfaa55857d7e7'>Commit.</a> </li>
<li>Match subsequences in application runner. <a href='https://commits.kde.org/plasma-workspace/ea59c63a815a963ec0d7e7d04827f983e08445a8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5870'>D5870</a></li>
<li>Use ${X11_XTest_LIB} rather than Xtst. <a href='https://commits.kde.org/plasma-workspace/f0e45cd42ba7e67ec444a4c14bac855b0ac195df'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5997'>D5997</a></li>
<li>Use right QProcess API. <a href='https://commits.kde.org/plasma-workspace/778ebff936e079604b24e3eedeab8c94d250e0a2'>Commit.</a> </li>
<li>Workaround for correct wayland positioning. <a href='https://commits.kde.org/plasma-workspace/d3bdb8d657bad59bad63c29f3c4c49485e16ea9b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5748'>D5748</a></li>
<li>Wire up requestToggleKeepAbove/below. <a href='https://commits.kde.org/plasma-workspace/2cdd97c68544eac83c95936aa0a2f0a3eb2b7938'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5758'>D5758</a></li>
<li>--crashes;. <a href='https://commits.kde.org/plasma-workspace/58e1e8e527b995b07d5f2ba14126e5dcbcaa19f9'>Commit.</a> </li>
<li>Improve positioning for highlight in systray. <a href='https://commits.kde.org/plasma-workspace/40cc6c359cb5417fd137b34cf5d73d3f97e8ddb4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5927'>D5927</a></li>
<li>Remove useless startup. <a href='https://commits.kde.org/plasma-workspace/8eb03288ed02dc0675d4cf66bd3888fef1db4e5a'>Commit.</a> </li>
<li>Don't check for panel visible during startup. <a href='https://commits.kde.org/plasma-workspace/6602e991617cf857c262ac3fbb2ac00c7de1fb15'>Commit.</a> See bug <a href='https://bugs.kde.org/377280'>#377280</a></li>
<li>We should not forget the launchers that are not currently shown. <a href='https://commits.kde.org/plasma-workspace/e8e2be90834b6526006f4961d62d511079d24d26'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5933'>D5933</a></li>
<li>Remove non-existing connect. <a href='https://commits.kde.org/plasma-workspace/73ff80d1c6f3af70d7d1f7dfe3667ec3d4572cd7'>Commit.</a> </li>
<li>Lift app identification heuristic out of XWindowTasksModel and share it with WaylandTasksModel. <a href='https://commits.kde.org/plasma-workspace/a87f619a985c7a2f19743a70f391ebbd36a1508d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5818'>D5818</a></li>
<li>Expose PlasmaWindow::pid through WaylandTasksModel. <a href='https://commits.kde.org/plasma-workspace/140c65f2236bffa9bbe1eaae423b7fea0b2d3ede'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5755'>D5755</a></li>
</ul>


<h3><a name='plymouth-kcm' href='https://commits.kde.org/plymouth-kcm'>Plymouth KControl Module</a> </h3>
<ul id='ulplymouth-kcm' style='display: block'>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/plymouth-kcm/3dbcd05a55e76bf9ebe1e02b3ef2057961538caa'>Commit.</a> </li>
<li>Set kauth timeout to timeout for the update-initramfs -u process. <a href='https://commits.kde.org/plymouth-kcm/dbdedcfa9de1dc725d66b81246df02d0381b704a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5943'>D5943</a></li>
<li>Check if plymouth file is known to update-alternatives before setting. <a href='https://commits.kde.org/plymouth-kcm/d8017f7b97444ffbc813566d2f5d5d5461953040'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5942'>D5942</a></li>
</ul>


<h3><a name='polkit-kde-agent-1' href='https://commits.kde.org/polkit-kde-agent-1'>polkit-kde-agent-1</a> </h3>
<ul id='ulpolkit-kde-agent-1' style='display: block'>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/polkit-kde-agent-1/011aab25f41cc5e2321a64eb40d68ec3cf530987'>Commit.</a> </li>
</ul>


<h3><a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>Disable DDCUtil by default. <a href='https://commits.kde.org/powerdevil/687587ea7cbc0489bdedd4b32a7fbd1dcd1cf4db'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7722'>D7722</a></li>
<li>Bump the version requirement for Qt. <a href='https://commits.kde.org/powerdevil/ff2bec6239a123d0ddac31a13ba7d9eb42b1ea0b'>Commit.</a> </li>
<li>Revert "skip the disabled backlight device". <a href='https://commits.kde.org/powerdevil/97f3900049aa657be5f4c124c9f992120f124944'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381114'>#381114</a>. Fixes bug <a href='https://bugs.kde.org/381199'>#381199</a></li>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/powerdevil/6ace4109e26efa5d6e3878ba7deb0a73a603bde6'>Commit.</a> </li>
<li>Cmake: link to ddcutil only if found. <a href='https://commits.kde.org/powerdevil/a3fce97fd6558b77c41ca1fbc6c83180273d8969'>Commit.</a> </li>
<li>Add brightness control using ddcutil lib. <a href='https://commits.kde.org/powerdevil/a3a61317ddebbcad526aa0496f766c9b1a4abdd6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5381'>D5381</a></li>
</ul>


<h3><a name='sddm-kcm' href='https://commits.kde.org/sddm-kcm'>SDDM KCM</a> </h3>
<ul id='ulsddm-kcm' style='display: block'>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/sddm-kcm/ac8399f8fc7525fb385912e89c83437769e0d172'>Commit.</a> </li>
</ul>


<h3><a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a> </h3>
<ul id='ulsystemsettings' style='display: block'>
<li>Header as one ToolButton for Back action. <a href='https://commits.kde.org/systemsettings/a88cad3bc72ccfee23bf2cf18e4e3cf94f339677'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7749'>D7749</a></li>
<li>Add comment for appstream validation. <a href='https://commits.kde.org/systemsettings/985f093164e44ecaab7ed4310591354d14509bc2'>Commit.</a> </li>
<li>Split sorting and filtering in 2 different proxies. <a href='https://commits.kde.org/systemsettings/b4c0e457b7ccd20eb78c9ad2ef6686a49c47ef96'>Commit.</a> </li>
<li>Remove leaveModuleView. <a href='https://commits.kde.org/systemsettings/820be3dfdb1030f7eb27cc233bd5a635ccd9d9a5'>Commit.</a> </li>
<li>Remove i in tooltip. <a href='https://commits.kde.org/systemsettings/5f9c6f12fda56e3ab343598460b4c26d1498e6a2'>Commit.</a> </li>
<li>Remove classic from tree view in system settings config dialogue. <a href='https://commits.kde.org/systemsettings/278e9f4c31a352b5f5a332854b7c78587c5ee4f2'>Commit.</a> </li>
<li>Capitalize. <a href='https://commits.kde.org/systemsettings/1df03f278450c18021920d182c5dc3b32ab61a20'>Commit.</a> </li>
<li>Better layout management for intro icons. <a href='https://commits.kde.org/systemsettings/aec08859558b1771cd1637d23e15fcb61218ee64'>Commit.</a> </li>
<li>Reset toolbar items when the module shows. <a href='https://commits.kde.org/systemsettings/7ac9bc5729825d22ff98c82c1edb1c75b3734ca6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380083'>#380083</a></li>
<li>Kservices in MenuItem can be not valid. <a href='https://commits.kde.org/systemsettings/c72349a81b1302f33ffb4c6bb83163f5b2e58d17'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381701'>#381701</a></li>
<li>Allow word wrap in intro icons. <a href='https://commits.kde.org/systemsettings/451304975351e446922f2e81dae16b6bc5a62a06'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380972'>#380972</a></li>
<li>Remove useless debug. <a href='https://commits.kde.org/systemsettings/33742e4f3cb298c75c36316ef179f283752c20df'>Commit.</a> </li>
<li>Go back on click of the whole header. <a href='https://commits.kde.org/systemsettings/df64fffdf561362f615c57b081a5540900125176'>Commit.</a> </li>
<li>Update the submenu when typing. <a href='https://commits.kde.org/systemsettings/28496b8c658ca092b013fda408947fd1b881e719'>Commit.</a> </li>
<li>Mark the icons as an accessible button. <a href='https://commits.kde.org/systemsettings/052b73f6a238379ee168ab6591a13e2213970cee'>Commit.</a> </li>
<li>Avoid an useless maptosource for the index. <a href='https://commits.kde.org/systemsettings/6453255d3352bcff417f02564b69e64b776e353e'>Commit.</a> </li>
<li>Refine layouts for items to be more aligned. <a href='https://commits.kde.org/systemsettings/c3334d0f14013cf4ce8cffddb3b0025d2caa83f6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6118'>D6118</a></li>
<li>Get rid of useless debug. <a href='https://commits.kde.org/systemsettings/82b96890a2c49fbf9145c71b7b16590c85aa9fcf'>Commit.</a> </li>
<li>Prototype of kactivity-based most used modules entries. <a href='https://commits.kde.org/systemsettings/1018b86e48dbcf5ff83ba571f2753c3220cf8cc6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6061'>D6061</a></li>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/systemsettings/2ff10f2f13b47d52e2992597fec672bcd4ff56b8'>Commit.</a> </li>
<li>Port the intro page to QML. <a href='https://commits.kde.org/systemsettings/84287a2f2c2d8766ceef155a1ae5eae0206c30d7'>Commit.</a> </li>
<li>Fix flicker on the second column loading a new category. <a href='https://commits.kde.org/systemsettings/cd21808d8cb4993d29706acbf732127891161cf8'>Commit.</a> </li>
<li>Kirigami2, not kirigami. <a href='https://commits.kde.org/systemsettings/f52d0122fbef89e9391dcbb148a0a7d606384e21'>Commit.</a> </li>
<li>Same logic for separators and normal list items. <a href='https://commits.kde.org/systemsettings/d4f261dcd7a47372a15eab304ef0e57f2d2445e9'>Commit.</a> </li>
<li>Make the two headers have the same exact height. <a href='https://commits.kde.org/systemsettings/19cb975a1807b44554b35ef8e1df6b572a7d87e6'>Commit.</a> </li>
<li>Require kirigami 2.1. <a href='https://commits.kde.org/systemsettings/cf3f4a843103c29f5d23c3f1170e7fb18b08bec9'>Commit.</a> </li>
<li>Workaround for Qt 5.7. <a href='https://commits.kde.org/systemsettings/492890a0525eda2d17860b223fcd95ccf6e90287'>Commit.</a> </li>
<li>Elide too long text. <a href='https://commits.kde.org/systemsettings/c12ff4a46403e01330d7d52efb74d1d76d4b02bf'>Commit.</a> </li>
<li>Now that uses qqc2, depends from Qt 5.7. <a href='https://commits.kde.org/systemsettings/bdb1b2f407d88c44675fdf766a7e15b0968c5982'>Commit.</a> </li>
<li>Follow settings for tooltips. <a href='https://commits.kde.org/systemsettings/7fd517d6023927b3dba2302a517ec62365065832'>Commit.</a> </li>
<li>Experimenta switch wo 2 columns mode when very large. <a href='https://commits.kde.org/systemsettings/5ff2bf3fc277210690a025583184446e5c9618a7'>Commit.</a> </li>
<li>Make qml widget size based upon qml item implicitwidth. <a href='https://commits.kde.org/systemsettings/76ea784247bb6c686a6a956db539ef3bead47f8f'>Commit.</a> </li>
<li>Better keyboard navigation. <a href='https://commits.kde.org/systemsettings/2759ca416503ec5ffb9dccbd29432168cd8bfa0e'>Commit.</a> </li>
<li>More separators. <a href='https://commits.kde.org/systemsettings/c5a62a5ec0993868f091a5173923759374bd0d2c'>Commit.</a> </li>
<li>Add a welcome page. <a href='https://commits.kde.org/systemsettings/af1dfa08ef62559a6e95a6a48dd0955583381c43'>Commit.</a> </li>
<li>Clear button. <a href='https://commits.kde.org/systemsettings/9d5184df566edc2f65841bcbd32213280917db3c'>Commit.</a> </li>
<li>Add icons in the menu. <a href='https://commits.kde.org/systemsettings/740beb768a47d3017911cdd3bc6f0523c422073e'>Commit.</a> </li>
<li>Map activeCategory to sourcemodel row. <a href='https://commits.kde.org/systemsettings/2656bad00617c0ca7fa7844a7d7a602dd8bb78e0'>Commit.</a> </li>
<li>Split category and subcategory pages in separate files. <a href='https://commits.kde.org/systemsettings/84a086dedd5a61f27d969d39d6bd64f1a93476ce'>Commit.</a> </li>
<li>Add missing tooltips files. <a href='https://commits.kde.org/systemsettings/ec3204527d0d0bbb2a918367319ef34536a6762a'>Commit.</a> </li>
<li>Add tooltips management back. <a href='https://commits.kde.org/systemsettings/eaf352e96474a53c82adb51018270035e2c37ffd'>Commit.</a> </li>
<li>Remove old autocompletion stuff. <a href='https://commits.kde.org/systemsettings/e57fd6d29db351ee970f479304defd406be1175f'>Commit.</a> </li>
<li>Set second page title. <a href='https://commits.kde.org/systemsettings/e2cb0d742aa56c3615bbf30b00d450aa53bcdc00'>Commit.</a> </li>
<li>Use ApplicationItem. <a href='https://commits.kde.org/systemsettings/5d93e91c3d7d772b53050427b4a9e6d5f0f0b57d'>Commit.</a> </li>
<li>Some keyboard navigation. <a href='https://commits.kde.org/systemsettings/e6daa48e7f1e392992d5131166516fa5121ef857'>Commit.</a> </li>
<li>Use KDeclarative to access i18n(). <a href='https://commits.kde.org/systemsettings/64520fc599326893c4c8529911a4152b58cce2b4'>Commit.</a> </li>
<li>Layout fixes. <a href='https://commits.kde.org/systemsettings/4374ead70a3f719ad18bbd255ea5da0a3d0ec254'>Commit.</a> </li>
<li>Tart port to QML. <a href='https://commits.kde.org/systemsettings/b41df9adda01d2dde6fceca05aab388c4a0f1df6'>Commit.</a> </li>
<li>Submenu in a different column. <a href='https://commits.kde.org/systemsettings/8285db38acbbba14929a23dc3dccefc50258e78a'>Commit.</a> </li>
<li>Different search mode. <a href='https://commits.kde.org/systemsettings/d40a135073973dc70054dbb9a317131e87c29a70'>Commit.</a> </li>
<li>Show/hide toolbars when it should. <a href='https://commits.kde.org/systemsettings/a7d6e056b50e6e548e6ca25772bd9a09e5f83c44'>Commit.</a> </li>
<li>A menu with main actions. <a href='https://commits.kde.org/systemsettings/06a34505841494ba445f64f53d76bca3c37ee3a5'>Commit.</a> </li>
<li>Custom delegate more kirigamish. <a href='https://commits.kde.org/systemsettings/5010ef134f216603c55536bc50df224ed036e7ae'>Commit.</a> </li>
<li>Preliminar for a new default systemsettings ui. <a href='https://commits.kde.org/systemsettings/625bae01c2521bcbcc92efd81edf95e91a6663f2'>Commit.</a> </li>
</ul>


<h3><a name='user-manager' href='https://commits.kde.org/user-manager'>User Manager</a> </h3>
<ul id='uluser-manager' style='display: block'>
<li>Don't show a region selector when selecting avatars from a gallery. <a href='https://commits.kde.org/user-manager/8defbd16e76704b38b5988ddff21cdf9ad880a64'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D7686'>D7686</a></li>
<li>Use Q_DECL_OVERRIDE. <a href='https://commits.kde.org/user-manager/97bae15cadb06adfffd1667bacba7e2300a4d34f'>Commit.</a> </li>
</ul>


<h3><a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a> </h3>
<ul id='ulxdg-desktop-portal-kde' style='display: block'>
<li>Use CMAKE_INSTALL_FULL_LIBEXECDIR. <a href='https://commits.kde.org/xdg-desktop-portal-kde/71a26013b409ddce0af66e6d0a04a45ff4f1bbcb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6749'>D6749</a></li>
<li>Add arcconfig. <a href='https://commits.kde.org/xdg-desktop-portal-kde/37378289f950a82d14228001fb47cd009212f35b'>Commit.</a> </li>
<li>Massively simplify the class DesktopPortal. <a href='https://commits.kde.org/xdg-desktop-portal-kde/45843db8d63aeefc8c634e40f06c5831a2595960'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6145'>D6145</a></li>
<li>Add missing files. <a href='https://commits.kde.org/xdg-desktop-portal-kde/2002600ffec9c2b9aef81b6513ee10446eedfa0d'>Commit.</a> </li>
<li>Add Access portal for requesting hardware access. <a href='https://commits.kde.org/xdg-desktop-portal-kde/7f28b67f398f4b5fa7ca733d9ceb595aaeebbe42'>Commit.</a> </li>
</ul>


</main>
<?php
	require('../aether/footer.php');
