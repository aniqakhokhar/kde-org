<?php
    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Plasma 5.20: One absolutely massive release",
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = 'plasma-5.20.0'; // for i18n
    $version = "5.20.0";
?>
<style>
.text-on-background {
  position: absolute;
  bottom: 4rem;
  right: 2rem;
  text-align: right;
  color: white;
}
@media (min-width: 900px) {
 .boxes {
  display:grid;
  grid-template-columns:repeat(2, 1fr);
  grid-gap:3%;
  margin:2em 0;
  margin-bottom:260px;
  --translation: 0
 }
 .boxes .box {
  width:100%;
  height:0;
  padding-bottom:65%;
  border-width:0;
  background:inherit;
  transform:translateY(var(--translation))
 }
 .boxes .box:nth-child(odd) {
  --translation: calc(var(--skew-padding))
 }
 .boxes .box:nth-child(even) {
  --translation: calc(var(--skew-padding) * 0.5)
 }
}
@media (max-width: 899px) {
 .boxes {
  display:flex;
  flex-wrap:wrap
 }
 .boxes .box {
  flex:0 0 50%;
  max-width:50%;
  padding:0 1rem
 }
}
@media (max-width: 799px) {
 .boxes .box {
  flex:0 0 100%;
  max-width:100%;
  padding:0
 }
}
</style>

<main class="releaseAnnouncment">
  <div class="container">
  </div>
  <div class="text-center py-4" style="background: radial-gradient(circle, #c203c5 0%, #4e02a7 80%); color: white">
    <h1 class="mt-0" style="background-color: red">NOT OUT YET</h1>
    <div class="container"><?php include "./announce-i18n-bar.inc"; ?></div>
    <div class="laptop-with-overlay d-block my-3 mx-auto w-100" style="max-width: 1000px">
      <img class="laptop img-fluid mt-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
      <div class="laptop-overlay">
        <img class="img-fluid mt-3" src="/announcements/plasma-5.20/plasma.png" style="box-shadow:inset 0px 0px 6px 0px #000000;" />
      </div>
    </div>
    <div class="container">
      <p><?php echo i18n("Tuesday, 13 October 2020."); ?></p>
      <h1 class="h2"><?php i18n("Plasma 5.20"); ?>
      <h2 class="h1"><?php i18n("New look on the outside, and better in the inside"); ?></h2>
      <p class="h5"><?php i18n("An massive release, that contains significant changes to dozens of components, widgets and the desktop behavior in general.") ?></p>
    </div>
  </div>
  <div class="container h5 py-5">
    <video controls class="w-100 h-100 mt-3" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.19/Thumbnail.png">
      <source src="https://cdn.kde.org/promo/Announcements/Plasma/5.19/Video.webm"
          type="video/webm">
      <source src="https://cdn.kde.org/promo/Announcements/Plasma/5.19/Video.mp4"
          type="video/mp4">
    </video>
    <p><?php echo i18n("Everyday utilities and tools such as the Panels, the Task Manager, the Notifications and the System Settings have all been overhauled to make them more usable, efficient and friendly."); ?></p>

    <p><?php echo i18n("Meanwhile, developers are hard at work adapting Plasma and all its bits and pieces to Wayland. Once done, Plasma will not only be readier for the future, but will also work better with touchscreens and multiple screens with different frequencies and DPIs. Plasma will also offer better support for hardware accelerated graphics, be more secure and enjoy many more advantages."); ?></p>

    <p><?php echo i18n("Read on to find out more about the new features and improvements included in Plasma 5.20..."); ?></p>
  </div>

  <div class="text-center" style="background: radial-gradient(circle, #6e5cee 0%, #4618ac 80%); color: white;">
    <div class="p-4">
      <h2 class="h1">Look and feel</h2>
      <p class="h5">Some of the changes will be obvious the moment Plasma desktop and its beautiful new wallpaper (<I>Shell</I> by the talented Lucas Andrade) loads onto your screen.</p>
    </div>
    <img src="/announcements/plasma-5.20/background-look-and-feel.png" class="img-fluid">
  </div>

  <div class="container">
    <div class="d-flex flex-column flex-md-row align-items-center">
      <div style="flex-basis: 60%">
        <h3 class="h3"><?php i18n("Task Manager"); ?></h3>
        <p><?php i18n("The Task Manager, for example, has not only changed its look (it is now icon-only by default) but also how it behaves: when you open several windows with the same application (like when you open several LibreOffice documents), the Task Manager will group them together and, in this new version, clicking on grouped windows, cycles through each, bringing each to the forefront until you reach the one you want. You can also tell  the Task Manager not to minimize the active task when you click on it in the Task Manager. As with most things Plasma, these behaviors are configurable and you can decide whether to keep them or ditch them."); ?></p>
      </div>
      <div class="p-0 p-md-3"><img src="/announcements/plasma-5.20/task-manager.png" class="img-fluid"></div>
    </div>
    <div class="d-flex flex-column flex-md-row align-items-center">
      <div class="order-md-1"  style="flex-basis: 100%;">
        <h3 class="h3"><?php i18n("System Tray") ?></h3>
        <p><?php i18n("Other changes are not so immediately evident, like the System Tray popup now shows items in a grid rather than a list, and the icon view on your panel can now be configured to scale the icons with the panel thickness."); ?></p>
        <p><?php i18n("Talking of scaling, the Web Browser widget also lets you zoom in and out from its contents by holding down the [Ctrl] key and rolling the mouse wheel. The Digital Clock widget has changed too and is now more compact and shows the current date by default. Throughout KDE apps in general, each toolbar button which displays a menu when clicked now shows a downward-pointing arrow to indicate this."); ?>
      </div>
      <div class="order-md-0 p-0 py-md-3 pr-md-3" style="flex-basis: 60%"><img src="/announcements/plasma-5.20/tray.png" class="img-fluid"></div>
    </div>
    <div class="d-flex flex-column flex-md-row align-items-center">
      <div>
        <h3 class="h4">On-screen displays</h3>
        <p><?php i18n("On-screen displays that appear when changing the volume or display brightness (for example) have been redesigned to be less obtrusive. Something else that has been tweaked is that, when you change the display brightness, it now transitions smoothly, and when using the 'raise maximum volume' setting, it will subtly warn you when going over 100% volume. Protect you eardrums, people!"); ?></p>
      </div>
      <div class="p-0 p-md-3" style="flex-basis: 100%"><img src="/announcements/plasma-5.20/ocd.png" class="img-fluid"></div>
    </div>
    <h3><?php i18n("KWin") ?></h3>
    <p><?php i18n('Then there are the changes that are not visual at all, but that affect usability and how you interact with your Plasma environment. When you wanted to  move and resize windows, for example, in Plasma versions prior to 5.20, you used to hold down [Alt] and then dragged with the mouse. This conflicted with many popular productivity applications that used the same gesture for different functions, so the default shortcut for moving and resizing is now to hold down the [Meta] ("Windows") key and drag instead.'); ?></p>
    <p><?php i18n('In a similar vein, you can push windows into a section of the screen so they take up half or a quarter of the available space (this is called "tiling") using a keyboard shortcut. To tile a window to the top half of the screen, for example, hold down the [Meta] key and tap the up arrow key. If you want to tile a window into  the upper left corner, hold down the [Meta] key and then tap the up and left arrow keys in quick succession; and so on and so forth.'); ?></p>
    <h3><?php i18n("Notification") ?></h3>
    <p><?php i18n("Then there are a lot of changes to the notification system. For one, you now get notified when your system is about to run out of space on your disk even if your home directory is on a different partition. Very important if you want to avoid sticky situations where you can't even save changes to a document because you have no more room on your hard drive."); ?></p>
    <p><?php i18n("Talking of disks and notifications, the <I>Device Notifier</I> applet has been renamed to <I>Disks & Devices</I> and it is now easier to use, as you can now show all disks, not just the removable ones. Keeping things tidy, unused audio devices are filtered out of the audio applet and System Settings page by default. As for keeping things safe, on supported laptops, you can configure a charge limit below 100% to preserve the battery health. If what you need is peace and quiet, Plasma 5.20 lets you enter the <I>Do Not Disturb</I> mode by simply middle-clicking on the Notifications applet or system tray icon."); ?></p>
    <p><?php i18n("Moving on to KRunner, KDE's application launcher, search-and-conversion utility, and general all-round useful thing, it now remembers the prior search text and you can choose to have it as a free-floating window, rather than having it permanently glued to the top of the screen. KRunner can now also search and open pages in <a href='https://www.falkon.org/'>Falkon</a>, KDE's advanced web browser."); ?></p>
    <p><?php i18n("And that is not all. Look out for the dozens of more subtle improvements sprinkled throughout Plasma 5.20 that make your experience with KDE's desktop sleeker and more enjoyable");?></p>
    <h2><?php i18n('System Settings & Info Center') ?></h2>
    <p><?php i18n("System Settings is the place where you go to configure most aspects of your system and it too gets its fair share of improvements in Plasma 5.20."); ?></p>
    <p><?php i18n("One of the most useful new features is that you can keep tabs on what you changed in your configurations with the <I>Highlight Changed Settings</I> feature: click the button in the bottom left of the window and Settings will show the things that have been modified from their default values.") ?></p>
    <p><?php i18n("Other things that will make your life easier are that the <I>Standard Shortcuts</I> and <I>Global Shortcuts</I> pages have been combined into one simply called <I>Shortcuts</I>; and the <I>Autostart</I>, <I>Bluetooth</I> and <I>User Manager</I> pages have been rewritten from scratch and redesigned according to modern user interface standards.") ?></p>
    <p><?php i18n('S.M.A.R.T is a technology included into many hard disks, SSDs and eMMC drives. It can help predict the failure of a storage device before it happens. Install <a href="https://invent.kde.org/plasma/plasma-disks/">Plasma Disks</a> from <a href="https://kde.org/applications/en/discover">Discover</a> and System Settings in Plasma 5.20 will be able to monitor S.M.A.R.T notifications and inform you of the state of your disks, allowing you to save your data before something catastrophic happens.'); ?></p>
    <p><?php i18n('Other new stuff includes an audio balance option that lets you adjust the volume of each individual audio channel and tools to adjust the touchpad cursor speed more to your liking.'); ?></p>
  </div>
  <div>
    <div class="boxes mx-auto mt-0" style="max-width: 1500px">
      <div class="box">
        <img loading="lazy" src="/announcements/plasma-5.20/bluetooth.png" class="img-fluid my-3" alt="KDE Slimbook with a fancy purple ambient color and showing KDevelop">
      </div>
      <div class="box">
        <img loading="lazy" src="/announcements/plasma-5.20/autostart.png" class="img-fluid my-3" alt="KDE Slimbook with a fancy purple ambient color and showing KDevelop">
      </div>
      <div class="box">
        <img loading="lazy" src="/announcements/plasma-5.20/autostart.png" class="img-fluid my-3" alt="KDE Slimbook with a fancy purple ambient color and showing KDevelop">
      </div>
      <div class="box">
        <img loading="lazy" src="/announcements/plasma-5.20/plasma-disks.png" class="img-fluid my-3" alt="KDE Slimbook with a fancy purple ambient color and showing KDevelop">
      </div>
    </div>
  </div>
  <div class="diagonal-box bg-purple">
    <section class="container content">
      <div class="text-center"><img src="/images/wayland.png" alt="Wayland logo" class="img-fluid"></div>
      <h2><?php i18n('Wayland'); ?></h2>
      <p><?php i18n('Back in 2019, the KDE Community decided to make adapting Plasma and its applications to Wayland an official goal and priority. The effort is starting to pay off big time as more and more features and utilities make their way to the new display server protocol. Like the Klipper clipboard utility, that now is fully operational on Wayland; or the multi-purpose launcher-calculator-searcher KRunner that now shows up in the correct place when you use a top panel.'); ?></p>
      <p><?php i18n('Mouse and touchpad support are nearly on par with their X counterparts and screencasting is also supported in Plasma 5.20. The Task Manager shows window thumbnails and in general the whole desktop is more stable and no longer crashes even if XWayland does.') ?></p>
      <p><?php i18n('These are just the highlights of Plasma 5.20. There is so much more! If you want a full list of everything that has gone into Plasma 5.20, check out <a href="https://kde.org/announcements/plasma-5.19.5-5.20.0-changelog">the full changelog</a> and enjoy your new desktop!'); ?></p>
    </section>
  </div>
  <div class="container">
    <!-- // Boilerplate again -->
    <section class="row get-it" id="download">
      <article class="col-md">
        <h2><?php i18n("Live Images");?></h2>
        <p>
            <?php i18n("The easiest way to try it out is with a live image booted off a USB disk. Docker images also provide a quick and easy way to test Plasma.");?>
        </p>
        <a href='https://community.kde.org/Plasma/Live_Images' class="learn-more"><?php i18n("Download live images with Plasma 5");?></a>
        <a href='https://community.kde.org/Plasma/Docker_Images' class="learn-more"><?php i18n("Download Docker images with Plasma 5");?></a>
      </article>

      <article class="col-md">
        <h2><?php i18n("Package Downloads");?></h2>
        <p>
            <?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
        </p>
        <a href='https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro' class="learn-more"><?php i18n("Get KDE Software on Your Linux Distro wiki page");?></a>
      </article>

      <article class="col-md">
        <h2><?php i18n("Source Downloads");?></h2>
        <p>
            <?php i18n("You can install Plasma 5 directly from source.");?>
        </p>
        <a href='https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source' class='learn-more'><?php i18n("Community instructions to compile it");?></a>
        <a href='../info/plasma-5.15.90.php' class='learn-more'><?php i18n("Source Info Page");?></a>
      </article>
    </section>

    <section class="give-feedback">
      <h2><?php i18n("Feedback");?></h2>

      <p class="kSocialLinks">
        <?php i18n("You can give us feedback and get updates on our social media channels:"); ?>
        <?php include($site_root . "/contact/social_link.inc"); ?>
      </p>
      <p>
        <?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?>
      </p>

      <p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>Plasma Matrix chat room</a>, <a href='%2'>Plasma-devel mailing list</a> or report issues via <a href='%3'>bugzilla</a>. If you like what the team is doing, please let them know!", "https://webchat.kde.org/#/room/#plasma:kde.org", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided"); ?>

      <p><?php i18n("Your feedback is greatly appreciated.");?></p>
    </section>

    <h2>
      <?php i18n("Supporting KDE");?>
    </h2>

    <p align="justify">
      <?php print i18n_var("KDE is a <a href='%1'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='%2'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='%3'>Join the Game</a> initiative.", "http://www.gnu.org/philosophy/free-sw.html", "/community/donations/", "https://relate.kde.org/civicrm/contribute/transact?id=5"); ?>
    </p>

    <?php include($site_root . "/contact/about_kde.inc"); ?>
  
    <h2><?php i18n("Press Contacts");?></h2>
  
    <?php include($site_root . "/contact/press_contacts.inc"); ?>
  </div>
</main>
<?php
  require('../aether/footer.php');
