<?php
  $page_title = "Linux Congress Documentation and Localization Workshop";
  $site_root="../";
  include "header.inc";
?>

<H2 ALIGN="CENTER">Linux Congress<BR>
KDE first Documentation and Localization Workshop<BR>
Erlangen, Germany<BR>
<SMALL>23rd and 24th September, 2000</SMALL></H2>
<P>
<P ALIGN="CENTER"><B>Report by Eric Bischoff &lt;ebisch@cybercable.tm.fr&gt;</B></P>
<P>
Documents:
<P>
<OL>
<LI><A HREF="#summary">Summary</A>
<LI><A HREF="#agenda">Agenda</A>
<LI><A HREF="#decisions">Decided action items</A>
<LI><A HREF="#presentations">Presentations</A>
<LI><A HREF="#kdeeditorial">KDE Editorial Team Status Report</A>
</OL>
<P>
<HR>
<P>
<H2><A NAME="summary">1. Summary</A></H2>
<P>
The first meeting of KDE translation and documentation teams has been a
success.
We must thank:
<UL>
<LI>
the German Unix Users Group for the initiative of this Workshop as
part of the 7th Linux Kongress
<LI>
the German Ministry of Education and Scientific Research for the
funding
<LI>
the Friedrich-Alexander University of Erlangen-N�rnberg for the
hosting
<LI>
the companies Caldera Deutschland and SuSE GmbH for the logistics.
</UL>
<P>
KDE is an Open Source software project (<A
HREF="http://www.kde.org/index.html">www.kde.org</A>).  The purpose of
the KDE Documentation and Localization Workshop was to give people
involved in KDE's documentation and translation an opportunity to:
<UL>
<LI>
meet
<LI>
define policies and priorities
<LI>
coordinate work
<LI>
solve technical issues.
</UL>
<P>
This success demonstrates that there are strong energies willing to
bring Unix Operating Systems and Free Software to the citizen with a
documented, easy-to-use graphical interface, in their own language.
This is an approach that proprietary operating systems are unable to
sustain consistently.  Now that computers have come to everyday life,
this is putting a threat on cultural independance of many countries
with respect to American languages.  On the contrary, all the people
attending the workshop are committed to making computers - even using
powerful Operating Systems like GNU/Linux - more easy to use for everyone.
<P>
Attending team leaders were:
<UL>
<LI>Thomas Diehl - applications translation coordination
<LI>Eric Bischoff - documentation coordination
<LI>Frederik Fouvry - DocBook technical issues
<LI>Stephan Kulow - servers and packaging
<LI>Lauri Watts - documentation editorial team
<LI>Karl Backstr�m - Swedish team
<LI>Juraj Bednar - Slovak team
<LI>G�rkem �etin - Turkish team
<LI>Fran�ois-Xavier Duranceau - French team
<LI>Matthias Kiefer - German team
<LI>Pedro Morais - Portuguese team
<LI>Denis Pershine - Russian team
<LI>Elvis Pfutzenreuter - Portuguese of Brazil team
<LI>Andrea Rizzi - Italian team
<LI>Victor Romero - Spanish team
<LI>Hasso Tepper - Estonian team
<LI>Lukas Tinkl - Czech Team
<LI>Andrej Vernekar - Slovenian team
</UL>
<P>
The KDE project (<A
HREF="http://www.kde.org/index.html">www.kde.org</A>) represents more
than 50 languages by now and we were sorry not to be able to invite a
representative for each team. We are hoping to be able to reproduce
this event with other persons soon, with a special thought for
Japanese, Chinese, Korean, Hebrew, Arabic, Greek, Dutch, Romanian,
Hungarian, Icelandic, Hindi/Urdu and many other teams.
<P>
Persons having collaborated every day for years, either on a voluntary
or on a professional basis, were happy to meet personnaly for the first
time. The ambiance was nice and productive.
<P>
Covered areas of work included:
<UL>
<LI>
applications translation
<LI>
documentation writing
<LI>
documentation translation
<LI>
web servers translation
<LI>
coordination with other projects.
</UL>
<P>
All the subjects of the agenda have been examined (with the exception of
the demonstration of the software tools that each team uses in prevision
of a future integration). A survey of the current situation has been
established and we reached a consensus on the short and long term plans
for the future.
<P>

<HR>

<P>
<H2><A NAME="agenda">2. Agenda</A></H2>
<P>
<H3>Saturday 23/09/00</H3>
<P>
<TABLE>
<TR>
<TD VALIGN="TOP">
<B>10:30</B>
<TD>
Welcome words, program, thanks to the sponsors
      (Eric Bischoff &amp; Thomas Diehl)
<P>&nbsp;
<TR>
<TD VALIGN="TOP">
<B>10:40</B>
<TD>
Status report from the national team leaders
<UL>
<LI>
accomplishments
<LI>
projects
<LI>
way of working
<LI>
problems encountered
(Team leaders, 10 minutes each)
</UL>
<P>&nbsp;
<TR>
<TD VALIGN="TOP">
<B>12:30</B>
<TD>
Lunch
<P>&nbsp;
<TR>
<TD VALIGN="TOP">
<B>14:00</B>
<TD>
Relations with other Free Software projects
<UL>
<LI>LDP and Gnome
<LI>GNU/Linux &amp; Unix distributions
<LI>Dewey project
<LI>Li18nux project
<LI>Conglomerate project
<LI>Docbook-tools project
<LI>SGML-tools project
<LI>LSB
<LI>discussion: how can we standardize GNU/Linux help browsers
        (Eric Bischoff)
</UL>
<P>&nbsp;
<TR>
<TD VALIGN="TOP">
<B>14:30</B>
<TD>
Writing new documentation
<UL>
<LI>current status of the work
<LI>reading a message from Mike
<LI>quality issues: how does one write a new doc?
<LI>discussion: how to get it faster/more efficient/better quality
        (Lauri Watts)
</UL>
<P>&nbsp;
<TR>
<TD VALIGN="TOP">
<B>15:30</B>
<TD>
Technical problems (web)
<UL>
<LI>automatic redirection
        (Fran�ois-Xavier Duranceau)
</UL>
<P>&nbsp;
<TR>
<TD VALIGN="TOP">
<B>15:40</B>
<TD>
Technical problems (apps)
<UL>
<LI>context information for strings
        (Stephan Kulow)
<LI>kBabel further improvements
        (Mathias Kiefer)
<LI>right to left suport
        (Hans Peter Bieker)
</UL>
<P>&nbsp;
<TR>
<TD VALIGN="TOP">
<B>16:10</B>
<TD>
Pause
<P>&nbsp;
<TR>
<TD VALIGN="TOP">
<B>16:20</B>
<TD>
Demonstrations (installation and usage)
<UL>
<LI>KDE2 help browser
        (Matthias H�lzer-K�pfel)
<LI>kbabel
<LI>kdedict
        (Matthias Kiefer)
<LI>DocBook tools
<LI>remote doc validation service
<LI>conglomerate
        (Eric Bischoff)
<LI>emacs in PSGML mode
<LI>sgmldiff
        (Frederik Fouvry)
<LI>ispell
<LI>ortho
<LI>French web site
        (Fran�ois-Xavier Duranceau)
<LI>db-gui
	(Andrej Vernekar)
<LI>ktranslator
<LI>flexicon
        (Thomas Diehl)
</UL>
<P>&nbsp;
<TR>
<TD VALIGN="TOP">
<B>18:00</B>
<TD>
Hacking session
<UL>
<LI>fix problems
<LI>write doc
<LI>translate doc and apps
</UL>
<P>&nbsp;
<TR>
<TD VALIGN="TOP">
<B>20:00</B>
<TD>
Social event at "der Pleitegeier"
</TABLE>

<H3>Sunday 24/09/00</H3>
<P>
<TABLE>
<TR>
<TD VALIGN="TOP">
<B>09:00</B>
<TD>
Technical problems (doc)
<UL>
<LI>screen shots
        (Lauri Watts)
<LI>FDL licence usage
<LI>the anchors
<LI>localization of the style sheets
        (Frederik Fouvry)
<LI>DocBook philosophy and its implications
<LI>need for a DocBook editor
<LI>move to XML, XSL and DocBook 4.0/4.1?
        (Eric Bischoff)
<LI>CVS branches for doc?
<LI>docs on Web server?
	(Stephan Kulow)
</UL>
<P>&nbsp;
<TR>
<TD VALIGN="TOP">
<B>10:00</B>
<TD>
Internationalization issues
<UL>
<LI>unicode and other encodings
<LI>locales
<LI>fonts for non-latin charsets
<LI>conversion &amp; visualization tools
        (Hans-Peter Bieker)
</UL>
<P>&nbsp;
<TR>
<TD VALIGN="TOP">
<B>10:20</B>
<TD>
Pause
<P>&nbsp;
<TR>
<TD VALIGN="TOP">
<B>10:30</B>
<TD>
Translation issues
<UL>
<LI>motivation
<LI>communication with developpers
<LI>putting our work online?
<LI>dispatching the work
<LI>glossaries
<LI>visual dictionnary
        (Eric Bischoff)
</UL>
<P>&nbsp;
<TR>
<TD VALIGN="TOP">
<B>11:30</B>
<TD>
policies and priorities
<UL>
<LI>what do we most need?
<LI>what do we do next?
<LI>to freeze or not to freeze?
<LI>distribute separatly docs &amp; apps?
<LI>attitude towards "small" languages
<LI>how can we organize better?
        (Thomas Diehl)
</UL>
<P>&nbsp;
<TR>
<TD VALIGN="TOP">
<B>12:30</B>
<TD>
Lunch
<P>&nbsp;
<TR>
<TD VALIGN="TOP">
<B>14:00</B>
<TD>
Conclusion, farewell words
      (Thomas Diehl &amp; Eric Bischoff)
<P>&nbsp;
<TR>
<TD VALIGN="TOP">
<B>14:10</B>
<TD>
Visit to Caldera offices
<P>&nbsp;
<TR>
<TD VALIGN="TOP">
<B>15:00</B>
<TD>
Hacking session
<UL>
<LI>fix problems
<LI>write doc
<LI>translate doc and apps
</UL>
</TABLE>

<P>
<HR>
<P>
<H2><A NAME="decisions">3. Decided action items</A></H2>
<P>
<H3><A NAME="3.1">3.1 - Translation teams</A></H3>
<P>
Almost all the teams have finished applications translation by
now. Documentation rewriting for KDE 2 is 50% complete.  Now it's time
to start translating the documentation itself.
<P>
If there are opportunities to coordinate dictionaries across projects
(LDP, Gnome, governemental organizations, etc), we must take them.
<P>
All the team leaders should be on kde-i18n-doc <TT>;-)</TT>.
<P>
Never fix mistakes of the original texts in the translation.  Instead,
translate the mistake and forward a bug report.
<P>
Don't translate first drafts of documentation in the CVS: always
control the summary page on <A
HREF="http://i18n.kde.org/doc">http://i18n.kde.org/doc</A>.
<P>
Do not update the <TT>&lt;date&gt;</TT> and the
<TT>&lt;releaseinfo&gt;</TT> when translating - it breaks the upcoming
updates process. Respect the formats 20/12/2000 and 1.01.25 to enable
automation.
<P>
Share the information that on a Mandrake, first uninstall the docbook
packages then reinstall the ones at <A
HREF="ftp://ftp.kde.org/pub/kde/devel/docbook">ftp://ftp.kde.org/pub/kde/devel/docbook</A>.
<I>Note: the Mandrake packager of DocBook tools knows about this and
ensures this will be fixed for 7.2.</I>
<P>
We need to publish again the docs on some web server. Find a machine
first.
<P>
We should be prepared for KDE 2 press releases so they get
instantaneously translated.  Team leaders should subscribe to kde-pr.
<P>
Pedro Morais will write a small "howto deal with translations" for
developers. Topics include anchors and calls to <TT>InvokeHelp()</TT>,
context information in po files, common mistakes, who should / how to
write documentation, etc.

<H3><A NAME="3.2">3.2 - Developpers</A></H3>
<P>
Developpers should help more the documentation writers to write their
doc, at least by answering questions.
<P>
When applications move from one package to the other, it should be
posted on kde-i18n-doc mailing list.
<P>
The default style and theme has changed again lately despite the UI
freeze!  This means we have to redo all the screen shots <TT>:-(</TT>.
<P>
The UI freeze on messages should be respected more strictly. We know
it's hard, but...
<P>
KDE does not use locales to sort. This is a localization bug.
<P>
Find a suitable technical way to add the name of the translators to the
"about" box of the applications (for motivation reasons).
<P>
kwrite needs to be able to write files in UTF-8 format, otherwise the
translators keep their time recoding po files.
<P>
kdevelop has a documentation mainly done for KDE 1. A big update seems
necessary.
<P>
khelpcenter should show up a page to redirect people at run time if
the documentation did not compile, did not exist or was not installed,
to help them solve the problem.
<P>
koffice is still moving too much to be able to be localized.
<P>
kbabel should add context on the near lines in the po files.
<P>
Make sure kdelibs does not generate anchors with upper case in calls to
InvokeHelp() (there seems to be a technical problem here).

<H3><A NAME="3.3">3.3 - DocBook team</A></H3>
<P>
DocBook is still too hard to install despite many past efforts.
Continue the simplification and standardization.
<P>
Move the <TT>INSTALL</TT> file for DocBook to the web server.
<P>
Document the fact that the screenshot filename should be translated.
<P>
Document the fact that doc writers should not use revision histories,
they are duplicate with the CVS logs.
<P>
Investigate if it is possible to share an user glossary between
documents while keeping proper markup.

<H3><A NAME="3.4">3.4 - Editorial team</A></H3>
<P>
Merge the two coordination web pages (the one for writers and the one
for translators) (?). Add a section "new docs already updated" to start
the update process of KDE 2 docs.
<P>
The doc writers should be more visible on the mailing lists. For
example, they should subscribe to kde-i18n-doc. The can also subscribe
to kde-devel.
<P>
There's not enough communication between writers themselves. Create a
new mailing list?
<P>
The name and email of the doc author is not enough for reporting bugs
(bouncing emails) and some writers resent giving out their email, which
is their right. We need a mail address for documentation bugs:
<UL>
<LI>
in khelpcenter pages
<LI>
on http://bugs.kde.org
(perharps use the hypotetical new mailing list for doc writers?)
</UL>
<P>
We need more style rules (capitalization, vocabulary, etc).
<P>
According to the FDL, we should copyright the docs.
<P>
In khelpcenter/contact.docbook, there should be a pointer to the teams
page so people know whom to contact to help translating.
<P>
Move the visual dictionnary from http://i18n.kde.org/doc to khelpcenter.

<H3><A NAME="3.5">3.5 - Web server maintainers</A></H3>
<P>
To encourage people to translate the Web server, the news should be
separated cleanly, thus enabling to translate only what is more static.

<H3><A NAME="3.5">3.6 - Troll Tech company</A></H3>
<P>
There should be a program to detect conflicts between menu
accelerators at run time in a Qt application.
<BR>
<I>Note: this program has been developped directly at the congress by
Matthias Ettrich and is named "Dr Krash". It's something really great
<TT>:-)</TT>.</I>

<P>
<HR>
<P>

<H2><A NAME="presentations">4. Presentations</A></H2>
<P>
<H3><A NAME="4.1">4.1 - Report from the translation teams</A></H3>
<P>
Number of persons per team vary from 1 to 20 very active people, and
usually the same quantity of occasional translators, with an average
of 10 active people per team. If we take a reasonable ratio for the
teams that were not represented at the workshop, we can estimate that
KDE has more than 300 active translators and the same number of
occasional helpers. These figures do not include the doc writers (25
active and 35 occasional) and the technical teams (4
active). Occasional helpers sometimes make lose more time than they
help gain because of bad quality.
<P>
Motivation is mainly ensured by having people's names in the
newspapers and the files. Other reasons for helping are altruism,
thankfulness to KDE, motivation by guiltiness, being paid by
commercial companies to help, and bad memories of proprietary
software. Articles in newspapers always bring new volunteers but a lot
of them do not stay.
<P>
Some teams divide the work by package (kdebase, kdelibs, etc), others
work on a per-file basis. In all teams, only a few people have CVS
access.  A lot of coordination work is always necessary besides the
repartition of work; edict rules, maintain local web site, welcome
newcomers, answer or forward questions, dispatch tools and other
resources, explain a lot, and last but notleast, discuss vocabulary.
<P>
Mainling list are the preferred way to share information. The
Portuguese team directly puts the work to translate/already translated
on a web site, like it used to be before the move to kde-i18n. There
is a strong interest on doing this aggain for all teams.
<P>
A lot of teams suffered people disappearing after KDE 1. They lost a
lot of time and people due to instability of early releases of KDE 2,
people resenting to install it. Same problem for DocBook, even if it
was clear that some people could mark up and/or validate for the
others. But now most teams finished the application messages and will
start translating the documentation which has been half rewritten by
now. Only a few teams translate the KDE web server, the main problem
being the news. The French team translates the news and send them over
to a special mailing list with about 500 subscribers interested in
KDE's life. This team also collects and forwards a lot of bug reports.
<P>
On the tools hit-parade, kbabel is the star. Of course vi and emacs
(with or without po and psgml modes) are mentioned a lot too. Next
come midnight commander and kdedict. A lot of small home-made tools
also exist and would gain by being merged into kbabel or at least
being gathered on i18n.kde.org.
<P>
Most teams have already moved to UTF-8 encoding. The remaining ones
are requested to do it urgently. This change is too prematurate for
doc and will probably done at the same time as move to XML. Some teams
have problems with fonts and locale (like two letters missing in
iso-latin 1 for French and Slovak). villa@linux.ee and the Czech and
Slovak teams might help a lot on these issues. Distributions are
requested to help fix localization problems.
<P>
Some teams already share dictionnaries with other projects (Gnome,
Mozzilla, etc) or institutions. Some teams have to face the varietions
on their language (Portuguese, Spanish, ...). All the teams have
problems on how much technical terms have to be translated from
English, the extremes probably being Italian (translate almost no
technical term) and French (translate nearly all technical terms).

<H3><A NAME="4.2">4.2 - Coordination with other projects</A></H3>
<P>
There are tight contacts and mutual help with LDP (Guylhelm Aznar)
and Gnome (Dan Mueth), as well as many Linux distributions. These
contacts allow to think about the following topics:
<UL>
<LI>
have metainformation for documentation
  (Dublin Core project), integrated to DocBook or not, like
  we already have Linux Standard Modules (LSMs).
<LI>
Standardize the help browser (Dewey Project)
<LI>
Standardize the DocBook tools, in conjunction with the DocBook-tools
  project at sourceware (Mark Galassi), the SGML-tools Lite project
  (Cees De Groot) and Linux Standard Base.
<LI>
Watch the Li18nux project on infrastructure for localization
<LI>
Help the conglomerate project to write a DocBook editor, maybe
  have own own someday
</UL>
and more generally to discuss any issue regarding internationalization,
translation and documentation.

<H3><A NAME="4.3">4.3 - Doc writers</A></H3>
<P>
Mike Mc Bride sent a detailed message on the status of the
editorial team (aka "English team") and Lauri read it during the
workshop. People (and especially developers) are encouraged to read
this great report that is attached to this document.
<P>
Eric was happy to announce the upcoming DocBook conversion service by
email developped by Laurent Larzilli�re. With this service, doc writers
(and translators) are not supposed anymore to install the DocBook tools.

<H3><A NAME="4.4">4.4 - Web server</A></H3>
<P>
Automatic redirection to the translated version of the main KDE web
server is something nice and on which the user has full control to
accept or not.
<P>
Translating a web server is a voluntary choice for big temas that are
able to translate the news regularly.
<P>
Because of that, the small teams would prefer a small flag to local
servers rather than automatic redirection. But the main page is already
crowded enough so it looks like there is no easy solution to this
problem.

<H3><A NAME="4.5">4.5 - Context information in po files</A></H3>
<P>
French team needs more context according to whether a term is found in
a menu or in a button due to linguistic conventions.
<P>
All slavic languages have problems with many assumptions coming from
English languagei in strings with <TT>%1</TT>, <TT>%2</TT>, etc:
<UL>
<LI>
English only has singular and plural so two msgids are enough:
<BLOCKQUOTE><PRE>
  "Are you sure you want to delete this file"
  "Are you sure you want to delete those %1 files"
</PRE></BLOCKQUOTE>
but the termination of the names and adjectives depend on the last
digit of the number displayed at <TT>%1</TT> placeholder.
<LI>
The prepositions for days of the week, and months depend on the day or
  month that follows, while "on" and "in" are enough in English.
<LI>
Etc
</UL>

<H3><A NAME="4.6">4.6 - kbabel</A></H3>
<P>
The future of kbabel is:
<UL>
<LI>modules for external programs
<LI>support for qml language in po files
<LI>dictionaries and anti-dictionaries
<LI>use other existing po files to start translation of a new one
<LI>context information on surrounding lines
</UL>

<H3><A NAME="4.7">4.7 - Screenshots</A></H3>
<P>
Use ksnapshot, it creates nice lightweight screenshots.
<P>
Translators are encouraged to localize the name of screenshot files.
(Note: it's the same for chapter and section IDs, but not for anchors).

<H3><A NAME="4.8">4.8 - FDL licence</A></H3>
<P>
It's a quite complicated licence intended for documentation (and books
in general)
<P>
It splits the document into several parts:
<UL>
<LI>title page
<LI>cover text (front and back)
<LI>main section (normal thing)
<LI>secondary sections (off topic)
<LI>invariant sections (might not be changed)
</UL>
<P>
Basically at KDE we will produce documentation with no invariant
sections.

<H3><A NAME="4.9">4.9 - Style sheets for documentation</A></H3>
<P>
Can be localized by changing:
<UL>
<LI>the fixed texts
<LI>the typographical conventions
<LI>the order of words
<LI>...
</UL>
<P>
They should be in unicode's formal U-xxxx; notation.

<H3><A NAME="4.10">4.10 - DocBook</A></H3>
<P>
The philosophy of this standard impones to think in terms of semantics,
not of desired rendering. This is very important.

<H3><A NAME="4.11">4.11 - Glossaries</A></H3>
<P>
For user glossaries, like the current visual dictionnary after it has
moved to khelpcenter:
<P>
If we want to mark them up like DocBook documents, it's not trivial
to share them between documents. Some work on this might be needed.
<P>
For translation glossaries, kdedict seems the good format if we want to
check consistency automatically.

<P>
<HR>
<P>

<H2><A NAME="kdeeditorial">5. KDE Editorial Team Status Report</A></H2>
<P>
<H3>5.1 Background (where we started from):</H3>
<P>
In November, 1999, the KDE Editorial Team (at that time called the
KDE English team), was non-existant.  The previous documentation
coordinator had stopped answering his emails for many months prior to
my contact with anyone on the KDE project.
<P>
I signed on to document KWord, and began writting.  At this time, I
was the only member of the documentation/translation team who belonged
to the English team.  When I commited my first attempt at KWord
documentation (December 1999), three people wrote me directly to ask
how they could help with KOffice documentation.  This was the full
extent of the "English team" until March 2000, when it became clear to
me that no one was stepping forward to help coordinate the English
team, I contacted Eric to offer any help (initially in an unofficial
position) in this area.  After a few emails between Eric, Fredrik and
myself, I accepted the role of heading up the English team.
<P>
In April, a general call was put out on the KDE Home page, and several
other KDE related locations, asking for volunteers to write
documentation.  Over 60 people expressed interest in helping.
Unfortunatly, at the time, KDE was very much an alpha project, and
many of these people became frustrated with the technical hurdles of
downloading, compiling and installing KDE at that stage, and left the
project.  Others have left for reasons outside of KDE (school, family,
personal tragedy, etc), and today, the Editorial team consists of
approximately 25 members.  Nearly all of these people have commited at
least one piece of documentation to the CVS, and many of them have
commited 3 or more seperate applications.

<H3>5.2 Accomplishments:</H3>
<P>
<OL>
<LI>
The infrastructure is in place, and coordinators (Lauri and Mike) have
enough background concerning the technical aspects of the KDE project
to be efficient.
<LI>
The editorial team has a website with information on writting, marking
up, and submitting documentation, FAQ's, help for installing
snapshots, and a status page so anyone can see who is working on which
applications documentatin, and its status.
<LI>
The editorial team has completed (in time for KDE 2.0):
<BLOCKQUOTE><PRE>
kdeadmin   66% (4 of 6)
kdebase  75% (9 of 12)
kdegames 36% (8 of 22)
kdegraphics 20 % (1 of 5)
kdemultimedia 50% (3 of 6)
kdenetwork 20% (2 of 8)
kdepim 50% (1 of 2)
kdeutils 71% (10 of 14)
TOTAL 38 pieces
</PRE></BLOCKQUOTE>
<P>
In order to qualify for this list, all of these documents must be: 1)
updated for KDE 2.0; 2) Current (to the best of our knowledge), 3)
Installed correctly, and correctly "attached" to the help features of
the application; 4) Correctly marked up for docbook 3.1.
<LI>
The editorial team is currently working on 19 additional
applications, which are at various stages of development.
<LI>
Every application in KDE 2.0 correctly loads a help page (even if
documentation has not been completed).  The most complete and current
version of documentation is made available, and if the documentation
has not been written at all, a small help page pointing users to
helpful links is loaded in its place.
<LI>
All documentation for unreleased/discontinued applications has been
moved/removed from the CVS.
<LI>
All docbook files, compile without errors when using jade and the
customized KDE files.
<LI>
Several documenters spent many weeks adding "What is" help to
kcontrol
modules, and applications.
</OL>

<H3>5.3 Way of working</H3>
<P>
The KDE Editorial team is organized as follows:
<P>
Coordinators: <B>Mike McBride</B> (team coordination, recruitment,
helps documenters with technical problems, maintaining/editing CVS for
the team, misc tasks), <B>Lauri Watts</B> (docbook markup,
documentation consistency, proofreading documentation)
<P>
Working with the two coordinators, are documenters, which make up the
greatest proportion of our team
<P>
There is currently one person, who is proofreading documentation and
watching for application changes.

<H3>5.4 The stated mission of the editorial team is to:</H3>
<P>
<OL>
<LI>
Compose new documentation and update previous versions of
documentation for KDE applications.
<LI>
Assist translation teams whenever possible so that they may
effectively and accurately translate this documentation.
<LI>
Work to ensure that all English documentation is correct and complete
with regards to docbook markup.
<LI>
Recruit new people to write documentation.
</OL>

<H3>5.5 New documentation is currently written as follows:</H3>
<P>
<OL>
<LI>
New documenters contact Mike, who answers questions, asks them to
read the
FAQ and guides to writing documentation on the web page.
<LI>
Documenter downloads a current snapshot and installs it
(questions/problems are referred to Mike who answers them if he can,
if not, the questions are referred to kde-devel).
<LI>
Once installed, the documenters and Mike work together to choose an
application for them to write about.
<LI>
The documenter contacts the primary developer on the application.
<LI>
The editorial team status page is marked "Started", to signify that
someone is working on that documentation.
<LI>
The documenter writes the documentation either in plain ASCII, or
docboook, depending on their level of knowledge of docbook.  If there
is previous documentation for an application, then the documenter uses
the "old" documentation as a starting point for the new documentation.
<LI>
After the documenter feels they have finished their documentation,
they email the document to either Lauri or Mike (who forwards it
immediately to Lauri).
<LI>
Lauri reviews the submitted documentation for: Correct use of docbook
markup, correct grammer/punctuation, accuracy, completeness, and
overall organization. Lauri works with the documenter to correct any
problems.
<LI>
Lauri sends the corrected documentation to Mike, who verifies the
docbook is error free, and briefly checks it for obvious errors.  Mike
then submits the documentation to the CVS, updates the two status
pages, updates the credits page, and informs the documenter that the
documentation has been sent to the CVS.
<LI>
The next time the documenter updates their source code, they check
over the documentation to make sure everything is being installed
correctly, and that there are no errors.
<LI>
The documenter begins at step 3 with the next application.
</OL>
The Editorial team works with translators by helping them with
unfamiliar
words or phrases, updating errors discovered by translators, and by
monitoring the kde-i18n-doc and kde-docbook mailing lists.

<H3>5.6 Problems encountered: (In no particular order)</H3>
<P>
<OL>
<LI> Docbook tools are very difficult for most people to install.  Even
though
this is not a requirement for documenters, approximatly half of the
documenters insist on installing this program so that they can check
their
work, see how it looks, etc.  More than 10 potential documenters have
been
lost from the frustration of installing docbook tools.  I know that Eric
has
been working on simplifying this, and the frustration that I have noted,
has
been less over the past couple of months, though occasional discussions
of
this subject do occur on different mailing lists.

<LI> Early volunteers often had many problems installing KDE, since it was
still alpha code.  Now that KDE is more stable and robust, volunteers
have an
easier time installing and working with KDE, so fewer people are
leaving.

<LI> It has been very difficult to determine which applications would be
part
of which packages (or had been discontinued entirely).  Changes are made
to
the contents of packages, and to my knowledge, there is no central
location
to determine what applications are in which packages and no official
announcements are made.

<LI> The exact cutoff for submission of new documentation is not well
defined
in any of the recent release schedules.

<LI> Developers fail to return email messages from documenters or
documentation
coordinators regarding specific and usually technical questions. This
has the
obvious effect of delaying documentation, it also has the more
signifigant
effect of making documenters feel unappreciated and "like second class
citizens".

<LI> Developers will change the user interface of an application without
telling documenters, without any apparent announcements to help
documenters
out, and often after "feature freezes".

    (On September 16, Waldo Bastian has implemented an addition to the
CVS,
so that  the documentation team is notified whenever developers include
the
phrase "GUI" in their CVS commits.  It is too early to determine if this
is
effective yet, or not).

<LI> Markup choices have been a moving target for us, since many of the
details
were worked out as we went along and since Lauri and I were still
learning
the in's and outs of docbook as it relates to KDE.

<LI> Documenters have left the project without telling us.

<LI> Documentation that is written in a foreign language must be
translated
into English before other translations can translate it, but this
Language X
--> English translation is very slow.

<LI> A documenter was split off to help "Proofread the English of the
user
inteface."  After I sent him to the kde-i18n-doc mailing list, and told
the
people who had been requesting this person, no one ever helped him get
started with the project, so he sat waiting for over 2 months for some
help.
This has resulted in 1) A docuemnter not helping the documentation team
because he felt uneeded; 2) There are many minor problems with the
messages
of applications, that could have been fixed prior to a user interface
freeze.

<LI> Certain consistency issues have not been successfully implemented.
This
is usually due to Lauri or Mike not recognizing the need for consistancy
in
this specific situation.

<LI> Snapshot and graphics files locations and requirements were changed
part
way through documentation.  The required that all snapshots be updated,
and
that many large application documents be edited.  This has not been
completed.

<LI> Some documentation was written without informing the KDE
documentation
team.  This resulted in two instances where work on documentation was
discarded because of two different versions of documentation for an
application.

<LI> When applications were moved among the kde packages (into another
package, to kdenonbeta, or deleted entirely), the documentation was
often
left in its original place.  This is really a minor problem, and if I am
informed of package changes, then I can make sure the documentation is
where
it belongs.
</OL>

<H3>5.7 Writing new documentation</H3>
<P>
The current status of the KDE documentation project can always be
assessed at <A HREF="http://i18n.kde.org/teams/en/current.html.">http://i18n.kde.org/teams/en/current.html</A>.
<P>
This page lists all applications which the documentation team is aware
of, and what status they are in with reguards to KDE 2.0.  After the
release of KDE 2.0, the format of this page will change, but this will
still be the most organized way to see how things progressing.
<P>
Translators will be more interested in the "Work in Progress" page,
located at <A
HREF="http://i18n.kde.org/doc/work-in-progress.html">http://i18n.kde.org/doc/work-in-progress.html</A>.
<P>
Which lists the documentation files, in order of last update.  It also
groups each help file into a category (Done, Still using KDE 1.1.x
documentation, Deleted, etc).

<H3>5.8 To summarized the status of the documentation project:</H3>
<P>
Approximately 50% of the documentation is completely up to date for
KDE 2.0.  Focus has been placed on documentation for applications that
are either very popular (Kmail, konqueror, konsole, kscd, etc), are
used by new users (aKtion, kfloppy, etc) or that are more complex
(Kmail, KWrite, etc...).  This was done because it is likely that
these are the applications that users will refer to the help files the
most.  Approximately 6 or 7 applications have no documentation at all.
These are applications that are new to KDE, and they had no previous
documentation.  When a user selects help, they are presented with a
small help file, which informs them that documentation is not complete
yet, and suggests they visit the kde web page, or use the kde-user
mailing list for answers to questions.  The remainder of the
applications are using KDE 1.1.x documentation.  These applications
fall into one of three categories: Applications which were not
stablized until late in the process.  A good example of this, is
Knotes, which has only stablized its interface and operation in the
past couple weeks.  Games.  The previous documentation for games
generally contained good information on how the game is played.  They
were lacking information on installation, menu entries, etc.  Advanced
applications.  A good example of this is kwuftpd.  It has been
difficult to find someone on the documentation team who feels
comfortable writing documentation about this program.  Most of the
members of the documentation team are not network managers, so
sometimes there is a technical hurdle to overcome.

<H3>5.9 What works and what doesn't.</H3>
<P>
My initial goal, when I started working on this project, was (of
course), to have all documentation updated for KDE by the release of
KDE 2.0.  This goal has not been achieved.  Some of the problems that
the team has encountered, I expected, and some problems caught me
entirely by suprise.  I am, however, pleased with the progress that
the KDE documentation team has made in these past 6 months when I
consider where we were in April.

<H4>5.9.1 Works</H4>
<P>
The infrastructure is in place, and most of the bumps have been worked
out regarding coordination and communication within the team.  There
is now a KDE editorial team website (<A
HREF="http://i18n.kde.org/teams/en/index.html">http://i18n.kde.org/teams/en/index.html</A>)
, which contains a lot of information.  Obviously there is more that I
would like to include on that page, but it is a starting place for
anyone who is interested in helping us.  The new application licenses
are working well.  It is now possible for the documentation to include
any of the major licenses that are in use in KDE applications.

<H4>5.9.2 Not working well (and some possible solutions)</H4>
<P>
<H5>Problem #1</H5>
<P>
There is a large communication gap which exists between the
editorial team, and the developers.  I have tried to "get the word
out", that the editorial team is out, and hard at work, but I still
get notes from developers who "just found out there is a documentation
team".
<P>
I have sent messages to the kde-devel mailing list, but I am sure that
developers (I do this myself), only read the messages that they think
apply to them.  So when a message appears about documentation, they
skip right over it, and it never gets read by the people who need the
information.
<P>
I think a better solution, is to improve the web presence of the
documentation (and translation) teams.  Currently, the Editorial team
web page is buried so deep, I have to give people a URL for them to
find it.  There has been talk recently about updating and changing the
KDE web page.  I will work with the web designers to make it easier
for people to find out how to help the KDE documentation and
translation effort.
<P>
It is also necessary to improve the content of the documentation
servers.  After KDE 2.0 is released, I will talk with Eric, Fredrik,
Lauri and anyone else interested in improving the web site.  My goals
would be to make a site where people could:
<P>
Browse through the applications of KDE, and get a quick summary page
on the application, so that new users could find out which application
suits their needs the best.  Use this menu so that users could
download an updated copy of the documentation, download a PDF file of
the documentation, read the documentation on line.  They could also
send a message to the application developer, the bug system or the
documentation developer, through email links.  Provide useful
instructions on installing new documentation or on
internationalization issues.  Provide a system so that application
developers (whos application is not included in the base packages),
can add their documentation and links to the system.  Provide an
easier way for people to volunteer as documentation writers or
translators.
<P>
This increased presence on the web, will show the developers that the
documentation efforts are important and effective.

<H5>Problem #2</H5>
<P>
There is a lot of information that is discussed and decided on in
informal circles (private emails, irc, etc).  This is fine, and is
often the most convenient way to discuss information, but this
information must then be recorded, if it impacts people who were not
involved in the discussion.  Some examples of information that was not
available to documenters and translators who needed it are:
<OL>
<LI>
Current Release schedule.  An excellent release schedule was posted
(<A
HREF="http://developer.kde.org/development-versions/release-schedule.html">http://developer.kde.org/development-versions/release-schedule.html</A>),
which clearly spelled out each step in the release cycle, who was
responsible for that step, and what was completed.  Unfortunately,
this page was no longer updated a few weeks later, and remains more
than 45 days out of date.
<LI>
Applications have been removed from the KDE release.  It has been
difficult to ascertain which applications will be included in the KDE
2.0 release.  Obviously this is a moving target, but no announcements
were made to any kde mailing lists that I could find when such an
event happened.
<LI>
Which applications are part of which package.  This has been equally
challenging to the documenters because applications are moved between
packages, or into the kdenonbeta package, without any official
announcement.
<LI>
Who maintains the kde packages?  Who maintains the applications?.  It
is often difficult to determine who you should talk to if you have a
question about a package or an application.  Sometimes, you can use
the Help-> About option, sometimes the AUTHORS file will help,
sometimes, you can review the CVS logs to determine who is making the
major changes, but sometimes you can not determine who it should be.
</OL>
<P>
I propose that two web pages be created (and updated).
<P>
<B>A permenant KDE release schedule.</B> This should be a permenant
fixture in the KDE web pages.  There should always be some comment
about when the next release is planned.  Early on, the release
schedule could be vague ("The release of KDE 2.01 is expected to occur
in the first quarter of 2001"), but as the project gets closer to
release, the web page should become much more detailed and specific.
Since this is a schedule which is primarily driven by development, it
should be maintained by someone who is intimately involved with
developement, and who is included in discussions concerning changes in
release dates.
<P>
Another page should be created that lists <B>which applications are
currently within each package</B>, who maintains each package, and the
primary maintainer of each application.  A year ago, there was a page
like this for the KDE 1.1.x releases, and it was very helpful to me.
I am willing to maintain this page for the project if I can get
cooperation (people need to let me know when things change), and the
current information (who maintains what).
<P>
It is very important that these pages are updated on a regular basis.

<H5>Problem #3</H5>
<P>
Developers do not respond to documenters requests for information.
This even extends to documentation coordinators (Lauri and myself).
<P>
This produces several effects:
<OL>
<LI> The resulting documentation may be incomplete or inaccurate.
<LI> The documenter feels like a second class citizen to the developer she
is
trying to help.
<LI> Documentation updates are delayed.
<LI> Applications do not get documentation in time for release because
their
exact status cannot be verified.
</OL>
<P>
The solution to problem is obvious.  The question is how to convince
developers that this is the best course of events.  It seems to me it
is in the developers best interest to answer questions from
documenters.  Every bit of documentation that the documenter writes,
the developer doesn't need to worry about.  A good manual should
prevent the developer from answering the same question many times over
after its release.  (Even if the question is still asked, a polite
RTFM will answer it now).  It is not unreasonable to work with
documenters the same way you work with other developers on a project
you are programming on.  I hope that everyone involved, will try to
continue to encourage developers to work with documenters.

<H5>Problem #4</H5>
<P>
Consistancy.  We still need to work on some consistancy issues.  These
will be corrected, after the KDE 2.0 release, when there is more time,
and when these issues have been decided.  Some issues are:
<P>
Naming programs (KControl, Kcontrol, kcontrol, etc).  Do we include
application web sites (they may change too often....)?  Snapshots.
The standards for snapshots was changed part way through the 2.0
update.  Some screen shots were not redone.
<P>
There are also some markup consistency issues.  These will also be
reworked as time allows after KDE 2.0.
<?php include "footer.inc" ?>
