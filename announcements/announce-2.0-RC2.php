<?php
  $page_title = "KDE 2.0 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<P>DATELINE OCTOBER 10, 2000</P>
<P>FOR IMMEDIATE RELEASE</P>
<H3 ALIGN="center">KDE Desktop 2.0 Final Release Candidate Available for Linux<SUP>&reg;</SUP></H3>
<P><STRONG>Final Release Candidate of Leading Desktop for Linux<SUP>&reg;</SUP> and Other UNIXes<SUP>&reg;</SUP></STRONG></P>
<P>October 10, 2000 (The INTERNET).  The <A href="/">KDE
Team</A> today announced the release of KDE 2.0 RC2, the second and 
(barring any unforeseen problems) final
release candidate for Kopernicus (KDE 2.0), KDE's next-generation, powerful,
modular desktop.  The KDE team has previously released five Beta versions --
the first on May 10 of this year -- publicly; the sole prior release
candidate was released internally only.
RC2 is based on <A HREF="http://www.trolltech.com/">Trolltech's</A><SUP>tm</SUP>
Qt<SUP>&reg;</SUP> 2.2.1 and includes the core libraries,
the core desktop environment, the KOffice suite, as well
as the over 100 applications from the other standard base KDE packages: 
Administration, Games, Graphics, Multimedia, Network, Personal
Information Management (PIM), Toys and Utilities. 
This release marks the last opportunity for developers and users to
report problems prior to the official release of Kopernicus (KDE 2.0)
slated for this October 23.
</P>
<H4>Downloading and Compiling KDE 2.0 RC2</H4>
<P>
The source packages for RC2 are available for free download at
<A HREF="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0RC2/tar/src/">ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0RC2/tar/src/</A> or in the
equivalent directory at one of the many KDE ftp server
<A HREF="../mirrors/ftp.php">mirrors</A>.  RC2 requires
qt-2.2.1, which is available from the above locations under the name
<A HREF="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0RC2/tar/src/qt-x11-2.2.1.tar.gz">qt-x11-2.2.1.tar.gz</A>.
Please be advised that RC2 will <STRONG>not</STRONG> work with any
older versions of Qt.  Qt is not part of KDE's release testing.
</P>
<P>
For further instructions on compiling and installing RC2, please consult
the <A HREF="http://developer.kde.org/build/index.html">installation
instructions</A> and, should you encounter problems, the
<A HREF="http://developer.kde.org/build/index.html">compilation FAQ</A>.
</P>
<H4>Installing Binary Packages of KDE 2.0 RC2</H4>
<P>
Some distributors choose to provide binaries of KDE for certain versions
of their distributions.  Some of these binary packages for RC2 will
be available for free download under
<A HREF="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0RC2/">ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0RC2/</A>
or under the equivalent directory at one of the many KDE ftp server
<A HREF="../mirrors/ftp.php">mirrors</A>. Please note that
the KDE team is <EM>not</EM> responsible for these packages as they
are packaged by third parties, typically, but not always, the distributor
of the relevant distribution.
</P>
<P>
RC2 requires
qt2.2.1, which is available from the above locations under the name
qt-x11-2.2.1 or some variation thereof adopted by the responsible packager. 
Please be advised that RC2 will <STRONG>not</STRONG> work with any
older versions of Qt.  Qt is not part of KDE's release testing.
</P>
<P>
At the time of this release, pre-compiled packages are available for:
</P>
<UL>
<LI><A HREF="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0RC2/rpm/Mandrake/RPMS/">Linux Mandrake</A></LI>
<LI><A HREF="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0RC2/rpm/RH/">Redhat 7.0</A> and <A HREF="http://developer.kde.org/~bero/rc2/rh6.2/">RedHat 6.2</A></LI>
<LI><A HREF="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0RC2/rpm/SuSE/6.4/">SuSE 6.4</A> and <A HREF="ftp://ftp.kde.org/pub/kde/unstable/distribution/2.0RC2/rpm/SuSE/7.0/">SuSE 7.0</A></LI>
</UL>
<P>
Check the ftp servers periodically for pre-compiled packages for other
distributions.  More binary packages will become available over the
coming days.
</P>

<H4>About KDE</H4>
<P>KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environment
employing a component-based, network-transparent architecture.
Currently development is focused on KDE 2, which will for the first time
offer a free, Open Source, fully-featured office suite and which promises to
make the Linux desktop as easy to use as Windows<SUP>&reg;</SUP> and
the Macintosh<SUP>&reg;</SUP>
while remaining loyal to open standards and empowering developers and users
with Open Source software.  KDE is working proof of how the Open Source
software development model can create first-rate technologies on par with
and superior to even the most complex commercial software.</P>

<P>For more information about KDE, please visit KDE's <A HREF="../whatiskde/">web site</A>.</P>
<BR>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<TABLE BORDER=0 CELLPADDING=8 CELLSPACING=0>
<TR><TH COLSPAN=2 ALIGN="left">
Press Contacts:
</TH></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
United&nbsp;States:
</TD><TD NOWRAP>
Kurt Granroth<BR>
&#103;r&#x61;&#x6e;rot&#0104;&#x40;&#107;de.&#x6f;r&#103;<BR>
(1) 480 732 1752<BR>&nbsp;<BR>
Andreas Pour<BR>
po&#117;&#0114;&#0064;kd&#101;.o&#x72;g<BR>
(1) 718 456 1165
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (French and English):
</TD><TD NOWRAP>
David Faure<BR>
f&#x61;ur&#101;&#64;&#107;&#100;e&#x2e;&#x6f;&#0114;&#103;<BR>
(44) 1225 837409
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (German and English):
</TD><TD NOWRAP>
Martin Konold<BR>
&#x6b;o&#110;ol&#00100;&#x40;&#x6b;&#x64;&#x65;.o&#114;&#103;<BR>
(49) 179 2252249
</TD></TR>
</TABLE>

<?php include "footer.inc" ?>
