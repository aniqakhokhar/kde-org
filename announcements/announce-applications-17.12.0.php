<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships KDE Applications 17.12.0");
  $site_root = "../";
  $version = "17.12.0";
  $release = "applications-".$version;
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p align="justify">
<?php print i18n_var("December 14, 2017. KDE Applications 17.12.0 are now released.");?>
</p>

<p align="justify">
<?php print i18n_var("We continuously work on improving the software included in our KDE Application series, and we hope you will find all the new enhancements and bug fixes useful!");?>
</p>

<h3 style="clear:both;" ><?php print i18n_var("What's new in KDE Applications 17.12");?></h3>

<h4 style="clear:both;" ><?php print i18n_var("System");?></h3>
<figure style="float: right; margin: 0px"><a href="dolphin1712.gif"><img src="dolphin1712_small.gif" width="250" height="188" /></a></figure>
<p align="justify">
<?php print i18n_var("<a href='%1'>Dolphin</a>, our file manager, can now save searches and limit the search only to folders. Renaming files is now easier; simply double click on the file name. More file information is now at your hands, as the modification date and origin URL of downloaded files are now displayed in the information panel. Additionally, new Genre, Bitrate, and Release Year columns have been introduced.", "https://www.kde.org/applications/system/dolphin/");?>
</p>

<h4 style="clear:both;" ><?php print i18n_var("Graphics");?></h3>
<p align="justify">
<?php print i18n_var("Our powerful document viewer <a href='%1'>Okular</a> gained support for HiDPI displays and Markdown language, and the rendering of documents that are slow to load is now shown progressively. An option is now available to share a document via email.", "https://www.kde.org/applications/graphics/okular/");?>
</p>
<p align="justify">
<?php print i18n_var("<a href='%1'>Gwenview</a> image viewer can now open and highlight images in the file manager, zooming is smoother, keyboard navigation has been improved, and it now supports the FITS and Truevision TGA formats. Images are now protected from being accidentally removed by the Delete key when they are not selected.", "https://www.kde.org/applications/graphics/gwenview/");?>
</p>

<h4 style="clear:both;" ><?php print i18n_var("Multimedia");?></h3>
<p align="justify">
<?php print i18n_var("<a href='%1'>Kdenlive</a> now uses less memory when handling video projects which include many images, default proxy profiles have been refined, and an annoying bug related to jumping one second forward when playing backward has been fixed.", "https://www.kde.org/applications/multimedia/kdenlive/");?>
</p>

<h4 style="clear:both;" ><?php print i18n_var("Utilities");?></h3>
<figure style="float: right; margin: 0px"><a href="kate1712.png"><img src="kate1712.png" width="250" height="140" /></a></figure>
<p align="justify">
<?php print i18n_var("<a href='%1'>Ark</a>'s zip support in the libzip backend has been improved. <a href='%2'>Kate</a> has a new <a href='%3'>Preview plugin</a> that allows you to see a live preview of the text document in the final format, applying any available  KParts plugins (e.g. for Markdown, SVG, Dot graph, Qt UI, or patches). This plugin also works in <a href='%4'>KDevelop.</a>", "https://www.kde.org/applications/utilities/ark/", "https://www.kde.org/applications/utilities/kate/", "https://frinring.wordpress.com/2017/09/25/ktexteditorpreviewplugin-0-1-0/", "https://www.kde.org/applications/development/kdevelop/");?>
</p>

<h4 style="clear:both;" ><?php print i18n_var("Development");?></h3>
<figure style="float: right; margin: 0px"><a href="kuiviewer1712.png"><img src="kuiviewer1712.png" width="250" height="115" /></a></figure>
<p align="justify">
<?php print i18n_var("<a href='%1'>Kompare</a> now provides a context menu in the diff area, allowing for quicker access to navigation or modification actions. If you are a developer, you might find KUIViewers' new in-pane preview of UI object described by Qt UI files (widgets, dialogs, etc) useful. It now also supports KParts streaming API.", "https://www.kde.org/applications/development/kompare/");?>
</p>

<h4 style="clear:both;" ><?php print i18n_var("Office");?></h3>
<p align="justify">
<?php print i18n_var("The <a href='%1'>Kontact</a> team has been hard at work improving and refining. Much of the work has been modernizing the code, but users will notice that encrypted messages display has been improved and support has been added for text/pgp and <a href='%2'>Apple® Wallet Pass</a>. There is now an option to select IMAP folder during vacation configuration, a new warning in KMail when a mail gets reopened and identity/mailtransport is not the same, new <a href='%3'>support for Microsoft® Exchange™</a>, support for Nylas Mail and improved Geary import in the akonadi-import-wizard, along with various other bug-fixes and general improvements.", "https://www.kde.org/applications/office/kontact", "https://phabricator.kde.org/D8395", "https://micreabog.wordpress.com/2017/10/05/akonadi-ews-resource-now-part-of-kde-pim/");?>
</p>

<h4 style="clear:both;" ><?php print i18n_var("Games");?></h3>
<figure style="float: right; margin: 0px"><a href="ktuberling1712.jpg"><img src="ktuberling1712.jpg" width="250" height="140" /></a></figure>
<p align="justify">
<?php print i18n_var("<a href='%1'>KTuberling</a> can now reach a wider audience, as it has been <a href='%2'>ported to Android</a>. <a href='%3'>Kolf</a>, <a href='%4'>KsirK</a>, and <a href='%5'>Palapeli</a> complete the porting of KDE games to Frameworks 5.", "https://www.kde.org/applications/games/ktuberling/", "https://tsdgeos.blogspot.nl/2017/10/kde-edu-sprint-in-berlin.html", "https://www.kde.org/applications/games/kolf/", "https://www.kde.org/applications/games/ksirk/", "https://www.kde.org/applications/games/palapeli/");?>
</p>

<h3 style="clear:both;" ><?php print i18n_var("More Porting to KDE Frameworks 5");?></h3>
<?php print i18n_var("Even more applications which were based on kdelibs4 have now been ported to KDE Frameworks 5. These include the music player <a href='%1'>JuK</a>, the download manager <a href='%2'>KGet</a>, <a href='%3'>KMix</a>, utilities such as <a href='%4'>Sweeper</a> and <a href='%5'>KMouth</a>, and <a href='%6'>KImageMapEditor</a> and Zeroconf-ioslave. Many thanks to the hard-working developers who volunteered their time and work to make this happen!", "https://www.kde.org/applications/multimedia/juk/", "https://www.kde.org/applications/internet/kget/", "https://www.kde.org/applications/multimedia/kmix/", "https://www.kde.org/applications/utilities/sweeper/", "https://www.kde.org/applications/utilities/kmouth/", "https://www.kde.org/applications/development/kimagemapeditor/");?>
<p align="justify">

<h3 style="clear:both;" ><?php print i18n_var("Applications moving to their own release schedule");?></h3>
<?php print i18n_var("<a href='%1'>KStars</a> now has its own release schedule; check this <a href='%2'>developer's blog</a> for announcements. It is worth noting that several applications such as Kopete and Blogilo are <a href='%3'>no longer shipped</a> with the Application series, as they have not yet been ported to KDE Frameworks 5, or are not actively maintained at the moment.", "https://www.kde.org/applications/education/kstars/", "https://knro.blogspot.de", "https://community.kde.org/Applications/17.12_Release_Notes#Tarballs_that_we_do_not_ship_anymore");?>
<p align="justify">

<h3 style="clear:both;" ><?php print i18n_var("Bug Stomping");?></h3>
<p align="justify">
<?php print i18n_var("More than 110 bugs have been resolved in applications including the Kontact Suite, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello and more!");?>
</p>

<h3 style="clear:both;"><?php print i18n_var("Full Changelog");?></h3>

<p align="justify">
<?php print i18n_var("If you would like to read more about the changes in this release, <a href='%1'>head over to the complete changelog</a>. Although a bit intimidating due to its breadth, the changelog can be an excellent way to learn about KDE's internal workings and discover apps and features you never knew you had.", "fulllog_applications.php?version=".$version);?>
</p>

<h4>
<?php i18n("Spread the Word");?>
</h4>
<p align="justify">
<?php print i18n_var("Non-technical contributors play an important part in KDE's success. While proprietary software companies have huge advertising budgets for new software releases, KDE often depends on word of mouth."); ?>
</p>

<p align="justify">
<?php print i18n_var("There are many ways to support the KDE Applications 17.12 release: you can report bugs, encourage others to join the KDE Community, or <a href='%1'>support the non-profit organization behind the KDE Community</a>.", "https://relate.kde.org/civicrm/contribute/transact?reset=1&id=5"); ?>
</p>

<p align="justify">
<?php i18n("Help us spread the word on the social web. You can submit stories to news sites like Reddit, Facebook and Twitter; upload screenshots of your new set-up to services like Snapchat, Instagram and Google+; or create screencasts and upload them to YouTube, Blip.tv, and Vimeo, or stream them live over Twitch!"); ?>
</p>

<p align="justify">
<?php i18n("Remember to tag your posts and uploaded materials with the <i>KDE</i> moniker, as this makes them easier to find and gives the KDE Promo Team a way to analyze coverage for the KDE Applications 17.12 release."); ?>
</p>

<!-- // Boilerplate again -->

<h4>
  <?php i18n("Installing KDE Applications 17.12 Binary Packages");?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php i18n("Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 17.12 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.");?>
</p>

<p align="justify">
  <a name="package_locations"></a><em><?php i18n("Package Locations");?></em>.
  <?php i18n("For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_Applications/Binary_Packages'>Community Wiki</a>.");?>
</p>

<h4>
  <?php i18n("Compiling KDE Applications 17.12");?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php i18n("The complete source code for KDE Applications 17.12 may be <a href='http://download.kde.org/stable/applications/17.12.0/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications-17.12.0.php'>KDE Applications 17.12.0 Info Page</a>.");?>
</p>

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
