<?php
    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.73.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.73.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>


<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
August 01, 2020. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.73.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Center 16px help-about and help-whatsthis");?></li>
<li><?php i18n("Add kirigami-gallery icon");?></li>
<li><?php i18n("Add Kontrast icon");?></li>
<li><?php i18n("Add applications/pkcs12 mime type icon");?></li>
<li><?php i18n("Make breeze-dark audio-volume icons match breeze");?></li>
<li><?php i18n("Make microphone style more consistent with other audio icons and pixel aligned");?></li>
<li><?php i18n("Use 35% opacity for faded waves in audio-volume icons and make mute waves faded");?></li>
<li><?php i18n("Make audio-off and audio-volume-muted the same");?></li>
<li><?php i18n("Add snap-angle icon");?></li>
<li><?php i18n("Fix 22px application-x-ms-shortcut linking to a 16px icon");?></li>
<li><?php i18n("Fix inconsistencies in mimetype icons between light/dark versions");?></li>
<li><?php i18n("Fix invisible light/dark differences in vscode and wayland icons");?></li>
<li><?php i18n("Add document-replace icon (Like for Overwrite action)");?></li>
<li><?php i18n("Add template for making python scripts that edit SVGs");?></li>
<li><?php i18n("Add SMART status icon (bug 423997)");?></li>
<li><?php i18n("Add \"task-recurring\" and \"appointment-recurring\" icons (bug 397996)");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Add ecm_generate_dbus_service_file");?></li>
<li><?php i18n("Add ecm_install_configured_file");?></li>
<li><?php i18n("Export Wayland_DATADIR");?></li>
</ul>

<h3><?php i18n("KActivitiesStats");?></h3>

<ul>
<li><?php i18n("Ignore BoostConfig.cmake if present (bug 424799)");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("Support highlight indicator for QWidget and QtQuick based module");?></li>
<li><?php i18n("Add method to clear plugin selector (bug 382136)");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Update sGlobalFileName when QStandardPaths TestMode is toggled");?></li>
<li><?php i18n("API dox: state explicitly expected encoding for KConfig key &amp; group names");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("KCModule: Indicate when a setting has been changed from the default value");?></li>
<li><?php i18n("When resetting to system default do not use the standard palette of the style");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Introduce KRandom::shuffle(container)");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("SettingStateBinding : expose whether non default highlight is enabled");?></li>
<li><?php i18n("Make sure KF5CoreAddons is installed before using KF5Declarative");?></li>
<li><?php i18n("Add KF5::CoreAddons to public interface for KF5::QuickAddons");?></li>
<li><?php i18n("Introduce SettingState* elements to ease KCM writing");?></li>
<li><?php i18n("support config notifications in configpropertymap");?></li>
</ul>

<h3><?php i18n("KDE GUI Addons");?></h3>

<ul>
<li><?php i18n("Move KCursorSaver from libkdepim, improved");?></li>
</ul>

<h3><?php i18n("KImageFormats");?></h3>

<ul>
<li><?php i18n("Adapt license to LGPL-2.0-or-later");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("KFilterCombo: application/octet-stream is also hasAllFilesFilter");?></li>
<li><?php i18n("Introduce OpenOrExecuteFileInterface for handling opening executables");?></li>
<li><?php i18n("RenameDialog: Show if files are identical (bug 412662)");?></li>
<li><?php i18n("[rename dialog] Port Overwrite button to KStandardGuiItem::Overwrite (bug 424414)");?></li>
<li><?php i18n("Show up to three file item actions inline, not just one (bug 424281)");?></li>
<li><?php i18n("KFileFilterCombo: Show extensions if there's repeated mime comment");?></li>
<li><?php i18n("[KUrlCompletion] Don't append / to completed folders");?></li>
<li><?php i18n("[Properties] Add SHA512 algorithm to checksums widget (bug 423739)");?></li>
<li><?php i18n("[WebDav] Fix copies that include overwrites for the webdav slave (bug 422238)");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("Support action visibility in GlobalDrawer");?></li>
<li><?php i18n("Introduce FlexColumn component");?></li>
<li><?php i18n("Mobile layout optimizations");?></li>
<li><?php i18n("A layer must cover the tabbar as well");?></li>
<li><?php i18n("PageRouter: actually use preloaded pages when pushing");?></li>
<li><?php i18n("Improve (Abstract)ApplicationItem docs");?></li>
<li><?php i18n("Fix segfault on PageRouter teardown");?></li>
<li><?php i18n("Make ScrollablePage scrollable with keyboard");?></li>
<li><?php i18n("Improve accessibility of the Kirigami input fields");?></li>
<li><?php i18n("Fix the static build");?></li>
<li><?php i18n("PageRouter: Add convenience APIs for otherwise manual tasks");?></li>
<li><?php i18n("Introduce PageRouter lifecycle APIs");?></li>
<li><?php i18n("Force a low Z order of ShadowedRectangle's software fallback");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("KSelectionProxyModel: allow using the model with new-style connect");?></li>
<li><?php i18n("KRearrangeColumnsProxyModel: fix hasChildren() when no columns are set up yet");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("[QtQuick dialog] use update icon that exists (bug 424892)");?></li>
<li><?php i18n("[QtQuick GHNS dialog] Improve combobox labels");?></li>
<li><?php i18n("Don't show the downloaditem selector for only one downloadable item");?></li>
<li><?php i18n("Rejig the layout a little, for a more balanced look");?></li>
<li><?php i18n("Fix layout for the EntryDetails status card");?></li>
<li><?php i18n("Only link to Core, not the quick plugin (because that won't work)");?></li>
<li><?php i18n("Some prettification of the welcome screen (and don't use qml dialogs)");?></li>
<li><?php i18n("Use the dialog when a knsrc file is passed, otherwise not");?></li>
<li><?php i18n("Remove the button from the main window");?></li>
<li><?php i18n("Add a dialog (for showing the dialog directly when passed a file)");?></li>
<li><?php i18n("Add the new bits to the main application");?></li>
<li><?php i18n("Add a simple (at least for now) model for showing knsrc files");?></li>
<li><?php i18n("Move the main qml file into a qrc");?></li>
<li><?php i18n("Turn the KNewStuff Dialog test tool into a proper tool");?></li>
<li><?php i18n("Allow to delete updatable entries (bug 260836)");?></li>
<li><?php i18n("Do not focus first element automatically (bug 417843)");?></li>
<li><?php i18n("Fix display of Details and Uninstall button in Tiles view mode (bug 418034)");?></li>
<li><?php i18n("Fix moving buttons when search text is inserted (bug 406993)");?></li>
<li><?php i18n("Fix missing parameter for translated string");?></li>
<li><?php i18n("Fix details install dropdown in QWidgets dialog (bug 369561)");?></li>
<li><?php i18n("Add tooltips for different view modes in QML dialog");?></li>
<li><?php i18n("Set entry to uninstalled if installation fails (bug 422864)");?></li>
</ul>

<h3><?php i18n("KQuickCharts");?></h3>

<ul>
<li><?php i18n("Also take model properties into account when using ModelHistorySource");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("Implement KConfig watcher for enabled plugins and runner KCMs (bug 421426)");?></li>
<li><?php i18n("Do not remove virtual method from build (bug 423003)");?></li>
<li><?php i18n("Deprecate AbstractRunner::dataEngine(...)");?></li>
<li><?php i18n("Fix disabled runners and runner config for plasmoid");?></li>
<li><?php i18n("Delay emitting metadata porting warnings until KF 5.75");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Add overload to invoke terminal with ENV variables (bug 409107)");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Add icons to all buttons of file modified message (bug 423061)");?></li>
<li><?php i18n("Use the canonical docs.kde.org URLs");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("use better Name and follow HIG");?></li>
<li><?php i18n("Mark API as deprecated also in D-Bus interface description");?></li>
<li><?php i18n("Add copy of org.kde.KWallet.xml without deprecated API");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("plasma-window-management:  Adapt to changes in the protocol");?></li>
<li><?php i18n("PlasmaWindowManagement: adopt changes in the protocol");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("KMultiTabBar: paint tab icons active on mouseover");?></li>
<li><?php i18n("Fix KMultiTabBar to paint the icon shifted on down/checked by style design");?></li>
<li><?php i18n("Use new overwrite icon for Overwrite GUI Item (bug 406563)");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Make KXmlGuiVersionHandler::findVersionNumber public in KXMLGUIClient");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Remove secrets logging");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Set cpp ownership on the units instance");?></li>
<li><?php i18n("Add some missing PlasmaCore imports");?></li>
<li><?php i18n("Remove usage of context properties for theme and units");?></li>
<li><?php i18n("PC3: Improve look of the Menu");?></li>
<li><?php i18n("Adjust audio icons again to match breeze-icons");?></li>
<li><?php i18n("Make showTooltip() invokable from QML");?></li>
<li><?php i18n("[PlasmaComponents3] Honor icon.[name|source] property");?></li>
<li><?php i18n("Filter on formfactors if set");?></li>
<li><?php i18n("Update mute icon style to match breeze-icons");?></li>
<li><?php i18n("Remove broken metatype registration for types missing the \"*\" in the name");?></li>
<li><?php i18n("Use 35% opacity for faded elements in the network icons");?></li>
<li><?php i18n("Don't show Plasma dialogs in task switchers (bug 419239)");?></li>
<li><?php i18n("Correct QT Bug URL for font rendering hack");?></li>
<li><?php i18n("Don't use hand cursor because it is not consistent");?></li>
<li><?php i18n("[ExpandableListItem] use standard button sizes");?></li>
<li><?php i18n("[PlasmaComponents3] Show ToolButton focus effect and omit shadow when flat");?></li>
<li><?php i18n("Unify height of PC3 buttons, TextFields, and Comboboxes");?></li>
<li><?php i18n("[PlasmaComponents3] Add missing features to TextField");?></li>
<li><?php i18n("[ExpandableListItem] Fix silly error");?></li>
<li><?php i18n("Rewrite button.svg to make it easier to understand");?></li>
<li><?php i18n("Copy DataEngine relays before iterating (bug 423081)");?></li>
<li><?php i18n("Make signal strength in network icons more visible (bug 423843)");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Use \"raised\" style for non-flat toolbuttons");?></li>
<li><?php i18n("Only reserve space for the icon in toolbutton if we actually have an icon");?></li>
<li><?php i18n("Support showing a menu arrow on tool buttons");?></li>
<li><?php i18n("Really fix menu separator height on high DPI");?></li>
<li><?php i18n("Update Mainpage.dox");?></li>
<li><?php i18n("Set height of MenuSeparator properly (bug 423653)");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Clear m_deviceCache before introspecting again (bug 416495)");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Convert DetectChar to proper Detect2Chars");?></li>
<li><?php i18n("Mathematica: some improvements");?></li>
<li><?php i18n("Doxygen: fix some errors ; DoxygenLua: starts only with --- or --!");?></li>
<li><?php i18n("AutoHotkey: complete rewrite");?></li>
<li><?php i18n("Nim: fix comments");?></li>
<li><?php i18n("Add syntax highlighting for Nim");?></li>
<li><?php i18n("Scheme: fix identifier");?></li>
<li><?php i18n("Scheme: add datum comment, nested-comment and other improvements");?></li>
<li><?php i18n("Replace some RegExp by AnyChar, DetectChar, Detect2Chars or StringDetect");?></li>
<li><?php i18n("language.xsd: remove HlCFloat and introduce char type");?></li>
<li><?php i18n("KConfig: fix $(...) and operators + some improvements");?></li>
<li><?php i18n("ISO C++: fix performance in highlighting numbers");?></li>
<li><?php i18n("Lua: attribute with Lua54 and some other improvements");?></li>
<li><?php i18n("replace RegExpr=[.]{1,1} by DetectChar");?></li>
<li><?php i18n("Add PureScript highlighting based on Haskell rules. It gives pretty good approximation and starting point for PureScript");?></li>
<li><?php i18n("README.md: use the canonical docs.kde.org URLs");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.73");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.12");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://invent.kde.org/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://invent.kde.org/groups/frameworks/-/merge_requests", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
