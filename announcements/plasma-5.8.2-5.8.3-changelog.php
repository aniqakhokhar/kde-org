<?php
include_once ("functions.inc");
$translation_file = "kde-org";
$page_title = i18n_noop("Plasma 5.8.3 Complete Changelog");
$site_root = "../";
$release = 'plasma-5.8.3';
include "header.inc";
?>
<p><a href="plasma-5.8.3.php">Plasma 5.8.3</a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='breeze' href='http://quickgit.kde.org/?p=breeze.git'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>The combobox needs to be 2 pixels wider for contents to fit. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=3c7fd5a87e2e176f480b9bb67b594f59a08c9551'>Commit.</a> </li>
</ul>


<h3><a name='discover' href='http://quickgit.kde.org/?p=discover.git'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Trivial: Fix terminology. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=330c670793ed4ab0126c5e2daff01da70227145a'>Commit.</a> </li>
<li>Fix elision of sources delegate text. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=ac05888bac3a420f941bed341539b6d4f0d47e6a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371658'>#371658</a></li>
<li>Hide the page header overlay when on top. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=6065751ad9b7119c544d7437263927b089d757af'>Commit.</a> </li>
<li>Make sure the page header has a background when scrolled. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=0f807c9ae8495ad248983dd3e8509026145a28d3'>Commit.</a> </li>
<li>Make sure we never restore to a Hidden visibility. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=546ed8a1c7da6e35ffb8b7b08e08e2525cf9b961'>Commit.</a> </li>
<li>Show the progress in the install button. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=78013c3c0cd3195ba2ec1e4883dbad17a77c283a'>Commit.</a> </li>
<li>Improve update button looks. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=15bdcd27085c4183a36b75d87973d419bd4b55e4'>Commit.</a> </li>
<li>Don't cap the icon minimum size. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=1a935a7562e13ef2c81e20e0b8bea41985feae7d'>Commit.</a> </li>
<li>Properly update packages state after transaction. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=aacc46733056be6f5681f512efdb652fae9bf629'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370901'>#370901</a></li>
<li>Fix fetching. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=3df5025817456f827947fd41dc1af9eab23afc73'>Commit.</a> </li>
<li>Don't uncheck if the selected category is called again. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=df8552fb4ab584e5478c6e73dd9349c171ec26fc'>Commit.</a> </li>
<li>Be explicit on category navigation. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=d07093e0443cc1e7dea8d90d2b1264cc0f18ade2'>Commit.</a> </li>
<li>Clear search text field when reseting back home. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=a8cb29501e4e0200b282017eeb5e260e336476d3'>Commit.</a> </li>
<li>When requesting search, select the search text. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=b3a911e2f96d225508f775ae57a574e162c85277'>Commit.</a> </li>
<li>Compress packagekit getDetails requests. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=96a163bf16dbbee864443b761b86c6207c3bcfc7'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>Guard against dataengine missing. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=360036fb0bdcf2b29104972dcdce6f8b3880e3c7'>Commit.</a> See bug <a href='https://bugs.kde.org/371779'>#371779</a></li>
<li>Count only applications in memory usage. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=346b492b362a1ee74ea809de80c0cdaa60ef1dda'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366342'>#366342</a>. Code review <a href='https://git.reviewboard.kde.org/r/129230'>#129230</a></li>
</ul>


<h3><a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>[autotest] Add test case for window caption need to be simplified. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=55a0afca97ccfd6b0c3ff3b1516aff36e97c9c89'>Commit.</a> See bug <a href='https://bugs.kde.org/323798'>#323798</a></li>
<li>Send a pointer leave when triggering a move resize. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=82054a406a6470c763d72c39f6c7fdf5667ee629'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371573'>#371573</a></li>
<li>[autotest] Make SlidingPopupsTest a little bit more robust. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e4930c0c410d6aa0c38e8bb0887f1d1a4ffa9b11'>Commit.</a> </li>
<li>[autotests] Extend SlidingPopupTests for Wayland windows. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3aeb7367bba1c3271addf2b855541256c893d50b'>Commit.</a> </li>
<li>[autotests] Extend SlidingPopupsTest::testWithOtherEffect. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b84fd50319feed58791ec8577c18d88229c2d589'>Commit.</a> See bug <a href='https://bugs.kde.org/336866'>#336866</a></li>
<li>[autotests/effect] Add test case for Sliding Popups effect. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=42e7ffa263ddc7a13d32126ac6356a7f1b1950b3'>Commit.</a> See bug <a href='https://bugs.kde.org/336866'>#336866</a></li>
<li>Support for KWin.registerShortcut() in declarative script. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0c4c529d6884c101cfe7aee05557f18dc86802e0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/340225'>#340225</a>. Code review <a href='https://git.reviewboard.kde.org/r/129250'>#129250</a></li>
<li>[decorations/aurorae] Fix typo in Plastik theme. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=9e3fb472599e9acfa6783de9abf7648f7617d7f2'>Commit.</a> </li>
<li>[effects/translucency] Cancel existing animations before starting new. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=94c5704af74db5e0f8e86f0c572e4e11453e7002'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/342716'>#342716</a></li>
<li>Ensure the complete decoration texture gets repainted on recreation. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=37067f538ec071e8ab9637052410953973b7c864'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371735'>#371735</a></li>
<li>Trigger resize of input window when deco size changes. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=2e3c6c92e94e9902a9c808f6af92aa3afc797fdc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371284'>#371284</a></li>
<li>[autotests] Add test case for translucency effect of dialog window. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=eb88762e0793b7f4f3facbaccb6a783370d6ee0e'>Commit.</a> See bug <a href='https://bugs.kde.org/342716'>#342716</a></li>
<li>[plugins/qpa] Properly clean up the created KWayland::Client::Outputs. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=acb4336932c3f70335c1dcb9f4ae09ffcba9c63e'>Commit.</a> </li>
<li>[platformx/x11] Add a freeze protection against OpenGL. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=19147f5f85bd87a1fedb460939dfdcef76ed7242'>Commit.</a> </li>
</ul>


<h3><a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>[Task Manager] Return early if there's no launcherUrl. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=83778b041637c428c8148d48f2284378e1bd3a9a'>Commit.</a> </li>
<li>Launchers set new bool in plasma-frameworks to toggle expanded. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=616dedc7fcf59939fa27a55c70c09fc9186836f1'>Commit.</a> </li>
</ul>


<h3><a name='plasma-pa' href='http://quickgit.kde.org/?p=plasma-pa.git'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>Mute volume when decreasing volume to zero. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=6366791aaa5077e2c553b25c5d10c6029412a95c'>Commit.</a> </li>
<li>Fix crash in application shutdown (alternate fix). <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=f143312e3bb427fe3a31ae1095db4410656b749e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363291'>#363291</a></li>
</ul>


<h3><a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Actually remove the notification on swipe. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=05800ae75f805b5203d5ee8adb93b4818a9348a2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371488'>#371488</a></li>
<li>[Logout Greeter] Add QtQuickSettings. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f12a62830e811b2663e50fe8bcbb6091f341c1eb'>Commit.</a> </li>
<li>[Session Model] Show new session entry only if we actually can. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f0e27ed71bcf0c7f48edad6ee638bd7fe7160036'>Commit.</a> </li>
<li>Update screen pool connector ID ordering before adjusting desktop containments. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=9331001f4a97a1edb2b2e3f95b591f94abb17c33'>Commit.</a> See bug <a href='https://bugs.kde.org/370711'>#370711</a></li>
<li>Guard pointer. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=274752cda7e723ee21386bb642499a1244223409'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371201'>#371201</a></li>
<li>Fix translation catalog by Victor. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e2689c0b1568ffac0321fe9b5cb60b58029f4f0f'>Commit.</a> </li>
<li>Remove unused import QtGraphicalEffects. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b61c232722aa80567590ee6dfee655e93e95f7ff'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371493'>#371493</a></li>
<li>Directly show the username/password textboxes when user list is empty. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=70fc5cc70281abad8e50ae17a9d0d90817488813'>Commit.</a> </li>
</ul>


<h3><a name='powerdevil' href='http://quickgit.kde.org/?p=powerdevil.git'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>PowerDevil no longer crashes on logout. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=70177b065389db8cc822dbe88b3cdd383cd1d4cc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371127'>#371127</a></li>
</ul>


<h3><a name='user-manager' href='http://quickgit.kde.org/?p=user-manager.git'>User Manager</a> </h3>
<ul id='uluser-manager' style='display: block'>
<li>Do not ask for root permissions when it's unnecessary. <a href='http://quickgit.kde.org/?p=user-manager.git&amp;a=commit&amp;h=a666712102be7ef4dd48202cc2411921fc4d392b'>Commit.</a> </li>
</ul>


<?php
  include("footer.inc");
?>
