<?php
  $page_title = "SourceForge hosts KDE CVS";
  $site_root = "../";
  include "header.inc";
?>

<pre>

KDE and VA Linux announced at the recent Linux World Expo that the KDE
development system (our CVS) will be hosted by VA Linux with their
SourceForge setup.  SourceForge is a Open Source project hosting site
located at <a href="http://www.sourceforge.net">http://www.sourceforge.net</a>
 
This Q&A will attempt to answer some of your questions even before you
ask them!
 
<b>Q: What *exactly* will SourceForge be hosting?</b>
A: Initially, they will be hosting only our CVS repository (currently
   hosted at a university in Germany).  They will also very likely
   host our FTP server sometime soon if everything goes well.
 
<b>Q: Is that it?</b>
A: Well, that depends.  SourceForge has a very rich set of tools at
   our disposal, including mailing lists and bug tracking.  We will
   evaluate those tools on a case-by-case basis to see if they fit our
   needs.
 
<b>Q: Will the www.kde.org website be hosted by VA Linux.</b>
A: No.
 
<b>Q: Why are you switching to a new host?  What was wrong with the old
   one?</b>
A: First, we are very grateful to Uni-Luebeck for hosting our CVS for
   as long as they did.  However, the KDE project is just getting too
   big.  Our server and bandwith needs are quite high and getting 

  higher at a continuous rate.  Uni-Luebeck has been very good to us
   in the past.. but it's just not fair to them (and to the students
   there) to continue hogging their resources and their system
   administrators.  By moving to a host dedicated only to Open Source
   projects, we can expand our resources and make necessary demands
   without feeling like we are imposing on someone or getting in the
   way.
 
<b>Q: What will developers have to do to work with the new system.</b>
A: The only big change is that the CVS now uses ssh instead of the old
   unsecure pserver mechanism.  That means that all developers must
   have ssh installed on their development machines.  All developers
   with up to date entries in kde-common/accounts will be
   automatically added to the SourceForge CVS.  More details will be
   forthcoming when they emerge.
 
<b>Q: Will non-developers still have CVSup access?</b>
A: Yes.

<b>Q: Will I need ssh to access CVSup?</b>
A: No.  The only people that need ssh are developers with write access
   to CVS.  If you are not a developer, you will not need ssh.

<b>Q: Will I need ssh to compile KDE?</b>
A: No.  Compiling KDE will never require the use of ssh.  There was a
   bug in the kde configure scripts that seemed to indicate this but
   that was a mistake and a complete coincidence.

<b>Q: What version of ssh is needed?</b>
A: The last "free" version -- 1.2.27 (or something similar to that).
   OpenSSH should work.

<b>Q: Will KDE be using anonymous CVS with SourceForge?</b>
A: At least at first.  There <em>may</em> be possible problems with
   anonymous CVS locking out write access by developers.  If that
   happens, then anon CVS will be disabled.  We'll see.

If you have any other questions, please direct them to Kurt Granroth
<a href="&#109;a&#x69;l&#x74;&#111;:&#103;r&#97;&#00110;roth&#64;&#107;&#100;e.o&#114;&#103;">g&#114;an&#x72;ot&#x68;&#064;kd&#0101;.&#111;&#114;&#x67;</a>.  I will answer most questions directly but will
compile a list of frequently asked ones if such a need arises. 

</pre>

<?php include "footer.inc" ?>
