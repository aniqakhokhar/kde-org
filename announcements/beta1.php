<?php
  $page_title = "KDE 1.0-beta1 Information";
  $site_root = "../";
  include "header.inc";
?>

<CENTER><TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH="570" >
<TR VALIGN=TOP>
<TD>
<BR>
<h2>Where to get the base packages</h2>

You can download the KDE base  packages  from
ftp.kde.org or one of its many mirrors. The  precise
locations of the beta 1 packages on ftp.kde.org are:<p>

<a href="ftp://ftp.kde.org/pub/kde/stable/Beta1/distribution/tgz/binary">
ftp://ftp.kde.org/pub/kde/stable/Beta1/distribution/tgz/binary</a><br>

<a href="ftp://ftp.kde.org/pub/kde/stable/Beta1/distribution/tgz/source">
ftp://ftp.kde.org/pub/kde/stable/Beta1/distribution/tgz/source</a><br>

<a href="ftp://ftp.kde.org/pub/kde/stable/Beta1/distribution/rpm/binary">
ftp://ftp.kde.org/pub/kde/stable/Beta1/distribution/rpm/binary</a><br>

<a href="ftp://ftp.kde.org/pub/kde/stable/Beta1/distribution/rpm/source">
ftp://ftp.kde.org/pub/kde/stable/Beta1/distribution/rpm/source</a><br>

<p>
There are a great number of applications available for KDE.<br> 
<ul>
<li><a href="ftp://ftp.kde.org/pub/kde/unstable/apps/">
ftp://ftp.kde.org/pub/kde/unstable/apps</a><br>
<li> <a href="ftp://ftp.kde.org/pub/kde/stable/Beta1/apps/">
ftp://ftp.kde.org/pub/kde/stable/Beta1/apps</a><br>
</ul>
Those in the 'stable' directory tree should work fine with KDE Beta1,
while those in the unstable directory tree are not guaranteed to compile
and install without problems. Nevertheless, most work fine.
<p>
Please consider downloading from one of our many 
<a href="mirrors.html">mirror</a> sites.
<p>
You may also want to browse the
<a href="applications.html">KDE applications</a>
and the
<a href="cvs-applications.html">KDE applications in the CVS</a>
pages, for a complete overview of all available KDE applications,
Only a small percentage of all available KDE applications are part of the base distributions
made available above.


<H2>Installation Instructions</H2>

These are the installation instructions for the Beta 1 (Arnsberg) 
of the KDE Desktop Environment. Please read them carefully
and try to find out yourself if anything goes wrong. If you need
further assistance, consider joining the 
<a href"../contact.html">kde mailing lists</a>. 


<H3>Available package formats</h3>

The KDE team provides four different kinds of packages (there may be
other formats like .deb provided by third parties): source and binary
RPMs and source and binary .tgz files. The installation process
depends on which package you choose. After installation, there are
post-installation procedures that apply for all package formats. Note
that if you an unexperienced Unix user and have a RPM-based system
(like Red Hat Linux or SuSE Linux 5.0 or higher), you are probably
best off choosing the binary RPM package. If you need special
configuration options (e.g. because you have shadow passwords and want
to use the screensavers), your best bet is to use the source .tgz
packages and compile the KDE Desktop Environment yourself.


<H3>Prequisites</H3>

You need the Qt library (and header file if you want to compile KDE
yourself), version 1.31 or higher, available at no cost from
<a href="http://www.troll.no/dl">http://www.troll.no/dl</a>. 
You also need the libgr which should be included in most distributions. 


<H3>Available packages</H3>

The base distribution currently consists of eight packages. Some are
required, some are optional. Each package is available in each fo the
aforementioned package formats.

<ul>
<li><b>kdesupport</b><br><tt>RECOMMENDED</tt><br>
This package contains support libraries that have
not been written as part of the KDE project, but
are needed nevertheless. If you are already have
the libraries (libgif, libjpeg, libmime, libuu,
libgdbm) in this package in the required versions,
you might not need to install it. We do however recommend
installing this packages since it will save you trouble
in case you have incompatible version of the above mentioned
libraries

<li><b>kdelibs</b><br><tt>REQUIRED</tt><br>
This package contains shared libraries that are
needed by all KDE applications.

<li><b>kdebase</b><br><tt>REQUIRED</tt><br>
This package contains the base applications that
form the core of the KDE Desktop Environment like
the window manager, the terminal emulator, the
control center, the file manager and the panel.

<li><b>kdegames</b><br><tt>OPTIONAL</tt><br>
Various games like kmahjongg, ksnake, kasteroids
and ktetris.

<li><b>kdegraphics</b><br><tt>OPTIONAL</tt><br>
Various graphics related programs like kghostview,
kdvi, kfax and kpaint.

<li><b>kdeutils</b><br><tt>OPTIONAL</tt><br>
Various desktop tools like a calculator, an editor
and other useful accessories

<li><b>kdemultimedia</b><br><tt>OPTIONAL</tt><br>
Multimedia applications like a CD player and a
mixer.
	
<li><b>kdenetwork</b><br><tt>OPTIONAL</tt><br>
Internet applications. Currently contains the mail
program kmail and the news reader knews.
</ul>

<tt>kdesupport</tt>  should be installed before everything else. The
next package must be kdelibs. The other packages can be
installed in an arbitrary order.
<p>


<HR WIDTH=570 SIZE=1 ALIGN=CENTER NOSHADE>
<B>Note:</b><br>
The distributions do not contain all KDE applications. In fact an
ever growing number of additional kde applications, some of which of exeptional
quality and stability, are
available on ftp.kde.org. 
Please browse the
<a href="applications.html">KDE applications</a>
and the
<a href="cvs-applications.html">KDE applications in the CVS</a>
pages which nicely list all available KDE applications on ftp.kde.org.
The distributions are only meant to get you started. 
<HR WIDTH=570 SIZE=1 ALIGN=CENTER NOSHADE>

<H2>Installation instructions for the different package formats</H2>


<H3>Installation of the RPM packages</H3>

The RPM packages install per default into <tt>/opt/kde</tt>. You can override
this with the <tt>--prefix</tt> switch.
<ul>
<li>Become superuser
<li>Execute: <tt>rpm -i packagename.rpm </tt>
</ul>

<h3>Installation of the source .tgz files</h3>

The source .tgz package installs into /usr/local/kde per default. You
can override this setting by setting the environment variable KDEDIR
to the desired directory.

<ul>
<li>Unpack the packages with:<br><tt> tar xvfz packagename.tgz</tt>
<li>Change directory in to the package directory: <br><tt>cd packagename</tt>
<li>Configure the package: <br>
<tt>
./configure 
</tt><br>
Some packages (notably kdebase) have special configuration options
that might be applicable to your installation. Execute <br>
<tt>
 ./configure --help
</tt><br>
to see the available options.
<li>Build the package:<br><tt> make</tt>
<li>Install the package: <br>
<tt>
su -c "make install"
</tt>
</ul>


<h3>Installation of the binary .tgz files</h3>

The binary .tgz package installs into <tt>/opt/kde</tt>.<br>
<tt>
- Become superuser <br>
- cd / <br>
- tar xvfz packagename.tgz <br>
</tt>

<h2>Post-installation procedures</H2>


First of all, make sure that you have added KDE's binary installation
directory (e.g. </tt>/opt/kde/bin</tt>) to your <tt>PATH</tt> and KDE's library
installation directory to your <tt>LD_LIBRARY_PATH</tt> (this environment
variable may be called differently on some systems, e.g. it is called
<tt>SHLIB_PATH</tt> on Irix). Then set the environment variable <tt>KDEDIR</tt> to the
base of your KDE tree, e.g. <tt>/opt/kde.</tt>

Even though you can use most of the KDE applications simply by calling
them, you can only benefit fully from KDE's advanced features if you
use the KDE window manager kwm and its helper programs.

In order to make it easy for you, we have provided a simple script
called <tt>startkde</tt> which gets installed in <tt>$KDEDIR/bin</tt> and is therefore
in your path. Edit the file .xinitrc in your home directory (make a
backup copy first!), remove everything that looks like calling a
window manager, and insert <tt>startkde</tt> instead. Restart X. This should
present you with your shining new KDE desktop. You can now start to
explore the wonderful world of KDE. In case you want to read some
documentation first, we have provided the
<a href="http://www.kde.org/documentation/quickstart.html">quick-start guide</a>
for you.
Also, every application
has an online help that is available via the help menu.


<H3>Reporting bugs</H3>

Please send bug reports to k&#00100;e&#x2d;&#98;&#0117;g&#115;&#64;&#x6b;&#x64;&#101;&#46;&#x6f;r&#103; where they will be
distributed to the developer in charge.
<p>


Have fun with KDE!<p>

<b>The KDE Core Team</b>

</TD>
</TR>
</TABLE></CENTER>

<?php include "footer.inc" ?>
