<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.45.0");
  $site_root = "../";
  $release = '5.45.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
April 14, 2018. KDE today announces the release
of KDE Frameworks 5.45.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Attica");?></h3>

<ul>
<li><?php i18n("Explicitly set content type to form data");?></li>
</ul>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Simplify Term operator&amp;&amp; and ||");?></li>
<li><?php i18n("Do not retrieve document ID for skipped result entries");?></li>
<li><?php i18n("Do not retrieve mtime from database repeatedly when sorting");?></li>
<li><?php i18n("Do not export databasesanitizer by default");?></li>
<li><?php i18n("baloodb: Add experimental message");?></li>
<li><?php i18n("Introduce baloodb CLI tool");?></li>
<li><?php i18n("Introduce sanitizer class");?></li>
<li><?php i18n("[FileIndexerConfig] Delay populating folders until actually used");?></li>
<li><?php i18n("src/kioslaves/search/CMakeLists.txt - link to Qt5Network following changes to kio");?></li>
<li><?php i18n("balooctl: checkDb should also verify the last known url for the documentId");?></li>
<li><?php i18n("balooctl monitor: Resume to wait for service");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("add window-pin icon (bug 385170 add window-pin icon)");?></li>
<li><?php i18n("rename 64 px icons added for elisa");?></li>
<li><?php i18n("change 32px icons for playlist shuffle and repeat");?></li>
<li><?php i18n("Missing icons for inline Messages (bug 392391)");?></li>
<li><?php i18n("New icon for Elisa music player");?></li>
<li><?php i18n("Add media status icons");?></li>
<li><?php i18n("Remove frame around media action icons");?></li>
<li><?php i18n("add media-playlist-append and play icons");?></li>
<li><?php i18n("add view-media-album-cover for babe");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Make use of upstream CMake infrastructure to detect the compiler toolchain");?></li>
<li><?php i18n("API dox: fix some \"code-block\" lines to have empty lines before/after");?></li>
<li><?php i18n("Add ECMSetupQtPluginMacroNames");?></li>
<li><?php i18n("Provide androiddeployqt with all prefix paths");?></li>
<li><?php i18n("Include the \"stdcpp-path\" in the json file");?></li>
<li><?php i18n("Resolve symlinks in QML import paths");?></li>
<li><?php i18n("Provide QML import paths to androiddeployqt");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("kpackage-install-handlers/kns/CMakeLists.txt - link to Qt::Xml following changes in knewstuff");?></li>
</ul>

<h3><?php i18n("KActivitiesStats");?></h3>

<ul>
<li><?php i18n("Do not assume SQLite works and do not terminate on errors");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Look first for qhelpgenerator-qt5 for help generation");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("karchive, kzip: try to handle duplicate files in a bit nicer way");?></li>
<li><?php i18n("Use nullptr for passing a null pointer to crc32");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("Make it possible to request a plugin configuration module programatically");?></li>
<li><?php i18n("Consistently use X-KDE-ServiceTypes instead of ServiceTypes");?></li>
<li><?php i18n("Add X-KDE-OnlyShowOnQtPlatforms to KCModule servicetype definition");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("KTextToHTML: return when url is empty");?></li>
<li><?php i18n("Cleanup m_inotify_wd_to_entry before invalidating Entry pointers (bug 390214)");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Setup QQmlEngine only once in QmlObject");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("Add X-KDE-OnlyShowOnQtPlatforms to KDEDModule servicetype definition");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Add entities for Elisa, Markdown, KParts, DOT, SVG to general.entities");?></li>
<li><?php i18n("customization/ru: Fix translation of underCCBYSA4.docbook and underFDL.docbook");?></li>
<li><?php i18n("Fix duplicate lgpl-notice/gpl-notice/fdl-notice");?></li>
<li><?php i18n("customization/ru: Translate fdl-notice.docbook");?></li>
<li><?php i18n("change spelling of kwave requested by the maintainer");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("taglibextractor: Refactor for better readability");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("Don't assert if used incorrectly from dbus (bug 389375)");?></li>
</ul>

<h3><?php i18n("KHolidays");?></h3>

<ul>
<li><?php i18n("holidays/plan2/holiday_in_en-gb - update holiday file for India (bug 392503)");?></li>
<li><?php i18n("This package was not be updated. Perhaps a problem with script");?></li>
<li><?php i18n("Reworked the holiday files for Germany (bug 373686)");?></li>
<li><?php i18n("Format README.md as the tools expect (with an Introduction section)");?></li>
</ul>

<h3><?php i18n("KHTML");?></h3>

<ul>
<li><?php i18n("avoid asking for an empty protocol");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Make sure ki18n can build its own translations");?></li>
<li><?php i18n("Don't call PythonInterp.cmake in KF5I18NMacros");?></li>
<li><?php i18n("Make it possible to generate po files in parallel");?></li>
<li><?php i18n("Create a constructor for KLocalizedStringPrivate");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Make KIconEngine export comment accurate");?></li>
<li><?php i18n("Avoid an asan runtime error");?></li>
</ul>

<h3><?php i18n("KInit");?></h3>

<ul>
<li><?php i18n("Delete IdleSlave having temporary authorization");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Ensure that the model is set when resetResizing is called");?></li>
<li><?php i18n("pwd.h isn't present on windows");?></li>
<li><?php i18n("Remove Recently Saved This Month and Recently Saved Last Month entries by default");?></li>
<li><?php i18n("Have KIO build for Android");?></li>
<li><?php i18n("Temporarily disable installation of file ioslave's kauth helper and policy file");?></li>
<li><?php i18n("Handle privilege operation confirmation prompts in SlaveBase rather than in KIO::Job");?></li>
<li><?php i18n("Improve consistency of \"Open With\" UI by always showing top app inline");?></li>
<li><?php i18n("Fix crash when device emits ready read after job is finished");?></li>
<li><?php i18n("Highlight selected items when showing parent folder from the open/save dialog (bug 392330)");?></li>
<li><?php i18n("Support NTFS hidden files");?></li>
<li><?php i18n("Consistently use X-KDE-ServiceTypes instead of ServiceTypes");?></li>
<li><?php i18n("Fix assert in concatPaths when pasting a full path into KFileWidget's lineedit");?></li>
<li><?php i18n("[KPropertiesDialog] Support Checksum tab for any local path (bug 392100)");?></li>
<li><?php i18n("[KFilePlacesView] Call KDiskFreeSpaceInfo only if necessary");?></li>
<li><?php i18n("FileUndoManager: don't delete non-existing local files");?></li>
<li><?php i18n("[KProtocolInfoFactory] Don't clear cache if it had just been built");?></li>
<li><?php i18n("Don't try to find an icon for a relative URL either (e.g. '~')");?></li>
<li><?php i18n("Use correct item URL for Create New context menu (bug 387387)");?></li>
<li><?php i18n("Fix more cases of incorrect parameter to findProtocol");?></li>
<li><?php i18n("KUrlCompletion: early return if the URL is invalid like \":/\"");?></li>
<li><?php i18n("Don't try to find an icon for an empty url");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("Bigger icons in mobile mode");?></li>
<li><?php i18n("Force a content size into the background style item");?></li>
<li><?php i18n("Add InlineMessage type and Gallery app example page");?></li>
<li><?php i18n("better heuristics selective coloring");?></li>
<li><?php i18n("make loading from local svgs actually work");?></li>
<li><?php i18n("support the android icon loading method as well");?></li>
<li><?php i18n("use a coloring strategy similar to the former different styles");?></li>
<li><?php i18n("[Card] Use own \"findIndex\" implementation");?></li>
<li><?php i18n("kill network transfers if we change icon while running");?></li>
<li><?php i18n("first prototype for a delegate recycler");?></li>
<li><?php i18n("Allow OverlaySheet clients to omit the built-in close button");?></li>
<li><?php i18n("Components for Cards");?></li>
<li><?php i18n("Fix ActionButton size");?></li>
<li><?php i18n("Make passiveNotifications last longer, so users can actually read them");?></li>
<li><?php i18n("Remove unused QQC1 dependency");?></li>
<li><?php i18n("ToolbarApplicationHeader layout");?></li>
<li><?php i18n("Make it possible to show the title despite having ctx actions");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Actually vote when clicking stars in the list view (bug 391112)");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("Try to fix the FreeBSD build");?></li>
<li><?php i18n("Use Qt5::rcc instead of looking for the executable");?></li>
<li><?php i18n("Use NO_DEFAULT_PATH to ensure the right command is picked up");?></li>
<li><?php i18n("Look also for prefixed rcc executables");?></li>
<li><?php i18n("set component for correct qrc generation");?></li>
<li><?php i18n("Fix the rcc binary package generation");?></li>
<li><?php i18n("Generate the rcc file every time, at install time");?></li>
<li><?php i18n("Make org.kde. components include a donate URL");?></li>
<li><?php i18n("Mark kpackage_install_package undeprecated for plasma_install_package");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("Expose PersonData::phoneNumber to QML");?></li>
</ul>

<h3><?php i18n("Kross");?></h3>

<ul>
<li><?php i18n("No need to have kdoctools required");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("API dox: consistently use X-KDE-ServiceTypes instead of ServiceTypes");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Make it possible for KTextEditor to build on Android NDK's gcc 4.9");?></li>
<li><?php i18n("avoid Asan runtime error: shift exponent -1 is negative");?></li>
<li><?php i18n("optimization of TextLineData::attribute");?></li>
<li><?php i18n("Don't calculate attribute() twice");?></li>
<li><?php i18n("Revert Fix: View jumps when Scroll past end of document is enabled (bug 391838)");?></li>
<li><?php i18n("don't pollute the clipboard history with dupes");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Add Remote Access interface to KWayland");?></li>
<li><?php i18n("[server] Add support for the frame semantics of Pointer version 5 (bug 389189)");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("KColorButtonTest: remove todo code");?></li>
<li><?php i18n("ktooltipwidget: Subtract margins from available size");?></li>
<li><?php i18n("[KAcceleratorManager] Only set iconText() if actually changed (bug 391002)");?></li>
<li><?php i18n("ktooltipwidget: Prevent offscreen display");?></li>
<li><?php i18n("KCapacityBar: set QStyle::State_Horizontal state");?></li>
<li><?php i18n("Sync with KColorScheme changes");?></li>
<li><?php i18n("ktooltipwidget: Fix tooltip positioning (bug 388583)");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Add \"SkipSwitcher\" to API");?></li>
<li><?php i18n("[xcb] Fix implementation of _NET_WM_FULLSCREEN_MONITORS (bug 391960)");?></li>
<li><?php i18n("Reduce plasmashell frozen time");?></li>
</ul>

<h3><?php i18n("ModemManagerQt");?></h3>

<ul>
<li><?php i18n("cmake: don't flag libnm-util as found when ModemManager is found");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Export the NetworkManager include dirs");?></li>
<li><?php i18n("Start requiring NM 1.0.0");?></li>
<li><?php i18n("device: define StateChangeReason and MeteredStatus as Q_ENUMs");?></li>
<li><?php i18n("Fix conversion of AccessPoint flags to capabilities");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Wallpaper templates: set background color to ensure contrast to sample text content");?></li>
<li><?php i18n("Add template for Plasma wallpaper with QML extension");?></li>
<li><?php i18n("[ToolTipArea] Add \"aboutToShow\" signal");?></li>
<li><?php i18n("windowthumbnail: Use gamma correct scaling");?></li>
<li><?php i18n("windowthumbnail: Use mipmap texture filtering (bug 390457)");?></li>
<li><?php i18n("Remove unused X-Plasma-RemoteLocation entries");?></li>
<li><?php i18n("Templates: drop unused X-Plasma-DefaultSize from applet metadata");?></li>
<li><?php i18n("Consistently use X-KDE-ServiceTypes instead of ServiceTypes");?></li>
<li><?php i18n("Templates: drop unused X-Plasma-Requires-* entries from applet metadata");?></li>
<li><?php i18n("remove anchors of item in a layout");?></li>
<li><?php i18n("Reduce plasmashell frozen time");?></li>
<li><?php i18n("preload only after the containment emitted uiReadyChanged");?></li>
<li><?php i18n("Fix combobox breakage (bug 392026)");?></li>
<li><?php i18n("Fix text scaling with non-integer scale factors when PLASMA_USE_QT_SCALING=1 is set (bug 356446)");?></li>
<li><?php i18n("new icons for disconnected/disabled devices");?></li>
<li><?php i18n("[Dialog] Allow setting outputOnly for NoBackground dialog");?></li>
<li><?php i18n("[ToolTip] Check file name in KDirWatch handler");?></li>
<li><?php i18n("Disable deprecation warning from kpackage_install_package for now");?></li>
<li><?php i18n("[Breeze Plasma Theme] Apply currentColorFix.sh to changed media icons");?></li>
<li><?php i18n("[Breeze Plasma Theme] Add media status icons with circles");?></li>
<li><?php i18n("Remove frames around media buttons");?></li>
<li><?php i18n("[Window Thumbnail] Allow using atlas texture");?></li>
<li><?php i18n("[Dialog] Remove now obsolete KWindowSystem::setState calls");?></li>
<li><?php i18n("Support Atlas textures in FadingNode");?></li>
<li><?php i18n("Fix FadingMaterial fragment with core profile");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("fix rendering when disabled");?></li>
<li><?php i18n("better layout");?></li>
<li><?php i18n("experimental support for auto mnemonics");?></li>
<li><?php i18n("Make sure we are taking into account the size of the element when styling");?></li>
<li><?php i18n("Fix font rendering for non-HiDPI and integer scale factors (bug 391780)");?></li>
<li><?php i18n("fix icons colors with colorsets");?></li>
<li><?php i18n("fix icon colors for toolbuttons");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Solid can now query for batteries in e.g. wireless gamepads and joysticks");?></li>
<li><?php i18n("Use recently introduced UP enums");?></li>
<li><?php i18n("add gaming_input devices and others to Battery");?></li>
<li><?php i18n("Adding Battery Devices Enum");?></li>
<li><?php i18n("[UDevManager] Also explicitly query for cameras");?></li>
<li><?php i18n("[UDevManager] Already filter for subsystem before querying (bug 391738)");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Don't impose using the default client, pick one that supports the requested language.");?></li>
<li><?php i18n("Include replacement strings in the suggestion list");?></li>
<li><?php i18n("implement NSSpellCheckerDict::addPersonal()");?></li>
<li><?php i18n("NSSpellCheckerDict::suggest() returns a list of suggestions");?></li>
<li><?php i18n("initialise NSSpellChecker language in NSSpellCheckerDict ctor");?></li>
<li><?php i18n("implement NSSpellChecker logging category");?></li>
<li><?php i18n("NSSpellChecker requires AppKit");?></li>
<li><?php i18n("Move NSSpellCheckerClient::reliability() out of line");?></li>
<li><?php i18n("use the preferred Mac platform token");?></li>
<li><?php i18n("Use correct directory to lookup trigrams in windows build dir");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Make it possible to fully build the project when crosscompiling");?></li>
<li><?php i18n("Redesign CMake syntax generator");?></li>
<li><?php i18n("Optimize highlighting Bash, Cisco, Clipper, Coffee, Gap, Haml, Haskell");?></li>
<li><?php i18n("Add syntax highlighting for MIB files");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.45");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.8");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
