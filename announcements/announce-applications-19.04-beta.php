<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  require('../aether/config.php');

  $pageConfig = array_merge($pageConfig, [
      'title' => i18n_noop("KDE Ships Beta of KDE Applications 19.04"),
      'cssFile' => '/css/announce.css'
  ]);

  require('../aether/header.php');
  $site_root = "../";
  $release = 'applications-19.03.80';
  $version_text = "19.04 Beta";
  $version_number = "19.03.80";
?>

<main class="releaseAnnouncment container">

<?php
  include "./announce-i18n-bar.inc";
?>

<p>
<?php i18n("March 22, 2019. Today KDE released the beta of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.");?>
</p>

<p>
<?php print i18n_var("Check the <a href='%1'>community release notes</a> for information on tarballs and known issues. A more complete announcement will be available for the final release.", "https://community.kde.org/Applications/19.04_Release_Notes");?>
</p>

<p>
<?php i18n("The KDE Applications 19.04 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the beta <a href='https://bugs.kde.org/'>and reporting any bugs</a>.");?>
</p>

<!-- Boilerplate -->

<section class="row get-it">
    <article class="col-md">
        <h2><?php i18n("Package Downloads");?></h2>
        <p>
            <?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
        </p>
        <p><a href='https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro' class="learn-more"><?php i18n("Package download wiki page");?></a></p>
    </article>

    <article class="col-md">
        <h2><?php i18n("Source Downloads");?></h2>
        <p><?php print i18n_var("The complete source code for KDE Applications %1 may be <a href='http://download.kde.org/unstable/applications/%2/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications-%3.php'>KDE Applications %4 Info Page</a>.", $version_text, $version_number, $version_number, $version_text);?></p>
    </article>
</section>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.");?>
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
?>
