<?php
  $page_title = "Announcing KDE 3.3 Beta 2 (\"Kollege\")";
  $site_root = "../";
  include "header.inc";
?>

<p>DATELINE July 22, 2004</p>

<h3 align="center">KDE Project Ships Second Beta of Next Major Release</h3>

<p>
July 22, 2004 (The Internet) - 
The <a href="http://www.kde.org">KDE Project</a> is pleased to announce
the immediate availability of KDE 3.3 Beta 2. The focus of this release,
code-named Kollege, is to fix bugs in the run-up to <a
href="http://conference2004.kde.org">aKademy</a> in late August.
</p>

<h4>Getting Kollege</h4>

<p>
KDE 3.3 Beta 2 can be downloaded over the Internet by visiting <a
href="http://download.kde.org/unstable/3.2.92">download.kde.org</a>.
Source code and vendor supplied binary packages are available. For
additional information on package availability and to read further
release notes,
please visit the <a href="http://www.kde.org/info/3.3beta2.php">KDE 3.3
Beta 2 information page</a>.</p>

<h4>Help the KDE team squash bugs</h4>

<p>Kollege is very stable, and you can enjoy lots of new features and
applications if you're still using KDE 3.2 or older. But many bugs
remain, and though hundreds are being fixed each week, hundreds more are
found.
The KDE team asks you to try out this release, and to then help them by
finding bugs, reporting them on the <a href="http://bugs.kde.org">the
bug tracking system</a>, and helping developers fix them. If you
reported bugs which are still open for past KDE versions, please report
if they still can be reproduced with this version.</p>

<p>If you've not done this before, or you'd like to do more than enter
the occasional wish or crash report, you might want to read the Quality
Team <a href="http://quality.kde.org/develop/howto/howtobugs.php">Bug
Report and
Management HOWTO</a>. It shows you how to use Bugzilla, including
explanations of all the terminology, and it offers help on managing your
reports while the developers try and fix them. To be really helpful, you
can manage other people's reports as well, so that coders can focus on
their work.</p>

<p>You can also help out the KDE Project in lots of other ways. For a
good gateway into helping the project, whether you can write code,
documentation, report and manage bugs, do media work or just help
developers communicate with users, visit the <a
href="http://quality.kde.org/">Quality Team web site</a> for ideas. Or,
if you're pressed for time, consider <a
href="http://www.kde.org/support/#Donating">donating to the KDE
Project</a>.

<h4>
  KDE Sponsorship
</h4>
<p align="justify">
  Besides the superb and invaluable efforts by the
  <a href="http://www.kde.org/people/">KDE developers</a>
  themselves, significant support for KDE development has been provided
  by
  <a href="http://www.mandrakesoft.com/">MandrakeSoft</a>,
  <a href="http://www.trolltech.com/">Trolltech</a> and
  <a href="http://www.suse.com/">SuSE</a>.
  <a href="http://www.ibm.com/">IBM</a> has donated significant hardware
  to the KDE Project, and the
  <a href="http://www.uni-tuebingen.de/">University of T&uuml;bingen</a>
  and the <a href="http://www.uni-kl.de/">University of
  Kaiserslautern</a>
  provide most of the Internet bandwidth for the KDE project.  Thanks!
</p>

<h4>
  About KDE
</h4>
<p align="justify">
  KDE is an independent project of hundreds of developers, translators,
  artists and other professionals worldwide collaborating over the
  Internet
  to create and freely distribute a sophisticated, customizable and
  stable
  desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding
  development
  platform.  KDE provides a stable, mature desktop, a full,
  component-based
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking and administration tools and utilities, and an
  efficient, intuitive development environment featuring the excellent
  IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.  KDE is working proof
  that the Open Source "Bazaar-style" software development model can
  yield
  first-rate technologies on par with and superior to even the most
  complex
  commercial software.
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<p align="justify">
  <font size="2">
  <em>Trademark Notices.</em>
  KDE and K Desktop Environment are trademarks of KDE e.V.

  Linux is a registered trademark of Linus Torvalds.

  UNIX is a registered trademark of The Open Group in the United States and
  other countries.

  All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.
  </font>
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<h4>Press Contacts</h4>
<table cellpadding="10"><tr valign="top">
<td>

<b>Africa</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="mail&#x74;o&#058;in&#102;o-a&#00102;rica&#x40;&#x6b;d&#101;&#46;or&#00103;">&#105;&#110;&#0102;&#x6f;-afr&#105;&#x63;a&#x40;k&#x64;e&#46;&#x6f;r&#x67;</a><br />
</td>

<td>
<b>Asia</b><br />
Sirtaj S. Kang <br />
C-324 Defence Colony <br />
New Delhi <br />
India 110024 <br />
Phone: +91-981807-8372 <br />
<a href="&#0109;a&#105;l&#00116;&#111;&#x3a;i&#110;fo-&#x61;sia&#064;&#x6b;de.o&#114;&#103;">in&#102;o&#x2d;&#97;s&#x69;&#97;&#x40;&#0107;&#100;&#101;&#x2e;&#x6f;rg</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europe</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="&#0109;a&#x69;l&#116;&#x6f;:&#105;n&#x66;o-&#101;u&#x72;ope&#064;k&#100;e&#x2e;&#111;&#0114;&#103;">&#0105;nf&#00111;-eu&#x72;&#00111;&#0112;e&#x40;&#x6b;&#00100;&#00101;.org</a>
</td>

<td>
<b>North America</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Phone: (416)-925-4030 <br />
<a href="m&#x61;&#x69;&#x6c;to:&#x69;nfo-n&#x6f;&#114;&#116;&#x68;&#x61;me&#0114;i&#x63;&#97;&#x40;kde.&#x6f;&#114;g">i&#x6e;&#102;&#111;&#x2d;&#00110;o&#114;t&#104;am&#101;ri&#99;&#97;&#x40;&#x6b;&#100;&#x65;&#46;or&#x67;</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="mai&#x6c;to:&#x69;nfo-&#x6f;&#0099;&#101;&#097;&#110;ia&#0064;kd&#x65;.o&#x72;&#x67;">in&#x66;&#x6f;-oce&#97;&#110;&#105;&#x61;&#x40;k&#100;&#x65;.o&#114;g</a><br />
</td>

<td>
<b>South America</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="&#00109;&#97;i&#108;to:in&#102;&#111;-sou&#x74;&#0104;&#00097;&#109;&#0101;&#00114;&#x69;&#x63;&#0097;&#00064;k&#100;e&#x2e;&#111;r&#x67;">i&#110;&#0102;o&#00045;so&#x75;tha&#x6d;eric&#x61;&#64;&#0107;&#100;e&#x2e;o&#0114;&#103;</a><br />
</td>


</tr></table>

<?php
  include("footer.inc");
?>

