<?php

    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "KDE Plasma 5.16: Now Smoother and More Fun",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = 'plasma-5.16.0'; // for i18n
    $version = "5.16.0";
?>

<script src="/js/use-ekko-lightbox.js" defer="true"></script>

<main class="releaseAnnouncment container" itemscope itemtype="https://schema.org/SoftwareApplication">

    <meta itemprop="name" content="Plasma 5.16" />
    <meta itemprop="url" content="Pla" />
    <meta itemprop="applicationCategory" content="Desktop Environment" />
    <meta itemprop="operatingSystem" content="GNU/Linux, FreeBSD" />

    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("Plasma %1", $version)?></h1>

    <?php include "./announce-i18n-bar.inc"; ?>

    <figure class="videoBlock">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/T-29hJUxoFQ?rel=0" allowfullscreen='true'></iframe>
    </figure>

    <figure class="topImage" itemprop="screenshot" itemscope itemtype="http://schema.org/ImageObject">
        <a href="plasma-5.16/plasma_5.16.png" data-toggle="lightbox">
            <img src="plasma-5.16/plasma_5.16-wee.png" height="338" width="600" style="width: 100%; max-width: 600px; height: auto;" alt="Plasma 5.16" itemprop="contentUrl"/>
        </a>
        <figcaption itemprop="description"><?php print i18n_var("KDE Plasma %1", "5.16")?></figcaption>
    </figure>

    <p><?php i18n("Tuesday, 11 June 2019.")?></p>
    <p><?php i18n("Today KDE launches the latest version of its desktop environment, Plasma 5.16.")?></p>

    <p><?php i18n("For this release, KDE developers have worked hard to polish Plasma to a high gloss. The results of their efforts provide a more consistent experience and bring new features to all Plasma users."); ?></p>

    <p><?php i18n("One of the most obvious changes is the completely rewritten notification system that comes with a <i>Do Not Disturb</i> mode, a more intelligent history which groups notifications together, and critical notifications in fullscreen apps. Besides many other things, it features better notifications for file transfer jobs, and a much more usable <i>System Settings</i> page to configure all notification-related things. "); ?></p>

    <p><?php i18n("The system and widget settings have been refined and improved by porting code to newer Kirigami and Qt technologies, while at the same time polishing the user interface. The Visual Design Group and the Plasma team continue their efforts towards the usability and productivity goal, getting feedback and removing all the papercuts in our software so that you find Plasma smoother, as well as more intuitive and consistent to use."); ?></p>

    <p><?php i18n("For the first time, the default wallpaper of Plasma 5.16 has been decided by a contest where everyone could participate and submit their original art. The winning wallpaper - the work of a talented Argentinian artist - is as sleek and cool as Plasma itself, and will keep your desktop looking fresh during the summer."); ?></p>

    <p><?php i18n("We hope you enjoy using Plasma 5.16 as much as we did making it."); ?></p>

    <h2 id="desktop"><?php i18n("Desktop Management");?></h3>

    <ul>
        <li><?php i18n("Plasma 5.16 comes with a completely rewritten notification system. You can mute notifications with the <i>Do Not Disturb</i> mode, and the list of past notifications now displays them grouped by app. Critical notifications show up even when apps are in fullscreen mode and we have also improved notifications for file transfer jobs. Another thing users will appreciate is that the <i>System Settings</i> page for notifications is much clearer and more usable."); ?>
            <br />
            <figure class="row justify-content-center align-items-center mt-3">
                <a href="plasma-5.16/notifications_2.png" class="col-12" data-toggle="lightbox">
                    <img src="plasma-5.16/notifications_2-wee.png" alt="<?php i18n("New notifications"); ?>" class="img-fluid d-block mx-auto">
                </a>
                <figcaption><?php i18n("New Notifications"); ?></figcaption>
            </figure>
        </li>

        <li><?php i18n("Plasma's themes system has also been greatly improved. For one, when you select a new theme, it now gets correctly applied to panels. We have some great news for theme designers: you now have more control over customizing widgets. You can tweak the look of the analog clock, for example, by adjusting the offset of its hands and toggling the blur behind them."); ?>
            <br />
            <figure class="row justify-content-center align-items-center mt-3">
                <a href="plasma-5.16/plasma-theme-fixes.png" class="col-12" data-toggle="lightbox">
                    <img src="plasma-5.16/plasma-theme-fixes.png" alt="<?php i18n("Theme Engine Fixes for Clock Hands!"); ?>" class="img-fluid d-block mx-auto">
                </a>
                <figcaption><?php i18n("Theme Engine Fixes for Clock Hands!"); ?></figcaption>
            </figure>
        </li>

        <li><?php i18n("We have modernized all widget configuration settings and improved the panels to make them clearer and easier to use. The <i>Color Picker</i> widget has also been improved, and lets you drag colors from the plasmoid to text editors, or the palette of a photo editor. The <i>Show Desktop</i> icon is now also present in the panel by default."); ?>
        </li>

        <li><?php i18n("Plasma 5.16 protects your privacy, too. When any app is recording audio, a microphone icon will appear in the <i>System Tray</i> warning you of the fact. You can then raise or lower the volume using the wheel on your mouse, or mute and unmute the mic with a middle click."); ?></li>

        <li><?php i18n("The look and feel of the lock, login and logout screens has been improved with new icons, labels, hover behavior, and by adjusting the login button layout."); ?>
            <br />
            <div class="row">
                <figure class="col-6 justify-content-center align-items-center mt-3">
                    <a href="plasma-5.16/lock_screen.png" class="col-12" data-toggle="lightbox">
                        <img src="plasma-5.16/lock_screen-wee.png" alt="<?php i18n("Login Screen Theme Improved"); ?>" class="img-fluid d-block mx-auto">
                    </a>
                    <figcaption><?php i18n("Login Screen Theme Improved"); ?></figcaption>
                </figure>
                <figure class="col-6 justify-content-center align-items-center mt-3">
                    <a href="plasma-5.16/sddm-theme-1.png" class="col-12" data-toggle="lightbox">
                        <img src="plasma-5.16/sddm-theme-1-wee.png" alt="<?php i18n("SDDM Theme Improved"); ?>" class="img-fluid d-block mx-auto">
                    </a>
                    <figcaption><?php i18n("SDDM Theme Improved"); ?></figcaption>
                </figure>
            </div>
        </li>

        <li><?php i18n("The settings window of the <i>Wallpaper Slideshow</i> displays the images in the folders you selected, and lets you select only the ones you want to display in the slideshow."); ?></li>
            <br />
            <figure class="row justify-content-center align-items-center mt-3">
                <a href="plasma-5.16/wallpaper.png" class="col-12" data-toggle="lightbox">
                    <img src="plasma-5.16/wallpaper-wee.png" alt="<?php i18n("Wallpaper Slideshow settings window"); ?>" class="img-fluid d-block mx-auto">
                </a>
                <figcaption><?php i18n("Wallpaper Slideshow settings window"); ?></figcaption>
            </figure>

        <li><?php i18n("The <i>Task Manager</i> has better organized context menus, and you can configure it to move a window from a different virtual desktop to the current one with a middle click."); ?></li>

        <li><?php i18n("The default <i>Breeze</i> window and menu shadow colors are back to pure black. This improves the visibility of many things, especially when using a dark color scheme."); ?></li>

        <li><?php i18n("The <i>Show Alternatives</i> button is visible in panel edit mode, and you can use it to quickly swap one widget for another with similar functionalities. For example, you can use it to replace the analog clock with the digital or binary clock."); ?>
            <br />
            <figure class="row justify-content-center align-items-center mt-3">
                <a href="plasma-5.16/alternatives_menu-1.png" class="col-12" data-toggle="lightbox">
                    <img src="plasma-5.16/alternatives_menu-1-wee.png" alt="<?php i18n("Panel Edition Offers Alternatives"); ?>" class="img-fluid d-block mx-auto">
                </a>
                <figcaption><?php i18n("Panel Editing Offers Alternatives"); ?></figcaption>
            </figure>
        </li>

        <li><?php i18n("In Plasma 5.16, you can lock and unlock Plasma Vaults directly from Dolphin."); ?></li>
    </ul>

    <h2 id="settings">Settings</h2>

    <ul>
        <li><?php i18n("We have polished all pages and refined the entire <i>Appearance</i> section. The <i>Look and Feel</i> page has moved to the top level, and we added improved icons to many of the pages."); ?>
            <br />
            <figure class="row justify-content-center align-items-center mt-3">
                <a href="plasma-5.16/application-style.png" class="col-12" data-toggle="lightbox">
                    <img src="plasma-5.16/application-style-wee.png" alt="<?php i18n("Application Style and Appearance Settings"); ?>" class="img-fluid d-block mx-auto">
                </a>
                <figcaption><?php i18n("Application Style and Appearance Settings"); ?></figcaption>
            </figure>
        </li>

        <li><?php i18n("The <i>Color Scheme</i> and <i>Window Decorations</i> pages have been redesigned with a more consistent grid view, and the <i>Color Scheme</i> page now supports filtering by light and dark themes. It also lets you drag-and-drop to install themes, and undo if you accidentally delete a theme. To apply a theme, you just have to double-click on it."); ?>
            <br />
            <figure class="row justify-content-center align-items-center mt-3">
                <a href="plasma-5.16/colors_kcm-2.png" class="col-12" data-toggle="lightbox">
                    <img src="plasma-5.16/colors_kcm-2-wee.png" alt="<?php i18n("Color Scheme"); ?>" class="img-fluid d-block mx-auto">
                </a>
                <figcaption><?php i18n("Color Scheme"); ?></figcaption>
            </figure>
        <li>
        <li><?php i18n("The theme preview of the <i>Login Screen</i> page has been overhauled, and the <i>Desktop Session</i> page now features a <i>Reboot to UEFI Setup</i> option."); ?></li>

        <li><?php i18n("Plasma 5.16 supports configuring touchpads using the Libinput driver on X11."); ?></li>
    </ul>

    <h2 id="window">Window Management</h2>

    <ul>
        <li><?php i18n("Initial support for using Wayland with proprietary Nvidia drivers has been added. When using Qt 5.13 with this driver, graphics are also no longer distorted after waking the computer from sleep."); ?></li>

        <li><?php i18n("Wayland now features drag-and-drop between XWayland and Wayland native windows. The <i>System Settings</i> libinput touchpad page lets you configure the click method, switching between <i>areas</i> or <i>clickfinger</i>."); ?></li>

        <li><?php i18n("KWin's blur effect looks more natural and correct as it doesn't unnecessarily darken the area between blurred colors anymore. GTK windows now apply correct active and inactive color schemes."); ?>
            <br />
            <figure class="row justify-content-center align-items-center mt-3">
                <a href="plasma-5.16/blur_konsole.png" class="col-12" data-toggle="lightbox">
                    <img src="plasma-5.16/blur_konsole-wee.png" alt="<?php i18n("Lock screen in Plasma 5.16"); ?>" class="img-fluid d-block mx-auto">
                </a>
                <figcaption><?php i18n("Window Management"); ?></figcaption>
            </figure>
        </li>

        <li><?php i18n("We have added two new default shortcuts: <kbd>Meta</kbd> + <kbd>L</kbd> lets you lock the screen and <kbd>Meta</kbd> + <kbd>D</kbd> offers a quick way to show and hide the desktop."); ?></li>
    </ul>

    <h2 id="network-manager">Plasma Network Manager</h2>

    <ul>
        <li><?php i18n("The <i>Networks</i> widget is now faster to refresh Wi-Fi networks and more reliable in doing so. It also has a button to display a search field that helps you find a particular network in the list of available choices. Right-clicking on any network will show a <i>Configure…</i> action."); ?></li>

        <li><?php i18n("WireGuard, the simple and secure VPN, is compatible with NetworkManager 1.16. We have also added One Time Password (OTP) support in the Openconnect VPN plugin."); ?></li>
    </ul>

    <h2 id="discover">Discover</h2>

    <ul>
        <li><?php i18n("Discover is Plasma's software manager, and the latest version available in Plasma 5.16 comes with improvements that make it easier to use. In the new <i>Update</i> page, for example, apps and packages now come with distinct <i>downloading</i> and <i>installing</i> sections. When an item has finished installing, it disappears from the view. You can also force-quit while an installation or an update operation is in progress."); ?>
            <br />
            <figure class="row justify-content-center align-items-center mt-3">
                <a href="plasma-5.16/discover-update.png" class="justify-content-center align-items-center col-12" data-toggle="lightbox">
                    <img src="plasma-5.16/discover-update-wee.png" alt="<?php i18n("Updates in Discover"); ?>" class="img-fluid d-block mx-auto">
                </a>
                <figcaption><?php i18n("Updates in Discover"); ?></figcaption>
            </figure>
        </li>

        <li><?php i18n("The task completion indicator now looks better, featuring a real progress bar. Discover also displays a busy indicator when checking for updates, and has improved support and reliability for AppImages and other apps that come from <a href='https://store.kde.org'>store.kde.org</a>."); ?></li>

        <li><?php i18n(" The <i>Sources</i> menu now shows the version number for each different source of an app."); ?></li>
    </ul>


    <br clear="all" />

    <a href="plasma-5.15.5-5.16.0-changelog.php"><?php print i18n_var("Full Plasma %1 changelog", $version); ?></a>

    <!-- // Boilerplate again -->
    <section class="row get-it">
        <article class="col-md">
            <h2><?php i18n("Live Images");?></h2>
            <p>
                <?php i18n("The easiest way to try it out is with a live image booted off a USB disk. Docker images also provide a quick and easy way to test Plasma.");?>
            </p>
            <a href='https://community.kde.org/Plasma/Live_Images' class="learn-more"><?php i18n("Download live images with Plasma 5");?></a>
            <a href='https://community.kde.org/Plasma/Docker_Images' class="learn-more"><?php i18n("Download Docker images with Plasma 5");?></a>
        </article>

        <article class="col-md">
            <h2><?php i18n("Package Downloads");?></h2>
            <p>
                <?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
            </p>
            <a href='https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro' class="learn-more"><?php i18n("Get KDE Software on Your Linux Distro wiki page");?></a>
        </article>

        <article class="col-md">
            <h2><?php i18n("Source Downloads");?></h2>
            <p>
                <?php i18n("You can install Plasma 5 directly from source.");?>
            </p>
            <a href='https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source' class='learn-more'><?php i18n("Community instructions to compile it");?></a>
            <a href='../info/plasma-5.16.0.php' class='learn-more'><?php i18n("Source Info Page");?></a>
        </article>
    </section>

    <section class="give-feedback">
        <h2><?php i18n("Feedback");?></h2>

        <p class="kSocialLinks">
            <?php i18n("You can give us feedback and get updates on our social media channels:"); ?>
            <?php include($site_root . "/contact/social_link.inc"); ?>
        </p>
        <p>
            <?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?>
        </p>

        <p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>Plasma Matrix chat room</a>, <a href='%2'>Plasma-devel mailing list</a> or report issues via <a href='%3'>bugzilla</a>. If you like what the team is doing, please let them know!", "https://webchat.kde.org/#/room/#kde-devel:kde.org", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided"); ?>

        <p><?php i18n("Your feedback is greatly appreciated.");?></p>
    </section>

    <h2>
        <?php i18n("Supporting KDE");?>
    </h2>

    <p align="justify">
        <?php print i18n_var("KDE is a <a href='%1'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='%2'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='%3'>Join the Game</a> initiative.", "http://www.gnu.org/philosophy/free-sw.html", "/community/donations/", "https://relate.kde.org/civicrm/contribute/transact?id=5"); ?>
    </p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

</main>
<?php
  require('../aether/footer.php');
