<?php
  $page_title = "KDE SC 4.4.0 Caikaku väljalasketeade";
  $site_root = "../";
  include "header.inc";
?>

<p>Teistes keeltes:
<?php
  $release = '4.4';
  include "../announce-i18n-bar.inc";
?>
</p>

<!-- // Boilerplate -->

<h3 align="center">
  KDE tarkvarakogu 4.4.0 pakub uuendustena väikesülearvuti liidest, akende kasutamist kaartidena ja autentimise raamistikku
</h3>

<p align="justify">
  <strong>
    KDE tarkvarakogu 4.4.0 (koodnimi: <i>"Caikaku"</i>) on ilmunud
  </strong>
</p>

<p align="justify">
9. veebruar 2010. <a href="http://www.kde.org/">KDE</a> annab teada KDE tarkvarakogu 4.4, koodnimega <i>"Caikaku"</i>, kohesest saadavusest. See pakub vaba tarkvara kasutajatele rohkelt uuenduslikke rakendusi. Uudisasjadena pakutakse mitmeid olulisi tehnoloogiaid, sealhulgas sotsiaalvõrkude ja internetikoostöö võimalusi, spetsiaalselt väikesülearvutitele kohandatud uut kasutajaliidest ning mõningaid uuendusi infrastruktuuri vallas, näiteks autentimise raamistikku KAuth. KDE vigade jälgimise süsteemi kohaselt on parandatud 7293 viga ning täidetud 1433 uute võimaluste sooviavaldust. KDE kogukond soovib tänada kõiki, kelle panusega on see võimalikuks osutunud.
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="images/general-desktop.png"><img src="screenshots/thumbs/general-desktop_thumb.png" align="center" width="540" height="338" alt="KDE Plasma töökeskkond" title="KDE Plasma töökeskkond" /></a>
<br />
<em>KDE Plasma töökeskkond</em></div>

<p align="justify">
 Uuendusi ja muudatusi, mida pakub 4.4, kirjeldab üksikasjalikumalt <a href="http://www.kde.org/announcements/4.4/guide.php">KDE tarkvarakogu 4.4 visuaalne tutvustus</a>, kuid lühema ülevaate saab ka allpool.
</p>

<h3>
  Plasma töökeskkond pakub sotsiaalse suhtlemise ja võrgukoostöö võimalusi
</h3>
<br />
<p align="justify">
 KDE Plasma töökeskkond võimaldab põhimõtteliselt kõike, mida kasutaja vajab rakenduste, failide ja globaalsete seadistuste käivitamiseks ja haldamiseks. KDE töökeskkonna arendajad pakuvad nüüd uut väikesülearvutite liidest, viimistletumat kunstilist kujundust ning senisest etemaid töö korraldamise võimalusi. Sotsiaalvõrkude ja internetiteenuste võimaluste juurutamise taga seisab KDE kogukonna kui vaba tarkvara meeskonna koostööle suunatud loomus.
</p>

<!-- Plasma Screencast -->
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<p>
<embed src="http://blip.tv/play/hZElgcKJVgA" type="application/x-shockwave-flash" width="500" height="346" allowscriptaccess="always" allowfullscreen="true"></embed>
<br />
<em>Plasma töölaua täiustatud interaktiivsus (<a href="http://blip.tv/file/get/Sebasje-KDEPlasmaIn44540.ogg">laadi alla</a>)</em>
</p>
</div>


<p align="justify">
<ul>
  <li>
    <strong>Plasma Netbook</strong> on versioonis 4.4.0 uustulnuk. See on Plasma töölaua alternatiivne liides, mille loomisel on spetsiaalselt silmas peetud väikesülearvutite ja muude pisemate sülearvutite ergonoomilisi vajadusi. Tegelikult on kogu Plasma raamistik algusest peale loodud nii, et seda oleks võimalik kohandada ka muudele seadmetele kui lauaarvutitele. Plasma Netbookil on väga suur ühisosa tavalise Plasma töölauaga, kuid selle kujundamisel on eriliselt silmas peetud vähese ruumi vajadusi ning arvestatud ka puuteekraani võimalustega.
    Plasma Netbooki omaduste hulka kuuluvad rakenduste käivitamise ja otsimise liides ning niinimetatud ajaleht, mis pakub rohkelt vidinaid veebi ja Plasma kasutajatele juba varem tuntud tavapäraste vidinate sisu kuvamiseks.
  </li>
  <li>
    <strong>Sotsiaalse töölaua</strong> algatusega on täiustatud kogukonna vidinat (varasem sotsiaalse töölaua vidin), mis võimaldab kasutajatel otse vidinast saata sõnumeid ning leida sõpru. Uus sotsiaalsete uudiste vidin näitab reaalajas, mis toimub kasutaja sotsiaalvõrgus, samuti uus teadmusbaasi vidin võimaldab aga kasutajatel otsida nii vastuseid kui ka küsimusi mitmesuguste pakkujate, sealhulgas loomulikult ka openDesktop.org-i teadmusbaasist.
  </li>
  <li>
    KWini uus kaardiliidese võimalus laseb kasutajal <strong>rühmitada aknaid</strong> veebibrauseri moodi kaartidena, mis muudab suurema hulga rakenduste käsitlemise märksa hõlpsamaks ja tõhusamaks. Lisaks sellele on akende kasutamist parandatud mitmesuguste omadustega, näiteks nakkamisega mõnele ekraani servale ning akende maksimeerimisega nende lohistamisel. KWini meeskond on ühtlasi koostöös Plasma arendajatega muutnud sujuvamaks töökeskkonna rakenduste omavahelise koostöö, mida näitavamad elegantsemad animatsioonid ja paranenud jõudlus. Senisest märksa seadistatavam akende dekoratsioonide kohandamine, mis lubab kasutada ka skaleeritavat graafikat, annab kunstikalduvustega inimestele suuremad võimalused töötada välja omaenda teemasid ning neid teistega jagada.
  </li>
</ul>
</p>

<!-- Window grouping Screencast -->
<div align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<p>
<embed src="http://blip.tv/play/hZElgcKPKwA" type="application/x-shockwave-flash" width="500" height="346" allowscriptaccess="always" allowfullscreen="true"></embed>
<br />
<em>Akende hõlpsam kasutamine  (<a href="http://blip.tv/file/get/Sebasje-WindowManagingFeaturesInKDEPlasma44222.ogg">laadi alla</a>)</em>
</p>
</div>



<h3>
  KDE uuenduslikud rakendused
</h3>
<p align="justify">
KDE kogukond pakub suurel hulgal võimsaid, aga ometi väga lihtsalt kasutatavaid rakendusi. Käesolevas väljalaskes on paljusid rakendusi märgatavalt parandatud ning lisandunud on ka uusi rakendusi.
</p>
<p align="justify">
<ul>
  <li>
    Käesolevas KDE tarkvarakogu väljalaskes on pikaajalise kujundamise ja planeerimise tulemusel tunduvalt täiustatud <strong>uue kuuma kraami hankimise liidest</strong>. Selle sihiks on võimaldada KDE väliste kaasautorite tohutul kogukonnal <strong>hõlpsamini kontakteeruda</strong> miljonite nende toodetud sisu kasutajatega. Kasutajad saavad alla laadida mitmesuguseid andmeid, näiteks <a href="http://dimsuz.wordpress.com/2009/11/11/katomic-new-feature-level-sets-support/">KAtomicu uusi tasemeid</a>, uusi tähistaeva elemente või uusi omadusi tagavaid skripte otse vastavast rakendusest. Uueks nähtuseks on <strong>sotsiaalsed võimalused</strong>, näiteks sisu kommenteerimine ja hindamine ning meeldiva toote fänniks hakkamine, mille puhul kasutajale näidatakse vastava toote uuendusi sotsiaalsete uudiste vidinas. Samuti võivad kasutajad <strong>omaenda loomingut veebi üles laadida</strong> üsna mitme rakenduse seest, mis kõrvaldab tülika pakendamise ja käsitsi üleslaadimise vajaduse.
  </li>
  <li>
    Kaks KDE kogukonna ammust projekti on selles väljalaskes saavutanud korraliku küpsuse. Rahvusvaheliste pingutustega arendatav ning EL-i rahastatav Nepomuk on nüüd piisavalt stabiilne ja mõistliku jõudlusega. <strong><a href="http://ppenz.blogspot.com/2009/11/searching.html">Dolphini lõimimine töölauaotsinguga</a></strong> võimaldab kasutajatel Nepomuki semantilise raamistiku abil edukamalt oma andmete asupaika tuvastada ning neid korraldada. KDE PIM-i meeskond on samal ajal pannud esimesed rakendused kasutama uut andmete salvestamise ja hankimise süsteemi <strong>Akonadi</strong>. KDE aadressiraamatu rakendus on põhjalikult ümber kirjutatud ning see tarvitab nüüd kergesti hoomatavat kolme paneeliga liidest. Rakenduste laiem, põhjalikum ja täielikum üleminek mainitud uutele tehnoloogiatele seisab ees KDE tarkvarakogu järgmistes väljalasetes.
  </li>
  <li>
    Lisaks lõimimisele eespool nimetatud oluliste tehnoloogiatega on mitmed arendajate meeskonnad mitmel moel parandanud ja täiustanud oma rakendusi. KGeti arendajad lisasid digitaalallkirja kontrollimise ning failide mitmest kohast allalaadimise võimaluse, Gwenview aga pakub nüüd lihtsat fotode importimise tööriista. Nende kõrval näevad käesolevas väljalaskes päevavalgust mõned uued või täielikult ümber kujundatud rakendused. Puslemäng Palapeli annab kasutajale võimaluse panna puslesid kokku otse oma arvutis ning soovi korral saab puslesid ise luua ja teistega jagaga. Cantor kujutab endast lihtsat ja hõlpsasti mõistetavat liidest muidu keerukale, aga väga avarate võimalustega statistika- ja teadusarvutustarkvarale (<a href="http://r-project.org/">R</a>, <a href="http://sagemath.org/">SAGE</a> ja <a href="http://maxima.sourceforge.net/">Maxima</a>). KDE PIM-i komplekt pakub uuena välja ajaveebi sissekannete loomise ja haldamise rakenduse Blogilo.
  </li>
</ul>
</p>
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="screenshots/dolphin-search.png"><img src="screenshots/thumbs/dolphin-search_thumb.png" align="center" width="540" height="338"  alt="Töölauaotsingu lõimimine KDE failihalduriga Dolphin" title="Töölauaotsingu lõimimine KDE failihalduriga Dolphin" /></a><br />
<em>Töölauaotsingu lõimimine KDE failihalduriga Dolphin</em></div>

<h3>
  Platvorm kiirendab arendamist
</h3>
<p align="justify">
KDE kogukonna tõsine keskendumine tehnoloogia täiustamisele on toonud kaasa kõikehõlmavama, kooskõlalise ja tõhusa arendusplatvormi tekkimise, millele vaevu leidub konkurenti. 4.4 väljalaskega kaasneb KDE teekides hulk uusi koostöö ja sotsiaalvõrkudega arvestavaid tehnoloogiaid. Me pakume seni levinud tehnoloogiate võimsaid ja paindlikke avatud alternatiive. Selmet muuta kasutajad meie toodete pimedateks kasutajateks, püüame me tähelepanu koondada hoopis uuenduslikkusele nii interneti- kui ka töölauaruumis.
</p>
<p align="justify">
<ul>
  <li>
    KDE tarkvara taga seisev infrastruktuur on üle elanud mitmeid märkimisväärseid uuendusi. Tähtsaim neist on vahest see, et <strong>Qt 4.6</strong> toetab nüüd Symbiani platvormi ning pakub uut animatsiooniraamistikku, mitmikpuute võimalusi ning üldiselt paremat jõudlust. Eelmises väljalaskes ilmavalgust näinud <strong>sotsiaalse töölaua tehnoloogiat</strong> on täiendatud keskse identiteedi haldamise võimalusega ning libattica kui läbipaistva veebiteenuste kasutamise teegi kasutusele võtmisega. <strong>Nepomuk</strong> kasutab nüüd märksa stabiilsemat taustaprogrammi, mis muudab ta suurepäraseks valikuks kõige jaoks, mida rakendused vajavad metaandmete haldamiseks, otsinguteks ja indekseerimiseks.
  </li>
  <li>
    Uue asjana on kasutusel autentimise raamistik KAuth. <strong>KAuth võimaldab turvalist autentimist</strong> ning annab arendajate käsutusse kasutajaliidese elemendid, kui nad soovivad panna rakendusi käivitama ülesandeid, mis vajavad administraatori või üldse mingeid muid, tavapärasest erinevaid õigusi. Linuxis kasutab KAuth taustaporgrammina PolicyKitti, mis annab võimaluse <strong>muretuks töölauast sõltumatuks lõimimiseks</strong>. KAuth on juba kasutusel mõnes Süsteemi seadistuste dialoogis ning lähemas tulevikus on kavas see lõimida KDE Plasma töölaua ja KDE rakendustega. KAuth toetab õiguste andmise ja tühistamise täppishäälestamist, paroolide puhverdamist ning mitmeid dünaamilise kasutajaliidese elemente, mis võimaldavad seda edukalt kasutada igasugustes rakendustes.
  </li>
  <li>
    Vaba töölaua grupitöö puhver <strong>Akonadi</strong> on samuti KDE rakendustesse lõimitud alates 4.4.0 väljalaskest. KDE SC 4.4 koosseisus ilmuv aadressiraamat on esimene KDE rakendus, mis kasutab täielikult ära uut Akonadi infrastruktuuri. KDE aadressiraamat toetab nii kohalikke aadressiraamatuid kui ka aadressiraamatuid mitmesugustes grupitöö serverites. KDE SC 4.4.0 tähistab KDE tarkvarakogus tõelist Akonadi-ajastu algust ning järgmistes väljalasetes saab seda ära kasutavaid rakendusi olema aina rohkem. Akonadi kujuneb kava kohaselt keskseks liideseks e-kirjadele, kontaktidele, kalendriandmetele ning muule personaalsele teabele. Akonadi kujutab selles mõttes endast e-kirjade, grupitöö serverite ja teiste internetiressursside läbipaistvat puhvrit.
  </li>
</ul>
</p>
<p>
Teadlikult litsenseeritud LGPL alusel (võimaldab nii avatud kui suletud lähtekoodiga arendust) ning platormist sõltumatu (Linux, UNIX, Mac ja MS Windows).
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="screenshots/social-web-widgets.png"><img src="screenshots/thumbs/social-web-widgets_thumb.png" align="center" width="540" height="338" alt="Veeb ja sotsiaalvõrgud Plasma töölaual" title="Veeb ja sotsiaalvõrgud Plasma töölaual"/></a><br />
<em>Veeb ja sotsiaalvõrgud Plasma töölaual</em></div>

<h4>
Muud muudatused
</h4>
<p align="justify">
Nagu juba mainitud, kujutab eelöeldu pelgalt valikut kõigist muudatustest ja täiustustest, mida KDE töökeskkond, KDE rakendustekogu ja rakenduste arendamise raamistik on üle elanud. Põhjalikuma, kuid ikkagi veel ebatäieliku nimekirja leiab <a href="http://techbase.kde.org/Schedules/KDE4/4.4_Feature_Plan">KDE SC 4.4 omaduste kavast</a> <a href="http://techbase.kde.org">TechBase'is'</a>. Teavet rakenduste kohta, mida KDE kogukond arendab väljaspool KDE rakendustekogu, leiab <a href="http://kde-apps.org">kde-apps'i veebileheküljelt</a>.
</p>


<h4>
    Teabe levitamine ja jälgimine
</h4>
<p align="justify">
KDE kogukond õhutab kõiki <strong>levitama teavet</strong> sotsiaalvõrkudes.
Kirjutage lugusid uudistesaitidele või kasutage selliseid kanaleid, nagu delicious, digg, reddit, twitter,
identi.ca. Laadige ekraanipilte üles sellistele teenustele, nagu Facebook, FlickR,
ipernity ja Picasa, ning postitage neid sobivatesse gruppidesse. Looge ekraanivideoid ja laadige
need üles YouTube'i, Blip.tv-sse, Vimeosse ja mujale. Ärge unustage lisamast üleslaaditud
materjalile <em>silti <strong>kde</strong></em>, et kõik saaksid seda hõlpsamini leida
ja et KDE meeskond saaks koostada ülevaateid, kuidas on KDE SC 4.4 väljalasketeadet
kajastatud ja levitatud. <strong>Aidake meil teavet levitada ja levitage seda ise!</strong></p>

<p align="justify">
Seda, mis toimub sotsiaalvõrkudes seoses KDE SC 4.4 väljalaskega, saab vahetult jälgida
uhiuues <a href="http://buzz.kde.org"><strong>KDE kogukonna live-kanalis</strong></a>. See koondab kõike, mis
toimub identi.ca-s, twitteris, youtube'is, flickris, picasawebis, ajaveebides ja paljudes teistes
sotsiaalvõrgustikes, sealjuures reaalajas. Live-kanali leiab aadressilt <strong><a href="http://buzz.kde.org">buzz.kde.org</a></strong>.
</p>

<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/linux_unix/KDE_Software_Compilation_4_4_0_Introduces_new_innovations"><img src="buttons/digg.gif" alt="Digg" title="Digg" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/r/technology/comments/azx29/kde_software_compilation_440_introduces_netbook/"><img src="buttons/reddit.gif" alt="Reddit" title="Reddit" /></a>
    </td>
    <td>
        <a href="http://twitter.com/#search?q=kde44"><img src="buttons/twitter.gif" alt="Twitter" title="Twitter" /></a>
    </td>
    <td>
        <a href="http://identi.ca/search/notice?q=kde44"><img src="buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde44"><img src="buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde44"><img src="buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde44"><img src="buttons/facebook.gif" alt="Facebook" title="Facebook" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde44"><img src="buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
    </td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>


<h4>
  KDE SC 4.4.0 paigaldamine
</h4>
<p align="justify">
KDE, kaasa arvatud kõik teegid ja rakendused, on saadaval vabalt ja tasuta vastavalt avatud tarkvara litsentsidele. KDE tarkvara töötab mitmesuguse riistvara, operatsioonisüsteemide ning aknahaldurite või töökeskkondadega. Lisaks Linuxi ja UNIX-i põhistele operatsioonisüsteemidele leiab suure osa KDE rakenduste Microsoft Windowsi versioonid leheküljel <a href="http://windows.kde.org">KDE Windowsis</a> ning Apple Mac OS X versioonid leheküljel <a href="http://mac.kde.org/">KDE Macil</a>. Veebist võib leida ka eksperimentaalseid versioone, milles KDE rakendused töötavad mitmel mobiilsel platvormil, näiteks MS Windows Mobile ja Symbian, kuid need on praegu veel ametliku toetuseta.
<br />
KDE saab hankida lähtekoodina ja mitmes binaarvormingus leheküljelt <a
href="http://download.kde.org/stable/4.4.0/">http://download.kde.org</a>. Samuti
saab selle hankida <a href="http://www.kde.org/download/cdrom.php">CD-l</a>
või leida pea kõigis <a href="http://www.kde.org/download/distributions.php">tähtsamates
GNU/Linuxi ja UNIX-i süsteemides</a>.
</p>
<p align="justify">
  <em>Pakendajad</em>.
  Osa Linuxi/UNIX-i OS-ide valmistajaid on lahkelt välja pakkunud mõnele oma distributsiooni
versioonile loodud KDE SC 4.4.0 binaarpaketid, mõne puhul on seda teinud vastava
distributsiooni kogukonna vabatahtlikud. <br />
  Mõningaid niisuguseid binaarpakette saab vabalt alla laadida KDE leheküljelt <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.4.0/">http://download.kde.org</a>.
  Teised binaarpaketid ning praegu saadaolevate pakettide uuendused lisanduvad
lähinädalatel.
</p>

<p align="justify">
  <a name="package_locations"><em>Pakettide asukohad</em></a>.
  Praegu saadaolevate binaarpakettide nimekirja, millest KDE projekt on teadlik,
leiab <a href="/info/4.4.0.php">KDE SC 4.4.0 teabeleheküljelt</a>.
</p>

<h4>
  KDE SC 4.4.0 kompileerimine
</h4>
<p align="justify">
  <a name="source_code"></a>
  Kogu KDE SC 4.4.0 lähtekoodi võib vabalt ja tasuta <a
href="http://download.kde.org/stable/4.4.0/src/">alla laadida</a>.
Juhised KDE SC 4.4.0 kompileerimiseks ja paigaldamiseks leiab
  <a href="/info/4.4.0.php#binary">KDE SC 4.4.0 teabeleheküljelt</a>.
</p>
<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Kontaktid ajakirjanikele</h4>

<?php
  include($site_root . "/community/whatiskde/press_contacts.inc");
  include("footer.inc");
?>
