<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.20.0");
  $site_root = "../";
  $release = '5.20.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
March 13, 2016. KDE today announces the release
of KDE Frameworks 5.20.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Many new icons");?></li>
<li><?php i18n("Add virtualbox mimetype icons and some other missing mimetypes");?></li>
<li><?php i18n("Add synaptic and octopi icon support");?></li>
<li><?php i18n("Fix cut icon (bug 354061)");?></li>
<li><?php i18n("Fix name of audio-headphones.svg (+=d)");?></li>
<li><?php i18n("Rating icons with smaller margin (1px)");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Remove possible file-name in KDEPlatformFileDialog::setDirectory()");?></li>
<li><?php i18n("Don't filter by name if we have mime types");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Remove dependency on Qt5::Widgets");?></li>
<li><?php i18n("Remove dependency on KDBusAddons");?></li>
<li><?php i18n("Remove dependency on KI18n");?></li>
<li><?php i18n("Remove unused includes");?></li>
<li><?php i18n("Shell scripts output improved");?></li>
<li><?php i18n("Added the data model (ActivitiesModel) showing the activities to the library");?></li>
<li><?php i18n("Build only the library by default");?></li>
<li><?php i18n("Remove the service and workspace components from the build");?></li>
<li><?php i18n("Move the library into src/lib from src/lib/core");?></li>
<li><?php i18n("Fix CMake warning");?></li>
<li><?php i18n("Fix crash in activities context menu (bug 351485)");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Fix kded5 dead lock when a program using kauth exits");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("KConfigIniBackend: Fix expensive detach in lookup");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Fix Kdelibs4 config migration for Windows");?></li>
<li><?php i18n("Add API to get Frameworks runtime version info");?></li>
<li><?php i18n("KRandom: Don't use up 16K of /dev/urandom to seed rand() (bug 359485)");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Don't call null object pointer (bug 347962)");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("Make it possible to compile with -DQT_NO_CAST_FROM_ASCII");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Fix session management for KApplication based applications (bug 354724)");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Use unicode characters for callouts");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("KFileMetadata can now query and store information about the original email a saved file may have been an attachment of");?></li>
</ul>

<h3><?php i18n("KHTML");?></h3>

<ul>
<li><?php i18n("Fix cursor updating in view");?></li>
<li><?php i18n("Limit string memory use");?></li>
<li><?php i18n("KHTML java applet viewer: repair broken DBus call to kpasswdserver");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Use portable import macro for nl_msg_cat_cntr");?></li>
<li><?php i18n("Skip looking up . and .. to find the translations for an application");?></li>
<li><?php i18n("Restrict _nl_msg_cat_cntr use to GNU gettext implementations");?></li>
<li><?php i18n("Add KLocalizedString::languages()");?></li>
<li><?php i18n("Place Gettext calls only if catalog has been located");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Make sure variable is being initialized");?></li>
</ul>

<h3><?php i18n("KInit");?></h3>

<ul>
<li><?php i18n("kdeinit: Prefer loading libraries from RUNPATH");?></li>
<li><?php i18n("Implement \"Qt5 TODO: use QUrl::fromStringList\"");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Fix KIO app-slave connection breaking if appName contains a '/' (bug 357499)");?></li>
<li><?php i18n("Try multiple authentication methods in case of failures");?></li>
<li><?php i18n("help: fix mimeType() on get()");?></li>
<li><?php i18n("KOpenWithDialog: show mimetype name and comment in \"Remember\" checkbox text (bug 110146)");?></li>
<li><?php i18n("A series of changes to avoid a directory relist after a file rename in more cases (bug 359596)");?></li>
<li><?php i18n("http: rename m_iError to m_kioError");?></li>
<li><?php i18n("kio_http: read and discard body after a 404 with errorPage=false");?></li>
<li><?php i18n("kio_http: fix mimetype determination when URL ends with '/'");?></li>
<li><?php i18n("FavIconRequestJob: add accessor hostUrl() so that konqueror can find out what the job was for, in the slot");?></li>
<li><?php i18n("FavIconRequestJob: fix job hanging when aborting due to favicon too big");?></li>
<li><?php i18n("FavIconRequestJob: fix errorString(), it only had the URL");?></li>
<li><?php i18n("KIO::RenameDialog: restore preview support, add date and size labels (bug 356278)");?></li>
<li><?php i18n("KIO::RenameDialog: refactor duplicated code");?></li>
<li><?php i18n("Fix wrong path-to-QUrl conversions");?></li>
<li><?php i18n("Use kf5.kio in the category name to match other categories");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("KLinkItemSelectionModel: Add new default constructor");?></li>
<li><?php i18n("KLinkItemSelectionModel: Make the linked selection model settable");?></li>
<li><?php i18n("KLinkItemSelectionModel: Handle changes to the selectionModel model");?></li>
<li><?php i18n("KLinkItemSelectionModel: Don't store model locally");?></li>
<li><?php i18n("KSelectionProxyModel: Fix iteration bug");?></li>
<li><?php i18n("Reset KSelectionProxyModel state when needed");?></li>
<li><?php i18n("Add a property indicating whether the models form a connected chain");?></li>
<li><?php i18n("KModelIndexProxyMapper: Simplify logic of connected check");?></li>
</ul>

<h3><?php i18n("KJS");?></h3>

<ul>
<li><?php i18n("Limit string memory use");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Show a warning if there's an error in the Engine");?></li>
</ul>

<h3><?php i18n("Package Framework");?></h3>

<ul>
<li><?php i18n("Let KDocTools stay optional on KPackage");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("Fix deprecated API usage");?></li>
<li><?php i18n("Add actionType to the declarative plugin");?></li>
<li><?php i18n("Reverse the filtering logic in PersonsSortFilterProxyModel");?></li>
<li><?php i18n("Make the QML example slightly more usable");?></li>
<li><?php i18n("Add actionType to the PersonActionsModel");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Simplify code, reduce pointer dereferences, container-related improvements");?></li>
<li><?php i18n("Add kmimeassociations_dumper test program, inspired by bug 359850");?></li>
<li><?php i18n("Fix chromium/wine apps not loading on some distributions (bug 213972)");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Fix highlighting of all occurrences in ReadOnlyPart");?></li>
<li><?php i18n("Don't iterate over a QString as if it was a QStringList");?></li>
<li><?php i18n("Properly initialize static QMaps");?></li>
<li><?php i18n("Prefer toDisplayString(QUrl::PreferLocalFile)");?></li>
<li><?php i18n("Support surrogate character sending from input method");?></li>
<li><?php i18n("Do not crash on shutdown when text animation is still running");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("Make sure KDocTools is looked-up");?></li>
<li><?php i18n("Don't pass a negative number to dbus, it asserts in libdbus");?></li>
<li><?php i18n("Clean cmake files");?></li>
<li><?php i18n("KWallet::openWallet(Synchronous): don't time out after 25 seconds");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("support _NET_WM_BYPASS_COMPOSITOR (bug 349910)");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Use non-native Language name as fallback");?></li>
<li><?php i18n("Fix session management broken since KF5 / Qt5 (bug 354724)");?></li>
<li><?php i18n("Shortcut schemes: support globally installed schemes");?></li>
<li><?php i18n("Use Qt's qHash(QKeySequence) when building against Qt 5.6+");?></li>
<li><?php i18n("Shortcut schemes: fix bug where two KXMLGUIClients with the same name overwrite each other's scheme file");?></li>
<li><?php i18n("kxmlguiwindowtest: add shortcuts dialog, for testing the shortcut schemes editor");?></li>
<li><?php i18n("Shortcut schemes: improve usability by changing texts in GUI");?></li>
<li><?php i18n("Shortcut schemes: improve scheme list combo (automatic size, don't clear on unknown scheme)");?></li>
<li><?php i18n("Shortcut schemes: don't prepend the guiclient name to the filename");?></li>
<li><?php i18n("Shortcut schemes: create dir before trying to save a new shortcut scheme");?></li>
<li><?php i18n("Shortcut schemes: restore layout margin, it looks very cramped otherwise");?></li>
<li><?php i18n("Fix memory leak in KXmlGui startup hook");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("IconItem: Don't overwrite source when using QIcon::name()");?></li>
<li><?php i18n("ContainmentInterface: Fix use of QRect right() and bottom()");?></li>
<li><?php i18n("Remove effectively duplicate code path for handling QPixmaps");?></li>
<li><?php i18n("Add API docs for IconItem");?></li>
<li><?php i18n("Fix stylesheet (bug 359345)");?></li>
<li><?php i18n("Don't wipe window mask on every geometry change when compositing is active and no mask has been set");?></li>
<li><?php i18n("Applet: Don't crash on remove panel (bug 345723)");?></li>
<li><?php i18n("Theme: Discard pixmap cache when changing theme (bug 359924)");?></li>
<li><?php i18n("IconItemTest: Skip when grabToImage fails");?></li>
<li><?php i18n("IconItem: Fix changing color of svg icons loaded from icon theme");?></li>
<li><?php i18n("Fix svg iconPath resolving in IconItem");?></li>
<li><?php i18n("If path is passed, pick the tail (bug 359902)");?></li>
<li><?php i18n("Add properties configurationRequired and reason");?></li>
<li><?php i18n("Move contextualActionsAboutToShow to Applet");?></li>
<li><?php i18n("ScrollViewStyle: Do not use margins of the flickable item");?></li>
<li><?php i18n("DataContainer: Fix slot checks before connect/disconnect");?></li>
<li><?php i18n("ToolTip: Prevent multiple geometry changes while changing contents");?></li>
<li><?php i18n("SvgItem: Don't use Plasma::Theme from rendering thread");?></li>
<li><?php i18n("AppletQuickItem: Fix finding own attached layout (bug 358849)");?></li>
<li><?php i18n("Smaller expander for the taskbar");?></li>
<li><?php i18n("ToolTip: Stop show timer if hideTooltip is called (bug 358894)");?></li>
<li><?php i18n("Disable animation of icons in plasma tooltips");?></li>
<li><?php i18n("Drop animations from tooltips");?></li>
<li><?php i18n("Default theme follows color scheme");?></li>
<li><?php i18n("Fix IconItem not loading non-theme icons with name (bug 359388)");?></li>
<li><?php i18n("Prefer other containments than desktop in containmentAt()");?></li>
<li><?php i18n("WindowThumbnail: Discard glx pixmap in stopRedirecting() (bug 357895)");?></li>
<li><?php i18n("Remove the legacy applets filter");?></li>
<li><?php i18n("ToolButtonStyle: Don't rely on an outside ID");?></li>
<li><?php i18n("Don't assume we find a corona (bug 359026)");?></li>
<li><?php i18n("Calendar: Add proper back/forward buttons and a \"Today\" button (bugs 336124, 348362, 358536)");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Don't disable language detection just because a language is set");?></li>
<li><?php i18n("Disable automatic disabling of automatic spelling by default");?></li>
<li><?php i18n("Fix TextBreaks");?></li>
<li><?php i18n("Fix Hunspell dictionary search paths missing '/' (bug 359866)");?></li>
<li><?php i18n("Add &lt;app dir&gt;/../share/hunspell to dictionary search path");?></li>
</ul>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.20");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.3");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
