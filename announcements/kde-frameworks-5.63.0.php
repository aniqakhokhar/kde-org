<?php
    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.63.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.63.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
October 12, 2019. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.63.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Improve KFloppy icon (bug 412404)");?></li>
<li><?php i18n("Add format-text-underline-squiggle actions icons (bug 408283)");?></li>
<li><?php i18n("Add colorful preferences-desktop-filter icon (bug 406900)");?></li>
<li><?php i18n("Add app icon for the Kirogi Drone control app");?></li>
<li><?php i18n("Added scripts to create a webfont out of all breeze action icons");?></li>
<li><?php i18n("Add enablefont and disablefont icon for kfontinst KCM");?></li>
<li><?php i18n("Fix large system-reboot icons rotating in an inconsistent direction (bug 411671)");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("new module ECMSourceVersionControl");?></li>
<li><?php i18n("Fix FindEGL when using Emscripten");?></li>
<li><?php i18n("ECMAddQch: add INCLUDE_DIRS argument");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("ensure winId() not called on non-native widgets (bug 412675)");?></li>
</ul>

<h3><?php i18n("kcalendarcore");?></h3>

<p>New module, previously known as kcalcore in kdepim</p>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("Suppress mouse events in KCMs causing window moves");?></li>
<li><?php i18n("adjust margins of KCMultiDialog (bug 411161)");?></li>
</ul>

<h3><?php i18n("KCompletion");?></h3>

<ul>
<li><?php i18n("[KComboBox] Properly disable Qt's builtin completer [regression fix]");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Fix generating properties that start with an uppercase letter");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Make KColorScheme compatible with QVariant");?></li>
</ul>

<h3><?php i18n("kcontacts");?></h3>

<p><?php i18n("New module, previously part of KDE PIM");?></p>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Add KListOpenFilesJob");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Delete QQmlObjectSharedEngine context in sync with QQmlObject");?></li>
<li><?php i18n("[KDeclarative] Port from deprecated QWheelEvent::delta() to angleDelta()");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Support NetworkManager 1.20 and do actually compile the NM backend");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Deprecate the global [Small|Desktop|Bar]Icon() methods");?></li>
</ul>

<h3><?php i18n("KImageFormats");?></h3>

<ul>
<li><?php i18n("Add files for testing bug411327");?></li>
<li><?php i18n("xcf: Fix regression when reading files with \"unsupported\" properties");?></li>
<li><?php i18n("xcf: Properly read image resolution");?></li>
<li><?php i18n("Port HDR (Radiance RGBE) image loader to Qt5");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("[Places panel] Revamp the Recently Saved section");?></li>
<li><?php i18n("[DataProtocol] compile without implicit conversion from ascii");?></li>
<li><?php i18n("Consider the usage of WebDAV methods sufficient for assuming WebDAV");?></li>
<li><?php i18n("REPORT also supports the Depth header");?></li>
<li><?php i18n("Make QSslError::SslError &lt;-&gt; KSslError::Error conversion reusable");?></li>
<li><?php i18n("Deprecate the KSslError::Error ctor of KSslError");?></li>
<li><?php i18n("[Windows] fix listing the parent dir of C:\foo, that's C:\ and not C:");?></li>
<li><?php i18n("Fix crash on exit in kio_file (bug 408797)");?></li>
<li><?php i18n("Add == and != operators to KIO::UDSEntry");?></li>
<li><?php i18n("Replace KSslError::errorString with QSslError::errorString");?></li>
<li><?php i18n("Move/copy job: skip stat'ing sources if the destination dir isn't writable (bug 141564)");?></li>
<li><?php i18n("Fixed interaction with DOS/Windows executables in KRun::runUrl");?></li>
<li><?php i18n("[KUrlNavigatorPlacesSelector] Properly identify teardown action (bug 403454)");?></li>
<li><?php i18n("KCoreDirLister: fix crash when creating new folders from kfilewidget (bug 401916)");?></li>
<li><?php i18n("[kpropertiesdialog] add icons for the size section");?></li>
<li><?php i18n("Add icons for \"Open With\" and \"Actions\" menus");?></li>
<li><?php i18n("Avoid initializing an unnecessary variable");?></li>
<li><?php i18n("Move more functionality from KRun::runCommand/runApplication to KProcessRunner");?></li>
<li><?php i18n("[Advanced Permissions] Fix icon names (bug 411915)");?></li>
<li><?php i18n("[KUrlNavigatorButton] Fix QString usage to not use [] out of bounds");?></li>
<li><?php i18n("Make KSslError hold a QSslError internally");?></li>
<li><?php i18n("Split KSslErrorUiData from KTcpSocket");?></li>
<li><?php i18n("Port kpac from QtScript");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("always cache just the last item");?></li>
<li><?php i18n("more z (bug 411832)");?></li>
<li><?php i18n("fix import version in PagePoolAction");?></li>
<li><?php i18n("PagePool is Kirigami 2.11");?></li>
<li><?php i18n("take into account dragging speed when a flick ends");?></li>
<li><?php i18n("Fix copying urls to the clipboard");?></li>
<li><?php i18n("check more if we are reparenting an actual Item");?></li>
<li><?php i18n("basic support for ListItem actions");?></li>
<li><?php i18n("introduce cachePages");?></li>
<li><?php i18n("fix compatibility with Qt5.11");?></li>
<li><?php i18n("introduce PagePoolAction");?></li>
<li><?php i18n("new class: PagePool to manage recycling of pages after they're popped");?></li>
<li><?php i18n("make tabbars look better");?></li>
<li><?php i18n("some margin on the right (bug 409630)");?></li>
<li><?php i18n("Revert \"Compensate smaller icon sizes on mobile in the ActionButton\"");?></li>
<li><?php i18n("don't make list items look inactive (bug 408191)");?></li>
<li><?php i18n("Revert \"Remove scaling of iconsize unit for isMobile\"");?></li>
<li><?php i18n("Layout.fillWidth should be done by the client (bug 411188)");?></li>
<li><?php i18n("Add template for Kirigami application development");?></li>
<li><?php i18n("Add a mode to center actions and omit the title when using a ToolBar style (bug 402948)");?></li>
<li><?php i18n("Compensate smaller icon sizes on mobile in the ActionButton");?></li>
<li><?php i18n("Fixed some undefined properties runtime errors");?></li>
<li><?php i18n("Fix ListSectionHeader background color for some color schemes");?></li>
<li><?php i18n("Remove custom content item from ActionMenu separator");?></li>
</ul>

<h3><?php i18n("KItemViews");?></h3>

<ul>
<li><?php i18n("[KItemViews] Port to non-deprecated QWheelEvent API");?></li>
</ul>

<h3><?php i18n("KJobWidgets");?></h3>

<ul>
<li><?php i18n("cleanup dbus related objects early enough to avoid hang on program exit");?></li>
</ul>

<h3><?php i18n("KJS");?></h3>

<ul>
<li><?php i18n("Added startsWith(), endsWith() and includes() JS String functions");?></li>
<li><?php i18n("Fixed Date.prototype.toJSON() called on non-Date objects");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Bring KNewStuffQuick to feature parity with KNewStuff(Widgets)");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("Claim Android as a supported platform");?></li>
<li><?php i18n("Deploy default avatar via qrc");?></li>
<li><?php i18n("Bundle plugin files on Android");?></li>
<li><?php i18n("Disable DBus pieces on Android");?></li>
<li><?php i18n("Fix crash when monitoring a contact that gets removed on PersonData (bug 410746)");?></li>
<li><?php i18n("Use fully qualified types on signals");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("Consider UNC paths as NetworkShare context");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Move Amusement to Games directory instead of Games &gt; Toys (bug 412553)");?></li>
<li><?php i18n("[KService] Add copy constructor");?></li>
<li><?php i18n("[KService] add workingDirectory(), deprecate path()");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("try to avoid artifacts in text preview");?></li>
<li><?php i18n("Variable expansion: Use std::function internally");?></li>
<li><?php i18n("QRectF instead of QRect solves clipping issues, (bug 390451)");?></li>
<li><?php i18n("next rendering artifact goes away if you adjust the clip rect a bit (bug 390451)");?></li>
<li><?php i18n("avoid the font choosing magic and turn of anti aliasing (bug 390451)");?></li>
<li><?php i18n("KadeModeMenuList: fix memory leaks and others");?></li>
<li><?php i18n("try to scan for usable fonts, works reasonable well if you use no dumb scaling factor like 1.1");?></li>
<li><?php i18n("Status bar mode menu: Reuse empty QIcon that is implicitly shared");?></li>
<li><?php i18n("Expose KTextEditor::MainWindow::showPluginConfigPage()");?></li>
<li><?php i18n("Replace QSignalMapper with lambda");?></li>
<li><?php i18n("KateModeMenuList: use QString() for empty strings");?></li>
<li><?php i18n("KateModeMenuList: add \"Best Search Matches\" section and fixes for Windows");?></li>
<li><?php i18n("Variable expansion: Support QTextEdits");?></li>
<li><?php i18n("Add keyboard shortcut for switching Input modes to edit menu (bug 400486)");?></li>
<li><?php i18n("Variable expansion dialog: properly handle selection changes and item activation");?></li>
<li><?php i18n("Variable expansion dialog: add filter line edit");?></li>
<li><?php i18n("Backup on save: Support time and date string replacements (bug 403583)");?></li>
<li><?php i18n("Variable expansion: Prefer return value over return argument");?></li>
<li><?php i18n("Initial start of variables dialog");?></li>
<li><?php i18n("use new format API");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("HiDPI support");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Sort files alphabetically in cmake list");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Make OK button configurable in KMessageBox::sorry/detailedSorry");?></li>
<li><?php i18n("[KCollapsibleGroupBox] Fix QTimeLine::start warning at runtime");?></li>
<li><?php i18n("Improve naming of KTitleWidget icon methods");?></li>
<li><?php i18n("Add QIcon setters for the password dialogs");?></li>
<li><?php i18n("[KWidgetAddons] port to non-deprecated Qt API");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Set XCB to required if building the X backend");?></li>
<li><?php i18n("Make less use of deprecated enum alias NET::StaysOnTop");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Move \"Full Screen Mode\" item from Settings menu to View menu (bug 106807)");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("ActiveConnection: connect stateChanged() signal to correct interface");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Export Plasma core lib log category, add a category to a qWarning");?></li>
<li><?php i18n("[pluginloader] Use categorized logging");?></li>
<li><?php i18n("make editMode a corona global property");?></li>
<li><?php i18n("Honor global animation speed factor");?></li>
<li><?php i18n("properly install whole plasmacomponent3");?></li>
<li><?php i18n("[Dialog] Apply window type after changing flags");?></li>
<li><?php i18n("Change reveal password button logic");?></li>
<li><?php i18n("Fix crash on teardown with Applet's ConfigLoader (bug 411221)");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Fix several build system errors");?></li>
<li><?php i18n("take margins from qstyle");?></li>
<li><?php i18n("[Tab] Fix sizing (bug 409390)");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Add syntax highlighting for RenPy (.rpy) (bug 381547)");?></li>
<li><?php i18n("WordDetect rule: detect delimiters at the inner edge of the string");?></li>
<li><?php i18n("Highlight GeoJSON files as if they were plain JSON");?></li>
<li><?php i18n("Add syntax highlighting for SubRip Text (SRT) Subtitles");?></li>
<li><?php i18n("Fix skipOffset with dynamic RegExpr (bug 399388)");?></li>
<li><?php i18n("bitbake: handle embedded shell and python");?></li>
<li><?php i18n("Jam: fix identifier in a SubRule");?></li>
<li><?php i18n("Add syntax definition for Perl6 (bug 392468)");?></li>
<li><?php i18n("support .inl extension for C++, not used by other xml files at the moment (bug 411921)");?></li>
<li><?php i18n("support *.rej for diff highlighting (bug 411857)");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p><?php i18n("The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB");?></p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.63");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.11");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://phabricator.kde.org/project/view/90/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
