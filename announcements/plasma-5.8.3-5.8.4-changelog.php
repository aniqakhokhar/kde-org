<?php
include_once ("functions.inc");
$translation_file = "kde-org";
$page_title = i18n_noop("Plasma 5.8.4 Complete Changelog");
$site_root = "../";
$release = 'plasma-5.8.4';
include "header.inc";
?>
<p><a href="plasma-5.8.4.php">Plasma 5.8.4</a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='breeze' href='http://cgit.kde.org/breeze.git'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>Revert "[kstyle] Implement application unpolish to delete ShadowHelper". <a href='http://commits.kde.org/breeze/adcd98aa1f2ac2f3d0da46dcc41eb23944acbafe'>Commit.</a> </li>
<li>[kstyle] Implement application unpolish to delete ShadowHelper. <a href='http://commits.kde.org/breeze/a0433d089aa96ff60b625be10082e914c19b58d3'>Commit.</a> See bug <a href='https://bugs.kde.org/372001'>#372001</a></li>
<li>[kstyle] Delay init of Wayland setup till next event cycle. <a href='http://commits.kde.org/breeze/088c7672981ba6d4b83946a280683764ce300d60'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372001'>#372001</a></li>
<li>Fix broken kcfgc files. <a href='http://commits.kde.org/breeze/a6ade1b7ccde2b7bdc273d6b1d15936c8f20b24b'>Commit.</a> </li>
</ul>


<h3><a name='breeze-grub' href='http://cgit.kde.org/breeze-grub.git'>Breeze-grub</a></h3>
<ul id='ulbreeze-grub' style='display: block'><li>New in this release</li></ul>
<h3><a name='discover' href='http://cgit.kde.org/discover.git'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Make sure the application delegate doesn't overflow when the view is narrow. <a href='http://commits.kde.org/discover/6d4968159f5d62fea748b6c8a56307d31bb41f8f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372561'>#372561</a></li>
<li>Remove unused files. <a href='http://commits.kde.org/discover/7143b7eb77a3f13fcebad24a9d2b3681067538bb'>Commit.</a> </li>
<li>Elide category names when they're too big. <a href='http://commits.kde.org/discover/f4c58d9cff385af1b0de4645d376347b1f45c0aa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372559'>#372559</a></li>
<li>Make it possible to compile with the old and new App*treamConfig.cmake. <a href='http://commits.kde.org/discover/1343fee00aa15f8722b42686088112bbf582b583'>Commit.</a> </li>
<li>Move ifdefs to Discover instead of modifying Appstream. <a href='http://commits.kde.org/discover/18b610278e2b7122a9dd164a665d76d1e5b7a9e1'>Commit.</a> </li>
<li>Make it possible for 5.8 to compile against the last AppStreamQt. <a href='http://commits.kde.org/discover/c52df70071837998f535005e7d9695ca14795128'>Commit.</a> </li>
<li>Make screenshots visible when there's only one screenshot too. <a href='http://commits.kde.org/discover/3297fe6026edebb8db72bb179289bee844c26ae3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371724'>#371724</a></li>
</ul>


<h3><a name='kdeplasma-addons' href='http://cgit.kde.org/kdeplasma-addons.git'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>Font rendering in window/desktop switchers now follows anti-aliasing and hinting settings. <a href='http://commits.kde.org/kdeplasma-addons/00369ec41315470ebf121bd12d8309ddf9ed1b87'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372218'>#372218</a></li>
<li>Fix systemloadviewer tooltip. <a href='http://commits.kde.org/kdeplasma-addons/02ebdb543506ce308a8e3e819ccb7d95c4b9bab4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129309'>#129309</a></li>
</ul>


<h3><a name='kwayland-integration' href='http://cgit.kde.org/kwayland-integration.git'>KWayland-integration</a> </h3>
<ul id='ulkwayland-integration' style='display: block'>
<li>[idletime] Tear-down Wayland objects before app quits. <a href='http://commits.kde.org/kwayland-integration/375b7e52326b95ab0b728d16a53344e2c958b264'>Commit.</a> </li>
</ul>


<h3><a name='kwin' href='http://cgit.kde.org/kwin.git'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Fix AbstractClient::sizeForClientSize. <a href='http://commits.kde.org/kwin/16c7650d760f2d0b9e2d0d826b820963dc3d018c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370345'>#370345</a></li>
<li>Ensure that all Effects honour the grab roles correctly. <a href='http://commits.kde.org/kwin/fb69b791a16f4a89fd79a010ce8f67419de16004'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/336866'>#336866</a></li>
<li>Unpolish QStyle on QApp prior to destroying internal Wayland connection. <a href='http://commits.kde.org/kwin/ee7da425cefcd5cbe480335a144eee6218207dd9'>Commit.</a> See bug <a href='https://bugs.kde.org/372001'>#372001</a></li>
<li>[helpers/killer] Force to platform XCB. <a href='http://commits.kde.org/kwin/f3325a1cb8014f8073638f8322276562a863451c'>Commit.</a> </li>
<li>Simplify the window title passed in from the window system. <a href='http://commits.kde.org/kwin/2a155925719f7540373386e29c6a23a5c6021b56'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/323798'>#323798</a></li>
<li>[desktopgrid] Don't manipulate virtual desktop of desktop window. <a href='http://commits.kde.org/kwin/3c6371390d698c8dd7a8c0704debba43d5d3b465'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372037'>#372037</a></li>
<li>Invert the scroll direction for wheel events on internal windows. <a href='http://commits.kde.org/kwin/da0ba763248367fffb9fa7277009110ca851a2a1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371999'>#371999</a></li>
<li>Internal windows cannot be minimized. <a href='http://commits.kde.org/kwin/db58c421eb1b873516d00244984b22e38d4935a8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372000'>#372000</a></li>
<li>[autotests/libinput] Extend the Device test. <a href='http://commits.kde.org/kwin/fb4b0c40efcd02877a286e4fd2ff6a649e9fc8e1'>Commit.</a> </li>
<li>[autotests/libinput] Extend device test for scrol methods. <a href='http://commits.kde.org/kwin/f1d17c1a5690f6dfb0491a27cf68bac8e81ebebe'>Commit.</a> </li>
<li>[autotests/libinput] Extend Device test case. <a href='http://commits.kde.org/kwin/e9dfdbe6a8956722ed983957453db117e89b168f'>Commit.</a> </li>
<li>[libinput] Add scroll method configuration support. <a href='http://commits.kde.org/kwin/e4283543f633b8e56fe8ce999dcd8d12bfa31e64'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371803'>#371803</a></li>
<li>[libinput] Add natural scrolling support. <a href='http://commits.kde.org/kwin/b19341777a566ba4b20485e161c5eeb25b521d0a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371791'>#371791</a></li>
<li>[libinput] Add middle mouse button emulation support. <a href='http://commits.kde.org/kwin/e00649d00224326cb782776d3c7af8a0d0727803'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371756'>#371756</a></li>
<li>Don't snap to auto-hidden panels. <a href='http://commits.kde.org/kwin/679e41780888b428a64389e06d7e2738904f3a98'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365892'>#365892</a></li>
<li>[scripting] Silence unused variable warnings. <a href='http://commits.kde.org/kwin/6c121436e2c38624e89b2d607f573491a1a24b73'>Commit.</a> </li>
<li>[scripting] Fix export of WorkspaceWrapper in QtScript. <a href='http://commits.kde.org/kwin/be9ee989b19e51a61e257e7224786369ba87cbd1'>Commit.</a> </li>
<li>Support for workspace.clientList() in declarative script. <a href='http://commits.kde.org/kwin/4730be084c1b071db5b0869f13e6c444a35e2fb3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/340125'>#340125</a>. Code review <a href='https://git.reviewboard.kde.org/r/D3185'>#D3185</a></li>
<li>Correctly set i18n suffix in mousemark spinbox. <a href='http://commits.kde.org/kwin/254690e899dce589ce3b172c998b031a6405895b'>Commit.</a> </li>
<li>[autotests] Add test case for snap to auto-hiding panel. <a href='http://commits.kde.org/kwin/3a179f32af51301d831891ce507714deb2fca952'>Commit.</a> See bug <a href='https://bugs.kde.org/365892'>#365892</a></li>
</ul>


<h3><a name='oxygen' href='http://cgit.kde.org/oxygen.git'>Oxygen</a> </h3>
<ul id='uloxygen' style='display: block'>
<li>Fix broken kcfgc files. <a href='http://commits.kde.org/oxygen/eb6ba888913283f74de5e2172e872bc9d3f983a7'>Commit.</a> </li>
</ul>


<h3><a name='plasma-desktop' href='http://cgit.kde.org/plasma-desktop.git'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Badges and progress display in task manager now works with more 3rd party applications. <a href='http://commits.kde.org/plasma-desktop/5e1d92afd0b12ee89d14d686e98568ff5cd41ea3'>Commit.</a> </li>
<li>[Task Manager] Pass local file to KService instead of stringified URL. <a href='http://commits.kde.org/plasma-desktop/a01792207c9cc786243e815606f7a70e27a74ea0'>Commit.</a> </li>
<li>[Task Manager] Fix Unity launcher count badge rendering when task is created. <a href='http://commits.kde.org/plasma-desktop/87579efbe4f3d163c576b6f90661b8116c51de69'>Commit.</a> </li>
</ul>


<h3><a name='plasma-sdk' href='http://cgit.kde.org/plasma-sdk.git'>Plasma SDK</a> </h3>
<ul id='ulplasma-sdk' style='display: block'>
<li>Use NO_POLICY_SCOPE on KDECompilerSettings. <a href='http://commits.kde.org/plasma-sdk/26fd06004b413a6aafcfb8ebce20d82cba1c871e'>Commit.</a> </li>
</ul>


<h3><a name='plasma-workspace' href='http://cgit.kde.org/plasma-workspace.git'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Disable Qt's high DPI scaling on shutdown/switch user dialogs. <a href='http://commits.kde.org/plasma-workspace/4b2abc581c6b3e7a4c2f1f893d47fad5d3806aca'>Commit.</a> See bug <a href='https://bugs.kde.org/366451'>#366451</a></li>
<li>Don't set PanelView visibilty when opening/closing config. <a href='http://commits.kde.org/plasma-workspace/7248a71a959f2099a4c27533c5316345c1a11fa1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372248'>#372248</a></li>
<li>Check KPluginInfo.service() returns a valid object. <a href='http://commits.kde.org/plasma-workspace/238ec3ee671bcf6716348122bebefb20c25d1101'>Commit.</a> </li>
<li>Add plasma-discover to mappings. <a href='http://commits.kde.org/plasma-workspace/de6ee92146bbe3646f11e078071d659394412094'>Commit.</a> </li>
<li>[Windowed Widgets Runner] Don't access invalid KPluginMetaData. <a href='http://commits.kde.org/plasma-workspace/59b2d1effcee8d449cbbcd237ba8cebaeb4dd949'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372017'>#372017</a></li>
<li>Load screenpool at the same time as we connect to screenchanged signals. <a href='http://commits.kde.org/plasma-workspace/7154fb681adc73c482e862febc7ad008f77058dd'>Commit.</a> See bug <a href='https://bugs.kde.org/372099'>#372099</a>. See bug <a href='https://bugs.kde.org/371858'>#371858</a>. See bug <a href='https://bugs.kde.org/371819'>#371819</a>. See bug <a href='https://bugs.kde.org/371734'>#371734</a></li>
<li>Avoid connecting to screen changed signals twice. <a href='http://commits.kde.org/plasma-workspace/8a472f17ce11f3b79d740cdc21096d82b8683f3d'>Commit.</a> See bug <a href='https://bugs.kde.org/372099'>#372099</a>. See bug <a href='https://bugs.kde.org/371858'>#371858</a>. See bug <a href='https://bugs.kde.org/371819'>#371819</a>. See bug <a href='https://bugs.kde.org/371734'>#371734</a></li>
<li>Sync app config in sync with applets config. <a href='http://commits.kde.org/plasma-workspace/329f00dc90f6645ff6e531542212ea73fb8ef274'>Commit.</a> </li>
<li>Make sure all outputs are known. <a href='http://commits.kde.org/plasma-workspace/b8d3e09b3687082037a6d280d2032617121ae5e5'>Commit.</a> See bug <a href='https://bugs.kde.org/372099'>#372099</a>. See bug <a href='https://bugs.kde.org/371858'>#371858</a>. See bug <a href='https://bugs.kde.org/371991'>#371991</a>. See bug <a href='https://bugs.kde.org/371819'>#371819</a>. See bug <a href='https://bugs.kde.org/371734'>#371734</a></li>
<li>[taskmanagerrulesrc] Add Rewrite Rule for Google-chrome. <a href='http://commits.kde.org/plasma-workspace/7c443aa53900521deab4fcd4641ea5273afc294e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372211'>#372211</a></li>
<li>[XWindowTasksModel] Check for service being empty before searching new ones. <a href='http://commits.kde.org/plasma-workspace/61860066a4cea845598fa4904607db1bba411356'>Commit.</a> See bug <a href='https://bugs.kde.org/372211'>#372211</a></li>
<li>Fix behavior of scrollable systray popups. <a href='http://commits.kde.org/plasma-workspace/95a8a620248d96ca089c222689605042e27e8fb5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372050'>#372050</a></li>
<li>Introduce new pre-layout script hook. <a href='http://commits.kde.org/plasma-workspace/b9760e852042fb113caef8ad547967b98cfdd32a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371704'>#371704</a></li>
<li>Make sure we're looking both for json and desktop metadata. <a href='http://commits.kde.org/plasma-workspace/bfb91f9de153d53a30d82ad6ce0d6732da4ab367'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129276'>#129276</a></li>
<li>Modified distance function to keep aspect ratio as close as possible. <a href='http://commits.kde.org/plasma-workspace/18858d16d6b1ac246193f75673cdbcb791daebff'>Commit.</a> </li>
</ul>


<?php
  include("footer.inc");
?>
