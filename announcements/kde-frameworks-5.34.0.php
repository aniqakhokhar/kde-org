<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.34.0");
  $site_root = "../";
  $release = '5.34.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
May 13, 2017. KDE today announces the release
of KDE Frameworks 5.34.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("balooctl, baloosearch, balooshow: Fix order of QCoreApplication object creation (bug 378539)");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Add icons for hotspot (https://github.com/KDAB/hotspot)");?></li>
<li><?php i18n("Better version control system icons (bug 377380)");?></li>
<li><?php i18n("Add plasmate icon (bug 376780)");?></li>
<li><?php i18n("Update microphone-sensitivity icons (bug 377012)");?></li>
<li><?php i18n("Raise default for 'Panel' icons to 48");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Sanitizers: Don't use GCC-like flags for e.g. MSVC");?></li>
<li><?php i18n("KDEPackageAppTemplates: documentation improvements");?></li>
<li><?php i18n("KDECompilerSettings: Pass -Wvla &amp; -Wdate-time");?></li>
<li><?php i18n("Support older qmlplugindump versions");?></li>
<li><?php i18n("Introduce ecm_generate_qmltypes");?></li>
<li><?php i18n("Allow projects to include the file twice");?></li>
<li><?php i18n("Fix rx that matches project names out of the git uri");?></li>
<li><?php i18n("Introduce fetch-translations build command");?></li>
<li><?php i18n("Use -Wno-gnu-zero-variadic-macro-arguments more");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("We are using only Tier 1 frameworks, so move us to Tier 2");?></li>
<li><?php i18n("Removed KIO from the deps");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Security fix: verify that whoever is calling us is actually who he says he is");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Fix relativePath calculation in KDesktopFile::locateLocal() (bug 345100)");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Set the icon for the Donate action");?></li>
<li><?php i18n("Relax constraints for processing QGroupBoxes");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Don't set ItemHasContents in DropArea");?></li>
<li><?php i18n("Don't accept hover events in the DragArea");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Workaround for MSVC and catalog loading");?></li>
<li><?php i18n("Solve a visibility conflict for meinproc5 (bug 379142)");?></li>
<li><?php i18n("Quote few other variables with path (avoid issues with spaces)");?></li>
<li><?php i18n("Quote few variables with path (avoid issues with spaces)");?></li>
<li><?php i18n("Temporarily disable the local doc on Windows");?></li>
<li><?php i18n("FindDocBookXML4.cmake, FindDocBookXSL.cmake - search in homebrew installations");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("makes KArchive be optional and do not build extractors needing it");?></li>
<li><?php i18n("fix duplicated symbols compilation error with mingw on Windows");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("build: Remove KService dependency");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("fix basename handling of po files (bug 379116)");?></li>
<li><?php i18n("Fix ki18n bootstrapping");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Don't even try to create icons with empty sizes");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("KDirSortFilterProxyModel: bring back natural sorting (bug 343452)");?></li>
<li><?php i18n("Fill UDS_CREATION_TIME with the value of st_birthtime on FreeBSD");?></li>
<li><?php i18n("http slave: send error page after authorization failure (bug 373323)");?></li>
<li><?php i18n("kioexec: delegate upload to a kded module (bug 370532)");?></li>
<li><?php i18n("Fix KDirlister Gui Test setting URL scheme twice");?></li>
<li><?php i18n("Delete kiod modules on exit");?></li>
<li><?php i18n("Generate a moc_predefs.h file for KIOCore (bug 371721)");?></li>
<li><?php i18n("kioexec: fix support for --suggestedfilename");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Allow multiple categories with the same name");?></li>
<li><?php i18n("KNewStuff: Show file's size information in grid delegate");?></li>
<li><?php i18n("If an entry's size is known, show it in the list view");?></li>
<li><?php i18n("Register and declare KNSCore::EntryInternal::List as a metatype");?></li>
<li><?php i18n("Don't fall through the switch. Double entries? No please");?></li>
<li><?php i18n("always close the downloaded file after downloading");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("Fix include path in KF5PackageMacros.cmake");?></li>
<li><?php i18n("Ignore warnings during appdata generation (bug 378529)");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("Template: Change toplevel template category to \"Plasma\"");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("KAuth integration in document saving - vol. 2");?></li>
<li><?php i18n("Fix assertion when applying code folding that changes cursor position");?></li>
<li><?php i18n("Use non-deprecated &lt;gui&gt; root element in ui.rc file");?></li>
<li><?php i18n("Add scroll-bar-marks also to the built-in search&amp;replace");?></li>
<li><?php i18n("KAuth integration in document saving");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Validate surface is valid when sending TextInput leave event");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("KNewPasswordWidget: don't hide visibility action in plaintext mode (bug 378276)");?></li>
<li><?php i18n("KPasswordDialog: don't hide visibility action in plaintext mode (bug 378276)");?></li>
<li><?php i18n("Fix KActionSelectorPrivate::insertionIndex()");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("kcm_useraccount is dead, long live user_manager");?></li>
<li><?php i18n("Reproducible builds: drop version from XMLGUI_COMPILING_OS");?></li>
<li><?php i18n("Fix: DOCTYPE name must match root element type");?></li>
<li><?php i18n("Fix wrong usage of ANY in kpartgui.dtd");?></li>
<li><?php i18n("Use non-deprecated &lt;gui&gt; root element");?></li>
<li><?php i18n("API dox fixes: replace 0 with nullptr or remove where not applied");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Fix crash when retrieving active connection list (bug 373993)");?></li>
<li><?php i18n("Set default value for auto-negotiation based on running NM version");?></li>
</ul>

<h3><?php i18n("Oxygen Icons");?></h3>

<ul>
<li><?php i18n("Add icon for hotspot (https://github.com/KDAB/hotspot)");?></li>
<li><?php i18n("Raise default for 'Panel' icons to 48");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("reload icon when usesPlasmaTheme changes");?></li>
<li><?php i18n("Install Plasma Components 3 so they can be used");?></li>
<li><?php i18n("Introduce units.iconSizeHints.* to provide user-configurable icon size hints (bug 378443)");?></li>
<li><?php i18n("[TextFieldStyle] Fix textField is not defined error");?></li>
<li><?php i18n("Update the ungrabMouse hack for Qt 5.8");?></li>
<li><?php i18n("Guard against Applet not loading AppletInterface (bug 377050)");?></li>
<li><?php i18n("Calendar: Use correct language for month and day names");?></li>
<li><?php i18n("Generate plugins.qmltypes files for the plugins we install");?></li>
<li><?php i18n("if the user did set an implicit size, keep it");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Add include that is needed in msys2");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Add Arduino extension");?></li>
<li><?php i18n("LaTeX: Fix Incorrect termination of \iffalse comments (bug 378487)");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.34");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.6");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
