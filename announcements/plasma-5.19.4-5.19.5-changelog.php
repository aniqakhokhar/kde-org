<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.19.5 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.19.5";
?>

<style>
main {
	padding-top: 20px;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Properly wrap text on review popup header. <a href='https://commits.kde.org/discover/e18bddd4d5d8be31cf7af8e98562ab63af464d0c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425382'>#425382</a></li>
<li>Ignore --mode when we have a specific query requested already. <a href='https://commits.kde.org/discover/a4cbdcc5e80b0866bff3969297e880baa5e24bd0'>Commit.</a> </li>
</ul>


<h3><a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a> </h3>
<ul id='ulksysguard' style='display: block'>
<li>[ksgrd] Correctly handle monitors list changing. <a href='https://commits.kde.org/ksysguard/dbb656d54fea47fcb322ffb8e54a3b59ade316fd'>Commit.</a> </li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Effects/slidingpopups: Properly clip windows. <a href='https://commits.kde.org/kwin/edcfe6e342824790a08e4132d564e767672f0d95'>Commit.</a> </li>
</ul>


<h3><a name='plasma-browser-integration' href='https://commits.kde.org/plasma-browser-integration'>plasma-browser-integration</a> </h3>
<ul id='ulplasma-browser-integration' style='display: block'>
<li>[DownloadJob] Fall back to url when finalUrl isn't set. <a href='https://commits.kde.org/plasma-browser-integration/38b3ec95feedc3a3eea19d80d664dfbdaddfab21'>Commit.</a> </li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Revert "[Folder view] Fix binding loop on width". <a href='https://commits.kde.org/plasma-desktop/e7da61c1471d83ef28d6686542b40156cc9bd231'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423511'>#423511</a></li>
<li>Hardcode icons for default plasma components. <a href='https://commits.kde.org/plasma-desktop/3b56b25d96fb94e0f5f31925eeddf8453fb63399'>Commit.</a> See bug <a href='https://bugs.kde.org/424197'>#424197</a>. See bug <a href='https://bugs.kde.org/424198'>#424198</a>. See bug <a href='https://bugs.kde.org/424194'>#424194</a>. See bug <a href='https://bugs.kde.org/424195'>#424195</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[logout screen] Set implicit widths on OK and Cancel buttons. <a href='https://commits.kde.org/plasma-workspace/163abea5e4d0dc258e936702d57bc0377ada4a8a'>Commit.</a> </li>
<li>KDisplayManager: Work without $DISPLAY set. <a href='https://commits.kde.org/plasma-workspace/19c06e72937d355ef73cd74b8df126f875822f3a'>Commit.</a> </li>
</ul>


<h3><a name='plymouth-kcm' href='https://commits.kde.org/plymouth-kcm'>Plymouth KControl Module</a> </h3>
<ul id='ulplymouth-kcm' style='display: block'>
<li>Fix running of update-alternatives. <a href='https://commits.kde.org/plymouth-kcm/d172c05e18d20f351fd68f65f964163594f38592'>Commit.</a> </li>
</ul>


<h3><a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>On wakeup from suspend restore remembered keyboard brightness. <a href='https://commits.kde.org/powerdevil/e1ed36480cec6a49c166347efba6ab52adc0c37c'>Commit.</a> </li>
</ul>


<h3><a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a> </h3>
<ul id='ulxdg-desktop-portal-kde' style='display: block'>
<li>Add "Name" entry into the desktop file. <a href='https://commits.kde.org/xdg-desktop-portal-kde/79cc80a2b6140e2136db24896f97e6ecd212bba0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/426004'>#426004</a></li>
<li>Print: enable printing of multiple copies. <a href='https://commits.kde.org/xdg-desktop-portal-kde/06d1c6874829849210f62f069b93b6c74bcb363c'>Commit.</a> See bug <a href='https://bugs.kde.org/419944'>#419944</a></li>
</ul>


</main>
<?php
	require('../aether/footer.php');
