<?php
  $page_title = "KDE included in SuSE Linux Announcements";
  $site_root = "../";
  include "header.inc";
?>

<CENTER><TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH="570" >
<TR VALIGN=TOP>
<TD>

<pre>



  FOR IMMEDIATE RELEASE


  OAKLAND,  CALIF & FUERTH, GERMANY (July 29, 1998) - S.u.S.E., Inc.,
  in conjunction with S.u.S.E., GMBH, announced the inclusion of  the
  KDE   desktop environment  in  S.u.S.E.  Linux  5.3,  scheduled for
  release in Germany on August 3.  The international release of   the
  English version is scheduled for August 20, with Italian and French
  versions following in early September.

  S.u.S.E. Linux 5.3 is the first commercial  Linux  distribution  to
  include  the K Desktop Environment in its entirety. "KDE, operating
  from a shared commitment to provide the latest  in  precision-engi-
  neered,  open-source  technology, allows users a remarkably stable,
  user-friendly, and customizable desktop environment.  We stand with
  confidence  behind  KDE, and it is our firm belief that any release
  of the Linux operating  system would  be incomplete   without   its
  inclusion,"  said  Scott Winterton, Outreach Director for S.u.S.E.,
  Inc.

  The new and innovative KDE desktop provides a contemporary, network
  transparent desktop environment for the Unix platform.  Among KDE's
  key strengths are its functionality, stability, as well as unprece-
  dented   ease of  use.  A  rich set of KDE applications, exhibiting
  umatched interoperability, is supplied with the K desktop  environ-
  ment.

  S.u.S.E., Inc. and S.u.S.E., GMBH develop and distribute the preci-
  sion-engineered S.u.S.E. Linux Operating system, in addition  to  a
  variety of other commercial Linux applications.

  Press Contact US: Scott Winterton, (510) 835-7873; saw@suse.com.
  Press Contact Europe: Christian Egle + 49 911 740 53 44; ce@suse.de

  Find  out more about S.u.S.E. Linux 5.3 and other S.u.S.E. products
  by visiting their web site: 

		http://www.suse.com/Product/lx53/

  For more information on the K  Desktop  Environment,  please  visit
  their web site:
                       http://www.kde.org

  or contact press@kde.org.



</pre>

</TD>
</TR>
</TABLE></CENTER>


<?php include "footer.inc" ?>
