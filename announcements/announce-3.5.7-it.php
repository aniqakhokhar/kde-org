<?php

  $page_title = "Annuncio del rilascio di KDE 3.5.7";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

<!-- Other languages translations -->
Disponibile anche in:
<a href="announce-3.5.7.php">Inglese</a>
<a href="announce-3.5.7-ca.php">Catalano</a>
<a href="announce-3.5.7-es.php">Spagnolo</a>
<a href="announce-3.5.7-nl.php">Olandese</a>
<a href="announce-3.5.7-pt.php">Portoghese</a>

<h3 align="center">
   Il progetto KDE presenta la settima versione di servizio e traduzione della serie 3.5 di questo importante ambiente desktop libero
</h3>

<p align="justify">
  <strong>
    KDE 3.5.7 include traduzioni in 65 lingue, miglioramenti nella suite KDE PIM e anche in altre applicazioni.
  </strong>
</p>

<p align="justify">
  22 maggio 2007 (INTERNET)</b>. Il <a href="http://www.kde.org/"> progetto KDE</a> annuncia oggi l'immediata
  disponibilit&agrave; di KDE 3.5.7, una versione di manutenzione per l'ultima generazione del pi&ugrave; avanzato e
  potente desktop <em>libero</em> per GNU/Linux e altri UNIX. KDE attualmente supporta
  <a href="http://l10n.kde.org/stats/gui/stable/">65 lingue</a> e ci&ograve; lo rende accessibile a pi&ugrave; persone
  di quanto lo possa fare la maggior parte del software proprietario. Inoltre KDE pu&ograve; essere esteso facilmente 
  per supportare il lavoro proveniente da gruppi che desiderano contribuire nel progetto open source.
</p>

<p align="justify">
  Questo rilascio ha riservato un'attenzione particolare alle applicazioni di <a href="http://pim.kde.org/">KDE PIM</a>.
  <a href="http://pim.kde.org/components/kaddressbook.php">KAddressBook</a>,
  <a href="http://kontact.kde.org/korganizer/">KOrganizer</a> e
  <a href="http://pim.kde.org/components/kalarm.php">KAlarm</a> hanno ricevuto un profonda attenzione nella correzione
  di errori mentre
  <a href="http://pim.kde.org/components/kmail.php">KMail</a> presenta in pi&ugrave; nuove caratteristiche e miglioramenti
  con modifiche sia all'interfaccia sia alla gestione di IMAP: adesso pu&ograve; gestire le quote IMAP e spostare tutte
  le cartelle.
</p>

<p>
  Molte altre applicazioni hanno avuto dei miglioramenti:
</p>
<ul>
<li>
  <a href="http://kpdf.kde.org/">KPDF</a> mostra - fermandosi sopra col mouse - i suggerimenti
  per i collegamenti, visualizza correttamente file complessi come
  <a href="http://kpdf.kde.org/stuff/nytimes-firefox-final.pdf">questa pubblicit&agrave; di Firefox</a>
  e risponde ai comandi per l'apertura del pannello della tavola dei contenuti.
</li>

<li>
  <a href="http://uml.sourceforge.net/">Umbrello</a> adesso pu&ograve; generare ed esportare codice
  C# ed inoltre possiede il supporto per i tipi generici di Java 5.
</li>

<li>
  <a href="http://www.kdevelop.org/">KDevelop</a> ha avuto un aggiornamento alla versione principale
  3.4.1. Le nuove caratteristiche comprendono miglioramenti nel completamento del codice e nella navigazione,
  un'interfaccia pi&ugrave; affidabile al sistema di debug, supporto per Qt4 e un supporto migliorato per lo sviluppo di
  Ruby e KDE4.
</li>
</ul>

<p>
  Oltre alle nuove caratteristiche ci sono state parecchie correzioni di errori un po' dappertutto,
  specialmente nei pacchetti <a href="http://edu.kde.org/">Istruzione divertente</a>,
  <a href="http://games.kde.org/">Giochi</a> e <a href="http://kopete.kde.org/">Kopete</a>.
  Kopete, oltre alle correzioni di errori, ha un considerevole miglioramento delle prestazioni nella
  visualizzazione della chat.
</p>

<p>
  Come si aspettano gli utenti di KDE, questa nuova versione include un lavoro costante su
  KHTML e KJS, i motori HTML e Javascript di KDE. Una nuova e interessante caratteristica di usabilit&agrave;
  in KHTML fa in modo che il puntatore del mouse indichi se un collegamento si aprir&agrave; in una nuova finestra
  oppure no.
</p>

<p align="justify">
  Per una lista pi&ugrave; dettagliata di miglioramenti rispetto a
  <a href="http://www.kde.org/announcements/announce-3.5.6-it.php">KDE 3.5.6</a> del 25 gennaio 2007, fai
  riferimento al collegamento:
  <a href="http://www.kde.org/announcements/changelogs/changelog3_5_6to3_5_7.php">Cambiamenti KDE 3.5.7</a>.
</p>

<p align="justify">
  KDE 3.5.7 viene distribuito con il desktop base e 15 pacchetti aggiuntivi (PIM, amministrazione,
  rete, istruzione, utilit&agrave;, multimedia, giochi, artwork, sviluppo web ed altro).
  Le applicazioni e gli strumenti di KDE sono vincitori di molti riconoscimenti e sono disponibili
  in <strong>65 lingue</strong>.
</p>

<h4>
  Distribuzioni che rilasciano KDE
</h4>
<p align="justify">
  La maggior parte delle distribuzioni Linux e dei sistemi operativi UNIX non includono immediatamente
  i nuovi rilasci di KDE, ma i pacchetti di KDE 3.5.7 saranno integrati nei loro prossimi rilasci.
  Controlla <a href="http://www.kde.org/download/distributions.php">la lista</a> per vedere quali distribuzioni
  sono rilasciate con KDE.
</p>

<h4>
  Installazione pacchetti binari KDE 3.5.7
</h4>
<p align="justify">
  <em>Creatori dei pacchetti</em>.
  Alcuni produttori del sistema operativo hanno fornito gentilmente i pacchetti binari di
  KDE 3.5.7 per alcune versioni delle loro distribuzioni; in altri casi i pacchetti
  sono stati forniti da alcuni volontari.
  Alcuni di questi pacchetti binari sono disponibili e possono essere scaricati liberamente 
  al server di scaricamento di KDE
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.7/">http://download.kde.org</a>.
  Altri pacchetti binari, cos&igrave; come gli aggiornamenti ai pacchetti attualmente presenti, saranno
  disponibili nelle settimane successive.
</p>

<p align="justify">
  <a name="package_locations"><em>Posizione pacchetti</em></a>.
  Per una lista dei pacchetti binari disponibili dei quali il progetto KDE
  &egrave; stato informato visita <a href="/info/3.5.7.php">Pagina informazioni KDE 3.5.7</a>.
</p>

<h4>
  Compilazione KDE 3.5.7
</h4>
<p align="justify">
  <a name="source_code"></a><em>Codice Sorgente</em>.
  Il codice sorgente completo di KDE 3.5.7 pu&ograve; essere
  <a href="http://download.kde.org/stable/3.5.7/src/">scaricato liberamente</a>.
  Le istruzioni per compilare ed installare KDE 3.5.7
  sono disponibili nella <a href="/info/3.5.7.php">Pagina informazioni KDE 3.5.7</a>.
</p>

<h4>
  Sostenere KDE
</h4>
<p align="justify">
KDE &egrave; un progetto di <a href="http://www.gnu.org/philosophy/free-sw.it.html">Software Libero</a> che esiste
e cresce solo grazie all'aiuto di moltissimi volontari che donano il loro tempo e capacit&agrave;. KDE &egrave;
sempre alla ricerca di nuovi volontari e collaboratori che possano aiutare nella scrittura del codice, segnalazione
e correzione errori, scrittura della documentazione, traduzione, promozione, donazione di denaro, ecc. Tutti i
contributi sono apprezzati e calorosamente accettati. Per favore leggi la pagina <a href="/community/donations/">Come sostenere
KDE</a> per ulteriori informazioni.</p>

<p align="justify">
Non vediamo l'ora di sentir parlare di te!
</p>

<h4>
  Informazioni su KDE
</h4>
<p align="justify">
  KDE &egrave; un progetto <a href="/awards/">vincitore di riconoscimenti</a>, indipendente, che
  <a href="/people/">coinvolge centinaia</a> di sviluppatori, traduttori, artisti ed altri professionisti che
  collaborano da tutto il mondo attraverso internet.
  Questa squadra crea e distribuisce liberamente un ambiente desktop e una suite d'ufficio stabile e integrato.
  KDE fornisce un'architettura flessibile, suddivisa in componenti e trasparente nelle reti. KDE inoltre &egrave;
  dotato di potenti strumenti di sviluppo che offrono una piattaforma di sviluppo eccezionale.</p>

<p align="justify">
  KDE fornisce un desktop stabile e maturo che include un browser avanzato
  (<a href="http://konqueror.kde.org/">Konqueror</a>), una suite di gestione informazioni personali
  (<a href="http://kontact.org/">Kontact</a>), una suite per ufficio completa
  (<a href="http://www.koffice.org/">KOffice</a>), una grande raccolta di applicazioni
  e di utilit&agrave; di rete, e inoltre un ambiente di sviluppo efficiente,
  intuitivo con un'eccellente interfaccia integrata di sviluppo:
  <a href="http://www.kdevelop.org/">KDevelop</a>.</p>

<p align="justify">
  KDE &egrave; la prova che il modello di sviluppo di software Open Source "Stile-Bazar"
 pu&ograve; portare a tecnologie di prim'ordine alla pari e spesso superiori al software
 commerciale pi&ugrave; complesso.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Marchi registrati.</em>
  KDE<sup>&#174;</sup> ed il logo K Desktop Environment<sup>&#174;</sup> sono marchi
  registrati di KDE e.V.

  Linux &egrave; un marchio registrato di Linus Torvalds.

  UNIX &egrave; un marchio registrato di The Open Group negli USA ed in altri paesi.

  Tutti gli altri marchi registrati e copyright a cui si fa riferimento in questo annuncio sono
  di propriet&agrave; dei loro rispettivi proprietari.
  </font>
</p>

<hr />

<h4>Contatti per ulteriori informazioni</h4>
<table cellpadding="10"><tr valign="top">
<td>

<b>Africa</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Tel.: +264 - 61 - 24 92 49<br />
<a href="&#109;a&#105;l&#116;o:&#105;&#110;fo-&#0097;&#0102;r&#105;&#99;a&#x40;k&#100;e.&#111;&#x72;g">info-africa kde.org</a><br />
</td>

<td>
<b>Asia e India</b><br />
     Pradeepto Bhattacharya<br/>
     A-4 Sonal Coop. Hsg. Society<br/>
     Plot-4, Sector-3,<br/>
     New Panvel,<br/>
     Maharashtra.<br/>
     India 410206<br/>
     Tel.: +91-9821033168<br/>
<a href="ma&#0105;&#108;to&#00058;inf&#00111;-&#97;&#115;&#x69;a&#x40;kde.or&#x67;">info-asia kde.org</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europa</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Svezia<br />
Tel.: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="m&#x61;il&#0116;o&#x3a;i&#x6e;fo-&#00101;&#00117;rope&#64;k&#x64;&#x65;&#00046;o&#x72;&#00103;">info-europe kde.org</a>
</td>

<td>
<b>Nord America</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Tel.: (416)-925-4030 <br />
<a href="&#109;ai&#x6c;&#x74;&#x6f;&#0058;i&#x6e;&#0102;o&#0045;no&#0114;t&#104;&#0097;m&#x65;&#x72;i&#x63;&#x61;&#x40;k&#x64;e&#46;&#0111;&#x72;&#x67;">info-northamerica kde.org</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Tel.: (+61)402 346684<br />
<a href="&#109;&#x61;&#x69;&#x6c;&#x74;o:&#105;&#x6e;fo&#45;&#x6f;c&#101;&#x61;&#110;ia&#064;kde&#00046;org">info-oceania kde.org</a><br />
</td>

<td>
<b>Sud America</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brasile<br />
Tel.: +55(41)262-0782 / +55(41)360-2670<br />
<a href="ma&#105;&#x6c;&#116;&#x6f;&#x3a;&#0105;&#110;&#102;&#x6f;-&#00115;&#111;ut&#104;&#97;&#x6d;e&#0114;&#x69;ca&#0064;&#107;d&#x65;.&#111;r&#x67;">info-southamerica kde.org</a><br />
</td>

</tr></table>

<?php

  include("footer.inc");
?>
