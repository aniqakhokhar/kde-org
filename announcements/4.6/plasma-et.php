<?php

  $release = '4.6';
  $release_full = '4.6.0';
  $page_title = "Plasma töötsoonid annavad juhtohjad kasutaja kätte";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<h2>KDE laskis välja töötsoonid 4.6.0</h2>

<?php
    centerThumbScreenshot("46-w10.png", "Plasma töölaud 4.6.0-s");
?>

<p>
26. jaanuar 2011. KDE-l on rõõm teatada Plasma töölaua ja Plasma Netbooki uuest väljalaskest. Plasma töötsoonid on saanud nii seniste funktsioonide korralikku lihvi kui ka mitmeid olulisi uuendusi, mis süvendavad veelgi Plasma suunda semantilisema ja ülesandepõhisema töökorralduse poole.
</p>
<p>
<b>Tegevuste</b> süsteem on ümber kujundatud, et seda oleks lihtsam kasutada. Paremklõpsuga akna tiitliribale saab nüüd muuta rakendused ja failid vajaliku tegevuse osaks. Tegevust valides näitab Plasma töötsoon just seda, mida kasutaja seal näha tahab. Tegevuste lisamine, eemaldamine ja nende nime muutmine on samuti senisest mugavam.</li>
</p>

<?php
centerThumbScreenshot("46-w01.png", "Rakendusi on hõlpus tegevustega seostada");
?>
<p>
<b>Toitehaldus</b> on põhjalikult ümber töötatud, selle kood on nüüd kümme korda väiksem ja väga modulaarne, mis muudab hooldamise lihtsamaks ning võimaldab kergesti kirjutada pluginaid toitehaldusele uute võimaluste lisamiseks. Toitehalduse seadistustedialoog on nüüd paremini mõistetav ja kasutatav. Uus "reegliagent" võimaldab mitmesuguseid monitoriga seotud toiminguid, näiteks märguandeid või ekraani väljalülitamist filmi vaatamise ajal.
</p>
<?php
centerThumbScreenshot("46-w02.png", "Uus lihtsam toitehalduse liides");
?>

Aknahaldur <b>KWin</b> on varasemast oluliselt kiirem ja töötab paremini senisest rohkemate graafikadraiveritega tänu olulisele optimeerimisele, mis puhverdab akende seadistused ja värskendab ainult neid ekraani osi, mida on vaja värskendada.Sellega kaasneb:
<ul>
    <li>märkimisväärselt <a href="http://blog.martin-graesslin.com/blog/2010/10/optimization-in-kwin-4-6/">paranenud jõudlus</a>.</li>
    <li>graafikadraiverite omaduste parem tuvastamine.</li>
    <li>aktiivsete akende efekt – aknaid saab nüüd sulgeda otse ülevaatest.</li>
</ul>

<p>
Muud täiustused:
<ul>
    <li>rakenduste käivitamisaegsete märguannete tõhusam käitlemine.</li>
    <li>ümbertöötatud märguannete süsteem. Nüüd võib lasta märguannete hüpikakendel ilmuda just selles ekraaniosas, kus kasutaja seda soovib - piisab vaid, kui märguanne vajalikku kohta lohistada. Märguannete ajaloo ja tööde hüpikdialoogi välimus on klaarim ning allalaadimiste korral on võimalik edenemismärguannet laiendades näha allalaadimise kiirust ka graafiliselt.</li>
</ul>
</p>
<?php
centerThumbScreenshot("46-w04.png", "Märguandeid võib nüüd lohistada ükspuha kuhu töölaual");
?>
<p>
<ul>
    <li><a href="http://i158.photobucket.com/albums/t120/wdawn/sjelf-1.png">sahtlividin</a> - paneelil olles omandab automaatselt vajaliku suuruse ning näitab lugemata kirjade arvu ja võrgus olevaid kontakte.</li>
    <li>paneeli <b>digitaalkell</b> on oluliselt ümbertöötatud ning näitab nüüd varjutatud teksti, mis mitme tausta ja teema korral on tunduvalt loetavam.</li>
    <li><b>tegumiriba</b> toetab nüüd tähtsamaid käivitajaid: lohistage vaid programm tegumiribale ja sellest saab kiirkäivituskirje või ka võimalus panna rakendus tööle otse tegumiribal, milleks tuleb sellel teha paremklõps ja valida vastav valik menüüst.</li>
    <li><b>Plasma Netbooki</b> on jätkuvalt optimeeritud, nii et "otsimine ja käivitamine" peaks olema tunduvalt kiirem, samuti peaks ajalehetegevuse "esimene lehekülg" olema palju mugavam kasutada ka puuteekraaniga seadmetes.</li>
</ul>
</p>

<?php

centerThumbScreenshot("46-w07.png", "Plasma Netbooki otsing ja käivitamine on kiirem");
?>
<p>
Plasma töötsoonid on loetletule lisaks saanud veel palju täiustusi, mille hulgas võib ära mainida muudatused Oxygeni ikooniteemas ning KDE platvormi väliselt loodud rakenduste parema lõimimise tänu Oxygeni GTK teemale.

<?php
centerThumbScreenshot("46-w06.png", "Uus Oxygeni GTK teema aitab sujuvalt ühendada KDE-välised rakendused Plasma töötsooni");
?>
</p>

<h4>Plasma paigaldamine</h4>
<?php
  include("boilerplate-et.inc");
?>

<h2>Täna ilmusid veel:</h2>

<?php

include("trailer-applications-et.inc");
include("trailer-platform-et.inc");

include("footer.inc");

?>
