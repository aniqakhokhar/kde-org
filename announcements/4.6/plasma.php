<?php

  $release = '4.6';
  $release_full = '4.6.0';
  $page_title = "Plasma Workspaces Put You in Control";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<h2>KDE Releases Workspaces 4.6.0</h2>

<?php
    centerThumbScreenshot("46-w10.png", "Plasma Desktop in 4.6.0");
?>

<p>
January, 26th 2011. KDE is happy to announce the immediate availability of Plasma Desktop and Plasma Netbook 4.6. The Plasma Workspaces have seen both polish in existing functionality as well as the introduction of significant new innovations extending Plasma's capabilities further towards a more semantic, task-driven workflow.
</p>
<p>
The <b>Activities </b>system has been redesigned making it easier to take advantage of them. By right clicking to the window title, you can now make applications and files part of an activity. Changing to this activity, the Plasma workspace will show you what you need when you need it. The process for adding, renaming and removing Activities has also been improved.</li>
</p>

<?php
centerThumbScreenshot("46-w01.png", "It is easy to associate applications with activities");
?>
<p>
<b>Power management</b> has been revamped, with shorter and highly modular code (1/10th its previous size) which is easier to maintain and allows plugins to trigger power management features. Power settings are easier to understand and manage with the new configuration user interface. A new ‘Policy Agent’ allows screen inhibition features, such as notifications or the screen turning off while you're watching a movie
</p>
<?php
centerThumbScreenshot("46-w02.png", "The new simplified power management interface");
?>

<b>KWin</b>, the window and compositing manager, is faster than ever and works better with more graphics drivers, due to significant optimizations that cache window settings and only refresh the needed areas of the screen. This brings:
<ul>
    <li>Significant <a href="http://blog.martin-graesslin.com/blog/2010/10/optimization-in-kwin-4-6/">performance optimizations</a></li>
    <li>Better detection of graphic driver capabilities.</li>
    <li>Present Windows effect – Windows can now be closed directly from this overview.</li>
</ul>

<p>
Other improvements include:
<ul>
    <li>Program start-up notifications are now handled more efficiently.</li>
    <li>Revamped notifications system.  It is now possible to have the notification popup in any point of the screen by just dragging it, the notification history and jobs popup looks cleaner, and running downloads have gained a download speed plot when the progress notification is expanded.</li>
</ul>
</p>
<?php
centerThumbScreenshot("46-w04.png", "You can now drag notifications to a convenient place on the desktop");
?>
<p>
<ul>
    <li><a href="http://i158.photobucket.com/albums/t120/wdawn/sjelf-1.png">Shelf widget</a> - when in a panel: gained automatic sizing of the popup, shows the number of unread mails and online contacts</li>
    <li>The <b>digital clock</b> in the panel was revamped, and now features a shadowed text which improves readability with some backgrounds and themes.</li>
    <li>The <b>The taskbar</b> now has support for basic launchers: Just drag a program icon onto the task bar and it will become a quick launch entry, or pin a running app to the taskbar by right clicking it and using the new entry in the Advanced menu.</li>
    <li><b>Plasma Netbook</b> has been further optimized under the hood for a faster "Search and Launch" experience and more touch-friendly behaviour on the "Page One" newspaper activity.</li>
</ul>
</p>

<?php

centerThumbScreenshot("46-w07.png", "The Plasma Netbook Search and Launch is faster");
?>
<p>
A variety of other improvements apparent in the Plasma Workspaces include changes to the Oxygen icon theme and better integration of applications not built on the KDE Platform thanks to a completely rewritten Oxygen GTK theme.

<?php
centerThumbScreenshot("46-w06.png", "The new Oxygen GTK theme helps to integrate non-KDE applications in a Plasma workspace");
?>
</p>

<h4>Installing Plasma</h4>
<?php
  include("boilerplate.inc");
?>

<h2>Also Released Today:</h2>

<?php

include("trailer-applications.inc");
include("trailer-platform.inc");

include("footer.inc");

?>
