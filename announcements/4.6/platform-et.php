<?php

  $release = '4.6';
  $release_full = '4.6.0';
  $page_title = "Suund mobiilsusele kahandab KDE platvormi kaalu";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";

?>
<h2>KDE laskis välja arendusplatvormi 4.6.0</h2>
<p>
26. jaanuar 2011. KDE teatab uhkuse KDE platvormi 4.6 väljalaskest. Plasma töötsoonide ja KDE rakenduste alus on saanud olulisi parandusi paljudes valdkondades. Lisaks uue tehnoloogia pakkumisele on tublisti tööd tehtud platvormi põhialuste jõudluse parandamisel ja stabiilsuse tugevdamisel.
</p>
<h2>Vähenõudlik mobiilne profiil kahandab sõltuvusahelaid</h2>
<p>
KDE teekide jätkuva modulariseerimisega on nüüd võimalik kasutada mõningaid KDE platvormi osi mobiilsetes ja sardsüsteemides. Vähenenud teekidevahelised sõltuvused ja võimalus teatavaid omadusi keelata lubavad KDE raamistikke edaspidi hõlpsasti kasutada mobiilsetes seadmetes. Mobiilset profiili pruugivad juba mõned KDE rakenduste mobiiltelefonidele ja tahvelarvutitele mõeldud versioonid, näiteks <a href="http://userbase.kde.org/Kontact_Touch">Kontact Touch</a>, KDE kontoritöö pakett mobiilidele ning Plasma kasutajaliidesed <a href="http://www.notmart.org/index.php/Software/Plasma:_now_comes_in_tablets">tahvelarvutitele</a> ja pihuseadmetele.
</p>

<h2>Plasma jõu rakendamine QML-i teenistusse</h2>
<p>Uue väljalaskega toetab Plasma raamistik nüüd ka Qt Quicki deklaratiivses kasutajaliidese programmeerimiskeeles <b>QML</b> kirjutatud Plasma vidinaid. Olemasolevad vidinad töötavad endiselt edasi, kuid nüüdsest on just QML eelistatud uute vidinate loomise meetod. Plasma andmemootorid on saanud uusi omadusi, sealhulgas võime jagada faile JavaScripti plugina vahendusel ning salvestusteenuse, mis lubab andmemootoritel puhverdada andmeid, et neid saaks kasutada ka ajal, mil võrguühendus puudub. Loodud on uus Plasma KPart, mis muudab arendajatel lihtsamaks põimida nii mainitud uued kui ka mõistagi senised Plasma tehnoloogiad oma rakendustesse - juba praegu käib näiteks töö Plasma raamistiku tarvitamiseks Kontactis ja Skrooges.
</p>

<h2>UPoweri, UDevi ja UDisksi toetus, metaandmete varundamine</h2>
<p>
Solidi uute <b>UPoweri, UDevi ja UDisksi</b> taustaprogrammide tõttu ei ole enam Linuxis vajalik pruukida riistvara haldamiseks iganenud HAL-i. Rakendused ei vaja uute taustaprogrammide kasutamiseks mingeid muudatusi. Süsteemide jaoks, mis ei toeta UPowerit, on siiski endiselt saadaval ka HAL-i taustaprogramm.
</p>
<p>
KDE platvormi semantilise töölaua tehnoloogia <b>Nepomuk</b> toetab nüüd <a href="http://vhanda.in/blog/2010/11/nepomuk-backup/">varundamist ja sünkroonimist</a>, mis muudab metaandmete vahetamise seadmete vahel palju lihtsamaks. Kasutajad võivad oma semantilisi andmeid varundada graafilise liidese abil. Täiendavad sünkroonimisvõimalused on esialgu kasutatavad ainult käsurealt.
</p>

<?php
centerThumbScreenshot("46-p01.png", "Semantilisi andmeid saab nüüd hõlpsasti graafiliselt varundada ja taastada");
?>

<h2>KWin muutus skriptitavaks, Oxygen-GTK parandab töökeskkondade vahelist lõimimist</h2>
<p>
KDE aknahalduril <b>Kwin</b> on nüüd <a href="http://rohanprabhu.com/?p=116">skriptiliides</a>. See annab kogenud kasutajatele ja distributsioonide valmistajatele märksa suuremaid võimalusi määrata akende käitumist Plasma töötsoonis - nii võib näiteks lasta aknal olla alati teiste peal, kui see pole maksimeeritud, ja kohelda seda tavalise aknana, mida saab ka teise aknaga katta, kui see on maksimeeritud. Kui aken taas väiksemaks muudetakse, omandab ta automaatselt taas teiste peal olemise omaduse. KWini meeskond töötab praegu agaralt OpenGL-ES toetuse kallal, mis loodetavasti saab valmis 4.7 ilmumisajaks 2011. aasta suvel, andes siis KWinile võimaluse töötada ka mobiilsetes seadmetes.
</p>
<p>
KDE platvormi välimuse eest hoolitsevat <b>Oxygeni</b> on samuti palju edendatud, sealhulgas on täielikult muudetud kõiki MIME tüüpide ikoone ning võetud kasutusele <a href="http://hugo-kde.blogspot.com/2010/11/oxygen-gtk.html">Oxygen-GTK teemamootor</a>, mis võimaldab GTK rakenduste paremat lõimimist KDE Plasma töötsoonidesse, pakkudes ka näiteks üleminekuid ja veel mitmeid võimalusi, mida kasutajad Oxygeni puhul enesestmõistetavaks peavad.
</p>

<?php
centerThumbScreenshot("46-p02.png", "Oxygen-GTK teema lubab KDE ja GTK rakendustel teineteise moodi välja näha");
?>

<h2>Uus Bluetoothi raamistik laseb seadmetel paremini läbi saada</h2>
<p>
Tuliuus Bluetoothi raamistik <b>BlueDevil</b> võimaldab seadmetel üksteist paremini ära tunda ja neid mugavamalt hallata. BlueDevil asendab senise KBluetoothi ja toetub BlueZile. See võimaldab muu hulgas järgmist:
</p>
<ul>
    <li>seadmete paaritamiseks saab kasutada nõustajat,</li>
    <li>Bluetoothi seadmete faile võib sirvida Dolphinis või Konqueroris,</li>
    <li>Bluetoothi seadistustele pääseb ligi nii KDE Süsteemi seadistustes kui ka otse süsteemisalvest,</li>
    <li>uus raamistik jälgib ka saabuvaid soove, näiteks failide vastuvõtmiseks või PIN-i sisestamiseks.</li>
</ul>

<h4>KDE arendusplatvormi paigaldamine</h4>
<?php
  include("boilerplate-et.inc");
?>

<h2>Täna ilmusid veel:</h2>
<?php

include("trailer-plasma-et.inc");
include("trailer-applications-et.inc");

include("footer.inc");
?>
