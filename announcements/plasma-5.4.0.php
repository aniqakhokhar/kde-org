<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships Plasma 5.4.0, Feature Release for August");
  $site_root = "../";
  $release = 'plasma-5.4.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<style>
figure {text-align: center; float: right; margin: 0px;}
figure img {padding: 1ex; border: 0px; background-image: none;}
figure video {padding: 1ex; border: 0px; background-image: none;}
figcaption {font-style: italic;}
</style>

<figure style="float: none">
<a href="plasma-5.4/plasma-screen-desktop-2-shadow.png">
<img src="plasma-5.4/plasma-screen-desktop-2-shadow.png" style="border: 0px" width="600" height="380" alt="<?php i18n("Plasma 5.4");?>" />
</a>
<figcaption><?php i18n("Plasma 5.4");?></figcaption>
</figure>

<p>
<?php i18n("Tuesday, 25 August 2015. "); ?>
<?php i18n("Today KDE releases a feature release of the new version of Plasma 5.
");?>
</p>

<p>
<?php i18n("
This release of Plasma brings many nice touches for our users such as
much improved high DPI support, KRunner auto-completion and many new
beautiful Breeze icons.  It also lays the ground for the future with a
tech preview of Wayland session available.  We're shipping a few new
components such as an Audio Volume Plasma Widget, monitor
calibration tool and the User Manager tool comes out beta.
");?>
</p>

<br clear="all" />
<figure>
<a href="plasma-5.4/plasma-screen-audiovolume-shadows.png">
<img src="plasma-5.4/plasma-screen-audiovolume-shadows-wee.png" style="border: 0px" width="300" height="235" alt="<?php i18n("Audio Volume");?>" />
</a>
<figcaption><?php i18n("The new Audio Volume Applet");?></figcaption>
</figure>

<h3><?php i18n("New Audio Volume Applet") ?></h3>
<p>
<?php i18n("
Our new Audio Volume applet works directly with PulseAudio, the popular
sound server for Linux, to give you full control over volume and output settings in
a beautifully designed simple interface.
");
?>
</p>

<br clear="all" />
<figure>
<a href="plasma-5.4/plasma-screen-dashboard-2-shadow.png">
<img src="plasma-5.4/plasma-screen-dashboard-2-shadow-wee.png" style="border: 0px" width="300" height="190" alt="<?php i18n("Dashboard alternative launcher");?>" />
</a>
<figcaption><?php i18n("The new Dashboard alternative launcher");?></figcaption>
</figure>

<h3><?php i18n("Application Dashboard alternative launcher") ?></h3>
<ul>
<?php i18n("
Plasma 5.4 brings an entirely new fullscreen launcher Application Dashboard in
kdeplasma-addons: Featuring all features of Application Menu it includes sophisticated scaling to
screen size and full spatial keyboard navigation.

The new launcher allows you to easily and quickly find applications, as well as recently used or favorited documents and contacts based on your previous activity.
");
?>
</ul>

<br clear="all" />
<figure>
<a href="https://kver.files.wordpress.com/2015/07/image10430.png">
<img src="https://kver.files.wordpress.com/2015/07/image10430.png" style="border: 0px" width="300" height="210" alt="<?php i18n("New Icons");?>" />
</a>
<figcaption><?php i18n("Just some of the new icons in this release");?></figcaption>
</figure>


<h3><?php i18n("Artwork Galore") ?></h3>
<?php i18n("Plasma 5.4 brings over 1400 new icons covering not only all the KDE applications, but also providing Breeze themed artwork to apps such as Inkscape, Firefox and LibreOffice providing a more integrated, native feel."); ?>



<br clear="all" />
<figure>
<a href="plasma-5.4/plasma-screen-krunner-shadow.png">
<img src="plasma-5.4/plasma-screen-krunner-shadow-wee.png" style="border: 0px" width="300" height="210" alt="<?php i18n("KRunner");?>" />
</a>
<figcaption><?php i18n("KRunner");?></figcaption>
</figure>

<h3><?php i18n("KRunner history") ?></h3>
<ul>
<?php i18n("
KRunner now remembers your previous searches and automatically completes from the history as you type.
");
?>
</ul>

<br clear="all" />
<figure>
<a href="plasma-5.4/plasma-screen-nm-graph-shadow.png">
<img src="plasma-5.4/plasma-screen-nm-graph-shadow-wee.png" style="border: 0px" width="300" height="279" alt="<?php i18n("Networks Graphs");?>" />
</a>
<figcaption><?php i18n("Network Graphs");?></figcaption>
</figure>

<h3><?php i18n("Useful graphs in Networks applet") ?></h3>
<ul>
<?php i18n("
The Networks applet is now able to display network traffic graphs. It also supports two new VPN plugins for connecting over SSH or SSTP.");
?>
</ul>

<br clear="all" />
<h3><?php i18n("Wayland Technology Preview") ?></h3>
<ul>
<?php i18n("

With Plasma 5.4 the first technology preview of a Wayland session is
released. On systems with free graphics drivers it is possible to run
Plasma using KWin, Plasma's Wayland compositor and X11 window manager,
through <a href=\"https://en.wikipedia.org/wiki/Direct_Rendering_Manager\">kernel mode settings</a>. The
currently supported feature set is driven by the needs for the 
<a href=\"https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform\">
Plasma Mobile project</a>
and more desktop oriented features are not yet fully implemented. The
current state does not yet allow to use it as a replacement for Xorg
based desktop, but allows to easily test it, contribute and watch tear
free videos. Instructions on how to start Plasma on Wayland can be
found in the <a href='https://community.kde.org/KWin/Wayland#Start_a_Plasma_session_on_Wayland'>KWin wiki pages</a>.
Wayland support will improve in future releases with the aim to get to a stable release soon.
")?>
</ul>

<h3><?php i18n("Other changes and additions") ?></h3>
<ul>
<?php i18n("
<li>Much improved high DPI support</li>
<li>Smaller memory footprint</li>
<li>Our desktop search got new and much faster backend</li>
<li>Sticky notes adds drag &amp; drop support and keyboard navigation</li>
<li>Trash applet now works again with drag &amp; drop</li>
<li>System tray gains quicker configurability</li>
<li>The documentation has been reviewed and updated</li>
<li>Improved layout for Digital clock in slim panels</li>
<li>ISO date support in Digital clock</li>
<li>New easy way to switch 12h/24h clock format in Digital clock</li>
<li>Week numbers in the calendar</li>
<li>Any type of item can now be favorited in Application Menu (Kicker) from any view, adding support for document and Telepathy contact favorites</li>
<li>Telepathy contact favorites show the contact photo and a realtime presence status badge</li>
<li>Improved focus and activation handling between applets and containment on the desktop</li>
<li>Various small fixes in Folder View: Better default sizes, fixes for mouse interaction issues, text label wrapping</li>
<li>The Task Manager now tries harder to preserve the icon it derived for a launcher by default</li>
<li>It's possible to add launchers by dropping apps on the Task Manager again</li>
<li>It's now possible to configure what happens when middle-clicking a task button in the Task Manager: Nothing, window close, or launching a new instance of the same app</li>
<li>The Task Manager will now sort column-major if the user forces more than one row; many users expected and prefer this sorting as it causes less task button moves as windows come and go</li>
<li>Improved icon and margin scaling for task buttons in the Task Manager</li>
<li>Various small fixes in the Task Manager: Forcing columns in vertical instance now works, touch event handling now works on all systems, fixed a visual issue with the group expander arrow</li>
<li>Provided the Purpose framework tech preview is available, the QuickShare Plasmoid can be used, making it easy to share files on many web services.</li>
<li>Monitor configuration tool added</li>
<li>kwallet-pam is added to open your wallet on login</li>
<li>User Manager now syncs contacts to KConfig settings and the User Account module has gone away</li>
<li>Performance improvements to Application Menu (Kicker)</li>

<li>Various small fixes to Application Menu (Kicker): Hiding/unhiding
apps is more reliable, alignment fixes for top panels, 'Add to
Desktop' against a Folder View containment is more reliable,
better behavior in the KActivities-based Recent models</li>

<li>Support for custom menu layouts (through kmenuedit) and menu separator items in Application Menu (Kicker)</li>

<li>Folder View has improved mode when in panel (<a href=\"https://blogs.kde.org/2015/06/04/folder-view-panel-popups-are-list-views-again\">blog</a>)</li>
<li>Dropping a folder on the Desktop containment will now offer creating a Folder View again</li>

");
?>
</ul>

<br clear="all" />
<a href="plasma-5.3.2-5.4.0-changelog.php">
<?php i18n("Full Plasma 5.4 changelog");?></a>

<!-- // Boilerplate again -->

<h2><?php i18n("Live Images");?></h2>

<p><?php print i18n_var("
The easiest way to try it out is with a live image booted off a
USB disk. You can find a list of <a href='%1'>Live Images with Plasma 5</a> on the KDE Community Wiki.
", "https://community.kde.org/Plasma/LiveImages");?></p>

<h2><?php i18n("Package Downloads");?></h2>

<p><?php i18n("Distributions have created, or are in the process
of creating, packages listed on our wiki page.
");?></p>

<ul>
<li>
<?php print i18n_var("<a
href='https://community.kde.org/Plasma/Packages'>Package
download wiki page</a>"
, $release);?>
</li>
</ul>

<h2><?php i18n("Source Downloads");?></h2>

<p><?php i18n("You can install Plasma 5 directly from source. KDE's
community wiki has <a
href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>.
Note that Plasma 5 does not co-install with Plasma 4, you will need
to uninstall older versions or install into a separate prefix.
");?>
</p>

<ul>
<li>
<?php print i18n_var("
<a href='../info/%1.php'>Source Info Page</a>
", $release);?>
</li>
</ul>

<h2><?php i18n("Feedback");?></h2>

<?php print i18n_var("You can give us feedback and get updates on %1 or %2 or %3.", "<a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='facebook.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a>", "<a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='twitter.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a>", "<a href='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='googleplus.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>" );?>

<p>
<?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?></a>
</p>

<p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>#Plasma IRC channel</a>,
<a href='%2'>Plasma-devel mailing list</a> or report issues via
<a href='%3'>bugzilla</a>.  If you like what the
team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided");?></p>

<p><?php i18n("Your feedback is greatly appreciated.");?></p>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
