<?php

  include_once ("functions.inc");
  $translation_file = "kde-org";
  $release = 'frameworks5TP';
  $page_title = i18n_noop("Frameworks 5 Technology Preview");
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<?php
  include "../announce-i18n-bar.inc";
?>


<p>
<img src="images/KDE_QT.jpg" style="float: right; border: 0; background-image: none; margin-top: -80px;" alt="<?php i18n("Collaboration between Qt and KDE");?>" />
<font size="+1">
<?php i18n("January 7, 2014. The KDE Community is proud to announce a Tech Preview of KDE Frameworks 5. Frameworks 5 is the result of almost three years of work to plan, modularize, review and port the set of libraries previously known as KDElibs or KDE Platform 4 into a set of Qt Addons, separate libraries with well-defined dependencies and abilities, ready for Qt 5. This gives the Qt ecosystem a powerful set of drop-in libraries providing additional functionality for a wide variety of tasks and platforms, based on over 15 years of KDE experience in building applications. Today, all the Frameworks are available in Tech Preview mode; a final release is planned for the first half of 2014. Some Tech Preview addons (notably KArchive and Threadweaver) are more mature than others at this time.");?>
</font>
</p>
<h2><?php i18n("What is Frameworks 5?");?></h2>
<p>
<?php i18n("The KDE libraries are currently the common code base for (almost) all KDE applications. They provide high-level functionality such as toolbars and menus, spell checking and file access. Currently, 'kdelibs' is distributed as a single set of interconnected libraries. Through KDE Frameworks efforts, these libraries have been methodically reworked into a set of independent, cross platform libraries that will be readily available to all Qt developers.");?>
</p>
<p><?php i18n("The KDE Frameworks—designed as drop-in Qt Addons—will enrich Qt as a development environment with libraries providing functions that simplify, accelerate and reduce the cost of Qt development. Frameworks eliminate the need to reinvent key functionality.");?>
</p>



<p><?php i18n("The transition from Platform to Frameworks has been underway for almost three years and is being implemented by a team of about 20 (paid and volunteer) developers and actively supported by four companies. Frameworks 5 consists of 57 modules: 19 independent libraries (Qt Addons) not requiring any dependencies; 9 that require libraries which themselves are independent; and 29 with more significant dependency chains. Frameworks are developed following the <a href='http://community.kde.org/Frameworks/Policies'>Frameworks Policies</a>, in a vendor neutral, open process.");?>
</p>
<p><?php i18n("
<a href='http://dot.kde.org/2013/09/25/frameworks-5'>This KDE News article</a> has more background on Frameworks 5.");?>
</p>
<?php i18n("<h2>Available today</h2>");?>
<p><?php i18n("The tech preview made available today contains all 57 modules that are part of Frameworks 5. Of these, two have a maturity level that shows the direction of Frameworks: ThreadWeaver and KArchive. Developers are invited to take all of the modules for a spin and provide feedback (and patches) to help bring them to the same level of maturity.");?>
</p>

<p><?php i18n("KArchive offers support for many popular compression codecs in a self-contained, featureful and easy-to-use file archiving and extracting library. Just feed it files; there is no need to reinvent an archiving function in your Qt-based application! ThreadWeaver offers a high-level API to manage threads using job- and queue-based interfaces. It allows easy scheduling of thread execution by specifying dependencies between the threads and executing them while satisfying these dependencies, greatly simplifying the use of multiple threads. These are available for production use now.");?>
</p>

<p><?php i18n("There is a <a href='http://community.kde.org/Frameworks/List'>full list of the Frameworks</a>; tarballs with the current code can be <a href='http://download.kde.org/unstable/frameworks/4.95.0/'>downloaded</a>. <a href='http://community.kde.org/Frameworks/Binary_Packages'>Binaries</a> are available as well.");?>
</p>

<a href="http://dot.kde.org/sites/dot.kde.org/files/kf5_big_0.png"><img src="images/kf5_small.png" style="float: none; border: 0; background-image: none;" alt="<?php i18n("Overview of the KDE Frameworks (a work in progress!)");?>" /></a>

<p><?php i18n("The team is currently working on providing a detailed listing of all Frameworks and third party libraries at <a href='http://inqlude.org'>inqlude.org</a>, the curated archive of Qt libraries. Each entry includes a dependency tree view. Dependency diagrams can also be found <a href='http://agateau.com/tmp/kf5/'>here</a>.");?>
</p>

<?php i18n("<h2>Working towards a final release</h2>");?>
<p><?php i18n("The team will do monthly releases with a beta planned for the first week of April and a final release in the beginning of June.");?>
</p>

<p><?php i18n("Plans for this period include tidying up the infrastructure, integration with QMake and pkg-config for non-CMake users, getting CMake contributions upstream, and a final round of API cleanups and reviews. Frameworks 5 will be open for API changes until the beta in April.");?>
</p>

<p><?php i18n("Those interested in following progress can check out the <a href='https://projects.kde.org/projects/frameworks'>git repositories</a>, follow the discussions on the <a href='https://mail.kde.org/mailman/listinfo/kde-frameworks-devel'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='https://git.reviewboard.kde.org/groups/kdeframeworks/'>review board</a>. Policies and the current state of the project and plans are available at the <a href='http://community.kde.org/Frameworks'>Frameworks wiki</a>. Real-time discussions take place on the <a href='irc://#kde-devel@freenode.net'>#kde-devel IRC channel on freenode.net</a>.");?>
</p>

<h2><?php i18n("Discuss, Spread the Word and See What's Happening: Tag as &quot;KDE&quot;");?></h2>
<p>
<?php i18n("KDE encourages people to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with &quot;KDE&quot;. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for these releases of KDE software.");?>
</p>
<p>
<?php i18n("You can discuss this news story <a href='http://dot.kde.org/2014/01/07/frameworks-5-tech-preview'>on the Dot</a>, KDE's news site.");?>
</p>
<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/frameworks5TP/&amp;title=KDE%20releases%20Frameworks%205%20Technology%20Preview" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="https://www.kde.org/announcements/frameworks5TP/" data-text="#KDE releases Frameworks 5 Technology Preview →" data-via="kdecommunity" data-hashtags="linux,opensource">Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/frameworks5TP/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2Fframeworks5TP%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="https://www.kde.org/announcements/frameworks5TP/"></g:plusone>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://www.flickr.com/photos/tags/kdef5tp"><img src="buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kdef5tp"><img src="buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kdef5tp"><img src="buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>

<h4><?php i18n("Support KDE");?></h4>


<a href="http://jointhegame.kde.org/"><img src="images/join-the-game.png" width="231" height="120"
alt="<?php i18n("Join the Game");?>" align="left"/> </a>
<p align="justify"> <?php i18n("KDE e.V.'s new <a
href='http://jointhegame.kde.org/'>Supporting Member program</a> is
now open.  For &euro;25 a quarter you can ensure the international
community of KDE continues to grow making world class Free
Software.");?></p>
<br clear="all" />
<p>&nbsp;</p>

<?php
  include("footer.inc");
?>
