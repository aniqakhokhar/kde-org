<?php
  $page_title = "aKademy 2006 Schedule Finalized";
  $site_root = "../";
  include "header.inc";
?>



<p>DATELINE September 18, 2006</p>
<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
KDE Developers and Contributors Gather in Dublin to Work on KDE 4
and Desktop Standards
</h3>

<p align="justify">
<strong>
September 18, 2006 (The Internet)  More than 200 members of the KDE
community, related projects, industry partners, and interested users
will be gathering Friday for aKademy 2006 at the 
<a href="http://www.tcd.ie/">Trinity College Dublin</a>
to work on the next-generation desktop, KDE 4, and desktop standards. 
The <a href="http://conference2006.kde.org">aKademy conference</a> 
will run through Saturday, September 23, through Saturday, September 30.
</strong>
</p>

<p align="justify">
Attendees will begin arriving on Friday, September 22. The 
<a href="http://conference2006.kde.org/conference/program.php">KDE
Contributors Conference</a> will run from Saturday, September 23 to Sunday,
September 24. The conference will feature tracks on cross-desktop
development, KDE 4 planning, programming, community building, and
an Asia track to help build bridges to Asian communities. 
</p>

<p align="justify">
<a href="http://conference2006.kde.org/conference/speakers.php">Speakers 
for the two days</a> of the conference include Aaron
J. Seigo, Waldo Bastian, John Cherry, Keith Packard, David Faure,
Adriaan de Groot, Jens Herden, and many other active contributors
to KDE and other open source community projects. The conference will
wrap up Sunday with the aKademy Award Ceremony and closing talk.
</p>

<p align="justify">
Monday, September 25, is the 
<a href="http://conference2006.kde.org/codingmarathon/ev-meeting.php">KDE 
e.V. General Assembly Day</a> for
members of KDE e.V. only. The KDE e.V. is a non-profit organization
representing and supporting the KDE project in financial and legal
matters.
</p>

<p align="justify">
Tuesday, September 26, is the 
<a href="http://conference2006.kde.org/codingmarathon/opendocumentday.php">
OpenDocument Day at aKademy</a>. The event
is open to all developers interested in working on the OpenDocument
Format (ODF). Topics will include interoperability between ODF
implementations, new uses for ODF, libraries and tools for ODF,
and extensions for ODF.
</p>

<p align="justify">
The aKademy conference will also feature 
<a href="http://conference2006.kde.org/codingmarathon/hciday.php">Human Computer Interaction
(HCI) Day</a> on Wednesday, September 27. This event is organized
by the Human Computer Interaction Working Group, formed at last
year's aKademy. HCI Day will focus on accessibility, usability,
internationalization, the Human Interface Guidelines (HIG), artwork,
and documentation to create a better and more usable KDE interface
for all users.
</p>

<p align="justify">
aKademy will also feature a coding marathon, to run daily from Tuesday,
September 26, through Saturday, September 30. 
<a href="http://conference2006.kde.org/codingmarathon/bof.php">Birds of a Feather
(BoF) sessions</a> will also be scheduled daily from Tuesday through
Saturday. The preliminary Birds of a Feather sessions already scheduled
include BoFs for Kubuntu, KDE PIM, Collaboration and Web Services,
Marketing KDE, and Regional KDE Groups. BoF slots are still open,
and may be scheduled through <a href="akademy-team@kde.org">Cornelius Schumacher</a>.
</p>

<p align="justify">
aKademy 2006 is organized by a local team of volunteers headed by
Marcus Furlong, the KDE e.V., and other members of the KDE community.
</p>

<p align="justify">
aKademy is made possible by a number of 
<a href="http://conference2006.kde.org/sponsors/">sponsors</a>: The host sponsor 
is the School of Computer Science and Statistics, Trinity College Dublin.  
Kubuntu and OSDL are this year's Gold Sponsors. Intel, Klarälvdalens 
Datakonsult AB, NLNet, Trolltech, Xandros, Ricoh, and Nokia are this
year's Silver Sponsors. Enterprise Ireland, Google, Staikos Computing,
and Novell are Bronze Sponsors. Linux Magazine, O'Reilly, and
RealNetworks are the Media Sponsors, and aKademy 2006 is also sponsored
by Tight Poker.
</p>

<p align="justify">
<h4>
  Supporting KDE
</h4>
<p align="justify">
KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a> 
project that exists and grows only because of the
help of many volunteers that donate their time and effort. KDE
is always looking for new volunteers and contributions, whether its
help with coding, bug fixing or reporting, writing documentation,
translations, promotion, money, etc. All contributions are gratefully
appreciated and eagerly accepted. Please read through the 
<a href="/community/donations/">Supporting KDE page</a> for further information. </p>

<p align="justify">
We look forward to hearing from you soon!
</p>

<h4>
  About KDE
</h4>
<p align="justify">
  KDE is an <a href="/awards/">award-winning</a>, independent 
  <a href="/people/">project of hundreds</a> of developers, 
  translators, artists and other professionals worldwide collaborating over 
  the Internet to create and freely distribute a sophisticated, customizable 
  and stable desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform.</p>

<p align="justify">
  KDE provides a stable, mature desktop including a state-of-the-art browser
  (<a href="http://konqueror.kde.org/">Konqueror</a>), a personal information
  management suite (<a href="http://kontact.org/">Kontact</a>), a full 
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking application and utilities, and an
  efficient, intuitive development environment featuring the excellent IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.</p>

<p align="justify">
  KDE is working proof
  that the Open Source "Bazaar-style" software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software.
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<p align="justify">
  <font size="2">
  <em>Trademark Notices.</em>
  KDE<sup>&#174;</sup> and the K Desktop Environment<sup>&#174;</sup> logo are 
  registered trademarks of KDE e.V.

  Linux is a registered trademark of Linus Torvalds.

  UNIX is a registered trademark of The Open Group in the United States and
  other countries.

  All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.
  </font>
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<h4>Press Contacts</h4>
<p>
If you have a press enquiry related to aKademy, please direct it to the
aKademy team: <a href="mailto:akademy-team@kde.org">akademy-team@kde.org</a>.
Alternatively, you can contact the following representatives:
KDE project: <a href="mailto:kde-ev-board@kde.org">KDE e.V. Board</a>
Head of local organizing team: <a href="mailto:furlongm@cs.tcd.ie">Marcus 
Furlong</a>.
</p>
<?php

  include("footer.inc");
?>