<?php
  $page_title = "KDE 4.1 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

Also available in:
<?php
  $release = '4.1';
  include "../announce-i18n-bar.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
Społeczność KDE ogłasza wydanie KDE 4.1.0
</h3>

<p align="justify">
  <strong>
    KDE wydaje kolejną wersję środowiska graficznego dedykowaną pamięci Uwe Thiem
  </strong>
</p>

<p align="justify">
29 lipca 2008.
<a href="http://www.kde.org/">Społeczność KDE</a> wydała dzisiaj KDE 4.1.0. Wydanie to jest drugim głównym wydaniem KDE z serii 4, prezentującym nowe aplikacje i funkcje korzystające z filarów KDE 4. KDE 4.1 jest też pierwszym wydaniem zawierającym Pakiet Zarządzania Informacjami Osobistymi KDE-PIM składający się z klienta email KMail, organizera KOrganizer, czytnika RSSów Akregator, czytnika grup dyskusyjnych KNode i innych komponentów zgrupowanych w aplikacji Kontact. Nowy interfejs pulpitu - Plasma, wprowadzony w KDE 4.0 dojrzał już do tego stopnia, że może zastąpić interfejs pulpitu znany z KDE 3. Tak jak w przypadku poprzednich wydań wiele czasu poświęcono na poprawki w bibliotekach na których opiera się KDE.
<br />
Dirk M&uuml;ller, jeden z menadżerów wydań KDE mówi:<em>"Pomiędzy KDE 4.0 a 4.1 dokonano 20803 commitów oraz 15432 poprawek i zmian w tłumaczeniach. Prawie 35000 commitów dokonano także w osobnych gałęziach, a potem część z nich została przeniesiona do KDE 4.1."</em>
M&uuml;ller dodaje też, że administratorzy serwerów utworzyli na serwerach KDE SVN 166 nowych kont dla programistów.a

<?php
    screenshot("desktop_thumb.png", "desktop.png", "center",
"Pulpit KDE 4.1", "405");
?>

</p>
<p>
<strong>Kluczowe zmiany w KDE 4.1 to:</strong>
<ul>
    <li>przywrócenie pakietu KDE-PIM,</li>
    <li>dojrzałość Plasmy,</li>
    <li>wiele nowych aplikacji.</li>
</ul>

</p>


<h3>
  <a name="changes">Pamięci Uwe Thiem</a>
</h3>
<p align="justify">
Społeczność KDE dedykuje to wydanie Uwe Thiem, długotrwałemu programiście KDE, który niedawno zmarł z powodu wady nerki. Śmierć Uwe przyszła zupełnie niespodziewanie i była szokiem dla jego przyjaciół. Uwe pracował nad projektem KDE do końca, nie tylko jako programista. Odgrywał również ważną rolę w edukacji użytkowników Afryki w temacie Wolnego Oprogramowania. Razem z nagłą śmiercią Uwe, projekt KDE stracił nieocenionego przyjaciela. Łączymy się w bólu z jego rodziną i tymi, których opuścił.
</p>


<h3>
  <a name="changes">Przeszłość, teraźniejszość, przyszłość</a>
</h3>
<p align="justify">
Mimo, że KDE 4.1 jest pierwszym wydaniem przeznaczonym dla szerszego grona użytkowników, nadal brakuje mu niektórych funkcji znanych z serii 3.5. Ekipa KDE ciągle jednak nad tym pracuje i stara się, aby były one dostępne w kolejnych wydaniach. Nie ma gwarancji, że pojawi się każda pojedyncza funkcja z KDE 3.5, ale KDE 4.1 już teraz jest potężnym środowiskiem.<br />
Część opcji w interfejsie została przeniesiona w inne miejsca, tak aby pasowała do danych na jakich operuje, wiec przed zgłoszeniem brakującej funkcji należy się upewnić, czy szukamy jej w dobrym miejscu.<p />
KDE 4.1 jest dużym krokiem w rozwoju KDE 4 i tworzy solidne fundamenty pod kolejne wydania. KDE 4.2 można oczekiwać w styczniu 2009.
</p>


<?php
    screenshot("kdepim-screenie_thumb.png", "kdepim-screenie.png", "center",
"Pakiet KDE PIM powrócił", "305");
?>

<h3>
  <a name="changes">Zmiany</a>
</h3>
<p align="justify">
Podczas stabilizacji nowych frameworków w KDE 4.1, dużo nacisku położono na elementy widoczne dla użytkowników. Poniżej znajduje się lista zmian w dokonanych w KDE 4.1. Więcej informacji można znaleźć na stronach <a href="http://techbase.kde.org/Schedules/KDE4/4.1_Release_Goals">KDE 4.1 Release Goals</a> i <a href="http://techbase.kde.org/Schedules/KDE4/4.1_Feature_Plan">KDE 4.1 Feature Plan</a>.
</p>

<h4>
  Dla użytkowników
</h4>
<p align="justify">

<ul>
    <li>
        W wydaniu 4.1 powrócił pakiet <strong>KDE-PIM</strong> zawierający programy do zarządzania informacjami osobistymi i komunikacji. KMail - klient email, organizer KOrganizer i Akregator - czytnik kanałów RSS powróciły z nowym wyglądem i funkcjonalnością,
    </li>
    <li>
        Na scenę wkroczył także <strong>Dragon Player</strong> - odtwarzacz wideo,
    </li>
    <li>
        <strong>Okteta</strong> to nowy zaawansowany hexedytor,
    </li>
    <li>
        <strong>Step</strong> symulator procesów fizycznych czyni naukę łatwą i przyjemną,
    </li>
    <li>
        <strong>KSystemLog</strong> pomaga w śledzeniu tego, co dzieje się w systemie,
    </li>
    <li>
        <strong>Nowe gry</strong> takie jak KDiamond, Kollision, KBreakOut i Kubrick sprawią, że nie oprzesz się pokusie zrobienia sobie przerwy w pracy,
    </li>
    <li>
        <strong>Lokalize</strong> sprawi, że tłumacze będą w stanie przetłumczyć KDE 4 na twój język (jeśli jeszcze nie ma go na liście ponad 50 dostępnych języków),
    </li>
    <li>
        <strong>KSCD</strong> - odtwarzacz CD znowu jest dostępny w KDE.
    </li>
</ul>

Odpowiedzi na najczęściej zadawane pytania znalazły się w <a href="http://www.kde.org/users/faq.php">FAQ dla użytkowników</a>, które warto przeczytać, jeśli chcesz się dowiedzieć się więcej na temat KDE 4.
</p>

<p align="justify">
<?php
    screenshot("dolphin-screenie_thumb.png", "dolphin-screenie.png", "center",
"Nowy mechanizm zaznaczania w Dolphinie", "483");
?>

<ul>
    <li>
        <strong>Dolphin</strong> - menadżer plików KDE otrzymał widok drzewa oraz obsługę kart. Nowy i innowacyjny system zaznaczania elementów za pomocą jednego kliknięcia zwiększa wygodę pracy, a akcje menu kontekstowego "Kopiuj do" oraz "Przenieś do" ułatwiają dostęp do tych funkcji. Konqueror oczywiście nadal jest dostępny jako alternatywa dla Dolphina i ma zaimplementowanych większość z tych funkcji. [<a href="#screenshots-dolphin">Zrzuty ekranu.</a>]
    </li>
    <li>
        <strong>Konqueror</strong> - przeglądarka internetowa dla KDE uzyskała obsługę przywracania zamkniętych okien i kart. Poprawie uległo także przewijanie stron, które jest teraz bardziej "gładkie".
    </li>
    <li>
        <strong>Gwenview</strong> Gwenview - przeglądarka zdjęć otrzymała nowy widok pełnoekranowy, pasek miniatur, służący do łatwego poruszania się pomiędzy zdjęciami, inteligentny system cofania oraz możliwość oceniania zdjęć. [<a href="#screenshots-gwenview">Zrzuty ekranu.</a>]
    </li>
    <li>
        <strong>KRDC</strong>  - klient zdalnego pulpitu od teraz może automatycznie znajdować zdalne pulpity w sieci lokalnej dzięki wykorzystaniu protokołu ZeroConf.
    </li>

    <li>
        <strong>Marble</strong> - globus dla KDE integruje się teraz z serwisem <a
        href="http://www.openstreetmap.org/">OpenStreetMap</a>, dzięki czemu zawsze odnajdziesz drogę wykorzystując Wolne Mapy. [<a href="#screenshots-marble">Zrzuty ekranu.</a>]
    </li>
    <li>
        <strong>KSysGuard</strong> umożliwia monitorowanie wyjścia procesów lub działających aplikacji, dzięki czemu nie trzeba ponownie uruchamiać aplikacji z poziomu konsoli, aby zobaczyć, co aplikacja robi.
    </li>
    <li>
        Efekty graficzne w <strong>KWin</strong> zostały ustabilizowane i rozszerzone. Dodano także efekty takie jak Coverswitch window switcher oraz słynne "wobbly windows". [<a href="#screenshots-kwin">Zrzuty ekranu.</a>]
    </li>
    <li>
        Rozszerzono możliwości konfiguracyjne panelu <strong>Plasmy</strong>. Nowy kontroler panelu pozwala na łatwe dostosowanie jego wyglądu do swoich potrzeb. Można także dodawać panele na wszystkich krawędziach ekranu. Nowy aplet Folderview pozwala na przechowywanie plików na pulpicie (tak w zasadzie to umożliwia wyświetlenie na pulpicie zawartości dowolnego katalogu). Na pulpicie można umieścić kilka apletów folderview, uzyskując łatwy i szybki dostęp do potrzebnych plików. [<a href="#screenshots-plasma">Zrzuty ekranu.</a>]
    </li>
</ul>
</p>


<h4>
  Dla programistów
</h4>
<p align="justify">

<ul>
    <li>
        Biblioteka <strong>Akonadi</strong> PIM umożliwia łatwe przechowywanie i wymianę emaili i informacji na temat kontaktów pomiędzy aplikacjami. <a href="http://en.wikipedia.org/wiki/Akonadi">Akonadi</a> obsługuje wyszukiwanie danych oraz powiadamianie aplikacji, jeśli dane ulegną zmianie.
    </li>
    <li>
        Aplikacje KDE mogą być pisane w Pythonie i Rubym. <strong>Interfejsy tych języków</strong> zostały <a href="http://techbase.kde.org/Development/Languages">określone</a> jako stabilne,dojrzale i dogodne dla programistów aplikacji.
    </li>
    <li>
        <strong>Libksane</strong> umożliwia łatwą obsługę skanera w aplikacjach takich jak Skanlite.
    </li>
    <li>
        Powstała ujednolicona obsługa <strong>emotikon</strong>, wykorzystywana jak na razie przez KMail i Kopete.
    </li>
    <li>
        Silniki dla <strong>Phonona</strong> do obsługi multimediów poprzez GStreamer, QuickTime i DirectShow9 poprawiają działanie KDE na systemach Windows i Mac OS.
    </li>

</ul>
</p>

<h3>
  Nowe platformy
</h3>
<p align="justify">
<ul>
    <li>
        Działanie KDE na <a href="http://techbase.kde.org/Projects/KDE_on_Solaris"><strong>OpenSolarisie</strong></a> poprawiło się. Pozostało jednak jeszcze kilka błędów.
    </li>
    <li>
        Dostępne są już pierwsze wersje <a href="http://windows.kde.org">aplikacji KDE dla platformy <strong>Windows</strong></a>. Biblioteki są już dość stabilne, jednak pełna funkcjonalność kdelibs nie jest jeszcze dostępna. Niektóre aplikacje działają na Windowsie całkiem ładnie, inne mogą jeszcze nie działać wcale.
    </li>
    <li>
        <strong>Mac OS X</strong> jest kolejną platformą na którą wkracza KDE. <a href="http://mac.kde.org">KDE na Macach</a> nie nadaje się jeszcze do normalnej pracy. Obsługa multimediów dzięki Phononowi jest już dostępna, ale pełna obsługa sprzętu i wyszukiwania nie jest jeszcze ukończona.
    </li>
</ul>
</p>


<a name="screenshots-dolphin"></a>
<h3>
  Zrzuty ekranu
</h3>
<p align="justify">

<a name="screenshots-dolphin"></a>
<h4>Dolphin</h4>
<?php

    screenshot("dolphin-treeview_thumb.png", "dolphin-treeview.png", "center",
               "Nowy widok drzewa w Dolphinie daje Ci szybszy dostęp do katalogów. Opcja ta jest wyłączona w ustawieniach domyślnych.", "448");

    screenshot("dolphin-tagging_thumb.png", "dolphin-tagging.png", "center",
               "Nepomuk wprowadza tagowanie i ocenianie do KDE - a więc też i do Dolphina.", "337");

    screenshot("dolphin-icons_thumb.png", "dolphin-icons.png", "center",
               "Podgląd i panel informacyjny dostarczają inforamcji o pliku.", "390");

    screenshot("dolphin-filterbar_thumb.png", "dolphin-filterbar.png", "center",
               "Łatwiej odnajduj swoje pliki dzięki filtrowaniu.", "372");
?>

<a name="screenshots-gwenview"></a>
<h4>Gwenview</h4>
<?php

    screenshot("gwenview-browse_thumb.png", "gwenview-browse.png", "center",
               "Gwenview umożliwia przeglądanie katalogów zawierających obrazy.", "440");

    screenshot("gwenview-open_thumb.png", "gwenview-open.png", "center",
               "Otwieranie plików z dysku twardego czy sieci jest tak samo łatwe dzięki infrastrukturze KDE.", "439");

    screenshot("gwenview-thumbnailbar_thumb.png", "gwenview-thumbnailbar.png", "center",
               "Nowy pasek z miniaturkami pozwala na łatwe przełączanie się między obrazami. Jest on również dostępny w trybie pełnoekranowym.", "684");

    screenshot("gwenview-sidebar_thumb.png", "gwenview-sidebar.png", "center",
               "Boczny panel Gwenview pokazuje dodatkowe informacje o obrazie oraz daje dostęp do opcji manipulacyjnych.", "462");

?>

<a name="screenshots-marble"></a>
<h4>Marble</h4>

<?php

    screenshot("marble-globe_thumb.png", "marble-globe.png", "center",
               "Globus w aplikacji Marble.", "427");

    screenshot("marble-osm_thumb.png", "marble-osm.png", "center",
               "Integracja Marble z OpenStreetMap dostarcza również informacji o transporcie publicznym.", "425");


?>
<a name="screenshots-kwin"></a>
<h4>KWin</h4>

<?php

    screenshot("kwin-desktopgrid_thumb.png", "kwin-desktopgrid.png", "center",
               "Siatka pulpitów KWin ukazuje wirtualne pulpity i ułatwia zapamiętanie gdzie mamy otwarte interesujące nas okno.", "405");

    screenshot("kwin-coverswitch_thumb.png", "kwin-coverswitch.png", "center",
               "Coverswitcher sprawia, że przełączanie się między oknami za pomocą Alt+Tab jest miłe dla oka. Można je wybrać w ustawieniach efektów pulpitu.", "405");

    screenshot("kwin-wobbly_thumb.png", "kwin-wobbly.png", "center",
               "KWin obsługuje również efekt gumowych okien (wobbly windows, domyślnie wyłączone).", "405");


?>

<a name="screenshots-plasma"></a>
<h4>Plasma</h4>
<?php

    screenshot("plasma-folderview_thumb.png", "plasma-folderview.png", "center",
               "Nowy aplet Widoku Folderu wyświetla zawartość dowolnego katalogu na Twoim pulpicie. Upuść katalog na odblokowanym pulpicie, aby stworzyć nowy aplet. Możesz nie tylko wyświetlać lokalne foldery, ale również zasoby sieciowe.", "405");

    screenshot("panel-controller_thumb.png", "panel-controller.png", "center",
               "Nowe funkcje ustawień paneli pozwalają na łatwe zmienianie ich rozmiaru i położenia. Możesz również zmienić pozycje apletów na panelu poprzez przeciąganie ich na nowe położenie.", "116");

    screenshot("krunner-screenie_thumb.png", "krunner-screenie.png", "center",
               "Dzięki KRunner możesz uruchamiać programy, pisać bezpośrednio maile do przyjaciół oraz wykonywać wiele innych drobnych zadań.", "238");

    screenshot("plasma-kickoff_thumb.png", "plasma-kickoff.png", "center",
               "Menu Kickoff zostało odświeżone.", "405");

    screenshot("switch-menu_thumb.png", "switch-menu_thumb.png", "center",
               "Możesz wybrać pomiędzy menu Kickoff, a klasycznym menu.", "344");
?>

</p>

<h4>
  Znane problemy
</h4>
<p align="justify">
<ul>
    <li>Użytkownicy kart <strong>NVidia</strong> i sterowników binarnych, przygotowanych przez NVidia mogą mieć problemy przy przełączaniu między oknami i przy zmianie ich rozmiarów. Powiadomiliśmy o tym fakcie inżynierów NVidia, jednak do tej pory nie pojawiły się jeszcze sterowniki naprawiające tą usterkę. Na <a href="http://techbase.kde.org/User:Lemma/GPU-Performance">Techbase</a> możecie znaleźć informacje jak poprawić wydajność w takim wypadku, mimo to ostatecznie musimy poczekać na poprawione sterowniki do kart.</li>
</ul>

</p>



<!-- ==================================================================================== -->
<h4>
  Pobierz, uruchom, przetestuj
</h4>
<p align="justify">
  Społeczność użytkowników oraz dostawcy systemów Linux/UNIX przygotowali już binarne pakiety instalacyjne dla części dystrybucji, systemu Mac OS X i Windows. Sprawdź, w swoim zarządcy pakietów, czy są one już dostępne.
</p>
<h4>
  Kompilacja KDE 4.1.0
</h4>
<p align="justify">
  <a name="source_code"></a><em>Kod źródłowy</em>.
  Kod źródłowy KDE 4.1.0 jest w <a
  href="http://www.kde.org/info/4.1.0.php">całości dostępny</a>. Instrukcje dotyczące kompilacji KDE można znaleźć w serwisie <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Kontakt dla prasy</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
