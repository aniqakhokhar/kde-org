<?php
  $page_title = "Najava izida KDE 4.1";
  $site_root = "../";
  include "header.inc";
?>

Na voljo tudi v drugih jezikih:
<?php
  $release = '4.1';
  include "../announce-i18n-bar.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
Skupnost KDE najavlja izid KDE 4.1.0
</h3>

<p align="justify">
  <strong>
    KDE je izdal izboljšano namizno okolje in programe. Izid je posvečen Uweju Thiemu.
  </strong>
</p>

<p align="justify">
29. julij 2008. <a href="http://www.kde.org/">Skupnost KDE</a> je danes izdala KDE 4.1.0. To je druga izdaja iz serije KDE 4 in prinaša nove funkcije in nove programe, ki so osnovani na temeljnih tehnologijah KDE-ja 4. KDE 4.1 je prva izdaja iz serije KDE 4, ki vsebuje paket programov za upravljanje z zasebnimi podatki KDE PIM. V paketu so odjemalec za e-pošto KMail, organizator KOrganizer, bralnik virov RSS Akregator, bralnik novičarskih skupin KNode in mnoge druge komponente, ki so združene v skupnem vmesniku Kontact. Novo namizje Plazma, prvič predstavljeno v KDE 4.0, se je razvilo do te mere, ko lahko za večino uporabnikov nadomesti namizje iz KDE 3. Kot pri prejšnji izdaji, je bilo tudi tokrat veliko časa posvečenega izboljšavi osnovnih programskih knjižnic, s pomočjo katerih je zgrajen KDE.
<br />
Dirk M&uuml;ller, en izmed vodij izdaje, je podal nekaj številk: <em>»Med izdajama KDE 4.0 in KDE 4.1 je bilo opravljenih 20803 sprememb kode in 15432 sprememb v prevodih. Skoraj 35000 sprememb kode se je zgodilo v delovnih vejah, od katerih so bile nekatere vključene v KDE 4.1 in niti niso štete.«</em> M&uuml;ller je prav tako povedal, da je ekipa sistemskih administratorjev v tem času na KDE-jevem SVN strežniku ustvarila 166 novih računov za razvijalce.

<?php
    screenshot("desktop_thumb.png", "desktop.png", "center",
"Namizje KDE 4.1", "405");
?>

</p>
<p>
<strong>Ključne izboljšave v KDE 4.1 so:</strong>
<ul>
    <li>Vrnil se je paket programov KDE PIM</li>
    <li>Namizje Plazma je postalo uporabno</li>
    <li>Več novih in izboljšanih programov ter ogrodij</li>
</ul>

</p>


<h3>
  <a name="changes">V spomin: Uwe Thiem</a>
</h3>
<p align="justify">
Skupnost KDE to izdajo posveča Uweju Thiemu, ki je več let prispeval k razvoju KDE-ja in je nedavno preminil zaradi nenadne odpovedi ledvic. Smrt Uweja je prišla popolnoma nepričakovano in je pretresla ostale prispevajoče. Uwe je, povsem dobesedno, do konca svojih dni prispeval k razvoju KDE-ja, in to ne samo s programiranjem. Igral je tudi pomembno vlogo pri izobraževanju uporabnikov v Afriki o prosti programski opremi. Z nenadno smrtjo Uweja je KDE izgubil dragocenega člana skupnosti in prijatelja. Naše misli so z njegovo družino in vsemi, ki jih je pustil za sabo.
</p>

<h3>
  <a name="changes">Preteklost, sedanjost in prihodnost</a>
</h3>
<p align="justify">
Cilj KDE 4.1 je biti prva izdaja, ki je primerna za uporabnike, ki med prvimi pričnejo uporabljati nove tehnologije. Kljub temu pa boste mogoče pogrešali kako funkcijo iz KDE 3.5, ki še ni implementirana v KDE 4.1. Ekipa KDE-ja dela na teh funkcijah in jih lahko pričakujete v eni izmed naslednjih izdaj. KDE 4.1 že ponuja močno in s funkcijami bogato delovno okolje, ni pa moč zagotoviti, da bo implementirana prav vsaka funkcija iz KDE 3.5.<br />
Vedite, da so se nekatere nastavitve uporabniškega vmesnika premaknile na mesto, kjer so bližje predmetu, ki ga nastavljate. Preden poročate o manjkajoči nastavitvi zato skrbno preglejte vmesnik.<br />
KDE 4.1 je velik korak naprej v seriji KDE 4 in nastavlja tempo za nadaljnji razvoj. KDE 4.2 lahko pričakujete v januarju 2009.
</p>

<?php
    screenshot("kdepim-screenie_thumb.png", "kdepim-screenie.png", "center",
"KDE PIM se je vrnil", "305");
?>

<h3>
  <a name="changes">Izboljšave</a>
</h3>
<p align="justify">
Med stabilizacijo novih ogrodij v KDE 4.1 se je fokus pomaknil proti delom, ki so vidni uporabnikom. Spodaj si lahko preberete o izboljšavah v KDE 4.1. Več podatkov najdete na straneh <a href="http://techbase.kde.org/Schedules/KDE4/4.1_Release_Goals">KDE 4.1 Release Goals</a> in <a href="http://techbase.kde.org/Schedules/KDE4/4.1_Feature_Plan">KDE 4.1 Feature Plan</a>.
</p>

<h4>
  Za uporabnike
</h4>
<p align="justify">

<ul>
    <li>
        <strong>KDE PIM</strong> se je s KDE 4.1 vrnil in vsebuje programe, ki jih potrebujete za upravljanje z zasebnimi podatki in za komunikacijo. Odjemalec e-pošte KMail, organizator KOrganizer, bralnik virov RSS Akregator in ostali programi so spet na voljo z novim izgledom KDE 4.
    </li>
    <li>
        <strong>Dragon Player</strong> je nov predvajalnik videov, ki je preprost za uporabo.
    </li>
    <li>
        <strong>Okteta</strong> je nov binarni urejevalnik datotek z veliko funcijami.
    </li>
    <li>
        <strong>Step</strong> je fizikalni simulator, s katerim je učenje fizike zabavno in preprosto.
    </li>
    <li>
        <strong>KSystemLog</strong> vam pomaga spremljati dogajanje na računalniku.
    </li>
    <li>
        <strong>Nove igre</strong> KDiamond, Kollision, KBreakOut in Kubrick kar kličejo po sprostitvi med delom.
    </li>
    <li>
        <strong>Lokalize</strong> pomaga prevajalcem, ki vam omogočijo, da KDE uporbljate <a href="https://wiki.lugos.si/slovenjenje:kde:prevajanje">v slovenščini</a>, ali pa v enem izmed 50 drugih jezikov.
    </li>
    <li>
        <strong>KSCD</strong> bo ponovno predvajal vaše glasbene zgoščenke.
    </li>
</ul>

Odgovori na pogosto zastavljena vprašanja so zbrana na strani <a href="http://www.kde.org/users/faq.php">KDE4 End User FAQ</a>. Branje priporočamo, če želite izvedeti kaj več o KDE 4.
</p>

<p align="justify">
<?php
    screenshot("dolphin-screenie_thumb.png", "dolphin-screenie.png", "center",
"Nov način za izbiranje datotek v Dolphinu", "483");
?>

<ul>
    <li>
        <strong>Dolphin</strong>, KDE-jev upravljalnik datotek, prinaša nov drevesni prikaz in zavihke. Novo in inovativno izbiranje z enim klikom prinaša doslednejšo uporabniško izkušnjo. Funkciji »Skopiraj v« in »Premakni v« sta hitro dostopni v priročnem meniju. Seveda je Konqueror še vedno na voljo kot alternativa Dolphinu in prav tako vsebuje omenjene izboljšave. [<a href="#screenshots-dolphin">Zaslonski posnetki za Dolphin</a>]
    </li>
    <li>
        <strong>Konqueror</strong>, KDE-jev spletni brskalnik, sedaj vsebuje podporo za ponovno odpiranje zaprtih oken in zavihkov. Prav tako je dodana možnost za gladko drsenje.
    </li>
    <li>
        <strong>Gwenview</strong>, KDE-jev pregledovalnik slik, je pridobil nov celozaslonski prikaz, vrstico z ogledi za hiter dostop do drugih slik, pameten sistem za razveljavljanje in podporo za ocenjevanje slik. [<a href="#screenshots-gwenview">Zaslonski posnetki za Gwenview</a>]
    </li>
    <li>
        <strong>KRDC</strong>, KDE-jev odjemalec za oddaljena namizja, sedaj namizja v krajevnem omrežju zazna samodejno s pomočjo protokola ZeroConf.
    </li>

    <li>
        <strong>Marble</strong>, KDE-jev namizni globus, prinaša integracijo z <a
        href="http://www.openstreetmap.org/">OpenStreetMap</a>. Tako lahko hitro najdete svojo pot s pomočjo prostih zemljevidov. [<a href="#screenshots-marble">Zaslonski posnetki za Marble</a>]
    </li>
    <li>
        <strong>KSysGuard</strong> sedaj podpira spremljanje izhoda procesov zagnanih programov. Ko boste želeli videti, kaj se dogaja, vam ne bo več potrebno znova zaganjati programov iz konzole.
    </li>
    <li>
        <strong>KWin</strong>, KDE-jev upravljalnik oken, je postal bolj stabilen in je pridobil nekaj novih funkcij. Dodani so bili nekateri novi efekti, npr. preklapljanje z izgledom ovitkov in želatinasta okna. [<a href="#screenshots-kwin">Zaslonski posnetki za KWin</a>]
    </li>
    <li>
        Namizje <strong>Plazma</strong> prinaša novo nastavljanje pultov. Novi kontrolnik omogoča preprosto nastavljanje pultov s takoj vidnimi rezultati. Pulte lahko dodajate in jih premaknete na različne robove zaslona. Nov gradnik »Prikaz mape« vam omogoča prikaz datotek iz katere koli mape (krajevne ali z omrežja) na namizju. Na namizje lahko postavite poljubno število teh gradnikov, kar vam omogoča preprost in zelo prilagodljiv dostop do datotek.
        [<a href="#screenshots-plasma">Zaslonski posnetki Plazme</a>]
    </li>
</ul>
</p>

<h4>
  Za razvijalce
</h4>
<p align="justify">

<ul>
    <li>
        <strong>Akonadi</strong> je novo ogrodje za hranjenje podatkov povezanih z upravljanjem zasebnih informacij. Programom omogoča učinkovito shranjevanje in pridobivanje e-pošte in stikov. <a href="http://en.wikipedia.org/wiki/Akonadi">Akonadi</a> podpira iskanje po podatkih in programe, ki uporabljajo to ogrodje, obvešča o spremembah.
    </li>
    <li>
        Programe za KDE lahko pišete v <a href="http://techbase.kde.org/Development/Languages">programskih jezikih</a> <strong>Python</strong> in <strong>Ruby</strong>. Podpora za te jezike se smatra kot
        stabilna in primerna za razvijalce programov.
    </li>
    <li>
        <strong>Libksane</strong> omogoča preprost dostop do programov za skeniranje slik, kakršen je novi program Skanlite.
    </li>
    <li>
        Deljen sistem za <strong>ikone čustev (smeškote)</strong>, ki ga že uporabljata KMail in Kopete.
    </li>
    <li>
        Nove večpredstavnostne hrbtenice za GStreamer, QuickTime in DirectShow9 za večpredstavnostni sistem <strong>Phonon</strong>. S tem KDE-jev sistem za večpredstavnost podpira tudi Mac OS in Windows.
    </li>

</ul>
</p>

<h3>
  Nove platforme
</h3>
<p align="justify">
<ul>
    <li>
        Podpora za <a href="http://techbase.kde.org/Projects/KDE_on_Solaris"><strong>OpenSolaris</strong></a>
        se še ureja. KDE večinoma deluje, a preostajajo še nekatere večje napake.
    </li>
    <li>
        Razvijalci na <strong>Windows</strong> lahko <a href="http://windows.kde.org">prenesejo</a>
        predoglede KDE-jevih programov za Windows. Programske knjižnice so že dokaj stabilne, vendar vse funkcije na Windows še niso na voljo. Nekateri programi že delujejo zelo dobro, nekateri pa ne tako dobro.
    </li>
    <li>
        <strong>Mac OS X</strong> je še ena platforma na katero prihaja KDE.
        <a href="http://mac.kde.org">KDE na Macu</a> še ni pripravljen za vsakodnevno uporabo. Podpora za večpredstavnost prek ogrodja Phonon je že na voljo. Integracija s strojno opremo in iskanjem še ni zaključena.
    </li>
</ul>
</p>

<a name="screenshots-dolphin"></a>
<h3>
  Zaslonski posnetki
</h3>
<p align="justify">

<a name="screenshots-dolphin"></a>
<h4>Dolphin</h4>
<?php

    screenshot("dolphin-treeview_thumb.png", "dolphin-treeview.png", "center",
               "Novi drevesni prikaz omogoča hitrejši dostop do map.
               Privzeto ni vklopljen.", "448");

    screenshot("dolphin-tagging_thumb.png", "dolphin-tagging.png", "center",
               "Nepomuk omogoča označevanje in ocenjevanje po vsem KDE-ju.", "337");

    screenshot("dolphin-icons_thumb.png", "dolphin-icons.png", "center",
               "Ogledi in vrstice s podatki ponujajo pregled nad dokumenti.", "390");

    screenshot("dolphin-filterbar_thumb.png", "dolphin-filterbar.png", "center",
               "Z vrstico za filtriranje boste hitreje našli svoje datoteke.", "372");
?>

<a name="screenshots-gwenview"></a>
<h4>Gwenview</h4>
<?php

    screenshot("gwenview-browse_thumb.png", "gwenview-browse.png", "center",
               "Lahko brskate po mapah s slikami. Ob lebdenju nad sliko se pojavi
               priročna vrstica s pogostimi dejanji.", "440");

    screenshot("gwenview-open_thumb.png", "gwenview-open.png", "center",
               "Zahvaljujoč KDE-jevi infrastrukturi je odpiranje slik s trdega diska
               in z omrežja enako preprosto.", "439");

    screenshot("gwenview-thumbnailbar_thumb.png", "gwenview-thumbnailbar.png", "center",
               "Nova vrstica z ogledi vam omogoča hitro preklapljanje med slikami.
               Na voljo je tudi v celozaslonskem načinu.", "684");

    screenshot("gwenview-sidebar_thumb.png", "gwenview-sidebar.png", "center",
               "Stranski stolpec prikazuje dodatne podatke o slikah in
               vsebuje orodja za obdelavo slik.", "462");

?>

<a name="screenshots-marble"></a>
<h4>Marble</h4>

<?php

    screenshot("marble-globe_thumb.png", "marble-globe.png", "center",
               "Namizni globus Marble.", "427");

    screenshot("marble-osm_thumb.png", "marble-osm.png", "center",
               "Nova integracija z OpenStreetMap omogoča tudi prikaz informacij
               o javnem prometu.", "425");


?>
<a name="screenshots-kwin"></a>
<h4>KWin</h4>

<?php

    screenshot("kwin-desktopgrid_thumb.png", "kwin-desktopgrid.png", "center",
               "Mreža namizij prikazuje koncept navideznih namizij in vam omogoča,
               da si lažje zapomnite, kje ste pustili določeno okno.", "405");

    screenshot("kwin-coverswitch_thumb.png", "kwin-coverswitch.png", "center",
               "Preklapjanje med okni, podobno ovitkom, omogoča preklapljanje med
               programi z Alt+Tab, ki je prava paša za oči. Vklopite ga lahko v nastavitvah
               namiznih učinkov.", "405");

    screenshot("kwin-wobbly_thumb.png", "kwin-wobbly.png", "center",
               "Sedaj so na voljo tudi priljubljena želatinasta okna (privzeto ni vklopljeno).", "405");


?>

<a name="screenshots-plasma"></a>
<h4>Plasma</h4>
<?php

    screenshot("plasma-folderview_thumb.png", "plasma-folderview.png", "center",
               "Novi gradnik »Prikaz mape« vam omogoča prikaz vsebine poljubne mape
               na namizju. Odklenite gradnika, povlecite mapo na namizje in ustvaril se bo
               prikaz te mape. Mapa je lahko na krajevnem disku ali drugem računalniku v omrežju.", "405");

    screenshot("panel-controller_thumb.png", "panel-controller.png", "center",
               "Novo nastavljanje pulta vam omogoča preprosto spreminjanje velikosti in
               položaja. Spreminjate lahko tudi položaj gradnikov na pultu, tako da jih
               povlečete in spustite.", "116");

    screenshot("krunner-screenie_thumb.png", "krunner-screenie.png", "center",
               "S KRunnerjem (Alt+F2) lahko zaganjate programe, pošiljate e-pošto in
               in postorite marsikaj drugega.", "238");

    screenshot("plasma-kickoff_thumb.png", "plasma-kickoff.png", "center",
               "Plazmin zaganjalnik programov Kickoff je dobil lepši izgled.", "405");

    screenshot("switch-menu_thumb.png", "switch-menu_thumb.png", "center",
               "Preklapljate lahko med zaganjalnikom programov Kickoff in klasičnim menijem.", "344");
?>

</p>

<h4>
  Znane težave
</h4>
<p align="justify">
<ul>
    <li>Uporabniki grafičnih kartic <strong>NVidia</strong> z zaprto-kodnimi gonilniki od NVidie lahko naletijo na težave s hitrostjo, npr. med premikanjem oken ali spreminjanjem njihove velikosti. NVidiini inženirji so že seznanjeni s težavami, a v tem trenutku še niso izdali popravljenih gonilnikov. Nekaj napotkov, ki vam lahko pomagajo omiliti težavo je na strani <a href="http://techbase.kde.org/User:Lemma/GPU-Performance">Techbase</a>, vendar bo za končno rešitev težav potrebno počakati na NVidio, da izda popravljene gonilnike.</li>
</ul>

</p>

<!-- ==================================================================================== -->
<h4>
  Dobite, poganjajte in testirajte
</h4>
<p align="justify">
Prostovoljci in izdelovalci distribucij operacijskega sistema Linux so pripravili binarne paketke za KDE 4.1.0 za nekatere distribucije Linux. za Mac OS X in Windows.
</p>
<h4>
  Prevajanje izvorne kode KDE 4.1.0
</h4>
<p align="justify">
  <a name="source_code"></a><em>Izvorna koda</em>.
  Celotna izvorna koda za KDE 4.1.0 je na voljo za <a
  href="http://www.kde.org/info/4.1.0.php">prost prenos</a>.
  Navodila za prevajanje in nameščanje KDE 4.1.0
  so na voljo na strani <a href="/info/4.1.0.php">KDE 4.1.0 Info</a> ali pa na strani <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Stiki z mediji</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>