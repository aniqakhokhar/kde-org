<?php
  $page_title = "KDE 4.0-alpha1: A Visual Guide to New Features";
  $site_root = "../";
  include "header.inc";
?>

<div style="float: right; padding: 1ex; ">
	<img src="visual_guide_images-4.0-alpha1/kdelogo.png" border="0" 
	title="The KDE logo in Oxygen style" alt="The KDE logo in Oxygen style" />
</div>

The KDE Community is happy to announce the immediate availability of the first alpha release of the
KDE Desktop Environment, version 4.0. The release is a basis for the integration of powerful new
technologies that will be included in KDE 4. It has been given the codename "Knut".
<p />

Highlights of KDE 4.0 Alpha1 are:

<ul>
	<li> A new <strong>visual appearance</strong> through Oxygen</li>
	<li> New <strong>frameworks to build applications</strong> with, providing vastly improved
hardware and multimedia integration (through <a href="http://solid.kde.org">Solid</a> and <a
href="http://phonon.kde.org">Phonon</a>, spelling and grammar checking, to name just a few)</li>
	<li>New <strong>applications</strong> that focus on a smooth user experience, such as Dolphin, the
file manager, and Okular, the document viewer</li>
</ul>

<div style="align: center; padding: 1ex; ">
	<a href="visual_guide_images-4.0-alpha1/desktop.png">
		<img src="visual_guide_images-4.0-alpha1/desktop-thumb.png" border="0" 
		title="The desktop in its budding glory"  
		alt="The desktop in its budding glory" />
	</a>
</div>
<p />

KDE 4.0 Alpha1 marks the end of the addition of large features to the KDE base libraries and shifts
the focus onto integrating those new technologies into applications and the basic desktop. The
next few months will be spent on bringing the desktop into shape after two years of frenzied development
leaving very little untouched.
<p />
The final release of KDE 4.0 is expected in late October 2007.
The work on the framework makes application development easier and more satisfying.  This will result in a crop of new features and applications over the next few years, underlining the position KDE holds as the leading Free Desktop and
bringing Free Software to new users world-wide. A detailed <a href="http://techbase.kde.org/Schedules/KDE4/4.0_Release_Schedule">release schedule</a> can be found
at <a href="http://techbase.kde.org">techbase.kde.org</a>, the new technical resource accompanying the platform, that provides extensive and high-quality documentation about KDE.
<p />

<a name="status">

<h2>Status and Outlook</h2>

<div style="float: right; padding: 1ex;">
<img src="visual_guide_images-4.0-alpha1/krash.png" border="0" alt="" /></div>
While Alpha1 is nowhere near finished, it includes all of the exciting new frameworks that 
lift the Free Desktop to an unparalleled level of integration. Areas that need attention include:
<ul>
<li>Finishing Plasma and turning it into the default desktop shell</li>
<li>Finishing porting applications and integrating the new technologies into them</li>
<li>Fixing lots of bugs</li>
</ul>
<p />

Alpha1 comes with <a
href="http://www.trolltech.com/products/qt/whatsnew/whatsnew-qt43">Qt 4.3 preview</a>, and most
packagers will enable debugging.  So you shouldn't expect it to be fast or stable, nor is it
feature complete. But despite all of the things that are still very much a work in progress,
there is fun to have and features to see, and you are invited to have a look.


<a name="sightseeing" />
<h2>Sightseeing</h2>
While you are going to check it out, what are the areas with most to see?  Put the KDE
Education applications under the microscope, which have made good progress and have 
received many new features. Have a look at <a href="http://edu.kde.org/kalzium/">Kalzium</a> and its
new <a href="http://cniehaus.livejournal.com/24404.html">3D molecule viewer</a>. And kick around 
the KDE Games, which have been updated with <a
href="http://johann.pwsp.net/2007/04/09/a-new-game-starter/">features</a> like <a
href="http://annma.blogspot.com/2007/04/klettres.html">scalable</a> <a
href="http://piacentini.livejournal.com/1680.html">vector</a> <a
href="http://johann.pwsp.net/2007/04/22/kbattleship-a-winning-team/">graphics</a>, theme support,
better AI and network support and there are already some <a href="http://home.gna.org/kiriki/">new ones</a>.
The more adventurous might want to step into the <a href="http://koffice.kde.org">KOffice</a>. Not all distributions' packages include it;
only openSUSE does and Kubuntu might. The KOffice project will not release its first
alpha alongside KDE 4.0 Alpha1. KOffice in KDE 4 is still very unstable, and most things
barely work - but still, you can see the <a
href="http://www.kdedevelopers.org/node/2759">effects</a> of the new framework. You can create
shapes using vector graphics, text or even <a
href="http://www.valdyas.org/fading/index.cgi/2007/04/22#musicflake1">music notations</a>, and
manipulate them. Each can be rotated or sheared, and can have several effects applied. Don't forget
the <a href="http://www.valdyas.org/fading/index.cgi/hacking/krita">work</a> <a
href="http://cyrilleberger.blogspot.com/">on</a> <a href="http://koffice.kde.org/krita">Krita</a>,
which also is based on these shapes.
<p />

There are things like <a href="http://edu.kde.org/marble/">Marble</a>, a digital globe and map application for
KDE, changes in the file dialogs, configuration screens, color picker, font dialogs and dozens of <a
href="http://ariya.blogspot.com/2007/04/custom-toggle-action-for-qdockwidget.html">smaller</a> <a
href="http://www.kdedevelopers.org/node/2758">and</a> <a
href="http://techbase.kde.org/Projects/Widgets_and_Classes">larger things</a> that make KDE 4 fresh and vital.
<p />

<a href="http://pim.kde.org">KDE PIM</a>'s development continues at an amazing pace, but due to the scope
of the changes, including a shared storage layer for all PIM applications, <a href="http://pim.kde.org/akonadi/">Akonadi</a>, there is not much to see yet and the major changes will first appear in KDE 4.1. Any <a
href="http://pim.kde.org/development/">help</a> would be very appreciated, of course! You could
start with the <a
href="http://bugs.kde.org/buglist.cgi?short_desc_type=regexp&short_desc=JJ.*&product=akregator&
product=kaddressbook&product=kalarm&product=kandy&product=karm&product=kitchensync&product=kmail&
product=knode&product=knotes&product=konsolekalendar&product=kontact&product=korganizer&product=
kpilot&product=ktnef&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED">
Junior Jobs</a>, or drop by in #kontact on <a href="http://www.freenode.net/">freenode</a> for more
info.
<p />
Additionally, configuration via KControl is not currently possible. For
now, you can use kcmshell to configure KDE instead:<br>
<pre>kcmshell --list</pre> - gives a list of configuration modules, and <br>
<pre>kcmshell fonts style</pre> - shows the config dialog with with fonts and style modules.<br>
<p />


<a name="eye_candy" />
<h3>Eyecandy and Plasma</h3>

<div style="float: right; padding: 1ex;">
	<a href="visual_guide_images-4.0-alpha1/krunner.png">
		<img src="visual_guide_images-4.0-alpha1/krunner-thumb.png" border="0" title="KRunner, one
of the parts of Plasma"  alt="KRunner, one of the parts of Plasma" />
	</a>
</div>

For those looking for eye candy, the new <a
href="http://www.kdedevelopers.org/node/2787">composite-enabled</a> branch of KWin has been merged
but the composite features are still disabled by default, and the Oxygen icons are in, 
but the new Oxygen widget style is
still too immature to be included.  The basics of Plasma are there (try Alt-F2, and check out the
new Run Command dialog as shown on the right), but most of the changes are still only in the libraries, 
so Kicker, the KDE 3 panel, is still present when you log in. There <em>is</em> a lot too see in the
games and educational applications, though, and of course many other things to explore.

<p />
<h3>The new file manager: Dolphin</h3>

<div style="float: left; padding: 1ex;">
	<a href="visual_guide_images-4.0-alpha1/dolphin.png">
		<img src="visual_guide_images-4.0-alpha1/dolphin-thumb.png" border="0" title="Dolphin, the
new default file manager" alt="Dolphin, the
new default file manager" />
	</a>
</div>

<a href="http://enzosworld.gmxhome.de/index.html">Dolphin</a> is the new KDE file manager. It is
under heavy <a href="http://www.ereslibre.es/?p=35">development</a>, but relatively stable, and
worth a look. It already uses the <a href="http://dot.kde.org/1176310483/">Strigi infrastructure</a>
for integrated desktop search and meta-data extraction. Also see KSysguard, which
has been greatly <a href="http://img54.imageshack.us/img54/653/ksysguard4dm1.png">improved</a> in
terms of looks, usability and performance. Developers might be interested in checking out
<a href="http://www.kdevelop.org/mediawiki/index.php/KDevelop_4">KDevelop's new
features</a> to ease development. The <a href="http://kross.dipe.org/">Kross</a> support in KDE 4
has led to <a href="http://www.kdedevelopers.org/node/2779">Ruby</a>, Python and JavaScript support
in more and more applications, like <a
href="http://netdragon.sourceforge.net/ssuperkaramba.html">Superkaramba</a> and <a
href="http://KOffice.kde.org">KOffice</a>. Adventurous types, who compiled KDE from subversion
themselves can try some things from Playground, like the new Oxygen widget style.

<a name="getting_it">
<h2>Getting KDE 4.0 Alpha1</h2>

So if you decide to give it a try, how can you get KDE 4 on your desktop? There are several
different ways to get it running, ranging from the easy to much more complex. You could pop in
a LiveCD which allows you to test drive Alpha1 with just a reboot. Other options include installing
packages for your distribution, using the automated kdesvn-build script, hand build using the source
tarballs, or get the freshest code from <a href="http://anonsvn.kde.org/">anonsvn</a> yourself.
<p />
At the time of this announcement one KDE 4 based LiveCD is available, appropriately called <a
href="http://home.kde.org/~binner/kde-four-live/">"KDE Four Live"</a>.
It is an openSUSE based LiveCD, regularly updated complete with KDE 4 builds (in this case
Alpha1). Special thanks go to Stephan Binner for putting this one together. If you would like a safe
way to try KDE 4 without risking any of your system files, this is the way to go. Other distributions' LiveCDs are
in preparation and will be released soon.
<p />
<div style="float: right; padding: 1ex;">
	<a href="visual_guide_images-4.0-alpha1/knetwalk.png">
		<img src="visual_guide_images-4.0-alpha1/knetwalk-thumb.png" border="0" title="KNetwalk, one
of the overhauled games with new fancy vector graphics" alt="KNetwalk, one
of the overhauled games with new fancy vector graphics" />
	</a>
</div>


Additionally, distribution packages are becoming available. Packages are (or will be very shortly)
available for: 

<ul>
	<li><a href="http://kubuntu.org/announcements/kde4-alpha1.php">Kubuntu</a> </li>
	<li><a href="http://en.opensuse.org/KDE4">openSUSE</a> </li>
	<li><a href="http://ranger.users.finkproject.org/kde/index.php/Home">OS X</a> </li>
	<li><a href="http://overlays.gentoo.org/proj/kde">Gentoo</a> </li>
</ul>

Others, like <a href="http://pkg-kde.alioth.debian.org/kde4.html">Debian</a> expect packages to take
a few weeks to be finished, and we expect more distributions to join this list.
<p />
The <a href="http://kde-redhat.sourceforge.net/">Fedora KDE team</a> is still busy finishing Fedora
7, and has only some basic KDE 4 library packages available. These libraries are mainly intended for
developers so they can easily work on KDE 4 applications. But Fedora is highly committed to KDE and
is expected to release the next major Fedora release (Fedora 8) with KDE 4, probably making them one
of the first major distributions to include KDE 4. Meanwhile, thanks to the hard work of the
Mandriva KDE hackers, led by <a href="http://www.kde.org.uk/apps/kspread/interview.html">Laurent
Montel</a>, Mandriva users have been able to test KDE 4 packages from Mandriva's <a
href="http://wiki.mandriva.com/en/Development">Cooker development tree</a> for quite some time. They
are preparing a LiveCD with KDE 4.0 Alpha1 + <a
href="http://nepomuk-kde.semanticdesktop.org/xwiki/bin/view/Main/">Nepomuk</a> development for the
end of May - watch <a href="http://dot.kde.org">dot.kde.org</a> for details.  Mepis founder Warren is
discussing a special KDE 4 LiveCD to show their support, information can be found <a
href="http://www.mepis.org/docs/en/KDE4">here</a>. And don't forget the people from <a
href="http://www.arklinux.org/">Ark Linux</a>, another KDE-minded distribution. They, too are
working on a KDE 4 LiveCD and packages. Their development tree will switch to KDE 4 as soon as
2007.1 is ready, and they plan to release the 2007.2 version on the day of the KDE 4.0 release.
<p />
<div style="float: right; padding: 1ex;">
	<a href="visual_guide_images-4.0-alpha1/okular.png">
		<img src="visual_guide_images-4.0-alpha1/okular-thumb.png" border="0" 
		title="Okular, the universal document viewer in KDE 4" 
		alt="Okular, the universal document viewer in KDE 4" />
	</a>
</div>

Michael Pyne has been working hard on the kdesvn-build script to get it into shape for
Alpha1.  It is available <a href="http://kdesvn-build.kde.org">here</a>. This program is useful
for building KDE from sources automatically, not just for the alpha release. For those of you who
would like to stay up to date with KDE 4 development, but do not want the hassle of manual
compilation, this script comes highly recommended.
<p />
If you don't like the scripted approach, you can snag the Alpha1 source tarballs from 
<a href=" http://kde.org/info/3.90.1.php">the KDE servers</a> or use the <a
href="http://techbase.kde.org/Getting_Started/Build/KDE4_Alpha_1">techbase instructions</a> 
to build and setup your environment.
<p />
This release of Alpha1 contains many new KDE technologies, including some very broken ones
that will be vastly improved as the march towards 4.0 proceeds. Most importantly
though, the KDE libraries have largely stabilized, and will be entering a soft freeze later this
week. This means that the underlying core is mostly complete for 4.0, and the focus can now shift
towards the applications and workspace environment without their developers having to worry about
shifting APIs.

<p />
Of course this is only a brief (and nowhere near complete) overview of the exciting new stuff
in KDE. We hope you enjoy the experience, like what you see, and will help us improve what you don't like!

<p>Written and illustrated by Troy Unrau, Jos Poortvliet, Sebastian K&uuml;gler and Will Stephenson.</p>

<?php
  include("footer.inc");
?> 
