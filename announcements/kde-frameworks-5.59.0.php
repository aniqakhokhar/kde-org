<?php
    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.59.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.59.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
June 08, 2019. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.59.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Don't try to index SQL database dumps");?></li>
<li><?php i18n("Exclude .gcode and virtual machine files from indexing consideration");?></li>
</ul>

<h3><?php i18n("BluezQt");?></h3>

<ul>
<li><?php i18n("Add Bluez API to DBus XML parser/generator");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("gcompris-qt too");?></li>
<li><?php i18n("Make falkon icon a real SVG");?></li>
<li><?php i18n("add missing icons from the apps, to be redone https://bugs.kde.org/show_bug.cgi?id=407527");?></li>
<li><?php i18n("add icon for kfourinline from app, needs updating too");?></li>
<li><?php i18n("add kigo icon https://bugs.kde.org/show_bug.cgi?id=407527");?></li>
<li><?php i18n("add kwave icon from kwave, to be redone in breeze style");?></li>
<li><?php i18n("Symlink arrow-*-double to go-*-skip, add 24px go-*-skip");?></li>
<li><?php i18n("Change input-* device icon styles, add 16px icons");?></li>
<li><?php i18n("Add dark version of new Knights icon which escaped from my previous commit");?></li>
<li><?php i18n("Create new icon for Knights based on Anjuta's icon (bug 407527)");?></li>
<li><?php i18n("add icons for apps which miss them in breeze, these should be updated to be more breezy but they are needed for the new kde.org/applications for now");?></li>
<li><?php i18n("kxstitch icon from kde:kxstitch, to be updated");?></li>
<li><?php i18n("don't glob everything and the kitchen sink");?></li>
<li><?php i18n("make sure to also assert ScaledDirectories");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Create specific directory for Qt logging categories file");?></li>
<li><?php i18n("Don't enable QT_STRICT_ITERATORS on Windows");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("ensure to search also in the legacy location");?></li>
<li><?php i18n("search in the new location for knsrc files");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Test reading and seeking in KCompressionDevice");?></li>
<li><?php i18n("KCompressionDevice: Remove bIgnoreData");?></li>
<li><?php i18n("KAr: fix out-of-bounds read (on invalid input) by porting to QByteArray");?></li>
<li><?php i18n("KAr: fix parsing of long filenames with Qt-5.10");?></li>
<li><?php i18n("KAr: the permissions are in octal, not decimal");?></li>
<li><?php i18n("KAr::openArchive: Also check ar_longnamesIndex is not &lt; 0");?></li>
<li><?php i18n("KAr::openArchive: Fix invalid memory access on broken files");?></li>
<li><?php i18n("KAr::openArchive: Protect against Heap-buffer-overflow in broken files");?></li>
<li><?php i18n("KTar::KTarPrivate::readLonglink: Fix crash in malformed files");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Don't hardcode dbus policy install dir");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Use locale currency for donate icon");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Fix compilation for python bindings (bug 407306)");?></li>
<li><?php i18n("Add GetProcessList for retrieving the list of currently active processes");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Fix qmldir files");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Remove QApplication::setColorSpec (empty method)");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Show 3 significant figures when displaying doubles (bug 343273)");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Manipulate bytes instead of characters");?></li>
<li><?php i18n("Fix kioslave executables never exiting, when setting KDE_FORK_SLAVES");?></li>
<li><?php i18n("Fix desktop link to file or directory (bug 357171)");?></li>
<li><?php i18n("Test current filter before setting a new one (bug 407642)");?></li>
<li><?php i18n("[kioslave/file] Add a codec for legacy filenames (bug 165044)");?></li>
<li><?php i18n("Rely upon QSysInfo to retrieve the system details");?></li>
<li><?php i18n("Add Documents to the default list of Places");?></li>
<li><?php i18n("kioslave: preserve argv[0], to fix applicationDirPath() on non-Linux");?></li>
<li><?php i18n("Allow to drop one file or one folder on KDirOperator (bug 45154)");?></li>
<li><?php i18n("Truncate long filename before creating a link (bug 342247)");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("[ActionTextField] Make QML tooltip consistent");?></li>
<li><?php i18n("base on height for items that should have a top padding (bug 405614)");?></li>
<li><?php i18n("Performance: compress color changes without a QTimer");?></li>
<li><?php i18n("[FormLayout] Use even top and bottom spacing for separator (bug 405614)");?></li>
<li><?php i18n("ScrollablePage: Make sure the scrolled view gets the focus when it's set (bug 389510)");?></li>
<li><?php i18n("Improve keyboard-only usage of the toolbar (bug 403711)");?></li>
<li><?php i18n("make the recycler a FocusScope");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Handle apps which set the desktopFileName property with filename suffix");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Fix assert (hash != 0) sometimes when a file is deleted by another process");?></li>
<li><?php i18n("Fix another assert when the file disappears under us: ASSERT: \"ctime != 0\"");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Don't delete entire previous line by backspace in pos 0 (bug 408016)");?></li>
<li><?php i18n("Use native dialog overwrite check");?></li>
<li><?php i18n("Add action to reset font size");?></li>
<li><?php i18n("show static word wrap marker always if requested");?></li>
<li><?php i18n("Ensure highlighted range begin/end marker after unfold");?></li>
<li><?php i18n("Fix: don't reset Highlight when saving some files (bug 407763)");?></li>
<li><?php i18n("Auto indentation: Use std::vector instead of QList");?></li>
<li><?php i18n("Fix: Use default indentation mode for new files (bug 375502)");?></li>
<li><?php i18n("remove duplicated assignment");?></li>
<li><?php i18n("honor auto-bracket setting for balance check");?></li>
<li><?php i18n("improve invalid character check on loading (bug 406571)");?></li>
<li><?php i18n("New menu of syntax highlighting in the status bar");?></li>
<li><?php i18n("Avoid infinite loop in \"Toggle Contained Nodes\" action");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Allow compositors to send discrete axis values (bug 404152)");?></li>
<li><?php i18n("Implement set_window_geometry");?></li>
<li><?php i18n("Implement wl_surface::damage_buffer");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("KNewPasswordDialog: add periods to message widgets");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Don't fetch device statistics upon construction");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Make Breeze Light/Dark use more system colors");?></li>
<li><?php i18n("Export SortFilterModel sort column to QML");?></li>
<li><?php i18n("plasmacore: fix qmldir, ToolTip.qml no longer part of module");?></li>
<li><?php i18n("signal availableScreenRectChanged for all applets");?></li>
<li><?php i18n("Use simply configure_file to generate the plasmacomponents3 files");?></li>
<li><?php i18n("Update *.qmltypes to current API of QML modules");?></li>
<li><?php i18n("FrameSvg: also clear mask cache on clearCache()");?></li>
<li><?php i18n("FrameSvg: make hasElementPrefix() also handle prefix with trailing -");?></li>
<li><?php i18n("FrameSvgPrivate::generateBackground: generate background also if reqp != p");?></li>
<li><?php i18n("FrameSvgItem: emit maskChanged also from geometryChanged()");?></li>
<li><?php i18n("FrameSvg: prevent crash when calling mask() with no frame yet created");?></li>
<li><?php i18n("FrameSvgItem: emit maskChanged always from doUpdate()");?></li>
<li><?php i18n("API dox: note for FrameSvg::prefix()/actualPrefix() the trailing '-'");?></li>
<li><?php i18n("API dox: point to Plasma5 versions on techbase if avail");?></li>
<li><?php i18n("FrameSvg: l &amp; r borders or t &amp; b don't need to have same height resp. width");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("[JobDialog] Also signal cancellation when window is closed by the user");?></li>
<li><?php i18n("Report cancelling a configuration as finished with an error (bug 407356)");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Remove DefaultListItemBackground and MenuItem animation");?></li>
<li><?php i18n("[QQC2 Slider Style] Fix wrong handle positioning when initial value is 1 (bug 405471)");?></li>
<li><?php i18n("ScrollBar: Make it work as a horizontal scroll bar as well (bug 390351)");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Refactor the way device backends are built and registered");?></li>
<li><?php i18n("[Fstab] Use folder-decrypted icon for encrypting fuse mounts");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("YAML: only comments after spaces and other improvements/fixes (bug 407060)");?></li>
<li><?php i18n("Markdown: use includeAttrib in code blocks");?></li>
<li><?php i18n("fix highlighting of \"\\0\" in C mode");?></li>
<li><?php i18n("Tcsh: fix operators and keywords");?></li>
<li><?php i18n("Add syntax definition for the Common Intermediate Language");?></li>
<li><?php i18n("SyntaxHighlighter: Fix foreground color for text without special highlighting (bug 406816)");?></li>
<li><?php i18n("Add example app for printing highlighted text to pdf");?></li>
<li><?php i18n("Markdown: Use color with higher contrast for lists (bug 405824)");?></li>
<li><?php i18n("Remove .conf extension from \"INI Files\" hl, to determine the highlighter using MIME type (bug 400290)");?></li>
<li><?php i18n("Perl: fix the // operator (bug 407327)");?></li>
<li><?php i18n("fix casing of UInt* types in Julia hl (bug 407611)");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.59");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.10");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://phabricator.kde.org/project/view/90/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
