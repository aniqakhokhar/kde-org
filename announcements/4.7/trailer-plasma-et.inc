
<h3>
<a href="plasma-et.php">
Plasma töötsoonid: KWin aitab neid kõikjal kasutada
</a>
</h3>

<p>
<a href="plasma-et.php">
<img src="images/plasma.png" class="app-icon" alt="KDE Plasma töötsoonid 4.7" width="64" height="64" />
</a>
Plasma töötsoonidele on kasuks tulnud ulatuslik arendustegevus KDE komposiit-aknahalduri KWin kallal ning selliste uute Qt tehnoloogiate nagu Qt Quick kasutamise võimalus. Täpsemalt kõneleb kõigest  <a href="plasma-et.php">Plasma töölaua ja Plasma Netbook 4.7 väljalasketeade</a>.

<br /><br />
<br /><br />
</p>
