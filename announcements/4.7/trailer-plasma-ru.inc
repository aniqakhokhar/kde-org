
<h3>
<a href="plasma-ru.php">
Оболочки Plasma стали более подходящими для мобильных устройств благодаря KWin
</a>
</h3>

<p>
<a href="plasma-ru.php">
<img src="images/plasma.png" class="app-icon" alt="Оболочки рабочего стола KDE Plasma 4.7" width="64" height="64" />
</a>
Оболочки рабочего стола Plasma выиграли от усердной работы над 
композитным диспетчером окон KWin и от применения новых технологий Qt, 
например Qt Quick. 
Чтобы узнать подробности, прочитайте <a href="plasma-ru.php">анонс выпуска оболочек рабочего стола Plasma 4.7</a>.
<br /><br />
<br /><br />
</p>
