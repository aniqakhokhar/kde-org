
<h3>
<a href="plasma.php">
Plasma Workspaces Become More Portable Thanks to KWin
</a>
</h3>

<p>
<a href="plasma.php">
<img src="images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.7" width="64" height="64" />
</a>
The Plasma Workspaces gain from extensive work to KDE’s compositing window manager, KWin, and from the leveraging of new Qt technologies such as Qt Quick. For full details, read the <a href="plasma.php">Plasma Desktop and Plasma Netbook 4.7 release announcement</a>.

<br /><br />
<br /><br />
</p>
