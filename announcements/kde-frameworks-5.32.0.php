<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.32.0");
  $site_root = "../";
  $release = '5.32.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
March 11, 2017. KDE today announces the release
of KDE Frameworks 5.32.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Implement nested tags");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Added icons for Plasma Vault");?></li>
<li><?php i18n("Renamed icons for encrypted and decrypted folders");?></li>
<li><?php i18n("Add torrents icon 22px");?></li>
<li><?php i18n("Add nm-tray icons (bug 374672)");?></li>
<li><?php i18n("color-management: removed undefined links (bug 374843)");?></li>
<li><?php i18n("system-run is now an action until &lt;= 32px and 48px an app icon (bug 375970)");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Detect inotify");?></li>
<li><?php i18n("Revert \"Automatically mark classes with pure virtual functions as /Abstract/.\"");?></li>
</ul>

<h3><?php i18n("KActivitiesStats");?></h3>

<ul>
<li><?php i18n("Allow to plan ahead and set the order for an item not yet in the list");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Fix Potential leak of memory pointed to by 'limitedDev'");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("Fixed potential crash in QML KCMs when application palette changes");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("KConfig: stop exporting and installing KConfigBackend");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("KColorScheme: default to application scheme if set by KColorSchemeManager (bug 373764)");?></li>
<li><?php i18n("KConfigDialogManager: get change signal from metaObject or special property");?></li>
<li><?php i18n("Fix KCModule::setAuthAction error checking");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Exclude (6) from emoticons recognition");?></li>
<li><?php i18n("KDirWatch: fix memory leak on destruction");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Fix bug in kfiledialog.cpp that causes crashing when native widgets are used");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("meinproc5: link to the files, not to the library (bug 377406)");?></li>
<li><?php i18n("Remove the KF5::XsltKde static library");?></li>
<li><?php i18n("Export a proper shared library for KDocTools");?></li>
<li><?php i18n("Port to categorized logging and clean includes");?></li>
<li><?php i18n("Add function to extract a single file");?></li>
<li><?php i18n("Fail the build early if xmllint is not available (bug 376246)");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("New maintainer for kfilemetadata");?></li>
<li><?php i18n("[ExtractorCollection] Use mimetype inheritance to return plugins");?></li>
<li><?php i18n("add a new property DiscNumber for audio files from multi-disc albums");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Cookies KCM: disable \"delete\" button when there is no current item");?></li>
<li><?php i18n("kio_help: Use the new shared library exported by KDocTools");?></li>
<li><?php i18n("kpac: Sanitize URLs before passing them to FindProxyForURL (security fix)");?></li>
<li><?php i18n("Import remote ioslave from plasma-workspace");?></li>
<li><?php i18n("kio_trash: implement renaming of toplevel files and dirs");?></li>
<li><?php i18n("PreviewJob: Remove maximum size for local files by default");?></li>
<li><?php i18n("DropJob: allow to add application actions on an open menu");?></li>
<li><?php i18n("ThumbCreator: deprecate DrawFrame, as discussed in https://git.reviewboard.kde.org/r/129921/");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Add support for flatpak portals");?></li>
<li><?php i18n("Send desktopfilename as part of notifyByPopup hints");?></li>
<li><?php i18n("[KStatusNotifierItem] Restore minimized window as normal");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("Finish support for opening compressed packages");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Remember file type set by user over sessions");?></li>
<li><?php i18n("Reset filetype when opening url");?></li>
<li><?php i18n("Added getter for word-count configuration value");?></li>
<li><?php i18n("Consistent conversion from/to cursor to/from coordinates");?></li>
<li><?php i18n("Update file type on save only if path changes");?></li>
<li><?php i18n("Support for EditorConfig configuration files (for details: http://editorconfig.org/)");?></li>
<li><?php i18n("Add FindEditorConfig to ktexteditor");?></li>
<li><?php i18n("Fix: emmetToggleComment action doesn't work (bug 375159)");?></li>
<li><?php i18n("Use sentence style capitalization with label texts of edit fields");?></li>
<li><?php i18n("Reverse meaning of :split, :vsplit to match vi and Kate actions");?></li>
<li><?php i18n("Use C++11 log2() instead of log() / log(2)");?></li>
<li><?php i18n("KateSaveConfigTab: put spacer behind last group on Advanced tab, not inside");?></li>
<li><?php i18n("KateSaveConfigTab: Remove wrong margin around content of Advanced tab");?></li>
<li><?php i18n("Borders config subpage: fix scrollbar visibility combobox being off-placed");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("KToolTipWidget: hide tooltip in enterEvent if hideDelay is zero");?></li>
<li><?php i18n("Fix KEditListWidget losing the focus on click of buttons");?></li>
<li><?php i18n("Add decomposition of Hangul Syllables into Hangul Jamo");?></li>
<li><?php i18n("KMessageWidget: fix behaviour on overlapping calls of animatedShow/animatedHide");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Don't use KConfig keys with backslashes");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Sync introspections and generated files with NM 1.6.0");?></li>
<li><?php i18n("Manager: Fix emitting deviceAdded twice when NM restarts");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("set default hints when repr doesn't export Layout.* (bug 377153)");?></li>
<li><?php i18n("possible to set expanded=false for a containment");?></li>
<li><?php i18n("[Menu] Improve available space correction for openRelative");?></li>
<li><?php i18n("move setImagePath logic into updateFrameData() (bug 376754)");?></li>
<li><?php i18n("IconItem: Add roundToIconSize property");?></li>
<li><?php i18n("[SliderStyle] Allow providing a \"hint-handle-size\" element");?></li>
<li><?php i18n("Connect all connections to action in QMenuItem::setAction");?></li>
<li><?php i18n("[ConfigView] Honor KIOSK Control Module restrictions");?></li>
<li><?php i18n("Fix disabling the spinner animation when Busy indicator has no opacity");?></li>
<li><?php i18n("[FrameSvgItemMargins] Don't update on repaintNeeded");?></li>
<li><?php i18n("Applet icons for the Plasma Vault");?></li>
<li><?php i18n("Migrate AppearAnimation and DisappearAnimation to Animators");?></li>
<li><?php i18n("Align bottom edge to top edge of visualParent in the TopPosedLeftAlignedPopup case");?></li>
<li><?php i18n("[ConfigModel] Emit dataChanged when a ConfigCategory changes");?></li>
<li><?php i18n("[ScrollViewStyle] Evaluate frameVisible property");?></li>
<li><?php i18n("[Button Styles] Use Layout.fillHeight instead of parent.height in a Layout (bug 375911)");?></li>
<li><?php i18n("[ContainmentInterface] Also align containment context menu to panel");?></li>
</ul>

<h3><?php i18n("Prison");?></h3>

<ul>
<li><?php i18n("Fix min qt version");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Floppy disks now show up as \"Floppy Disk\" instead of \"0 B Removable Media\"");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Add more keyword. Disable spellchecking for keywords");?></li>
<li><?php i18n("Add more keyword");?></li>
<li><?php i18n("Add *.RHTML file extension to Ruby on Rails highlighting (bug 375266)");?></li>
<li><?php i18n("Update SCSS and CSS syntax highlight (bug 376005)");?></li>
<li><?php i18n("less highlighting: Fix single line comments starting new regions");?></li>
<li><?php i18n("LaTeX highlighting: fix alignat environment (bug 373286)");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.32");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.6");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
