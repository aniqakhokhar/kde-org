<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Plasma 5.6 Release");
  $site_root = "../";
  $release = 'plasma-5.6.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<style>
figure {text-align: center; float: right; margin: 0px;}
figure img {padding: 1ex; border: 0px; background-image: none;}
figure video {padding: 1ex; border: 0px; background-image: none;}
figcaption {font-style: italic;}
</style>

<figure style="float: none">
<iframe style="text-align: center" width="560" height="315" src="https://www.youtube.com/embed/v0TzoXhAbxg?rel=0" frameborder="0" allowfullscreen></iframe>
<figcaption><?php i18n("KDE Plasma 5.6 Video");?></figcaption>
</figure>
<br clear="all" />

<figure style="float: none">
<a href="plasma-5.6/plasma-5.6.png">
<img src="plasma-5.6/plasma-5.6-wee.png" style="border: 0px" width="600" height="338" alt="<?php i18n("Plasma 5.6");?>" />
</a>
<figcaption><?php i18n("KDE Plasma 5.6");?></figcaption>
</figure>


<p>
<?php i18n("Tuesday, 22 March 2016. "); ?>
<?php i18n("Today KDE releases a feature-packed new version of its desktop user interface, Plasma 5.6.
");?>
</p>

<p>
<?php i18n("
This release of Plasma brings many improvements to the task manager, KRunner, activities, and Wayland support as well as a much more refined look and feel.
");?>
</p>

<br clear="all" />
<h3>
<?php i18n("
Slicker Plasma Theme
");?>
</h3>

<figure style="float: right">
<a href="plasma-5.6/colour-schemes.png">
<img src="plasma-5.6/colour-schemes-wee.png" style="border: 0px" width="300" height="169" alt="<?php i18n("Breeze Color Scheme Support");?>" />
</a>
<figcaption><?php i18n("Breeze Color Scheme Support");?></figcaption>
</figure>

<p>
<?php i18n("
The default Plasma theme, Breeze, now follows the application color scheme allowing for a more personalized experience. A new 'Breeze Light' together with 'Breeze Dark' theme can be used to bring back the previous behavior. Additionally, tooltip animations have become more subtle.
");?>
</p>

<br clear="all" />
<h3>
<?php i18n("
Supercharged Task Manager
");?>
</h3>

<figure style="float: right">
<a href="plasma-5.6/download.png">
<img src="plasma-5.6/download-wee.png" style="border: 0px" width="324" height="58" alt="<?php i18n("Copy Progress");?>" />
</a>
<figcaption><?php i18n("Copy Progress");?></figcaption>
</figure>

<p>
<?php i18n("
Multitasking has just become easier. The much improved task manager in Plasma 5.6 now displays progress of tasks, such as downloading or copying files.
");?>
</p>

<br clear="all" />
<figure style="float: none">
<a href="plasma-5.6/media-player.png">
<img src="plasma-5.6/media-player-wee.png" style="border: 0px" width="300" height="240" alt="<?php i18n("Media Controls");?>" />
</a>
<a href="plasma-5.6/media-player-tooltips.png">
<img src="plasma-5.6/media-player-tooltips-wee.png" style="border: 0px" width="300" height="187" alt="<?php i18n("Media Controls");?>" />
</a>
<figcaption><?php i18n("Media Controls in Panel and Tooltips");?></figcaption>
</figure>

<p>
<?php i18n("
Moreover, hovering a music or video player shows beautiful album art and media controls, so you never have to leave the application you're currently working with. Our media controller applet that shows up during playback also received some updates, notably support for multiple players running simultaneously.
");?>
</p>

<br clear="all" />
<figure style="float: right">
<a href="plasma-5.6/jumplistfirefoxprivate.png">
<img src="plasma-5.6/jumplistfirefoxprivate-wee.png" style="border: 0px" width="300" height="121" alt="<?php i18n("Jump List Using Firefox");?>" />
</a>
<figcaption><?php i18n("Jump List Using Firefox");?></figcaption>
</figure>
<figure style="float: right">
<a href="plasma-5.6/jumpliststeam.png">
<img src="plasma-5.6/jumpliststeam-wee.png" style="border: 0px" width="300" height="293" alt="<?php i18n("Jump List Using Steam");?>" />
</a>
<figcaption><?php i18n("Jump List Using Steam");?></figcaption>
</figure>

<p>
<?php i18n("
Not only did we improve interacting with running applications, starting applications gets in your way less, too. Using Jump Lists you can launch an application and jump, hence the name, to a specific task right away. This feature is also present in the application launchers.
");?>
</p>


<br clear="all" />
<h3>
<?php i18n("
Smoother Widgets
");?>
</h3>

<figure style="float: none">
<a href="plasma-5.6/krunner.png">
<img src="plasma-5.6/krunner-wee.png" style="border: 0px" width="300" height="199" alt="<?php i18n("KRunner");?>" />
</a>
<a href="plasma-5.6/folderview.png">
<img src="plasma-5.6/folderview-wee.png" style="border: 0px" width="300" height="252" alt="<?php i18n("Folderview in Panel");?>" />
</a>
<figcaption><?php i18n("KRunner's Smoother look and Folderview in Panel");?></figcaption>
</figure>

<p>
<?php i18n("
There are many refinements to the overall visuals of Plasma in this release. KRunner gained support for drag and drop and lost separator lines to look smoother while icons on the desktop traded the solid label background for a chic drop shadow. Users that place a folder applet in their panel can enjoy improved drag and drop, support for the back button on a mouse as well as choosing between list and icon view. On the more technical side, many small fixes to hi-dpi scaling have found their way into this release.
");?>
</p>
<br clear="all" />
<h3>Weather</h3>

<figure style="float: right">
<a href="plasma-5.6/weather.png">
<img src="plasma-5.6/weather-wee.png" style="border: 0px" width="300" height="245" alt="<?php i18n("Weather Widget");?>" />
</a>
<figcaption><?php i18n("Weather Widget");?></figcaption>
</figure>

<p>
<?php i18n("
Another feature returns from the old days, the weather widget.
");?>
</p>

<br clear="all" />
<h3>
<?php i18n("
On the road to Wayland
");?>
</h3>

<figure style="float: right">
<a href="plasma-5.6/wayland.png">
<img src="plasma-5.6/wayland-wee.png" style="border: 0px" width="300" height="169" alt="<?php i18n("Wayland");?>" />
</a>
<figcaption><?php i18n("Plasma using Wayland");?></figcaption>
</figure>

<p>
<?php i18n("
With Plasma 5.5 for the first time we shipped a Wayland session for you to try out. While we still do not recommend using Wayland as a daily driver, we've made some significant advances:</p><ul>
<li> Window decorations are now supported for Wayland clients giving you a beautiful and unified user experience
    <li> Input handling gained all features you've come to know and love from the X11 world, including 'Focus follows mouse', Alt + mouse button to move and resize windows, etc</li>
    <li> Different keyboard layouts and layout switching</li>
    
");?>
</ul>
    
    
<h3>
<?php i18n("
Tech Preview System Integration Themes
");?>
</h3>

<p>
<?php i18n("
We are trialing a tech preview of Breeze themes for Plymouth and Grub, so Plasma can give you a complete system experience from the moment you turn your computer on.
");?>
</p>

<p>
<?php i18n("
Also previewed in simple systemtray, an experimental systemtray replacement.  Plasma Media Center remains in tech preview but work is ongoing for Plasma 5.7.
");?>
</p>

<a href="plasma-5.5.5-5.6.0-changelog.php">
<?php i18n("Full Plasma 5.6.0 changelog");?></a>

<!-- // Boilerplate again -->

<h2><?php i18n("Live Images");?></h2>

<p><?php print i18n_var("
The easiest way to try it out is with a live image booted off a
USB disk. You can find a list of <a href='%1'>Live Images with Plasma 5</a> on the KDE Community Wiki.
", "https://community.kde.org/Plasma/LiveImages");?></p>

<h2><?php i18n("Package Downloads");?></h2>

<p><?php i18n("Distributions have created, or are in the process
of creating, packages listed on our wiki page.
");?></p>

<ul>
<li>
<?php print i18n_var("<a
href='https://community.kde.org/Plasma/Packages'>Package
download wiki page</a>"
, $release);?>
</li>
</ul>

<h2><?php i18n("Source Downloads");?></h2>

<p><?php i18n("You can install Plasma 5 directly from source. KDE's
community wiki has <a
href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>.
Note that Plasma 5 does not co-install with Plasma 4, you will need
to uninstall older versions or install into a separate prefix.
");?>
</p>

<ul>
<li>
<?php print i18n_var("
<a href='../info/%1.php'>Source Info Page</a>
", $release);?>
</li>
</ul>

<h2><?php i18n("Feedback");?></h2>

<?php print i18n_var("You can give us feedback and get updates on %1 or %2 or %3.", "<a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='facebook.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a>", "<a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='twitter.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a>", "<a href='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='googleplus.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>" );?>

<p>
<?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?></a>
</p>

<p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>#Plasma IRC channel</a>,
<a href='%2'>Plasma-devel mailing list</a> or report issues via
<a href='%3'>bugzilla</a>.  If you like what the
team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided");?></p>

<p><?php i18n("Your feedback is greatly appreciated.");?></p>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
