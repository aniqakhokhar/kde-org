<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.13.90 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.13.90";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='bluedevil' href='https://commits.kde.org/bluedevil'>Bluedevil</a> </h3>
<ul id='ulbluedevil' style='display: block'>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/bluedevil/d79b92f2d872fb03585db1f764ba2d0bd2989def'>Commit.</a> </li>
<li>Edit gitignore. <a href='https://commits.kde.org/bluedevil/61010a5e55c2bd0ff8499c1b161b171783ff0d5e'>Commit.</a> </li>
<li>Remove FileItemActionPlugin in favor of Purpose plugin. <a href='https://commits.kde.org/bluedevil/6fa60e4bf363f33c6d7c646ac3a8cf03410f9260'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14056'>D14056</a></li>
<li>Add license files. <a href='https://commits.kde.org/bluedevil/c9e283a7f14f2debb86615b399c8e9768f4bbc41'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13811'>D13811</a></li>
<li>[Device Monitor] Create KFilePlacesModel on demand. <a href='https://commits.kde.org/bluedevil/688e0492109484cade8aef1f856fe112860dcb01'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13751'>D13751</a></li>
<li>Remove unused entry X-KDE-DBus-ModuleName from the kded plugin metadata. <a href='https://commits.kde.org/bluedevil/ebba20a30a2cb180482901d16adb38565a6684bf'>Commit.</a> </li>
<li>Use more contextual strings for some button labels. <a href='https://commits.kde.org/bluedevil/c7f72367599b8248e69f194243ab95fc315ce841'>Commit.</a> See bug <a href='https://bugs.kde.org/394778'>#394778</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13346'>D13346</a></li>
</ul>


<h3><a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>Disable the title bar separator by default. <a href='https://commits.kde.org/breeze/51cedac0889b4c439f370ce2c4d7c4cd833b1c0e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10429'>D10429</a></li>
<li>[kstyle] Fix deprecation warnings. <a href='https://commits.kde.org/breeze/7e1c0fb1fc3d45c63cbb1c12e75cffc72ed29f0c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15285'>D15285</a></li>
<li>Properly use kpackage_install_package. <a href='https://commits.kde.org/breeze/45ca8d95d543dd11e11e5c29b434668de32df6c8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11909'>D11909</a></li>
<li>Set complete vectors instead of creting them at runtime. <a href='https://commits.kde.org/breeze/c671ea1906455c8322a1a43eb65267817d5169ef'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13591'>D13591</a></li>
<li>Invert shade button by same logic as keep-above button. <a href='https://commits.kde.org/breeze/ebae01db13228231a4c41da96195fb6d43249d3d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14389'>D14389</a></li>
<li>[kstyle] Create shadow tiles on demand. <a href='https://commits.kde.org/breeze/cc24d25a5962d3bb89106e99f46d912bab8c44d4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14898'>D14898</a></li>
<li>Rename libbreezecommon to libbreezecommon5 and don't install .so links. <a href='https://commits.kde.org/breeze/763ec6d335417df7985fbbd4ef456287915b4d1d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14599'>D14599</a></li>
<li>Only include QtQuick support in Breeze KStyle if QtQuick is available. <a href='https://commits.kde.org/breeze/0c7f351623fbfb488b36daf5100b3ce63bd8fca0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12708'>D12708</a></li>
<li>Optimised 5.14 wallpaper filesizes and corrected banding issues. <a href='https://commits.kde.org/breeze/4c8b02ca7bb46258365f9ea9e6e0c6d81a118b02'>Commit.</a> </li>
<li>Plasma 5.14 "Cluster" Wallpaper. <a href='https://commits.kde.org/breeze/6499f93c840f5017e7b1a716e4d62098768efc54'>Commit.</a> </li>
<li>[kdecoration] Delete unused includes. <a href='https://commits.kde.org/breeze/f28e3333591d69e150a511b3d778a07f9ac3656e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14150'>D14150</a></li>
<li>[libbreezecommon] Fix build when qreal is float. <a href='https://commits.kde.org/breeze/b7abb46ee7bf0305aa1803cd405a82b75b00a017'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14090'>D14090</a></li>
<li>[kdecoration] Make shadows lighter. <a href='https://commits.kde.org/breeze/02519ad921d3479855fe8fb9e2848e0d8b460c9d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14133'>D14133</a></li>
<li>[kstyle] Refine shadows. <a href='https://commits.kde.org/breeze/5a15e8ed40e12d68a61389a2601d9259499195f0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11175'>D11175</a></li>
<li>[kdecoration] Refine shadows. <a href='https://commits.kde.org/breeze/54b75015eda87be31029f0d966368f2ee6d89811'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11069'>D11069</a></li>
<li>[libbreezecommon] Add box shadow helper. <a href='https://commits.kde.org/breeze/bffe8faa87068ae7a55b6be19161cd18ceaf6b4c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11198'>D11198</a></li>
<li>Fixed detection of horizontal progressbar by re-introducing check on QStyleOptionProgressBar::orientation. <a href='https://commits.kde.org/breeze/6d2b04fab801f58071fa472be91a40efc7cc7c8c'>Commit.</a> </li>
<li>[kstyle] Don't suppress deprecation warnings. <a href='https://commits.kde.org/breeze/d2c04c6571984dd037dd815b920198e7871b5b7f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12263'>D12263</a></li>
<li>[kstyle] Drop QStyleOptionViewItemV4 in Qt 5 style plugin. <a href='https://commits.kde.org/breeze/3c40e386979618e6a5022ed4404f0dcace3316d8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12259'>D12259</a></li>
<li>[kstyle] Drop QStyleOptionTabWidgetFrameV2 in Qt 5 style plugin. <a href='https://commits.kde.org/breeze/aa4d7d6247f77620b0d3b5d989cbe39338699f99'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12258'>D12258</a></li>
<li>[kstyle] Drop QStyleOptionTabV3 in Qt 5 style plugin. <a href='https://commits.kde.org/breeze/342c05c21813bd15db0de55f54d48cf774edf1c6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12257'>D12257</a></li>
<li>[kstyle] Drop QStyleOptionProgressBarV2 in Qt 5 style plugin. <a href='https://commits.kde.org/breeze/ad50781ec718042679c3f19cc1e728a916f35a6d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12256'>D12256</a></li>
<li>[kstyle] Drop QStyleOptionFrameV3 in Qt 5 style plugin. <a href='https://commits.kde.org/breeze/abc4464fecb313f3b8ba38e60b438f78100095c2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12261'>D12261</a></li>
<li>[kstyle] Drop QStyleOptionFrameV2 in Qt 5 style plugin. <a href='https://commits.kde.org/breeze/6c79210f31a937c25601b23ddb364982bf66bcc4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12260'>D12260</a></li>
<li>[kstyle] Drop QStyleOptionDockWidgetV2 in Qt 5 style plugin. <a href='https://commits.kde.org/breeze/34011b10b5c68df29336183d1d699a7ee241d9fd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12262'>D12262</a></li>
<li>Turn off the extended resize handle/black triangle when there are no borders. <a href='https://commits.kde.org/breeze/47ca4e814e7bee1318baa6c2d3ef127ed3d6406c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13277'>D13277</a></li>
</ul>


<h3><a name='breeze-gtk' href='https://commits.kde.org/breeze-gtk'>Breeze GTK</a> </h3>
<ul id='ulbreeze-gtk' style='display: block'>
<li>Add .arcconfig. <a href='https://commits.kde.org/breeze-gtk/c2ff497c6381dd6cf0ba4a2153bba23f043a0fe0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13220'>D13220</a></li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Fwupd isn't an applications backend. <a href='https://commits.kde.org/discover/874c53e845da21fb000122cce830fb4e03db4738'>Commit.</a> </li>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/discover/8d0d13dff5782d6c81863ab62daddfe92492e427'>Commit.</a> </li>
<li>Better to keep warning. <a href='https://commits.kde.org/discover/11138e5464984351d65884df132d3e541684b4a6'>Commit.</a> </li>
<li>Fix build. <a href='https://commits.kde.org/discover/83d6068baaf6f88fb8f3e374ff211415c8469a66'>Commit.</a> </li>
<li>Add more debug categories. <a href='https://commits.kde.org/discover/a15bab36ab9a56edc9dd905894ba86ab01145d94'>Commit.</a> </li>
<li>Start to use debug categories. <a href='https://commits.kde.org/discover/595001391c6335666302bd92455ad32c6755f965'>Commit.</a> </li>
<li>Create version variable. <a href='https://commits.kde.org/discover/eebd044e3b0f28373dd8439cdb7c175b8833346a'>Commit.</a> </li>
<li>Add new line. <a href='https://commits.kde.org/discover/01f981dbe565f86cac78e4dd11196c70720ca13f'>Commit.</a> </li>
<li>Fixes minor memory leak in the KNSBackend. <a href='https://commits.kde.org/discover/5b2d4af46af634bf04595f78ef71f28ad35d8c88'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15145'>D15145</a></li>
<li>Disable the update button when any backend is busy. <a href='https://commits.kde.org/discover/2bd91b93357318c4f7dd96b2f6e7a07dd74494c8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397938'>#397938</a></li>
<li>Implemented Version checker for fwupd in FindLIBFWUPD.cmake. <a href='https://commits.kde.org/discover/10b790f430526db5ae2a409f8b4190fdafd681cf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14933'>D14933</a></li>
<li>Share appstream screenshots code extraction between flatpak and packagekit. <a href='https://commits.kde.org/discover/1ef4447b9e829bb0dc686ca70a453edd71e6859b'>Commit.</a> </li>
<li>Fwupd: fix compiler warning. <a href='https://commits.kde.org/discover/aeb9e71f4e209992713a9752d173ead0a1e76b8b'>Commit.</a> </li>
<li>Fwupd: better variable scope. <a href='https://commits.kde.org/discover/aeacfe2b11824c8122170938da6a1ac6cf33ffa5'>Commit.</a> </li>
<li>Fwupd: don't return every resource if we don't know about the URL we're given. <a href='https://commits.kde.org/discover/3efdec4a551bf47540dd734e563d9aa40abd2aee'>Commit.</a> </li>
<li>PK: Improve changelog for appstream applications. <a href='https://commits.kde.org/discover/4b1c55098170ea23a2ef847c40b306486edcaa22'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397201'>#397201</a></li>
<li>Fwupd-Backend (Fixes). <a href='https://commits.kde.org/discover/f804580adf566cf49a9267c19588159f27b5a745'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14649'>D14649</a></li>
<li>Fwupd-Integration (Fixes). <a href='https://commits.kde.org/discover/03aeb5ba2077c344b5de38ec37858a3603b07733'>Commit.</a> </li>
<li>Add missing endif(), remove deprecated args in closing conditionals. <a href='https://commits.kde.org/discover/5dbb15a3007bc00abfe85455c8de545dc6fc5de8'>Commit.</a> </li>
<li>Fix Build Fails Due to variable Not set in FINDLIBFWUPD.cmake. <a href='https://commits.kde.org/discover/a55b13c3f3a5ba4ab081dbf5f0493d35a39a743c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14566'>D14566</a></li>
<li>Remove some hardcoded sizes. <a href='https://commits.kde.org/discover/f74ee5fbc1c757b5c5c198edcc14536a9d39cdc1'>Commit.</a> </li>
<li>Fwupd-Backend Integration. <a href='https://commits.kde.org/discover/a30850900bbf9487f08bd43d988267e2ec034fd0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14050'>D14050</a></li>
<li>Don't load categories for invalid backends. <a href='https://commits.kde.org/discover/05d10118d3611dd1db79ae9efe6538811e3139af'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397003'>#397003</a></li>
<li>Unify header and toolbar background color. <a href='https://commits.kde.org/discover/af0c90831d814db9c5a76a6df00cc02ac6a61eb2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397058'>#397058</a></li>
<li>Fix indentation. <a href='https://commits.kde.org/discover/8b8cbe301f91179ba7c8bc5d0deb245beb41b41c'>Commit.</a> </li>
<li>Use a consistent visual style on the Settings page. <a href='https://commits.kde.org/discover/85f6a6a0476ad8db7e75ed92e8ffcefd90d5358a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14557'>D14557</a></li>
<li>Remove unused class. <a href='https://commits.kde.org/discover/0a468792d65505a1afc93cbc1528c33cebeb1c16'>Commit.</a> </li>
<li>Improve Updates page section header appearance. <a href='https://commits.kde.org/discover/735e77c6294533cc5d8bd55bd7ca62f4eee00f2c'>Commit.</a> </li>
<li>Stop enforcing spacings. <a href='https://commits.kde.org/discover/b648825801a30afd4187c6ad244f81e4f2a279e0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396895'>#396895</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14474'>D14474</a></li>
<li>Keep a FlatpakResource::Id instance saved to reuse. <a href='https://commits.kde.org/discover/9f64c14a537fec69fe2c74f22d121a102364a84f'>Commit.</a> </li>
<li>Flatpak: Reduce memory allocations. <a href='https://commits.kde.org/discover/1a8f58762011b40135789034b1f9c6de37a4a87e'>Commit.</a> </li>
<li>Use the right header. <a href='https://commits.kde.org/discover/5e77a146bdd225b5d669629194e6d496a3b0c9ea'>Commit.</a> </li>
<li>Don't recreate the state-related properties when the state changes. <a href='https://commits.kde.org/discover/8a78ef269c4ad2d5e1f73157df97722f1e5d2081'>Commit.</a> </li>
<li>Increase backend header text's left margin. <a href='https://commits.kde.org/discover/3cd95b4a4f4d80826d50dbdd2cc4b2ef783cf54e'>Commit.</a> See bug <a href='https://bugs.kde.org/396833'>#396833</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14385'>D14385</a></li>
<li>Flatpak: simplify internal identification of resources into a hash. <a href='https://commits.kde.org/discover/ba61b469bc1c84da18411e35a79ffd2802eeeaa1'>Commit.</a> </li>
<li>Flatpak: fix clang warning. <a href='https://commits.kde.org/discover/4c65d034b7a61469631e6a58c6634291df16ea80'>Commit.</a> </li>
<li>Flatpak: remove unused data field. <a href='https://commits.kde.org/discover/437ecaee94c73d611e38fb561c09aa1609cd8db1'>Commit.</a> </li>
<li>Flatpak: Remove unused function. <a href='https://commits.kde.org/discover/1da8945de3385df5d582e37d31fc3091794c6a49'>Commit.</a> </li>
<li>Flatpak: Make sure we cancel ongoing transactions on shutdown. <a href='https://commits.kde.org/discover/f55fd33c294284a0a1e64c0c7c626e3bb0f853d4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396730'>#396730</a></li>
<li>Fix clang warning. <a href='https://commits.kde.org/discover/8303e5d1dec146a44d9d7fa7f36cee0d39306527'>Commit.</a> </li>
<li>Add warning to make the cc'd bug below easier to notice. <a href='https://commits.kde.org/discover/47c1c036d49c14fd84e68370f46c0dcf0007d300'>Commit.</a> See bug <a href='https://bugs.kde.org/396832'>#396832</a></li>
<li>Sort update items by completion. <a href='https://commits.kde.org/discover/42083fa8ac72d88f8f4f2cc64b7e272347f9f8eb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390911'>#390911</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14250'>D14250</a></li>
<li>Respect BUILD_TESTING. <a href='https://commits.kde.org/discover/43ab6b126d282939c0b5aa19c1818c30bd3e04eb'>Commit.</a> </li>
<li>Make the snap source less of a weird thing. <a href='https://commits.kde.org/discover/2a8cabccf854e28eeaffadd832a2d0ab222c9273'>Commit.</a> </li>
<li>Polish the code change for the previous commit. <a href='https://commits.kde.org/discover/ef66f3d0f8947f15d9a149d27b104664d64c2b1f'>Commit.</a> </li>
<li>Polish Notifier Plasmoid's UI. <a href='https://commits.kde.org/discover/ee062be1ca0e7fc16399748e08cddbd805a0042f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14238'>D14238</a></li>
<li>Fix build with older versions of Snap. <a href='https://commits.kde.org/discover/b51f759b9e3666ce41491df714ae79e1c8ba0dbb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396508'>#396508</a></li>
<li>Fix clazy warnings. <a href='https://commits.kde.org/discover/1c2edcd90b7a405406a50ea4a96459fe13f3521c'>Commit.</a> </li>
<li>Initialize the reboot state if it already needed reboot. <a href='https://commits.kde.org/discover/05a6d49133aa6444de969bc682bc63865028e26f'>Commit.</a> </li>
<li>Have properties change with the state change. <a href='https://commits.kde.org/discover/8fa275eafdb9f5e970bff4d93012c24e79d93c9a'>Commit.</a> </li>
<li>Properly compute the reboot state. <a href='https://commits.kde.org/discover/ab6b68948ce80f1e9abb5ed426b9a5ba4289b7c3'>Commit.</a> </li>
<li>Attempt at making less refreshes. <a href='https://commits.kde.org/discover/f9b17f26591582fa84aeb5ba1aacd640170914d3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14059'>D14059</a></li>
<li>Require an update when the offline-update-action is enabled. <a href='https://commits.kde.org/discover/c78bbf9771f277359ff70435ba8f50578bfb5829'>Commit.</a> </li>
<li>Properly brace if. <a href='https://commits.kde.org/discover/2f80d7ce8472e0afcece3e0c3a4be26abf3085ec'>Commit.</a> </li>
<li>Include a "needs reboot" state in the discover notifier. <a href='https://commits.kde.org/discover/d082ab025594eac59eee7e83ee3a33853a8f80ce'>Commit.</a> </li>
<li>Show a message saying the system needs rebooting. <a href='https://commits.kde.org/discover/8f0fb642058c9fe451ec00f0452b4e8611095987'>Commit.</a> </li>
<li>Style++. <a href='https://commits.kde.org/discover/faaf62835b70000a6253ed3bc855a6584f3d6203'>Commit.</a> </li>
<li>Prefer deprecated but available API. <a href='https://commits.kde.org/discover/fda5b6f2ccf26ac43446b37c26cbf920ac784565'>Commit.</a> </li>
<li>Snap: Support configuring used snap channels. <a href='https://commits.kde.org/discover/67293df0f26ff8a4497dc5596df3a8aa1a04f021'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389012'>#389012</a></li>
<li>Just show the license name as the button caption. <a href='https://commits.kde.org/discover/17ef1f5414d2bc69027d19a73bb959df61ab6275'>Commit.</a> </li>
<li>Snap: Use the right kirigami api. <a href='https://commits.kde.org/discover/7da60f3a4053c4d4414ade050279d960f333e687'>Commit.</a> </li>
<li>Fix path. <a href='https://commits.kde.org/discover/56b92ec52771a2df255d45ccfa766b7c9df4200f'>Commit.</a> </li>
<li>Make it possible to copy urls from a url into the clipboard. <a href='https://commits.kde.org/discover/96f3b700dedb4eebca5721cbb2e8d5f0423683be'>Commit.</a> </li>
<li>Remove copy-pasted code into a UrlButton new component. <a href='https://commits.kde.org/discover/13f5d531e8802b38dc76440ee732aa57a84fa3ad'>Commit.</a> </li>
<li>Revert "Snap: Port use of deprecated API". <a href='https://commits.kde.org/discover/0901e3492389b42795978b5fd1f1f8efc4c448c5'>Commit.</a> </li>
<li>Snap: Prefer deprecated but backwards-compatible version. <a href='https://commits.kde.org/discover/01d3b7e2960c258ad68b3cd9a001a53537b5e711'>Commit.</a> </li>
<li>Don't use ::commonIds if snap is too old. <a href='https://commits.kde.org/discover/961df98e91ad6417778fd0c84af79fbaf08b121f'>Commit.</a> </li>
<li>Use i18np for a plural string. <a href='https://commits.kde.org/discover/d70256447042d33ccab047d3f5c9fba0d6f9c8a5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395951'>#395951</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13785'>D13785</a></li>
<li>Snap: support looking up by appstream id. <a href='https://commits.kde.org/discover/dee02d6950782ac1ca53ed41e339f3fc0122b933'>Commit.</a> </li>
<li>Snap: Reserve vector size when we know the size. <a href='https://commits.kde.org/discover/2b79366e054d419a392f7a993469e1a3efd26481'>Commit.</a> </li>
<li>Snap: Port use of deprecated API. <a href='https://commits.kde.org/discover/6b35e8e046f8100d16ebaf5f206f5e5d15144028'>Commit.</a> </li>
<li>Adapt to new syntax for the header. <a href='https://commits.kde.org/discover/d5f7c102854c41e4ee9eb00cfb979993f2396233'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395455'>#395455</a></li>
<li>Small cleanup. <a href='https://commits.kde.org/discover/cde88d111e1a25810adc19c579afeeadb6422da0'>Commit.</a> </li>
<li>No need to keep the objects inside every resource. <a href='https://commits.kde.org/discover/9778f1f9da158d2c741335312ef77994b1821fc7'>Commit.</a> </li>
<li>Overhaul presentation of link buttons in the Reviews section. <a href='https://commits.kde.org/discover/5ee0a9c8c4cbd8a723e1295d5cad6dd582196bde'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13678'>D13678</a></li>
<li>Show release date inline with version string. <a href='https://commits.kde.org/discover/c4a74b532cd2d05fd419757076d11af0ba0f3cad'>Commit.</a> See bug <a href='https://bugs.kde.org/395677'>#395677</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13656'>D13656</a></li>
<li>Include the release date in the application page if available. <a href='https://commits.kde.org/discover/8f70aa711a1519543f1ced735424f0ce9721049f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395677'>#395677</a></li>
<li>Do not duplicate work of KAboutData::setupCommandLine(). <a href='https://commits.kde.org/discover/2d1a010e3050b369a46a863cbf6736764d7779ef'>Commit.</a> </li>
<li>Improve the clarity and presentation of the update details text field. <a href='https://commits.kde.org/discover/1e31a6e8d52481326ccec866498eb4af83d74467'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13367'>D13367</a></li>
<li>Allow Compact view with narrower windows. <a href='https://commits.kde.org/discover/0403d9e8a63bd958059d8970732127186565ec0c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13361'>D13361</a></li>
<li>Fix message placeholder. <a href='https://commits.kde.org/discover/4760c92282234507a661d25c2e17326e9de09f41'>Commit.</a> </li>
<li>Improve look of the dependencies and permissions popup. <a href='https://commits.kde.org/discover/786f4ffa90d73f46332196a6afff10182af6335f'>Commit.</a> </li>
<li>--debug. <a href='https://commits.kde.org/discover/d0c8c5ec0e08b2063d1a075c997f8037fe7bad64'>Commit.</a> </li>
<li>Make update warning about removal more verbose. <a href='https://commits.kde.org/discover/66bd5e3935db2bb3cfdd08bcd182b9bf5f279e58'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387934'>#387934</a></li>
<li>PK: Add a LinkButton to show dependencies. <a href='https://commits.kde.org/discover/a14e99c80837840be63b801ee73e77c70817d4cb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383514'>#383514</a></li>
<li>When we know the plugin is missing, offer it. <a href='https://commits.kde.org/discover/7f61173abeb6d6e99fc2a2776b3a9827985be8b6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387844'>#387844</a></li>
</ul>


<h3><a name='drkonqi' href='https://commits.kde.org/drkonqi'>drkonqi</a> </h3>
<ul id='uldrkonqi' style='display: block'>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/drkonqi/b023fb8f684efcb61febf47221bd3dd6b1193fd3'>Commit.</a> </li>
<li>Mingw compile fix. <a href='https://commits.kde.org/drkonqi/7251f82347302b0dc3c62559f135e17937381da3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396821'>#396821</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14423'>D14423</a></li>
<li>Fix signal/slot. <a href='https://commits.kde.org/drkonqi/08e4b4dff8d814190033b74e7564c3cac4af0123'>Commit.</a> </li>
<li>Use KPasswordLineEdit. <a href='https://commits.kde.org/drkonqi/913adb0df3c9be9d1378e0c790f7d3ea0091c662'>Commit.</a> </li>
<li>Fix enable/disable widget. <a href='https://commits.kde.org/drkonqi/d06646c11914470a1768ca7de2b61a0c239b7b0e'>Commit.</a> </li>
<li>Fix warning. <a href='https://commits.kde.org/drkonqi/157184544c2bd821827aae1fd8e25272086be4e5'>Commit.</a> </li>
<li>Add Exec= key. <a href='https://commits.kde.org/drkonqi/30a36e188aa1e75257c9daa160a316671bc083f5'>Commit.</a> </li>
</ul>


<h3><a name='kactivitymanagerd' href='https://commits.kde.org/kactivitymanagerd'>kactivitymanagerd</a> </h3>
<ul id='ulkactivitymanagerd' style='display: block'>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/kactivitymanagerd/f814096cfa4c7b5999e1be7ca175d8ce0b2c06fa'>Commit.</a> </li>
<li>Enabled the global shortcuts plugin by default. <a href='https://commits.kde.org/kactivitymanagerd/23d2d2f5191c5f1253a9540ca5f991fc46068217'>Commit.</a> </li>
<li>Remove unused KAMD_DATA_DIR variables. <a href='https://commits.kde.org/kactivitymanagerd/35b1c1fe43ca94251b77047050197982ff085b26'>Commit.</a> </li>
<li>Use (non-deprecated) KDEInstallDirs variables. <a href='https://commits.kde.org/kactivitymanagerd/2e9035fbf1d90ebe102411de8bf0a2942e13c0cd'>Commit.</a> </li>
<li>Use KDE_INSTALL_FULL_BINDIR instead of hardcoding bin/ subdir. <a href='https://commits.kde.org/kactivitymanagerd/e7efeabb7f9b842593c5fa7641fb0affad6a76d3'>Commit.</a> </li>
<li>Remove unneeded include_directories. <a href='https://commits.kde.org/kactivitymanagerd/c41fd1d50b549cc3e7f9dd8323ff32dfcbf472e0'>Commit.</a> </li>
<li>No need to set CMAKE_INCLUDE_CURRENT_DIR, done by KDECMakeSettings. <a href='https://commits.kde.org/kactivitymanagerd/fa5779b97a51d0a2d6bddab6711559997e6c7253'>Commit.</a> </li>
<li>Add some QT_ definitions to enforce current strict state. <a href='https://commits.kde.org/kactivitymanagerd/ef5056720f5bbc1b4e30dc0c7ea76712d2a5a3b4'>Commit.</a> </li>
<li>Use override. <a href='https://commits.kde.org/kactivitymanagerd/aab8bb4cfd443b032c70e4bfa1e126faa6bd1e38'>Commit.</a> </li>
<li>Use nullptr. <a href='https://commits.kde.org/kactivitymanagerd/0909007ae0fda6ed01177ff2f9b065e96ba064a4'>Commit.</a> </li>
<li>Fix typo in assert messages. <a href='https://commits.kde.org/kactivitymanagerd/55c17c05d85980a6bedcae2bd528b1acae47e6f2'>Commit.</a> </li>
<li>Use more explicit. <a href='https://commits.kde.org/kactivitymanagerd/086e32acf89f7c57dbb9f53d297c08faeaa9a097'>Commit.</a> </li>
<li>No need to set CMAKE_AUTOMOC ourselves, done by KDECMakeSettings. <a href='https://commits.kde.org/kactivitymanagerd/602ac9928e77e69716ed3e4d98c631dc883d290c'>Commit.</a> </li>
<li>Use ecm_setup_qtplugin_macro_names for KAMD_EXPORT_PLUGIN. <a href='https://commits.kde.org/kactivitymanagerd/4414ca9af8b33f798cd142dc354d40a17782a847'>Commit.</a> </li>
<li>No need to add explicitly ECM_KDE_MODULE_DIR, part of ECM_MODULE_PATH. <a href='https://commits.kde.org/kactivitymanagerd/53d2f4c32201e3a3f8225771106fafb7d9deeeda'>Commit.</a> </li>
<li>Use KF5_MIN_VERSION also for ECM, now that it's part of KF. <a href='https://commits.kde.org/kactivitymanagerd/3e0a23a5fd87deb43520f3f07ab9c3cb1c43f0e0'>Commit.</a> </li>
<li>Remove unneeded cmake macro includes. <a href='https://commits.kde.org/kactivitymanagerd/d2720520f2303489da985c08624611991a8cf85f'>Commit.</a> </li>
<li>Require KDE Frameworks 5.42 and Qt 5.9. <a href='https://commits.kde.org/kactivitymanagerd/2b906e052e4ca637a9693ce7dce2a0c277e0fbe0'>Commit.</a> </li>
<li>Bump min cmake version to 3.0. <a href='https://commits.kde.org/kactivitymanagerd/767afac0f7fcfdc0717277c20bf378641e9809f3'>Commit.</a> </li>
</ul>


<h3><a name='kde-cli-tools' href='https://commits.kde.org/kde-cli-tools'>kde-cli-tools</a> </h3>
<ul id='ulkde-cli-tools' style='display: block'>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/kde-cli-tools/fd6fe2f2a0f8409829b77af89a86522fb83fbaa0'>Commit.</a> </li>
<li>[KEditFileType] Show application icon in preference list. <a href='https://commits.kde.org/kde-cli-tools/3a8ef4cf6d0275311df63cadbd14ee0d78666c00'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14204'>D14204</a></li>
<li>Make build with strict compile flags. <a href='https://commits.kde.org/kde-cli-tools/d95c4efc2263688ccafe8ba7d651c635b643685a'>Commit.</a> </li>
</ul>


<h3><a name='kdecoration' href='https://commits.kde.org/kdecoration'>KDE Window Decoration Library</a> </h3>
<ul id='ulkdecoration' style='display: block'>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/kdecoration/69d20d4b7d91be42aa6c52090dbe406805db642d'>Commit.</a> </li>
<li>Check against QRect whether pointer is inside DecorationButton. <a href='https://commits.kde.org/kdecoration/c9cfd840137b65bc7391f301f4f63da6df9de859'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15226'>D15226</a></li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>[Spellchecker Runner] Add button to open Spell Check KCM. <a href='https://commits.kde.org/kdeplasma-addons/1d99daa41fd6d5b596a849205cecfb8efe193ee4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398244'>#398244</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15427'>D15427</a></li>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/kdeplasma-addons/69dffdd82b2aa63b03f42817a81fa8ed8a8f7bf6'>Commit.</a> </li>
<li>Show the state as well on the full representation. <a href='https://commits.kde.org/kdeplasma-addons/5eecd4d82434fd780bf4d0b098caceb6c9c195cd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397365'>#397365</a></li>
<li>[Timer applet] Reset opacity on timer start and reset. <a href='https://commits.kde.org/kdeplasma-addons/28fe01674f6c9a7fd644f9a753665f52d3188eaf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396497'>#396497</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14701'>D14701</a></li>
<li>Changed variable type to int to display squares instead of rectangles. <a href='https://commits.kde.org/kdeplasma-addons/0aa141ad3a202f369c19c55dbf3f73abb6bd798e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14377'>D14377</a></li>
<li>Changed refresh time from 30 to 60 seconds. <a href='https://commits.kde.org/kdeplasma-addons/9eae090a3038ebf97ffeee8c9b773a8610e13021'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14307'>D14307</a></li>
<li>Cleaned code for binary clock applet and added comments. <a href='https://commits.kde.org/kdeplasma-addons/b5551c17f4e4d832ea8510449d29d1d3df3957ab'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14267'>D14267</a></li>
<li>New icons for Keyboard Indicator Closes T9050. <a href='https://commits.kde.org/kdeplasma-addons/05b0efa3d97c4718cb32f48e5e0ef9249c82b78d'>Commit.</a> </li>
<li>Show info if we have this info :). <a href='https://commits.kde.org/kdeplasma-addons/174d7e9e6e52e94ea09215d1d736221cd15f0ebd'>Commit.</a> </li>
<li>Centre align SystemLoadViewer headers. <a href='https://commits.kde.org/kdeplasma-addons/11e9f3d16c069cc74475d0fd50c73f6bd51805fb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13292'>D13292</a></li>
</ul>


<h3><a name='kgamma5' href='https://commits.kde.org/kgamma5'>Gamma Monitor Calibration Tool</a> </h3>
<ul id='ulkgamma5' style='display: block'>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/kgamma5/53ead5b48fd0c372b6efa6799cc35782ac6de1ed'>Commit.</a> </li>
<li>Fix compile with strict compile flags. <a href='https://commits.kde.org/kgamma5/c13d9950036a030a5f41e30feb75dddbe911a5f9'>Commit.</a> </li>
</ul>


<h3><a name='khotkeys' href='https://commits.kde.org/khotkeys'>KDE Hotkeys</a> </h3>
<ul id='ulkhotkeys' style='display: block'>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/khotkeys/c3d49873ab9a7beab0357080372164a3c1f08e24'>Commit.</a> </li>
<li>Remove unused entry X-KDE-DBus-ModuleName from the kded plugin metadata. <a href='https://commits.kde.org/khotkeys/ffc5ab601876c269c09f830b6f828d546c3af59d'>Commit.</a> </li>
<li>Make Apply/Discard settings prompt consistent with rest of settings. <a href='https://commits.kde.org/khotkeys/ac133254b76167f7eaa5e953190a8a306aed94cf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12928'>D12928</a></li>
</ul>


<h3><a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a> </h3>
<ul id='ulkinfocenter' style='display: block'>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/kinfocenter/2b3c5b2291138a73d86799c7786596138f02061f'>Commit.</a> </li>
<li>Add "Copy Info" button to the About System KCM. <a href='https://commits.kde.org/kinfocenter/c0f28efd280f4c22c1fc061c32827086c1c52388'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366266'>#366266</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7087'>D7087</a></li>
<li>Fix minor EBN issues. <a href='https://commits.kde.org/kinfocenter/5d81ddbf469baae39a375beeabdf0f5911688447'>Commit.</a> </li>
<li>Move "update" button from the left side to the right side in NIC module. <a href='https://commits.kde.org/kinfocenter/e3f24f95966bbf11ae8ad772f4d172ebddc68f46'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14737'>D14737</a></li>
<li>Add QStringLiteral when necessary. <a href='https://commits.kde.org/kinfocenter/2c5399f5f8c75b26461b096c9cff327a5eaaeb94'>Commit.</a> </li>
<li>Increase version as for other apps. <a href='https://commits.kde.org/kinfocenter/60fc3b4a764b0584fc721a109a4f0b65f35dc36b'>Commit.</a> </li>
<li>Warning--. <a href='https://commits.kde.org/kinfocenter/852fb0da88c44d72a2226e280518785b43ea10bb'>Commit.</a> </li>
<li>Use QStringLiteral where it's possible. <a href='https://commits.kde.org/kinfocenter/5db30eca181948b82b8cce6dc98b7c84f1822a81'>Commit.</a> </li>
<li>Implement sorting of the device tree items. <a href='https://commits.kde.org/kinfocenter/7c0f85162ee978753e7bbe726455b7388023afb6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380355'>#380355</a>. Phabricator Code review <a href='https://phabricator.kde.org/D7155'>D7155</a></li>
<li>Do not duplicate work of KAboutData::setupCommandLine(). <a href='https://commits.kde.org/kinfocenter/ed7850bad1b3c424456a975ddf8805a97e7a840d'>Commit.</a> </li>
<li>Fix wrong full form of acronym SMB in kinfocenter docbook. <a href='https://commits.kde.org/kinfocenter/f40ba4b48f341b350d7e3def9560ee21e1967412'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13240'>D13240</a></li>
<li>Improve single-instance application behavior. <a href='https://commits.kde.org/kinfocenter/cc3e7450b506724fcc005f821ef000ccd6c2d63e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12760'>D12760</a></li>
</ul>


<h3><a name='kmenuedit' href='https://commits.kde.org/kmenuedit'>KMenuEdit</a> </h3>
<ul id='ulkmenuedit' style='display: block'>
<li>Cmake: look for DocTools & Init frameworks. <a href='https://commits.kde.org/kmenuedit/431cfc8afd07631bce09282d7c1bee9880dbe781'>Commit.</a> </li>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/kmenuedit/70dd39e17a6fc2ce431a647051289bca10f47384'>Commit.</a> </li>
<li>Update screenshots. <a href='https://commits.kde.org/kmenuedit/a2fd37e7171c8ce87b1cb280b2740b4e19ab1a14'>Commit.</a> </li>
<li>Doc: Delete info about removed checkbox "Place in system tray". <a href='https://commits.kde.org/kmenuedit/c8d7e854738ae598d2937b8c2bb62803332d5100'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14007'>D14007</a></li>
<li>Do not duplicate work of KAboutData::setupCommandLine(). <a href='https://commits.kde.org/kmenuedit/3b0a957a769a780fe940c9dd79d8e425d7625137'>Commit.</a> </li>
</ul>


<h3><a name='kscreen' href='https://commits.kde.org/kscreen'>KScreen</a> </h3>
<ul id='ulkscreen' style='display: block'>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/kscreen/bdbde6e1216825abeec69a79f9b15031c1a71564'>Commit.</a> </li>
<li>Add applet with screen layouts and presentation mode. <a href='https://commits.kde.org/kscreen/b4baa322c3610766a5a7f38e9a501a18a2e7b051'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14855'>D14855</a></li>
<li>Add screen to the keyword of kscreen KCM. <a href='https://commits.kde.org/kscreen/24f041478771dc10c9013af147cd03bed3b3d4e5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14950'>D14950</a></li>
<li>QMLScreen: do not declare the engine a member. <a href='https://commits.kde.org/kscreen/a6929de88b89f4b92dbecc7977a0759621d36bac'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14476'>D14476</a></li>
<li>Do not leak all instances of QMLOutput when QMLScreen is destroyed. <a href='https://commits.kde.org/kscreen/ff717c0a9c4ca65b3dc50f932d1b097ca513b4d2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14478'>D14478</a></li>
<li>Do not leak Widget::ui. <a href='https://commits.kde.org/kscreen/749ab17d5e482fce2861e5bec2e3815d1dfe2f93'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14477'>D14477</a></li>
<li>Use nullptr instead of Q_NULLPTR. <a href='https://commits.kde.org/kscreen/098ae52e0086decb55fa06a4b6edcf5645a0d58f'>Commit.</a> </li>
<li>Fix typos. <a href='https://commits.kde.org/kscreen/49ea560a67b41953d68e2f63ca7d9b7811deea8f'>Commit.</a> </li>
<li>Remove unused function. <a href='https://commits.kde.org/kscreen/ed42cf46ae2c38233125e13bf2f6d32ce420d488'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14361'>D14361</a></li>
<li>Create m_saveTimer lazily. <a href='https://commits.kde.org/kscreen/f4b0d85fe061aed6b1b6558d1356542572047684'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14359'>D14359</a></li>
<li>Fix layout of resolution selection. <a href='https://commits.kde.org/kscreen/5b6e9767a73ec57b48115f32336af406c9b5f448'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14326'>D14326</a></li>
<li>Change screen orientation label from Normal to No Rotation. <a href='https://commits.kde.org/kscreen/6dd05a592d484f5b5f16e4a8619911b0b9f8854f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14403'>D14403</a></li>
<li>Change rotation icons to be a rotated preview icon. <a href='https://commits.kde.org/kscreen/200cdd782e5f3aad4ac7121edd983f07c39f4ae8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14339'>D14339</a></li>
<li>Make variable name consistent. <a href='https://commits.kde.org/kscreen/e40e747c4a13bae6ebfab8e0a80aa58d41271c38'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14358'>D14358</a></li>
<li>Remove Advanced section from screen config. <a href='https://commits.kde.org/kscreen/51c93ef1f9eb583652175dfe6dbefb090a69e4a5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14325'>D14325</a></li>
<li>Handle keyboard in action selector OSD. <a href='https://commits.kde.org/kscreen/bb6c51b0e858b276239f69902bb6fb98f67323af'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395804'>#395804</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14165'>D14165</a></li>
<li>Make the action selector OSD independent of the other OSDs. <a href='https://commits.kde.org/kscreen/5238ae3a7251adea15a18c24fb4201b764364ac1'>Commit.</a> See bug <a href='https://bugs.kde.org/395804'>#395804</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14143'>D14143</a></li>
<li>Use hideOsd function instead of its code copied. <a href='https://commits.kde.org/kscreen/ea63b2ce6dbfffb7db9b5a89a9c63a95026232f2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14225'>D14225</a></li>
<li>Make OSD buttons' accessible. <a href='https://commits.kde.org/kscreen/9431bd13fb24f7f544c6a48160658ac1de764d45'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14228'>D14228</a></li>
<li>Fix pre-selected refresh rate. <a href='https://commits.kde.org/kscreen/61ebc0208396a80c7a719d628d1007dba2d8b790'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14328'>D14328</a></li>
<li>Fix duplicate object name in ui file. <a href='https://commits.kde.org/kscreen/0597035f8611a0b17544461ba54856410e40447c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14223'>D14223</a></li>
<li>Simplify if/else that all make the same call. <a href='https://commits.kde.org/kscreen/64e03b1e7b2b4e4d6ebe4c3d56fa830ab9a32911'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14213'>D14213</a></li>
<li>Turn OsdManager into a regular class. <a href='https://commits.kde.org/kscreen/58f5bb623d14fcc6f1c0f750e1a20a6ee371a91d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14166'>D14166</a></li>
<li>Remove ref from returned QString. <a href='https://commits.kde.org/kscreen/97f7b9cff7822a463dd0d8f97ed2ef0853393c45'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14152'>D14152</a></li>
<li>Use QStringLiteral when a conversion to QString will happen. <a href='https://commits.kde.org/kscreen/ccbee3e89925d2b3277d246a8834bb8edab2fde4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14076'>D14076</a></li>
<li>Use normal connection to slot instead of lambda. <a href='https://commits.kde.org/kscreen/5327746f81c500e8cd7d6830155ec23f96232fae'>Commit.</a> </li>
<li>In function returning void, use plain return. <a href='https://commits.kde.org/kscreen/f62f316ab207e8f0cf63deabeb7d482529efcd06'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14075'>D14075</a></li>
<li>Use context argument in lambda connect. <a href='https://commits.kde.org/kscreen/439feca241e882fa4645dec9a902ce6611454697'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14074'>D14074</a></li>
<li>Improve wording in comment. <a href='https://commits.kde.org/kscreen/94d19d7e335f77d8c2de9de29b380f568534a3dd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14073'>D14073</a></li>
<li>Be strict about implicit string casts. <a href='https://commits.kde.org/kscreen/e9cca953e8bc4b803e1f8d35398c8dd435d7a0ad'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14065'>D14065</a></li>
<li>Fixuifiles. <a href='https://commits.kde.org/kscreen/88843a4e32b9bc8e2fc29eb4675f0b1ced2071fe'>Commit.</a> </li>
<li>Move scale and unify button to the top, next to primary combo. <a href='https://commits.kde.org/kscreen/b01b567296b70b9b593eaa438d86dffe7b663afa'>Commit.</a> </li>
<li>Move KCM to UI file. <a href='https://commits.kde.org/kscreen/ce68ee44e0990370204e21cf501a05f25bb9410a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14024'>D14024</a></li>
<li>Make Primary Combo a regular QComboBox. <a href='https://commits.kde.org/kscreen/77ee982118957f3dba33e2ed4d0fd42b6b90e396'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14023'>D14023</a></li>
<li>Move ECM & Co. macro includes before finding Qt & KF. <a href='https://commits.kde.org/kscreen/615b0353f29462125680766f1da30697dd19ea43'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14031'>D14031</a></li>
<li>Fix Don't call QList::operator[]() on temporary. <a href='https://commits.kde.org/kscreen/644bc8c57fe739b93f72ba472d7ac654c9c033ec'>Commit.</a> </li>
<li>Use range based for. <a href='https://commits.kde.org/kscreen/c969d711afde5b77dc4f04fb1b4cab4385f41c62'>Commit.</a> </li>
<li>Clean up initializers. <a href='https://commits.kde.org/kscreen/743c6039d12007ca0c4ef84ffaf610d40e906de2'>Commit.</a> </li>
<li>Use include guard matching the file name. <a href='https://commits.kde.org/kscreen/946613df0aa8ec973c9a32627f1f3c6df2880235'>Commit.</a> </li>
<li>Remove duplicated include. <a href='https://commits.kde.org/kscreen/5d8901385457269a444c4f3c4bab5c3169e28e4b'>Commit.</a> </li>
<li>Use consistently override. <a href='https://commits.kde.org/kscreen/6fbc81454eef59024390e1e11d150338932c33f0'>Commit.</a> </li>
<li>Use more explicit. <a href='https://commits.kde.org/kscreen/78eb5f09b86b5693d4af7ace00f77a4240aeca94'>Commit.</a> </li>
<li>Use more nullptr. <a href='https://commits.kde.org/kscreen/7fbcfc41d1e770c91f752be1297fc5d8855cb123'>Commit.</a> </li>
<li>Remove unneeded/no longer existing include dirs. <a href='https://commits.kde.org/kscreen/3ccc098e09b65c5d6e97d4e27515c76c482546ba'>Commit.</a> </li>
<li>Remove module prefixes from Qt includes. <a href='https://commits.kde.org/kscreen/55533e0cb4657919142eb0763b4f5fb39cdac3a6'>Commit.</a> </li>
<li>Use non-deprecated KDEInstallDirs variables. <a href='https://commits.kde.org/kscreen/d93a85cc78747b762b48dd79df03fb43d147b8e7'>Commit.</a> </li>
<li>Use KF5_MIN_VERSION also for ECM, now that it's part of KF. <a href='https://commits.kde.org/kscreen/b81e025c51104b63de82da0c7178567b42b4320b'>Commit.</a> </li>
<li>Use add_test signature where target command gets resolved to binary path. <a href='https://commits.kde.org/kscreen/1cbe9d412ff511325d5deaa5977f59289992981f'>Commit.</a> </li>
<li>Require KDE Frameworks 5.42 and Qt 5.9. <a href='https://commits.kde.org/kscreen/36c521215afe91d0cbc843fe9af0c085252d32fa'>Commit.</a> </li>
<li>Bump min cmake version to 3.0, move cmake_minimum_required to begin. <a href='https://commits.kde.org/kscreen/f51fa99e34c08c459eb5135e8a74586ee2665b0a'>Commit.</a> </li>
<li>Remove trivial destructors. <a href='https://commits.kde.org/kscreen/798605b5a9c50152bc3957919ecefe6b9820147e'>Commit.</a> </li>
<li>Return Model name from EDID even when Vendor is empty. <a href='https://commits.kde.org/kscreen/2581892e928f666ead0fff25c453696843fd8d35'>Commit.</a> </li>
<li>Simple cleanup. <a href='https://commits.kde.org/kscreen/d5a08db06c7388692378385a0eb5b65556d8c1d1'>Commit.</a> </li>
<li>Be consistent about initializers. <a href='https://commits.kde.org/kscreen/01297182ba163dac161565669123a7dc017cf45d'>Commit.</a> </li>
<li>Prefer override over virtual. <a href='https://commits.kde.org/kscreen/3716d792150b750e24acbeee3f0f0d61bf9a06bc'>Commit.</a> </li>
<li>Remove redundant initializer. <a href='https://commits.kde.org/kscreen/204ce92d9134ffda9c4afc4e72db3755c56bf3f5'>Commit.</a> </li>
<li>Remove random include. <a href='https://commits.kde.org/kscreen/bafe80ec733d3b71cbc78d5ff38dae3ac20e23e1'>Commit.</a> </li>
<li>Remove left-over comment. <a href='https://commits.kde.org/kscreen/e08ff396c4402ce1319be09a8ff6d9fa88c0dc7f'>Commit.</a> </li>
<li>Remove unused entry X-KDE-DBus-ModuleName from the kded plugin metadata. <a href='https://commits.kde.org/kscreen/342ec0ec48d05baf387bcc95000898f788254172'>Commit.</a> </li>
</ul>


<h3><a name='kscreenlocker' href='https://commits.kde.org/kscreenlocker'>KScreenlocker</a> </h3>
<ul id='ulkscreenlocker' style='display: block'>
<li>Fix the plasma-workspace build. <a href='https://commits.kde.org/kscreenlocker/36a7e0313b153cc0c1598fa4e3254ee5ef2533d7'>Commit.</a> </li>
<li>Export install location for DBUS interfaces via CMake. <a href='https://commits.kde.org/kscreenlocker/bfd3fe9cbe0282ef8701bd9ae4b8182f235cc868'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15228'>D15228</a></li>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/kscreenlocker/2bfdccb0e8fb442943d95af15c31754fb63365bc'>Commit.</a> </li>
<li>Install our org.kde.screensaver interface definition. <a href='https://commits.kde.org/kscreenlocker/a1960594cba760b6a67da450c372d5ea75cd101d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15186'>D15186</a></li>
<li>Load QtQuickSettings for software rendering. <a href='https://commits.kde.org/kscreenlocker/3f104cbb0a34024dc1aa45538d2ef2403a7a45f0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14708'>D14708</a></li>
<li>Force software rendering when greeter crashed. <a href='https://commits.kde.org/kscreenlocker/875e44f56d4597ba3b65f5bf4ff2b315376c05a4'>Commit.</a> </li>
<li>Fix build, remove some implicit QString casts. <a href='https://commits.kde.org/kscreenlocker/540b9d13e9204e2bc042cd948e4be936a7f3dba8'>Commit.</a> </li>
<li>Make it compile with strict compile flags. <a href='https://commits.kde.org/kscreenlocker/bb760c771affa12a8b428c4059ae1d424a840911'>Commit.</a> </li>
<li>Add explicit. <a href='https://commits.kde.org/kscreenlocker/c7816f74845aaa86e9f9d8dc138fca30b8a84916'>Commit.</a> </li>
</ul>


<h3><a name='ksshaskpass' href='https://commits.kde.org/ksshaskpass'>KSSHAskPass</a> </h3>
<ul id='ulksshaskpass' style='display: block'>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/ksshaskpass/81b17baeac9d26d607c3984d66ad1e3e76e5bb94'>Commit.</a> </li>
<li>Add qt/kf5 version. <a href='https://commits.kde.org/ksshaskpass/b57d7e35b844cd8367912cfba265ce47087288a9'>Commit.</a> </li>
</ul>


<h3><a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a> </h3>
<ul id='ulksysguard' style='display: block'>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/ksysguard/04704b052f531f2402f32efce5613858d61c3faa'>Commit.</a> </li>
<li>Add AppStream metadata. <a href='https://commits.kde.org/ksysguard/daa9e0c1eb12311b295d2507d98325247d5ecb6c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13772'>D13772</a></li>
</ul>


<h3><a name='kwallet-pam' href='https://commits.kde.org/kwallet-pam'>kwallet-pam</a> </h3>
<ul id='ulkwallet-pam' style='display: block'>
<li>Move remaining salt file operations into unprivileged processes. <a href='https://commits.kde.org/kwallet-pam/c78e8ade9e2bdf64e8b5d9e28d50d01bf9d0af63'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13776'>D13776</a></li>
<li>Drop privileges when reading the salt file. <a href='https://commits.kde.org/kwallet-pam/f06d119e6ddd3886f0cc340e5ab89a60c349ec7d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12937'>D12937</a></li>
<li>Revert "do not search for C++ compiler". <a href='https://commits.kde.org/kwallet-pam/6238b4e188ada1a483a5172c1b865fb0b82207dc'>Commit.</a> </li>
<li>Use pid_t for fork() result. <a href='https://commits.kde.org/kwallet-pam/cd50e55a3fe1a5d6bacfc97cf548b6730eba0d49'>Commit.</a> </li>
<li>Store string literal in const variable. <a href='https://commits.kde.org/kwallet-pam/b1b7098079060bd8600b5f2b1e50585d0250696e'>Commit.</a> </li>
<li>Do not search for C++ compiler. <a href='https://commits.kde.org/kwallet-pam/bb1bce811545588755018e676f1248ec8e833344'>Commit.</a> </li>
<li>Use explicit_bzero() if it is present. <a href='https://commits.kde.org/kwallet-pam/1f8707c4e1145422ff53686d377d7d99070c452b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13015'>D13015</a></li>
</ul>


<h3><a name='kwayland-integration' href='https://commits.kde.org/kwayland-integration'>KWayland-integration</a> </h3>
<ul id='ulkwayland-integration' style='display: block'>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/kwayland-integration/58f9fd7bbad6a87b89cb6a7d56958e09e93e7b51'>Commit.</a> </li>
<li>Respect BUILD_TESTING. <a href='https://commits.kde.org/kwayland-integration/e2ddf8933f428bacd183401132a841db874306d3'>Commit.</a> </li>
<li>Add category file. <a href='https://commits.kde.org/kwayland-integration/bf26f276c9149fc3f3c46e78980999520aaa4f71'>Commit.</a> </li>
<li>Autogenerate loggin category. <a href='https://commits.kde.org/kwayland-integration/982b512df78017e70e766b91b12e80b49325bcf5'>Commit.</a> </li>
<li>USe override/nullptr. <a href='https://commits.kde.org/kwayland-integration/1497269d4a7f280b60db7fdd5346948e6cae496a'>Commit.</a> </li>
<li>Make sure that it builds with strict compile flags. <a href='https://commits.kde.org/kwayland-integration/cef281fe790cfe6c753d5306dfb39aa864b526bc'>Commit.</a> </li>
<li>Align with the rest of Plasma. <a href='https://commits.kde.org/kwayland-integration/94708faf0fb481c58cdb4700c50dc58cf812325c'>Commit.</a> </li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>[shadow] Rebuild quads after creation of shadow. <a href='https://commits.kde.org/kwin/213239a0ea0a9c0967bb68d1eda7a8d4d6a09498'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398572'>#398572</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15475'>D15475</a></li>
<li>Remove duplicated auto backend resolution. <a href='https://commits.kde.org/kwin/0a2e51db47f4c91490626872375d61c8fec3aa73'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13658'>D13658</a></li>
<li>Avoid crash with on scripted window teardown with threaded quick render loop. <a href='https://commits.kde.org/kwin/d485dfe7ef1910a7f6652610388ae2d93cdec29f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397767'>#397767</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15025'>D15025</a></li>
<li>Remove breaking pointer constraints functionality. <a href='https://commits.kde.org/kwin/6072b0cd6c370749f28418f22a1284de5aba3462'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15234'>D15234</a></li>
<li>[effects/trackmouse] Allow to use both modifiers and shortcut. <a href='https://commits.kde.org/kwin/74994a7fbda0432e7b3773e053e4c8c940f66a53'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398124'>#398124</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15272'>D15272</a></li>
<li>[effects/showpaint] Modernize code. <a href='https://commits.kde.org/kwin/cc1f30b43d16d132f02c4ada5643a6c1b43cc2f3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15424'>D15424</a></li>
<li>[effects] Fix type of X-KWin-Video-Url. <a href='https://commits.kde.org/kwin/113ebe9bc2258833f89f958e3953bc74670ca34a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15373'>D15373</a></li>
<li>Activate clients on drag enter. <a href='https://commits.kde.org/kwin/1fada99478a807e23f4887c590a0516e027851a2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15225'>D15225</a></li>
<li>[effects/dialogparent] Fix strict mode issues. <a href='https://commits.kde.org/kwin/03a2a05fc7f2df9de95c7f3ff95a6ba058813580'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15413'>D15413</a></li>
<li>[effects/magnifier] Fix 1 px gap between magnified area and frame. <a href='https://commits.kde.org/kwin/617b4d92fa54a43014f722398ad5828175ba3029'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15275'>D15275</a></li>
<li>Revert "Search in default path before calling pkg-config". <a href='https://commits.kde.org/kwin/ce2705d71fd36dce0217ca2051b7e74ceb0251cd'>Commit.</a> </li>
<li>Search in default path before calling pkg-config. <a href='https://commits.kde.org/kwin/4f4f3295f2a8a366c954025652785e8fda34df3e'>Commit.</a> </li>
<li>[effects/slide] Use QOverload. <a href='https://commits.kde.org/kwin/3a46b3707a208392aba9dc57b787a7859787c2be'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15300'>D15300</a></li>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/kwin/f92d806343efcfc6f093d925d51be1939683146a'>Commit.</a> </li>
<li>Avoid invalid geometry of internal clients through plasma surface interface. <a href='https://commits.kde.org/kwin/1fb2eace3fbfc6645e024aa0f5003306ac60a417'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386304'>#386304</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13084'>D13084</a></li>
<li>Clean up includes. <a href='https://commits.kde.org/kwin/624a453109c5255cf7d10309d017fcfbc3184bb8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15191'>D15191</a></li>
<li>Fix capitalization typo. <a href='https://commits.kde.org/kwin/f96bea7c739ff53033630d8ad7303b5d5fa09a02'>Commit.</a> </li>
<li>[scenes/opengl] Fix overlaps in shadow texture atlas. <a href='https://commits.kde.org/kwin/dadcd511351399c60e24ae247af44518ac538a17'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14784'>D14784</a></li>
<li>[scenes/opengl] Correctly draw shadows when corner tiles are missing. <a href='https://commits.kde.org/kwin/5e55664de8a3cad364513a108a5eadd7b5d4f870'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14783'>D14783</a></li>
<li>[platforms/virtual] Set raw physical size of outputs. <a href='https://commits.kde.org/kwin/95cb47cae2ae2f4ed156a5da83ca12757d03c52c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15183'>D15183</a></li>
<li>[colorcorrection] Set gamma through Output class. <a href='https://commits.kde.org/kwin/9cf2730f8dcc2e4e20955dff90fa86e95aa2d380'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11803'>D11803</a></li>
<li>[platforms/virtual] Let VirtualOutput inherit Output. <a href='https://commits.kde.org/kwin/b22c362bd5253664a4d52287292abd2cbff8eaa7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11789'>D11789</a></li>
<li>Introduce OutputScreens class. <a href='https://commits.kde.org/kwin/3482378278d6c11359785d04ea90f1c2209dd40a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11782'>D11782</a></li>
<li>Introduce generic Output class. <a href='https://commits.kde.org/kwin/fe63e21f80008a807ac13b661e87f484d2f9fd5e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11781'>D11781</a></li>
<li>[libkwineffects] Cleanup effects handler global pointer on destruction. <a href='https://commits.kde.org/kwin/8cf5041e69d1e4aaf6b922099ccb2b6d9f3e45ab'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15171'>D15171</a></li>
<li>[autotests] Make lifespan of EffectsHandler outlive Effect. <a href='https://commits.kde.org/kwin/7e7eadb44fa5851a17d4b5dee52b819817660a40'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15167'>D15167</a></li>
<li>Avoid global static for effects. <a href='https://commits.kde.org/kwin/0fd939425bf7bed20ae0435f864eadcaa6f5a3b0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15163'>D15163</a></li>
<li>Fix minor EBN issues. <a href='https://commits.kde.org/kwin/bf58da3e9a6be89a3011941570018726edad99bb'>Commit.</a> </li>
<li>[effects/diminactive] Fix initialization of m_activeWindow on reconfigure. <a href='https://commits.kde.org/kwin/defa1061afe8583096d9e887dab3ce81fe1324bf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14973'>D14973</a></li>
<li>[effects] Rewrite the Dim Inactive effect. <a href='https://commits.kde.org/kwin/5165ee45b5e3448f46490edfe9326375ffa91a29'>Commit.</a> See bug <a href='https://bugs.kde.org/359251'>#359251</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13720'>D13720</a></li>
<li>[autotests] Test ScriptedEffects stackingOrder. <a href='https://commits.kde.org/kwin/1238c7bc4736d859119cf5c3ecda93f70515b33a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14533'>D14533</a></li>
<li>[effects/logout] Animate ksmserver-logout-greeter. <a href='https://commits.kde.org/kwin/0a31feb3212c22553075aa68be74f6fa7fcb684c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14848'>D14848</a></li>
<li>[effects/slidingpopups] Overhaul slotPropertyNotify. <a href='https://commits.kde.org/kwin/4a775dd485d8d6488e4125803c6b4ec09acf136b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14862'>D14862</a></li>
<li>[effects/minimizeanimation] Delete useless comments. <a href='https://commits.kde.org/kwin/7fd3672de2d1e052d0db12f365528e79681eca69'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14728'>D14728</a></li>
<li>[kcmkwin/compositing] Make the Background Contrast effect searchable. <a href='https://commits.kde.org/kwin/4576d1bfa925a936c2a905a61c02132fff7eeebe'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14766'>D14766</a></li>
<li>[kcmkwin/compositing] Delete leftover after removed effects. <a href='https://commits.kde.org/kwin/a18633b2b80d0fd27901e4d7e71f3af5c567eb35'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14765'>D14765</a></li>
<li>[effects/slidingpopups] Fix coding style. <a href='https://commits.kde.org/kwin/83c1548205a3126680f14b4ef294ad82342685a4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14825'>D14825</a></li>
<li>[effects/slidingpopups] Use new connect syntax. <a href='https://commits.kde.org/kwin/cca11405b031f3c0f6ac398a5c1bf6bfea75d421'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14823'>D14823</a></li>
<li>[effects/slidingpopups] Use override keyword. <a href='https://commits.kde.org/kwin/6e2292c81ca41ff091fb8dc087eb7dee7b58da3f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14801'>D14801</a></li>
<li>[effects/slidingpopups] Overhaul the animations data struct. <a href='https://commits.kde.org/kwin/653cea5f49e9a1a2119a9c036d8f3caf41fac9fe'>Commit.</a> See bug <a href='https://bugs.kde.org/331118'>#331118</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14301'>D14301</a></li>
<li>[effects/slidingpopups] Don't filter window quads. <a href='https://commits.kde.org/kwin/4a38f15d90e05b1e15e920db65048a1f7b49ea78'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14450'>D14450</a></li>
<li>[platforms/fbdev] Attempt to set the framebuffer color layout on the framebuffer device. <a href='https://commits.kde.org/kwin/304528e80b935efea05e2d5e3030266e0eddc44c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9572'>D9572</a></li>
<li>[effects/sheet] Modernize code. <a href='https://commits.kde.org/kwin/77966a09f2d2201204ea9fb8c3c04cf9119ba4b9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14685'>D14685</a></li>
<li>[effects] Drop the Scale in effect. <a href='https://commits.kde.org/kwin/4299b81f6599e1e6246954e7b370f369d64c7e87'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13462'>D13462</a></li>
<li>[effects] Add Scale effect. <a href='https://commits.kde.org/kwin/9d197e8cb6e96e392cea2d78f696b51d9b3d238b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13461'>D13461</a></li>
<li>[effects/glide] Don't animate the Application Dashboard. <a href='https://commits.kde.org/kwin/a8563304f2d2ac0939bb54d772b25512ba938691'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14367'>D14367</a></li>
<li>[effects/minimizeanimation] Use override keyword. <a href='https://commits.kde.org/kwin/77daaeaaa1538af30253117fe24385a9b4975754'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14729'>D14729</a></li>
<li>[effects/mousemark] Properly use GL_LINE_SMOOTH. <a href='https://commits.kde.org/kwin/99804e0233cbb1054b1b1f5148dc5c7a6833a709'>Commit.</a> See bug <a href='https://bugs.kde.org/337044'>#337044</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14714'>D14714</a></li>
<li>[effects/sheet] Fix undesired perspective distortion. <a href='https://commits.kde.org/kwin/fb4dc9a9cf91c455351567bd186cc18513636c7e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14687'>D14687</a></li>
<li>[effects/sheet] Grab modal windows. <a href='https://commits.kde.org/kwin/10e99f64c1e1f50c678a39114b1bd6b287c89479'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14560'>D14560</a></li>
<li>[scripting] Fix effect.animate() curve argument being actually used. <a href='https://commits.kde.org/kwin/3332b32101d7d9b3a8d9337efaa9d47f7436005b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14669'>D14669</a></li>
<li>[effects/logout] Animate the disappearing of the logout screen. <a href='https://commits.kde.org/kwin/5336c9a1e2001aa7b46a42b73fdea42d0c101414'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14592'>D14592</a></li>
<li>Fractional scaling in DRM kscreen integration. <a href='https://commits.kde.org/kwin/7aedacd89eb2df5849a814f50085db0876cf5014'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13616'>D13616</a></li>
<li>[effects/slidingpopups] Warnings--. <a href='https://commits.kde.org/kwin/1df27f8272eb839e7e52a1eaf90b7a0de26ca4ea'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14615'>D14615</a></li>
<li>[effects/fade] Don't animate the logout screen. <a href='https://commits.kde.org/kwin/d61f3ec2d537b0164a3537c5c80cb748a5c35730'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14582'>D14582</a></li>
<li>[effects/minimizeanimation] Fix coding style. <a href='https://commits.kde.org/kwin/92880381b8689ee1a18547e2beb0f0fe99259593'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14608'>D14608</a></li>
<li>Fix typo. <a href='https://commits.kde.org/kwin/ce9965ccadc1b447fd51abdae433d0bd600cd4d2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14584'>D14584</a></li>
<li>[effects] Move Fall Apart and Sheet to the Candy category. <a href='https://commits.kde.org/kwin/e615b25957fd6788a8762a989b1037b566e31cb3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14517'>D14517</a></li>
<li>[kcmkwin/compositing] Move show desktop effects to their own category. <a href='https://commits.kde.org/kwin/119d0d8f8ca1e4aa5bbdbebd6cdd7356f428f481'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14458'>D14458</a></li>
<li>[effects/minimizeanimation] Use interpolate helper. <a href='https://commits.kde.org/kwin/950bf94f30a6e9a8f2da8b9799e7b7282bb295af'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14553'>D14553</a></li>
<li>[effects/minimizeanimation] Make it a little bit smoother. <a href='https://commits.kde.org/kwin/c36b99cdbc65e45f91ee93c016664a39d79c159a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14552'>D14552</a></li>
<li>[autotests] Unit most scripted effects API. <a href='https://commits.kde.org/kwin/5d279a0ddd539cc977c4d9d210f3453ae2f5ddfd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14482'>D14482</a></li>
<li>[libkwineffects] Remove broken copy constructor of AniData. <a href='https://commits.kde.org/kwin/7b65aa9199186d64567fa6e18b80fe65f5f76206'>Commit.</a> </li>
<li>[effects] Move the Wobby Windows effect to the Candy category. <a href='https://commits.kde.org/kwin/cb820d8966c95d8e9e80e2d4ec25822e10ffd189'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14455'>D14455</a></li>
<li>[effects] Edit description of the Logout effect. <a href='https://commits.kde.org/kwin/89b3da58fc6982b0ea85261ddfdc88d5c744de8c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14461'>D14461</a></li>
<li>[effects] Edit description of the Glide effect. <a href='https://commits.kde.org/kwin/d8288012f0e0dddbb4f79177c4b0f428bfb83d2d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14460'>D14460</a></li>
<li>Use explicit Chain type in the focus chain. <a href='https://commits.kde.org/kwin/756d4e6e3f8c3ededec287e1752f1b14ee697a5a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14431'>D14431</a></li>
<li>[effects] Put Window Aperture and eye On Screen in an exclusive category. <a href='https://commits.kde.org/kwin/c7298b24e6369f9bf68e83ca1738cf060cc8c24e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14456'>D14456</a></li>
<li>Use locked cursor position hint. <a href='https://commits.kde.org/kwin/054ccc389899acf1f15001105e8b03a4ffb203d0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14176'>D14176</a></li>
<li>[kcmkwin/kwindecoration] Add missing QT include. <a href='https://commits.kde.org/kwin/d723ce2c40f4e9fd4473347646119bebc3cae743'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14173'>D14173</a></li>
<li>[effects] Rewrite the Glide effect. <a href='https://commits.kde.org/kwin/6a7e780d74ec6a5b504332b484a6734b0be42073'>Commit.</a> See bug <a href='https://bugs.kde.org/394245'>#394245</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13338'>D13338</a></li>
<li>Require KDecoration >= 5.13.0 . <a href='https://commits.kde.org/kwin/df802d49a1daa2294840c2689da39c1915f441d3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14320'>D14320</a></li>
<li>[effects/slidingpopups] Use single QHash to store animations. <a href='https://commits.kde.org/kwin/7785c19f11d3c33154a5b520d6ae5a48058fc488'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14096'>D14096</a></li>
<li>[effects/slidingpopups] Unconditionally force background contrast. <a href='https://commits.kde.org/kwin/82ab1d1ecd5f0ebe4ac93e67535eb27fc6ec9d2a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14092'>D14092</a></li>
<li>[effects/slide] Fix coding style. <a href='https://commits.kde.org/kwin/9cff6c21f34d987d56b1621397e8063bd2876a78'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14260'>D14260</a></li>
<li>[effects/sheet] Drop IsSheetWindow hack. <a href='https://commits.kde.org/kwin/59e3e21c47bbd82a6ab46969a0682caebe5bddc3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14246'>D14246</a></li>
<li>Call workspaceEvent and updateX11Time only once per event. <a href='https://commits.kde.org/kwin/afc80f8bd5bd218edb1050a9d2190749446eb589'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14215'>D14215</a></li>
<li>Don't remove outputs during page flip. <a href='https://commits.kde.org/kwin/9f2f6d96577dd4aba878e53259e20bde72d752d1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396272'>#396272</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14210'>D14210</a></li>
<li>[libkwineffects] Deprecate Outline feature. <a href='https://commits.kde.org/kwin/653e98d5ac0aa08f986911b178ae66cba6302382'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14188'>D14188</a></li>
<li>Explicitly disable pointer constraints on TabBox invocation. <a href='https://commits.kde.org/kwin/f0ba436c724e32103bfdc584a636d4a8ea2a0d49'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14142'>D14142</a></li>
<li>[effects/magiclamp] Eliminate unnecessary reallocations. <a href='https://commits.kde.org/kwin/4aa19c2e91898ae22d39bb346c35763b7564c4f4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14117'>D14117</a></li>
<li>[effects/slidingpopups] Use range-based for loops. <a href='https://commits.kde.org/kwin/049c6e0966ceda533e023a22f29a5d43a65cef0c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14086'>D14086</a></li>
<li>[effects/slidingpopups] Reserve enough memory for filtered quads. <a href='https://commits.kde.org/kwin/a63c9ab319b93ccd112c50df24bdfbb55787e1f2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14085'>D14085</a></li>
<li>[effects/slidingpopups] Fix possible segfault. <a href='https://commits.kde.org/kwin/461aace55f71c172eeff820de82509c5c82a07cb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14087'>D14087</a></li>
<li>[effects/slidingpopups] Simplify math in setupAnimData. <a href='https://commits.kde.org/kwin/4e1e22c76d5bc98ff3350dfb64e04496c2a6ed99'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14088'>D14088</a></li>
<li>[effects/slidingpopups] Delete unused prePaintScreen method. <a href='https://commits.kde.org/kwin/2d1afb809cda14d333e69402f55c914651fee899'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14091'>D14091</a></li>
<li>[effects/slide] Clean up. <a href='https://commits.kde.org/kwin/d0ceaf4c7025f4d9cb5766cf9cbbd1449e5e0a27'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13979'>D13979</a></li>
<li>[libkwineffects] Use more std::transform in implementation part. <a href='https://commits.kde.org/kwin/91e69e3fbd67e0f92b04885b1ca4c04ff44928a8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14002'>D14002</a></li>
<li>[libkwineffects] Use std::copy. <a href='https://commits.kde.org/kwin/81d851f6aa735821fba041750b3a0cb1ca0e7d56'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14004'>D14004</a></li>
<li>[kcmkwin/kwindesktop] Use PackageLoader to get effect metadata. <a href='https://commits.kde.org/kwin/f48cf072f9daf5f083bcc26ccb86c9564a8ac2a1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13851'>D13851</a></li>
<li>[effects/kscreen] Port to TimeLine. <a href='https://commits.kde.org/kwin/30954506d3bd27753e88ca9dc4b236c765172b5f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13865'>D13865</a></li>
<li>[libkwineffects] Push render targets more efficiently in GLRenderTarget::pushRenderTargets. <a href='https://commits.kde.org/kwin/709b7c2c58f5f0bd11d344e0e499d6b0434ec31c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13823'>D13823</a></li>
<li>Use std::find_if in EffectsHandlerImpl::isEffectLoaded. <a href='https://commits.kde.org/kwin/7ee83dc5e425bfa2f7f3f92586bc4434da80634b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13836'>D13836</a></li>
<li>[effects/slide] Expose support information. <a href='https://commits.kde.org/kwin/c3fd38e18286329e2a485b4db74453b9055615d7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13843'>D13843</a></li>
<li>Use QHash::value() in EffectWindowImpl::data(). <a href='https://commits.kde.org/kwin/6f173fc609c94343c1f737446c57aded63974f98'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13820'>D13820</a></li>
<li>[effects/slidingpopups] Port to TimeLine. <a href='https://commits.kde.org/kwin/22a6cab15ff28c543440bb9230d88e5ab48b503c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13801'>D13801</a></li>
<li>Don't detach list of damaged windows when sending 'done' events. <a href='https://commits.kde.org/kwin/89001b1a5afbf429402337834bf659066ce8ad6a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13830'>D13830</a></li>
<li>Lookup loaded effects only once when building support information. <a href='https://commits.kde.org/kwin/e7d6f12bbaf4c94be9ab201431fb2daedad356cd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13837'>D13837</a></li>
<li>[effects/cube] Change type of the rotationDuration property to int. <a href='https://commits.kde.org/kwin/a1e76b4802ce9a6d6cdba75c211d9acebf5297dd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13839'>D13839</a></li>
<li>Fix KDeclarative::setupBindings() deprecation warnings. <a href='https://commits.kde.org/kwin/a453925306d37af1546e0b91580ea1d8d140bd56'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13835'>D13835</a></li>
<li>Use std::transform in libkwineffects implementation part. <a href='https://commits.kde.org/kwin/e0bbf5c87f621be233877217dd3e26770342c927'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13821'>D13821</a></li>
<li>Use qDeleteAll in destructor of Scene class. <a href='https://commits.kde.org/kwin/e0088b9c9da2b8b4a0bc8578d162f73a3cc91a28'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13822'>D13822</a></li>
<li>[effects/slide] Port to TimeLine. <a href='https://commits.kde.org/kwin/d82aded4a647757aefe8b3d832f911932da0e38c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13764'>D13764</a></li>
<li>[effects/cube] Port to TimeLine. <a href='https://commits.kde.org/kwin/5e24bed369df6b9b8f05a80e8a4fa27e3126f887'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13765'>D13765</a></li>
<li>[effects/minimizeanimation] Port to TimeLine. <a href='https://commits.kde.org/kwin/0be5010c0a6ff0308c2ad124f80e2f1d35a99312'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13761'>D13761</a></li>
<li>[effects/magiclamp] Port to TimeLine. <a href='https://commits.kde.org/kwin/138b185889cda2ba53f0dddce93336809ad8c879'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13762'>D13762</a></li>
<li>[libkwineffects] Add TimeLine helper. <a href='https://commits.kde.org/kwin/ee88951b17c8400d4ca0471704bdedbdc6376d96'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13740'>D13740</a></li>
<li>Remove unneeded QT definitions. <a href='https://commits.kde.org/kwin/27cd0bc5d07dfb55a3f82b16cfbaf75b9cb2fa2e'>Commit.</a> </li>
<li>Make keyboard focus a pointer constraints necessity. <a href='https://commits.kde.org/kwin/0bd5eff8623dceb6e2dee445edcc244ccb620660'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13492'>D13492</a></li>
<li>[libkwineffects] Save value of the managed property during construction of EffectWindow. <a href='https://commits.kde.org/kwin/f977e60850b9cccf6a11ed3018558511da8695b3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13690'>D13690</a></li>
<li>[libkwineffects] Emit a signal when active fullscreen effect changed. <a href='https://commits.kde.org/kwin/95a2c3bf760146123e0a3dccd7ef0ff08abeeac6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13701'>D13701</a></li>
<li>Load Kwin's internal cursors for the highest resolution of attached monitors. <a href='https://commits.kde.org/kwin/c857c03561cbb1c93f4f64b339543c5522e4adfc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13608'>D13608</a></li>
<li>Set correct DPR on wayland cursors received from remote buffers. <a href='https://commits.kde.org/kwin/1761b75b558e1ff9925a7b7a14d82d574b387469'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13606'>D13606</a></li>
<li>Remove concept of resolution dependent cursors. <a href='https://commits.kde.org/kwin/6bdfea6d2f5a0891142231f21f8bd954423ae318'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13607'>D13607</a></li>
<li>DRM cursor scaling. <a href='https://commits.kde.org/kwin/2cc42ecc1220428d1c4fc19e9c85f89c1f5da4cf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13605'>D13605</a></li>
<li>Support cursor scaling in X windowed backend. <a href='https://commits.kde.org/kwin/9ff1f77e8edaa0c87aeee1cdcc698e42121e2c59'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13642'>D13642</a></li>
<li>[effects/slide] Disable "Slide docks". <a href='https://commits.kde.org/kwin/263503f8ec83f05e2a6c78ed22071db83f677fdb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13566'>D13566</a></li>
<li>[libkwineffects] Add keepBelow property to EffectWindow. <a href='https://commits.kde.org/kwin/6a175ece489a36a47ea4700326c8beaed52a3379'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13650'>D13650</a></li>
<li>Add some missing properties to Deleted. <a href='https://commits.kde.org/kwin/dcc349c1ef230020a4d69934ec99aa348151baf8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13649'>D13649</a></li>
<li>[effects] Ignore previous state of WindowForceBlurRole. <a href='https://commits.kde.org/kwin/e9ab34854d1cda789526a829bd83deafe48dc4dd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13479'>D13479</a></li>
<li>[effects/slide] Add "Slide desktop background" option. <a href='https://commits.kde.org/kwin/64d1b0e93c42cc4243b523d8df5ba85dfa87ac4f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13542'>D13542</a></li>
<li>[effects] Use more effectData() in BuiltInEffects. <a href='https://commits.kde.org/kwin/7bfaa6e913dff867e6b6c3d4b451eb274683ca87'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13587'>D13587</a></li>
<li>[effects/blur] Clean up shader code. <a href='https://commits.kde.org/kwin/168109f3bb36882cdd1f18ebe3ce052e4cd608a0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13110'>D13110</a></li>
<li>[kcmkwin/kwindesktop] Make Slide effect configurable. <a href='https://commits.kde.org/kwin/586460dbfcd0591b74d4ce2996e2634c10eb4ee8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395377'>#395377</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13544'>D13544</a></li>
<li>Small code style improvement. <a href='https://commits.kde.org/kwin/ddb44b4383ac1b50e1516b082ed35ec5a340e557'>Commit.</a> </li>
<li>[effects/fallapart] Fade out window parts. <a href='https://commits.kde.org/kwin/8593823f6cd83492cc2fa68d464aef3a417d90ec'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13528'>D13528</a></li>
<li>Revert "Disable unit test which fails to compile on the CI system.". <a href='https://commits.kde.org/kwin/9260b3c51e63bd87543f39d15866e37ea0c23a68'>Commit.</a> </li>
<li>[effects/fallapart] Fix flickering problem. <a href='https://commits.kde.org/kwin/5daa612bce7404cd22dadc00121a739c225e2bb6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13527'>D13527</a></li>
<li>Compare doubles to doubles. <a href='https://commits.kde.org/kwin/3e2ff0e870585125ea3b06133d693e2fa553dc1b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13526'>D13526</a></li>
<li>Disable unit test which fails to compile on the CI system. <a href='https://commits.kde.org/kwin/e6cdf966ff10d875ceaf65c85356ed7732b585a4'>Commit.</a> </li>
<li>Remove pointer constraint on resource unbind. <a href='https://commits.kde.org/kwin/2694839099c3a0e9da3682569e1bd569247c3ae8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388885'>#388885</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13466'>D13466</a></li>
<li>[effects/slide] Completely delete forced roles. <a href='https://commits.kde.org/kwin/e38ecfcbcfe4233ad30cbc026cc43953b11d11ee'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13470'>D13470</a></li>
<li>[effects/slide] Use FormLayout in KCM. <a href='https://commits.kde.org/kwin/1a1845b7d75740e7bd86fbe7680c373a37343fc5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13422'>D13422</a></li>
<li>Correctly check forcebackgroundcontrastrule. <a href='https://commits.kde.org/kwin/1193e3c3692abae689144677a6ab3b5ca20134e3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13478'>D13478</a></li>
<li>Set specific edge cursor shape when resizing. <a href='https://commits.kde.org/kwin/5b4eb80c8fdb6ea72c6046389578421b00824c6f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13396'>D13396</a></li>
<li>[tests] Fix unconfine Button. <a href='https://commits.kde.org/kwin/fcfe8763f6f020dbb9f0b45dbe42d23952fc877f'>Commit.</a> </li>
<li>[tests] Add pointer constraints test. <a href='https://commits.kde.org/kwin/497d077349c98beb872a6ecbb661d5f6c9766284'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13439'>D13439</a></li>
<li>[effects/dimscreen] Use QSet for checking whether activated window asks for permissions. <a href='https://commits.kde.org/kwin/7550d2a02064e7e524945a659b8499ed7a9dc0d6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13440'>D13440</a></li>
<li>[kcmkwin/kwindecoration] Properly render shadows with big tiles. <a href='https://commits.kde.org/kwin/d50f28033abd222b705aaf930e1f239f5a004990'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10942'>D10942</a></li>
<li>[scenes/qpainter] Draw decoration shadows. <a href='https://commits.kde.org/kwin/2d01ba6450020383c74f0094a22036eb4ed0194f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10943'>D10943</a></li>
<li>[scenes/opengl] Fix overlapping shadow tiles. <a href='https://commits.kde.org/kwin/7637cfc22bbdc9c6f51f2e32b24ba26fb839351d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10811'>D10811</a></li>
<li>Remove Qt module declarations in includes. <a href='https://commits.kde.org/kwin/a3cff85e7aec126e089a1ee7685fa1ebfd84813a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13359'>D13359</a></li>
<li>[effects/minimizeanimation] Use new connect syntax. <a href='https://commits.kde.org/kwin/60c34b14134b92e2b8a96e82cf3dcd4e6a7e37fd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13354'>D13354</a></li>
<li>[effects/glide] Do not animate logout screen. <a href='https://commits.kde.org/kwin/c629d282acceaff5c6f35f06f20bdee52b167142'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13263'>D13263</a></li>
<li>[effects/cube] Fix animation and reflection issues. <a href='https://commits.kde.org/kwin/6408e0ba6045d03b8872eab71060ac8d6f13ee9f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/213599'>#213599</a>. Fixes bug <a href='https://bugs.kde.org/373101'>#373101</a>. Phabricator Code review <a href='https://phabricator.kde.org/D9860'>D9860</a></li>
<li>[effects/blur] Check for blitting support. <a href='https://commits.kde.org/kwin/81f38abe6d6db94f56c0d2e857fd90cc1a73c293'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13246'>D13246</a></li>
<li>Fix blur on Wayland when scaling is used. <a href='https://commits.kde.org/kwin/b3b691a25074abc8866c3ed5cd67cc8177e6418d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391387'>#391387</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12700'>D12700</a></li>
<li>[effects/maximize] Use more verbose name for WindowForceBlurRole instead of an integer. <a href='https://commits.kde.org/kwin/42dc3e4cd5606aa373412bc706d1238b5e745db5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13156'>D13156</a></li>
<li>[effects/slide] Const-ify variables. <a href='https://commits.kde.org/kwin/c5697d64159b90add0e19ff01481a6c194c03710'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13116'>D13116</a></li>
<li>[effects/frozenapp] Delete debug prints. <a href='https://commits.kde.org/kwin/02c08ca33d380a8370a319bc887dad482312f1a7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13157'>D13157</a></li>
<li>[effects/blur] Lookup window once when disconnecting from blurChanged. <a href='https://commits.kde.org/kwin/95ee052442991e99981c9e5f7a6ed6ed115b5aab'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13118'>D13118</a></li>
<li>Honor BUILD_TESTING. <a href='https://commits.kde.org/kwin/54b69bec446d596bea0b15fda38522d9a47885bf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13163'>D13163</a></li>
<li>Fix multimonitor blur. <a href='https://commits.kde.org/kwin/99532fb95c1edb855ffeedf8551130d7a783feff'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393723'>#393723</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12678'>D12678</a></li>
<li>[libkwineffects/kwinglutils] Calculate correct srcY0 and srcY1 in GLRenderTarget::blitFromFramebuffer. <a href='https://commits.kde.org/kwin/8342cdd3b6d97ee3f12836cdcbf63ee92134bd42'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12452'>D12452</a></li>
<li>Revert "Revert "Add "SkipSwitcher" to API"". <a href='https://commits.kde.org/kwin/393af855c483f74fccf3092717a95209bce9bd95'>Commit.</a> </li>
<li>Disallow running KWin/Wayland as root. <a href='https://commits.kde.org/kwin/267b5a11222e06b0f248ea084c95efa7de776dbe'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13008'>D13008</a></li>
<li>Drop no longer needed cast to Client in TabGroup related code. <a href='https://commits.kde.org/kwin/b19c3c1c8e52870b704ae1eb2207721f87dc35dd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12730'>D12730</a></li>
<li>Use AbstractClient in UserActionsMenu for tab functionality. <a href='https://commits.kde.org/kwin/b6befb4ef88f2f741882257d957ed3339c2e2023'>Commit.</a> </li>
<li>Move TabGroup functionality from Client to AbstractClient. <a href='https://commits.kde.org/kwin/46d8b87646cd4f90533ab8642eb0dfeb0d93d33b'>Commit.</a> </li>
<li>Port TabGroup from Client to AbstractClient. <a href='https://commits.kde.org/kwin/7defd93047de5ed542ad679f5b2e205c8175cac5'>Commit.</a> </li>
<li>Use existing KSharedConfig in RuleBook::save. <a href='https://commits.kde.org/kwin/a02797ca0608a302f466897d5bb5cd5cf4b9d13f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12747'>D12747</a></li>
<li>Fix message extraction. <a href='https://commits.kde.org/kwin/eb47c08204deca0814fd84dcf53b45e611ac4442'>Commit.</a> </li>
<li>Add XDG Output support. <a href='https://commits.kde.org/kwin/1403fcf3168551eb3eb4a8cd0998d9c78baa5aa3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12243'>D12243</a></li>
</ul>


<h3><a name='kwrited' href='https://commits.kde.org/kwrited'>kwrited</a> </h3>
<ul id='ulkwrited' style='display: block'>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/kwrited/baea22918f5030e11791d0ae6a0f94d231932518'>Commit.</a> </li>
<li>Trim kwrited text. <a href='https://commits.kde.org/kwrited/bc72a72e8e93cae5ca3de0cf5726dfa219ee49c5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14049'>D14049</a></li>
<li>Make it build with strict compile flags. <a href='https://commits.kde.org/kwrited/3360de6fc6b84255aa818de69747970c07619e46'>Commit.</a> </li>
<li>Remove unused entry X-KDE-DBus-ModuleName from the kded plugin metadata. <a href='https://commits.kde.org/kwrited/cdaee70b479871b142884f7d62e0dcf353c9c718'>Commit.</a> </li>
</ul>


<h3><a name='libkscreen' href='https://commits.kde.org/libkscreen'>libkscreen</a> </h3>
<ul id='ullibkscreen' style='display: block'>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/libkscreen/62a2542803b9a0e4b0ef6c8cf1938935bea769f9'>Commit.</a> </li>
<li>Support kwin fractional scaling in wayland backend. <a href='https://commits.kde.org/libkscreen/3a0e95629912d8c82d14e2b8c754ebbdbd4364f7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13617'>D13617</a></li>
<li>EDID parsing: replace non-printable characters with '-'. <a href='https://commits.kde.org/libkscreen/13bc5bcd4bf4eee23fce878d47578655f5133d62'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14549'>D14549</a></li>
<li>Fix some doc strings. <a href='https://commits.kde.org/libkscreen/0f32d126d73c907c63c6a22054b098873e6435f0'>Commit.</a> </li>
<li>Use reinterpret_cast instead of c-style cast. <a href='https://commits.kde.org/libkscreen/50dcd973c1c7c8f2ce0ec0002687acc1d730b28e'>Commit.</a> </li>
<li>Testedid: make sure to use uint consistently. <a href='https://commits.kde.org/libkscreen/c9955376ed2d96595a86c063c8d5de5fd08512c2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14548'>D14548</a></li>
<li>Let edidDecodeFraction return float. <a href='https://commits.kde.org/libkscreen/15aa08f365617e3b9ed0b770db6a8d9b3490e378'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14368'>D14368</a></li>
<li>Add test for edid parsing and fix reading gamma. <a href='https://commits.kde.org/libkscreen/b80bbeb2e41965bde49b91dd6c8e4cc4177387da'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14418'>D14418</a></li>
<li>Fix memory leak in test. <a href='https://commits.kde.org/libkscreen/931cead9c1382a931b4abacbb65977069768a9a3'>Commit.</a> </li>
<li>Make sure for loop does not detach. <a href='https://commits.kde.org/libkscreen/7cbccda5a273efa29d70565caad7388f4cb21d4c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14350'>D14350</a></li>
<li>When handing out ownership, use Q_REQUIRED_RESULT - Edid::clone. <a href='https://commits.kde.org/libkscreen/10625146167492318e71ded7dc073682f4065c50'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14369'>D14369</a></li>
<li>EDID parsing: strings are up to 13 bytes. <a href='https://commits.kde.org/libkscreen/c66291ec8307ed252f3b79ffbfe16a163c45fd52'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14410'>D14410</a></li>
<li>Simplify Edid::Private::edidParseString. <a href='https://commits.kde.org/libkscreen/2dfbe8d8a6cc28d4e0e5178dd68654b90c63be6b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14370'>D14370</a></li>
<li>Wayland: be consistent about screen name. <a href='https://commits.kde.org/libkscreen/d7cef1a5a98724f96c5a152b8be8124186d3d533'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14381'>D14381</a></li>
<li>Wayland: fix refresh rate conversion error. <a href='https://commits.kde.org/libkscreen/76cd972a93ff52de95c78503895a326fbf3129bd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14374'>D14374</a></li>
<li>Use fuzzy compare for floats. <a href='https://commits.kde.org/libkscreen/a96028fdad12beedaed54314a1656ba7ef32076f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14352'>D14352</a></li>
<li>Remove unused variables. <a href='https://commits.kde.org/libkscreen/cd72e176d7a33de44433f1d7a6f58bfa97911de7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14351'>D14351</a></li>
<li>Clean up casts. <a href='https://commits.kde.org/libkscreen/ec381c2e0480e36c30b2016418b3c3d75dcc8426'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14349'>D14349</a></li>
<li>Fix spaces in error messages. <a href='https://commits.kde.org/libkscreen/3e990216dca6dfb1a064d23e4f02c625d3b4c661'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14167'>D14167</a></li>
<li>Fix types, improve readability. <a href='https://commits.kde.org/libkscreen/20c88fd906dda7169d7ee2ff105c51a343de250b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14081'>D14081</a></li>
<li>Fix typo REflect_Y. <a href='https://commits.kde.org/libkscreen/2ecd720c1018a7b8025fc7a2b3f3bbead26120ba'>Commit.</a> </li>
<li>Use the same variable name in function declaration and definition. <a href='https://commits.kde.org/libkscreen/4b101b28fb183176d06c7a30423b7a10219ed5eb'>Commit.</a> </li>
<li>Simplify string comparison. <a href='https://commits.kde.org/libkscreen/9694b4a511e4c6e53afbb2a2989f0f126ad43354'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14078'>D14078</a></li>
<li>Clean up string casts. <a href='https://commits.kde.org/libkscreen/973bef8349d2acc88badafdb6b5437a19162f026'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14060'>D14060</a></li>
<li>Fix fake parser copy and paste error. <a href='https://commits.kde.org/libkscreen/967f21ac54f2a66c85a621491c6036d0d23cb1fb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14048'>D14048</a></li>
<li>Give connect to lambda a context object. <a href='https://commits.kde.org/libkscreen/bd4fa7d318e8e7d2cb854270dca1ca0578cd3847'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14058'>D14058</a></li>
<li>Simplify getting edid in xrandr backend. <a href='https://commits.kde.org/libkscreen/e4236e02b8144a1463c36ad7cb0d014f44c5de24'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14044'>D14044</a></li>
<li>Do not compare smartpointer with nullptr, use .isNull instead. <a href='https://commits.kde.org/libkscreen/06c0513f68bd7cdfb03bd51fa58be42dd5d65557'>Commit.</a> </li>
<li>Use fuzzy compare for floats. <a href='https://commits.kde.org/libkscreen/1fd3bee3d9d63f813969595d118dfa40bc8f077c'>Commit.</a> </li>
<li>Show which screen is primary in debug output. <a href='https://commits.kde.org/libkscreen/56f4b7c351c53f0ef8b0de78523007e51e69d92b'>Commit.</a> </li>
<li>Show which screen is primary in showOutputs. <a href='https://commits.kde.org/libkscreen/eb4bcc421c3ca637deaf7462a35d107d9ef0ccf3'>Commit.</a> </li>
<li>Fix typos. <a href='https://commits.kde.org/libkscreen/14449bd2a9bc38584dc77de941ec895462759c7f'>Commit.</a> </li>
<li>Do not explicitly call empty parent constructor. <a href='https://commits.kde.org/libkscreen/b0661b352becad7333301360b40535a1baafacef'>Commit.</a> </li>
<li>Trvial type warning fixes. <a href='https://commits.kde.org/libkscreen/1554471b5b55654ea51356764d1996f0b254f2cc'>Commit.</a> </li>
<li>Kscreen-doctor: show scale and rotation in output. <a href='https://commits.kde.org/libkscreen/2979b6d01b3584577cd6d90d70cea4e43a94e50f'>Commit.</a> </li>
<li>Fix unit tests for Qt5.9: QCOMPARE has no overload for double vs. int. <a href='https://commits.kde.org/libkscreen/03aed13d9ff3346441dc703699c1f712ca1464d3'>Commit.</a> </li>
<li>Wayland: when blocking remember latest config change instead of crashing. <a href='https://commits.kde.org/libkscreen/af609c37ddfd71dbdd4f8cec4e45639a1d3ed18f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12517'>D12517</a></li>
<li>Have newline at end of files. <a href='https://commits.kde.org/libkscreen/feb1099cd1473ffa519d074cbbb788d98b420456'>Commit.</a> </li>
<li>Use more explicit. <a href='https://commits.kde.org/libkscreen/dab1314f217cffe54acea4b32b64a44ed673225b'>Commit.</a> </li>
<li>Fix typos in comments. <a href='https://commits.kde.org/libkscreen/2ffda49058cc63757610f240975a75bf02e5d49f'>Commit.</a> </li>
<li>Use consistently override. <a href='https://commits.kde.org/libkscreen/11ac5183aa7868adf5d407c06395b4e0dd1b9b31'>Commit.</a> </li>
<li>Use nullptr. <a href='https://commits.kde.org/libkscreen/51fad4d64c451d91001821d31931588c5b888ab1'>Commit.</a> </li>
</ul>


<h3><a name='libksysguard' href='https://commits.kde.org/libksysguard'>libksysguard</a> </h3>
<ul id='ullibksysguard' style='display: block'>
<li>Use lambda instead of QSignalMapper. <a href='https://commits.kde.org/libksysguard/fe44c73bd32ee8a7afbd800af7248c59c236a1a5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15289'>D15289</a></li>
<li>Add new "Tools" button above System Monitor's process list. <a href='https://commits.kde.org/libksysguard/d039c8c74dcb04e483b93f34f037cb81306a2078'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10297'>D10297</a></li>
<li>Remove unneeded QT definitions. <a href='https://commits.kde.org/libksysguard/b9ae4883101f7d6de4403166ce6703e630873688'>Commit.</a> </li>
</ul>


<h3><a name='milou' href='https://commits.kde.org/milou'>Milou</a> </h3>
<ul id='ulmilou' style='display: block'>
<li>Fix minor EBN issues. <a href='https://commits.kde.org/milou/1e239be8df1014c5c4574360def20c824917e7fb'>Commit.</a> </li>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/milou/193f082b8e175b03ccdc5eb4f234d32c67152b7d'>Commit.</a> </li>
</ul>


<h3><a name='oxygen' href='https://commits.kde.org/oxygen'>Oxygen</a> </h3>
<ul id='uloxygen' style='display: block'>
<li>Correcting symlinks between col- and row-resize and split_v and _h cursors. <a href='https://commits.kde.org/oxygen/5bf9dfe906b50f2455210a29a00ec5494d8e26b7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8267'>D8267</a></li>
</ul>


<h3><a name='plasma-browser-integration' href='https://commits.kde.org/plasma-browser-integration'>plasma-browser-integration</a> </h3>
<ul id='ulplasma-browser-integration' style='display: block'>
<li>Keep track of multiple players. <a href='https://commits.kde.org/plasma-browser-integration/a3cf696d5777c158d58f0c017c3d14e903800810'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14221'>D14221</a></li>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/plasma-browser-integration/97c04dc779d27ff0420e6d22bc59b88b9a574093'>Commit.</a> </li>
<li>Also register players immediately in addition to DOMContentLoaded. <a href='https://commits.kde.org/plasma-browser-integration/389bd34ff51349b601c95a8064e4449b9809ba57'>Commit.</a> </li>
<li>Install MutationObserver immediately. <a href='https://commits.kde.org/plasma-browser-integration/183c91c6bb93003880504479333e077ee247e1b2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14217'>D14217</a></li>
<li>Also monitor DOM node being removed for player gone. <a href='https://commits.kde.org/plasma-browser-integration/09c729554ea1b5bea52f01b0d9a86f76f24e500d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395521'>#395521</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14207'>D14207</a></li>
<li>Let download update timer only run when there's running downloads. <a href='https://commits.kde.org/plasma-browser-integration/c8b49b565796a3e6c2c7ffaf3791b539d640d3ff'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14226'>D14226</a></li>
<li>Listen for player "emptied" signal. <a href='https://commits.kde.org/plasma-browser-integration/216d5b12f7d58e6687e169419ea6428cad5c1369'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14209'>D14209</a></li>
<li>Sort keys in translated json file. <a href='https://commits.kde.org/plasma-browser-integration/6535db4f9dcac4323d8fdcb0025418697d2c0c95'>Commit.</a> </li>
<li>Bump manifest version. <a href='https://commits.kde.org/plasma-browser-integration/cb3f2a53ec5a9835cdcd6a9381d6386cdb8ec3e9'>Commit.</a> </li>
<li>USe nullptr/override/ Fix compile with strict compile flags. <a href='https://commits.kde.org/plasma-browser-integration/b8ed382c1f8a90934c7ac5ccbfb9fb8e82b576c6'>Commit.</a> </li>
<li>Also intercept creation of video elements. <a href='https://commits.kde.org/plasma-browser-integration/e77c0489cbcc2cd160138dcc08131f431d961e26'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13878'>D13878</a></li>
<li>Don't keep transfer div in DOM. <a href='https://commits.kde.org/plasma-browser-integration/114a1aa927b2f0e0d9842da36cde89b82b97e6ff'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13883'>D13883</a></li>
<li>Bump manifest version. <a href='https://commits.kde.org/plasma-browser-integration/b58df209d11874c73c5b16c04d53015846814eed'>Commit.</a> </li>
<li>Fix Enhanced Media Controls disabled state. <a href='https://commits.kde.org/plasma-browser-integration/e992f0f1c9eff3ca646f7d94a4c756467bc1bd09'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13845'>D13845</a></li>
<li>Bump manifest version. <a href='https://commits.kde.org/plasma-browser-integration/a0c0e81c341ec8578564941438e68a087e0db2e4'>Commit.</a> </li>
<li>Make content-script media player code follow user settings. <a href='https://commits.kde.org/plasma-browser-integration/bbce684853a3b614678733a97cd813a97684f53f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13628'>D13628</a></li>
<li>Hide settings when run on non-supported platform. <a href='https://commits.kde.org/plasma-browser-integration/27096b7afc8182e7364bb7249b7a83f657b6ee23'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13326'>D13326</a></li>
<li>Commit dev oriented readme. <a href='https://commits.kde.org/plasma-browser-integration/2ba74255fa778c95cd2b6a5bbee896d0bd9a8a05'>Commit.</a> </li>
<li>Add README.md file with link to community wiki page. <a href='https://commits.kde.org/plasma-browser-integration/569620ba0932e67285e0bf4d6687693f340df0ec'>Commit.</a> </li>
<li>Remove unused translation keys. <a href='https://commits.kde.org/plasma-browser-integration/8e3a3a306356122e9fc2dc26e5d117debb1773ac'>Commit.</a> </li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Fix translations kcm to be usable on new users/installations. <a href='https://commits.kde.org/plasma-desktop/991bcf7d0d90045879f5f36dd4ccd46dd2c36e8b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15115'>D15115</a></li>
<li>Add translucent background attribute to desktop icon popup menu. <a href='https://commits.kde.org/plasma-desktop/cc47b95094a331fe5bdd38f340d549dfa1bbf507'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395262'>#395262</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15435'>D15435</a></li>
<li>[Mouse KCM] Avoid changes to touchpads in libinput backend. <a href='https://commits.kde.org/plasma-desktop/a4c724173b5c6a59331587f2e5db746dffbabdc6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395401'>#395401</a>. Fixes bug <a href='https://bugs.kde.org/395722'>#395722</a>. Fixes bug <a href='https://bugs.kde.org/396269'>#396269</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15256'>D15256</a></li>
<li>[Folder View] Create KFilePlacesModel only when needed and listen for changes. <a href='https://commits.kde.org/plasma-desktop/dff9d7fe09f290fa5f960957e0141fad56b0cefa'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14426'>D14426</a></li>
<li>[Folder View] Hide /home/foo/Desktop place in configuration. <a href='https://commits.kde.org/plasma-desktop/45c420b7fd1115f569ffdcdb1be308e1ecf8cdc6'>Commit.</a> See bug <a href='https://bugs.kde.org/398141'>#398141</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15331'>D15331</a></li>
<li>Improve arrow key navigation of Kicker search results. <a href='https://commits.kde.org/plasma-desktop/1692ae244bc5229df78df2d5ba2e76418362cb50'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397779'>#397779</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15286'>D15286</a></li>
<li>[Folder View] Disable the actions themselves rather than just not adding them to the menu. <a href='https://commits.kde.org/plasma-desktop/7590487788e989179498fb58d3f0e40ae1a14019'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15162'>D15162</a></li>
<li>[Folder View] Honor editable_desktop_icons KIOSK restriction. <a href='https://commits.kde.org/plasma-desktop/74697d0c9072d222547ec338a49737c0109cbd86'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15160'>D15160</a></li>
<li>Sorting files by date in Folder View is significantly faster now. <a href='https://commits.kde.org/plasma-desktop/2d008a5b931a266fec3661bcfc0a9db2cac4ae76'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15291'>D15291</a></li>
<li>Fix minor typos. <a href='https://commits.kde.org/plasma-desktop/b8bad291501414a3fb03f994b015e7be5fdb91cc'>Commit.</a> </li>
<li>Update icons kcm docbook to 5.13. <a href='https://commits.kde.org/plasma-desktop/6864d6fee4a220e7345329363e8511a4b617baa5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15024'>D15024</a></li>
<li>[Folder View] Add checkbox for toggling previews. <a href='https://commits.kde.org/plasma-desktop/b2ae7ecf2035688500f962cb87d700d3fe1ce6ed'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15269'>D15269</a></li>
<li>Folder View now enables previews for most file types by default. <a href='https://commits.kde.org/plasma-desktop/d4839a256cb7937b7d972b11e94a288ef250fec0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15268'>D15268</a></li>
<li>[Trash Applet] Use Qt.binding instead of programmatical updating. <a href='https://commits.kde.org/plasma-desktop/95c0c3bc1af64123e47ea547998c8c16d8a5ae17'>Commit.</a> </li>
<li>Don't use older buggier versions of AppStream Qt. <a href='https://commits.kde.org/plasma-desktop/b96651acdca4bb093bc7fcb23eb80456942ab6cc'>Commit.</a> See bug <a href='https://bugs.kde.org/391468'>#391468</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15263'>D15263</a></li>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/plasma-desktop/8bf123691728525cd5cc100807f834ae58e10111'>Commit.</a> </li>
<li>[Widget Explorer] always show the search field. <a href='https://commits.kde.org/plasma-desktop/491137007c1126107052e8efd76d8c9cd445a36d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15101'>D15101</a></li>
<li>Fix minor EBN issues. <a href='https://commits.kde.org/plasma-desktop/89ff12d266d3c5fd16d6208ce2ecefe4bfaed978'>Commit.</a> </li>
<li>Move Widget search field to its own row so it doesn't get compressed. <a href='https://commits.kde.org/plasma-desktop/08983522bfb46744edc1f1300017636990e47a16'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393427'>#393427</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12855'>D12855</a></li>
<li>[Folder Model] Use document-properties icon for "Properties" context menu entry. <a href='https://commits.kde.org/plasma-desktop/6bfd40c06eb6717289561b6cef0fb9f9f8fdd13b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15018'>D15018</a></li>
<li>[Positioner] Be a flat list model. <a href='https://commits.kde.org/plasma-desktop/89af273f379914f03b5d0e550e95d3b8d43c148f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14488'>D14488</a></li>
<li>[Kicker] Use document-properties icon for "Properties" context menu entry. <a href='https://commits.kde.org/plasma-desktop/baf46c91448282776e16d95f06fbf69484de207b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14671'>D14671</a></li>
<li>[Kicker] Look up relative entryPaths. <a href='https://commits.kde.org/plasma-desktop/2ce76bc5ade277f6a020bff062733601dc24e1f2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397070'>#397070</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14647'>D14647</a></li>
<li>Port to KRun::runApplication. <a href='https://commits.kde.org/plasma-desktop/e0d50ff1d88deeb79a4cd49a548e9d0bcf1ad279'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14652'>D14652</a></li>
<li>Fontinst quits after KJob is done. <a href='https://commits.kde.org/plasma-desktop/60c895758f20bd2cb17ab12e57cf34cc37197098'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379524'>#379524</a>. Fixes bug <a href='https://bugs.kde.org/379324'>#379324</a>. Fixes bug <a href='https://bugs.kde.org/349673'>#349673</a>. Fixes bug <a href='https://bugs.kde.org/361960'>#361960</a>. Fixes bug <a href='https://bugs.kde.org/392267'>#392267</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14493'>D14493</a></li>
<li>Restore colors.desktop. <a href='https://commits.kde.org/plasma-desktop/af1605d6e9cef5c092b1694f5f75bc672d551e33'>Commit.</a> </li>
<li>[Multimedia KCM] Kill PulseAudio device setup. <a href='https://commits.kde.org/plasma-desktop/06c8b9bade762a756500986a615915b3b0ce3c36'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14340'>D14340</a></li>
<li>Use QStringLiteral. <a href='https://commits.kde.org/plasma-desktop/646bcc1d685087eaf05fca28860de91fab4b3c4e'>Commit.</a> </li>
<li>Use QStringLiteral/nullptr/explicit. <a href='https://commits.kde.org/plasma-desktop/d385704cab22a76785c9c8a32e85b53b36a10d62'>Commit.</a> </li>
<li>Use QLatin1Char. <a href='https://commits.kde.org/plasma-desktop/935b6190307843298df22c32c11d73eaed47f572'>Commit.</a> </li>
<li>Fix Kickoff compact repr size hints in panel. <a href='https://commits.kde.org/plasma-desktop/664865f12bc756f4ac75e2915dfebb88aa3abf54'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395594'>#395594</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14285'>D14285</a></li>
<li>Use more QStringLiteral/nullptr. <a href='https://commits.kde.org/plasma-desktop/6d4f1bc27d1e566acf8530fe036337b03afa591c'>Commit.</a> </li>
<li>Use QStringLiteral. <a href='https://commits.kde.org/plasma-desktop/78f4cc6f6a89f1be16f7febfecf2b9add8aa2949'>Commit.</a> </li>
<li>Use nullptr/explicit/QStringLiteral. <a href='https://commits.kde.org/plasma-desktop/5b08f70b0e52c92d81a04d05aa54c04efd080124'>Commit.</a> </li>
<li>Revert last commit in two cases - don't compare two char pointers. <a href='https://commits.kde.org/plasma-desktop/e99778b93f40bc3064f43e86903330dad7d3266b'>Commit.</a> </li>
<li>Add some QStringLiteral + nullptr. <a href='https://commits.kde.org/plasma-desktop/a0b1312d0db208e20243868ef0ad3f89d95c3d1a'>Commit.</a> </li>
<li>Use BUILD_TESTING. <a href='https://commits.kde.org/plasma-desktop/92450699ce645e27db7b460cf258a8a700085099'>Commit.</a> </li>
<li>Show a vertical menu for the panel widget options pop-up. <a href='https://commits.kde.org/plasma-desktop/9adb94dc5b83546a031a0b644225d1a7d5c2b8ff'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14145'>D14145</a></li>
<li>Fix drop between shared views. <a href='https://commits.kde.org/plasma-desktop/cab9078eaf21f27c034c93ac94dbbfed8c228f7c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13612'>D13612</a></li>
<li>Fix blur behind folderview context menus. <a href='https://commits.kde.org/plasma-desktop/42a74c9a79baafb222513f3ffad35b736b708454'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395262'>#395262</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14174'>D14174</a></li>
<li>[Pager] Don't show a window list on hover if there is only one window. <a href='https://commits.kde.org/plasma-desktop/7f729fa1268c49e00baaca3b7db5e8be8cd409fd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14026'>D14026</a></li>
<li>Use a trash icon for anything that will remove a widget or a panel. <a href='https://commits.kde.org/plasma-desktop/1f87cec54e1d4fcd9fced13b30a01349c7b0f63f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14138'>D14138</a></li>
<li>Use a better icon for "Remove" on the widget handle. <a href='https://commits.kde.org/plasma-desktop/b00ef00b6dd73276c6ae12b6ebca194abb6cb1a8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14116'>D14116</a></li>
<li>[Folder View] Don't spawn multiple stat jobs for the same folder. <a href='https://commits.kde.org/plasma-desktop/e03217edfc1a002a3ea654c58a28c1e1f689f3bf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14077'>D14077</a></li>
<li>[Folder View] Create rename editor on demand. <a href='https://commits.kde.org/plasma-desktop/6f6a002a48e9987d0cec6794b64dc21ac25f8ee1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14079'>D14079</a></li>
<li>[Kickoff] Remove unused Footer.qml. <a href='https://commits.kde.org/plasma-desktop/ce8c022e0e3b3bf95bf8bbd7ba33096b3ee4fe53'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14055'>D14055</a></li>
<li>[Kickoff] Use KFilePlacesModel groups. <a href='https://commits.kde.org/plasma-desktop/4f1091f1316b98bcb324c40ac621253a0e90c81a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14030'>D14030</a></li>
<li>[Kicker System Entry] Remove KDisplayManager global static. <a href='https://commits.kde.org/plasma-desktop/1cdb7ae4dfea5f986295a46f1f2d04e23f59813b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13999'>D13999</a></li>
<li>Doc: Fix actions description: the buttons are on the bottom. <a href='https://commits.kde.org/plasma-desktop/3a5ee447dc7e898426b3ff8a98a0d3d16e8beb92'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13903'>D13903</a></li>
<li>[Device Automounter] Load kded module only if enabled. <a href='https://commits.kde.org/plasma-desktop/2b9c762f9221290cf56bd8c98d501cafabed9b46'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13852'>D13852</a></li>
<li>Touchpad KCM Pointer Speed Slider Improvement. <a href='https://commits.kde.org/plasma-desktop/86e674c6a2b9bd8c99b419b15f33c0bf898e7d54'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13767'>D13767</a></li>
<li>Mouse KCM Pointer Speed Slider Improvement. <a href='https://commits.kde.org/plasma-desktop/13b35bd8025a4bcf399670c32ee20327b0ace392'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395681'>#395681</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13672'>D13672</a></li>
<li>Touchpad KCM QtQuickControls2 Conversion. <a href='https://commits.kde.org/plasma-desktop/6a4b5870fb2ff918df9e5b8f708e8039b394177e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13573'>D13573</a></li>
<li>Do not duplicate work of KAboutData::setupCommandLine(). <a href='https://commits.kde.org/plasma-desktop/280249015d7e9639fcb211a23f09965ba8be2233'>Commit.</a> </li>
<li>Touchpad KDED module: Convert to JSON metadata. <a href='https://commits.kde.org/plasma-desktop/d0307eff98f27b252252a6b9933032f229c5d25c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13360'>D13360</a></li>
<li>Use value. <a href='https://commits.kde.org/plasma-desktop/2470d502f6b6e13b3ca8480351cc3cef6e8f51c2'>Commit.</a> </li>
<li>Fix warning. <a href='https://commits.kde.org/plasma-desktop/1344fa64bb7d050b438db550018e9b576a9c2fef'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13523'>D13523</a></li>
<li>Mouse KCM Redesign Using Kirigami. <a href='https://commits.kde.org/plasma-desktop/e4ce025aa7065d09765211e1dd0433403b37a51b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13372'>D13372</a></li>
<li>Use the new drag handle in the Language KCM. <a href='https://commits.kde.org/plasma-desktop/59d0458bd09eb7cbe7ae5a15f3a9f951cf9a1b45'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13194'>D13194</a></li>
<li>Touchpad KCM Redesign Using Kirigami. <a href='https://commits.kde.org/plasma-desktop/dd1244d6676620c06011b6c1db0c0ff3d5cdf0ab'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13141'>D13141</a></li>
<li>Make drags from the Task Manager less prone to disaster. <a href='https://commits.kde.org/plasma-desktop/c801276a593c3787fa07fc6640028bcc32dfffd0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13274'>D13274</a></li>
<li>Implement a triangle filter for mouse events on the Kickoff tabbar. <a href='https://commits.kde.org/plasma-desktop/62f92fd822efb059a07dd77c93158ecba73d1ced'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388205'>#388205</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13192'>D13192</a></li>
<li>Translate it. <a href='https://commits.kde.org/plasma-desktop/a2b9942322238dfd4ee5af238a059b66d02717c4'>Commit.</a> </li>
<li>[Baloo KCM] Prevent duplicate paths in the config file. <a href='https://commits.kde.org/plasma-desktop/ed76e11a623da02b8986e670dd418a953e889c71'>Commit.</a> </li>
<li>Make the string 'No touchpad found' translatable. <a href='https://commits.kde.org/plasma-desktop/9c5d8bfd66a44b8de0ae9b79a758959aa3c3a86d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13185'>D13185</a></li>
<li>Revert "[Kicker] Only show "Add to Panel (Widget)" When there's no Task Manager". <a href='https://commits.kde.org/plasma-desktop/c4f2ee35c3ecb3a040cb135c069c7fe1e0df4f1d'>Commit.</a> </li>
<li>Do not show activity switcher during fast switches. <a href='https://commits.kde.org/plasma-desktop/8dc366d6c1632b9ebece272d295dae88bb5e4388'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13042'>D13042</a></li>
<li>Add second line of text for KickerDash item descriptions. <a href='https://commits.kde.org/plasma-desktop/52cd092af19934424e0f1a6d5b3e469c149de2ef'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362986'>#362986</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13122'>D13122</a></li>
<li>Add workspaceoptions docbook. <a href='https://commits.kde.org/plasma-desktop/6229570c3da21072fdb887e585a9b68d9bcaf11e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13022'>D13022</a></li>
<li>Workspace KCM Code Improvement. <a href='https://commits.kde.org/plasma-desktop/de742972bf317182a0cdff6bfa4f6d22abcf9a6a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12974'>D12974</a></li>
<li>Activity switcher auto-hide when using Meta-Tab. <a href='https://commits.kde.org/plasma-desktop/d234b8fde4c3c131545686a117e284b31c6a95a9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393912'>#393912</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13012'>D13012</a></li>
<li>Workspace KCM Redesign Using Kirigami. <a href='https://commits.kde.org/plasma-desktop/78cd8bb31ea329d76ca54889857873033b883ac3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12973'>D12973</a></li>
<li>Update mouse kcm docbook to 5.13. <a href='https://commits.kde.org/plasma-desktop/5c1b125ce1924a8e601455eef58a4e8357682595'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12990'>D12990</a></li>
<li>[Kicker] Only show "Add to Panel (Widget)" When there's no Task Manager. <a href='https://commits.kde.org/plasma-desktop/8d113b09598c0cd0f92d6395c7c7dec8c65fa735'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390817'>#390817</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12969'>D12969</a></li>
</ul>


<h3><a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a> </h3>
<ul id='ulplasma-integration' style='display: block'>
<li>Revert "Search in default path before calling find_package". <a href='https://commits.kde.org/plasma-integration/e722319d870db8f83e27f17ff8963f26aff4ac2f'>Commit.</a> </li>
<li>Revert "Using Qt5Core rather than Qt5Widget for the version check". <a href='https://commits.kde.org/plasma-integration/46c8b2a2ac7421b363b574a4c4f2a04ac8f0d9ea'>Commit.</a> </li>
<li>Revert "Search in default path before calling pkg-config". <a href='https://commits.kde.org/plasma-integration/4392ad581658024d33c1997804b4d3f5f6ebf1c9'>Commit.</a> </li>
<li>Search in default path before calling pkg-config. <a href='https://commits.kde.org/plasma-integration/d9578de8298f75f3c65c55ec8621cdb1a925c092'>Commit.</a> </li>
<li>Using Qt5Core rather than Qt5Widget for the version check. <a href='https://commits.kde.org/plasma-integration/831db7ed507a33f859901dae7274115be22fcc1a'>Commit.</a> </li>
<li>Search in default path before calling find_package. <a href='https://commits.kde.org/plasma-integration/693cf9d952b79c35d4c6c1297f91364de67f5b74'>Commit.</a> </li>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/plasma-integration/920f9d75aad936a6b66bb13363964c66c98b92ce'>Commit.</a> </li>
<li>Don't depend on the exact version of Breeze. <a href='https://commits.kde.org/plasma-integration/893175d8c38be9157545a50dbd155d873027a9d5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15008'>D15008</a></li>
<li>Fix QFileDialog not remembering the last visited directory. <a href='https://commits.kde.org/plasma-integration/4848bec177b2527662098f97aa745bb839bc15e3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14437'>D14437</a></li>
<li>KDE platform plugin: don't force default stylename on user-specified fonts. <a href='https://commits.kde.org/plasma-integration/2e971be576d24a82974eaf2397d4703fca0188d8'>Commit.</a> See bug <a href='https://bugs.kde.org/378523'>#378523</a>. Phabricator Code review <a href='https://phabricator.kde.org/D9070'>D9070</a></li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Make sure we change icon on NM connectivity change. <a href='https://commits.kde.org/plasma-nm/a71ca963f798ca405878305b5433550e6c267d87'>Commit.</a> </li>
<li>Use QOverload to select overloaded functions. <a href='https://commits.kde.org/plasma-nm/7e578ddc687310a25e5fff89aaa5f8b8fa330804'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15297'>D15297</a></li>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/plasma-nm/250e7ac7e9724561fe02f543d94ef1d3e313a903'>Commit.</a> </li>
<li>Use NetworkManager::DeviceStatistics instead of Plasma data engine. <a href='https://commits.kde.org/plasma-nm/36dd019c98107cf1f1155988915841048b607c1b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14835'>D14835</a></li>
<li>Require NM 1.4.0 and newer. <a href='https://commits.kde.org/plasma-nm/888058adbed37c997e9cd7ecaca7350f90a6831f'>Commit.</a> </li>
<li>Unbreak SSH agent support for SSH VPN tunnels. <a href='https://commits.kde.org/plasma-nm/38b659465d864fa1c33e0eb3ab360dccf8b4475b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14035'>D14035</a></li>
<li>Parse protocol in port option. <a href='https://commits.kde.org/plasma-nm/1feaa4ca1cdf7c5df37ae45b7053b54821702980'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14130'>D14130</a></li>
<li>Port away from KLocale and KDELibs4Support. <a href='https://commits.kde.org/plasma-nm/9a7c8370d9b5a482898c399a6610fd2257037570'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14794'>D14794</a></li>
<li>Update connection details only when necessary. <a href='https://commits.kde.org/plasma-nm/ab45698f7c6dd197f9ebaf0317f469b82fad55fe'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14793'>D14793</a></li>
<li>Use more nullptr. <a href='https://commits.kde.org/plasma-nm/1b911b2b74b153cd66e46491bd309144b2f7edcb'>Commit.</a> </li>
<li>Port from KStandardDirs to QStandardPaths. <a href='https://commits.kde.org/plasma-nm/f327922e60fb84f55aef75297d44d6f958355039'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13818'>D13818</a></li>
<li>Use non-deprecated KDEInstallDirs variables. <a href='https://commits.kde.org/plasma-nm/c8a29ae8f6eacd6d66477098e7b174fa310796c0'>Commit.</a> </li>
<li>Use KF5_MIN_VERSION also for KF5ModemManagerQt. <a href='https://commits.kde.org/plasma-nm/5c3e0d9ec0f0fda1fd2c8d01adbd43b29f9dd937'>Commit.</a> </li>
<li>Find KF5NetworkManagerQt together with other KF5 modules. <a href='https://commits.kde.org/plasma-nm/2d5534334e894f146a5f01747cc3193d00d5720b'>Commit.</a> </li>
<li>Fix label buddy in fortisslvpn auth widget. <a href='https://commits.kde.org/plasma-nm/41cfaa2eef860520026b5c6ed907c8eff2c30630'>Commit.</a> </li>
<li>Cleanup used Qt & KF libraries a bit. <a href='https://commits.kde.org/plasma-nm/a44b7f38c548d756b6cb822c047ef71cf67645f5'>Commit.</a> </li>
<li>Remove unneeded include. <a href='https://commits.kde.org/plasma-nm/8dd200485f13444907c88fa4e01712c2ca9d1240'>Commit.</a> </li>
<li>Use KF5Wallet include for KWallet, not the kdelibs4support one. <a href='https://commits.kde.org/plasma-nm/9ca67ddcf60acf27264c26a2318913e4bc11677c'>Commit.</a> </li>
<li>Remove duplicated include. <a href='https://commits.kde.org/plasma-nm/eee509c736f634d577768317fcc3556cccfd278c'>Commit.</a> </li>
<li>Use explicit. <a href='https://commits.kde.org/plasma-nm/c784d67683efd1d1a3a16f5199eb2552a425b620'>Commit.</a> </li>
<li>Use nullptr. <a href='https://commits.kde.org/plasma-nm/6bfec59d82f3eb12c044c99c42ae39cbd4763ac5'>Commit.</a> </li>
<li>Use override consistently. <a href='https://commits.kde.org/plasma-nm/111ac65ae79f1a777e3b4a6389e916f0dfccd35e'>Commit.</a> </li>
<li>Remove unused entry X-KDE-DBus-ModuleName from the kded plugin metadata. <a href='https://commits.kde.org/plasma-nm/17761be9acc9c29ce24c40b6d2b221420c0674dd'>Commit.</a> </li>
<li>Remove usage of dead QT_USE_FAST_CONCATENATION. <a href='https://commits.kde.org/plasma-nm/bb4754d7c157f02ffdc6fcc133537fe4cc1a7a70'>Commit.</a> </li>
<li>Remove Qt module includes. <a href='https://commits.kde.org/plasma-nm/76ac1d533b3215cae52489a3655e3a1f4142b47a'>Commit.</a> </li>
<li>Remove unneeded cmake macro includes. <a href='https://commits.kde.org/plasma-nm/13dc3728f8453bcbf90ae4bebc5339db39d3c280'>Commit.</a> </li>
<li>Use KF5_MIN_VERSION also for ECM, now that it's part of KF. <a href='https://commits.kde.org/plasma-nm/115029e88762b486733bc342cc5322ef768ecf35'>Commit.</a> </li>
<li>Require KDE Frameworks 5.42 and Qt 5.9. <a href='https://commits.kde.org/plasma-nm/b446e8cf2ba6be0512e9bdb2280287acb33e4f87'>Commit.</a> </li>
<li>Bump min cmake version to 3.0, move cmake_minimum_required to begin. <a href='https://commits.kde.org/plasma-nm/bc4944684c77719a4f52edf69d9f74878765009f'>Commit.</a> </li>
</ul>


<h3><a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>Use unpretty channel name in speakertest. <a href='https://commits.kde.org/plasma-pa/7cd16db35a491e8e9e8591c5ef9327aef3172fcc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14362'>D14362</a></li>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/plasma-pa/88e2ffb7538b5cc0573860b2afeddd00b0c600a3'>Commit.</a> </li>
<li>Show "muted" icon for dummy output. <a href='https://commits.kde.org/plasma-pa/ae9fb0a57cefdc5fdef0a8ef8ea5ccfac07fadfe'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14156'>D14156</a></li>
<li>Add .kdev4 to gitignore. <a href='https://commits.kde.org/plasma-pa/e2287ccc02cf640c80a45d004ec7d1be7b325bf8'>Commit.</a> </li>
<li>Add QT_NO_KEYWORDS definition. <a href='https://commits.kde.org/plasma-pa/dd15322e50eeb2b7771c3a47dcacac534f3d8327'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14157'>D14157</a></li>
<li>[KCM] Add speaker placement test. <a href='https://commits.kde.org/plasma-pa/ed0c58639d127690742a596bd249b9eea566a77c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13931'>D13931</a></li>
<li>Fix last merge - missing changes in src/CMakeLists.txt. <a href='https://commits.kde.org/plasma-pa/8d7543ebd79e4fe9e2d5002c94ff8e6008b20327'>Commit.</a> </li>
<li>Use non-deprecated KDEInstallDirs variables. <a href='https://commits.kde.org/plasma-pa/6c7f8266929e20205d7aedd6ac2a99c2353e12f0'>Commit.</a> </li>
<li>Use more explicit. <a href='https://commits.kde.org/plasma-pa/22bade6f19e8cd15c7b43eb2866c17aea20250c4'>Commit.</a> </li>
<li>Use consistently nullptr. <a href='https://commits.kde.org/plasma-pa/a46365459b75a5bdefaedd6bf1dc13787767ace4'>Commit.</a> </li>
<li>Use consistently override (or final where Q_DECL_FINAL was used). <a href='https://commits.kde.org/plasma-pa/b18687ce6741ba83c0085fa90361a11fdcb56e98'>Commit.</a> </li>
<li>No need to add ECM_KDE_MODULE_DIR, part of ECM_MODULE_PATH. <a href='https://commits.kde.org/plasma-pa/2f79730ef7d3b695e2a12edef4c2c5a5fc352bb2'>Commit.</a> </li>
<li>Use more common QDebug include, not QtDebug. <a href='https://commits.kde.org/plasma-pa/5adf1506bb58d08ce0b6bed39b7adc30959e70d5'>Commit.</a> </li>
<li>Remove Qt module prefix from include. <a href='https://commits.kde.org/plasma-pa/9d5d4ab9acf22c4fcf8f0d1e28caf176a8910c76'>Commit.</a> </li>
<li>Remove Qt module include. <a href='https://commits.kde.org/plasma-pa/77a07ba133beba6e5eec2bd8cf5d8adc50820814'>Commit.</a> </li>
<li>Use KF5_MIN_VERSION also for ECM, now that it's part of KF. <a href='https://commits.kde.org/plasma-pa/c8bc63eea142e0948c439f38ec86e2d4ac25cfea'>Commit.</a> </li>
<li>Require KDE Frameworks 5.42 and Qt 5.9. <a href='https://commits.kde.org/plasma-pa/e5c522f3fd935c7a4dc4cb80abe46d9bcc077505'>Commit.</a> </li>
<li>Bump min cmake version to 3.0, move cmake_minimum_required to begin. <a href='https://commits.kde.org/plasma-pa/9e76a0d4f48c487c9fb3610eeffd6bf7cef460f5'>Commit.</a> </li>
<li>Fix wrong availability of profiles and ports. <a href='https://commits.kde.org/plasma-pa/ba737f478208b37826f14c929ab8611a2cb20798'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13694'>D13694</a></li>
</ul>


<h3><a name='plasma-sdk' href='https://commits.kde.org/plasma-sdk'>Plasma SDK</a> </h3>
<ul id='ulplasma-sdk' style='display: block'>
<li>Hide button. <a href='https://commits.kde.org/plasma-sdk/78ae97ae615419d00dec467cef2dde506bd9309d'>Commit.</a> </li>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/plasma-sdk/68996ed8afe1f0e5fd4d92cf9736bfdae395525e'>Commit.</a> </li>
</ul>


<h3><a name='plasma-tests' href='https://commits.kde.org/plasma-tests'>plasma-tests</a> </h3>
<ul id='ulplasma-tests' style='display: block'>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/plasma-tests/7b3ef24e5c4492e473643ae1da03b95af4d49afa'>Commit.</a> </li>
</ul>


<h3><a name='plasma-vault' href='https://commits.kde.org/plasma-vault'>plasma-vault</a> </h3>
<ul id='ulplasma-vault' style='display: block'>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/plasma-vault/658de446d85b7d7a012990a28f21e20950663830'>Commit.</a> </li>
<li>Refactored the wizard logic. <a href='https://commits.kde.org/plasma-vault/18db0b79dff55f136c04f4c61847ed8fc458c2a2'>Commit.</a> </li>
<li>Remove the trailing slashes from the mount point. <a href='https://commits.kde.org/plasma-vault/7ee10da0d6283154846f5654a1f8b5d802684951'>Commit.</a> </li>
<li>Showing error messages for badly chosen paths. <a href='https://commits.kde.org/plasma-vault/bdf111ba714cce6d4c71b9c1f158d321e58e4e40'>Commit.</a> </li>
<li>Importing existing encrypted directories added to the backend. <a href='https://commits.kde.org/plasma-vault/42ad78f258019eb244901b3cf2fbd8abe1e437f0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386200'>#386200</a></li>
<li>Postpone deleting vaults while there are async operations on them. <a href='https://commits.kde.org/plasma-vault/b32b0e71146d11a4990f716ed7d01569ce19e243'>Commit.</a> </li>
<li>Better design for the directory chooser. <a href='https://commits.kde.org/plasma-vault/b8411a5c9f828a1bd60f570b7538668dbda492da'>Commit.</a> </li>
<li>Allowing custom options to be passed to encfs and cryfs. <a href='https://commits.kde.org/plasma-vault/108d408dac3c875cd71272305ae6a8d096ef4b5a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396621'>#396621</a></li>
<li>Make dependency on KF5NetworkManagerQt optional. <a href='https://commits.kde.org/plasma-vault/42723bb3cb70e99a7cefe799a536de7ff9cdca31'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13255'>D13255</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Set gtk-modules to auto-load appmenu-gtk-module. <a href='https://commits.kde.org/plasma-workspace/3954e7e07c9c1d5ef3110b0c0d79582f2c94eeeb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15315'>D15315</a></li>
<li>[AppStream Runner] Reduce verbosity of log output. <a href='https://commits.kde.org/plasma-workspace/8d945c2e5547c117ad6b5d3333a456c00c8e9310'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14806'>D14806</a></li>
<li>Plasmashell freezes when trying to get free space info from mounted remote filesystem after losing connection to it. <a href='https://commits.kde.org/plasma-workspace/e1c19ce4daf92a14dee44b44d199672034a346c0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397537'>#397537</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14895'>D14895</a></li>
<li>Add back comment and fix indentation. <a href='https://commits.kde.org/plasma-workspace/2949e7c432b59c5d7762bfb3780319d193089bdc'>Commit.</a> </li>
<li>Do ignore-filtered-siblings properly. <a href='https://commits.kde.org/plasma-workspace/c81205cc458e1b08eb06295ac31d9bb46cfee9f6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15462'>D15462</a></li>
<li>Re-filter launcher when a window changes identity. <a href='https://commits.kde.org/plasma-workspace/a057edc7e9fbe9c8cd8178889e80e2c938156f39'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15458'>D15458</a></li>
<li>Handle clients which change window metadata during early startup. <a href='https://commits.kde.org/plasma-workspace/b15eaf38b6bf8d5af7fdc0caff05679a063819cf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396871'>#396871</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15410'>D15410</a></li>
<li>Open Web Shorcuts KCM from Web Shortcut Runner config. <a href='https://commits.kde.org/plasma-workspace/4b4439fe2ef5fdcf236f6f6bb2237a6499a812f5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398242'>#398242</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15430'>D15430</a></li>
<li>[libdbusmenuqt] Port to categorized logging. <a href='https://commits.kde.org/plasma-workspace/1e1ce733920d4086c500ead03987b32eb175b18c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15447'>D15447</a></li>
<li>Add some special case mappings for appmenu-gtk-module. <a href='https://commits.kde.org/plasma-workspace/4bc3eb8300078c18adec3edb9797c341081093a3'>Commit.</a> </li>
<li>[Bookmarks Runner] Fix cleanup of favicon directory. <a href='https://commits.kde.org/plasma-workspace/140b908e4c1799527eca781f61b68ebcf03a30e1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15391'>D15391</a></li>
<li>Don't consider filtered out tasks siblings when sorting in new tasks. <a href='https://commits.kde.org/plasma-workspace/c8358c203f118f2559651c0a48d12a825ffee7da'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14392'>D14392</a></li>
<li>Use consistent "Suspend" terminology. <a href='https://commits.kde.org/plasma-workspace/4500ff9de102a2cae19855678b7392fa9504e258'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15386'>D15386</a></li>
<li>[Bookmarks Runner] Remove unused define. <a href='https://commits.kde.org/plasma-workspace/9be8c3c7e59bf54e515e1ff1fa4ed93425ae540b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15384'>D15384</a></li>
<li>Fix Firefox profile location lookup after location has changed. <a href='https://commits.kde.org/plasma-workspace/dda9aaba2d5c4004ee150f9c2c835cc83f87d3c6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15254'>D15254</a></li>
<li>[Bookmarks Runner] Filter out empty urls, simplify, fix comment. <a href='https://commits.kde.org/plasma-workspace/08895fd7f1ab0e7299e6c166f4439e38a93fc476'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15356'>D15356</a></li>
<li>BookmarksRunner: Avoid multiple connections of identical signal. <a href='https://commits.kde.org/plasma-workspace/cb6805374d122ee909c8b6c11c4507f95e081169'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15306'>D15306</a></li>
<li>[Logout Dialog] Add "Hibernate" option. <a href='https://commits.kde.org/plasma-workspace/33746fa4e7e2e2d57691983e090ba6b2a3595fbd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398184'>#398184</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15287'>D15287</a></li>
<li>Add appmenu-gtk-module as runtime dependency. <a href='https://commits.kde.org/plasma-workspace/0a56e67cd5cd120bd92bf4071380aee9ffeeaa5b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15327'>D15327</a></li>
<li>KRunner: remove no longer existant and unused column from SQL query. <a href='https://commits.kde.org/plasma-workspace/cb99ab735c813b250f4cd5031b4b078fe0b6c250'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398305'>#398305</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15305'>D15305</a></li>
<li>Port to PlasmaComponents3. <a href='https://commits.kde.org/plasma-workspace/51d863ce2438f132add9a469807d38947ad7fca8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397845'>#397845</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15165'>D15165</a></li>
<li>[Media Player] Change int to double for positions. <a href='https://commits.kde.org/plasma-workspace/1bb02b98cfedfd6b51ac8de0c34a5778659433f7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397591'>#397591</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15311'>D15311</a></li>
<li>[Media Controller] Avoid warnings when player quits. <a href='https://commits.kde.org/plasma-workspace/c6343ff485800f97cfcb3d67a5d80de2d6002305'>Commit.</a> </li>
<li>Specify minimum version for KScreenlocker, use correct DBUS path. <a href='https://commits.kde.org/plasma-workspace/aab6b5c23ea59c529e4ead82a2435fbd73fac0cf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15229'>D15229</a></li>
<li>[PanelShadows] Use 0 offset for disabled borders on Wayland. <a href='https://commits.kde.org/plasma-workspace/1cfe69b80d47982c43e94574d3cd2fe57b1fdbab'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14999'>D14999</a></li>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/plasma-workspace/8cf6f8e3ba0ab8083308365e0a6bf5251a37ea2e'>Commit.</a> </li>
<li>Restore initial setting of ksmserver dialogActive. <a href='https://commits.kde.org/plasma-workspace/66ab5f3e3e70af947dba22fcbd2654c5ae3e5e8b'>Commit.</a> </li>
<li>Further cleanup. <a href='https://commits.kde.org/plasma-workspace/d57e530f3a463aa359fd052aa397e3916f25b05a'>Commit.</a> </li>
<li>Merge switch user dialog into lockscreen. <a href='https://commits.kde.org/plasma-workspace/084b9a408cda665328e440c354ca8ef19207d0f6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15187'>D15187</a></li>
<li>Fix kscreenlocker_greet --switchuser. <a href='https://commits.kde.org/plasma-workspace/747135631bfe3af5f37faa9a5d0b1a44a53f4dd1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15185'>D15185</a></li>
<li>[AppStream Runner] Use categorized logging. <a href='https://commits.kde.org/plasma-workspace/86e3748f904a85f2ea721781cc6e1ba28bbe3c96'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14805'>D14805</a></li>
<li>[AppStream Runner] Pass on error message from the library. <a href='https://commits.kde.org/plasma-workspace/74a38143e2130aa9d80a8161b5a5fecd2c0fac64'>Commit.</a> See bug <a href='https://bugs.kde.org/374279'>#374279</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14804'>D14804</a></li>
<li>[AppStream Runner] Also search when there were errors during Pool::load. <a href='https://commits.kde.org/plasma-workspace/0ec3f76810b712b99f18ef5f1afa70fe4a3884fd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397531'>#397531</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14807'>D14807</a></li>
<li>Fix setting primary connector if primary output changed. <a href='https://commits.kde.org/plasma-workspace/ea19f9b6f59d7b1090b17b1bf09e070f993b325b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13853'>D13853</a></li>
<li>Allow wallpaper slideshow blur config to persist. <a href='https://commits.kde.org/plasma-workspace/26050397cbaf4a96e101f25356b0e2804c938bf9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13555'>D13555</a></li>
<li>[Klipper] Only create KHelpMenu when used. <a href='https://commits.kde.org/plasma-workspace/adff6dcf7759c11417f3e57d2c30fd2c0c5f9578'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14487'>D14487</a></li>
<li>Fix minor EBN issues. <a href='https://commits.kde.org/plasma-workspace/ea415539fc6256494d5c12296a6216e522e12b0a'>Commit.</a> </li>
<li>Use QJSValue as method parameter type for the scripting interface. <a href='https://commits.kde.org/plasma-workspace/6e5c9e9b16666cea60f8d95f33dfd2664c7ea626'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397338'>#397338</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14822'>D14822</a></li>
<li>Only warn if there is more than one user logged in. <a href='https://commits.kde.org/plasma-workspace/ec4dfb974e115e1f2936252ba217717bfc9a270a'>Commit.</a> </li>
<li>Warn on logout screen when another user is logged in. <a href='https://commits.kde.org/plasma-workspace/dd149d62b61c1b69bc2506abc7da967deccca558'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14905'>D14905</a></li>
<li>Label job notifications with destination file name. <a href='https://commits.kde.org/plasma-workspace/499d145e1a36fa0e08ba9013c41805483c22da83'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396744'>#396744</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14857'>D14857</a></li>
<li>[Shell Corona] Don't create ScriptingEngine if there are no update scripts. <a href='https://commits.kde.org/plasma-workspace/f326b1435a714b45c48fbc78a4f0b68106d429a6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14751'>D14751</a></li>
<li>Use a broom-style icon for clearing clipboard and notification history. <a href='https://commits.kde.org/plasma-workspace/ad9ec635bed1a45dc73faca68be3041f90c7b086'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14105'>D14105</a></li>
<li>[Lock Screen] Fix avatar not showing in software rendering. <a href='https://commits.kde.org/plasma-workspace/665c9b1c431d048674c08a48c44f53c1e29d0e08'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14051'>D14051</a></li>
<li>[Logout Greeter] Remove KDM cruft. <a href='https://commits.kde.org/plasma-workspace/485193ebcaf0ec4b61236eab0e4b1da09752c71b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13998'>D13998</a></li>
<li>Add some QStringLiteral. <a href='https://commits.kde.org/plasma-workspace/e1b9f53dff3c952afeb41c06def323ab87991644'>Commit.</a> </li>
<li>Build solidautoeject only on FreeBSD. <a href='https://commits.kde.org/plasma-workspace/edd5e889f58306431bedcd7a82a13841fb46a3c3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13752'>D13752</a></li>
<li>Updated RunCommand.qml to scroll with Ctrl + home row (j, k). <a href='https://commits.kde.org/plasma-workspace/a58e48707601d627781c6681389e4abe0f6e8899'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13686'>D13686</a></li>
<li>[User Switcher] Swap Cancel and Switch buttons. <a href='https://commits.kde.org/plasma-workspace/eaa093443a7ce7ee2befd321a974d4d7a8ad7801'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13923'>D13923</a></li>
<li>Remove pointless setMin/setMax size on DesktopView. <a href='https://commits.kde.org/plasma-workspace/c96a9f5afec325f0beaa5b4ea4c5ac603f5bfff0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13850'>D13850</a></li>
<li>Remove usage of dead QT_USE_FAST_CONCATENATION. <a href='https://commits.kde.org/plasma-workspace/29dc4f4f3988039d0d13529384cd2688637e3e20'>Commit.</a> </li>
<li>Fix-up last merge, remove slipped back X-KDE-DBus-ModuleName. <a href='https://commits.kde.org/plasma-workspace/b296b92036bd905585463d432809d110c0d90e29'>Commit.</a> </li>
<li>Do not duplicate work of KAboutData::setupCommandLine(). <a href='https://commits.kde.org/plasma-workspace/e3681ccfa40402f06b994b2344f080293c15fa7b'>Commit.</a> </li>
<li>Systemmonitor: use different label format when the applet is on a vertical panel. <a href='https://commits.kde.org/plasma-workspace/740289bd824df858d3ff9ebdccd1bfd5e2459218'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12857'>D12857</a></li>
<li>Port Plasma Desktop Scripting to QJSEngine. <a href='https://commits.kde.org/plasma-workspace/033ad5ca60b3d4250a4f354fa38fc60f7d97c78b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13112'>D13112</a></li>
<li>Re-order setting sources and add_library. <a href='https://commits.kde.org/plasma-workspace/17e2699460870be3fe1c3c52cc2a130c2d5ff438'>Commit.</a> </li>
<li>Use logging categories in a few dataengines. <a href='https://commits.kde.org/plasma-workspace/a8573d1be5a45cca1b60504a56289c0bfcb3d834'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13357'>D13357</a></li>
<li>Warning--. <a href='https://commits.kde.org/plasma-workspace/a9993017143e6bda49c930aeba7f20b4f1b85fc3'>Commit.</a> </li>
<li>[FreeSpaceNotifier] Use KIO FileSystemFreeSpaceJob. <a href='https://commits.kde.org/plasma-workspace/fb96a53daaaa86648b24a64cc4091e4a84872654'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13350'>D13350</a></li>
<li>Add Exec= key. <a href='https://commits.kde.org/plasma-workspace/11e570d861858f3b3e2694e5f9ec242d7f9f12b9'>Commit.</a> </li>
<li>Port away from KLineEdit. <a href='https://commits.kde.org/plasma-workspace/bf5e72907510bb60cbf83ea6bfa4a12d8f5eeeb9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13071'>D13071</a></li>
<li>Add missing QUrlQuery includes. <a href='https://commits.kde.org/plasma-workspace/ed274a056de9683fb80e4b4f8461cbcaec472f8f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13070'>D13070</a></li>
</ul>


<h3><a name='plymouth-kcm' href='https://commits.kde.org/plymouth-kcm'>Plymouth KControl Module</a> </h3>
<ul id='ulplymouth-kcm' style='display: block'>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/plymouth-kcm/055a10e7b1c64b0cc75bbf3863dc89e598f8cc1b'>Commit.</a> </li>
</ul>


<h3><a name='polkit-kde-agent-1' href='https://commits.kde.org/polkit-kde-agent-1'>polkit-kde-agent-1</a> </h3>
<ul id='ulpolkit-kde-agent-1' style='display: block'>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/polkit-kde-agent-1/3bccc5a46be421c385011d6f1b1660ce602e881c'>Commit.</a> </li>
<li>Align lock icon with bold message text; reduce overall size of dialog. <a href='https://commits.kde.org/polkit-kde-agent-1/d4ee26a76a217a5beed73c44080b3a49b7b681a0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12311'>D12311</a></li>
</ul>


<h3><a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>Remove XRandrBrightness. <a href='https://commits.kde.org/powerdevil/7e29869329184d9e2bae6019939e2ecb0eceac0d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15261'>D15261</a></li>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/powerdevil/a75a4c251434b0784c1e9769189c9dbbb5e35a72'>Commit.</a> </li>
<li>Remove wrong & unused X-KDE-DBus-ModuleName entry from daemon app metadata. <a href='https://commits.kde.org/powerdevil/cbdc574e45a92cc8c7ec11d5ffd1c6f1775b8a63'>Commit.</a> </li>
<li>Display brightness OSD even when min and max is reached. <a href='https://commits.kde.org/powerdevil/4b8ae4d708a6f4cc854ea82ca96987f0b47ea722'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387284'>#387284</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13030'>D13030</a></li>
</ul>


<h3><a name='sddm-kcm' href='https://commits.kde.org/sddm-kcm'>SDDM KCM</a> </h3>
<ul id='ulsddm-kcm' style='display: block'>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/sddm-kcm/f2b5f266e07f83bad64c96a57f75de4959c1841d'>Commit.</a> </li>
<li>Sync MAX_UID with upstream. <a href='https://commits.kde.org/sddm-kcm/1421359002d6022c2d183ccad830cb4147bc917c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13911'>D13911</a></li>
<li>Remove deprecated class. <a href='https://commits.kde.org/sddm-kcm/097d6c64ae489dc684356e23e8d627b88df8c02a'>Commit.</a> </li>
<li>Check version. <a href='https://commits.kde.org/sddm-kcm/6faea6aa7dd2ddce9696d8900f7546504dc7022a'>Commit.</a> </li>
<li>Remove unused include moc. <a href='https://commits.kde.org/sddm-kcm/5d641f20e0b11a7696b5b94d7105fcce074269da'>Commit.</a> </li>
<li>Use nullptr. Use const'ref. <a href='https://commits.kde.org/sddm-kcm/7a9b287424b6c06e175e64bcb1bdecbeba6afaba'>Commit.</a> </li>
<li>Make compile with strict compile flags (I found a missing i18n too). <a href='https://commits.kde.org/sddm-kcm/67ab368ca6ed1b35a44b8e81577a51e8077d13a8'>Commit.</a> </li>
</ul>


<h3><a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a> </h3>
<ul id='ulsystemsettings' style='display: block'>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/systemsettings/596a5051113f9edb7bce86804cef0f449cc01ad4'>Commit.</a> </li>
<li>Forget all kcms that don't exist anymore. <a href='https://commits.kde.org/systemsettings/6ce66025e976b3629197eff46abad5daf75a217e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394553'>#394553</a></li>
<li>Quit gracefully on sidebar QML errors. <a href='https://commits.kde.org/systemsettings/81e167416bb1286a8e114890b4e5c69b33c003e5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394902'>#394902</a></li>
<li>Do not duplicate work of KAboutData::setupCommandLine(). <a href='https://commits.kde.org/systemsettings/86c2497ffa57c3c9dc0f2133eff6f82a0b205b7d'>Commit.</a> </li>
</ul>


<h3><a name='user-manager' href='https://commits.kde.org/user-manager'>User Manager</a> </h3>
<ul id='uluser-manager' style='display: block'>
<li>Fix minor EBN issues. <a href='https://commits.kde.org/user-manager/c364c727d5d85e4172495114b2da6ca1ebc9990d'>Commit.</a> </li>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/user-manager/b414353dfb48b6ceda72980e0a75f0d36e4ec332'>Commit.</a> </li>
<li>Use override. Use nullptr. <a href='https://commits.kde.org/user-manager/7376011b98290a33533107c64d940e064e1000cf'>Commit.</a> </li>
<li>Fix compile with strict compile flags. <a href='https://commits.kde.org/user-manager/80eb5f3ac82b969e75eeef30e3c201e2d00c84e6'>Commit.</a> </li>
<li>Update version. <a href='https://commits.kde.org/user-manager/2fe718369a5509924b7ce5fe0236df75e4915acb'>Commit.</a> </li>
<li>Warning--. <a href='https://commits.kde.org/user-manager/8165e2259e6d10023f3973a0285a120682a62a02'>Commit.</a> </li>
</ul>


<h3><a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a> </h3>
<ul id='ulxdg-desktop-portal-kde' style='display: block'>
<li>QT_MIN_VERSIONS is Qt 5.11 for Plasma 5.14. Agreed at kickoff meeting.  Set everywhere for clearity and consistency. <a href='https://commits.kde.org/xdg-desktop-portal-kde/242a3a283a69eb0af5e649215ab50472192f2f12'>Commit.</a> </li>
<li>Add support for PickColor method from Screenshot portal. <a href='https://commits.kde.org/xdg-desktop-portal-kde/ee590b2242257f6b96edda6a784b3972ee272387'>Commit.</a> </li>
<li>Use prefix ++operator for loop index increment. <a href='https://commits.kde.org/xdg-desktop-portal-kde/ad5ad003c676408aaa3b7c3523405586e05edd5a'>Commit.</a> </li>
<li>Pass substiture args to i18n strings inside the i18n call. <a href='https://commits.kde.org/xdg-desktop-portal-kde/b74474e7a3a3c423c7f7831aaf90f3249cc4c499'>Commit.</a> </li>
<li>Include own header first. <a href='https://commits.kde.org/xdg-desktop-portal-kde/cdb5948b49f8abc03cf5a50e0e52fa4d1c370511'>Commit.</a> </li>
<li>Use explicit. <a href='https://commits.kde.org/xdg-desktop-portal-kde/175331454b865bf22f9673b876ea9fd0cfe93bbc'>Commit.</a> </li>
<li>NULL -> nullptr (this is C++, not C in the end). <a href='https://commits.kde.org/xdg-desktop-portal-kde/b359f00c069f82ce2fef5f53cba73a6f68977c2a'>Commit.</a> </li>
<li>Use {} initializer instead of 0 for QFlags-based args. <a href='https://commits.kde.org/xdg-desktop-portal-kde/4a82041ede95776f70e95cc60c0ae4572fd334d8'>Commit.</a> </li>
<li>Remove Qt module includes. <a href='https://commits.kde.org/xdg-desktop-portal-kde/fa0628079db3309fbbb9734f5791634cb5f045ff'>Commit.</a> </li>
<li>Properly find and link against Qt5::Concurrent. <a href='https://commits.kde.org/xdg-desktop-portal-kde/215c97e8d39e0c7f324f0ba252d01d85ae069fc1'>Commit.</a> </li>
<li>Remove unneeded cmake macro includes. <a href='https://commits.kde.org/xdg-desktop-portal-kde/2366a5ef8328290933149bcea60e77dcae2d4608'>Commit.</a> </li>
<li>Use KF5_MIN_VERSION also for ECM, now that it's part of KF. <a href='https://commits.kde.org/xdg-desktop-portal-kde/7d2a02204d1fefd81b5dfcd5489afe0a72702f6c'>Commit.</a> </li>
<li>Require KDE Frameworks 5.42 and Qt 5.9. <a href='https://commits.kde.org/xdg-desktop-portal-kde/3c382b2e7b952ad1188a8398b33f67b6b381f9ef'>Commit.</a> </li>
<li>Bump min cmake version to 3.0, move cmake_minimum_required to begin. <a href='https://commits.kde.org/xdg-desktop-portal-kde/4d6170c810d4998dceb1969ee13a920420bd226c'>Commit.</a> </li>
<li>Remove usage of dead QT_USE_FAST_CONCATENATION. <a href='https://commits.kde.org/xdg-desktop-portal-kde/da1749aa73d485a6da932e61d699f425c02ed873'>Commit.</a> </li>
<li>Use override, remove not necessary virtual keyword. <a href='https://commits.kde.org/xdg-desktop-portal-kde/22dab63f79e43bb28660e84730094a4d2cdee7ca'>Commit.</a> </li>
</ul>


</main>
<?php
	require('../aether/footer.php');
