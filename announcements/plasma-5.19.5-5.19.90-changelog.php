<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.19.90 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.19.90";
?>

<style>
main {
	padding-top: 20px;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='plasma-nano' href='https://commits.kde.org/plasma-nano'>plasma-nano</a> </h3>
<ul id='ulplasma-nano' style='display: block'>
<li>Require C++17. <a href='https://commits.kde.org/plasma-nano/489e46eb10ba552874a84c45de64458302eaf059'>Commit.</a> </li>
<li>Drop empty X-KDE-PluginInfo-Depends. <a href='https://commits.kde.org/plasma-nano/9043d800cfe75b502df075148b55b645a014d917'>Commit.</a> </li>
<li>Give a margin to wallpapers. <a href='https://commits.kde.org/plasma-nano/d2cee7bab20be105c742891f54aade7102b59659'>Commit.</a> </li>
<li>Rework open animation. <a href='https://commits.kde.org/plasma-nano/c3719d661c1cd8b81519859d27f212ec490d4c4c'>Commit.</a> </li>
<li>Fix QML errors. <a href='https://commits.kde.org/plasma-nano/7538a3c112dec087d457992bffbaad33e2009cc7'>Commit.</a> </li>
<li>Use animators. <a href='https://commits.kde.org/plasma-nano/6b936dc2129802e48cea9022c56842f14e648f26'>Commit.</a> </li>
<li>Not rounded anymore. <a href='https://commits.kde.org/plasma-nano/397abcd9ac3c2c83bd1129bf164268f2a96dc839'>Commit.</a> </li>
<li>Prettier startup feedback. <a href='https://commits.kde.org/plasma-nano/6db2dec1e1a38efd17b4b1bf67928e180ee1bb08'>Commit.</a> </li>
<li>More hacks for proper display. <a href='https://commits.kde.org/plasma-nano/fd53315fd9f20288b461b5a0ae11cf6b18812982'>Commit.</a> </li>
<li>Revert "restore previous hacks". <a href='https://commits.kde.org/plasma-nano/f9e441efea46db7c6e9bd444eff640a3ddd0b85c'>Commit.</a> </li>
<li>Restore previous hacks. <a href='https://commits.kde.org/plasma-nano/1c0ed8551d136e31e067c9b10f3e006b85f4086e'>Commit.</a> </li>
<li>Don't set manually all those flags. <a href='https://commits.kde.org/plasma-nano/618e01432add3b7fefbc2816b67453929c1af63f'>Commit.</a> </li>
<li>Fix explorer layout. <a href='https://commits.kde.org/plasma-nano/4b954a4db2baee059398cd60409ee0f32116d0b9'>Commit.</a> </li>
<li>Make the 'More Wallpapers...' string translatable. <a href='https://commits.kde.org/plasma-nano/06dc6f3fea3d30efa009be2561ffeb0f3e01af77'>Commit.</a> </li>
<li>Remove timeout. <a href='https://commits.kde.org/plasma-nano/5b0c2d1cbf509e926dfd8466300687e62d0f56f6'>Commit.</a> </li>
<li>Make window background transparent again. <a href='https://commits.kde.org/plasma-nano/348a5116abb03bfcf73d5a921270e51934349957'>Commit.</a> </li>
<li>Restore property binding. <a href='https://commits.kde.org/plasma-nano/37809920fa192fbd9eeeb28f37bce93f5d814ad6'>Commit.</a> </li>
<li>StartupFeedback: Add optional color argument. <a href='https://commits.kde.org/plasma-nano/c702803fc6eec9e8218df967a1157f84a3fdd4ce'>Commit.</a> </li>
<li>StartupFeedback: calculate background color from icon. <a href='https://commits.kde.org/plasma-nano/6e82a8555980af9eaadce7d011d891d556f6307d'>Commit.</a> </li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Assign the correct speeds to upload and download. <a href='https://commits.kde.org/plasma-nm/0d3e8f2874c43272e0e9111e28dfd8583ed0a33e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/426620'>#426620</a></li>
<li>Improve wording of authentication dialog messages. <a href='https://commits.kde.org/plasma-nm/4bf85a7ef31109cf72433b454fe89518c166b394'>Commit.</a> </li>
<li>Remove explicit ECM_KDE_MODULE_DIR. <a href='https://commits.kde.org/plasma-nm/1c9a0edc722676c469c70001677d9468fc0ae10e'>Commit.</a> </li>
<li>Require C++17. <a href='https://commits.kde.org/plasma-nm/c0eb86d005786f03ee46c1488e84d15127503fef'>Commit.</a> </li>
<li>Add a legend at the bottom of the TrafficMonitor. <a href='https://commits.kde.org/plasma-nm/20bde5a04a9988d6e939c82086c9aa8fa2643a27'>Commit.</a> </li>
<li>Only calculate the upload and download rates once. <a href='https://commits.kde.org/plasma-nm/e51d9c17d575866f91bc521be4fe0e403a9628a3'>Commit.</a> </li>
<li>Port TrafficMonitor from KQuickControlsAddons.Plotter to KQuickCharts. <a href='https://commits.kde.org/plasma-nm/5963322d48241810041b83f979a69127aa5a0dc6'>Commit.</a> </li>
<li>Drop empty X-KDE-PluginInfo-Depends. <a href='https://commits.kde.org/plasma-nm/a5538e83d944989175800b12020999b66532dae7'>Commit.</a> </li>
<li>[kded] Ignore connection deactivation shortly after wakeup. <a href='https://commits.kde.org/plasma-nm/6c9a4227aa896c7c28d2aae4aaa0eaf5fc5223b1'>Commit.</a> </li>
<li>[applet] Fix busy indicator sizing. <a href='https://commits.kde.org/plasma-nm/ca3e94d43826e75252883fc483cd15638447b7bc'>Commit.</a> </li>
<li>[applet] Display airplane mode checkbox's tooltip correctly. <a href='https://commits.kde.org/plasma-nm/bd801543b89f5f141b96f59f7146e5aa16771f3e'>Commit.</a> </li>
<li>[applet] Use correct search icon. <a href='https://commits.kde.org/plasma-nm/bb7e95e2620eec645ede9c13a1cba6f88acc896f'>Commit.</a> </li>
<li>[applet] Use standard icon sizes. <a href='https://commits.kde.org/plasma-nm/12860ab6c99334d7d6c609b6cf08493307219bfe'>Commit.</a> </li>
<li>[applet] Port to checkbox with internally-defined icon property. <a href='https://commits.kde.org/plasma-nm/116f3a5bb934ca05b7a99e91321c683a469b1bea'>Commit.</a> </li>
<li>Use title capitalization for notification names. <a href='https://commits.kde.org/plasma-nm/13e4d26c0c555535ee64ba66e0bc1392bcfc49ab'>Commit.</a> </li>
<li>Fix #424214. <a href='https://commits.kde.org/plasma-nm/3b8b131dadda0b6a1494112171ff4c6c404ef90e'>Commit.</a> </li>
<li>Don't list in widget explorer in plasma mobile. <a href='https://commits.kde.org/plasma-nm/42fea0bf70724f1df10cc6e10dc3f60f845a0fcf'>Commit.</a> </li>
<li>Revert "[applet] Port to PlasmaExtras.SwitchButton". <a href='https://commits.kde.org/plasma-nm/45d3614a41ed6bb40e0d57f4dc692b4eb785273c'>Commit.</a> </li>
<li>[applet] Port to PlasmaExtras.SwitchButton. <a href='https://commits.kde.org/plasma-nm/6d6ffa93b512da0c57478e0a32d274883c1a084b'>Commit.</a> </li>
<li>[applet] Port mostly to PlasmaComponents3. <a href='https://commits.kde.org/plasma-nm/fabfea4c8324b95f1b3d6cbe7970532391138f42'>Commit.</a> </li>
<li>[Applet] Port to PlasmaExtras.PlaceholderMessage and simplify. <a href='https://commits.kde.org/plasma-nm/aaa685e2b656aca4d0b816032e3b449663d63fa4'>Commit.</a> </li>
<li>Remove (seemingly debug) warning statement from passworddialog. <a href='https://commits.kde.org/plasma-nm/700815f79beff6efde8dc38db3f35f38afe0cf5e'>Commit.</a> </li>
<li>[BUGFIX] Fix crash in applet. <a href='https://commits.kde.org/plasma-nm/0fbf1e59472e12470107490ca97c6ef0eace539a'>Commit.</a> </li>
<li>Show device name in Connection Details. <a href='https://commits.kde.org/plasma-nm/a0455c31d9b274feea8d1e962032b89dae2c7755'>Commit.</a> </li>
<li>[WIP] [applet] Fix inline password field component not working properly. <a href='https://commits.kde.org/plasma-nm/271332cfc19ede398854113d037abfb35503179f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423093'>#423093</a></li>
<li>Openvpn: avoid enabling TCP if the remote has been set on another line. <a href='https://commits.kde.org/plasma-nm/c62ff795fe551bb7fd43658110461e0bd8c8d3c9'>Commit.</a> </li>
<li>[applet] Improve presentation and tooltips for toolbar toggles. <a href='https://commits.kde.org/plasma-nm/2030d8b68abc9ddae60238a273ad70021fb0b081'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421371'>#421371</a></li>
<li>[applet] Show KCM in System Settings. <a href='https://commits.kde.org/plasma-nm/3b8fc45f8b576b6843ecf568dcb10812ad87093a'>Commit.</a> See bug <a href='https://bugs.kde.org/417836'>#417836</a>. Phabricator Code review <a href='https://phabricator.kde.org/D29716'>D29716</a></li>
<li>Fix importing key-direction from *.ovpn files. <a href='https://commits.kde.org/plasma-nm/ce91de0268eebe3b2882c912d93d375862e19bf1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D29781'>D29781</a></li>
</ul>


<h3><a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>Default maximumPercent value of 100. So we can call osd.show() with single argument in Plasma Mobile plasmoid. <a href='https://commits.kde.org/plasma-pa/70f8e86cedbc49eccdb9479675d0b9e582fa580e'>Commit.</a> </li>
<li>Use pulseaudio's equal functions instead of memcmp. <a href='https://commits.kde.org/plasma-pa/208ae94b05765758dfd16d4ace2413a3c17f230f'>Commit.</a> </li>
<li>Show and hide the inactive device buttons based on new count property. <a href='https://commits.kde.org/plasma-pa/fbd66cdc9d89262ed32cbf0c9c6263615e3b2ff5'>Commit.</a> </li>
<li>Use i18nd consistently. <a href='https://commits.kde.org/plasma-pa/9ad933071c41036ae49cfb0dac51dd5db0e1d2d9'>Commit.</a> </li>
<li>Don't show in widget explorer on plasma mobile. <a href='https://commits.kde.org/plasma-pa/a154a3bc5447691648e51aa7ad83f8d0a9ff7740'>Commit.</a> </li>
<li>[applet] Allow showing inactive devices as in the KCM. <a href='https://commits.kde.org/plasma-pa/aa3c7235181abe4d386519f1e14d8a667766059f'>Commit.</a> </li>
<li>[applet] Port towards PlasmaComponents3. <a href='https://commits.kde.org/plasma-pa/081b11b7059a50f5830e0b48c22f5a20470dd4c6'>Commit.</a> </li>
<li>Colorize volume percentage text when over 100%. <a href='https://commits.kde.org/plasma-pa/0cb8d246360054f5bda4dc6ce9cb929c73e61286'>Commit.</a> </li>
<li>Show correct volume level in OSD when maximum volume is raised. <a href='https://commits.kde.org/plasma-pa/3f2040d4e13381713b94317b017f42b9ab31b8de'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422537'>#422537</a></li>
<li>Filter out currently unused devices. <a href='https://commits.kde.org/plasma-pa/6611ff9e89ecedbedb38c630ece042d57832f72c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422612'>#422612</a></li>
<li>Fix Layout warnings. <a href='https://commits.kde.org/plasma-pa/a93eac2c4b234750aa0e9667b6153f2dcac43bf1'>Commit.</a> </li>
<li>Take into account Balance button for layouting. <a href='https://commits.kde.org/plasma-pa/62fe68d4e56f9c30b8267f422961d4512f51b16b'>Commit.</a> </li>
<li>Fix centering Volume icon around channel sliders. <a href='https://commits.kde.org/plasma-pa/52390d18939746186a2da59b77d69003db4bd458'>Commit.</a> </li>
<li>Change it to a "Balance" button so also non-geeks can comprehend it. <a href='https://commits.kde.org/plasma-pa/2f84fd94fe127d0c66b817e74f7d211f6cc7145c'>Commit.</a> </li>
<li>Allow "unlocking" volume slider for per-channel (e.g. left, right) volume sliders. <a href='https://commits.kde.org/plasma-pa/12c07e355b8cebaea61413bd870e27904b3eada6'>Commit.</a> </li>
<li>[ListItemBase] Open options menu also on right click like a context menu. <a href='https://commits.kde.org/plasma-pa/37214faf0c9c8ea6dcef2a5d210c645afa3ba8bd'>Commit.</a> </li>
</ul>


<h3><a name='plasma-phone-components' href='https://commits.kde.org/plasma-phone-components'>plasma-phone-components</a> </h3>
<ul id='ulplasma-phone-components' style='display: block'>
<li>Lockscreen by default at startup. <a href='https://commits.kde.org/plasma-phone-components/a857dbc29955a4bb158a23313e698650542b9a05'>Commit.</a> </li>
<li>Slightly larger password font size and add clear function. <a href='https://commits.kde.org/plasma-phone-components/eda8fadb1220285fa95113a7085f759e7ce6e5d1'>Commit.</a> </li>
<li>Remove qtvirtualkeyboardplugin. <a href='https://commits.kde.org/plasma-phone-components/d797e88aeef172cbffe58d3be35effa3eec1bc9c'>Commit.</a> </li>
<li>Remove cmake bits. <a href='https://commits.kde.org/plasma-phone-components/7c4a831fe9c2ee303e90686acdd826ab013b67b5'>Commit.</a> </li>
<li>Require C++17. <a href='https://commits.kde.org/plasma-phone-components/330c3afa48fbb0e2385f511cfa0b7e7f92ebcbde'>Commit.</a> </li>
<li>Apply suggestions. <a href='https://commits.kde.org/plasma-phone-components/e6b055a915d243419a45f74bf78e2dd99fb9ca50'>Commit.</a> </li>
<li>Use Kirigami's ColorScope. <a href='https://commits.kde.org/plasma-phone-components/43603ed2aeccb7fc80683afb71de122bf73e73f9'>Commit.</a> </li>
<li>Fix minor overlap. <a href='https://commits.kde.org/plasma-phone-components/8487ccb35eb1a25f1b794c70cf74a8b4fe04e7e7'>Commit.</a> </li>
<li>Fix top panel being greyed out by SlidingPanel. <a href='https://commits.kde.org/plasma-phone-components/e74833e9abab28ce6b7aa45f638c0ff997298fce'>Commit.</a> </li>
<li>Tweak delegate padding and color choice. <a href='https://commits.kde.org/plasma-phone-components/8eecc27cbc0030245c0a10791834382a95439e93'>Commit.</a> </li>
<li>Use rounded edges for drawer. <a href='https://commits.kde.org/plasma-phone-components/0308d1fd62e0102fc078166aaba73e9f2f4451a7'>Commit.</a> </li>
<li>Offset sliding panel to where finger is. <a href='https://commits.kde.org/plasma-phone-components/392ed46c5591d413e170e2da5b36e03e21e326d6'>Commit.</a> </li>
<li>Proper height for the panel. <a href='https://commits.kde.org/plasma-phone-components/e6d041f90fd271f2207a5588854badfba081fb30'>Commit.</a> </li>
<li>Don't show notification when it shouldn't. <a href='https://commits.kde.org/plasma-phone-components/4a5ac4a6aa05fe2901a3b33e14735b1fd7f97ec7'>Commit.</a> </li>
<li>Drop empty X-KDE-PluginInfo-Depends. <a href='https://commits.kde.org/plasma-phone-components/c2d71ad6f25bdaacbb8a5a754319334a42bdf4f9'>Commit.</a> </li>
<li>Implement lockscreen mockups. <a href='https://commits.kde.org/plasma-phone-components/46022538ac7f8ed0b8efddf362d968049adea411'>Commit.</a> </li>
<li>Allow holding top panel delegate to trigger label action. <a href='https://commits.kde.org/plasma-phone-components/da3d3c022f8b903ef027ffbd1ebbeb1b6867d368'>Commit.</a> </li>
<li>Bigger hit area for the up arrow. <a href='https://commits.kde.org/plasma-phone-components/bb3225d4b563dd13e9c880da17fc99b87e3b68fb'>Commit.</a> </li>
<li>Ifix close button. <a href='https://commits.kde.org/plasma-phone-components/349de2f252691dbaa99e3feb37dd7b5c6eec2e7c'>Commit.</a> </li>
<li>Copyright. <a href='https://commits.kde.org/plasma-phone-components/f3193f5aef29c1a532ac54c3cfbfb9dffc417437'>Commit.</a> </li>
<li>Show a close anim. <a href='https://commits.kde.org/plasma-phone-components/aabcc0949f069c93b97207d6ffebcb8849170b0a'>Commit.</a> </li>
<li>Invoke plasma-settings for wifi and broadband. <a href='https://commits.kde.org/plasma-phone-components/96420f508067f84026b56db0f7ffd6cc1e0645e7'>Commit.</a> </li>
<li>New shutdown screen design. <a href='https://commits.kde.org/plasma-phone-components/b690c533d14f158d8241dd6f24033a02ec3018a3'>Commit.</a> </li>
<li>Better layout in widescreen mode. <a href='https://commits.kde.org/plasma-phone-components/b675eeb635d68589b19b0ebfc909c2a8608e9a75'>Commit.</a> </li>
<li>Fix logic for open/close top panel. <a href='https://commits.kde.org/plasma-phone-components/5be8125c629c558a6f2aae269942da81fc464a45'>Commit.</a> </li>
<li>Specify the maliit-keyboard as the input method to use. <a href='https://commits.kde.org/plasma-phone-components/0998bcfa27ee24ae4b587e3a3e55c1690cce4716'>Commit.</a> </li>
<li>No side borders. <a href='https://commits.kde.org/plasma-phone-components/54bb4a39468ecfc8ea6b2fc8be5fc6dee201b7aa'>Commit.</a> </li>
<li>Custom look for scroll indicator. <a href='https://commits.kde.org/plasma-phone-components/b71adfeef31faaae1fc4c9df8a03b8a39935ce67'>Commit.</a> </li>
<li>Up arrow becomes down. <a href='https://commits.kde.org/plasma-phone-components/93d8e2c130845829fd9e7ae39cdd2adea9ec5ae0'>Commit.</a> </li>
<li>A dark overlay over the main icons. <a href='https://commits.kde.org/plasma-phone-components/f30008b3d9b813b9fa8c3b31795fbc0f90fa2ccf'>Commit.</a> </li>
<li>Bury the thumbnail under a loader for now. <a href='https://commits.kde.org/plasma-phone-components/6cdcd8b56f8e7b90937fec274e8dd3f9732eb72d'>Commit.</a> </li>
<li>Cmake: remove wallpaper installation code. <a href='https://commits.kde.org/plasma-phone-components/cc24bebd7d068a3d4cac54d9413c9febd87d1cfa'>Commit.</a> </li>
<li>Same speed animation with kwin minimize. <a href='https://commits.kde.org/plasma-phone-components/4b39250ffa5d0fc0a59ea31c58946763fdb8eb1b'>Commit.</a> </li>
<li>Remove unused wallpaper thing. <a href='https://commits.kde.org/plasma-phone-components/853f2b113b6692dc4f2c46ec95d1c97243b63cf8'>Commit.</a> </li>
<li>Add missing file. <a href='https://commits.kde.org/plasma-phone-components/953b384cb3daa4d56ada253803cfaa3c6a7069ac'>Commit.</a> </li>
<li>Add media controls to lockscreen. <a href='https://commits.kde.org/plasma-phone-components/7f4f10a994c78db3d4f36baae3d425303bcb37c1'>Commit.</a> </li>
<li>Fixes to close anim. <a href='https://commits.kde.org/plasma-phone-components/29dda81ac69de977f854fe8b733771ebb2ea21d1'>Commit.</a> </li>
<li>Remove dummy windowtask. <a href='https://commits.kde.org/plasma-phone-components/775870852c54f68950f3daaaa71c42bd307aa63a'>Commit.</a> </li>
<li>Single mousearea on bottom. <a href='https://commits.kde.org/plasma-phone-components/ef7a78cfe1b1c55805251895514e74d6cdfcb0c8'>Commit.</a> </li>
<li>Return to bounds after flicking. <a href='https://commits.kde.org/plasma-phone-components/0c6d19e8568781a9fc88140337185d6090ff7551'>Commit.</a> </li>
<li>Fix close slide direction. <a href='https://commits.kde.org/plasma-phone-components/ae67112a5c9f9b63cc9189883840b99e8f618a05'>Commit.</a> </li>
<li>Litch -- when activating window. <a href='https://commits.kde.org/plasma-phone-components/928ac5c1950066b01e72f05282483cb3a58fe1f6'>Commit.</a> </li>
<li>Removeuseless debug. <a href='https://commits.kde.org/plasma-phone-components/24ef679755d775dd67ae9c481598ce6b7f27595a'>Commit.</a> </li>
<li>When task switch appears release minimize on launchers. <a href='https://commits.kde.org/plasma-phone-components/16b1cae73e05b0195f74d67ba60d090019127609'>Commit.</a> </li>
<li>Fix closing. <a href='https://commits.kde.org/plasma-phone-components/c81eaee35249149aaaf5f84b9909541ead21bb21'>Commit.</a> </li>
<li>Close anim if window active. <a href='https://commits.kde.org/plasma-phone-components/9f25435d828313a911c79be118e38dd1bf9e3c32'>Commit.</a> </li>
<li>New close anim. <a href='https://commits.kde.org/plasma-phone-components/8f39bd38287ee0331705755b08daa02316d2e93b'>Commit.</a> </li>
<li>Minimize to launcher when the switcher is not visible. <a href='https://commits.kde.org/plasma-phone-components/a1b7ef5d36d7b965bb6aa6176a575dfcc59b1e18'>Commit.</a> </li>
<li>Delay the first active window sync. <a href='https://commits.kde.org/plasma-phone-components/69d9c2557dd6c12e96966ed0e6acce0a2e195c2e'>Commit.</a> </li>
<li>Color panel when startupfeedback is used. <a href='https://commits.kde.org/plasma-phone-components/0c3b152f99dc29b611aac8d959d859762553b8c1'>Commit.</a> </li>
<li>UpdateActiveWindow at start. <a href='https://commits.kde.org/plasma-phone-components/a9a9e13ad6bc465177787288909e4d68a0390a9d'>Commit.</a> </li>
<li>Initial support for pipewire thumbnails. <a href='https://commits.kde.org/plasma-phone-components/d983aeebe3c073e7e1d055c827a90459eceedbdd'>Commit.</a> </li>
<li>When an application is already running bring window on front. <a href='https://commits.kde.org/plasma-phone-components/0d438d87d33d53030f19351105dff52f00022842'>Commit.</a> </li>
<li>Minimize animations. <a href='https://commits.kde.org/plasma-phone-components/5881d6be925cf1c260cc8a6fa5685d99490a05da'>Commit.</a> </li>
<li>New delegate for search applet. <a href='https://commits.kde.org/plasma-phone-components/93e16a9ca1f21b4df3cf641cd8b2a7499c54177f'>Commit.</a> </li>
<li>Add margin before search bar. <a href='https://commits.kde.org/plasma-phone-components/0eed8d3a90e5196419560bfc2b5e40e1fc818b1e'>Commit.</a> </li>
<li>Taller search field by default. <a href='https://commits.kde.org/plasma-phone-components/752e8fde6c96d40a44a3beac1c956cb559ce24fc'>Commit.</a> </li>
<li>Things go under the top panel. <a href='https://commits.kde.org/plasma-phone-components/12c34a7a6abf42851b60f058419046f86db318e6'>Commit.</a> </li>
<li>Croll up homescreen when there are no tasks. <a href='https://commits.kde.org/plasma-phone-components/ece092bf988c6a416c5181f2fb4a0930c2934f8b'>Commit.</a> </li>
<li>Cleanup not found favorites. <a href='https://commits.kde.org/plasma-phone-components/daebf5ae603d5080ad38f7a9e1315b63cdaa4592'>Commit.</a> </li>
<li>Disable own clock for now. <a href='https://commits.kde.org/plasma-phone-components/13500ee9e26e60ff3bc6f5bcc390753dbe860def'>Commit.</a> </li>
<li>New look for the search applet. <a href='https://commits.kde.org/plasma-phone-components/e18f7b4560d54e39ed4eccd76780f5772c60bb02'>Commit.</a> </li>
<li>Fixes to task switcher experience. <a href='https://commits.kde.org/plasma-phone-components/dd315d81e04b3aafc0d4cae73c3ded7280997007'>Commit.</a> </li>
<li>Make use of HomeScreenControls. <a href='https://commits.kde.org/plasma-phone-components/3f32a18b6299ff50f5a143f93d8e495cd0179a36'>Commit.</a> </li>
<li>Proper module name. <a href='https://commits.kde.org/plasma-phone-components/7e6f6b063e581e15f370a7924dd946e635a75824'>Commit.</a> </li>
<li>Add mobile shell private components. <a href='https://commits.kde.org/plasma-phone-components/6d0265c3e232331fdc1ae24f40f92b37591aba70'>Commit.</a> </li>
<li>Flick after manual drag. <a href='https://commits.kde.org/plasma-phone-components/c8d9ef81d8103cda6a74b286e2d919e4ce78c6a5'>Commit.</a> </li>
<li>Minimize instead of showdesktop. <a href='https://commits.kde.org/plasma-phone-components/76050a60bda6b283b43323ebb911014d1c1fe42b'>Commit.</a> </li>
<li>At least 4 favorites. <a href='https://commits.kde.org/plasma-phone-components/a5d1f4d8807f13d421082e7aae773822fe800436'>Commit.</a> </li>
<li>Disable the scrollindicator. <a href='https://commits.kde.org/plasma-phone-components/d6bd455b1ed079e3cf3b06dc11ce4012bbe58ba6'>Commit.</a> </li>
<li>Drag vertically also on the launcher strip. <a href='https://commits.kde.org/plasma-phone-components/dae850dbc999e41d2d4b64ed332853d5ec7fe985'>Commit.</a> </li>
<li>Fix bottom gesture. <a href='https://commits.kde.org/plasma-phone-components/ead97b23997506eb39b8522a0a4e8d6c539c1690'>Commit.</a> </li>
<li>Rework tasks switcher. <a href='https://commits.kde.org/plasma-phone-components/d4e443104a0f1c01dedd44de40a854d31384f9ee'>Commit.</a> </li>
<li>Use proper task label. <a href='https://commits.kde.org/plasma-phone-components/bdac73fdab10e49898873d343344025df09deaa4'>Commit.</a> </li>
<li>Don't show the window on hide. <a href='https://commits.kde.org/plasma-phone-components/d5e0c7acc8c035b6e429c9c6f5e1abe5689f7468'>Commit.</a> </li>
<li>Ensure taskswitcher is fullscreen. <a href='https://commits.kde.org/plasma-phone-components/69ae5a1ba4a81c05c769ba4c55d3a8447dc751ce'>Commit.</a> </li>
<li>Better spacing. <a href='https://commits.kde.org/plasma-phone-components/f8df7b3b0002f35b55c6521ff6adae67c1c24f9c'>Commit.</a> </li>
<li>Rework panel coloring logic based on tasks. <a href='https://commits.kde.org/plasma-phone-components/b27e67037ccda617d2f20a2fe12381c8a3b10527'>Commit.</a> </li>
<li>Enforce a big size. <a href='https://commits.kde.org/plasma-phone-components/f7843014d78c3939990ce0b4c97fadfa30bf74b4'>Commit.</a> </li>
<li>Reset position when launched. <a href='https://commits.kde.org/plasma-phone-components/cbdb6b6f054a56944a95692fd94d72c14881a0bb'>Commit.</a> </li>
<li>Fix widget explorer. <a href='https://commits.kde.org/plasma-phone-components/e4014bd0ba3d827f7e0dc1220698286cf482b3b3'>Commit.</a> </li>
<li>Temporarly remove the horizontal activity switcher. <a href='https://commits.kde.org/plasma-phone-components/96f4e7be533a7992160c266d9a1b4f687daf4d1f'>Commit.</a> </li>
<li>Adjust dot colours to be white, remove arrow label. <a href='https://commits.kde.org/plasma-phone-components/dfe40102d8cf178fb84aca6a5292508da84aa916'>Commit.</a> </li>
<li>Add swipe up label, and make main bottom elements also swipe up. <a href='https://commits.kde.org/plasma-phone-components/3eccd42e1388767508c811a583257d638b4155c7'>Commit.</a> </li>
<li>Prevent overflow password characters from stretching keypad width. <a href='https://commits.kde.org/plasma-phone-components/ff6f583289cbdd3902cab6a846269193b313835d'>Commit.</a> </li>
<li>Remove blur, hide clock when keypad is shown, add enter pin labels. <a href='https://commits.kde.org/plasma-phone-components/92c079fbb6396bc1b545949658b63189d29beb0d'>Commit.</a> </li>
<li>Clear password when the keypad is not shown. <a href='https://commits.kde.org/plasma-phone-components/b86bfa645e7abce55a6590ddf3472aee048540f2'>Commit.</a> </li>
<li>Add scrolling lockscreen, and a darken effect when keypad is shown. <a href='https://commits.kde.org/plasma-phone-components/30847f710ef5e4b1954cbbbc3c9896b34b14f727'>Commit.</a> </li>
<li>Fix the LauncherGrid. <a href='https://commits.kde.org/plasma-phone-components/e0dd5108617bd3951d78d658d607b68589e6c655'>Commit.</a> </li>
<li>Remove traces of color average properly. <a href='https://commits.kde.org/plasma-phone-components/d73b2455dfe5fa89a2470298b89626889c05d64d'>Commit.</a> </li>
<li>Leave calculating the startup feedback color to plasma-nano. <a href='https://commits.kde.org/plasma-phone-components/48f8f00034be30553f0790893526b208b547e04a'>Commit.</a> </li>
<li>Correct showinApp typo. <a href='https://commits.kde.org/plasma-phone-components/d76fc145f924e06353e38defb088b4e9b32e0e52'>Commit.</a> </li>
<li>Make panels opaque when StartupFeedback is open. <a href='https://commits.kde.org/plasma-phone-components/a89d76b23ce6f3bb4e60d3570f54f61c35f96ff7'>Commit.</a> </li>
<li>Another approach to use the enum everywhere in ApplicationListModel. <a href='https://commits.kde.org/plasma-phone-components/5f0e3ec05d6b6a85786e5087d3b58be781a41080'>Commit.</a> </li>
<li>Revert "Open and close StartupFeedback using KStartupInfo". <a href='https://commits.kde.org/plasma-phone-components/3f4915d426e10254e1bac750f9a5c111fc64ddcb'>Commit.</a> </li>
<li>Revert "Comment out debug logging". <a href='https://commits.kde.org/plasma-phone-components/881f7c7a96d7e2204cf176b9dbede61b4fed73c2'>Commit.</a> </li>
<li>Comment out debug logging. <a href='https://commits.kde.org/plasma-phone-components/c2306cb5563617789825be2466655ad0ce6f7fce'>Commit.</a> </li>
<li>Remove unuused FeedbackWindow components. <a href='https://commits.kde.org/plasma-phone-components/efc04968c0a2b433b3c727b8fa2131ff22113901'>Commit.</a> </li>
<li>Open and close StartupFeedback using KStartupInfo. <a href='https://commits.kde.org/plasma-phone-components/b856b78790e4754642562613abcf55144c23edcb'>Commit.</a> </li>
<li>Adapt to plasma nano API change. <a href='https://commits.kde.org/plasma-phone-components/f3ab3c0a567d06073ecea90ea62e390cb66a0742'>Commit.</a> </li>
</ul>


<h3><a name='plasma-sdk' href='https://commits.kde.org/plasma-sdk'>Plasma SDK</a> </h3>
<ul id='ulplasma-sdk' style='display: block'>
<li>Cleanup includes. <a href='https://commits.kde.org/plasma-sdk/969444dc099a71ed92c91981e1e4fe34d83c199e'>Commit.</a> </li>
<li>[engineexplorer] Port away from deprecated KTitleWidget::setPixmap and KIconThemes. <a href='https://commits.kde.org/plasma-sdk/75d4a868072d3505d0facda516c7efc7935bfbca'>Commit.</a> </li>
<li>Convert copyright statements to SPDX expressions. <a href='https://commits.kde.org/plasma-sdk/dd63e0ee2fd5b8391e310c8d7cd2b83c0080e158'>Commit.</a> </li>
<li>Drop empty X-KDE-PluginInfo-Depends. <a href='https://commits.kde.org/plasma-sdk/e606219ca26654bdfc5bc08f1166df5092fb0751'>Commit.</a> </li>
<li>Use Kirigami icon sizes instead of hardcoded numbers. <a href='https://commits.kde.org/plasma-sdk/3a7103552cffa90005ca705c4a8b7e0b4d344082'>Commit.</a> </li>
<li>Center the Comparison sheet and move slider into the header. <a href='https://commits.kde.org/plasma-sdk/fbb726976389644dda99507db6a53ec96676fb77'>Commit.</a> </li>
</ul>


<h3><a name='plasma-tests' href='https://commits.kde.org/plasma-tests'>plasma-tests</a> </h3>
<ul id='ulplasma-tests' style='display: block'>
<li>Remove explicit ECM_KDE_MODULE_DIR. <a href='https://commits.kde.org/plasma-tests/41d3fab935766a2bc333ad04eeaf9e1dd12d8bae'>Commit.</a> </li>
</ul>


<h3><a name='plasma-vault' href='https://commits.kde.org/plasma-vault'>plasma-vault</a> </h3>
<ul id='ulplasma-vault' style='display: block'>
<li>Remove explicit ECM_KDE_MODULE_DIR. <a href='https://commits.kde.org/plasma-vault/4d8e10d5d518bb784cd7c447f10ebed98cd48792'>Commit.</a> </li>
<li>Drop empty X-KDE-PluginInfo-Depends. <a href='https://commits.kde.org/plasma-vault/06f8a970bab6406a9b5ad8c62de47a1adbea6598'>Commit.</a> </li>
<li>[applet] Port towards PlasmaComponents3. <a href='https://commits.kde.org/plasma-vault/499025890a3d71f7ca3b6486d8a33646baf276fa'>Commit.</a> </li>
<li>Reset password field when the user clicks Ok. <a href='https://commits.kde.org/plasma-vault/c4932f99056ff348e354e95fcbdd1af28f25c5ef'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/424063'>#424063</a></li>
<li>Increase frameworks dependency to 5.72 for PlasmaExtras.PlaceholderMessage. <a href='https://commits.kde.org/plasma-vault/a31cf36417d27dbd0e32868a0996dc057012820a'>Commit.</a> </li>
<li>Port applet to use PlasmaExtras.PlaceholderMessage. <a href='https://commits.kde.org/plasma-vault/f8f7179824f5540ee64900d20af39dfc9bacd490'>Commit.</a> </li>
<li>[applet] Improve presentation and consistency for vault items. <a href='https://commits.kde.org/plasma-vault/dce7d9f4e11aa3ea0dec0944ff2c294c46b4d2a4'>Commit.</a> </li>
<li>Move comment about unempty mount point to its code. <a href='https://commits.kde.org/plasma-vault/72808959502b0d5a3ef580a0bc7cc72c8df9013c'>Commit.</a> </li>
<li>[fileitemaction] Clean up includes. <a href='https://commits.kde.org/plasma-vault/2f42276cb9d2d319f4f48f66d5048cf4e991aa23'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22702'>D22702</a></li>
<li>Use the new KSysGuard namespace for KSysGuard related targets. <a href='https://commits.kde.org/plasma-vault/08da0edb79901100c0b8660bb90ea4bb7d1548e0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D29630'>D29630</a></li>
<li>Added logo for GitLab. <a href='https://commits.kde.org/plasma-vault/b7472568ab00ef4d10b79b4dba260918843ad83c'>Commit.</a> </li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Drop outdated check for GL errors. <a href='https://commits.kde.org/plasma-workspace/ef8f85f6673ccd8345e16af33956d52c5d6fed93'>Commit.</a> </li>
<li>Update Breeze global theme preview images to match the dark ones. <a href='https://commits.kde.org/plasma-workspace/0b44a16ef483464a8d6968b9b8b1cd919b8363d5'>Commit.</a> </li>
<li>[applets/batterymonitor] Add display of screen brightness as a percentage. <a href='https://commits.kde.org/plasma-workspace/f249506c43c5293bd07bd70e69d1cdd758343d2e'>Commit.</a> </li>
<li>Update lockscreen preview from Flow to Shell. <a href='https://commits.kde.org/plasma-workspace/0858355c108a4176000d47363ca8632db0c321e1'>Commit.</a> </li>
<li>Don't crash ksmserver if restoreSession is called twice. <a href='https://commits.kde.org/plasma-workspace/6f51b766c9dd8563d4fa736e6d229f1ac2c2ec36'>Commit.</a> </li>
<li>KDisplayManager: Fix crash when using KDM. <a href='https://commits.kde.org/plasma-workspace/dbc1d4726c64ceb581e924032955a1247c2ef7d8'>Commit.</a> </li>
<li>Disabe ksycoca rebuilding on runner threads. <a href='https://commits.kde.org/plasma-workspace/5a648c56ebbbb9b8ba87fedf8423b5830edbd367'>Commit.</a> See bug <a href='https://bugs.kde.org/423818'>#423818</a></li>
<li>Preview. <a href='https://commits.kde.org/plasma-workspace/459f5162fbf17649e86863e3040e63ebcacbe2e4'>Commit.</a> </li>
<li>SDDM shell preview. <a href='https://commits.kde.org/plasma-workspace/9eb71ab8c0692ee1cd5946058b13abb37d952809'>Commit.</a> </li>
<li>[sddm-theme/lockscreen] Fix login button size. <a href='https://commits.kde.org/plasma-workspace/9a8fc811282e303ec2f93c978228a85961dae263'>Commit.</a> </li>
<li>KXftConfig: port from KGlobal::dirs to QStandardPaths. <a href='https://commits.kde.org/plasma-workspace/b454aa3c314c1c51cc8278c5b4ec42fccc50d5b8'>Commit.</a> </li>
<li>KXftConfig: modernize code. <a href='https://commits.kde.org/plasma-workspace/d45e5b238716412932f11d1b8826c14b2167c601'>Commit.</a> </li>
<li>[shell] Add QPAInfo source. <a href='https://commits.kde.org/plasma-workspace/e3639a7c38d6c44e9ab2b123171467d3c6cb9f6d'>Commit.</a> </li>
<li>[shell] Split UserFeedback out of ShellCorona. <a href='https://commits.kde.org/plasma-workspace/04035b2551618f6723b2a65dd0c8916e681f9141'>Commit.</a> </li>
<li>Adjust relevance of kcms in service runner. <a href='https://commits.kde.org/plasma-workspace/a3f22cded7921c0aa7bef5e312222895cafbadbe'>Commit.</a> </li>
<li>Add a find_package call for the freetype. <a href='https://commits.kde.org/plasma-workspace/3e2e0dd2370a10aa06fad4ba06daf1d0ff1e0b4e'>Commit.</a> </li>
<li>Clear KRunner text when setting was just disabled. <a href='https://commits.kde.org/plasma-workspace/60489c9d8bf7b84753dd7c816c6f95c72f83cdc9'>Commit.</a> </li>
<li>Fix clazy warning and deprecation. <a href='https://commits.kde.org/plasma-workspace/d11082523eda7df6b7dbd7205d5a177bf525d1fc'>Commit.</a> </li>
<li>Remove commented code. <a href='https://commits.kde.org/plasma-workspace/cee40b3e90e1909de9b811d55345cd161390de87'>Commit.</a> </li>
<li>Activate KRunner instead of running the binary. <a href='https://commits.kde.org/plasma-workspace/0e575a20ef36532b5b40a40ea30f915976942477'>Commit.</a> See bug <a href='https://bugs.kde.org/416145'>#416145</a></li>
<li>Aggregate text for KRunner in DesktopView. <a href='https://commits.kde.org/plasma-workspace/003e30a64e976e66c02d3aa8b06301710a231889'>Commit.</a> See bug <a href='https://bugs.kde.org/416145'>#416145</a></li>
<li>[Battery Monitor] Indicate when a charge limit has been configured. <a href='https://commits.kde.org/plasma-workspace/8801aae8aafbda0206bda7c7b47bf929cd7882a3'>Commit.</a> </li>
<li>Krunner: Fix performance issue raised by PC3. <a href='https://commits.kde.org/plasma-workspace/6827254e311429f9c4bb4ace45953ff6bdff579c'>Commit.</a> </li>
<li>Krunner: Define context variable before parsing the script. <a href='https://commits.kde.org/plasma-workspace/5eb251da80329644287f2ef9353f43ec9f600ac8'>Commit.</a> </li>
<li>Fix build. <a href='https://commits.kde.org/plasma-workspace/603af9f10dc2d8a4bdfb465215c491248a4e6836'>Commit.</a> </li>
<li>Use Klipper help for the Klipper config dialog. <a href='https://commits.kde.org/plasma-workspace/365eeb440d16e62d5ed3146b018d7ea4af16a526'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421589'>#421589</a></li>
<li>Make "Activities..." context menu item follow HIG. <a href='https://commits.kde.org/plasma-workspace/0e50ed76952b7a79a6c017c21f7e863addae440b'>Commit.</a> </li>
<li>Reuse text from query field instead of custom property. <a href='https://commits.kde.org/plasma-workspace/bd0f7892111098db5155398fd58456bde3d3de12'>Commit.</a> </li>
<li>Tweak description for appmenu, as it does not have to be in the top panel. <a href='https://commits.kde.org/plasma-workspace/2f111a7ddee8e7eb268061483b544f7478d21ba9'>Commit.</a> </li>
<li>Port to new KWorkspace API. <a href='https://commits.kde.org/plasma-workspace/05414ed58d43d87d907326636faac53ae2e7bd60'>Commit.</a> </li>
<li>Move "Configure Desktop" menu item to the top of the menu. <a href='https://commits.kde.org/plasma-workspace/f288e319355edae843e63bcd0efccd40f36fe5a7'>Commit.</a> </li>
<li>[ksmserver] Use UpdateLaunchEnvJob to sync SESSION_MANAGER env. <a href='https://commits.kde.org/plasma-workspace/d24cef54d68fb29994ed610bc70867d6fc811287'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425982'>#425982</a></li>
<li>Drop empty X-KDE-PluginInfo-Depends. <a href='https://commits.kde.org/plasma-workspace/57cfab21ac4e6a182d35a5e74b61a88a1ac99136'>Commit.</a> </li>
<li>Fix undetected KPackage installation. <a href='https://commits.kde.org/plasma-workspace/ce920603ea93ae4abf391d477001bb67cd429ed1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423055'>#423055</a>. Fixes bug <a href='https://bugs.kde.org/424429'>#424429</a></li>
<li>Port away from deprecated Qt::MidButton. <a href='https://commits.kde.org/plasma-workspace/ddb82b7a0ced50c74e28677213f37acdd0a63ed4'>Commit.</a> </li>
<li>Clean code to build desktop menu. <a href='https://commits.kde.org/plasma-workspace/ee6a885ee316524df03ec3461146b43f6511d9f7'>Commit.</a> </li>
<li>Allow comma when dot is decimal separator. <a href='https://commits.kde.org/plasma-workspace/a8644836253ee00f265ae11390dfd4afcef179ce'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406388'>#406388</a></li>
<li>Add option to disable KRunner history. <a href='https://commits.kde.org/plasma-workspace/4a44b30bdfd715e390d4bb5693922de0849811c7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376666'>#376666</a></li>
<li>Add option to retain prior search. <a href='https://commits.kde.org/plasma-workspace/ddd005b1f5bd802193d9393f68cf2bd1e01dee6d'>Commit.</a> Implements feature <a href='https://bugs.kde.org/397092'>#397092</a></li>
<li>Fix KNS deprecations. <a href='https://commits.kde.org/plasma-workspace/ad79bb5dad3bab35f8794758146723bd21127880'>Commit.</a> </li>
<li>Enable RemoveDeadEntries option for KNS. <a href='https://commits.kde.org/plasma-workspace/76d056dc87ee876eb2a67821bff591f2b3c26cea'>Commit.</a> See bug <a href='https://bugs.kde.org/416255'>#416255</a></li>
<li>Fix syntax. <a href='https://commits.kde.org/plasma-workspace/8189794d4e1f7f03fd13e716925b7f18650c1808'>Commit.</a> </li>
<li>Include libdrm headers. <a href='https://commits.kde.org/plasma-workspace/d9295e5ec6bc5c5c2f59e4bef25ebf7e01ef9648'>Commit.</a> </li>
<li>Remove stay include and only check for drm if pipewire is around. <a href='https://commits.kde.org/plasma-workspace/9e6b9bb2a47106d0dfa172030b549fb09fbda367'>Commit.</a> </li>
<li>Search for libdrm and libgbm in cmake, these packages are required to compile. <a href='https://commits.kde.org/plasma-workspace/13ba39cda89fab9bd02d6deb38551a46e035857d'>Commit.</a> </li>
<li>Pipewire: Remove unnecessary gbm.h include. <a href='https://commits.kde.org/plasma-workspace/55dbd9dcda324b1f219e87f6e6da52eb342ae4fa'>Commit.</a> </li>
<li>Fully validate value before syncing envs. <a href='https://commits.kde.org/plasma-workspace/875bcf1918381873f11eaa286ee3e8780c069676'>Commit.</a> </li>
<li>[applets/digital-clock] Add visible "configure" button to the header. <a href='https://commits.kde.org/plasma-workspace/d10cf35d48200e0ddaef26fb3935714301b05159'>Commit.</a> </li>
<li>Require C++17. <a href='https://commits.kde.org/plasma-workspace/007461dec54bff8b044ecfc110b91c2abe879ae7'>Commit.</a> </li>
<li>Doc: move the documentation for recently moved kcms here. <a href='https://commits.kde.org/plasma-workspace/3fd791392d75427eb32a225c1461b85cc8489852'>Commit.</a> </li>
<li>Kcms: move some of the KCMS to plasma-workspace. <a href='https://commits.kde.org/plasma-workspace/7d5806ff1c2d952e71200ef5beb8e197ad6dc493'>Commit.</a> </li>
<li>Make the combobox items translatable (patch by Victor Ryzhykh). <a href='https://commits.kde.org/plasma-workspace/175effc678aa805165486fee4eff5ac3b9ca1d4a'>Commit.</a> </li>
<li>Port away from KMimeTypeTrader. <a href='https://commits.kde.org/plasma-workspace/c998bb4017eea7bfcbf11a07a94cac45cd8fb5cb'>Commit.</a> </li>
<li>[lockscreen KCM] Simplify labels for options about what to show. <a href='https://commits.kde.org/plasma-workspace/6b1c5c1c45eb0b88058fefd913350f31053d24a8'>Commit.</a> </li>
<li>Fix all errors and warnings on login, lock, and logout screens. <a href='https://commits.kde.org/plasma-workspace/bcaf3886d92da5b5cc6f2ab6a6b43dbc096d7c60'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425349'>#425349</a></li>
<li>[klipper] Remove unused X-KDE-autostart-after=panel. <a href='https://commits.kde.org/plasma-workspace/70a71393e54aa6e650347c0427e517096fd51e33'>Commit.</a> </li>
<li>Interactive console: Fix loading of templates and port from deprecated api. <a href='https://commits.kde.org/plasma-workspace/d8948f54dea731fad9a1057f2418718a7a85fc55'>Commit.</a> </li>
<li>Port usages of deprecated KPluginInfo to KPluginMetadata. <a href='https://commits.kde.org/plasma-workspace/4fc03b71d7b78a01a6d1cd7637d524d4e0283453'>Commit.</a> </li>
<li>[Notifications] Slightly increase maximum occupiable screen space. <a href='https://commits.kde.org/plasma-workspace/4eb0f4f67031973863ae42dc37eaf2ba33da53e1'>Commit.</a> </li>
<li>Replace old ModelTest copy by QAbstractItemModelTester. <a href='https://commits.kde.org/plasma-workspace/a5f9345e756ced9f52f8b29e7cf9baf2b3e954b2'>Commit.</a> </li>
<li>Systemmonitor applets are desktop only for now. <a href='https://commits.kde.org/plasma-workspace/f2dc2bc50615f3e68b1752b3d1c537c5082a22e2'>Commit.</a> </li>
<li>Fix possible crash in bookmarksrunner. <a href='https://commits.kde.org/plasma-workspace/880c71dca39eeac2251875141cb648d049f5d3ad'>Commit.</a> </li>
<li>[applets/clipboard] Clear search text when clearing all entries. <a href='https://commits.kde.org/plasma-workspace/f7e459e3a3aa1d687524aeab86d003c49bded12c'>Commit.</a> </li>
<li>[applets/clipboard] Don't disable toolbar if search field has text. <a href='https://commits.kde.org/plasma-workspace/2b0ad193bb64c308c787e28cd70eccc8a7a548bd'>Commit.</a> </li>
<li>[applets/devicenotifier] Remove UI to suppress popup  for new device connection. <a href='https://commits.kde.org/plasma-workspace/ff544ed528ea316abf61b897eed4692b0cf0e61f'>Commit.</a> </li>
<li>[applets/devicenotifier] Move all settings into main UI itself and rename. <a href='https://commits.kde.org/plasma-workspace/b878a64cdb55e8c218fb4eaea656448cebc5a38c'>Commit.</a> </li>
<li>Libtaskmanager: fix build when Qt is built with GLESv2. <a href='https://commits.kde.org/plasma-workspace/01f68ba51abd0bbf81db6dd16d279306246cfca2'>Commit.</a> </li>
<li>Rename Web Shortcuts runner to Web Search keywords. <a href='https://commits.kde.org/plasma-workspace/3ab4c3dfd00caecf966e913d6653b34d040e1bb9'>Commit.</a> </li>
<li>KCM Feedback port to ManagedConfigModule and SettingStateBinding. <a href='https://commits.kde.org/plasma-workspace/a7b49bec6ce22d56e8ef8fe083020048fb262aa7'>Commit.</a> </li>
<li>[KCM] Use KCModuleData in order to allow settings loading without UI. <a href='https://commits.kde.org/plasma-workspace/0be055332b8323c71c5927b7b152cc5d2613285f'>Commit.</a> </li>
<li>[applets/notifications] Remove mnemonic remover. <a href='https://commits.kde.org/plasma-workspace/22c636abf1703061c5e1ec9602065fdb85019fa9'>Commit.</a> </li>
<li>Shell: allow plasmashell process to take screenshots. <a href='https://commits.kde.org/plasma-workspace/577ffd6da3653061e2044a56842388eb989dd792'>Commit.</a> </li>
<li>Fix typo in commit message. <a href='https://commits.kde.org/plasma-workspace/613e808d7058cf10273897aa1c66a7354342800f'>Commit.</a> </li>
<li>Do not close Kicker when pinning entry. <a href='https://commits.kde.org/plasma-workspace/1d4cb4857d5a401d293ea54cb35b3a4534da5f1c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390585'>#390585</a></li>
<li>[logout screen] Set implicit widths on OK and Cancel buttons. <a href='https://commits.kde.org/plasma-workspace/57a0f9334bce95281355589c7d8a18be1f5c3d12'>Commit.</a> </li>
<li>Remove unneeded include. <a href='https://commits.kde.org/plasma-workspace/5a47a83ef01a1682f576b1660be85658d01857b9'>Commit.</a> </li>
<li>Update sensors used for the network usage plasmoid preset. <a href='https://commits.kde.org/plasma-workspace/8528b8d692b4dd963055eb90d41908f306590ea1'>Commit.</a> </li>
<li>Improve disk write operations for firefox favicons. <a href='https://commits.kde.org/plasma-workspace/0d7eca0b4b69279c17120fb9807c951fcdb25baf'>Commit.</a> </li>
<li>Go back to constructing two temporary sets. <a href='https://commits.kde.org/plasma-workspace/ab4aec9d2aace85b27ac558d5b58d47000408f5b'>Commit.</a> </li>
<li>[klipper] Workaround race when klipper updates on wayland. <a href='https://commits.kde.org/plasma-workspace/feb67853c6137179c78debbcf54f0312d4866760'>Commit.</a> </li>
<li>[libkworkspace] Guard for environment variables that won't be accepted. <a href='https://commits.kde.org/plasma-workspace/e79000e5276d7c5ab81c16444d43c57d87ab0740'>Commit.</a> </li>
<li>[startkde] Replace calling dbus-update-activation-environment with native code. <a href='https://commits.kde.org/plasma-workspace/1e444c864fc175b3826c88a51a5e2c9f95e497c3'>Commit.</a> </li>
<li>[libkworkspace] Support batch operations in UpdateLaunchEnvJob. <a href='https://commits.kde.org/plasma-workspace/f03cae180bcaf57081995212d7e0e677fe2bfcce'>Commit.</a> </li>
<li>[startkde/waitforname] Fix ecm_mark_nongui_executable call. <a href='https://commits.kde.org/plasma-workspace/7079115defbfb3b8c1c6c1d672c6eae5d7e36bcb'>Commit.</a> </li>
<li>[libkworkspace] Update linked libraries for libkworkspace. <a href='https://commits.kde.org/plasma-workspace/60961d1f46b954f896f3fda6af2f711ff48e8047'>Commit.</a> </li>
<li>Changed notification width to a smaller size. <a href='https://commits.kde.org/plasma-workspace/613771e22cb8a4b12210f4df1360190563de387e'>Commit.</a> </li>
<li>[applets/batterymonitor] Use `plasmoid.action("[name]")` to access text and actions. <a href='https://commits.kde.org/plasma-workspace/fec21bb51941c0dbb41334f939d54b5ab09dcc2f'>Commit.</a> </li>
<li>[applets/batterymonitor] Show button in popup to open Energy KCM. <a href='https://commits.kde.org/plasma-workspace/03e280d1a7952ced266a16dd6d3c4ae8ad581342'>Commit.</a> </li>
<li>[applets/systemtray] Restore old spacing for very small icons on thin panels. <a href='https://commits.kde.org/plasma-workspace/933a343c1cb454c8836b92c07bb0184a0f90c865'>Commit.</a> </li>
<li>Allow ENV variables in shell runner. <a href='https://commits.kde.org/plasma-workspace/dc70225a7c1a373c0db1fe1054ee7048acd95115'>Commit.</a> Implements feature <a href='https://bugs.kde.org/409107'>#409107</a></li>
<li>KCMs: port Desktoptheme and icons to use SettingStateBinding. <a href='https://commits.kde.org/plasma-workspace/88ff6b48415f9004944369bb3406145393069c90'>Commit.</a> </li>
<li>Fix icon, "plasma" doesn't exist, it's "plasmashell". <a href='https://commits.kde.org/plasma-workspace/d3d9fa3ab2bd79c59b71326a2af4fba53310197c'>Commit.</a> </li>
<li>[KRunner] Set maximum width for text field. <a href='https://commits.kde.org/plasma-workspace/9fbc1a520f0cf97396bd9d877f7a28d94780945e'>Commit.</a> </li>
<li>[applets/notifications] Port DnD checkbox to set the icon itself. <a href='https://commits.kde.org/plasma-workspace/f34e7c10fee6844d7e13d2806c7461b2b2c29c22'>Commit.</a> </li>
<li>[applets/devicenotifier] Remove unused PlasmaComponents2 import. <a href='https://commits.kde.org/plasma-workspace/cc2f2ba88a1dca067be1e6d703cdc72406d6d802'>Commit.</a> </li>
<li>WIP: Port from PC2 ListItem to PE ListItem. <a href='https://commits.kde.org/plasma-workspace/4f1a63de3a6ad1e6099581b959cbc9dd0bb1d70c'>Commit.</a> </li>
<li>[applets/systemtray] Return to pre-5.20 default panel icon arrangement. <a href='https://commits.kde.org/plasma-workspace/394b0ca0f7fe57e3beae3fdbe1016ca2b225fbe7'>Commit.</a> </li>
<li>Raise the minimum CMake version after 157a82d7. <a href='https://commits.kde.org/plasma-workspace/b8debcbc52ff83c6d78aa7a3ba6ac50d6e18cbec'>Commit.</a> </li>
<li>Show the date in digital clock by default. <a href='https://commits.kde.org/plasma-workspace/8b0164b03ec659aa4ef3748bf807b3f91b129203'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423155'>#423155</a></li>
<li>Fitler out annoying QML warnings. <a href='https://commits.kde.org/plasma-workspace/5aebd1a8c122a30f950582832ab3d6cf1bc978f7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/424259'>#424259</a></li>
<li>Also find PkgConfig. <a href='https://commits.kde.org/plasma-workspace/949aaf7b07aea3d865779577b73c30a23296e06b'>Commit.</a> </li>
<li>Request that KNS cleans up its cache if there are dead entries. <a href='https://commits.kde.org/plasma-workspace/84401e094669bf9322659f4423a8d8972c9de52b'>Commit.</a> See bug <a href='https://bugs.kde.org/417985'>#417985</a></li>
<li>Port away from deprecated QSet related methods. <a href='https://commits.kde.org/plasma-workspace/3737a8cb4950912ff17d2f5b69bed3ed3e55538e'>Commit.</a> </li>
<li>[applets/systemtray] Return to pre-5.20 icon sizing. <a href='https://commits.kde.org/plasma-workspace/31c7ebe319229e6834397c57c48e34a333bf4b88'>Commit.</a> </li>
<li>Detect glEGLImageTargetTexture2DOES at runtime. <a href='https://commits.kde.org/plasma-workspace/0885e0176404718a032fd40a6553b8c099c733ce'>Commit.</a> </li>
<li>Fix build with gpsd 3.21. <a href='https://commits.kde.org/plasma-workspace/e3134289f522edb140797818fffd60d641b86cd8'>Commit.</a> </li>
<li>Provide the components for screencasting. <a href='https://commits.kde.org/plasma-workspace/18b0072f5981ff252112e4e8146779edd46b85e3'>Commit.</a> </li>
<li>Port the waylandtasksmodel to use the uuid instead of the internalId. <a href='https://commits.kde.org/plasma-workspace/157a82d7859f685a30988b5a4c32b9a4b8f06629'>Commit.</a> </li>
<li>Port away from obsolete QtAlgorithm functions. <a href='https://commits.kde.org/plasma-workspace/f717a2fb0940954f2102f0782fb5423ebc4db619'>Commit.</a> </li>
<li>Wayland: make logout greeter a Role::Panel PlasmaSurface. <a href='https://commits.kde.org/plasma-workspace/d6b53fca1fb43fe7299508d612b8a87cfb9546af'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423953'>#423953</a></li>
<li>[applets/appmenu] Handle appmenu location changing. <a href='https://commits.kde.org/plasma-workspace/6ae7d885ef2b73a7b354ff1463b9ca136725bcb1'>Commit.</a> </li>
<li>[applets/systemtray] Replace obsolete serviceOwnerChanged. <a href='https://commits.kde.org/plasma-workspace/d7922985b5044f0980b78e19b05beced1e9fd8c8'>Commit.</a> </li>
<li>Port from deprecated QProcess signals. <a href='https://commits.kde.org/plasma-workspace/cfa9ee2990213d55b0505688ec3cd517efb1f241'>Commit.</a> </li>
<li>[klipper] Prevent crash if clipboard is empty and preventEmptyClipboard is off. <a href='https://commits.kde.org/plasma-workspace/6db931f0b392ad1a600f050b1ad3591b9aeb6628'>Commit.</a> </li>
<li>[startkde] Move more logic into startplasma-wayland from waylandsession. <a href='https://commits.kde.org/plasma-workspace/483b30d7afd798c1865ef557e38d74c50b65b5f6'>Commit.</a> </li>
<li>Allow x and multiplication symbol instead of *. <a href='https://commits.kde.org/plasma-workspace/668feb3e17809f3da0f1a2b6a36b95d970d7e694'>Commit.</a> Implements feature <a href='https://bugs.kde.org/350066'>#350066</a></li>
<li>[applets/devicenotifier] Port placeholder message to PlaceholderMessage. <a href='https://commits.kde.org/plasma-workspace/5a147825fe1181bce4beef0114b3474f1576f941'>Commit.</a> </li>
<li>[applets/appmenu]: Allow the full menu to display on vertical panels. <a href='https://commits.kde.org/plasma-workspace/15d61847638e3f04e199fa9927f1c5d571197de0'>Commit.</a> </li>
<li>[startkde] Drop setting up of ghostscript. <a href='https://commits.kde.org/plasma-workspace/c19869c0e8eccb043e32fc384bc3586a19aa2bcd'>Commit.</a> </li>
<li>[applets/systemtray] Remove debug leftovers. <a href='https://commits.kde.org/plasma-workspace/45700b9958df61f4dfadcf9021e11ab991ee31e4'>Commit.</a> </li>
<li>Also call requestHideAutoHidingPanel for panels set to "windows can cover", so that the screen edge gets recreated for it. <a href='https://commits.kde.org/plasma-workspace/5b010aa1fa954855ad67788f0a3a35f6805e9c21'>Commit.</a> </li>
<li>[applets/systemtray] Fix icon hitboxes being too big in 2 row/column view. <a href='https://commits.kde.org/plasma-workspace/77357af0084b745fc47cc24de6321408290bd8f7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/424812'>#424812</a></li>
<li>[applets/systemtray] Small refactor to separate tweakable properties from derived properties. <a href='https://commits.kde.org/plasma-workspace/b6c0cddf7fd69e16cd09d655312a6567379abe97'>Commit.</a> </li>
<li>[applets/systemtray] Very slightly increase margins for small icon size. <a href='https://commits.kde.org/plasma-workspace/6dfac6d9af4b9f85d70f5e9826b701fc196c5d65'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/424711'>#424711</a></li>
<li>[applets/systemtray] tiny refactor of compact applet small icon sizing. <a href='https://commits.kde.org/plasma-workspace/63cd451939774ed0e5dcbe1c833b473cf9a5ed18'>Commit.</a> </li>
<li>Also bind background and disabledText colors. <a href='https://commits.kde.org/plasma-workspace/adff60f77c4b0baef9428bd5931bc38bd615b57b'>Commit.</a> </li>
<li>[applets/notifications] Fix PC3 porting regression with job tooltip text. <a href='https://commits.kde.org/plasma-workspace/a8940d44037c3104c067049e98e56cca994d239e'>Commit.</a> </li>
<li>Remove unused variable. <a href='https://commits.kde.org/plasma-workspace/2828bfe494268ff5e09aaee71834bb8251f4ed82'>Commit.</a> </li>
<li>Wallpapers: simplify the refreshes of the BackgroundsModel. <a href='https://commits.kde.org/plasma-workspace/048495a5f61f40408c3102f9a871364dbb5a331b'>Commit.</a> </li>
<li>Wallpapers: Rely on BackgroundsModel.count to know when the wallpapers changed. <a href='https://commits.kde.org/plasma-workspace/bdeaada42f4583144392fccc7ed43b0306e725f2'>Commit.</a> </li>
<li>Fix PlasmaComponents3 porting regression. <a href='https://commits.kde.org/plasma-workspace/8f2c8771383bb9957c9e7f66ef89f4503e5518aa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/424625'>#424625</a></li>
<li>Revert "Report missing or incorrect translations here". <a href='https://commits.kde.org/plasma-workspace/2c4069779c017564574081a865cc25c9c071f2a9'>Commit.</a> </li>
<li>Report missing or incorrect translations here. <a href='https://commits.kde.org/plasma-workspace/866df3e319de0723e85bc832afa470f1fffd852c'>Commit.</a> </li>
<li>[kcms/translation] Add Comment to package. <a href='https://commits.kde.org/plasma-workspace/2e71f752a0aa072e4d67267d3e606bafe5a9641f'>Commit.</a> </li>
<li>[klipper] Port to use wayland clipboard. <a href='https://commits.kde.org/plasma-workspace/b1ee38d2eead767b191d701ad5deeb0f485afeef'>Commit.</a> </li>
<li>Doc: import the desktop theme and icons kcm docbooks. <a href='https://commits.kde.org/plasma-workspace/2fbbff5dafcb030dddc530758ef7488e38ef6385'>Commit.</a> </li>
<li>Begin porting to PlasmaComponents3. <a href='https://commits.kde.org/plasma-workspace/f55b00febdd4a9ed2910b820a9f37a0083969d40'>Commit.</a> </li>
<li>Kcms: move desktoptheme and icons kcm to plasma-workspace. <a href='https://commits.kde.org/plasma-workspace/7f52a95bd48d9bd69e5917f47f3addab66967ce7'>Commit.</a> </li>
<li>Set formfactors for applets that shouldn't be on phone. <a href='https://commits.kde.org/plasma-workspace/0b73287cb8bb2f0a91641a3623b4c3c09b002435'>Commit.</a> </li>
<li>Remove dead code related to widget explorer extraActions. <a href='https://commits.kde.org/plasma-workspace/159f1a1ae99c7c195362ec1cb428e87980cbdbd2'>Commit.</a> </li>
<li>Commit https://phabricator.kde.org/D28686. <a href='https://commits.kde.org/plasma-workspace/38ebcf09b1631175d5a1b4d857622c5313146a60'>Commit.</a> </li>
<li>Fix broken change by d400faa670dd6d6acde951a91c308cd926c56639. <a href='https://commits.kde.org/plasma-workspace/4f24721f836fd95c1d80db0634dae4aa25402b90'>Commit.</a> </li>
<li>Port from deprecated QModelIndex::child to QAbstractItemModel::index. <a href='https://commits.kde.org/plasma-workspace/d400faa670dd6d6acde951a91c308cd926c56639'>Commit.</a> </li>
<li>[wallpaper] Avoid using pluginId for indexing package indexes. <a href='https://commits.kde.org/plasma-workspace/74e9f990ae6c461ec9848b68e537732eeb458eca'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423987'>#423987</a></li>
<li>Emit countChanged when we get a new source model. <a href='https://commits.kde.org/plasma-workspace/be421fa88ff26b7c04ab1a47f4a23b757e1057f9'>Commit.</a> </li>
<li>[applets/systemtray] Improve size options. <a href='https://commits.kde.org/plasma-workspace/4db4eab543cd049e629368029c7dc14d54b33bf8'>Commit.</a> </li>
<li>Revert "Fix broken ENV variables for detailed settings". <a href='https://commits.kde.org/plasma-workspace/1cfd9e882df3af50219b730493947e8ee02b5ed4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423995'>#423995</a></li>
<li>Add bookmarksrunner integration for Falkon. <a href='https://commits.kde.org/plasma-workspace/bbed524a2cec1508522ee5a903cab504b9c0d5a6'>Commit.</a> See bug <a href='https://bugs.kde.org/393544'>#393544</a></li>
<li>[applets/batterymonitor] Handle "plugged in but still discharging" state. <a href='https://commits.kde.org/plasma-workspace/19c41453739c10c4344ad8e907d8e687d869b668'>Commit.</a> </li>
<li>[applets/systemtray] Mark a read-only property with 'readonly'. <a href='https://commits.kde.org/plasma-workspace/73616e78aae6468e30da1f7398d29e041a642948'>Commit.</a> </li>
<li>[applets/systemtray] Increase height of popup a bit. <a href='https://commits.kde.org/plasma-workspace/32e43ecf03a07b8c58b182eb5947e7c1fbdc2f25'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417374'>#417374</a></li>
<li>[1/2] fix No input in nested session. <a href='https://commits.kde.org/plasma-workspace/eef9ee125e35ad8a96b39a2a3d0795a3250a83ae'>Commit.</a> </li>
<li>[wallpapers/image] Improve comments. <a href='https://commits.kde.org/plasma-workspace/0e3b502c69dde2ab5100f1e74bd08eed696824ba'>Commit.</a> </li>
<li>[applets/devicenotifier] Port to ExpandableListItem. <a href='https://commits.kde.org/plasma-workspace/b16eee88578d3dda29c44e24860ea10b62c282b0'>Commit.</a> </li>
<li>Revert "[wallpaper] Drop unused method". <a href='https://commits.kde.org/plasma-workspace/ede14440c7ead9e4fbe41b9b596b44feaf0d4e18'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/424161'>#424161</a></li>
<li>Display only recent documents in Kickers "Recent Documents". <a href='https://commits.kde.org/plasma-workspace/72c6daa6364dccb894fe55af84acf6db7e709ebf'>Commit.</a> See bug <a href='https://bugs.kde.org/420951'>#420951</a></li>
<li>Rename "Recent Documents" to "Recent Files" everywhere. <a href='https://commits.kde.org/plasma-workspace/d7c4b94bd5082d8f5703fcae0d3810b4e65028a6'>Commit.</a> See bug <a href='https://bugs.kde.org/424007'>#424007</a></li>
<li>[applet/systemtray] Regression: all applets in config are shown as disabled. <a href='https://commits.kde.org/plasma-workspace/d68c16b278d670e07af0dd251e8e0093b5ce563c'>Commit.</a> </li>
<li>Use horizontalbars face for disk usage. <a href='https://commits.kde.org/plasma-workspace/d752a3ed7442b7ab903df4cbfef37dcdd9bab574'>Commit.</a> </li>
<li>Disable interacting with the user list if it doesn't make sense. <a href='https://commits.kde.org/plasma-workspace/e7f8c1ef9994296819c0479d1f8fba90b7543bed'>Commit.</a> </li>
<li>[System Tray] Auto-scale icons and expose setting for number of rows/columns. <a href='https://commits.kde.org/plasma-workspace/55ae651a12f6242efdf2164338bbca2021ae06d9'>Commit.</a> </li>
<li>Hide face config button if the face can't config. <a href='https://commits.kde.org/plasma-workspace/793f609dea9b799009f5da6d821185ca461f784f'>Commit.</a> </li>
<li>Only open KCM in systemsettings if it can be displayed. <a href='https://commits.kde.org/plasma-workspace/2ebf8ab391466146a7a6ae3d4dad3e0ff0c7276d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423612'>#423612</a></li>
<li>[libkworkspace] Watch ksmserverrc for changes. <a href='https://commits.kde.org/plasma-workspace/fce0b68172636e27aaba9fa5143b78641a7f3b98'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423864'>#423864</a></li>
<li>[OSD] Colorize percentage text when over 100%. <a href='https://commits.kde.org/plasma-workspace/03528a97e7df7c9bb464307670c366c6e5b329fe'>Commit.</a> </li>
<li>[KRunner] prevent workspace geometry change on wayland. <a href='https://commits.kde.org/plasma-workspace/ade3bc4aa1efb8c5db9a7c9f3356b4e79df6cc43'>Commit.</a> </li>
<li>Notify of screen geometry changes only after DesktopView has updated. <a href='https://commits.kde.org/plasma-workspace/7852de5e697c17565fda8d0b24c062ea00605a37'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373075'>#373075</a></li>
<li>[applets/systemtray] Remove unneeded SNI roles. <a href='https://commits.kde.org/plasma-workspace/a9cc8508d16a6d92b5199eefcebc260291b78fcd'>Commit.</a> </li>
<li>Optimize Messages.sh a bit. <a href='https://commits.kde.org/plasma-workspace/8f3f6627706fe7f77eec53a87e3dada6c949da86'>Commit.</a> </li>
<li>WidgetExplorer: Only count applets in visible Containments. <a href='https://commits.kde.org/plasma-workspace/98390b63e30b154c68770dc2b7af325ef81ef2ca'>Commit.</a> </li>
<li>Fix my typos in docs. <a href='https://commits.kde.org/plasma-workspace/44607de8ef1039ccc92cfa7439556e07afc28d42'>Commit.</a> </li>
<li>Update KCM screenlocker docbook. <a href='https://commits.kde.org/plasma-workspace/bc094b4b4412ac11c7a8ac4f913d6bcddac53c40'>Commit.</a> </li>
<li>[wallpaper] Drop unused method. <a href='https://commits.kde.org/plasma-workspace/221a3a52223d90bdbe514d715732cfcd6fdb7abd'>Commit.</a> </li>
<li>[wallpaper] Save the package name in configuration not a URL. <a href='https://commits.kde.org/plasma-workspace/530b36abb0a0a04958417cf914906fb64ab64062'>Commit.</a> </li>
<li>Allow specifying a maximum percent value for volume OSDs. <a href='https://commits.kde.org/plasma-workspace/a67ab7e4cc54f3813988c10a5737f884eb6a2025'>Commit.</a> </li>
<li>Theme shouldn't inherit. <a href='https://commits.kde.org/plasma-workspace/7474d3970fe85cfdf8c877496fcbcc26315068fd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423747'>#423747</a></li>
<li>Use FormLayout with twinFormLayouts for alignment of lockscreen config. <a href='https://commits.kde.org/plasma-workspace/a3836058ce2263dd549afc063845fa688b60d5ed'>Commit.</a> </li>
<li>Fix KRunner to show on top of above windows. <a href='https://commits.kde.org/plasma-workspace/0ec7a3e2cbd39ef5a117f062bf56a70eb534cf0c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389964'>#389964</a></li>
<li>Only spawn dbus-run-session if there isn't a session already. <a href='https://commits.kde.org/plasma-workspace/8475fe4545998c806704a45a7d912f777a11533f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404335'>#404335</a></li>
<li>[applets/systemtray] Fix context menu misplaced. <a href='https://commits.kde.org/plasma-workspace/a409b06bbfb0ea10959d9f680ea1ef16e7b7827a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421275'>#421275</a></li>
<li>[applets/systemtray] minor code cleanup. <a href='https://commits.kde.org/plasma-workspace/d413985a3ecf5077903e9228ac6d3ae5381a153d'>Commit.</a> </li>
<li>[applets/systemtray] Fix context menu misplaced. <a href='https://commits.kde.org/plasma-workspace/c3254912ea916bf3cc6b215e196f41a61d764e82'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421275'>#421275</a></li>
<li>Update preferred browser when KSycoca changes. <a href='https://commits.kde.org/plasma-workspace/1db6885605a90c2528febb3276225f068ad26667'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/416161'>#416161</a></li>
<li>[applets/systemtray] Take into account kiosk mode and use standard text. <a href='https://commits.kde.org/plasma-workspace/58542f4960f2fd9e9c744a05fa401707ecca3988'>Commit.</a> </li>
<li>Quote url for private window action in webshortcuts runner. <a href='https://commits.kde.org/plasma-workspace/981468354242a8a4a5a99f77e7d96226068edf24'>Commit.</a> </li>
<li>[applets/mediaplayer] Make size of the standalone plasmoid similar to that of the system tray. <a href='https://commits.kde.org/plasma-workspace/6e34cd553fface1cd71c926d29465bee2d5dac1e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422736'>#422736</a></li>
<li>[applets/systemtray] Add a button in the hidden view to show the config window. <a href='https://commits.kde.org/plasma-workspace/80c737cb4889d0c343befe71265c540f2e1018e3'>Commit.</a> </li>
<li>Fix indentation for previous commit. <a href='https://commits.kde.org/plasma-workspace/d6a1b7cca76e82ea001fe515ee9557bec3fe5909'>Commit.</a> </li>
<li>[Breeze task switcher] Move key handling to the correct place. <a href='https://commits.kde.org/plasma-workspace/c5629ec497cf03f60104ae9459dfda0cdb2d613e'>Commit.</a> </li>
<li>[Logout] Port to QQC2 and PC3. <a href='https://commits.kde.org/plasma-workspace/21bdc2a89d123932af2a1b04bd8e98ff868bf3eb'>Commit.</a> </li>
<li>[libdbusmenuqt] Fix memory leak. <a href='https://commits.kde.org/plasma-workspace/6a312085a713a2a048e5f40b3e500024f88c9b40'>Commit.</a> </li>
<li>[libnotificationmanager] Integrate KJob::Unit::Items. <a href='https://commits.kde.org/plasma-workspace/dcaa4cfcb18fcdf3f0334df91a46e8c8db8c4ddf'>Commit.</a> See bug <a href='https://bugs.kde.org/422098'>#422098</a></li>
<li>[notifications] Fix notifications placement when aligned in center. <a href='https://commits.kde.org/plasma-workspace/ebfc2b53aed57c7910a17b5658cfe087dd739450'>Commit.</a> </li>
<li>Runners: port to new KRunner metadata system & install location. <a href='https://commits.kde.org/plasma-workspace/3c41e2c12f0f90cbb5901554eb34c9d0adc0353e'>Commit.</a> </li>
<li>Increase frameworks dependency to 5.72 for PlasmaExtras.PlaceholderMessage. <a href='https://commits.kde.org/plasma-workspace/c647c2dee446d7c32c2bd1094010ab1e1e2154b3'>Commit.</a> </li>
<li>Port applets to use PlasmaExtras.PlaceholderMessage. <a href='https://commits.kde.org/plasma-workspace/faef126255222f727219bf69ac2b89857dc61ab2'>Commit.</a> </li>
<li>[applets/digital clock] Make pop-up layout more compact and consistent. <a href='https://commits.kde.org/plasma-workspace/a45e27b43b791f693c740a414a4e51ac2b480fc5'>Commit.</a> </li>
<li>[applets/appmenu] Listen to more signals to ensure that appmenu applet can reliably catch the active window changing. <a href='https://commits.kde.org/plasma-workspace/16613309be67fde120c9b6f1af0c761f794edec4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422786'>#422786</a>. Fixes bug <a href='https://bugs.kde.org/422260'>#422260</a></li>
<li>[KRunner] Do not use QDBusInterface. <a href='https://commits.kde.org/plasma-workspace/6fcc54a5e4784587fc5cdc9cabc0fd67f2731c3d'>Commit.</a> </li>
<li>Reload private browser on database change. <a href='https://commits.kde.org/plasma-workspace/694efc31f775de1433e2b84eaae35e1cbf77a9c6'>Commit.</a> </li>
<li>Add action to launch webshortcut in private/incognito window. <a href='https://commits.kde.org/plasma-workspace/6443792eb946033542ad5ea26cbb2212be162c81'>Commit.</a> Implements feature <a href='https://bugs.kde.org/390009'>#390009</a></li>
<li>[applets/systemtray] Remove dead code. <a href='https://commits.kde.org/plasma-workspace/bf81b755f15b8ca14bdeef25c72e7a2a6a82cfa7'>Commit.</a> </li>
<li>[applets/systemtray] Add side margins for grid item text. <a href='https://commits.kde.org/plasma-workspace/f4d46cd8587d3be2cadaeaa85bb7c40e7fd194ce'>Commit.</a> </li>
<li>Fix minor typos. <a href='https://commits.kde.org/plasma-workspace/363004278bbc8ca118760396d5d991b59c26c5fa'>Commit.</a> </li>
<li>Systemtray items respect Fitt's Law when in panel. <a href='https://commits.kde.org/plasma-workspace/fd78da890b7c80c56ddb640ced943f183ca55141'>Commit.</a> </li>
<li>Cleanup runners. <a href='https://commits.kde.org/plasma-workspace/72cebd84c91a05c7cbcd619610b38fd900778226'>Commit.</a> </li>
<li>[wallpapers/image] Fix undefined config values QML warnings. <a href='https://commits.kde.org/plasma-workspace/b707970879f091d4ba356bcf909a7f4c8c3b4422'>Commit.</a> </li>
<li>Properly connect to lambda function. <a href='https://commits.kde.org/plasma-workspace/5388c24e684a9bf8542ac35b88c771f272ea79e0'>Commit.</a> </li>
<li>Ksplashqml: Fix QCoreApplication warning. <a href='https://commits.kde.org/plasma-workspace/8589376e7eab7c04ac0d9e582da534e135dc41ec'>Commit.</a> </li>
<li>[applets/digital-clock] Fix Connections deprecation warning. <a href='https://commits.kde.org/plasma-workspace/c3f635a140582dcc1e07f67caa0b61e99328312a'>Commit.</a> </li>
<li>[Notifications] Toggle do not disturb on icon middle click. <a href='https://commits.kde.org/plasma-workspace/eb736685a2b32e10c0b95ef70c00128c9e1c693e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422811'>#422811</a></li>
<li>Fix compile failure. <a href='https://commits.kde.org/plasma-workspace/9a4716e97120d84d706bc0275bf3aa6f90e0a0ee'>Commit.</a> </li>
<li>[applets/systemtray] Use a grid for the hidden items view. <a href='https://commits.kde.org/plasma-workspace/1669197b33f388b9a4d7f8fae66d725c62d026ee'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402681'>#402681</a></li>
<li>Support accessing applet geometry. <a href='https://commits.kde.org/plasma-workspace/790333991ee773326bb7643d0a03828f6d992b1e'>Commit.</a> </li>
<li>[Lock screen] Make clearPassword() do what it says and then use it. <a href='https://commits.kde.org/plasma-workspace/b3030730d816631e6fd5a45f1c597ab07c073b52'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/412252'>#412252</a></li>
<li>Fix use of deprecated QML feature in sddm-theme. <a href='https://commits.kde.org/plasma-workspace/31eb253ba1a7153eff1fbc8183af9ef5691801ee'>Commit.</a> </li>
<li>Add a new standalone executable to replace X-KDE-Autostart-Condition. <a href='https://commits.kde.org/plasma-workspace/6cd25ded9934b1e37e4c727d78dda28623fe0a37'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28424'>D28424</a></li>
<li>[applets/batterymonitor] Port to PlasmaComponents3. <a href='https://commits.kde.org/plasma-workspace/5973bda7605acd9eebf68261eaeaa022e962113d'>Commit.</a> </li>
<li>Suspend matching if plugin is not auhorized. <a href='https://commits.kde.org/plasma-workspace/20c5edd93a52a2fa88701b731f2170479fc8047e'>Commit.</a> </li>
<li>Cleanup lifecycle methods. <a href='https://commits.kde.org/plasma-workspace/abcffd85a4bbc50faad5e0afd940c9edd91df791'>Commit.</a> </li>
<li>Don't insert unprintable characters in KRunner. <a href='https://commits.kde.org/plasma-workspace/0ab573ced32652be58c4524abeac2e2bf7cce17f'>Commit.</a> </li>
<li>Set KRunner to be on top of above windows. <a href='https://commits.kde.org/plasma-workspace/24c90cab3b54b6c49792f8b372be439e8c6a068d'>Commit.</a> </li>
<li>Don't hide clock after idle time on login screen. <a href='https://commits.kde.org/plasma-workspace/53162d2ab72a0df505a453f876e43be3bb2194cd'>Commit.</a> </li>
<li>Runner: make recentdocument use KActivityStats data. <a href='https://commits.kde.org/plasma-workspace/c1b97eab1f6b7f7ac056425b92c47b6228b9cdc5'>Commit.</a> </li>
<li>Update availableScreenRect when a panel moves to another screen. <a href='https://commits.kde.org/plasma-workspace/13eaf08a6263c6926ac74d7241b313d5a64def4f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D29831'>D29831</a></li>
<li>Show percentage in OSD when a progress bar is visible. <a href='https://commits.kde.org/plasma-workspace/a4b6a9da58a021133f68f2f9a58900811fe61a26'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385602'>#385602</a></li>
<li>[applets/systemtray] Configurable maximum icon size. <a href='https://commits.kde.org/plasma-workspace/161309cdf5904bca3db5a64da3706bb808efe67c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360333'>#360333</a>. Fixes bug <a href='https://bugs.kde.org/355587'>#355587</a></li>
<li>Fix typo. <a href='https://commits.kde.org/plasma-workspace/3a39bd9475958564a66cd70f68595d9525911115'>Commit.</a> </li>
<li>[Notifications] Position popups more relative to notification icon. <a href='https://commits.kde.org/plasma-workspace/a3b679d4e1db1836735e16527d911a845a9f1f12'>Commit.</a> </li>
<li>[Clipboard Plasmoid] Port to Prison QML import. <a href='https://commits.kde.org/plasma-workspace/7bd9bea0723ca52161b77fa2f01fd818e01bb258'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D29478'>D29478</a></li>
<li>Remove obsolete kwin_interface things. <a href='https://commits.kde.org/plasma-workspace/281f44c79dd17e6580a11f939d0dade893a6fa25'>Commit.</a> </li>
<li>Fix finding _IceTransNoListen. <a href='https://commits.kde.org/plasma-workspace/0c4ad7f1aaf4433941f89552785b4f35e7f3a4c5'>Commit.</a> </li>
<li>Remove redundant #include. <a href='https://commits.kde.org/plasma-workspace/ac70c8966178dec75b7772f103475df42d18d002'>Commit.</a> </li>
<li>[kio_desktop] Implement SlaveBase::GetFileSystemFreeSpace. <a href='https://commits.kde.org/plasma-workspace/065ba968018f5afe0d42507d7fcf16f1d62977e1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417851'>#417851</a></li>
<li>BookmarksRunner: Change caching databases. <a href='https://commits.kde.org/plasma-workspace/61b02b16f4205d2d9b212a239fa65fe7710acc30'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D29726'>D29726</a></li>
<li>KRunner location: remove redundant setText. <a href='https://commits.kde.org/plasma-workspace/be84a82ab613d91249cc1caabe4cae69c90d6c9e'>Commit.</a> </li>
<li>[OSD] Add a bit more padding on either side of the layout. <a href='https://commits.kde.org/plasma-workspace/b47fb26099435fcb02a4f772785ce30d2656cc81'>Commit.</a> </li>
<li>[applets/batterymonitor] Improve UI for power management inhibition. <a href='https://commits.kde.org/plasma-workspace/544f917efae3b7a580adba66aee5b53fd5d69ab1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401108'>#401108</a></li>
<li>Update ExpandedRepresentation.qml. <a href='https://commits.kde.org/plasma-workspace/3437a1e065ac52ba516ff938fd0c7e03736c8d21'>Commit.</a> </li>
<li>Remove useless double line. <a href='https://commits.kde.org/plasma-workspace/b3dc997250f0b065e2b19d61f5b631fc75f48bb8'>Commit.</a> </li>
<li>Avoid system tray resizing. <a href='https://commits.kde.org/plasma-workspace/6e73cf357d6ca187c2662c802646b26b6eb9eb32'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D29824'>D29824</a></li>
<li>[Notifications] For group parents combine the child bodies. <a href='https://commits.kde.org/plasma-workspace/ebf61e5ecf9ef68ab8c816a8306da2ddf338e40c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27130'>D27130</a></li>
<li>Make system tray impossible to resize again. <a href='https://commits.kde.org/plasma-workspace/4b286d7ce6e0a7d06cc2d1ec2ecb07311b314eff'>Commit.</a> </li>
<li>[OSD] Fix ProgressBar potentially growing the dialog. <a href='https://commits.kde.org/plasma-workspace/c067d4985a49cdef8ed0d96ba8019a7b945b1741'>Commit.</a> </li>
<li>[applets/appmenu] Hide old menu before showing new menu. <a href='https://commits.kde.org/plasma-workspace/caea7f7618780ef98bc0ee88e100a32753cd5944'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D29464'>D29464</a></li>
<li>[lookandfeel] Port OSD's slider to PlasmaComponents3. <a href='https://commits.kde.org/plasma-workspace/b9aba6eb31df111a538c5e7381ec37c55294986a'>Commit.</a> </li>
<li>[applets/appmenu] Improve menu feel. <a href='https://commits.kde.org/plasma-workspace/bf337f552a029ef99c10bd53ba2a172530f3ec18'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D29465'>D29465</a></li>
<li>Use more compact OSD. <a href='https://commits.kde.org/plasma-workspace/af31ad46c025c0e53de1427527876366fcd869a6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344393'>#344393</a>. Fixes bug <a href='https://bugs.kde.org/372665'>#372665</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20569'>D20569</a></li>
<li>Use font: instead of font.pointSize: where possible. <a href='https://commits.kde.org/plasma-workspace/93594f606b61e568d401767273c0ae1a06005011'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D29437'>D29437</a></li>
<li>Check if the clipboard is empty when initializing the status of the Clipboard applet. <a href='https://commits.kde.org/plasma-workspace/d228a44a51e83da4cec778b629b14be02e288cfe'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D29818'>D29818</a></li>
<li>[applets] Show KCMs in System Settings or Info Center, as appropriate. <a href='https://commits.kde.org/plasma-workspace/e8c727449617e83a37aad47f772a1b9c91b31035'>Commit.</a> See bug <a href='https://bugs.kde.org/417836'>#417836</a>. Phabricator Code review <a href='https://phabricator.kde.org/D29712'>D29712</a></li>
<li>[IconApplet] Port KRun to ApplicationLauncherJob. <a href='https://commits.kde.org/plasma-workspace/e3b251fd5b06b47259d072928163a878071cd096'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D29687'>D29687</a></li>
<li>[Free Space Notifer] Use critical notification instead of tray icon and monitor Root, too. <a href='https://commits.kde.org/plasma-workspace/97e843d3ead33fc8f376fe08040fa1f923b831e6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/340582'>#340582</a>. Phabricator Code review <a href='https://phabricator.kde.org/D29770'>D29770</a></li>
<li>Automatically close Clipboard applet after clearing history or deleting the last element. <a href='https://commits.kde.org/plasma-workspace/7a4ea26a6561115058bd1cf9846e5f463aafb135'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421345'>#421345</a>. Phabricator Code review <a href='https://phabricator.kde.org/D29652'>D29652</a></li>
</ul>


<h3><a name='plasma-workspace-wallpapers' href='https://commits.kde.org/plasma-workspace-wallpapers'>Plasma Workspace Wallpapers</a> </h3>
<ul id='ulplasma-workspace-wallpapers' style='display: block'>
<li>Remove explicit ECM_KDE_MODULE_DIR. <a href='https://commits.kde.org/plasma-workspace-wallpapers/dd151ca4f7bd0bb7d1e65c1007665943e5353774'>Commit.</a> </li>
<li>Add "Flow" Wallpaper. <a href='https://commits.kde.org/plasma-workspace-wallpapers/bb48d15f3976149ee28bbba7a677911598cb7859'>Commit.</a> </li>
</ul>


<h3><a name='plymouth-kcm' href='https://commits.kde.org/plymouth-kcm'>Plymouth KControl Module</a> </h3>
<ul id='ulplymouth-kcm' style='display: block'>
<li>Remove explicit ECM_KDE_MODULE_DIR. <a href='https://commits.kde.org/plymouth-kcm/a74b8a7e6cc5d96c9993d1919013010b70ef1416'>Commit.</a> </li>
<li>Fix clazy warnings about calling first() on temporary. <a href='https://commits.kde.org/plymouth-kcm/7594b041ebcb245f2e26710b0fa26069c9b30990'>Commit.</a> </li>
<li>Use project version instead of undefined PLYMOUTH_KCM_VERSION. <a href='https://commits.kde.org/plymouth-kcm/8d1088c0b0a84a34862ae2de3f6e18328e06324f'>Commit.</a> </li>
<li>Cmake: fix search for pkg-config. <a href='https://commits.kde.org/plymouth-kcm/aca25cd6895caa00f8a9186b3be5a4beb1898a29'>Commit.</a> </li>
<li>Cmake: install plymouth.knsrc in newer ECM 5.58.0+ location. <a href='https://commits.kde.org/plymouth-kcm/e4c0cf3ff250370086c5bd577081827f34e4918f'>Commit.</a> </li>
<li>Fix running of update-alternatives. <a href='https://commits.kde.org/plymouth-kcm/1f91395d0280436dc4ecb3309577e210f42e0d6b'>Commit.</a> </li>
</ul>


<h3><a name='polkit-kde-agent-1' href='https://commits.kde.org/polkit-kde-agent-1'>polkit-kde-agent-1</a> </h3>
<ul id='ulpolkit-kde-agent-1' style='display: block'>
<li>Use pretty global notification name and title capitalization. <a href='https://commits.kde.org/polkit-kde-agent-1/052ed2123785db8dc2dc18d30e52b195e6e4b1dc'>Commit.</a> </li>
</ul>


<h3><a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>Remove explicit ECM_KDE_MODULE_DIR. <a href='https://commits.kde.org/powerdevil/42c0067579a7c887939120ff325126d5b4e59599'>Commit.</a> </li>
<li>Add setting for charge threshold. <a href='https://commits.kde.org/powerdevil/f6a1362ede4b23f0a5cb53c66d4f1885d1d84fdc'>Commit.</a> </li>
<li>Remove shortcut migration code. <a href='https://commits.kde.org/powerdevil/6881eb8ebc4e2a3d2782b34dccd2b7e4e25be09d'>Commit.</a> </li>
<li>[core] Return early if no wakeup was cleared. <a href='https://commits.kde.org/powerdevil/d7f3929215e72085974a259e53d2aa0d49f446ca'>Commit.</a> </li>
<li>[core] send the dbus error if wakeup is invalid. <a href='https://commits.kde.org/powerdevil/c14ff817d7dfdb31693c3c612a0844b661801acf'>Commit.</a> </li>
<li>Cmake: set the CAP_WAKE_ALARM capability on the powerdevil. <a href='https://commits.kde.org/powerdevil/54a85ddf51448ac1e94aafe2786e664f2b688b45'>Commit.</a> </li>
<li>[core] introduce the method to wake system up. <a href='https://commits.kde.org/powerdevil/bf14412420eefde6487ded5c3ef3a12ce9cc13e3'>Commit.</a> </li>
<li>Set min width of activities KCM to smaller value. <a href='https://commits.kde.org/powerdevil/671da4480222f93ab0d4ec855204ed553fdfae1b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407622'>#407622</a></li>
<li>On wakeup from suspend restore remembered keyboard brightness. <a href='https://commits.kde.org/powerdevil/ede6bb92ce170864505d1d9d444a41edcc188a7e'>Commit.</a> </li>
<li>[mobile] on mobile devices lock screen before we turn it off. <a href='https://commits.kde.org/powerdevil/e94b5d6079822b3d5ab406dc8ca636506f5e73a6'>Commit.</a> </li>
<li>[mobile] various adjustments related to power management. <a href='https://commits.kde.org/powerdevil/519dc0c442a85f2ef7f026728586b00cac712631'>Commit.</a> </li>
<li>[dpms action] introduce option to lock screen before turning off. <a href='https://commits.kde.org/powerdevil/a6e75570d23c3a29492f163659ef9e8105a51b4f'>Commit.</a> </li>
<li>Use camelcase (scripted). <a href='https://commits.kde.org/powerdevil/da5c94d975f7c9552d0baf1763396544713b4bf1'>Commit.</a> </li>
<li>[kcm] Remove incorrect information from error message. <a href='https://commits.kde.org/powerdevil/06539f067b73da15a1fdbbcd823d63ffe0d543d1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423872'>#423872</a></li>
<li>Fix compilation with ddcutil 0.9.9. <a href='https://commits.kde.org/powerdevil/fcb26be2fb279e6ad3b7b814d26a5921d16201eb'>Commit.</a> </li>
<li>Play sound when plugged in. <a href='https://commits.kde.org/powerdevil/f8736de4f0f4bc04e50b7c63e4212a4d5b0a3841'>Commit.</a> </li>
<li>Merge plasma/5.19. <a href='https://commits.kde.org/powerdevil/0e06ee589281c859c3db642ada08fa4c11236454'>Commit.</a> </li>
<li>Change "adaptor" to "adapter". <a href='https://commits.kde.org/powerdevil/d18d96f858c1b876c58c8d01d6f551db1276b5c1'>Commit.</a> </li>
<li>Fix build on FreeBSD. <a href='https://commits.kde.org/powerdevil/14c72b2d42aaebd98ea1b881843d468cebe39263'>Commit.</a> </li>
<li>WIP: Implement smooth brightness change for sysfs / backlight helper. <a href='https://commits.kde.org/powerdevil/c1ba54989cd973b744e2891ef389fee22f7a8ff5'>Commit.</a> </li>
<li>Use add_definitions instead of add_compile_definitions. <a href='https://commits.kde.org/powerdevil/18263ff6627ce253fe7c039ec60d0b86f7d5b81e'>Commit.</a> </li>
<li>Add a logging category config file. <a href='https://commits.kde.org/powerdevil/c7590f9065ec9547b7fabad77a548bbc0c693113'>Commit.</a> </li>
</ul>


<h3><a name='sddm-kcm' href='https://commits.kde.org/sddm-kcm'>SDDM KCM</a> </h3>
<ul id='ulsddm-kcm' style='display: block'>
<li>Remove explicit ECM_KDE_MODULE_DIR. <a href='https://commits.kde.org/sddm-kcm/e2cebc8834cf872705b72955aa54e19d7eb6e3fa'>Commit.</a> </li>
<li>Avoid empty message dialogs, cleanup. <a href='https://commits.kde.org/sddm-kcm/8acacca14eba2c445b0e9dd8a9b4193cb48cfb7d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407686'>#407686</a></li>
<li>RemoveDeadEntries option, cleanup files and config. <a href='https://commits.kde.org/sddm-kcm/e47246e24a9c7ddcdd87c2f1640ba0d07e254b4b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/416255'>#416255</a></li>
<li>Use camelcase (scripted). <a href='https://commits.kde.org/sddm-kcm/388ea232e9a44a6d3295c3acd44432642373d354'>Commit.</a> </li>
</ul>


<h3><a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a> </h3>
<ul id='ulsystemsettings' style='display: block'>
<li>Remove explicit ECM_KDE_MODULE_DIR. <a href='https://commits.kde.org/systemsettings/72f4aa2a2ed1ef043591cda944f04d0ff39eb775'>Commit.</a> </li>
<li>[Sidebar view] Only indent subsategory icons in the search view. <a href='https://commits.kde.org/systemsettings/90cbd408f5918a205a6d8244a3d5687c258b59fb'>Commit.</a> </li>
<li>Fix trying to call a method on a still null object. <a href='https://commits.kde.org/systemsettings/46d05391fc2c133020fa4133c3773a225d27240a'>Commit.</a> </li>
<li>[Sidebar view] Add arrow to list items which are parent/category items. <a href='https://commits.kde.org/systemsettings/afec6b29a4a6d49457d0376c5c42e30f0d3bdf35'>Commit.</a> </li>
<li>[Sidebar view] Add accessibility hints for icons-only ToolButtons. <a href='https://commits.kde.org/systemsettings/62aa94c055d2fd1001f428ab7c55d1efde84e04a'>Commit.</a> </li>
<li>Port sidebar placeholder message to Kirigami.PlaceholderMessage. <a href='https://commits.kde.org/systemsettings/091e1bc8159d14ca9cb58585a913d6e5b21ee9dc'>Commit.</a> </li>
<li>Don't show highlight changed entries button in kinfocenter. <a href='https://commits.kde.org/systemsettings/0995d6878108a564d784c14c7b4a3ce2443f5b54'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425964'>#425964</a></li>
<li>Rework main.cpp. <a href='https://commits.kde.org/systemsettings/949098e73da54ac9f38a21f5e396b5fe3ece5bf1'>Commit.</a> </li>
<li>Clean up subcategory model when going to home screen. <a href='https://commits.kde.org/systemsettings/313831f825c8c1cbeb1b05c8f49e976ae0eb82a3'>Commit.</a> </li>
<li>In sidebar mode show if a module is in default state or not. <a href='https://commits.kde.org/systemsettings/6cc205e3bfb888c62adb36a885a9f4e0bad6f739'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28461'>D28461</a></li>
<li>Button text Uses Title Case. <a href='https://commits.kde.org/systemsettings/19649460fed889d34df538fd3199981dd9a1ed16'>Commit.</a> </li>
<li>Add Separator. Remove round. Style. <a href='https://commits.kde.org/systemsettings/4375e4f020711e1236b0a50082228042c800fa5a'>Commit.</a> </li>
<li>Add button to activate defaults highlighting. <a href='https://commits.kde.org/systemsettings/e30185cb6e7c097cde9eb0a69299e8d69461188a'>Commit.</a> </li>
<li>[sidebar] Make SubcategoryModel dynamic. <a href='https://commits.kde.org/systemsettings/437706443456e016120e239e1966f39c5e70e8fa'>Commit.</a> </li>
<li>[sidebar view] Show list item separator lines. <a href='https://commits.kde.org/systemsettings/8e14096db9f5734da425ffd739504db60ce456d6'>Commit.</a> </li>
<li>Also hide Buttons when changing modules in info center mode. <a href='https://commits.kde.org/systemsettings/d8483b0fd9f76933272ec7ffdd705bd2e765ea38'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/424490'>#424490</a></li>
<li>[sidebar view] Remove size hacks for toolbar items. <a href='https://commits.kde.org/systemsettings/e711edd7a8592d1dd789ef9782de53d4726f3262'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/424228'>#424228</a></li>
<li>Correct appstream homepage URL to plasma homepage. <a href='https://commits.kde.org/systemsettings/fc09e9b07d62c26f091b7d1949a9c09366a71289'>Commit.</a> </li>
<li>Use camelcase (scripted). <a href='https://commits.kde.org/systemsettings/d96abcdf177dd656ee59173806cb19bc80590829'>Commit.</a> </li>
</ul>


<h3><a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a> </h3>
<ul id='ulxdg-desktop-portal-kde' style='display: block'>
<li>Remove explicit ECM_KDE_MODULE_DIR. <a href='https://commits.kde.org/xdg-desktop-portal-kde/5780ff5910d848db669e03e7f25d5762d06451bf'>Commit.</a> </li>
<li>AppModel: Rename role names. <a href='https://commits.kde.org/xdg-desktop-portal-kde/61ee6869277493e56580f1b9bcaa39f50c18e110'>Commit.</a> </li>
<li>Port AppChooserDialog to Kirigami. <a href='https://commits.kde.org/xdg-desktop-portal-kde/f0e760f9bac88bf309e9120fe8f132d18580423c'>Commit.</a> </li>
<li>Mobile filechooser: Allow to disable the mime type filter. <a href='https://commits.kde.org/xdg-desktop-portal-kde/0f3b64a8a98bccbf0af83f69d448a92671b2c836'>Commit.</a> </li>
<li>Mobile filechooser: Move more logic to c++. <a href='https://commits.kde.org/xdg-desktop-portal-kde/71b5e8dffcf4e7cd84cde2f18619f8ca6e69debe'>Commit.</a> </li>
<li>Appchooserdialog: Replace context properties. <a href='https://commits.kde.org/xdg-desktop-portal-kde/ee375e8a8abb0f8b84606b85dc664a7bc0720311'>Commit.</a> </li>
<li>Portaway from obsolute methods in QPrinter. <a href='https://commits.kde.org/xdg-desktop-portal-kde/0b0655b21e42c7427cf00eca805ef3e687d35886'>Commit.</a> </li>
<li>Mobile filechooser: Forward selectMultiple. <a href='https://commits.kde.org/xdg-desktop-portal-kde/9ed822ac94945f011b2c0731a6c112e72bd46a19'>Commit.</a> </li>
<li>FileChooser: Properly preselect MIME filter passed as "current_filter". <a href='https://commits.kde.org/xdg-desktop-portal-kde/c33458d37543e15b2a0cd2f6dcd3128218f89895'>Commit.</a> </li>
<li>Add "Name" entry into the desktop file. <a href='https://commits.kde.org/xdg-desktop-portal-kde/0b4dacf37b093a41438f6fd0e7157f23c97f4038'>Commit.</a> </li>
<li>Port appchooserdialog's app list to load kservices. <a href='https://commits.kde.org/xdg-desktop-portal-kde/c83f7c8b6a65610bd2222215282acf16fd4f5e1b'>Commit.</a> </li>
<li>Use kirigami icon instead of plasma icon in appchooser. <a href='https://commits.kde.org/xdg-desktop-portal-kde/cfd625a4a06a62eb4a49f06d4b24a929b65bd92f'>Commit.</a> </li>
<li>Print: enable printing of multiple copies. <a href='https://commits.kde.org/xdg-desktop-portal-kde/2fb7d5bd098216ac19b97918f6f0c0cacc04fcf2'>Commit.</a> See bug <a href='https://bugs.kde.org/419944'>#419944</a></li>
<li>Fix clazy warnings. <a href='https://commits.kde.org/xdg-desktop-portal-kde/dc73592e21781d300ea80225f1e5da89a61dd004'>Commit.</a> </li>
<li>Port away from deprecated API. <a href='https://commits.kde.org/xdg-desktop-portal-kde/b8c96dd28f6d967f0edae5cee03b510f8072e78d'>Commit.</a> </li>
<li>Open discover with mimetype when app choosing (if available). <a href='https://commits.kde.org/xdg-desktop-portal-kde/58122cb3ae3dc2743ae4fe23a916d8a177f48225'>Commit.</a> </li>
<li>Mobile filechooser: Fix handling of title and initial folder. <a href='https://commits.kde.org/xdg-desktop-portal-kde/74383e2642c01e0b9c64529906d7554a427adcca'>Commit.</a> </li>
<li>Screencasting: error out when kwin isn't providing the zkde_screencast_unstable_v1. <a href='https://commits.kde.org/xdg-desktop-portal-kde/8970eaf20c8022d7fe21fa0985ae4c477303ba6e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/424744'>#424744</a></li>
<li>FileChooser: Properly handle mnemonics in 'accept_label'. <a href='https://commits.kde.org/xdg-desktop-portal-kde/daab5c525e7aa14fbd63f678adc5f900e627a839'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422340'>#422340</a></li>
<li>Mobile filechooser: Show inline message for errors. <a href='https://commits.kde.org/xdg-desktop-portal-kde/9ee9f11342494512b4c0748132e448e25df541b8'>Commit.</a> </li>
<li>Mobile filechooser: Use an icon that is colorful even in small sizes. <a href='https://commits.kde.org/xdg-desktop-portal-kde/02bcfbc675290ab82a366f9a7727f19e987cb812'>Commit.</a> </li>
<li>Implement mobile filechooser interface. <a href='https://commits.kde.org/xdg-desktop-portal-kde/16e5d3ad9031210f00031150021027f0bd1c6293'>Commit.</a> </li>
<li>Screencasting: Adoption of the zkde_screencast_v1 Wayland protocol. <a href='https://commits.kde.org/xdg-desktop-portal-kde/ac67e45646b097a474f3284d2ff273a15112c749'>Commit.</a> </li>
<li>FileChooser: Return selected filter. <a href='https://commits.kde.org/xdg-desktop-portal-kde/8922d260001139925bc842d4ce8637f0cd941435'>Commit.</a> </li>
<li>FileChooser: Handle 'options' - 'choices'. <a href='https://commits.kde.org/xdg-desktop-portal-kde/67344e9f52ed638ec04143b64342eb6988127bdb'>Commit.</a> </li>
<li>FileChooser portal: Handle 'current_filter'. <a href='https://commits.kde.org/xdg-desktop-portal-kde/5b9b8f1923f49f33d3b2786dbb87e3d0d9fc2e30'>Commit.</a> </li>
</ul>


</main>
<?php
	require('../aether/footer.php');
