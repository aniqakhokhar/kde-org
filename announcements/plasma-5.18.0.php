<?php

    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "KDE Plasma 5.18: More Convenient and with Long Term Stability",
        'description' => 'Plasma 5.18 adds an emoji selector, user feedback capabilties, a global edit mode and improves and expands System Settings, the Discover software manager, widgets, GTK integration and much more.',
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = 'plasma-5.18.0'; // for i18n
    $version = "5.18.0";
?>

<script src="/js/use-ekko-lightbox.js" defer="true"></script>

<main class="releaseAnnouncment container" itemscope itemtype="https://schema.org/SoftwareApplication">

    <meta itemprop="name" content="Plasma 5.18" />
    <meta itemprop="url" content="https://kde.org/announcements/plasma-5.18.0" />
    <meta itemprop="applicationCategory" content="Desktop Environment" />
    <meta itemprop="operatingSystem" content="GNU/Linux, FreeBSD" />

    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("Plasma %1", $version)?> LTS</h1>

    <?php include "./announce-i18n-bar.inc"; ?>

    <figure class="embed-responsive embed-responsive-16by9 mx-auto" style="max-width: 900px">
        <iframe class="embed-responsive-item" sandbox="allow-same-origin allow-scripts" src="https://peertube.mastodon.host/videos/embed/cda402b5-2bcb-4c0c-b232-0fa5a4dacaf5" frameborder="0" allowfullscreen></iframe>
    </figure>


    <figure class="topImage" itemprop="screenshot" itemscope itemtype="http://schema.org/ImageObject">
        <img src="plasma-5.18/global_themes.png" class="w-100" style="max-width: 750px; height: auto;" alt="Plasma 5.18" itemprop="contentUrl"/>
        <figcaption itemprop="description"><?php print i18n_var("KDE Plasma %1", "5.18")?></figcaption>
    </figure>

    <p><?php i18n("Tuesday, 11 February 2020.")?></p>
    <p><?php i18n("Plasma 5.18 LTS is out!")?></p>

    <p><?php i18n("A brand new version of the Plasma desktop is now available. In Plasma 5.18 you will find neat new features that make notifications clearer, settings more streamlined and the overall look more attractive. Plasma 5.18 is easier and more fun to use, while at the same time allowing you to be more productive when it is time to work."); ?></p>

    <p><?php i18n("Apart from all the cool new stuff, Plasma 5.18 also comes with an LTS status. LTS stands for \"Long Term Support\". This means 5.18 will be updated and maintained by KDE contributors for the next two years (regular versions are maintained for 4 months). If you are thinking of updating or migrating your school, company or organization to Plasma, this version is your best bet, as you get the most stable version of Plasma *and* all the new features too."); ?></p>

    <h2 id="plasma"><?php i18n("Plasma");?></h3>

    <p><?php i18n("Plasma 5.18 is even more user-friendly as we have added more features that let you work, play and express yourself better. Take the new <i>Emoji Selector</i>: it is literally always just two keystrokes away. Hold down the Meta (Windows) key and press the period (.) and it will pop up. Click on the icon that best represents your feelings and you can paste the emoji into your email, social media post, text message or even your terminal."); ?></p>

    <figure class="text-center">
        <img style="max-width: 750px" class="w-100" src="plasma-5.18/emoji.png" />
        <figcaption><?php i18n("Emoji selector"); ?></figcaption>
    </figure>

    <p><?php i18n("You can also find the emoji finder in the <i>Kickoff</i> application launcher — the main menu button usually located in the bottom left-hand corner of the screen. Look under <i>Applications</i> &rarr; <i>Utilities</i>."); ?></p>

    <p><?php i18n("Another big change is the new <i>Global Edit</i> mode. The old desktop toolbox, the menu that used to live by default in the top right corner of your screen, is gone now, leaving a roomier, tidier desktop. Instead, we are introducing the <i>Global Edit</i> bar that you can activate by right-clicking on an empty area of your desktop and choosing \"<i>Customize layout</i>\" from the popup menu. The <i>Global Edit</i> bar appears across the top of the screen and gives you access to widgets, activities (extra workspaces you can set up separately from your regular virtual desktops) and the desktop configuration set of options."); ?></p>

    <figure class="text-center">
        <img style="max-width: 750px" class="w-100" src="plasma-5.18/customize-layout.png" />
        <figcaption><?php i18n("The new Global Edit mode"); ?></figcaption>
    </figure>

    <p><?php i18n("Continuing with improvements to the overall look, Plasma 5.18 comes with better support for GTK applications using client-side decorations. These applications now show proper shadows and the resize areas for them. GTK apps now also automatically inherit Plasma's settings for fonts, icons, mouse cursors and more."); ?></p>

    <p><?php i18n("To help relax your eyesight, there's a new system tray widget that lets you toggle the <i>Night Color</i> feature. You can also configure keyboard shortcuts to turn <i>Night Color</i> and <i>Do Not Disturb</i> modes on or off."); ?></p>

    <figure class="text-center">
        <img style="max-width: 750px" class="w-100" src="plasma-5.18/NightColorWidget.png" />
        <figcaption><?php i18n("Night color widget"); ?></figcaption>
    </figure>

    <p><?php i18n("Still down in the system tray, the <i>Audio Volume System</i> widget now sports a more compact design that makes it easier to choose the default audio device. There is also a clickable volume indicator for the apps shown in the <i>Task Manager</i> that are playing audio."); ?></p>

    <figure class="text-center">
        <img style="max-width: 750px" class="w-100" src="plasma-5.18/InteractiveVolumeTaskManager.png" />
        <figcaption><?php i18n("Per-application mute button in Task Manager"); ?></figcaption>
    </figure>

    <p><?php i18n("Then there are all the much more subtle details that help make Plasma look and feel smoother and slicker. These include from the tweaks to the weather widget, that now lets you view the wind conditions, down to the details of the <i>Kickoff</i> application launcher which is more touch-friendly and shows a user icon which is now circular instead of square."); ?></p>

    <h2><?php i18n("Notifications"); ?></h2>

    <p><?php i18n("The new more rounded and smoother Plasma look has also arrived to the timeout indicator on notifications, as this is also circular and surrounds the close button, making for a more compact and modern look."); ?></p>

    <p><?php i18n("But changes to notifications are not only aesthetic, as Plasma now shows you a notification when a connected Bluetooth device is about to run out of power. Notifications also add interactivity to their capabilities and the <i>File downloaded</i> notification now comes with a draggable icon, so you can quickly drag the file you just downloaded to where you want to keep it."); ?></p>

    <figure class="text-center">
        <img style="max-width: 750px" class="w-100" src="plasma-5.18/draggingDownloadwiget.png" />
        <figcaption><?php i18n("Drag and drop files from the notifications"); ?></figcaption>
    </figure>

    <h2><?php i18n("System Settings"); ?></h2>

    <p><?php i18n("There are quite a few new things in Plasma 5.18's <i>System Settings</i>. First and foremost is the optional <I>User Feedback</I> settings. These are disabled by default to protect your privacy."); ?></p>

    <p><?php i18n("That said, if you do decide to share information about your installation with us, none of the options allows the system to send any kind of personal information. In fact, the <i>Feedback settings</i> slider lets you decide how much you want to share with KDE developers. KDE developers can later use this information to improve Plasma further and better adapt it to your needs."); ?></p>

    <figure class="text-center">
        <img style="max-width: 750px" class="w-100" src="plasma-5.18/UserFeedback.png" />
        <figcaption><?php i18n("User feedback settings"); ?></figcaption>
    </figure>

    <p><?php i18n("Talking of adapting, if Plasma is famous for anything, it is for its flexibility and how it can be tweaked to all tastes. We made the process of customizing your desktop even more pleasant by redesigning the <i>Application Style</i> settings with a grid view. This makes it easier to see what your applications will look like once you pick your favorite style."); ?></p>

    <figure class="text-center">
        <img style="max-width: 750px" class="w-100" src="plasma-5.18/StyleLayout.png" />
        <figcaption><?php i18n("Application style settings"); ?></figcaption>
    </figure>

    <p><?php i18n("The grid view look with big previews has been extended to many other areas of <i>System Settings</i>. When you search for new themes to download in <i>Global Themes</i>, you can now appreciate the different designs in all their glory."); ?></p>

    <figure class="text-center">
        <img style="max-width: 750px" class="w-100" src="plasma-5.18/global_themes.png" />
        <figcaption><?php i18n("Global Theme: Get Hot New Stuff"); ?></figcaption>
    </figure>

    <p><?php i18n("Another visual aspect that you can tweak is the animations of the windows. We have added a slider for that under <i>General Behavior</i>: pull it all the way to the right, and windows will pop into existence instantaneously. Drag it all the way to the left, and they will sloooowly emerge from the task bar. Our advice is to go with the Goldilocks option and leave it somewhere in the middle. That provides enough visual cues for you to see what is happening, but is not so slow as to make it a drag to use."); ?></p>

    <p><?php i18n("If you find all these options overwhelming, don't worry: the <i>System Settings</i> search function has been much improved and will help you find the feature you want to change quickly. Start typing into the box at the top of the list and all the unrelated options will disappear, leaving you with a list of exactly what you're looking for."); ?>

    <h2><?php i18n("Discover"); ?></h2>

    <p><?php i18n("Discover is Plasma's visual software manager. From Discover, you can install, remove and update applications and libraries with a few clicks."); ?></p>

    <p><?php i18n("When you open Discover in Plasma 5.18, your cursor will be in the search field, inviting you to immediately start looking for the programs you want to install. But it is not only applications you can find here; you can also search for addons for Plasma (such as themes, splash screens, and cursor sets) and you can read the comments for each package users have written and find out what others think."); ?></p>

    <figure class="text-center">
        <img style="max-width: 750px" class="w-100" src="plasma-5.18/discover.png" />
        <figcaption><?php i18n("Plasma Discover"); ?></figcaption>
    </figure>

    <h2><?php i18n("More"); ?></h2>

    <p><?php i18n("Finally, we have improved the overall performance of Plasma on graphics hardware and decreased the amount of visual glitches in apps when using fractional scaling on X11. We have also included NVIDIA GPU stats into KSysGuard."); ?></p>

    <h2><?php i18n("New Since 5.12 LTS"); ?></h2>

    <p><?php i18n("For those upgrading from our previous Long Term Support release (Plasma 5.12), here are some of the highlights from the last two years of development:"); ?></p>

    <ul>
        <li><?php i18n("Completely rewritten notification system"); ?></li>
        <li><?php i18n("Browser integration"); ?></li>
        <li><?php i18n("Redesigned system settings pages, either using a consistent grid view or an overhauled interface"); ?></li>
        <li><?php i18n("GTK applications now respect more settings, use KDE color schemes, have shadows on X11, and support the global menu"); ?></li>
        <li><?php i18n("Display management improvements, including new OSD and widget"); ?></li>
        <li><?php i18n("Flatpak portal support"); ?></li>
        <li><?php i18n("Night Color feature"); ?></li>
        <li><?php i18n("Thunderbolt device management"); ?></li>
    </ul>


    <br clear="all" />

    <a href="plasma-5.17.5-5.18.0-changelog.php"><?php print i18n_var("Full Plasma %1 changelog", $version); ?></a>

    <?php /* Boilerplate again */ ?>
    <section class="row get-it">
        <article class="col-md">
            <h2><?php i18n("Live Images");?></h2>
            <p>
                <?php i18n("The easiest way to try it out is with a live image booted off a USB disk. Docker images also provide a quick and easy way to test Plasma.");?>
            </p>
            <a href='https://community.kde.org/Plasma/Live_Images' class="learn-more"><?php i18n("Download live images with Plasma 5");?></a>
            <a href='https://community.kde.org/Plasma/Docker_Images' class="learn-more"><?php i18n("Download Docker images with Plasma 5");?></a>
        </article>

        <article class="col-md">
            <h2><?php i18n("Package Downloads");?></h2>
            <p>
                <?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
            </p>
            <a href='https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro' class="learn-more"><?php i18n("Get KDE Software on Your Linux Distro wiki page");?></a>
        </article>

        <article class="col-md">
            <h2><?php i18n("Source Downloads");?></h2>
            <p>
                <?php i18n("You can install Plasma 5 directly from source.");?>
            </p>
            <a href='https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source' class='learn-more'><?php i18n("Community instructions to compile it");?></a>
            <a href='../info/plasma-5.18.0.php' class='learn-more'><?php i18n("Source Info Page");?></a>
        </article>
    </section>

    <section class="give-feedback">
        <h2><?php i18n("Feedback");?></h2>

        <p class="kSocialLinks">
            <?php i18n("You can give us feedback and get updates on our social media channels:"); ?>
            <?php include($site_root . "/contact/social_link.inc"); ?>
        </p>
        <p>
            <?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?>
        </p>

        <p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>Plasma Matrix chat room</a>, <a href='%2'>Plasma-devel mailing list</a> or report issues via <a href='%3'>bugzilla</a>. If you like what the team is doing, please let them know!", "https://webchat.kde.org/#/room/#kde-devel:kde.org", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided"); ?>

        <p><?php i18n("Your feedback is greatly appreciated.");?></p>
    </section>

    <h2>
        <?php i18n("Supporting KDE");?>
    </h2>

    <p align="justify">
        <?php print i18n_var("KDE is a <a href='%1'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='%2'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='%3'>Join the Game</a> initiative.", "http://www.gnu.org/philosophy/free-sw.html", "/community/donations/", "https://relate.kde.org/civicrm/contribute/transact?id=5"); ?>
    </p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

</main>
<?php
  require('../aether/footer.php');
