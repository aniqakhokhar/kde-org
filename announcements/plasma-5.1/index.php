<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Plasma 5.1 Brings Back Many Popular Features");
  $site_root = "../";
  $release = 'plasma-5.1.0';
  include "header.inc";
?>

<?php
  include "../announce-i18n-bar.inc";
?>

<div style="float: right; padding: 1ex; margin: 1ex; border: solid thin grey; background-image: none; text-align: center">
<a href="plasma-main.png">
<img src="plasma-main_wee.png" style="padding: 1ex; margin: 1ex; border: 0; background-image: none;" width="400" height="225" /></a><br />
<?php i18n("Plasma 5");?>
</div>

<p>
<?php i18n("October 15, 2014.
Today, KDE releases Plasma 5.1.0, the first release containing new features since the release of Plasma 5.0 this summer. Plasma 5.1 sports a wide variety of improvements, leading to greater stability, better performance and new and improved features. Thanks to the feedback of the community, KDE developers were able to package a large number of fixes and enhancements into this release, among which more complete and higher quality artwork following the new-in-5.0 Breeze style, re-addition of popular features such as the Icon Tasks taskswitcher and improved stability and performance.
</p><p>
Those travelling regularly will enjoy better support for time zones in the panel's clock, while those staying at home a revamped clipboard manager, allowing you to easily get at your past clipboard's content. The Breeze widget style is now also available for Qt4-based applications, leading to greater consistency across applications. The work to support Wayland as display server for Plasma is still ongoing, with improved, but not complete support in 5.1. <a href=\"https://community.kde.org/Plasma/5.1_Changes\">Changes</a> throughout many default components improve accessibility for visually impaired users by adding support for screenreaders and improved keyboard navigation.
</p><p>
</p><p>
Aside from the visual improvements and the work on features, the focus of this release lies also on stability and performance improvements, with over 180 bugs resolved since 5.0 in the shell alone. Plasma 5.1 requires <a href=\"https://www.kde.org/announcements/kde-frameworks-5.3.0.php\">KDE Frameworks 5.3</a>, which brings in another great amount of fixes and performance improvements on top of the large number of fixes that have gone into Plasma 5.1. If you want to help to make more of this happen, consider <a href='https://www.kde.org/fundraisers/yearend2014'>a donation</a> to KDE, so we can support more developers getting together to make great software.
");?>
</p>

<ul>
<li>
<?php print i18n("<a
href='https://community.kde.org/Plasma/5.1_Changes'>5.1 Changes List</a>");?>
</li>
</ul>


<br clear="all" />
<h2><?php i18n("Artwork and Visuals");?></h2>
<p>
<br clear="all" />
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; text-align: center">
<a href="qt4_widgets_plus_icontasks.jpg"><img src="qt4_widgets_plus_icontasks-wee.png" width="500" height="310"/></a><br />
<?php i18n("Breeze Theme for Qt 4");?>
</div>
<p>
<?php i18n("A new Breeze widget theme for Qt 4 lets applications written with KDE Platform 4 fit in with your Plasma 5 desktop.");?>
</p>
<?php i18n("
The Breeze artwork concept, which has made its first appearance in Plasma 5.0 has seen many improvements. The icon set is now more complete. The icons in the notification area in the panel have been touched up visually. A new native widget style improves rendering of applications used in Plasma. This new native style also works for Qt 4 letting applications written with KDE Platform 4 fit in with your Plasma 5 desktop. There is a <a href=\"plasma-lookandfeel.png\">new System Settings module</a> that lets you switch between desktop themes.
</p><p>
Overall, Plasma 5.1's Look and Feel refines the experience found in 5.0 noticeably. Behind all these changes are improvements to the <a href=\"https://techbase.kde.org/Projects/Usability/HIG\">Human Interface Guidelines</a>, which have led to a more consistent overall user experience.
");?>
</p>

<br clear="all" />
<h2><?php i18n("New and Old Features");?></h2>
<p>
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; text-align: center">
<a href="icons_task_manager.jpg">
<img src="icons_task_manager-wee.png" width="400" height="57" style="border: 0px" />
</a><br />
Icons-only Task Manager</div>
<?php i18n("
Plasma 5.1 brings back many features that users have grown used to from its 4.x predecessor. Popular additional widgets such as the <em>Icons-only Task Manager</em>, the <em>Notes</em> widget and the <em>System Load Viewer</em> make their re-entry. Support for multiple time zones has been added back in the panel's clock. The notifications have been visually improved, along with many bigger and smaller bug fixes.
</p><p>");?>

<br clear="all" />

<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; text-align: center">
<a href="alt_switcher.jpg">
<img src="alt_switcher-wee.png" width="320" height="261" style="border: 0px" /></a><br />
<?php i18n("Applet Switcher");?>
</div>
<?php i18n("A new feature allow you to easily switch between different widgets which share the same purpose. Changing the application launcher for example has become much easier to discover. Plasma panels have new switchers to easily swap between different widgets for the same task. You can select which application menu, clock or task manager you want with ease. The new <em>Clipboard</em> widget offers a redesigned user interface on top of Plasma's venerable clipboard manager, allowing the user to easily use the clipboard's history and preview files currently in the clipboard. Plasma's alternative launcher, <em>Kicker</em> has seen a large number of <a href=\"https://community.kde.org/Plasma/5.1_Changes#Kicker_Application_Menu\">improvements</a>, among which better accessibility and integration with the package manager.
</p><p>
Thanks to two Google Summer of Code projects, the Plasma Media Center and tablet-centric Plasma Active user experiences now have basic ports available from Git, but are not release-quality yet.
");?>
</p>
<br clear="all" />
<h2><?php i18n("Wayland");?></h2>
<p>
<?php i18n("
Further progress has been made on Wayland support. A new window manager binary 'kwin_wayland' now complements the existing 'kwin_x11', and is equipped with the ability to start a nested X server for compatibility with X11-based applications. A newly-created KWayland library provides Wayland setup information to KInfoCenter and other consumers. More work is needed and ongoing to run the Plasma workspace on Wayland; we expect this to bear fruit for end-users in 2015.
");?>
</p>

<?php i18n("
<h2>Suitability and Updates</h2>
<p>
Plasma 5.1 provides a core desktop with a feature set that will suffice for many users. The development team has concentrated on tools that make up the central workflows. While many features known from the Plasma 4.x series are already available in Plasma 5.1, not all of them have been ported and made available for Plasma 5 yet. As with any software release of this size, there may be bugs that make a migration to Plasma 5 hard for some users.  The development team would like to hear about issues you may run into, so they can be addressed and fixed. We have compiled a <a href=\"https://community.kde.org/Plasma/5.1_Errata\">list of problems</a> we are aware of, and working on. Users can expect monthly bugfix updates. A release bringing new features and brinding back even more old features will be made in early 2015.
</p>
");?>

<ul>
<li>
<?php print i18n("<a
href='https://community.kde.org/Plasma/5.1_Errata'>5.1 Known Bugs</a>");?>
</li>
</ul>

<br clear="all" />

<!-- // Boilerplate again -->

<h2><?php i18n("Live Images");?></h2>

<p><?php i18n("
The easiest way to try it out is the with a live image booted off a
USB disk.  Images are available for development versions of <a
href='http://cdimage.ubuntu.com/kubuntu-plasma5/'>Kubuntu Plasma 5</a>.
");?></p>

<h2><?php i18n("Package Downloads");?></h2>

<p><?php i18n("Some distributions have created, or are in the process
of creating, packages listed on our wiki page.
");?></p>

<ul>
<li>
<?php print i18n("<a
href='https://community.kde.org/Plasma/Packages'>Package
download wiki page</a>");?>
</li>
</ul>

<h2><?php i18n("Source Downloads");?></h2>

<p><?php i18n("You can install Plasma 5 directly from source. KDE's
community wiki has <a
href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>.
Note that Plasma 5 does not co-install with Plasma 4, you will need
to uninstall older versions or install into a separate prefix.
");?>
</p>

<ul>
<li>
<?php print i18n_var("
<a href='../../info/%1.php'>Source
Info Page</a>
", $release);?>
</li>
</ul>

<h2><?php i18n("Feedback");?></h2>

<?php print i18n_var("You can give us feedback and get updates on %1 or %2 or %3.", "<a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='../facebook.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a>", "<a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='../twitter.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a>", "<a href='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='../googleplus.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>" );?>

<p>
<?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?></a>
</p>

<p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>#Plasma IRC channel</a>,
<a href='%2'>Plasma-devel mailing list</a> or report issues via
<a href='%3'>bugzilla</a>.  If you like what the
team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided");?></p>

<p><?php i18n("Your feedback is greatly appreciated.");?></p>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p align="justify">
 <?php i18n("<p>We produce beautiful software for your computer, please we'd love you to join us improving it or helping fellow users.  If you can't find the time to contribute directly do consider <a href='https://www.kde.org/fundraisers/yearend2014'>sending a donation</a>, help to make the world a better place!");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
