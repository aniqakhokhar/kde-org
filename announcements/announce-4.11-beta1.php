<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships First Beta of Plasma Workspaces, Applications and Platform 4.11");
  $site_root = "../";
  $release = "4.10.80";
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p align="justify">

<?php i18n("June 13, 2013. Today KDE released the beta of the new versions of Workspaces, Applications, and Development Platform. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.");?>


<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="announce-4.11-beta1.png"><img src="announce-4.11-beta1_thumb.png" align="center" width="640" alt="<?php i18n("New KWalletManager user interface; changes to Okular");?>" title="<?php i18n("New KWalletManager user interface; changes to Okular");?>" /></a>
<br />
<em><?php i18n("New KWalletManager user interface; changes to Okular");?></em>
</div>


<?php i18n("The 4.11 releases include the following highlights and more:");?>

<ul>
    <li>
        <?php i18n("<strong>Qt Quick in Plasma Workspaces</strong>—Qt Quick is continuing to make its way into the Plasma Workspaces. Plasma Quick, KDE&quot;s extensions on top of Qt Quick, allow deeper integration and more powerful apps and Plasma components. One of the central Plasma widgets, the task manager, was completely rewritten in Plasma Quick. It got quite a few bug fixes on its way to the new QML version. The Battery widget was overhauled. It now shows information about all the batteries (e.g. mouse, keyboard) in a system. And the battery icons can now show a finer-grained load-status.");?>
    </li>

    <li>
    <?php i18n("<strong>Faster Nepomuk indexing</strong>—The Nepomuk semantic engine received massive performance optimizations (e.g., reading data is 6 or more times faster). Indexing happens in two stages: the first stage retrieves general information (such as file type and name) immediately; additional information like MP3 tags, author information and similar is extracted in a second, somehow slower stage. Metadata display is now much faster. In addition, the Nepomuk backup and restore system was improved. The system also got new indexers for documents like odt or docx.");?>
    </li>

    <li>
        <?php i18n("<strong>Kontact improvements</strong>—Kontact got a faster indexer for its PIM data with improvements to Nepomuk, and a <a href='http://www.aegiap.eu/kdeblog/2013/05/news-in-kdepim-4-11-header-theme-33-grantlee-theme-generator-headerthemeeditor/'>new theme editor</a> for email headers. The way it handles email images now allows it to resize pictures on the fly. The whole KDE PIM suite got a lot of bug fixes, such as the way it deals with Google Calender resources. The PIM Import Wizard allows users to import settings and data from <a href='http://trojita.flaska.net/'>Trojita</a> (the Qt IMAP email client) and all other importers were improved as well.");?>
    </li>

    <li>
        <?php i18n("<strong>KWin and Path to Wayland</strong>—Initial experimental support for Wayland was added to KWin. KWin also got many OpenGL improvements including support being added for creating an OpenGL 3.1 core context and robustness from using the new functionality provided by the GL_ARB_robustness extension. Numerous KWin optimizations are aimed at reducing CPU and memory overhead in the OpenGL backend. Some desktop effects have been re-written in JavaScript to ease maintenance.");?>
    </li>
</ul>

<?php i18n("More improvements can be found in <a href='http://techbase.kde.org/Schedules/KDE4/4.11_Feature_Plan'>the 4.11 Feature Plan</a>.");?>

<?php i18n("With the large number of changes, the 4.11 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the 4.11 team by installing the beta <a href='https://bugs.kde.org/'>and reporting any bugs</a>.");?>


<h3><?php i18n("KDE Software Compilation 4.11 Beta1");?></h3>
<p align="justify">
<?php i18n("The KDE Software Compilation, including all its libraries and its applications, is available for free under Open Source licenses. KDE's software can be obtained in source and various binary formats from <a href='http://download.kde.org/unstable/4.10.80/'>http://download.kde.org</a> or with any of the <a href='http://www.kde.org/download/distributions.php'>major GNU/Linux and UNIX systems</a> shipping today.");?>
</p>


<!-- // Boilerplate again -->

<h4>
  <?php i18n("Installing 4.11 Beta1 Binary Packages");?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php i18n("Some Linux/UNIX OS vendors have kindly provided binary packages of 4.11 Beta1 (internally 4.10.80) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.");?>
</p>

<p align="justify">
  <a name="package_locations"></a><em><?php i18n("Package Locations");?></em>.
  <?php i18n("For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11_Beta_1_.284.10.80.29'>Community Wiki</a>.");?>
</p>

<h4>
  <?php i18n("Compiling 4.11 Beta1");?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php i18n("The complete source code for 4.11 Beta1 may be <a href='http://download.kde.org/unstable/4.10.80/src/'>freely downloaded</a>. Instructions on compiling and installing 4.10.80 are available from the <a href='/info/4.10.80.php'>4.10.80 Info Page</a>.");?>
</p>

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='http://jointhegame.kde.org/'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
