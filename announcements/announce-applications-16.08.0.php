<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships KDE Applications 16.08.0");
  $site_root = "../";
  $version = "16.08.0";
  $release = "applications-".$version;
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p align="justify">
<?php print i18n_var("August 18, 2016. Today, KDE introduces KDE Applications 16.08, with an impressive array of upgrades when it comes to better ease of access, the introduction of highly useful functionalities and getting rid of some minor issues, bringing KDE Applications one step closer to offering you the perfect setup for your device.");?>
</p>

<p align="justify">
<?php print i18n_var("<a href='%1'>Kolourpaint</a>, <a href='%2'>Cervisia</a> and KDiskFree have now been ported to KDE Frameworks 5 and we look forward to your feedback and insight into the newest features introduced with this release.", "https://www.kde.org/applications/graphics/kolourpaint/", "https://www.kde.org/applications/development/cervisia/");?>
</p>

<p align="justify">
<?php print i18n_var("In the continued effort to split Kontact Suite libraries to make them easier to use for third parties, the kdepimlibs tarball has been split into akonadi-contacts, akonadi-mime and akonadi-notes.");?>
</p>


<p align="justify">
<?php print i18n_var("We have discontinued the following packages: kdegraphics-strigi-analyzer, kdenetwork-strigi-analyzers, kdesdk-strigi-analyzers, libkdeedu and mplayerthumbs. This will help us focus in the rest of the code.");?>
</p>


<h3 style="clear:both;" ><?php print i18n_var("Keeping in Kontact");?></h3>

<p align="justify">
<?php print i18n_var("<a href='%1'>The Kontact Suite</a> has got the usual round of cleanups, bug fixes and optimizations in this release. Notable is the use of QtWebEngine in various compontents, which allows for a more modern HTML rendering engine to be used. We have also improved VCard4 support as well as added new plugins that can warn if some conditions are met when sending an email, e.g. verifying that you want to allow sending emails with a given identity, or checking if you are sending email as plaintext, etc.", "https://userbase.kde.org/Kontact");?>
</p>


<h3 style="clear:both;" ><?php print i18n_var("New Marble version");?></h3>

<p align="justify">
<?php print i18n_var("<a href='%1'>Marble</a> 2.0 is part of KDE Applications 16.08 and includes more than 450 code changes including improvements in navigation, rendering and an experimental vector rendering of OpenStreetMap data.", "https://marble.kde.org/");?>
</p>


<h3 style="clear:both;" ><?php print i18n_var("More Archiving");?></h3>

<p align="justify">
<?php print i18n_var("<a href='%1'>Ark</a> can now extract AppImage and .xar files as well as testing the integrity of zip, 7z and rar archives. It can also add/edit comments in rar archives", "https://www.kde.org/applications/utilities/ark/");?>
</p>


<h3 style="clear:both;" ><?php print i18n_var("Terminal Improvements");?></h3>

<p align="justify">
<?php print i18n_var("<a href='%1'>Konsole</a> has had improvements regarding font rendering options and accessbility support.", "https://www.kde.org/applications/system/konsole/");?>
</p>


<h3 style="clear:both;" ><?php print i18n_var("And more!");?></h3>

<p align="justify">
<?php print i18n_var("<a href='%1'>Kate</a> got movable tabs. <a href='%2'>More information...</a>", "https://kate-editor.org", "https://kate-editor.org/2016/06/15/kates-tabbar-gets-movable-tabs/");?>
</p>

<p align="justify">
<?php print i18n_var("<a href='%1'>KGeography</a>, has added provinces and regions maps of Burkina Faso.", "https://www.kde.org/applications/education/kgeography/");?>
</p>


<h3 style="clear:both;" ><?php print i18n_var("Aggressive Pest Control");?></h3>
<p align="justify">
<?php print i18n_var("More than 120 bugs have been resolved in applications including the Kontact Suite, Ark, Cantor, Dolphin, KCalc, Kdenlive and more!");?>
</p>

<h3 style="clear:both;"><?php print i18n_var("Full Changelog");?></h3>

<p align="justify">
<?php print i18n_var("You can find the full list of changes <a href='%1'>here</a>.", "fulllog_applications.php?version=".$version);?>
</p>

<h4>
<?php i18n("Spread the Word");?>
</h4>
<p align="justify">
<?php print i18n_var("Non-technical contributors are an important part of KDE’s success. While proprietary software companies have huge advertising budgets for new software releases, KDE depends on people talking with other people. Even for those who are not software developers, there are many ways to support the KDE Applications 16.08 release. Report bugs. Encourage others to join the KDE Community. Or <a href='%1'>support the nonprofit organization behind the KDE community</a>.", "https://relate.kde.org/civicrm/contribute/transact?reset=1&id=5"); ?>
</p>

<p align="justify">
<?php i18n("Please spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, and twitter. Upload screenshots of your new set-up to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with 'KDE'. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the KDE Applications 16.08 release."); ?>
</p>

<!-- // Boilerplate again -->

<h4>
  <?php i18n("Installing KDE Applications 16.08 Binary Packages");?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php i18n("Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 16.08 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.");?>
</p>

<p align="justify">
  <a name="package_locations"></a><em><?php i18n("Package Locations");?></em>.
  <?php i18n("For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_Applications/Binary_Packages'>Community Wiki</a>.");?>
</p>

<h4>
  <?php i18n("Compiling KDE Applications 16.08");?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php i18n("The complete source code for KDE Applications 16.08 may be <a href='http://download.kde.org/stable/applications/16.08.0/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications-16.08.0.php'>KDE Applications 16.08.0 Info Page</a>.");?>
</p>

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
