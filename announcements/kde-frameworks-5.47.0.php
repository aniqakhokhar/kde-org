<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.47.0");
  $site_root = "../";
  $release = '5.47.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
June 09, 2018. KDE today announces the release
of KDE Frameworks 5.47.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Terminate query execution early if subterm returns empty result set");?></li>
<li><?php i18n("Avoid crash when reading corrupt data from document terms db (bug 392877)");?></li>
<li><?php i18n("handle string lists as input");?></li>
<li><?php i18n("Ignore more types of source files (bug 382117)");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("updated handles and overflow-menu");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Android toolchain: allow to specify extra libs manually");?></li>
<li><?php i18n("Android: Don't define qml-import-paths if it's empty");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("handle zip files embedded within zip files (bug 73821)");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("[KCModuleQml] Ignore disabled controls when tabbing");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("kcfg.xsd - do not require a kcfgfile element");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Fix the \"Default\" color scheme to match Breeze again");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Set kcm context property on the correct context");?></li>
<li><?php i18n("[Plotter] Don't render if m_node is null (bug 394283)");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Update the list of Ukrainian entities");?></li>
<li><?php i18n("add entity OSD to general.entites");?></li>
<li><?php i18n("Add entities CIFS, NFS, Samba, SMB to general.entities");?></li>
<li><?php i18n("Add Falkon, Kirigami, macOS, Solid, USB, Wayland, X11, SDDM to general entities");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("check that ffmpeg is at least version 3.1 that introduce the API we require");?></li>
<li><?php i18n("search for album artist and albumartist tags in taglibextractor");?></li>
<li><?php i18n("popplerextractor: don't try to guess the title if there isn't one");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("Make sure ungrab keyboard request is processed before emitting shortcut (bug 394689)");?></li>
</ul>

<h3><?php i18n("KHolidays");?></h3>

<ul>
<li><?php i18n("holiday_es_es - Fix day of the \"Comunidad de Madrid\"");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Check if group &lt; LastGroup, as KIconEffect doesn't handle UserGroup anyway");?></li>
</ul>

<h3><?php i18n("KImageFormats");?></h3>

<ul>
<li><?php i18n("Remove duplicated mime types from json files");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Check if destination exists also when pasting binary data (bug 394318)");?></li>
<li><?php i18n("Auth support: Return the actual length of socket buffer");?></li>
<li><?php i18n("Auth support: Unify API for file descriptor sharing");?></li>
<li><?php i18n("Auth support: Create socket file in user's runtime directory");?></li>
<li><?php i18n("Auth support: Delete socket file after use");?></li>
<li><?php i18n("Auth support: Move task of cleaning up socket file to FdReceiver");?></li>
<li><?php i18n("Auth support: In linux don't use abstract socket to share file descriptor");?></li>
<li><?php i18n("[kcoredirlister] Remove as many url.toString() as possible");?></li>
<li><?php i18n("KFileItemActions: fallback to default mimetype when selecting only files (bug 393710)");?></li>
<li><?php i18n("Introduce KFileItemListProperties::isFile()");?></li>
<li><?php i18n("KPropertiesDialogPlugin can now specify multiple supported protocols using X-KDE-Protocols");?></li>
<li><?php i18n("Preserve fragment when redirecting from http to https");?></li>
<li><?php i18n("[KUrlNavigator] Emit tabRequested when path in path selector menu is middle-clicked");?></li>
<li><?php i18n("Performance: use the new uds implementation");?></li>
<li><?php i18n("Don't redirect smb:/ to smb:// and then to smb:///");?></li>
<li><?php i18n("Allow accepting by double-click in save dialog (bug 267749)");?></li>
<li><?php i18n("Enable preview by default in the filepicker dialog");?></li>
<li><?php i18n("Hide file preview when icon is too small");?></li>
<li><?php i18n("i18n: use plural form again for plugin message");?></li>
<li><?php i18n("Use a regular dialog rather than a list dialog when trashing or deleting a single file");?></li>
<li><?php i18n("Make the warning text for deletion operations emphasize its permanency and irreversibility");?></li>
<li><?php i18n("Revert \"Show view mode buttons in the open/save dialog's toolbar\"");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("Show action.main more prominently on the ToolBarApplicationHeader");?></li>
<li><?php i18n("Allow Kirigami build without KWin tablet mode dependency");?></li>
<li><?php i18n("correct swipefilter on RTL");?></li>
<li><?php i18n("correct resizing of contentItem");?></li>
<li><?php i18n("fix --reverse behavior");?></li>
<li><?php i18n("share contextobject to always access i18n");?></li>
<li><?php i18n("make sure tooltip is hidden");?></li>
<li><?php i18n("make sure to not assign invalid variants to the tracked properties");?></li>
<li><?php i18n("handle not a MouseArea, dropped() signal");?></li>
<li><?php i18n("no hover effects on mobile");?></li>
<li><?php i18n("proper icons overflow-menu-left and right");?></li>
<li><?php i18n("Drag handle to reorder items in a ListView");?></li>
<li><?php i18n("Use Mnemonics on the toolbar buttons");?></li>
<li><?php i18n("Added missing files in QMake's .pri");?></li>
<li><?php i18n("[API dox] Fix Kirigami.InlineMessageType -&gt; Kirigami.MessageType");?></li>
<li><?php i18n("fix applicationheaders in applicationitem");?></li>
<li><?php i18n("Don't allow showing/hiding the drawer when there's no handle (bug 393776)");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("KConcatenateRowsProxyModel: properly sanitize input");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Fix crashes in NotifyByAudio when closing applications");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("kpackage_install_*package: fix missing dep between .desktop and .json");?></li>
<li><?php i18n("make sure paths in rcc are never derived from absolute paths");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("Process DBus replies in the ::match thread (bug 394272)");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Don't use title case for the \"show word count\" checkbox");?></li>
<li><?php i18n("Make the word/char count a global preference");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Increase org_kde_plasma_shell interface version");?></li>
<li><?php i18n("Add \"SkipSwitcher\" to API");?></li>
<li><?php i18n("Add XDG Output Protocol");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("[KCharSelect] Fix table cell size with Qt 5.11");?></li>
<li><?php i18n("[API dox] Remove usage of \overload, resulting in broken docs");?></li>
<li><?php i18n("[API dox] Tell doxygen \"e.g.\" does not end the sentence, use \".\ \"");?></li>
<li><?php i18n("[API dox] Remove unneeded HTML escaping");?></li>
<li><?php i18n("Don't automatically set the default icons for each style");?></li>
<li><?php i18n("Make KMessageWidget match Kirigami inlineMessage's style (bug 381255)");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Make information about unhandled property just debug messages");?></li>
<li><?php i18n("WirelessSetting: implement assignedMacAddress property");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Templates: consistent naming, fix translation catalog names &amp; more");?></li>
<li><?php i18n("[Breeze Plasma Theme] Fix kleopatra icon to use color stylesheet (bug 394400)");?></li>
<li><?php i18n("[Dialog] Handle dialog being minimized gracefully (bug 381242)");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("Improve Telegram integration");?></li>
<li><?php i18n("Treat inner arrays as OR constraints rather than AND");?></li>
<li><?php i18n("Make it possible to constrain plugins by a desktop file presence");?></li>
<li><?php i18n("Make it possible to filter plugins by executable");?></li>
<li><?php i18n("Highlight the selected device in the KDE Connect plugin");?></li>
<li><?php i18n("fix i18n issues in frameworks/purpose/plugins");?></li>
<li><?php i18n("Add Telegram plugin");?></li>
<li><?php i18n("kdeconnect: Notify when the process fails to start (bug 389765)");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Use pallet property only when using qtquickcontrols 2.4");?></li>
<li><?php i18n("Work with Qt&lt;5.10");?></li>
<li><?php i18n("Fix height of tabbar");?></li>
<li><?php i18n("Use Control.palette");?></li>
<li><?php i18n("[RadioButton] Rename \"control\" to \"controlRoot\"");?></li>
<li><?php i18n("Don't set explicit spacing on RadioButton/CheckBox");?></li>
<li><?php i18n("[FocusRect] Use manual placement instead of anchors");?></li>
<li><?php i18n("It turns out the flickable in a scrollview is the contentItem");?></li>
<li><?php i18n("Show focus rect when CheckBox or RadioButton are focused");?></li>
<li><?php i18n("hacky fix to scrollview detection");?></li>
<li><?php i18n("Don't reparent the flickable to the mousearea");?></li>
<li><?php i18n("[TabBar] Switch tabs with mouse wheel");?></li>
<li><?php i18n("Control must not have children (bug 394134)");?></li>
<li><?php i18n("Constrain scroll (bug 393992)");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Perl6: Add support for extensions .pl6, .p6, or .pm6 (bug 392468)");?></li>
<li><?php i18n("DoxygenLua: fix closing comment blocks (bug 394184)");?></li>
<li><?php i18n("Add pgf to the latex-ish file formats (same format as tikz)");?></li>
<li><?php i18n("Add postgresql keywords");?></li>
<li><?php i18n("Highlighting for OpenSCAD");?></li>
<li><?php i18n("debchangelog: add Cosmic Cuttlefish");?></li>
<li><?php i18n("cmake: Fix DetectChar warning about escaped backslash");?></li>
<li><?php i18n("Pony: fix identifier and keyword");?></li>
<li><?php i18n("Lua: updated for Lua5.3");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.47");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.8");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
