<?php
    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.74.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.74.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>


<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
September 06, 2020. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.74.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Attica");?></h3>

<ul>
<li><?php i18n("Use Q_DECLARE_OPERATORS_FOR_FLAGS in same namespace as flags definition");?></li>
</ul>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Relicense many files to LGPL-2.0-or-later");?></li>
<li><?php i18n("Use common UDS creation code also for tags (bug 419429)");?></li>
<li><?php i18n("Factor out common UDS creation code from KIO workers");?></li>
<li><?php i18n("[balooctl] Show allowed formats in help text");?></li>
<li><?php i18n("[balooctl] Show current file in status output in indexing state");?></li>
<li><?php i18n("[balooctl] Set QDBusServiceWatcher mode from constructor");?></li>
<li><?php i18n("[balooctl] Formatting cleanup");?></li>
<li><?php i18n("Use Q_DECLARE_OPERATORS_FOR_FLAGS in same namespace as flags definition");?></li>
<li><?php i18n("[OrPostingIterator] Do not advance when wanted id is lower than current");?></li>
<li><?php i18n("[Extractor] Remove QWidgets dependency from extractor helper");?></li>
<li><?php i18n("[Extractor] Remove KAboutData from extractor helper executable");?></li>
<li><?php i18n("Update various references in README");?></li>
<li><?php i18n("[FileContentIndexer] Remove unused config constructor argument and member");?></li>
<li><?php i18n("[balooctl] Fix QProcess::start deprecated warning, provide empty args list");?></li>
<li><?php i18n("[Engine] Propagate transaction errors (bug 425017)");?></li>
<li><?php i18n("[Engine] Remove unused method hasChanges from {Write}Transaction");?></li>
<li><?php i18n("Don't index .ytdl files (bug 424925)");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Make the keepassxc icon more faithful to the official one");?></li>
<li><?php i18n("Add more symlinks for new keepassxc system tray icon names (bug 425928)");?></li>
<li><?php i18n("Add another alias for keepass icon (bug 425928)");?></li>
<li><?php i18n("Add icon for Godot project MIME type");?></li>
<li><?php i18n("Add icon for Anaconda installer");?></li>
<li><?php i18n("Add 96px places icons");?></li>
<li><?php i18n("Remove unused places to communicate");?></li>
<li><?php i18n("Run application-x-bzip-compressed-tar through <code>scour</code> (bug 425089)");?></li>
<li><?php i18n("Make application-gzip symlink to application-x-gzip (bug 425059)");?></li>
<li><?php i18n("Add aliases for MP3 icons (bug 425059)");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Strip leading zeros from numerical version numbers in C++ code");?></li>
<li><?php i18n("Add timeout for qmlplugindump calls");?></li>
<li><?php i18n("Add WaylandProtocols find module");?></li>
<li><?php i18n("invoke update-mime-database with -n");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Clarify license statement according to KDElibs History");?></li>
</ul>

<h3><?php i18n("KActivitiesStats");?></h3>

<ul>
<li><?php i18n("Use Boost::boost for older CMake versions");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Drop empty X-KDE-PluginInfo-Depends");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Document dependencies in requirements.txt and install them in setup.py");?></li>
<li><?php i18n("Opt-In Display of Library License");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Use Q_DECLARE_OPERATORS_FOR_FLAGS in same namespace as flags definition");?></li>
</ul>

<h3><?php i18n("KBookmarks");?></h3>

<ul>
<li><?php i18n("KBookmarkManager: clear memory tree when the file is deleted");?></li>
</ul>

<h3><?php i18n("KCalendarCore");?></h3>

<ul>
<li><?php i18n("Always store X-KDE-VOLATILE-XXX properties as volatile");?></li>
<li><?php i18n("Document the expected TZ transitions in Prague");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("KCModuleData: Fix headers, improve api doc and rename method");?></li>
<li><?php i18n("Extend KCModuleData with revertToDefaults and matchQuery functionality");?></li>
<li><?php i18n("Try to avoid horizontal scrollbar in KCMultiDialog");?></li>
<li><?php i18n("Add KCModuleData as base class for plugin");?></li>
<li><?php i18n("Allow extra button to be added to KPluginSelector (bug 315829)");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Make KWindowConfig::allConnectedScreens() static and internal (bug 425953)");?></li>
<li><?php i18n("Add standard shortcut for \"Create Folder\"");?></li>
<li><?php i18n("Introduce method to query KConfigSkeletonItem default value");?></li>
<li><?php i18n("Remember window sizes on a per-screen-arrangement basis");?></li>
<li><?php i18n("Extract code to get list of connected screens into a re-usable function");?></li>
<li><?php i18n("Add functions to save and restore window positions on non-Wayland platforms (bug 415150)");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Avoid swapping defaults to read KConfigSkeletonItem default value");?></li>
<li><?php i18n("Fix KLanguageName::nameForCodeInLocale for codes that QLocale doesn't know about");?></li>
<li><?php i18n("KLanguageName::allLanguageCodes: Take into account there can be more than one locale directory");?></li>
<li><?php i18n("KConfigDialog: Try to avoid horizontal scrollbars");?></li>
<li><?php i18n("Function that returns the list of Language Codes");?></li>
</ul>

<h3><?php i18n("KContacts");?></h3>

<ul>
<li><?php i18n("Addressee::parseEmailAddress(): Check length of full name before trimming");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Add *.kcrash glob pattern to KCrash Report MIME type");?></li>
<li><?php i18n("Use Q_DECLARE_OPERATORS_FOR_FLAGS in same namespace as flags definition");?></li>
<li><?php i18n("Do not wait for fam events indefinitely (bug 423818)");?></li>
<li><?php i18n("[KFormat] Allow formatting values to arbitrary binary units");?></li>
<li><?php i18n("Make it possible to KPluginMetadata from QML");?></li>
<li><?php i18n("[KFormat] Fix binary example");?></li>
</ul>

<h3><?php i18n("KDAV");?></h3>

<ul>
<li><?php i18n("metainfo.yaml: Add tier key and tweak description");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("[KKeySequenceItem] Make Meta+Shift+num shortcuts work");?></li>
<li><?php i18n("Expose checkForConflictsAgainst property");?></li>
<li><?php i18n("Add a new AbstractKCM class");?></li>
<li><?php i18n("Port KRunProxy away from KRun");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("org.kde.kded5.desktop: Add missing required key \"Name\" (bug 408802)");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Update contributor.entities");?></li>
<li><?php i18n("Use placeholder markers");?></li>
</ul>

<h3><?php i18n("KEmoticons");?></h3>

<ul>
<li><?php i18n("Recover EmojiOne license information");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Convert old Mac line endings in lyrics tags (bug 425563)");?></li>
<li><?php i18n("Port to ECM new FindTaglib module");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("Load service files for shortcuts for applications data dir as fallback (bug 421329)");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Fix possible preprocessor race condition with i18n defines");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("StatJob: make mostLocalUrl work only with protoclClass == :local");?></li>
<li><?php i18n("KPropertiesDialog: also load plugins with JSON metadata");?></li>
<li><?php i18n("Revert \"[KUrlCompletion] Don't append / to completed folders\" (bug 425387)");?></li>
<li><?php i18n("Simplify KProcessRunner constructor");?></li>
<li><?php i18n("Allow CCBUG and FEATURE keywords for bugs.kde.org (bug )");?></li>
<li><?php i18n("Update help text for editing the app command in a .desktop entry to comply with current spec (bug 425145)");?></li>
<li><?php i18n("KFileFilterCombo: Don't add the allTypes option if we only have 1 item");?></li>
<li><?php i18n("Use Q_DECLARE_OPERATORS_FOR_FLAGS in same namespace as flags definition");?></li>
<li><?php i18n("Fix potential crash deleting a menu in event handler (bug 402793)");?></li>
<li><?php i18n("[filewidgets] Fix KUrlNavigatorButton padding on breadcrumb (bug 425570)");?></li>
<li><?php i18n("ApplicationLauncherJob: tweak docs");?></li>
<li><?php i18n("Fix untranslated items in kfileplacesmodel");?></li>
<li><?php i18n("ApplicationLauncherJob: fix crash if there's no open-with handler set");?></li>
<li><?php i18n("Rename \"Web Shortcuts\" KCM to \"Web Search Keywords\"");?></li>
<li><?php i18n("KFileWidget: reparse config to grab dirs added by other instances of the app (bug 403524)");?></li>
<li><?php i18n("Move web shortcuts kcm to search category");?></li>
<li><?php i18n("Automatically remove trailing whitespace instead of showing a warning");?></li>
<li><?php i18n("Avoid systemd launched applications from closing forked children (bug 425201)");?></li>
<li><?php i18n("smb: fix share name availability check");?></li>
<li><?php i18n("smb: keep stderr of net commands on add/remove (bug 334618)");?></li>
<li><?php i18n("smb: rejigger guest allowed check and publish as areGuestsAllowed");?></li>
<li><?php i18n("KFileWidget: Clear urls before rebuilding the list");?></li>
<li><?php i18n("KFileWidget: remove default URLs top path combo");?></li>
<li><?php i18n("KFilePlacesModel: add default places when upgrading from older version");?></li>
<li><?php i18n("Fix 2-years regression in KUrlComboBox, setUrl() didn't append anymore");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("kirigami.pri: Add managedtexturenode.cpp");?></li>
<li><?php i18n("make buddyfor custom positioning work again");?></li>
<li><?php i18n("Account for More button size if we already know it will be visible");?></li>
<li><?php i18n("Add a property to ToolBarLayout to control how to deal with item height (bug 425675)");?></li>
<li><?php i18n("make checkbox labels readable again");?></li>
<li><?php i18n("support displayComponent for actions");?></li>
<li><?php i18n("make submenus visible");?></li>
<li><?php i18n("Fix QSGMaterialType forward declaration");?></li>
<li><?php i18n("Make OverlaySheet header and footer use appropriate background colors");?></li>
<li><?php i18n("Check low power environment variable for value, not just if it is set");?></li>
<li><?php i18n("Add setShader function to ShadowedRectangleShader to simplify setting shaders");?></li>
<li><?php i18n("Add a low power version of the sdf functions file");?></li>
<li><?php i18n("Make overloads of sdf_render use the full-argument sdf_render function");?></li>
<li><?php i18n("Remove need for core profile defines in shadowedrect shader main");?></li>
<li><?php i18n("Base inner rectangle on outer rectangle for bordered shadowedrectangle");?></li>
<li><?php i18n("Use the right shaders for ShadowedTexture when using core profile");?></li>
<li><?php i18n("Add \"lowpower\" versions of the shadowedrectangle shaders");?></li>
<li><?php i18n("Initialize m_component member in PageRoute");?></li>
<li><?php i18n("Fix Material SwipeListItem");?></li>
<li><?php i18n("new logic to remove acceleration marks (bug 420409)");?></li>
<li><?php i18n("remove forceSoftwarerendering for now");?></li>
<li><?php i18n("just show the source item on software rendering");?></li>
<li><?php i18n("a software fallback for the shadowed texture");?></li>
<li><?php i18n("Use the new showMenuArrow property on background for the menu arrow");?></li>
<li><?php i18n("don't hide the header when overshooting up");?></li>
<li><?php i18n("move ManagedTextureNode in own file");?></li>
<li><?php i18n("ToolBarLayout: Add spacing to visibleWidth if we're displaying the more button");?></li>
<li><?php i18n("Do not override Heading pixel size in BreadCrumbControl (bug 404396)");?></li>
<li><?php i18n("Update app template");?></li>
<li><?php i18n("[passivenotification] Set explicit padding (bug 419391)");?></li>
<li><?php i18n("Enable clipping in the GlobalDrawer StackView");?></li>
<li><?php i18n("Remove opacity from disabled PrivateActionToolButton");?></li>
<li><?php i18n("Make ActionToolBar a control");?></li>
<li><?php i18n("swipenavigator: add control over which pages are displayed");?></li>
<li><?php i18n("Declare the underlying type of the DisplayHint enum to be uint");?></li>
<li><?php i18n("Add ToolBarLayout/ToolBarLayoutDelegate to pri file");?></li>
<li><?php i18n("kirigami.pro: Use source file list from kirigami.pri");?></li>
<li><?php i18n("Do not delete incubators in completion callback");?></li>
<li><?php i18n("Ensure menuActions remains an array instead of a list property");?></li>
<li><?php i18n("Add return type to DisplayHint singleton lambda");?></li>
<li><?php i18n("Account for item height when vertically centering delegates");?></li>
<li><?php i18n("Always queue a new layout, even if we are currently layouting");?></li>
<li><?php i18n("Rework InlineMessage layout using anchors");?></li>
<li><?php i18n("Use full width to check if all actions fit");?></li>
<li><?php i18n("Add a comment about the extra spacing for centering");?></li>
<li><?php i18n("Fix check for tooltip text in PrivateActionToolButton");?></li>
<li><?php i18n("Workaround qqc2-desktop-style ToolButton not respecting non-flat IconOnly");?></li>
<li><?php i18n("Prefer collapsing KeepVisible actions over hiding later KeepVisible actions");?></li>
<li><?php i18n("Hide all following actions when the first action gets hidden");?></li>
<li><?php i18n("Add a notify signal for ToolBarLayout::actions and emit it at the right time");?></li>
<li><?php i18n("Expose action to separator of ActionsMenu");?></li>
<li><?php i18n("Rename ActionsMenu loaderDelegate kirigamiAction property to action");?></li>
<li><?php i18n("Add missing loaderDelegate that checks visibility");?></li>
<li><?php i18n("Hide action delegates until they are positioned");?></li>
<li><?php i18n("ToolBarLayout: Replace custom lazy-loading of delegates with QQmlIncubator");?></li>
<li><?php i18n("Move displayHintSet to C++ so it can be called from there");?></li>
<li><?php i18n("Support right-to-left mode in ToolBarLayout");?></li>
<li><?php i18n("Add minimumWidth property to ToolBarLayout");?></li>
<li><?php i18n("Rework PrivateActionToolButton for improved performance");?></li>
<li><?php i18n("Deprecate ActionToolBar::hiddenActions");?></li>
<li><?php i18n("Set layout hints for ActionToolBar");?></li>
<li><?php i18n("Use non-deprecated DisplayHint in ToolBarPageHeader");?></li>
<li><?php i18n("Display component errors if delegate items fail instantiation");?></li>
<li><?php i18n("Hide delegates of actions that were removed");?></li>
<li><?php i18n("Cleanup full/icon delegate items on delegate destruction");?></li>
<li><?php i18n("Enforce full/icon delegate item visibility");?></li>
<li><?php i18n("Use ToolBarLayout for ActionToolBar's layouting");?></li>
<li><?php i18n("Add some comments to ToolBarLayout::maybeHideDelegate");?></li>
<li><?php i18n("Add visibleWidth property to ToolBarLayout");?></li>
<li><?php i18n("Introduce ToolBarLayout native object");?></li>
<li><?php i18n("Move DisplayHint from Action to C++ enums file");?></li>
<li><?php i18n("Add icons used in the about page to cmake icon packaging macro");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Fix cache sync issues with QtQuick dialog (bug 417985)");?></li>
<li><?php i18n("Drop empty X-KDE-PluginInfo-Depends");?></li>
<li><?php i18n("Remove entries if they do not match filter anymore (bug 425135)");?></li>
<li><?php i18n("Fix edge case where KNS gets stuck (bug 423055)");?></li>
<li><?php i18n("Make the internal kpackage job task less fragile (bug 425811)");?></li>
<li><?php i18n("Remove downloaded file when using kpackage installation");?></li>
<li><?php i18n("Support a different style of kpackage knsrc for fallback");?></li>
<li><?php i18n("Handle /* notation for RemoveDeadEntries (bug 425704)");?></li>
<li><?php i18n("Make it easier to get the cache for an already initialised engine");?></li>
<li><?php i18n("Add a reverse lookup for entries based on their installed files");?></li>
<li><?php i18n("Use /* notation for subdir uncompression and allow subdir uncompression if file is archive");?></li>
<li><?php i18n("Fix flood of \"Pixmap is a null pixmap\" warnings");?></li>
<li><?php i18n("Remove slashes from entry name when interpreting it as path (bug 417216)");?></li>
<li><?php i18n("Fix a sometimes-crash with the KPackageJob task (bug 425245)");?></li>
<li><?php i18n("Use same spinner for loading and initializing (bug 418031)");?></li>
<li><?php i18n("Remove details button (bug 424895)");?></li>
<li><?php i18n("Run uninstall script async (bug 418042)");?></li>
<li><?php i18n("Disable search when not available");?></li>
<li><?php i18n("Hide load more spinner while updating/installing (bug 422047)");?></li>
<li><?php i18n("Do not append download target dir to entry files");?></li>
<li><?php i18n("Remove * character when passing dir to script");?></li>
<li><?php i18n("Do not focus first element in icon view mode (bug 424894)");?></li>
<li><?php i18n("Avoid unnecessary starting of uninstall job");?></li>
<li><?php i18n("Add a RemoveDeadEntries option for knsrc files (bug 417985)");?></li>
<li><?php i18n("[QtQuick dialog] Fix last instance of incorrect update icon");?></li>
<li><?php i18n("[QtQuick dialog] Use more appropriate uninstall icons");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("Do not delete package root if package was deleted (bug 410682)");?></li>
</ul>

<h3><?php i18n("KQuickCharts");?></h3>

<ul>
<li><?php i18n("Add a fillColorSource property to Line charts");?></li>
<li><?php i18n("Calculate smoothing based on item size and device pixel ratio");?></li>
<li><?php i18n("Use average of size instead of maximum to determine line smoothness");?></li>
<li><?php i18n("Base amount of smoothing in line charts on chart size");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("Add template for python runner");?></li>
<li><?php i18n("Update template for KDE Store compatibility and improve README");?></li>
<li><?php i18n("Add support for runner syntaxes to dbus runners");?></li>
<li><?php i18n("Save RunnerContext after each match session (bug 424505)");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Implement invokeTerminal on Windows with workdir, command and envs");?></li>
<li><?php i18n("Fix application preference ordering for mimetypes with multiple inheritance (bug 425154)");?></li>
<li><?php i18n("Bundle the Application servicetype into a qrc file");?></li>
<li><?php i18n("Expand tilde character when reading working directory (bug 424974)");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Vimode: Make the small delete register (-) directly accessible");?></li>
<li><?php i18n("Vimode: Copy the behavior of vim's numbered registers");?></li>
<li><?php i18n("Vimode: Simplify the append-copy implementation");?></li>
<li><?php i18n("Make \"search for selection\" search if there is no selection");?></li>
<li><?php i18n("Multiline mode makes sense only for multiline regex");?></li>
<li><?php i18n("Add comment about checking pattern.isEmpty()");?></li>
<li><?php i18n("Port the search interface from QRegExp to QRegularExpression");?></li>
<li><?php i18n("Vimode: Implement append-copy");?></li>
<li><?php i18n("Speed up a *lot* loading large files");?></li>
<li><?php i18n("Only show zoom level when it is not 100%");?></li>
<li><?php i18n("Add a zoom indicator to the status bar");?></li>
<li><?php i18n("Added a separate config option for bracket match preview");?></li>
<li><?php i18n("Show a preview of the matching open bracket's line");?></li>
<li><?php i18n("Allow more control over invocation of completion models when automatic invocation is not used");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("Avoid clash with a macro in ctype.h from OpenBSD");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Add KRecentFilesMenu to replace KRecentFileAction");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Install platform plugins in a directory with no dots in file name (bug 425652)");?></li>
<li><?php i18n("[xcb] Scaled icon geometry correctly everywhere");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Allow opting out of remembering window positions on X11 (bug 415150)");?></li>
<li><?php i18n("Save and restore position of main window (bug 415150)");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("[PC3/BusyIndicator] Avoid running invisible animation");?></li>
<li><?php i18n("Don't use highlightedTextColor for TabButtons");?></li>
<li><?php i18n("Remove Layout.minimumWidth from Button and ToolButton");?></li>
<li><?php i18n("Use the spacing property for the spacing between Button/ToolButton icons and labels");?></li>
<li><?php i18n("Add private/ButtonContent.qml for PC3 Buttons and ToolButtons");?></li>
<li><?php i18n("Change PC3 Button and ToolButton implicitWidth and implicitHeight to account for inset values");?></li>
<li><?php i18n("Add implicitWidth and implicitHeight to ButtonBackground");?></li>
<li><?php i18n("Fix incorrect default for PlasmaExtras.ListItem (bug 425769)");?></li>
<li><?php i18n("Don't let the background become smaller than the svg (bug 424448)");?></li>
<li><?php i18n("Use Q_DECLARE_OPERATORS_FOR_FLAGS in same namespace as flags definition");?></li>
<li><?php i18n("Make PC3 BusyIndicator visuals keep a 1:1 aspect ratio (bug 425504)");?></li>
<li><?php i18n("Use ButtonFocus and ButtonHover in PC3 ComboBox");?></li>
<li><?php i18n("Use ButtonFocus and ButtonHover in PC3 RoundButton");?></li>
<li><?php i18n("Use ButtonFocus and ButtonHover in PC3 CheckIndicator");?></li>
<li><?php i18n("Unify the flat/normal behavior of PC3 Buttons/ToolButtons (bug 425174)");?></li>
<li><?php i18n("Make Heading use PC3 Label");?></li>
<li><?php i18n("[PlasmaComponents3] Make checkbox text fill its layout");?></li>
<li><?php i18n("Give PC2 slider implicitWidth and implicitHeight");?></li>
<li><?php i18n("Copy files rather than broken symlinks");?></li>
<li><?php i18n("Fix toolbutton-hover margins in button.svg (bug #425255)");?></li>
<li><?php i18n("[PlasmaComponents3] very small refactor of ToolButton shadow code");?></li>
<li><?php i18n("[PlasmaComponents3] Fix reversed condition for flat ToolButton shadow");?></li>
<li><?php i18n("[PlasmaComponents3] Strip mnemonic ampersands from tooltip text");?></li>
<li><?php i18n("Only draw the focus indicator when we got focus via the keyboard (bug 424446)");?></li>
<li><?php i18n("Drop implicit minimum sizing from PC2 and PC3 Buttons");?></li>
<li><?php i18n("Add PC3 equivalent to PC2 ListItem");?></li>
<li><?php i18n("Fix toolbar svg");?></li>
<li><?php i18n("[pc3] Make ToolBar more aligned with qqc2-desktop-style one");?></li>
<li><?php i18n("Expose the applet metadata in AppletInterface");?></li>
<li><?php i18n("Don't truncate DPR to an integer in cache ID");?></li>
<li><?php i18n("Add timeout property to ToolTipArea");?></li>
<li><?php i18n("Set type to Dialog in flags if type is Dialog::Normal");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("Apply initial configuration data when loading config UI");?></li>
<li><?php i18n("Restore behaviour of AlternativesView");?></li>
<li><?php i18n("[jobcontroller] Disable separate process");?></li>
<li><?php i18n("Rework job view handling (bug 419170)");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Fix StandardKey shortcut sequences in MenuItem showing as numbers");?></li>
<li><?php i18n("Don't use parent height/width for implicit ToolSeparator sizing (bug 425949)");?></li>
<li><?php i18n("Only use \"focus\" style for non-flat toolbuttons on press");?></li>
<li><?php i18n("Add top and bottom padding to ToolSeparator");?></li>
<li><?php i18n("Make ToolSeparator respect topPadding and bottomPadding values");?></li>
<li><?php i18n("Make MenuSeparator use background's calculated height not implicitheight");?></li>
<li><?php i18n("Fix toolbuttons with menus using newer Breeze");?></li>
<li><?php i18n("Draw the entire CheckBox control via the QStyle");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Adding static storageAccessFromPath method, needed by https://phabricator.kde.org/D28745");?></li>
</ul>

<h3><?php i18n("Syndication");?></h3>

<ul>
<li><?php i18n("Correct license exception");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("convert all themes to new keys for editor colors");?></li>
<li><?php i18n("change theme json format, use meta object enum names for editor colors");?></li>
<li><?php i18n("check kateversion &gt;= 5.62 for fallthroughContext without fallthrough=\"true\" and use attrToBool for boolean attribute");?></li>
<li><?php i18n("Add syntax definition for todo.txt");?></li>
<li><?php i18n("fix matchEscapedChar(): the last character of a line is ignored");?></li>
<li><?php i18n("Fix isDigit(), isOctalChar() and isHexChar(): must only match ascii characters");?></li>
<li><?php i18n("generate themes overview");?></li>
<li><?php i18n("add theme meta data to header of HTML pages we generate");?></li>
<li><?php i18n("start to generate theme collection page");?></li>
<li><?php i18n("rename 'Default' to 'Breeze Light'");?></li>
<li><?php i18n("Varnish, Vala &amp; TADS3: use the default color style");?></li>
<li><?php i18n("Improve the Vim Dark color theme");?></li>
<li><?php i18n("Add Vim Dark color theme");?></li>
<li><?php i18n("add syntax-highlighting theme files to json highlighting");?></li>
<li><?php i18n("Ruby/Rails/RHTML: add spellChecking in itemDatas");?></li>
<li><?php i18n("Ruby/Rails/RHTML: use the default color style and other improvements");?></li>
<li><?php i18n("LDIF, VHDL, D, Clojure &amp; ANS-Forth94: use the default color style");?></li>
<li><?php i18n("ASP: use the default color style and other improvements");?></li>
<li><?php i18n("let objective-c win for .m files");?></li>
<li><?php i18n(".mm is more likely Objective-C++ then meta math");?></li>
<li><?php i18n("use notAsciiDelimiters only with a not ascii char");?></li>
<li><?php i18n("optimize isWordDelimiter(c) with ascii character");?></li>
<li><?php i18n("optimize Context::load");?></li>
<li><?php i18n("use std::make_shared which removes the allocation on the control block");?></li>
<li><?php i18n("add proper license to update scripty");?></li>
<li><?php i18n("move update script for kate-editor.org/syntax to syntax repository");?></li>
<li><?php i18n("SELinux CIL &amp; Scheme: update bracket colors for dark themes");?></li>
<li><?php i18n("POV-Ray: use default color style");?></li>
<li><?php i18n("use krita installer NSIS script as example input, taken from krita.git");?></li>
<li><?php i18n("add hint to update script for website");?></li>
<li><?php i18n("Add minimal Django Template example");?></li>
<li><?php i18n("update refs after latest hl changes");?></li>
<li><?php i18n("CMake: fix illegible colors in dark themes and other improvements");?></li>
<li><?php i18n("Add pipe and ggplot2 example");?></li>
<li><?php i18n("fix naming error for varnish hl");?></li>
<li><?php i18n("use right highlighting for 68k ASM: Motorola 68k (VASM/Devpac)");?></li>
<li><?php i18n("fix XML to be valid in respect to XSD");?></li>
<li><?php i18n("BrightScript: Add exception syntax");?></li>
<li><?php i18n("Improve comments in some syntax definitions (Part 3)");?></li>
<li><?php i18n("R Script: use default color style and other improvements");?></li>
<li><?php i18n("PicAsm: fix unknown preprocessor keywords highlighting");?></li>
<li><?php i18n("Improve comments in some syntax definitions (Part 2)");?></li>
<li><?php i18n("Modelines: remove LineContinue rules");?></li>
<li><?php i18n("quickfix broken hl file");?></li>
<li><?php i18n("Optimization: check if we are on a comment before using ##Doxygen which contains several RegExpr");?></li>
<li><?php i18n("Assembly languages: several fixes and more context highlighting");?></li>
<li><?php i18n("ColdFusion: use the default color style and replace some RegExpr rules");?></li>
<li><?php i18n("Improve comments in some syntax definitions, Pt. 1");?></li>
<li><?php i18n("Use angle brackets for context information");?></li>
<li><?php i18n("Revert removal of byte-order-mark");?></li>
<li><?php i18n("xslt: change color of XSLT tags");?></li>
<li><?php i18n("Ruby, Perl, QML, VRML &amp; xslt: use the default color style and improve comments");?></li>
<li><?php i18n("txt2tags: improvements and fixes, use the default color style");?></li>
<li><?php i18n("Fix errors and add fallthroughContext=AttrNormal for each context of diff.xml because all rules contain column=0");?></li>
<li><?php i18n("fix issues found by static checker");?></li>
<li><?php i18n("import Pure highlighting from https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/pure.xml");?></li>
<li><?php i18n("fix kateversion attribute");?></li>
<li><?php i18n("make highlighting lookup order independent of translations");?></li>
<li><?php i18n("fix version format + spacing issues");?></li>
<li><?php i18n("import Modula-3 highlighting from https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/modula-3.xml");?></li>
<li><?php i18n("fix version format");?></li>
<li><?php i18n("fix spacing stuff found by static checker");?></li>
<li><?php i18n("import LLVM highlighting from https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/llvm.xml");?></li>
<li><?php i18n("fix spacing stuff found by static checker");?></li>
<li><?php i18n("import Idris from https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/idris.xml");?></li>
<li><?php i18n("fix faults found by static checker");?></li>
<li><?php i18n("import ATS from https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/ats.xml");?></li>
<li><?php i18n("detach only for not freshly created data");?></li>
<li><?php i18n("create StateData on demand");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.74");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.12");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://invent.kde.org/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://invent.kde.org/groups/frameworks/-/merge_requests", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
