<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.18.6 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.18.6";
?>

<style>
main {
	padding-top: 20px;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Confirm reboot action with the user. <a href='https://commits.kde.org/discover/f2dce150e6d32810d1deae08378a136fc13e9988'>Commit.</a> </li>
<li>Fwupd: discard search if the url is present but is not for fwupd. <a href='https://commits.kde.org/discover/a09c7e103da75966206fd2742a3f8a696d656c71'>Commit.</a> </li>
<li>Disable session management for Discover. <a href='https://commits.kde.org/discover/0012f339a6cb5fbfd51ad1747373a081a88959c2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415874'>#415874</a></li>
</ul>


<h3><a name='drkonqi' href='https://commits.kde.org/drkonqi'>Dr Konqi</a> </h3>
<ul id='uldrkonqi' style='display: block'>
<li>Initialize bools to false by default. <a href='https://commits.kde.org/drkonqi/c517b795936fdab641849ca83ec8c3e253e32de8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421187'>#421187</a>. Phabricator Code review <a href='https://phabricator.kde.org/D29675'>D29675</a></li>
</ul>


<h3><a name='kactivitymanagerd' href='https://commits.kde.org/kactivitymanagerd'>kactivitymanagerd</a> </h3>
<ul id='ulkactivitymanagerd' style='display: block'>
<li>Fix removing of icons and activity settings for new and existing setups. <a href='https://commits.kde.org/kactivitymanagerd/54896998ef1249f721a2fe616b61b51b4eda33fd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385814'>#385814</a></li>
</ul>


<h3><a name='kdecoration' href='https://commits.kde.org/kdecoration'>KDE Window Decoration Library</a> </h3>
<ul id='ulkdecoration' style='display: block'>
<li>Remove is-git-checkout condition for applying deprecated API visibility. <a href='https://commits.kde.org/kdecoration/56a772f45e4cf49f6cea77bbcac0869c5b195e50'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>Port task switchers to use standard sizes for everything else. <a href='https://commits.kde.org/kdeplasma-addons/36a1d68229d44d83df6292d4278bc5fe55e6d012'>Commit.</a> </li>
<li>Port task switchers to use units.iconSizes. <a href='https://commits.kde.org/kdeplasma-addons/29d0f34c6b0aad8c41a15c2227ea35764bb9d9cb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422547'>#422547</a></li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>X11: Make removal of X11 event filters safe. <a href='https://commits.kde.org/kwin/381b685b518f5dcffb93c18cdd89b558acde987b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423319'>#423319</a></li>
<li>Fix invalidation of cached x stacking order in wayland only mode. <a href='https://commits.kde.org/kwin/cf58fa1e6cb8efe1393f80bc57eb5916bfeb80ea'>Commit.</a> </li>
<li>Effects/slidingpopups: Properly clip windows. <a href='https://commits.kde.org/kwin/1df01c2d256289625870618abb8d0ab3f8b324ec'>Commit.</a> </li>
<li>Grab all possible keyboard modifiers for window commands. <a href='https://commits.kde.org/kwin/c8908fd0c215fd5fe7511e81c6f723163248fc5d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/424272'>#424272</a></li>
<li>[x11] Hold a passive grab on buttons only when needed. <a href='https://commits.kde.org/kwin/617adda2911e08cef70e110b3125e7d3f60498d1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394772'>#394772</a></li>
<li>Check if we successfully restored input focus. <a href='https://commits.kde.org/kwin/8894338eda9da05b687fdde3190ceeaef2ee9090'>Commit.</a> See bug <a href='https://bugs.kde.org/424223'>#424223</a></li>
<li>Partially revert a0c4a8e766a2160. <a href='https://commits.kde.org/kwin/ff22415ca6855bd9a7602f1ea849e460b252bf5b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/424223'>#424223</a></li>
<li>[x11] Force FocusIn events for already focused windows. <a href='https://commits.kde.org/kwin/a0c4a8e766a2160213838daf6f71c7ae6c3705df'>Commit.</a> </li>
<li>[x11] Create egl scene textures with right size. <a href='https://commits.kde.org/kwin/96774e79e7931d1fe6db8bc2c615d61fef366ad3'>Commit.</a> </li>
<li>Fix the Plastik decoration with Qt 5.15. <a href='https://commits.kde.org/kwin/ec602e0c2a676aed0707c7fb7edfe964516dbc77'>Commit.</a> </li>
<li>[wayland] Fix misuse of EGL/eglmesaext.h. <a href='https://commits.kde.org/kwin/2c76cc4784382b3df9b5413860d0793ea26cea31'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/422131'>#422131</a></li>
<li>[wayland] Place lockscreen greeter above other windows. <a href='https://commits.kde.org/kwin/6f8b8efb338117ee197092e46b25b489b612257d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D29523'>D29523</a></li>
<li>[kcmkwin/kwindecoration] Don't exec() QDialog. <a href='https://commits.kde.org/kwin/5ea54eda5d1f91428933d338ea8b950aea86d43a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421053'>#421053</a>. Phabricator Code review <a href='https://phabricator.kde.org/D29473'>D29473</a></li>
</ul>


<h3><a name='plasma-browser-integration' href='https://commits.kde.org/plasma-browser-integration'>plasma-browser-integration</a> </h3>
<ul id='ulplasma-browser-integration' style='display: block'>
<li>[DownloadJob] Fall back to url when finalUrl isn't set. <a href='https://commits.kde.org/plasma-browser-integration/d0dee5588cbe7e4ac055bfba975217f73ea3b947'>Commit.</a> </li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Kcm_fonts: Make the font selection dialog select the correct "Regular"-like style. <a href='https://commits.kde.org/plasma-desktop/e5e5f5ed51aadfac99bfbdf3d2db5be16a12443b'>Commit.</a> See bug <a href='https://bugs.kde.org/420287'>#420287</a></li>
<li>[kcms/desktoppath] Use folder dialogs instead of file dialogs. <a href='https://commits.kde.org/plasma-desktop/79ba2969da701868a772ca9f79f1a7248e9999a0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/424438'>#424438</a></li>
<li>[kcm cursortheme] Also clear default theme when resetting. <a href='https://commits.kde.org/plasma-desktop/91dd3c7c5ba3097f97b16a105fe64ebc5de0e4df'>Commit.</a> </li>
<li>Fix blurry icons in KColorSchemeEditor. <a href='https://commits.kde.org/plasma-desktop/2c46f28170c785f04fa396803037a608c8077025'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418165'>#418165</a>. Phabricator Code review <a href='https://phabricator.kde.org/D29481'>D29481</a></li>
</ul>


<h3><a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a> </h3>
<ul id='ulplasma-integration' style='display: block'>
<li>Use KDE shortcuts for "move to trash" action coming from QStandardKey. <a href='https://commits.kde.org/plasma-integration/b629b1d297b9758fe6207a45e960655cdf50bfd0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/426573'>#426573</a></li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Remove (seemingly debug) warning statement from passworddialog. <a href='https://commits.kde.org/plasma-nm/0c8118f256812b8b672a7ea17b0ceff6e713148d'>Commit.</a> </li>
<li>Icon in system tray missing when WireGuard connection active on startup. <a href='https://commits.kde.org/plasma-nm/da52d01788f5f07aa60ed7a6f21d7943a2e0c8d3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/420983'>#420983</a>. Phabricator Code review <a href='https://phabricator.kde.org/D29469'>D29469</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Fix calendar events not being shown at first. <a href='https://commits.kde.org/plasma-workspace/97648f015cff39e46e39ee7b150515d1d3bce5f7'>Commit.</a> </li>
<li>[applets/systemtray] Fix context menu misplaced. <a href='https://commits.kde.org/plasma-workspace/a409b06bbfb0ea10959d9f680ea1ef16e7b7827a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/421275'>#421275</a></li>
<li>[Notifications] Don't take updated time into account for sorting. <a href='https://commits.kde.org/plasma-workspace/d71181245f138f45da2760e85fdb6c38b785c0d9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D29771'>D29771</a></li>
<li>[sddm-theme] Prevent the logo from leaking in after a fadeout. <a href='https://commits.kde.org/plasma-workspace/68601675d13bdf44248991a56e888093e58c56c3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D29351'>D29351</a></li>
</ul>


<h3><a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>Watch DBus service right away to discard pending inhibitions reliably. <a href='https://commits.kde.org/powerdevil/d21102cc6c7a4db204a29f376ce5eb316ef57a6e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/423131'>#423131</a></li>
</ul>


<h3><a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a> </h3>
<ul id='ulsystemsettings' style='display: block'>
<li>[sidebar view] Set icon sizes properly on home screen. <a href='https://commits.kde.org/systemsettings/a783a055ecb8d07fd30f616b7e7f647ab2cc300f'>Commit.</a> </li>
<li>Hide configure action in Info center mode. <a href='https://commits.kde.org/systemsettings/8060235a74b8d1f9f2b3558066d69ac3569b1caf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417977'>#417977</a>. Fixes bug <a href='https://bugs.kde.org/417981'>#417981</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27565'>D27565</a></li>
</ul>


</main>
<?php
	require('../aether/footer.php');
