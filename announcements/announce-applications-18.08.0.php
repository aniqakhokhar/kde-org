<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships KDE Applications 18.08.0");
  $site_root = "../";
  $version = "18.08.0";
  $release = "applications-".$version;
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p align="justify">
<?php print i18n_var("August 16, 2018. KDE Applications 18.08.0 are now released.");?>
</p>

<p align="justify">
<?php print i18n_var("We continuously work on improving the software included in our KDE Application series, and we hope you will find all the new enhancements and bug fixes useful!");?>
</p>

<h3 style="clear:both;" ><?php print i18n_var("What's new in KDE Applications 18.08");?></h3>

<h4 style="clear:both;" ><?php print i18n_var("System");?></h4>
<figure style="float: right; margin: 0px"><a href="dolphin1808-settings.png"><img src="dolphin1808-settings.png" width="250" height="227" /></a></figure>
<p align="justify">
<?php print i18n_var("<a href='%1'>Dolphin</a>, KDE's powerful file manager, has received various quality-of-life improvements:", "https://www.kde.org/applications/system/dolphin/");?>
<ul>
<li><?php i18n("The 'Settings' dialog has been modernized to better follow our design guidelines and be more intuitive.");?></li>
<li><?php i18n("Various memory leaks that could slow down your computer have been eliminated.");?></li>
<li><?php i18n("'Create New' menu items are no longer available when viewing the trash.");?></li>
<li><?php i18n("The application now adapts better to high resolution screens.");?></li>
<li><?php i18n("The context menu now includes more useful options, allowing you to sort and change the view mode directly.");?></li>
<li><?php i18n("Sorting by modification time is now 12 times faster.");?></li>
<?php i18n("Also, you can now launch Dolphin again when logged in using the root user account. Support for modifying root-owned files when running Dolphin as a normal user is still work in progress.");?>
</ul>
</p>
<figure style="float: right; margin: 0px"><a href="konsole1808-find.png"><img src="konsole1808-find.png" width="250" height="166" /></a></figure>
<p align="justify">
<?php print i18n_var("Multiple enhancements for <a href='%1'>Konsole</a>, KDE's terminal emulator application, are available:", "https://www.kde.org/applications/system/konsole/");?>
<ul>
<li><?php i18n("The 'Find' widget will now appear on the top of the window without disrupting your workflow.");?></li>
<li><?php i18n("Support for more escape sequences (DECSCUSR & XTerm Alternate Scroll Mode) has been added.");?></li>
<li><?php i18n("You can now also assign any character(s) as a key for a shortcut.");?></li>
</ul>
</p>

<h4 style="clear:both;" ><?php print i18n_var("Graphics");?></h4>
<figure style="float: right; margin: 0px"><a href="gwenview1808.png"><img src="gwenview1808_small.png" width="250" height="238" /></a></figure>
<p align="justify">
<?php print i18n_var("18.08 is a major release for <a href='%1'>Gwenview</a>, KDE's image viewer and organizer. Over the last months contributors worked on a plethora of improvements. Highlights include:", "https://www.kde.org/applications/graphics/gwenview/");?>
<ul>
<li><?php i18n("Gwenview's statusbar now features an image counter and displays the total number of images.");?></li>
<li><?php i18n("It is now possible to sort by rating and in descending order. Sorting by date now separates directories and archives and was fixed in some situations.");?></li>
<li><?php i18n("Support for drag-and-drop has been improved to allow dragging files and folders to the View mode to display them, as well as dragging viewed items to external applications.");?></li>
<li><?php i18n("Pasting copied images from Gwenview now also works for applications which only accept raw image data, but no file path. Copying modified images is now supported as well.");?></li>
<li><?php i18n("Image resize dialog has been overhauled for greater usability and to add an option for resizing images based on percentage.");?></li>
<li><?php i18n("Red Eye Reduction tool's size slider and crosshair cursor were fixed.");?></li>
<li><?php i18n("Transparent background selection now has an option for 'None' and can be configured for SVGs as well.");?></li>
</ul>
<figure style="float: right; margin: 0px"><a href="gwenview1808-resize.png"><img src="gwenview1808-resize.png" width="250" height="191" /></a></figure>
<?php i18n("Image zooming has become more convenient:");?>
<ul>
<li><?php i18n("Enabled zooming by scrolling or clicking as well as panning also when the Crop or Red Eye Reduction tools are active.");?></li>
<li><?php i18n("Middle-clicking once again toggles between Fit zoom and 100% zoom.");?></li>
<li><?php i18n("Added Shift-middle-clicking and Shift+F keyboard shortcuts for toggling Fill zoom.");?></li>
<li><?php i18n("Ctrl-clicking now zooms faster and more reliably.");?></li>
<li><?php i18n("Gwenview now zooms to the cursor's current position for Zoom In/Out, Fill and 100% zoom operations when using the mouse and keyboard shortcuts.");?></li>
</ul>
<?php i18n("Image comparison mode received several enhancements:");?>
<ul>
<li><?php i18n("Fixed size and alignment of the selection highlight.");?></li>
<li><?php i18n("Fixed SVGs overlapping the selection highlight.");?></li>
<li><?php i18n("For small SVG images, the selection highlight matches the image size.");?></li>
</ul>
<figure style="float: right; margin: 0px"><a href="gwenview1808-sort.png"><img src="gwenview1808-sort_small.png" width="250" height="171" /></a></figure>
<?php i18n("A number of smaller enhancements was introduced to make your workflow even more enjoyable:");?>
<ul>
<li><?php i18n("Improved the fade transitions between images of varying sizes and transparencies.");?></li>
<li><?php i18n("Fixed the visibility of icons in some floating buttons when a light color scheme is used.");?></li>
<li><?php i18n("When saving an image under a new name, the viewer does not jump to an unrelated image afterwards.");?></li>
<li><?php i18n("When the share button is clicked and kipi-plugins are not installed, Gwenview will prompt the user to install them. After the installation they are immediately displayed.");?></li>
<li><?php i18n("Sidebar now prevents getting hidden accidentally while resizing and remembers its width.");?></li>
</ul>
</p>

<h4 style="clear:both;" ><?php print i18n_var("Office");?></h4>
<figure style="float: right; margin: 0px"><a href="kontact1808.png"><img src="kontact1808.png" width="250" height="205" /></a></figure>
<p align="justify">
<?php print i18n_var("<a href='%1'>KMail</a>, KDE's powerful email client, features some improvements in the travel data extraction engine. It now supports UIC 918.3 and SNCF train ticket barcodes and Wikidata-powered train station location lookup. Support for multi-traveler itineraries was added, and KMail now has integration with the KDE Itinerary app.", "https://www.kde.org/applications/internet/kmail/");?>
</p>
<p align="justify">
<?php print i18n_var("<a href='%1'>Akonadi</a>, the personal information management framework, is now faster thanks to notification payloads and features XOAUTH support for SMTP, allowing for native authentication with Gmail.", "https://userbase.kde.org/Akonadi");?>
</p>

<h4 style="clear:both;" ><?php print i18n_var("Education");?></h4>
<p align="justify">
<?php print i18n_var("<a href='%1'>Cantor</a>, KDE's frontend to mathematical software, now saves the status of panels (\"Variables\", \"Help\", etc.) for each session separately. Julia sessions have become much faster to create.", "https://www.kde.org/applications/education/cantor/");?>
</p>
<p align="justify">
<?php print i18n_var("User experience in <a href='%1'>KAlgebra</a>, our graphing calculator, has been significantly improved for touch devices.", "https://www.kde.org/applications/education/kalgebra/");?>
</p>

<h4 style="clear:both;" ><?php print i18n_var("Utilities");?></h4>
<figure style="float: right; margin: 0px"><a href="spectacle1808.png"><img src="spectacle1808_small.jpg" width="250" height="184" /></a></figure><p align="justify">
<?php print i18n_var("Contributors to <a href='%1'>Spectacle</a>, KDE's versatile screenshot tool, focused on improving the Rectangular Region mode:", "https://www.kde.org/applications/graphics/spectacle/");?>
<ul>
<li><?php i18n("In Rectangular Region mode, there is now a magnifier to help you draw a pixel-perfect selection rectangle.");?></li>
<li><?php i18n("You can now move and resize the selection rectangle using the keyboard.");?></li>
<li><?php i18n("The user interface follows the user's color scheme, and the presentation of the help text has been improved.");?></li>
</ul>
</p>
<p align="justify">
<?php i18n("To make sharing your screenshots with others easier, links for shared images are now automatically copied to the clipboard. Screenshots can now be automatically saved in user-specified sub-directories.");?>
</p>
<p align="justify">
<?php print i18n_var("<a href='%1'>Kamoso</a>, our webcam recorder, was updated to avoid crashes with newer GStreamer versions.", "https://userbase.kde.org/Kamoso");?>
</p>

<h3 style="clear:both;" ><?php print i18n_var("Bug Stomping");?></h3>
<p align="justify">
<?php print i18n_var("More than 120 bugs have been resolved in applications including the Kontact Suite, Ark, Cantor, Dolphin, Gwenview, Kate, Konsole, Okular, Spectacle, Umbrello and more!");?>
</p>

<h3 style="clear:both;"><?php print i18n_var("Full Changelog");?></h3>

<p align="justify">
<?php print i18n_var("If you would like to read more about the changes in this release, <a href='%1'>head over to the complete changelog</a>. Although a bit intimidating due to its breadth, the changelog can be an excellent way to learn about KDE's internal workings and discover apps and features you never knew you had.", "fulllog_applications.php?version=".$version);?>
</p>

<h4>
<?php i18n("Spread the Word");?>
</h4>
<p align="justify">
<?php print i18n_var("Non-technical contributors play an important part in KDE's success. While proprietary software companies have huge advertising budgets for new software releases, KDE often depends on word of mouth."); ?>
</p>

<p align="justify">
<?php print i18n_var("There are many ways to support the KDE Applications 18.08 release: you can report bugs, encourage others to join the KDE Community, or <a href='%1'>support the non-profit organization behind the KDE Community</a>.", "https://relate.kde.org/civicrm/contribute/transact?reset=1&id=5"); ?>
</p>

<p align="justify">
<?php i18n("Help us spread the word on the social web. You can submit stories to news sites like Reddit, Facebook and Twitter; upload screenshots of your new set-up to services like Snapchat, Instagram and Google+; or create screencasts and upload them to YouTube, Blip.tv, and Vimeo, or stream them live over Twitch!"); ?>
</p>

<p align="justify">
<?php i18n("Remember to tag your posts and uploaded materials with the <i>KDE</i> moniker, as this makes them easier to find and gives the KDE Promo Team a way to analyze coverage for the KDE Applications 18.08 release."); ?>
</p>

<!-- // Boilerplate again -->

<h4>
  <?php i18n("Installing KDE Applications 18.08 Binary Packages");?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php i18n("Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 18.08 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.");?>
</p>

<p align="justify">
  <a name="package_locations"></a><em><?php i18n("Package Locations");?></em>.
  <?php i18n("For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_Applications/Binary_Packages'>Community Wiki</a>.");?>
</p>

<h4>
  <?php i18n("Compiling KDE Applications 18.08");?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php i18n("The complete source code for KDE Applications 18.08 may be <a href='http://download.kde.org/stable/applications/18.08.0/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications-18.08.0.php'>KDE Applications 18.08.0 Info Page</a>.");?>
</p>

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
