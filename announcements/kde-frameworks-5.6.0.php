<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.6.0");
  $site_root = "../";
  $release = '5.6.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
January 08, 2015. KDE today announces the release
of KDE Frameworks 5.6.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 60 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<?php i18n("
<p>Two new frameworks in this release: KPackage and NetworkManagerQt</p>

<h3>KActivities</h3>

<ul>
<li>Introducing the ActivityInfo QML object</li>
<li>Linking now supports special values alongside activity ids. Supported values:
<em>* - :global  - links to all activities
*</em> - :current - links to the current activity</li>
</ul>

<h3>KCoreAddons</h3>

<ul>
<li>KDirWatch can now watch /dev/shm as well (bug 314982)</li>
<li>KDELibs4Migration now always returns an absolute path</li>
</ul>

<h3>KCrash</h3>

<ul>
<li>Fix build on FreeBSD</li>
</ul>

<h3>Kdelibs4Support</h3>

<ul>
<li>Add Lithuania to the list of Euro countries</li>
</ul>

<h3>KdeSU</h3>

<ul>
<li>Fix build on OSX and on FreeBSD</li>
</ul>

<h3>KHtml</h3>

<ul>
<li>6 bug fixes forward-ported from kdelibs4.</li>
</ul>

<h3>KIO</h3>

<ul>
<li>Add support for .hidden files, for users to hide some files from views. (feature 246260)</li>
<li>New method KRun::runService, like KRun::run but returns the PID of the newly started process.</li>
<li>kioexec: fixed many porting bugs, to make it work again</li>
<li>KRun::run: fixed porting bug, to make it work again in the case where KProcess is used directly</li>
<li>KRun: make klauncher runtime dependency optional</li>
<li>Fix compilation on MSVC</li>
<li>Performance: decrease memory consumption of UDSEntry</li>
<li>Actions in popupmenus: in case of multiple mimetypes selected, services that support all mimetypes
are now added to the menu.</li>
<li>New job: KIO::DropJob <em>KIO::drop(QDropEvent</em> ev, QUrl destUrl). Replaces KonqOperations::doDrop.</li>
<li>Restart directory watcher after a failed delete operation</li>
<li>Fix false warning about X-KDE-Protocols unused in desktop files without that field.</li>
<li>Merge various settings modules (KCMs) related to kio, into the kio framework.</li>
<li>When copying/moving out the trash, make the files writable.</li>
<li>KIO::file_move now does the chmod on the destination file before emitting result.</li>
</ul>

<h3>KNotifications</h3>

<ul>
<li>Remove NotifyBySound. NotifyByAudio implements the \"Sound\" notification already</li>
<li>Fix crash accessing dangling pointer in NotifyByPopup</li>
</ul>

<h3>KRunner</h3>

<ul>
<li>Do not detect anything with a '.' as a NetworkLocation (porting bug, bug 340140).
One can also uses a decimal point in a calculator.</li>
</ul>

<h3>KService</h3>

<ul>
<li>Fix build on MSVC.</li>
</ul>

<h3>KTextEditor</h3>

<ul>
<li>Fix build on MSVC.</li>
<li>vimode bugfixes</li>
<li>add syntax file for Oracle PL/SQL</li>
<li>ppd highlighting: better support for multiline values</li>
</ul>

<h3>KWidgetsAddons</h3>

<ul>
<li>Add runtime style element extension convenience functions for widgets: KStyleExtensions</li>
</ul>

<h3>KWindowSystem</h3>

<ul>
<li>Add OnScreenDisplay window type</li>
<li>Fix build on FreeBSD</li>
</ul>

<h3>Plasma-framework</h3>

<ul>
<li>Let month menu navigate in current year (bug 342327)</li>
<li>Expose new OnScreenDisplay window type in Dialog</li>
<li>Migrate Plasma::Package to KPackage</li>
<li>Fix labels not picking up changes to font settings at runtime</li>
<li>Fix text not properly updating its color when switching themes (especially dark&lt;-->light)</li>
<li>Fix placeholder text in textfields being too strong when using a light theme</li>
<li>Set visibility on mainItem to match Dialog</li>
<li>Load IconItem immediately upon componentComplete()</li>
<li>Use the same text colour for comboboxes as buttons</li>
<li>Performance improvements (less config-file reparsing, use shared config...)</li>
<li>roundToIconSize(0) now returns 0</li>
<li>Give undo notifications a title</li>
</ul>

<h3>Solid</h3>

<ul>
<li>Enable fstab and upower backends on FreeBSD</li>
<li>Power: Add aboutToSuspend signal</li>
</ul>

<h3>Buildsystem changes</h3>

<ul>
<li><p>ECM's KDEInstallDirs now supports KDE_INSTALL_* variables, which should
be used in preference to the CMAKE_INSTALL_* variables or the older
KDELibs4-compatible variables. The older forms of the variables are still
supported (and kept in sync) unless KDE_INSTALL_DIRS_NO_DEPRECATED
or KDE_INSTALL_DIRS_NO_CMAKE_VARIABLES are set to TRUE. See the
documentation for more details.</p></li>
<li><p>Add COMPATIBILITY argument to ecm_setup_version().
Lots of libraries will want to use SameMajorVersion to make sure
searching for version 1 of a library doesn't give you version 2, for
example.</p></li>
<li><p>Fix ECMQueryQmake when Qt5Core is missing.</p></li>
</ul>

<h3>Additional buildsystem changes in Extra-Cmake-Modules 1.6.1</h3>

<ul>
<li><p>Fix building projects that use both GNUInstallDirs and KDEInstallDirs in
different subdirectories by not unsetting cache variables in KDEInstallDirs.</p></li>
<li><p>Fix KDE_INSTALL_TARGETS_DEFAULT_ARGS value on OSX.</p></li>
</ul>

<h3>Frameworkintegration</h3>

<ul>
<li>Fix handling of palette change events (bug 336813)</li>
</ul>
");?>

<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%1/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%2.php'>KDE Frameworks %1 Info Page</a>.", "5.6", $release);?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.2");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
