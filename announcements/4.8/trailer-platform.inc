
<h3>
<a href="platform.php">
KDE Platform 4.8 Enhances Interoperability, Introduces Touch-Friendly Components
</a>
</h3>

<p>
<a href="platform.php">
<img src="images/platform.png" class="app-icon" alt="The KDE Development Platform 4.8"/>
</a>

KDE Platform provides the foundation for KDE software. KDE software is more stable than ever before. In addition to stability improvements and bugfixes, Platform 4.8 provides better tools for building fluid and touch-friendly user interfaces, integrates with other systems' password saving mechanisms and lays the base for more powerful interaction with other people using the new KDE Telepathy framework. For full details, read the <a href="platform.php">KDE Platform 4.8 release announcement</a>.
<br /><br />
<br /><br />

</p>
