<?php
  $page_title = "KDE 4.0 ਐਪਲੀਕੇਸ਼ਨ";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";
?>
<p>
Also available in:
<a href="applications-it.php">English</a>
<a href="applications-it.php">Italian</a>
<a href="applications-pa.php">Punjabi</a>
<a href="applications-sl.php">Slovenian</a>
</p>


<h2>ਡਾਲਫਿਨ ਫਾਇਲ-ਮੈਨੇਜਰ</h2>
<p>
ਡਾਲਫਿਨ KDE4 ਦਾ ਨਵਾਂ ਫਾਇਲ ਮੈਨੇਜਰ ਹੈ। ਆਪਣੀਆਂ ਫਾਇਲਾਂ ਡਾਲਫਿਨ ਨਾਲ ਵੇਖੋ, ਰੱਖੋ, ਖੋਲ੍ਹੋ, ਕਾਪੀ ਕਰੋ ਅਤੇ ਭੇਜੋ।
ਡਾਲਫਿਨ ਵਰਤਣ ਲਈ ਸੌਖ ਅਤੇ ਕੋਨਕਿਊਰੋਰ ਫਾਇਲ-ਮੈਨੇਜੇਮੈਂਟ ਭਾਗ ਨੂੰ ਬਦਲਣ ਲਈ ਹੈ, ਜੋ ਕਿ KDE3 ਵਿੱਚ ਵਰਤਿਆ
ਗਿਆ ਸੀ। ਜਦੋਂ ਕੋਨਕਿਊਰੋਰ ਹਾਲੇ ਵੀ ਫਾਇਲ ਮੈਨੇਜਰ ਵਜੋਂ ਵਰਤਿਆ ਜਾ ਸਕਦਾ ਹੈ, ਅਤੇ ਅਸਲ ਵਿੱਚ ਫਾਇਲ-ਵੇਖੋ ਫੰਕਸ਼ਨ
ਡਾਲਫਿਨ ਨਾਲ ਸਾਂਝਾ ਕਰਦਾ ਹੈ, KDE ਟੀਮ ਨੇ ਨਵੀਂ ਐਪਲੀਕੇਸ਼ਨ ਨੂੰ ਚਾਲੂ ਕਰਨ ਦਾ ਫੈਸਲਾ ਲਿਆ ਹੈ, ਜੋ ਕਿ ਫਾਇਲ ਸਿਸਟਮ
ਲਈ ਖਾਸ ਤੌਰ ਉੱਤੇ ਤਿਆਰ ਕੀਤੀ ਗਈ ਹੈ: ਡਾਲਫਿਨ
</p>
<?php
    screenshot("dolphin-splitview_thumb.jpg", "dolphin-splitview.jpg",
        "center", "ਡਾਲਫਿਨ ਨਾਲ ਫਾਇਲਾਂ ਕਾਪੀਆਂ ਕਰਨੀਆਂ");
?>

<p>
ਵੱਡੀ ਗਿਣਤੀ ਵਿੱਚ ਚਿੱਤਰ ਰੱਖਣ ਵਾਲੀਆਂ ਡਾਇਰੈਕਟਰੀਆਂ ਲਈ, ਡਾਲਫਿਨ ਦੇ ਟੂਲਬਾਰ ਉੱਤੇ ਝਲਕ ਬਟਨ ਦੱਬੋ, ਅਤੇ ਮੌਜੂਦਾ 
ਡਾਇਰੈਕਟਰੀ ਵਿੱਚ ਮੌਜੂਦ ਫਾਇਲਾਂ ਦੀ ਝਲਕ ਲਵੋ। ਡਾਇਰੈਕਟਰੀਆਂ ਵਿੱਚ ਤੇਜ਼ੀ ਨਾਲ ਨੇਵੀਗੇਟ ਕਰਨ ਲਈ,  ਫਾਇਲ
ਝਲਕ ਦੇ ਉੱਤੇ ਸੱਜੇ ਪਾਸੇ ਕਲਿੱਕ ਕਰੋ। ਅੱਗੇ ਤੀਰ ਨੂੰ ਕਲਿੱਕ ਕਰਨ ਬਾਅਦ, ਤੁਸੀਂ ਅਧੀਨ ਸਬ-ਡਾਇਰੈਕਟਰੀਆਂ
ਵਿੱਚ ਆਸਾਨੀ ਨਾਲ ਜਾ ਆ ਸਕਦੇ ਹੋ। ਨਾਲ-ਨਾਲ ਝਲਕ ਨਾਲ ਡਾਇਰੈਕਟਰੀਆਂ ਵਿੱਚ ਨਕਲ ਕਰਨਾ
ਸੌਖਾ ਹੋ ਗਿਆ ਹੈ, ਇਸ ਲਈ 'ਝਲਕ ਵੰਡੋ' ਬਟਨ ਦੱਬੋ। ਹਾਲਾਂਕਿ ਡਾਲਫਿਨ ਇੱਕ ਖਾਸ ਡਾਇਰੈਕਟਰੀ
ਦੀ ਸੈਟਿੰਗ ਯਾਦ ਰੱਖਦਾ ਹੈ, ਤੁਸੀਂ ਆਪਣੀ ਲੋੜ ਮੁਤਾਬਕ ਡਿਫਾਲਟ ਸੈਟਿੰਗ ਨੂੰ "ਸੈਟਿੰਗ | ਡਾਲਫਿਨ ਸੰਰਚਨਾ"
ਮੇਨੂ ਰਾਹੀਂ ਸੈੱਟ ਕਰ ਸਕਦੇ ਹੋ।
</p>

<p>
ਖੱਬੇ ਪਾਸੇ, ਡਾਲਫਿਨ ਦੀ ਸਾਈਡਬਾਰ ਤੁਹਾਨੂੰ ਵਰਤੇ ਜਾਂ ਟਿਕਾਣਾ, ਜਿਨ੍ਹਾਂ ਨੂੰ "ਥਾਵਾਂ" ਕਹਿੰਦੇ ਹਨ, ਲਈ ਤੁਰੰਤ ਪਹੁੰਚ ਦਿੰਦੀ ਹੈ।
ਇੱਕ ਫੋਲਡਰ ਨੂੰ ਸਾਈਡਬਾਰ ਵਿੱਚ ਡਰੈਗ ਕਰ ਦਿਓ ਅਤੇ ਤੁਸੀਂ ਇਸ ਨੂੰ ਤੁਰੰਤ ਵਰਤ ਸਕਦੇ ਹੋ ਡਾਲਫਿਨ ਨਾਲ, ਕਿੱਕਆਫ ਥਾਂ ਟੈਬ
ਨਾਲ ਅਤੇ ਆਪਣੇ ਸਭ ਐਪਲੀਕੇਸ਼ਨਾਂ ਦੇ "ਫਾਇਲ ਖੋਲ੍ਹੋ" ਡਾਈਲਾਗ ਨਾਲ।
</p>
<?php
    screenshot("dolphin-groups_thumb.jpg", "dolphin-groups.jpg", "center",
                "ਡਾਲਫਿਨ ਨਾਲ ਫਾਇਲਾਂ ਗਰੁੱਪ ਵਿੱਚ ਵੇਖਣੀਆਂ");
?>
<p>
ਜਾਣਕਾਰੀ ਸਾਇਡਬਾਰ ਚੁਣੀ ਫਾਇਲ ਲਈ ਕੁਝ ਹੋਰ ਮਹੱਤਵਪੂਰਨ ਜਾਣਕਾਰੀ ਨਾਲ ਝਲਕ ਦਿੰਦੀ ਹੈ। ਤੁਸੀਂ ਇਸ ਨੂੰ 
ਆਪਣੀਆਂ ਫਾਇਲਾਂ ਲਈ ਟਿੱਪਣੀਆਂ ਲਿਖਣ ਅਤੇ ਸੌਖੀ ਤਰ੍ਹਾਂ ਲੜੀਬੱਧ ਕਰਨ ਵਾਸਤੇ ਟੈਗ ਵੀ ਦੇ ਸਕਦੇ ਹੋ।
ਫਾਇਲਾਂ ਨੂੰ ਸਾਈਜ਼, ਟਾਈਪ ਜਾਂ ਹੋਰ ਗੁਣਾਂ ਮੁਤਾਬਕ ਲੜੀਬੱਧ ਕਰਨ ਲਈ "ਵੇਖੋ" | "ਗਰੁੱਪ 'ਚ ਵੇਖੋ"
ਚੋਣ ਯੋਗ ਕਰੋ।
</p>

<h2>ਓਕੁਲਾਰ ਅਤੇ ਜੀਵਿਨਵਿਊ: ਆਪਣੇ ਡੌਕੂਮੈਂਟ ਅਤੇ ਚਿੱਤਰ ਵੇਖੋ</h2>

<?php
    screenshot("gwenview_thumb.jpg", "gwenview.jpg", "center",
                "ਜੀਵਿਨਵਿਊ ਨਾਲ ਆਪਣੀਆਂ ਤਸਵੀਰਾਂ ਵੇਖੋ");
?>
<p>
<strong>ਜੀਵਿਨਵਿਊ</strong> KDE ਦਾ ਚਿੱਤਰ ਦਰਸ਼ਕ ਹੈ। ਭਾਵੇਂ ਕਿ ਇਹ KDE 3 ਵਿੱਚ ਵੀ ਉਪਲੱਬਧ ਸੀ,
ਪਰ KDE4 ਵਿੱਚ ਇਸ ਦਾ ਇੰਟਰਫੇਸ ਸਧਾਰਨ ਬਣਾਇਆ ਗਿਆ, ਤੁਹਾਡੇ ਚਿੱਤਰਾਂ ਦੇ ਭੰਡਾਰ ਨੂੰ ਤੇਜ਼ੀ ਨਾਲ ਵੇਖਣ ਲਈ
ਠੀਕ ਬਣਾਇਆ ਗਿਆ ਹੈ। ਜੀਵਿਨਵਿਊ ਨੂੰ ਚਿੱਤਰ ਵੇਖਾਉਣ ਲਈ ਵੀ ਵਰਤਿਆ ਜਾਂਦਾ ਹੈ। ਇੱਕ ਬਹੁਤ ਹੀ ਵਧੀਆ
ਪੂਰੀ ਸਕਰੀਨ ਇੰਟਰਫੇਸ ਦਿੱਤਾ ਜਾਂਦਾ ਹੈ, ਜਿਸ ਨੂੰ ਤੁਸੀਂ ਆਪਣੇ ਚਿੱਤਰਾਂ ਨੂੰ ਸਲਾਇਡ-ਸ਼ੋ ਵੇਖਾਉਣ ਲਈ ਇਸਤੇਮਲਾ
ਕਰ ਸਕਦੇ ਹੋ।
</p>
<?php
	screenshot("okular_thumb.jpg", "okular.jpg", "center", "ਓਕੁਲਾਰ KDE 4.0 ਦਾ ਤੇਜ਼ ਅਤੇ ਪਰਭਾਵੀ ਡੌਕੂਮੈਂਟ ਰੀਡਰ ਹੈ");
?><p>
<strong>ਓਕੁਲਾਰ</strong>  KDE4 ਦਾ ਡੌਕੂਮੈਂਟ ਦਰਸ਼ਕ ਹੈ। ਇਹ ਕਈ ਫਾਰਮੈਟਾਂ ਲਈ ਸਹਾਇਕ ਹੈ,
ਜਿਸ ਵਿੱਚ PDF ਫਾਇਲਾਂ ਤੋਂ ਲੈਕੇ ਓਪਨ-ਡੌਕੂਮੈਂਟ ਫਾਇਲਾਂ ਵੀ ਸ਼ਾਮਲ ਹਨ। ਓਕੁਲਾਰ ਫਾਇਲਾਂ ਨੂੰ ਕੇਵਲ ਪੜ੍ਹਨ
ਲਈ ਹੀ ਨਹੀਂ ਹੈ। ਨਵਾਂ "ਰਵਿਊ" ਫੀਚਰ ਤੁਹਾਨੂੰ ਡੌਕੂਮੈਂਟਾਂ ਵਿੱਚ ਹਵਾਲੇ ਦੇਣ ਲਈ ਸਹਾਇਕ ਹੈ। F6 ਦੱਬੋ, ਇੱਕ
ਪੈਨ ਚੁਣੋ ਅਤੇ ਆਪਣੇ ਡੌਕੂਮੈਂਟ ਵਿੱਚ ਟੈਕਸਟ ਮਾਰਕ ਕਰੋ, ਉਹਨਾਂ ਵਿੱਚ ਨੋਟ ਸ਼ਾਮਲ ਕਰੋ ਅਤੇ ਡੌਕੂਮੈਂਟ ਵਿੱਚ
ਸ਼ੈਕਸ਼ਨ ਉੱਤੇ ਟਿੱਪਣੀਆਂ ਦਿਓ। ਓਕੁਲਾਰ KPDF, KDE3 ਦਾ PDF ਦਰਸ਼ਕ, ਉੱਤੇ ਅਧਾਰਿਤ ਹੈ। ਓਕੁਲਾਰ 
ਵਰਤਣ ਸਹੂਲਤ ਲਈ ਫੋਕਸ ਹੈ ਅਤੇ ots ਦੇ ਫਾਰਮੈਟ ਲਈ ਸਹਾਇਕ ਹੈ।
</p>


<h2>ਸਿਸਟਮ ਸੈਟਿੰਗ</h2>
<p>
 ਸਿਸਟਮ ਸੈਟਿੰਗ KDE4 ਦਾ ਕੰਟਰੋਲ ਸੈਂਟਰ ਹੈ। ਇੱਥੇ ਤੁਸੀਂ ਆਪਣੀਆਂ ਐਪਲੀਕੇਸ਼ਨਾਂ ਦੀ ਦਿੱਖ, ਨਿੱਜੀ ਸੈਟਿੰਗ ਸੰਰਚਨਾ,
ਨੈੱਟਵਰਕ ਸੈਟਿੰਗ ਅਤੇ ਆਪਣੀ ਕੰਪਿਊਟਰ ਪਰਸ਼ਾਸ਼ਨ ਬਦਲ ਸਕਦੇ ਹੋ।
</p>
<?php
    screenshot("systemsettings-appearance_thumb.jpg",
     "systemsettings-appearance.jpg", "center", "ਡੈਸਕਟਾਪ ਦੀ ਦਿੱਖ ਸਿਸਟਮ ਸੈਟਿੰਗ ਵਿੱਚ ਬਦਲੋ");
?>

<p>
"ਦਿੱਖ" ਨੂੰ ਸਭ ਐਪਲੀਕੇਸ਼ਨਾਂ ਵਾਸਤੇ ਰੰਗ ਸਕੀਮ ਬਦਲਣ ਲਈ ਖੋਜੋ, ਜੋ ਕਿ ਵੱਧ ਕਨਟਰਾਸਟ ਦੇਣ ਜਾਂ ਤੁਹਾਡੇ ਆਪਣੇ
ਸੁਆਦ ਮੁਤਾਬਕ ਉਪਲੱਬਧ ਕਰਵਾਣ ਲਈ ਹੈ। ਇੱਥੇ ਤੁਸੀਂ ਆਪਣੇ ਐਪਲੀਕੇਸ਼ਨਾਂ ਲਈ ਫੋਟਾਂ ਦਾ ਸਾਈਜ਼ 
ਅਤੇ ਫੇਸ ਬਦਲ ਸਕਦੇ ਹੋ। ਭਾਵੇਂ KDE4 ਇੱਕ ਢੁੱਕਵੇਂ ਅਤੇ ਵਧੀਆ ਡਿਫਾਲਟ ਆਰਟਵਰਕ ਸੈੱਟ ਨਾਲ ਹੈ, ਪਰ
ਇਹ ਹਰੇਕ ਵਿਅਕਤੀ ਦੇ ਟੇਸਟ ਮੁਤਾਬਕ ਨਹੀਂ ਹੋ ਸਕਦਾ ਹੈ। ਸਿਸਟਮ ਸੈਟਿੰਗ ਵਿੱਚ ਦਿੱਖ ਸ਼ੀਟਾਂ ਤੁਹਾਨੂੰ ਆਪਣੇ
ਡੈਸਕਟਾਪ ਦੀ ਦਿੱਖ ਪੂਰੀ ਤਰ੍ਹਾਂ ਬਦਲਣ ਲਈ ਸਹਾਇਕ ਹੈ।
</p>
<?php
	screenshot("solid_thumb.jpg", "solid.jpg", "center", "ਹਾਰਡਵੇਅਰ ਐਂਟੀਗਰੇਸ਼ਨ ਸਾਲਡ ਫਰੇਮਵਰਕ ਦੇ ਰਾਹੀ");
?>
<p>
ਸਿਸਟਮ ਸੈਟਿੰਗ ਵਿੱਚ ਉਹ ਟੂਲ ਵੀ ਸ਼ਾਮਲ ਹਨ, ਜਿਸ ਨਾਲ ਅਸਲ ਵਿੱਚ ਓਪਰੇਟਿੰਗ ਸਿਸਟਮ ਨੂੰ ਕੰਟਰੋਲ ਕੀਤਾ ਜਾ ਸਕਦਾ ਹੈ। ਸਾਲਡ, ਜੋ ਕਿ ਪਾਵਰ
ਮੈਨੇਜੇਮੈਂਟ, ਹਾਟ-ਪਲੱਗ ਕੀਤੇ ਜੰਤਰ ਅਤੇ ਨੈੱਟਵਰਕ ਕੁਨੈਕਟਵਿਟੀ ਆਦਿ ਚੀਜ਼ਾਂ ਦਾ ਧਿਆਨ ਰੱਖਦਾ ਹੈ, ਜੋ ਕਿ ਅੰਦਰੂਨੀ ਰੂਪ ਵਿੱਚ ਭਰੋਸੇਯੋਗ ਢਾਂਚੇ
ਦੀ ਵਰਤੋਂ ਕਰਦਾ ਹੈ, ਜਿਸ ਨੂੰ ਅਸਲ ਓਪਰੇਟਿੰਗ ਸਿਸਟਮ ਦੇ ਮੁਤਾਬਕ ਸੈਟਅੱਪ ਕੀਤਾ ਜਾ ਸਕਦਾ ਹੈ।
</p>

<h2>ਕਨਸੋਲ</h2>
<p>
KDE 4.0 ਲਈ ਅਜੇਹਾ ਐਪਲੀਕੇਸ਼ਨਾਂ ਦੀ ਉਦਾਹਰਨ ਵਿੱਚ ਇੱਕ ਟਰਮੀਨਲ ਈਮੂਲੇਸ਼ਨ ਪਰੋਗਰਾਮ ਕਨਸੋਲ ਹੈ, ਜਿਸ ਵਿੱਚ ਵੱਡੇ ਪੱਧਰ ਉੱਤੇ
 ਸੁਧਾਰ ਕੀਤੇ ਗਏ ਹਨ। ਸੰਰਚਨਾ ਡਾਈਲਾਗ ਨੂੰ ਵਰਤਣ ਲਈ ਸੌਖਾ ਬਣਾ ਦਿੱਤਾ ਗਿਆ ਹੈ, ਕਨਸੋਲ ਦਾ ਫੀਚਰ-ਸੈੱਟ ਘਟਾਇਆ ਨਹੀਂ ਗਿਆ ਹੈ।
ਹੋਰ ਸੁਧਾਰਾਂ ਵਿੱਚ ਸ਼ਾਮਲ ਹਨ:
</p>
<?php
	screenshot("konsole_thumb.jpg", "konsole.jpg", "center", "ਕਨਸੋਲ, KDE ਦਾ ਟਰਮੀਨਲ ਏਮੂਲੇਟਰ");
?><p>
<ul>
	<li>
		ਯੂਜ਼ਰ ਇੰਟਰਫੇਸ ਨੂੰ ਮੁੜ-ਤਿਆਰ ਕੀਤਾ ਗਿਆ ਹੈ ਅਤੇ ਸਾਫ਼ ਕਰ ਦਿੱਤਾ ਗਿਆ ਹੈ, ਕਈ ਕੀਬੋਰਡ ਸ਼ਾਰਟਕੱਟਾਂ ਨੂੰ ਕਨਸੋਲ ਨੂੰ
                ਹੋਰ ਵੱਧ ਫਾਇਦੇਮੰਦ ਬਣਾਉਣ ਲਈ ਸ਼ਾਮਲ ਕੀਤਾ ਗਿਆ ਹੈ।
	</li>
	<li>
		ਇੱਕ ਵੰਡ (split) ਝਲਕ ਨੂੰ ਦਿੱਤਾ  ਗਿਆ ਹੈ ਤਾਂ ਯੂਜ਼ਰ ਕਨਸੋਲ ਨੂੰ ਵੱਖ ਵੱਖ ਏਰੀਏ ਵਿੱਚ ਵੰਡ ਸਕੇ। ਇਸ ਨਾਲ ਪਹਿਲੀ 
                ਆਉਟਪੁੱਟ ਦੀ ਸਕੈਨਿੰਗ ਅਤੇ ਨਿਗਰਾਨੀ ਸੌਖੀ ਹੋ ਜਾਂਦੀ ਹੈ।
	</li>
	<li>
		ਟੈਬਾਂ ਦੇ ਟਾਇਟਲ ਹੁਣ ਆਟੋਮੈਟਿਕ ਹੀ ਬਦਲ ਜਾਂਦੇ ਹਨ, ਜਿਸ ਨਾਲ ਉਨ੍ਹਾਂ ਦੀ ਪਛਾਣ ਸੌਖੀ ਹੋ ਗਈ ਹੈ।
	</li>
	<li>
		Incremental search results are highlighted and a search bar now make for a
		smoother user experience. Press CTRL+SHIFT+F in a terminal window to search
		through the buffer.
	</li>
	<li>
		ਵੱਡੇ ਟਰਮੀਨਲ ਡਿਸਪਲੇਅ ਆਉਟਪੁੱਟ ਦੀ ਵੱਡੀ ਮਾਤਰਾ ਵਿੱਚ ਖੋਜ ਲਈ ਕਾਰਗੁਜ਼ਾਰੀ ਵਾਸਤੇ ਸੁਧਾਰ ਕੀਤਾ ਗਿਆ ਹੈ।
	</li>
	<li>
		Terminal windows have real transparency that can be enabled in the Appearance tab.
		(Note that you might need to start the first Konsole window running with 
		"konsole --enable-transparency".)
	</li>
</ul>
</p>
<p>
ਕਨਸੋਲ ਦੇ ਹੋਰ ਫੀਚਰਾਂ ਨੂੰ ਇਸ ਦੇ
<a href="http://websvn.kde.org/branches/KDE/4.0/kdebase/apps/konsole/CHANGES-4.0?view=markup">
changelog</a> ਵਿੱਚ ਲੱਭਿਆ ਜਾ ਸਕਦਾ ਹੈ।
</p>

<h2>ਹੋਰ ਐਪਲੀਕੇਸ਼ਨ</h2>
<p>
The release of KDE 4.0 also brings good news for users of extragear applications. Applications 
in the extragear modules normally take care of their own releases, but from now on it is also 
possible to follow the main KDE Release Schedule. The release team therefore has extended their 
duties and will provide tarballs for applications which 
<a href="http://techbase.kde.org/Projects/extragearReleases">want to join</a> this for every KDE 
release. This first set of tarballs already contain some well known extragear applications, 
like: <a href="http://techbase.kde.org/Projects/Summer_of_Code/2007/Projects/KAider">Kaider</a>, 
<a href="http://ktorrent.org/">KTorrent[3]</a>, 
<a href="http://ktown.kde.org/kphotoalbum/">ਕੇ-ਫੋਟੋ-ਐਲਬਮ</a> and 
<a href="http://rsibreak.org/">RSIBreak</a>. 
ਇਹਨਾਂ ਐਪਲੀਕੇਸ਼ਨਾਂ ਲਈ KDE4 ਬਦਲਣਾ ਪੂਰਾ ਨਹੀਂ ਹੋਇਆ ਹੋ ਸਕਦਾ ਹੈ, ਪਰ ਉਹ ਹੁਣ ਬੱਗ ਰਿਪੋਰਟ ਲੈਣੀਆਂ ਪਸੰਦ ਕਰਨਗੇ।
</p>

<p>
<a href="educational.php">ਅਗਲਾ ਪੇਜ਼: KDE 4.0 ਦੇ ਵਿਦਿਅਕ ਸਾਫਟਵੇਅਰ</a>
</p>
<?php
  include("footer.inc");
?>

