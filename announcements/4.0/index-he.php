<?php

  $page_title = "KDE 4.0 שוחררה";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";
?>

<!-- please, do not remove this span, otherwise this page will look funky in  LTR pages -->
<div dir="RTL" align="RIGHT">

<p>לשיחרור מיידי</p>
כמו כן זמין בשפות הבאות:

<a href="index.php">אנגלית</a>
<a href="index-bn_IN.php">בנגלית</a>
<a href="index-ca.php">קטלאנית</a>
<a href="http://www.kdecn.org/announcements/4.0/index.php">סינית</a>
<a href="index-cz.php">צ'כית</a>
<a href="index-nl.php">הולדנית</a>
<a href="http://fr.kde.org/announcements/4.0/index.php">צרפתית</a>
<a href="http://www.kde.de/infos/ankuendigungen/40/">גרמנית</a>
<a href="index-sv.php">גוג'ראטית</a>
<a href="index-fa.php">פרסית</a>
<a href="index-hi.php">הינדית</a>
<a href="index-it.php">איטקלית</a>
<a href="index-lv.php">לאטבית</a>
<a href="index-ml.php">מליילם</a>
<a href="index-mr.php">מרטהי</a>
<a href="index-pl.php">פולנית</a>
<a href="index-pa.php">פונג'אבית</a>
<a href="index-pt_BR.php">פורטוגזית</a>
<a href="index-ro.php">רומנית</a>
<a href="index-ru.php">רוסית</a>
<a href="index-sl.php">סלובקית</a>
<a href="index-es.php">ספרדית</a>
<a href="index-sv.php">שוודית</a>
<a href="index-ta.php">Tamil</a>

<h3 align="center" dir="RTL" align="RIGHT">
   פרוייקט KDE משחרר את הגרסה הרביעית של שולחן העבודה המתקדם החופשי.
</h3>
<p align="justify">
  <strong>
    עם הגרסה הרביעית, הקהילה של KDE פותחת את העידן של KDE 4.
  </strong>
</p>
<p align="justify">
ינואר 11, 2008, האינטרנט
</p>

<p>
קהילת KDE שמחה להכריז על זמינות מיידית של <a href="http://www.kde.org/announcements/4.0/">KDE 4.0.0</a>.
גרסה חדשה זו מסמנת את סופו של מחזור פיתוח ארוך ואינטנסיבי המבשר את תחילת עידן KDE 4.
</p>

<?php
    screenshot("desktop_thumb.jpg", "desktop.jpg", "center", "שולחן העבודה של KDE 4.0 ");
?>

<p>
ה<strong>ספריות</strong> של KDE 4 שופרו בצורה משמעותית בכל התחומים. תשתית המולטימדיה Phonon מספקת 
תמיכת מולטימדיה ללא תלות בפלטפורמה לכל יישומי KDE, תשתית אינטגרציית החומרה Solid מקלה על העבודה עם התקנים 
(ניידים) ומספקת כלים לניהול הספק משופר. 
<p />
<p>
שולחן העבודה קיבל יכולות חדשות רבות. Plasma מספק ממשק חדש לסביבת העבודה, כולל לוח (פאנל), תפריט, 
יישומונים על שולחן העבודה (widgets) ומחוונים שונים. KWin, מנהל החלונות, ‫תומך כעת באפקטים
גרפיים מתקדמים המקלים על העבודה עם החלונות שלך.
<p />
<p>

<strong>ישומי</strong> KDE  רבים שופרו. תצוגה עדכנית תוך שימוש בגרפיקה וקטורית, שינויים בספריות התשתית, 
שיפורים בממשק המשתמש, תכונות חדשות ואף יישומים חדשים, כל דבר העולה על הדעת, כבר נמצא ב־KDE 4.0.
‏Okular, מציג המסמכים החדש ו־Dolphin, מנהל הקבצים החדש, הם רק שניים מהיישומים המנצלים את הטכנולוגיות החדשות של KDE 4.0.
<p />
<p>
<img src="images/oxybann.png" align="left" />
‫הצוות <strong>האומנותי</strong> של Oxygen סיפק משב רוח רענן לשולחן העבודה. כמעט 
כל הצד החזותי בשולחן העבודה וביישומים קיבל מתיחת פנים. יופי ועקביות הם שניים מהיסודות של Oxygen.
</p>

<h3 dir="RTL" align="RIGHT">שולחן עבודה</h3>
<ul dir="RTL" align="RIGHT">
	<li dir="RTL" align="RIGHT">
	  ‏Plasma הוא יישום שולחן העבודה החדש. Plasma מספק לוח, תפריט ואמצעים אינטואיטיבים נוספים
	  כממשק עם שולחן העבודה והיישומים.
	</li>
	<li  dir="RTL" align="RIGHT">
	‏Kwin, מנהל החלונות הוותיק של KDE, כעת מספקת יכולות שזירה (composition) חדשות. גרפיקה מואצת על ידי חומרה
	גורמת לחווית משתמש טובה יותר אינטראקציה טובה יותר עם חלונות אחרים.
	</li>
	<li dir="RTL" align="RIGHT">
	‏Oxygen היא ערכת הנושא של KDE 4.0. זוהי ערכת נושא עקבית, נעימה לעין ונאה למראה.
	</li>
</ul>
למד עוד אודות שהשולחן עבודה החדש <a href="desktop.php">במדריך החזותי</a>.

<h3 dir="RTL" align="RIGHT">יישומים</h3>
<ul>
	<li>
	‏Konqueror הוא דפדפן הרשת הוותיק של KDE. הוא קליל, מותאם היטב לסביבת העבודה של KDE ותומך 
	בתקני הרשת החדשים כגון CSS 3.
	</li>
	<li>
	‏‫‏Dolphin הוא מנהל הקבצים החדש של KDE. ‏ Dolphin, אשר פותח עם דגש על שימושיות, קל לשימוש ועם זאת רב עצמה.
	</li>
	<li>
‏System Settings, הוא מרכז בקרה החדש. מנתר המערכת KSysGuard מאפשר ניטור ושליטה פשוטים 
	של משאבי המערכת ופעילותה.
	</li>
	<li>
	‏Okular, מציג המסמכים של 4 KDE תומך בתסדירים רבים. Okular הוא אחד מיישומי KDE 4 אשר שופרו בעזרת
	שיתוף הפעולה של  <a href="http://openusability.org">OpenUsability Project</a>.
	</li>
	<li>
	יישומים חינוכיים היו בין היישומים הראשונים אשר הוסבו ופותחו בעזרת הטכנולוגיות של KDE 4. היישום Kalzium,
	טבלה מחזורית גראפית של יסודות כימיים, ו־Marble Desktop Globe הם רק שניים מהפנינים שבין היישומים החינוכיים.
	קרא עוד אודות הישומיים החינוכיים ב<a href="education.php">מדריך החזותי</a>.
	</li>
	<li>
	משחקי KDE גם עודכנו. משחקים כגון KMines (משחק דמוי שולה המוקשים) ו־KPat (משחק קלפים) קיבלו מתיחת פנים.
	הודות לגרפיקה וקטורית ויכולות גרפיות חדשות המשחקים כעת לא תלויים ברזולוציית מסך.
	</li>
</ul>
יישומים נוספים מוקדש להם הסברים מפורטים יותר ב<a href="education.php">מדריך החזותי</a>.

<?php
screenshot("dolphin-systemsettings-kickoff_thumb.jpg", "dolphin-systemsettings-kickoff.jpg", "center", 	"מנהל הקבצים, System Settings והתפריט בפעולה" );
?>

<h3 dir="RTL" align="RIGHT">ספריות</h3>
<p>
<ul>
	<li>
	‏Phonon מספקת שירותי מולטימדיה כגון ניגון שירים וווידאו. פנימית, Phonon משתמש בתשתיות שונות הניתנות
	להחלפה בזמן ריצה. מנוע ברירת המחדל ב־KDE 4.0 הוא Xine המספק תמיכה מצויינת בתסדירים שונים. Phonon גם מאפשר
	למשתמש לבחור את ההתקנים אליהם יועבר הפלט, בהתאם לסוג המולטימדיה.
	
	</li>
	<li>
	מערכת האינטרגרציה עם חומרה, Solid, עוזרת בהתממשקות של יישומי KDE מול התקנים ניידים ומקובעים 
	אל המחשב. Solid גם מתממשקת מול מערכת ניהול החשמל, מנהלת את חיבוריות הרשת והאינטגרציה של התקני BlueTooth.
	פנימית Solid משלבת את העוצמה של HAL, NetworkManager ושל חבילת התוכנה של‏ Bluez, אבל ניתן להחליף 
	רכיבים אלו מבלי לשבור תאימות בינארית של יישומים להבטחת ניידות
	מירבית של יישומים.
	</li>
	<li>
‏KHTML הוא מנוע הרינדור בשימוש ב־Konqueror, דפדפן הרשת של KDE. המנוע הוא קל ומהיר ותומך בתקנים מודרנים כגון CSS 3. 
המנוע KHTML היה גם המנוע הראשון לעבור את מבחן Acid 2.
	</li>
	<li>
	‏‫ספריית ThreadWeaver המגיעה עם kdelibs, מספקת ממשק תכנותי ומשפרת את ניצול המערכות מרובות הליבה של ימינו, 
	מספקת ליישומי KDE מגע של ליטוש וניצול יעיל של המשאבים הזמינים למערכת.
	</li>
	<li>
	בהיותה מבוססת על ספריית Qt4 של Trolltech,‏ KDE 4.0 יכולה להשתמש ביכולות הגרפיות המשופרות ובצריכת 
	הזכרון הנמוכה שלה. kdelibs מספקת הרחבה יוצאת מן הכלל לספריית Qt, ומוסיפה תכונות עיליות רבות ונוחות למפתח.
	</li>
</ul>
</p>
<p>
ספריית <a href="http://techbase.kde.org">Techbase</a> כוללת מידע נוסף אודות הספריות של KDE.
</p>


<h4 dir="RTL" align="RIGHT">
צא לסיור מודרך
</h4>
<p>
המדריך החזותי מספק מבט על מהיר על הטכנולוגיות החדשות והמשופרות של KDE 4. המדריך כולל תצלומי מסך רבים,
אשר מציגים את החלקים השונים של KDE 4.0 הטכנולוגיות השונות, והשיפורים עבור המשתמש. מוצגות שם תכונות חדשות של 
שולחן העבודה, יישומים כגון System Settings, מציג המסמכים Okular או מנהל הקבצים Dolphin. כמו כן, 
מוצגים שם <a href="education.php">יישומים חינוכיים</a> ו<a href="games.php">משחקים</a>.
</p>


<h4 dir="RTL" align="RIGHT">קח אותו לסיבוב...</h4>
<p>
עבור מי שמעוניין לקבל את התוכנה לבדיקה, מספר הפצות הודיעו לנו כי יפרסמו חבילות של KDE 4.0 מיד לאחר
השיחרור. הרשימה המלאה נמצאת ב<a href="http://www.kde.org/info/4.0.php">דף המידע של KDE 4.0</a>, וכמו כן 
ניתן למצוא שם קישורים אל קוד המקור, מידע אודות קימפול, אבטחה ועוד.

</p>
<p>
ההפצות הבאות הודיעו על זמינות של חבילות או תקליטורים חיים עבור KDE 4.0:
<ul dir="RTL" align="RIGHT">
	<li dir="RTL" align="RIGHT">
	הפצת <strong>Arklinux 2008.1</strong> מצפה לשחרר גרסת אלפא בזמן הקרוב לשיחרור של KDE 4.0, עם גרסה סופית כעבור 3 או 4 שבועות.
	</li>
	<li dir="RTL" align="RIGHT">
	הפצת <strong>Fedora</strong> תכלול את KDE 4.0 
	ב־Fedora 9 <a href="http://fedoraproject.org/wiki/Releases/9">התשוחרר</a>
	באפריל, עם כמה גרסאות ביטא החל מ־24 באפריל. חבילות של KDE 4.0 זמינות במאגר 
	 הפיתוח של <a href="http://fedoraproject.org/wiki/Releases/Rawhide">Rawhide</a>.
	</li>
	<li dir="RTL" align="RIGHT">
	הפצת <strong>דביאן</strong> מכילה חבילות של KDE 4.0 בהפצת "experimental", כאשר פלטפורמת הפיתוח של KDE תיכנס 
	אל <em>Lenny</em>. לפרטים נוספים יש לעקוב אחרי ההודעות באתר של <a href="http://pkg-kde.alioth.debian.org/">צוות KDE של דיבאן</a>.
	</li>
	<li dir="RTL" align="RIGHT">
	הפצת <strong>Gentoo Linux</strong> מספקת חבילות של KDE 4.0 באתר: <a href="http://kde.gentoo.org">http://kde.gentoo.org</a>.
	</li>
	<li dir="RTL" align="RIGHT">
	הפצת <strong>מנדריבה</strong> תספק חבילות עבור 2008.0 ומתכננת להפיק תקליטור חי עם חוד החנית של 2008.1.
	</li>
	<li dir="RTL" align="RIGHT">
	חבילות עבור <strong>openSUSE</strong> 
	<a href="http://en.opensuse.org/KDE4">זמינות</a>
	 להפצות openSUSE 10.3 ו־openSUSE 10.2  
	 (<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_10.3/KDE4-BASIS.ymp">התקנה בלחיצה בודדת</a>).
	 כמו כן, קיים 
	<a href="http://home.kde.org/~binner/kde-four-live/">תקליטור חי בשם KDE Four Live CD</a>.
	‏KDE 4.0 יהיה חלק מהגרסה הבאה הקרובה openSUSE 11.0 release.
	</li>
	<li dir="RTL" align="RIGHT">
	חבילות עבור <strong>Ubuntu</strong> קיימות בהפצה ‎"Hardy Heron" (8.04)‎, וגם קיימים עדכונים עבור 
	ההפצה היציבה ‎"Gutsy Gibbon" (7.10)‎. קיים גם תקליטור חי להתנסות ב־KDE 4.0. פרטים נוספים 
	<a href="http://kubuntu.org/announcements/kde-4.0.php">בהכרזה</a> 
	באתר Ubuntu.org. 
	</li>
</ul>
</p>

<h2 dir="RTL" align="RIGHT">אודות KDE 4</h2>
<p>
‏KDE 4.0 הוא שולחן עבודה חדשני המבוסס על תוכנה חופשית, המיועד לשימוש יום־יומי וגם לצרכים יחודיים. Plasma שולחן העבודה המפותח עבור KDE 4 המספק
ממשק משתמש אינטואיטיבי לעבודה מול שולחן העבודה ויישומים. דפדפן הרשת Konqueror משלב את הרשת עם שולחן העבודה. מנהל הקבצים Dolphin, 
קורא המסמכים Okular ומרכז הבקרה System Settings מספקים תפקודיות מלאה לשולחן עבודה בסיסי.
<br />
‏KDE נבנית על הספריות של KDE המספקות ממשק תכנותי קל לגישה אל משאבי רשת על בסיס KIO ויכולות גרפיות משופרות בעזרת Qt4. ‏Phonon ו־Solid, המהוות 
חלק מספריות KDE מוסיפות ממשק מולטמדיה ותמיכה טובה יותר בחומרה לכל יישומי KDE.
</p>

<div dir="ltr" align="left">
<?php
  include($site_root . "/contact/about_kde.inc");
?>
</div>

<h4 dir="RTL" align="RIGHT">אנשי קשר (אנגלית בלבד)</h4>
</div>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
