<?php
  $page_title = "Slikovni vodič po KDE 4.0: Igre";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";
  guide_links();
?>

<p>
<img src="images/games.png" align="right"  hspace="5"/>
Skupnost KDE Games je vložila ogromno truda v prenos in prenovo mnogih iger iz KDE 3
in ustvarjanje novih razburljivih iger za KDE 4.0.<br />
Igre so bolj intuitivne za igranje, imajo dodelano grafiko in so bolj neodvisne od
ločljivosti zaslona. Igre lahko igrate razpete čez ves zaslon ali pa v majhnem oknu.<br />
Večina grafike je ustvarjene povsem na novo, kar da igram v KDE 4.0 zelo dodelan izgled in modern videz.
</p>

<h2>KGoldrunner: arkadna igra</h2>
<?php
	screenshot("kgoldrunner_thumb.jpg", "kgoldrunner.jpg", "center", "Zberite zlate kepe v KGoldrunnerju");
?>
<p>
KGoldrunner je retro arkadna igra s priokusom reševanja ugank.
Vsebuje stotine stopenj, kjer morate zbrati kepe zlata, pri
tem pa vas neutrudno lovijo sovražniki.
</p>

<h2>KFourInLine: štiri v vrsto</h2>
<?php
	screenshot("kfourinline_thumb.jpg", "kfourinline.jpg", "center", "Postavite štiri žetone v vrsto v KFourInLine");
?>
<p>
KFourInLine je namizna igra za dva igralca, ki je znana pod
imenom »Štiri v vrsto«. S strateško postavitvijo žetonov vsak
igralec poskuša postaviti štiri žetone v ravno vrsto.
</p>

<h2>LSkat: igra s kartami</h2>
<?php
	screenshot("lskat_thumb.jpg", "lskat.jpg", "center", "Igrajte Skat");
?>
<p>
Poročnik Skat (nemško »Offiziersskat«) je zabavna in nalezljiva igra
s kartami za dva igralca. Drugi igralec je lahko živa oseba ali pa
računalnik.
</p>

<h2>KJumpingCube: igra s kockami</h2>
<?php
	screenshot("kjumpingcube_thumb.jpg", "kjumpingcube.jpg", "center", "Kockajte s KJumpingCube");
?>
<p>
KJumpingCube je preprosta strateška igra, ki temelji na igralnih kockah.
Igralna površina je sestavljena iz kvadratov s pikami. Ko je igralec na potezi,
klikne na prost kvadrat ali pa na svojega.
</p>

<h2>KSudoku: logična igra</h2>
<?php
	screenshot("ksudoku_thumb.jpg", "ksudoku.jpg", "center", "Razmigajte možgane s KSudoku");
?>
<p>
KSudoku je miselna igra postavljanja simbolov. Igralec mora zapolniti
mrežo na tak način, da je v eni vrstici, v enem stolpcu in v enem
bloku samo po en izvod vsakega simbola.
</p>

<h2>Konquest: strategija</h2>
<?php
	screenshot("konquest_thumb.jpg", "konquest.jpg", "center", "Osvojite planete v Konquest");
?>
<p>
Igralec osvaja druge planete s pošiljanjem svojih ladij nanje.
Cilj je zgraditi galaktični imperij, s tem da osvojite vse planete.
</p>

<p>
Več o zgoraj omenjenih in dodatnih igrah izveste na osveženi strani
skupnosti <a href="http://games.kde.org">KDE Games</a>.
</p>
<table width="100%">
	<tr>
		<td width="50%">
				<a href="education-sl.php">
				<img src="images/education-32.png" />
				Prejšnja stran: Izobraževalni programi
				</a>		
		</td>
		<td align="right" width="50%">
				<a href="guide-sl.php">Pregled
				<img src="images/star-32.png" /></a>
		</td>
	</tr>
</table>

<?php
  include("footer.inc");
?>
