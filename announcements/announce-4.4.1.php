<?php
  $page_title = "KDE Software Compilation 4.4.1 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

<!-- // Boilerplate -->

<h3 align="center">
  KDE Software Compilation 4.4.1 Out Now: Codename "Clara"
</h3>

<p align="justify">
  <strong>
KDE Community Ships First Translation and Service Release of the 4.4
Free Desktop, Containing Numerous Bugfixes, Performance Improvements and
Translation Updates
</strong>
</p>

<p align="justify">
March 2nd, 2010. Today, KDE has released a new version of the KDE Software Compilation (KDE SC). <a href="http://kde.org/announcements/announce-4.4.1.php">This month's edition of KDE SC</a> is a bugfix and translation update to KDE SC 4.4. KDE SC 4.4.1 is a recommended upgrade for everyone running KDE SC 4.4.0 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. Users around the world will appreciate that KDE SC 4.4.1 multi-language support is more complete. KDE SC 4 is already translated into more than 50 languages, with more to come.
</p>
<p>
KDE SC 4.4.1 has a number of improvements:
<ul>
    <li>
    A performance problem in KMail when sending emails has been fixed
    </li>
    <li>
    Various fixes in Plasma widgets and other addons, such as the analog clock and the picture frame
    </li>
    <li>
    A number of fixes in Konsole, KDE's powerful terminal application
    </li>
</ul>
The <a href="http://www.kde.org/announcements/changelogs/changelog4_4_0to4_4_1.php">changelog</a> lists more, if not all improvements since KDE SC 4.4.0.
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="4.4/images/general-desktop.jpg"><img src="4.4/screenshots/thumbs/general-desktop_thumb.jpg" align="center" width="540" height="338" alt="The KDE Plasma Desktop Workspace" title="The KDE Plasma Desktop Workspace" /></a>
<br />
<em>The KDE Plasma Desktop Workspace</em>
</div>

<p align="justify">
Note that the changelog is usually incomplete, for a complete list of
changes that went into KDE SC 4.4.1, you can browse the Subversion log.
KDE SC 4.4.1 also ships a more complete set of translations for many of the 50+ supported languages.
<p />
To find out more about the KDE 4.4 desktop and applications, please refer to the
<a href="http://www.kde.org/announcements/4.4/">KDE 4.4.0</a>,
<a href="http://www.kde.org/announcements/4.3/">KDE 4.3.0</a>,
<a href="http://www.kde.org/announcements/4.2/">KDE 4.2.0</a>,
<a href="http://www.kde.org/announcements/4.1/">KDE 4.1.0</a> and
<a href="http://www.kde.org/announcements/4.0/">KDE 4.0.0</a> release
notes. KDE SC 4.4.1 is a recommended update for everyone running KDE SC 4.4.0 or earlier versions.
<p />



<p align="justify">
 KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.4.1/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions.php">major
GNU/Linux and UNIX systems</a> shipping today.
</p>


<!-- // Boilerplate again -->

<h4>
  Installing KDE SC 4.4.1 Binary Packages
</h4>
<p align="justify">
  <em>Packagers</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE SC 4.4.1
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.4.1/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a name="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.4.1.php">KDE SC 4.4.1 Info
Page</a>.
</p>

<h4>
  Compiling KDE SC 4.4.1
</h4>
<p align="justify">
  <a name="source_code"></a>
  The complete source code for KDE SC 4.4.1 may be <a
href="http://download.kde.org/stable/4.4.1/src/">freely downloaded</a>.
Instructions on compiling and installing KDE SC 4.4.1
  are available from the <a href="/info/4.4.1.php#binary">KDE SC 4.4.1 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
