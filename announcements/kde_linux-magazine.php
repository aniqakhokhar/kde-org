<?php
  $page_title = "KDE as Linux Magazines cover story";
  $site_root = "../";
  include "header.inc";
?>

<!-- // Boilerplate -->

<p align="justify">
  <strong>
KDE software features on the cover of this month's Linux Magazine as Marcel Gagné <a href="http://www.linux-magazine.com/Issues/2010/116/PLASMA-BABY">explains how easy it is to create your own custom Plasma Desktop experience</a> by crafting Plasma widgets using JavaScript.
</strong>
</p>
<p>
New in Plasma Desktop 4.4, Marcel describes the ability to quickly create custom widgets as "the very future of computing, a future you can take part in". He explores the possibilities of network transmitted Plasma widgets and praises the portability of widgets, noting that the same widget can be used on your PC running Plasma Desktop, your notebook running Plasma Netbook and even on your smartphone with Plasma Mobile.
</p>
<p>
Marcel goes on to give some examples of simple JavaScript based Plasma widgets and introduces PlasMate, the new application that aims to provide a simple integrated development environment for creating Plasma widgets.
In conclusion, Marcel calls the ability to easily script new Plasma widgets "an amazing piece of forward-looking code" inviting inexperienced coders and dabblers in to the world of writing useful widgets for the KDE community.
<p>
<?php
  include("footer.inc");
?>
