<?php
	include_once ("functions.inc");
	$translation_file = "kde-org";
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "KDE Plasma 5.14 Beta: Reliable, Light and Innovative.",
		'cssFile' => '/content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = 'plasma-5.13.90.0'; // for i18n
	$version = "5.13.90";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px; 
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}

figure {
    position: relative;
    z-index: 2;
    font-size: smaller;
    text-shadow: 2px 2px 5px light-grey;
}

</style>

<main class="releaseAnnouncment container">

	<h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("Plasma %1", $version)?></h1>

	<?php include "./announce-i18n-bar.inc"; ?>
<!--
	<figure class="videoBlock">
		<iframe width="560" height="315" src="https://www.youtube.com/embed/C2kR1_n_d-g?rel=0" allowfullscreen='true'></iframe>
	</figure>
-->
	<figure class="topImage">
		<a href="plasma-5.14/plasma-5.14.png">
			<img src="https://www.kde.org/announcements/plasma-5.14/plasma-5.14-wee.png" width="600" height="338" alt="Plasma 5.14 Beta">
		</a>
		<figcaption><?php print i18n_var("KDE Plasma %1", "5.14 Beta")?></figcaption>
	</figure>

	<p>
		<?php i18n("Thursday, 13 September 2018.")?>
		<?php print i18n_var("Today KDE launches the %1 release of Plasma %2.", "beta", "5.14");?>
	</p>

	<p>
		<?php print i18n_var("Plasma is KDE's lightweight and full featured Linux desktop. For the last three months we have been adding features and fixing bugs and now invite you to test the beta pre-release of Plasma 5.14.");?>
	</p>

	<p>
		<?php print i18n_var("A lot of work has gone into improving Discover, Plasma's software manager, and, among other things, we have added a Firmware Update feature and many subtle user interface improvements to give it a smoother feel. We have also rewritten many effects in our window manager KWin and improved it for slicker animations in your work day. Other improvements we have made include a new Display Configuration widget which is useful when giving presentations.");?>
	</p>

        <p>
		<?php print i18n_var("Please test and send us bug reports and feedback. The final release is scheduled for three weeks' time.");?>
	</p>
	
<br clear="all" />

<p><?php i18n("Browse the full Plasma 5.14 Beta changelog to find out about more tweaks and bug fixes featured in this release: ");?><a href="plasma-5.13.5-5.13.90-changelog.php"><?php print i18n_var("Full Plasma %1 changelog", "5.14 Beta"); ?></a></p>

	<br clear="all" />

<h2><?php i18n("New in Plasma 5.14 Beta");?></h2>

<br clear="all" />
<h3><?php i18n("New Features");?></h3>
<ul>
<figure style="float: right; text-align: right">
<a href="plasma-5.14/kscreen-presentation-plasmoid.png">
<img src="plasma-5.14/kscreen-presentation-plasmoid-wee.png" style="border: 0px" width="350" height="264" alt="<?php i18n("Display Configuration Widget");?>" />
</a>
<figcaption style="text-align: right"><?php i18n("Display Configuration Widget");?></figcaption>
</figure>
<li><?php i18n("There's a new Display Configuration widget for screen management which is useful for presentations."); ?></li>
<li><?php i18n("The Audio Volume widget now has a built in speaker test feature moved from Phonon settings."); ?></li>
<li><?php i18n("The Network widget now works for SSH VPN tunnels again."); ?></li>
<li><?php i18n("Switching primary monitor when plugging in or unplugging monitors is now smoother."); ?></li>
<li><?php i18n("The lock screen now handles user-switching for better usability and security."); ?></li>
<li><?php i18n("You can now import existing encrypted files from a Plasma Vault."); ?></li>
<li><?php i18n("The Task Manager implements better compatibility with LibreOffice."); ?></li>
<br clear="all"/>
<figure style="float: right; text-align: right">
<a href="plasma-5.14/system-monitor-tools.png">
<img src="plasma-5.14/system-monitor-tools-wee.png" style="border: 0px" width="350" height="209" alt="<?php i18n("System Monitor Tools");?>" />
</a>
<figcaption style="text-align: right"><?php i18n("System Monitor Tools");?></figcaption>
</figure>
<li><?php i18n("The System Monitor now has a 'Tools' menu full of launchers to handy utilities."); ?></li>
<li><?php i18n("The Kickoff application menu now switches tabs instantly on hover."); ?></li>
<br clear="all"/>
<figure style="float: right; text-align: right">
<a href="plasma-5.14/panel-widget-options-old.png">
<img src="plasma-5.14/panel-widget-options-old-wee.png" style="border: 0px" width="350" height="139" alt="<?php i18n("Old Panel Widget Edit Menu");?>" />
&nbsp;&nbsp;&nbsp;
<a href="plasma-5.14/panel-widget-options-new.png">
<img src="plasma-5.14/panel-widget-options-new-wee.png" style="border: 0px" width="350" height="139" alt="<?php i18n("New Slicker Panel Widget Edit Menu");?>" />
</a>
<figcaption style="text-align: right"><?php i18n("Panel Widget Edit Menu Old and New Style");?></figcaption>
</figure>
<li><?php i18n("Widget and panels get consistent icons and other user interface improvements."); ?></li>
<br clear="all"/>
<figure style="float: right; text-align: right">
<a href="plasma-5.14/logout-warn.png">
<img src="plasma-5.14/logout-warn-wee.png" style="border: 0px" width="350" height="224" alt="<?php i18n("Logout Warning");?>" />
</a>
<figcaption style="text-align: right"><?php i18n("Logout Warning");?></figcaption>
</figure>
<li><?php i18n("Plasma now warns on logout when other users are logged in."); ?></li>
<li><?php i18n("The Breeze widget theme has improved shadows."); ?></li>
<li><?php i18n("<a href='https://blog.broulik.de/2018/03/gtk-global-menu/'>The Global menu now supports GTK applications.</a> This was a 'tech preview' in 5.13, but it now works out of the box in 5.14."); ?></li>
</ul>

<br clear="all" />
<figure style="float: right; text-align: right">
<a href="plasma-5.14/discover.png">
<img src="plasma-5.14/discover-wee.png" style="border: 0px" width="350" height="224" alt="<?php i18n("Plasma Discover");?>" />
</a>
<figcaption style="text-align: right"><?php i18n("Plasma Discover");?></figcaption>
</figure>
<h3 id="discover"><?php i18n("Plasma Discover");?></h3>
<p><?php i18n("Discover, our software and add-on installer, has more features and improves its look and feel.");?></p>
<ul>
<li><?php i18n("Discover gained <i>fwupd</i> support, allowing it to upgrade your computer's firmware.");?></li>
<li><?php i18n("It gained support for Snap channels.");?></li>
<li><?php i18n("Discover can now display and sort apps by release date.");?></li>
<li><?php i18n("You can now see an app's package dependencies.");?></li>
<li><?php i18n("When Discover is asked to install a standalone Flatpak file but the Flatpak backend is not installed, it now offers to first install the backend for you.");?></li>
<li><?php i18n("Discover now tells you when a package update will replace some packages with other ones.");?></li>
<li><?php i18n("We have added numerous minor user interface improvements: update button are disabled while checking for updates, there is visual consistency between settings and the update pages, updates are sorted by completion percentage, we have improved the review section of the update notifier plasmoid, etc..");?></li>
<li><?php i18n("We have improved reliability and stability through a bunch of bug fixes.");?></li>
</ul>

<br clear="all"/>
<figure style="float: right; text-align: right">
<video controls="true" width="350" height="200">
<source src="plasma-5.14/kwin-glide-effect.mp4" type="video/mp4" />
Your browser does not support the video tag.
</video>
<figcaption style="text-align: right"><?php i18n("Improved KWin Glide Effect");?></figcaption>
</figure>
<h3 id="bugfixes"><?php i18n("KWin and Wayland:");?></h3>
<ul>
<li><?php i18n("We fixed copy-paste between GTK and non-GTK apps on Wayland.");?></li>
<li><?php i18n("We fixed non-centered task switchers on Wayland.");?></li>
<li><?php i18n("We have improved pointer constraints.");?></li>
<li><?php i18n("There are two new interfaces, XdgShell and XdgOutput, for integrating more apps with the desktop.");?></li>
<li><?php i18n("We have considerably improved and polished KWin effects throughout, including completely rewriting the Dim Inactive effect, adding a new scale effect, rewriting the Glide effect, and more.");?></li>
</ul>

<br clear="all" />
<h3 id="bugfixes"><?php i18n("Bugfixes");?></h3>
<p><?php i18n("We fixed many bugs, including:");?></p>
<ul>
<li><?php i18n("Blurred backgrounds behind desktop context menus are no longer visually corrupted.");?></li>
<li><?php i18n("It's no longer possible to accidentally drag-and-drop task manager buttons into app windows.");?></li>
</ul>

<br clear="all" />
	<!-- // Boilerplate again -->
	<section class="row get-it">
		<article class="col-md">
			<h2><?php i18n("Live Images");?></h2>
			<p>
				<?php i18n("The easiest way to try out Plasma 5.14 beta is with a live image booted off a USB disk. Docker images also provide a quick and easy way to test Plasma.");?>
			</p>
			<a href='https://community.kde.org/Plasma/Live_Images' class="learn-more"><?php i18n("Download live images with Plasma 5");?></a>
			<a href='https://community.kde.org/Plasma/Docker_Images' class="learn-more"><?php i18n("Download Docker images with Plasma 5");?></a>
		</article>

		<article class="col-md">
			<h2><?php i18n("Package Downloads");?></h2>
			<p>
				<?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
			</p>
			<a href='https://community.kde.org/Plasma/Packages' class="learn-more"><?php i18n("Package download wiki page");?></a>
		</article>

		<article class="col-md">
			<h2><?php i18n("Source Downloads");?></h2>
			<p>
				<?php i18n("You can install Plasma 5 directly from source.");?>
			</p>
			<a href='http://community.kde.org/Frameworks/Building'><?php i18n("Community instructions to compile it");?></a>
			<a href='../info/plasma-5.13.90.php' class='learn-more'><?php i18n("Source Info Page");?></a>
		</article>
	</section>

	<section class="give-feedback">
		<h2><?php i18n("Feedback");?></h2>

		<p>
			<?php print i18n_var("You can send us feedback and get updates on  <a href='%1'><img src='%2' /></a> <a href='%3'>Facebook</a>
			or <a href='%4'><img src='%5' /></a> <a href='%6'>Twitter</a>
			or <a href='%7'><img src='%8' /></a> <a href='%9'>Google+</a>.", "https://www.facebook.com/kde", "https://www.kde.org/announcements/facebook.gif", "https://www.facebook.com/kde", "https://twitter.com/kdecommunity", "https://www.kde.org/announcements/twitter.png", "https://twitter.com/kdecommunity", "https://plus.google.com/105126786256705328374/posts", "https://www.kde.org/announcements/googleplus.png", "https://plus.google.com/105126786256705328374/posts"); ?>
		</p>
		<p>
			<?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?>
		</p>

		<p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>#Plasma IRC channel</a>, <a href='%2'>Plasma-devel mailing list</a> or report issues via <a href='%3'>bugzilla</a>. If you like what the team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided"); ?>

		<p><?php i18n("Your feedback is greatly appreciated.");?></p>
	</section>

	<h2>
		<?php i18n("Supporting KDE");?>
	</h2>

	<p align="justify">
		<?php print i18n_var("KDE is a <a href='%1'>Free Software</a> community that exists and grows thanks to the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions. Whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, or donating money, all contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='%2'>Supporting KDE page</a> for further information.", "http://www.gnu.org/philosophy/free-sw.html", "/community/donations/"); ?>
	</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

</main>
<?php
  require('../aether/footer.php');
