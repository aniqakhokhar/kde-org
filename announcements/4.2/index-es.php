<?php
  $page_title = "Anuncio de la publicación de KDE 4.2.0";
  $site_root = "../";
  include "header.inc";
?>

Also available in:
<?php
  $release = '4.2';
  include "../announce-i18n-bar.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
  La comunidad de KDE mejora el escritorio con KDE 4.2
</h3>

<p align="justify">
  <strong>
    KDE 4.2 (nombre en código: <em>«La respuesta»</em>) presenta un mejor
entorno de escritorio, y proporciona nuevas versiones de las aplicaciones y de
la plataforma de desarrollo.
  </strong>
</p>

<p align="justify">
27 de enero de 2009. La <a href="http://www.kde.org/">comunidad de KDE</a> ha
anunciado hoy la disponibilidad inmediata de <em>«La respuesta»</em>, (también
conocida como KDE 4.2.0), que deja preparado este escritorio libre para los
usuarios finales.  KDE 4.2 se basa en la tecnología introducida con KDE 4.0 en
enero de 2008.  Tras la publicación de KDE 4.1, que estaba dirigida a los
usuarios ocasionales, la comunidad de KDE considera que ahora tiene una oferta
convincente para la mayoría de los usuarios finales.
</p>

<p><strong><a href="guide.php">Consulta la Guía Visual de KDE 4.2</a></strong> para más detalles en cuanto a las novedades de 4.2 o continúa para un resumen.</p>

<a href="guide.php">
<?php
    screenshot("desktop_thumb.png", "", "center",
"El escritorio KDE 4.2", "337");
?>
</a>
<h3>
    Un mejor entorno de escritorio
</h3>
<p align="justify">

<ul>
    <li>
        <p align="justify">
        Aún más mejoras en la interfaz de escritorio Plasma, que hacen que
organizar su espacio de trabajo sea más fácil.
        <strong>Miniaplicaciones nuevas y mejoradas</strong>, como un lanzador
rápido, información metereológica, fuentes de noticias, tiras cómicas,
compartición rápida de ficheros por medio de servicios de tipo
<em>pastebin</em>.  Las miniaplicaciones de Plasma se pueden usar ahora encima
del salvapantallas, por ejemplo para dejar una nota mientras el usuario está
ausente.
        Plasma actúa opcionalmente como un <strong>escritorio tradicional, al
estilo de un gestor de ficheros</strong>.
        Se han añadido previsualizaciones para los iconos de fichero y la
ubicación persistente de los iconos.<br />

        El panel de Plasma <strong>agrupa ahora las tareas</strong> y las
muestra en varias filas.  La bandeja del sistema mejorada ahora <strong>sigue
la pista de las tareas que toman más tiempo</strong>, como las descargas.  Las
<strong>notificaciones</strong> del sistema y de las aplicaciones se muestran
de modo unificado por medio de la bandeja del sistema.  Para ahorrar espacio,
se pueden ocultar los iconos de la bandeja del sistema. Además, el panel se
puede <strong>ocultar automáticamente</strong> para liberar espacio en
pantalla.  Los elementos gráficos se pueden mostrar tanto en paneles como en
el escritorio.<br />
        </p>
    </li>
    <li>
        <p align="justify">
        KWin ofrece una gestión de ventana fluida y eficiente.  En KDE 4.2
emplea <strong>física del movimiento</strong> para dar dar un aire natural a
viejos y <strong>nuevos efectos</strong> tales como el «Cubo» y la «Lámpara
maravillosa».  KWin sólo habilita los efectos de escritorio en la
configuración predeterminada en aquellos equipos que sean capaces de
manejarlos.  Una <strong>configuración más fácil</strong> permite al usuario
seleccionar distintos efectos como intercambiador de ventanas, haciendo que
cambiar de ventanas sea más eficiente.
        </p>
    </li>
    <li>
        <p align="justify">
        Las herramientas nuevas y mejoradas del espacio de trabajo aumentan la
productividad.  PowerDevil facilita la vida móvil ofreciendo una
<strong>gestión de energía</strong> moderna y discreta para portátiles y
dispositivos móviles.  Ark ofrece <strong>creación y extracción</strong>
inteligente de archivos, y las nuevas herramientas del sistema de impresión
permiten al usuario <strong>gestionar impresoras</strong> y trabajos de
impresión fácilmente.
        </p>
    </li>
</ul>

<p>
Además, se han añadido varios idiomas nuevos, ampliando el número de usuarios
para los que KDE está disponible en su idioma materno en unos 700 millones. 
Entre los nuevos idiomas añadidos están el árabe, islandés, vascuence, hebreo,
rumano, tayiko y varios idiomas indios (bengalí, guyaratí, canarés, maithili,
maratí), lo que indica un aumento de popularidad en esta zona de Asia.
</p>

<p align="right">
<embed src="http://blip.tv/play/AwGOgSc" type="application/x-shockwave-flash"
width="540" height="462" allowscriptaccess="always"
allowfullscreen="true"></embed></p>

<h3>
    Las aplicaciones dan un paso adelante
</h3>
<p align="justify">
<ul>
    <li>
        <p align="justify">
        La gestión de ficheros se vuelve más rápida y eficiente.  El gestor de
ficheros Dolphin tiene ahora un deslizador para <strong>ajustar el tamaño de
los iconos</strong> fácilmente.  Hay más mejoras en la interfaz de usuario,
como <strong>sugerencias con previsualizaciones</strong> y un indicador de
capacidad para los dispositivos multimedia extraíbles.  Estos cambios también
se han aplicado a los <strong>diálogos de selección de  ficheros</strong> de
KDE, haciendo que sea más fácil encontrar el fichero correcto.
        </p>
    </li>
    <li>
        <p align="justify">
        Un estudiante del Google Summer of Code ha revisado las vistas de
listas de correo de KMail.  El usuario puede ahora configurar la
<strong>visualización de información adicional</strong>, optimizando el flujo
de trabajo para cada carpeta individualmente.  Tambien ha mejorado el uso de
<strong>IMAP y otros protocolos</strong>, con lo que KMail es mucho más
rápido.
        </p>
    </li>
    <li>
        <p align="justify">
        La navegación web ha mejorado.  El navegador web Konqueror ha mejorado
su manejo de los <strong>gráficos vectoriales escalables</strong> y ha
recibido muchas mejoras de rendimiento.  Un <strong>nuevo diálogo de
búsquedas</strong> hace que buscar dentro de páginas web sea menos molesto. 
Ahora Konqueror <strong>muestra los marcadores</strong> en el arranque.
        </p>
    </li>
</ul>
</p>

<p align="right">
<embed src="http://blip.tv/play/golB5aR7joEn"
type="application/x-shockwave-flash" width="540" height="462"
allowscriptaccess="always" allowfullscreen="true"></embed>

<h3>
    La plataforma acelera el desarrollo
</h3>
<p align="justify">
<ul>
    <li>
        <p align="justify">
        <strong>Implementación mejorada de los lenguajes de script</strong>. 
Las miniaplicaciones de Plasma se pueden escribir ahora en JavaScript, Python
y Ruby.  Estas miniaplicaciones se pueden distribuir en la red por medio de
servicios web y herramientas de colaboración en red como OpenDesktop.org. 
También se pueden usar en Plasma los GoogleGadgets, y ha mejorado aún más el
uso de elementos gráficos del <em>dashboard</em> de Mac OS X.
        </p>
    </li>
    <li>
        <p align="justify">
        Hay disponibles versiones previas de varias aplicaciones de KDE para
<strong>Windows</strong> y <strong>Mac OS X</strong>, y algunas aplicaciones
se están acercando ya a la calidad de publicación, mientras que otras
necesitan algo de trabajo dependiendo de las funcionalidades que implementan. 
La implementación de OpenSolaris está avanzando y aproximándose a la calidad
de una versión estable.  KDE4 sobre FreeBSD sigue madurando.
        </p>
    </li>
    <li>
        <p align="justify">
        Una vez se publique Qt bajo los términos de la <strong>LGPL</strong>,
tanto las bibliotecas de KDE como las Qt bajo ellas estarán disponibles bajo
esos términos de licencia, más laxos, con lo que la plataforma será más
atractiva para el desarrollo de software privativo.
        </p>
    </li>
</ul>
</p>


<h4>
  Instalación de KDE 4.2.0
</h4>

<p align="justify">
KDE, incluyendo todas sus bibliotecas y aplicaciones, está disponible bajo
licencias de software libre.  KDE se puede obtener en código fuente y varios
formatos binarios de <a
href="http://download.kde.org/stable/4.2.0/">http://download.kde.org</a> y
también se puede obtener en <a
href="http://www.kde.org/download/cdrom.php">CD-ROM</a> o con cualquiera de
las <a href="http://www.kde.org/download/distributions.php">principales
distribuciones de GNU/Linux y sistemas Unix</a> actuales.
</p>

<p align="justify">
  <em>Empaquetadores</em>.
  Algunos proveedores de sistemas operativos Linux/Unix han tenido la
amabilidad de proporcionar paquetes binarios de KDE 4.2.0 para algunas
versiones de su distribución, y en otros casos lo han hecho voluntarios de la
comunidad.  Algunos de estos paquetes binarios se pueden descargar
gratuitamente desde <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.2.0/">http://
download.kde.org</a>.  Durante las próximas semanas, puede que haya
disponibles más paquetes binarios, así como actualizaciones para los paquetes
que hay disponibles ahora.
</p>

<p align="justify">
  <a name="package_locations"><em>Ubicación de los paquetes</em></a>
  Visite la <a href="/info/4.2.0.php">página de información sobre KDE
4.2.0</a> para obtener una lista actualizada de los paquetes binarios
disponibles de los que ha sido informado el Proyecto KDE.
</p>

<p align="justify">
Los problemas de rendimiento con el controlador gráfico binario de
<em>NVidia</em> han sido <a
href="http://techbase.kde.org/User:Lemma/KDE4-NVIDIA">solucionados</a> en las
últimas versiones beta del controlador disponible de NVidia.
</p>


<h4>
  Compilación de KDE 4.2.0
</h4>

<p align="justify">
  <a name="source_code"></a>
  El código fuente completo de KDE 4.2.0 se puede <a
href="http://download.kde.org/stable/4.2.0/src/">descargar libremente</a>.
Las instrucciones para compilar e instalar KDE 4.2.0 están disponibles en la
<a href="/info/4.2.0.php#binary">página de información de KDE 4.2.0.</a>.
</p>


<h4>
    Corra la voz
</h4>

<p align="justify">
El equipo de KDE anima a todo el mundo a correr la voz, también en la web
social.  Envíe artículos a sitios web, use canales como delicious, digg,
reddit, twitter, identi.ca.  Envíe capturas de pantalla a servicios como
Facebook, FlickR, ipernity y Picasa y publíquelas en los grupos adecuados. 
Cree capturas de vídeo, y envíelas a YouTube, Blip.tv, Vimeo y otros sitios. 
No olvide etiquetar el material que envíe con la <em>etiqueta
<strong>kde42</strong></em> para que sea más fácil para todo el mundo
encontrar el material, y para el equipo de KDE compilar recortes de prensa del
anuncio de KDE 4.2.  Ésta es la primera vez que el equipo de KDE intenta un
esfuerzo coordinado para usar los medios sociales para hacer llegar sus
mensajes.  ¡Sea parte del proyecto!
</p>

<p>
En foros web, puede informar a la gente sobre las atractivas nuevas
características de KDE, ayudar a quienes estén empezando con su nuevo
escritorio y ayudarnos a difundir la información.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Contactos de prensa</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
