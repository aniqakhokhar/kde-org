<?php
  $page_title = "KDE 4.2.0&nbsp;– Pressemitteilung";
  $site_root = "../";
  include "header.inc";
?>

Auch verfügbar auf:
<?php
  $release = '4.2';
  include "../announce-i18n-bar.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
  Die Gemeinschaft der KDE-Entwickler sorgt für mehr Benutzer- und Bedienfreundlichkeit mit KDE 4.2.
</h3>

<p align="justify">
  <strong>
    KDE 4.2 (Kodename: <em>„The Answer“&nbsp;– „Die Antwort“</em>) bringt eine verbesserte Bedienbarkeit der Benutzeroberfläche, Anwendungen und Entwicklungsplattform mit sich.
  </strong>
</p>

<p align="justify">
27. Januar, 2009. Die <a href="http://www.kde.org/">Gemeinschaft der KDE-Entwickler</a> gab heute die sofortige Verfügbarkeit von <em>„The Answer“&nbsp;– „Die Antwort“</em> (alias KDE 4.2.0) bekannt, um die freie Arbeitsumgebung bereit für Endanwender zu machen. KDE 4.2 baut auf die Technologien, welche mit KDE 4.0 im Januar 2008 eingeführt wurden, auf. Nach der Veröffentlichung von KDE 4.1, welches sich noch mehr an versierte Benutzer richtete, ist sich die KDE-Entwicklergemeinde nun sicher ein ausgereiftes Produkt für den Großteil der Endanwender bereit zu stellen.
</p>

<p><strong><a href="guide.php">Lesen Sie auch den Visual Guide zu KDE 4.2</a></strong> für mehr Details zu den Innovationen in 4.2</p>

<a href="guide.php">
<?php
    screenshot("desktop_thumb.png", "desktop.png", "center",
"Die KDE-4.2-Arbeitsoberfläche.", "337");
?>
</a>

<h3>
    Die Arbeitsoberfläche verbessert die Benutzbarkeit
</h3>
<p align="justify">

<ul>
    <li>
        <p align="justify">
        Weitere Verbesserungen der Plasma-Arbeitsoberfläche machen die Organisation Ihrer Arbeitsumgebung noch einfacher.
        Es gibt <strong>neue und verbesserte Miniprogramme</strong>, wie zum Beispiel ein Schnellstarter-, Wetterinformation-, Nachrichtenfeed-, Comic- und „Pastebin“-Miniprogramm (für den einfachen, gemeinsamen Zugriff auf Dateien). Plasma-Miniprogramme können nun auch oberhalb des Bildschirmschoners verwendet werden, um zum Beispiel eine Notiz zu hinterlassen während der Besitzer nicht anwesend ist.
	Optional dazu kann Plasma auch wie eine <strong>traditionelle, einem Dateimanager ähnliche Arbeitsoberfläche</strong> verwendet werden.
        Vorschaubilder für Dateisymbole und dauerhafte Ausrichtungen von Symbolen wurden implementiert.<br />
        Die Plasma-Kontrollleiste verfügt nun über die Möglichkeit <strong>gleiche Anwendungen zu gruppieren</strong>, sowie mehrere Anwendungen mehrzeilig anzuzeigen. Der verbesserte Systemabschnitt der Kontrollleiste ist nun in der Lage <strong>länger dauernde Vorgänge mitzuverfolgen</strong> (zum Beispiel das Herunterladen von Dateien). <strong>System- und Programmnachrichten</strong> werden nun in einer einheitlichen Weise durch den Systemabschnitt der Kontrollleiste angezeigt. Um Platz zu sparen können Symbole in diesem Abschnitt nun ausgeblendet werden. Auch die Kontrollleiste selbst kann <strong>automatisch ausgeblendet werden</strong>, um mehr Platz auf dem Bildschirm zu schaffen. Miniprogramme können sowohl auf der Arbeitsoberfläche, als auch in Kontrollleisten angezeigt werden.<br />
        </p>
    </li>
    <li>
        <p align="justify">
        KWin bietet flüssiges und effizientes Fenstermanagement. In KDE 4.2 wird die <strong>Bewegungsphysik</strong> verwendet, um ein natürliches Gefühl bei alten und <strong>neuen Effekten</strong>, wie zum Beispiel dem „Würfel“ oder der „magischen Lampe“, zu erzeugen. KWin aktiviert diese Arbeitsflächeneffekte standardmäßig nur auf Rechnern, die technisch in der Lage sind sie zu handhaben. Eine <strong>vereinfachte Konfiguration</strong> erlaubt es dem Benutzer verschiedene Effekte zum Wechseln zwischen Fenstern auszuwählen, um den Vorgang so effizient wie möglich zu machen.
        </p>
    </li>
    <li>
        <p align="justify">
        Neue und verbesserte Arbeitswerkzeuge verbessern Ihre Produktivität. PowerDevil unterstützt Ihr mobiles Leben, indem es eine moderne und gleichzeitig dezente <strong>Stromverbrauchsüberwachung</strong>, für Laptops und mobile Geräte, bietet. Ark bietet Ihnen „intelligentes“ <strong>Entpacken und Erstellen</strong> von Archivdateien, und die neuen Drucker-Werkzeuge machen es einfach <strong>Drucker und Druckaufträge zu verwalten</strong>.
        </p>
    </li>
</ul>
Zudem wurde mit der Unterstützung einiger neuer Sprachen die Zahl der Benutzer, für die KDE in Ihrer Muttersprache zur Verfügung steht, auf zirka 700 Millionen erweitert. Die neuen unterstützten Sprachen sind Arabisch, Isländisch, Baskisch, Hebräisch, Rumänisch, Tadschikisch, sowie einige indische Sprachen (Bengalisch, Gujarati, Kanaresisch, Maithili und Marathi), was einen Popularitätsschub in diesem Teil von Asien zeigt.
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;"><br />
<embed src="http://blip.tv/play/AwGOgSc" type="application/x-shockwave-flash"
width="540" height="462" allowscriptaccess="always" allowfullscreen="true"></embed>
<a href="http://blip.tv/file/get/Sebasje-ThePlasmaDesktopShellInKDE42312.ogv">Ogg-Theora-Version</a>
</div>

<h3>
    Die Anwendungen machen einen großen Sprung nach vorn
</h3>
<p align="justify">
<ul>
    <li>
        <p align="justify">
        Das Verwalten von Dateien wird einfacher und schneller. Der Dateimanager Dolphin verfügt jetzt über einen Schieberegler, um ganz einfach die <strong>Symbolgröße einzustellen</strong>. Weitere Verbesserung der Benutzeroberfläche beinhalten <strong>Kurzinfos mit Vorschau</strong> und eine Anzeige für verfügbares Speichervolumen von entfernbaren Speichermedien. Diese Änderungen wurden auch auf die <strong>Datei-Dialoge</strong> in KDE angewendet, um das Auffinden der richtigen Datei einfacher zu machen. 
        </p>
    </li>
    <li>
        <p align="justify">
        Die E-Mail-Listenansicht von KMail wurde, im Rahmen des „Google Summer of Code“, von einem Studenten rundum erneuert. Es ist jetzt möglich das <strong>Anzeigen von zusätzlichen Informationen</strong> einzustellen, um so das Arbeiten mit jedem einzelnen Ordner zu optimieren. Auch wurde die Unterstützung von <strong>IMAP und anderen Protokollen</strong> verbessert, um KMail schneller zu machen.
        </p>
    </li>
    <li>
        <p align="justify">
        Das Benutzen und Durchsuchen des Internet wird besser. Der Webbrowser Konqueror bietet verbesserte Unterstützung für <strong>skalierbare Vektorgrafiken</strong> und erfuhr außerdem einige Verbesserungen in puncto Geschwindigkeit. Ein neuer <strong>Durchsuchen-Dialog</strong> gestaltet das Suchen innerhalb von Webseiten einfacher. Beim Start zeigt Ihnen Konqueror nun außerdem Ihre Lesezeichen.
        </p>
    </li>
</ul>
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;"><br />
<embed src="http://blip.tv/play/golB5aR7joEn" type="application/x-shockwave-flash" width="540" height="462" allowscriptaccess="always" allowfullscreen="true"></embed>
<a href="http://blip.tv/file/get/Sebasje-WindowManagementInKDE42153.ogv">Ogg-Theora-Version</a>
</div>

<h3>
    Die Plattform beschleunigt die Entwicklung
</h3>
<p align="justify">
<ul>
    <li>
        <p align="justify">
        <strong>Erweiterte Unterstützung für Skriptsprachen</strong>. Plasma-Miniprogramme können jetzt in JavaScript, Python und Ruby geschrieben werden. Diese Miniprogramme können online über Internet-Dienste wie zum Beispiel OpenDesktop.org an andere weitergegeben werden. GoogleGadgets können jetzt mit Plasma verwendet werden, und die Unterstützung für die so genannten „Mac OS X dashboard widgets“ wurde weiter verbessert.
        </p>
    </li>
    <li>
        <p align="justify">
        Vorabversionen von zahlreichen KDE-Anwendungen für <strong>Windows</strong> und <strong>Mac&nbsp;OS&nbsp;X</strong> sind verfügbar, einige dieser Anwendung haben bereits die für die Veröffentlichung nötige Qualität erreicht, an anderen muss noch gearbeitet werden. Abhängig von der Funktionalität, die diese Anwendungen implementieren. Die Unterstützung von OpenSolaris verbessert sich stetig und ist bereits ziemlich Stabil. Die Entwicklung von KDE&nbsp;4 auf FreeBSD macht Fortschritte und dauert noch an.
        </p>
    </li>
    <li>
        <p align="justify">
        Nachdem Qt unter den Bedingungen der <strong>LGPL</strong> veröffentlicht werden wird, sind dann Beide&nbsp;– die KDE-Bibliotheken und Qt&nbsp;– unter diesen aufgelockerten Lizenzbedingungen verfügbar. Dies macht KDE zu einer noch attraktiveren Plattform für kommerzielle Software-Entwicklung.
        </p>
    </li>
</ul>
</p>


<h4>
  KDE 4.2.0 installieren
</h4>
<p align="justify">
 KDE, inklusive all seiner Programmbibliotheken und Anwendungen, ist gratis unter freien Softwarelizenzen verfügbar. KDE kann im Quelltext und in zahlreichen Binärformaten unter <a
href="http://download.kde.org/stable/4.2.0/">http://download.kde.org</a> heruntergeladen werden, außerdem gibt es es auch auf <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>. Auch in allen <a href="http://www.kde.org/download/distributions.php">großen GNU/Linux- und UNIX-System</a>, die heutzutage angeboten werden ist KDE enthalten.
</p>
<p align="justify">
  <em>Paket-Hersteller</em>.
  Einige Linux- und Unix-Betriebssystemhersteller haben freundlicherweise Binärpakete von KDE 4.2.0 für einige Versionen ihrer Distributionen erstellt. In einigen anderen Fällen wurde dies von freiwilligen Mitarbeitern erledigt. Manche dieser Binärpakete können gratis von den Servern des KDE-Projekts (<a
href="http://download.kde.org/binarydownload.html?url=/stable/4.2.0/">http://download.kde.org</a>) heruntergeladen werden. Zusätzliche Binärpakete, sowie Aktualisierungen der bereits verfügbaren Pakete werden höchstwahrscheinlich in den kommenden Wochen verfügbar sein.
</p>

<p align="justify">
  <a name="package_locations"><em>Wo finde ich fertige Pakete?</em></a>.
  Für eine aktuelle Liste von verfügbaren Binärpaketen, über die das KDE-Projekt informiert wurde, werfen Sie bitte einen Blick auf die <a href="/info/4.2.0.php">KDE-4.2.0-Infoseite</a>.
</p>

<p align="justify">
Probleme bezüglich der Arbeitsleistung, im Zusammenhang mit dem binären Grafik-Treiber von <em>NVidia</em>, wurden in der letzten verfügbaren Beta-Version von NVidia <a href="http://techbase.kde.org/User:Lemma/KDE4-NVIDIA">behoben</a>.
</p>

<h4>
  KDE 4.2.0 kompilieren
</h4>
<p align="justify">
  <a name="source_code"></a>
  Der vollständige Quelltext von KDE 4.2.0 kann <a
href="http://download.kde.org/stable/4.2.0/src/">frei heruntergeladen werden</a>.
Informationen und Anweisungen zum Kompilieren und Installieren von KDE 4.2.0 finden Sie auf der <a href="/info/4.2.0.php#binary">KDE-4.2.0-Infoseite</a>.
</p>

<h4>
   Sagen Sie’s ruhig weiter!
</h4>
<p align="justify">
Das KDE-Team bittet und ermutigt jeden, diese Nachricht auch im „Web 2.0“ zu verbreiten. Stellen Sie Artikel auf Webseiten ein, verwenden Sie Dienste wie Mr.&nbsp;Wong, Yigg, Webnews, Delicious, identi.ca und Twitter. Laden Sie Bildschirmfotos auf Seiten wie <a href="http://de-de.facebook.com/">Facebook</a>, <a href="http://www.studivz.net/">StudiVZ</a>, <a href="http://www.flickr.com/">FlickR</a> und <a href="http://picasa.google.de/">Picasa</a> hoch, und übermitteln Sie sie an die entsprechenden Gruppen. Erstellen Sie Videos von Ihrer Arbeitsoberfläche und laden Sie sie auf YouTube, MyVideo, Vimeo und anderen Seiten hoch. Vergessen Sie nicht hochgeladenes Material mit dem <em>Schlagwort <strong>kde42</strong></em> zu kennzeichnen, so dass es für jedermann einfacher wird es zu finden und für das KDE-Team im Nachhinein Auswertungen über die Berichterstattung für die KDE-4.2-Pressemitteilung anzustellen. Dies ist das erste Mal, dass das KDE-Team versucht das „Web 2.0“ gezielt, in einem koordinierten Bemühen, für seine Zwecke zu nutzen. Helfen Sie uns die freudige Nachricht zu verbreiten, seien Sie ein Teil dieses Bestrebens.
</p>
<p>
Als Nutzer von Web-Foren können Sie uns helfen, in dem Sie anderen Leuten über die neuen Funktionen von KDE erzählen und ihnen helfen mit ihrer neuen Arbeitsumgebung zurechtzukommen. Helfen Sie uns, indem Sie die Informationen verbreiten.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Pressekontakte</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
