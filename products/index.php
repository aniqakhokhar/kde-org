<?php
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Products",
        'section' => 'products',
        'cdnCSSFiles' => ['/version/kde-org/products.css'],
        'description' => 'The KDE community develops a lot of different products for personnal and professional use. Learn more about these products here.',
    ]);

    require('../aether/header.php');

?>

    <main class="container" id="community-lists">
        <div class="laptop-with-overlay mt-3 mb-3 w-75 ml-auto mr-auto">
            <img class="laptop img-fluid mb-3" src="/content/plasma-desktop/laptop.png" alt="empty laptop with an overlay">
            <div class="laptop-overlay">
                <img class="img-fluid mb-3" src="/announcements/plasma-5.18/customize-layout.png" alt="Screenshot Plasma with the kokkini wallpaper" />
            </div>
        </div>
        <h1 class="text-center">Plasma</h1>
        <p style="max-width: 800px;" class="ml-auto mr-auto text-center"
            Plasma is KDE's flagship product, offering the most customizable
            desktop environment available. The KDE
            community has the driving goal of making it simple by default, and powerful
            when needed.
        </p>
        <p style="max-width: 800px" class="ml-auto mr-auto d-flex flex-wrap justify-content-center">
            <a href="/plasma-desktop" class="learn-more button mb-2 ml-1 mr-1">Learn about Plasma</a>
            <a href="https://community.kde.org/Distributions" class="learn-more button mb-2 ml-1 mr-1">View Distributions offering Plasma</a>
        </p>


        <section class="row" id="productsRow">
            <article class="col-md">
                <a href="/applications/"><img src="apps.png" class="w-100 d-block" style="max-width: 120px; margin: 20px auto;" /></a>
                <h3><a href="/applications/">KDE Applications</a></h3>
                <p>
                    Applications which empower users with freedom and privacy.
                </p>
            </article>
            <article class="col-md">
                <a href="https://plasma-mobile.org/overview/"><img src="phone.png" class="w-100 d-block" style="max-width: 120px; margin: 20px auto;" /></a>
                <h3><a href="https://plasma-mobile.org/overview/">Plasma Mobile</a></h3>
                <p>
                    Experience the freedom and privacy of KDE software on your mobile device.
                </p>
            </article>
            <article class="col-md">
                <a href="https://neon.kde.org"><img src="neon.png" class="w-100 d-block" style="max-width: 120px; margin: 20px auto;" /></a>
                <h3><a href="https://neon.kde.org">KDE neon</a></h3>
                <p>
                    The latest and greatest of KDE community software packaged on
                    a rock-solid base.
                </p>
            </article>

            <article class="col-md">
                <a href="https://techbase.kde.org/Development/Tools"><img src="develop.png" class="w-100 d-block" style="max-width: 120px; margin: 20px auto;" /></a>
                <h3><a href="https://techbase.kde.org/Development/Tools">Development Tools<a/></h3>
                <p>
                    Developers using KDE tools are given strong foundations to
                    build great software.
                </p>
            </article>
            <article class="col-md">
                <a href="https://wikitolearn.org/"><img src="wikitolearn.png" class="w-100 d-block" style="max-width: 120px; margin: 20px auto;" /></a>
                <h3><a href="https://wikitolearn.org/">WikiToLearn</a></h3>
                <p>
                    Collaborative textbooks. Learn with the best. Create books. Share knowledge.
                </p>
            </article>
            <article class="col-md">
                <a href="https://develop.kde.org/products/frameworks//"><img src="frameworks.png" class="w-100 d-block" style="max-width: 120px; margin: 20px auto;" /></a>
                <h3><a href="https://develop.kde.org/products/frameworks//">KDE Frameworks</a></h3>
                <p>
                    Add-ons for Qt. Try, for example, <a href="https://develop.kde.org/frameworks/kirigami/">Kirigami</a>: the UI for building apps for desktop, mobile, and everything in between.
                </p>
            </article>
        </section>
    </main>
<?php


    require('../aether/footer.php');

?>
