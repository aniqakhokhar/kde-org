<?php

  // Don't show the global donation square, it's confusing in this page
  $page_disablekdeevdonatebutton = true;

  $page_title="KDE Sprints 2015 fundraising";

  $goal=38500;
//   $teaser=true;
//   $teaserFiles = array("randa1.jpg", "randa2.jpg", "randa3.jpg", "randa4.jpg");
    require('../../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "KDE Sprints 2015 fundraising",
        'cssFile' => '/css/announce.css'
    ]);

    require('../../aether/header.php');
    $site_root = "../../";

    require("www_config.php");

    $res = $dbConnection->prepare("SELECT *, UNIX_TIMESTAMP(date) AS date_t FROM randameetings2015donations ORDER BY date DESC;");
    $res->execute();
    $total = 0;
    $count = 0;
    $table = "<table border='1' width='100%'>";
    $table.="<tr><th width='20'>No.</th><th width='180'>Date</th><th width='80'>Amount</th><th style='text-align:left'>Donor Name</th></tr>";
    while ($row = $res->fetch()) {
            $msg = htmlspecialchars($row["message"]);
            if ($msg == "") {
                    $msg = "<i>Anonymous donation</i>";
            }
            $total += $row["amount"];
            $count++;

            $table.="<tr>";
            $table.="<td align='center'>".$row["id"]."</td>";
            $table.="<td align='center'>".date("jS F Y", $row["date_t"])."</td>";
            $table.="<td align='right'>&euro;&nbsp;".number_format($row["amount"],2)."</td>";
            $table.="<td>".$msg."</td>";
            $table.="</tr>";
    }
    $table.="</table><br/>";

    $percent=round($total * 100 / $goal);
    $percent=min($percent, 100);
    $graph_style = "width: 100%; height: 30px; border: 1px solid #888; background: rgb(204,204,204);
                    position: relative;";
    $bar_style = "height: 30px; background: rgb(68,132,242); width: ".$percent."%";
?>

<main class="container">

<h1>KDE Sprints 2015 fundraising</h1>

<?php /* <div class="formholder" style='float:right; width:40%;'>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="business" value="kde-ev-paypal@kde.org"> --> */ ?>

<?php
    echo "<p align='center'>€$total raised of a €$goal goal</p>";
    echo "<div style='$graph_style'><div style='$bar_style'></div></div>";
    echo "<br/>"

/*<!-- <input type="hidden" name="cmd" value="_donations" />
 <input type="hidden" name="lc" value="GB" />
 <input type="hidden" name="item_name" value="KDE e.V." />
 <input type="hidden" name="item_number" value="Donation to Randa Meetings 2015 KDE e.V." />
 <input type="hidden" name="currency_code" value="EUR" />
 <input type="hidden" name="cbt" value="Return to www.kde.org" />
 <input type="hidden" name="on0" value="Show Name on donor List" />
 <input type="hidden" name="no_note " value="1" />
 <input type="hidden" name="return" value="https://www.kde.org/fundraisers/kdesprints2015/thanks_paypal.php">
 <input type="hidden" name="notify_url" value="https://www.kde.org/fundraisers/kdesprints2015/notify.php">
 <div align="center">
 <input type='text' name="amount" value='40' style="vertical-align:middle;text-align:right;" size="4" /> €
 <input type="image" name="submit" src="https://www.paypal.com/en_US/i/btn/btn_donate_LG.gif" alt="PayPal"  style="vertical-align:middle;" />
 <br/>
 <br/>
 Show my name on the <a href="#donorlist">donor list</a> <select name="os0">
    <option value="Yes">Yes</option>
    <option value="No">No</option>
 </select>
 </div>
</form>
</div> -->*/
?>

<b>The Randa Meetings 2015 Fundraiser has finished. Thank you everybody who supported us in this fundraiser. We didn't reach the set goal but we collected quite some money and that means there will be more KDE Sprints thanks to your support! See <a href="http://planet.kde.org">http://planet.kde.org</a> for more information to come and go to the <a href="https://www.kde.org/community/donations/index.php">KDE donation page</a> if you want to support us further.</b>

<p><strong>[update]</strong> The fundraising campaign has been extended. We had intended to have ongoing updates from the Randa Meetings. However, developers were working rather than writing articles and blogposts. By extending the campaign, more information can be provided about what was actually accomplished.</p>

<p>For the sixth year, the KDE sprints in Randa, Switzerland will include key projects and top developers,  working concurrently under one roof, free from noise and distractions.</p>


<p> The week-long Randa Meetings have been intense and productive, generating exceptional results. Expectations are high again this year. Most importantly, the Randa Meetings have produced <a href="http://dot.kde.org/2011/06/29/platform-frameworks-kde-hackers-meet-switzerland">significant breakthroughs</a> that benefit all KDE software users and developers.</p>

<!--<ul>
  <li><a href="http://dot.kde.org/2011/06/29/platform-frameworks-kde-hackers-meet-switzerland">KDE Frameworks 5 were started at the Randa Meetings in 2011</a></li>
  <li><a href="http://www.kdenlive.org/users/j-b-m/randa-kde-sprint">Kdenlive came closer to KDE in 2011</a></li>
  <li><a href="http://dot.kde.org/2010/06/19/report-successful-multimedia-and-edu-sprint-randa">After a discussion with the Qt offices in Brisbane it was decided to keep Phonon and further develop it</a></li>
</ul>-->

<p>Randa Meetings participants donate their time to work on important KDE projects. The Randa Meetings organizers prepare meals and arrange breaks to maintain energy and effectiveness. Volunteer staff people take care of details that can interfere with the highly productive software development.</p>

<p>While the developers cover some of their own costs, there are still hard expenses like accommodation and travel to get the volunteer contributors to Randa. Having these expenses covered is a valuable incentive for Randa developers.</p>

<p>The theme of the 2015 Randa Meetings is <strong>Bring Touch to KDE</strong>. Participants will be working to expand touch capabilities in KDE applications for tablets, smartphones and other touch-enabled devices. These efforts will augment Plasma Mobile, the Free Mobile Platform <a href="https://dot.kde.org/2015/07/25/plasma-mobile-free-mobile-platform">announced on July 25th</a> at <a href="https://akademy.kde.org/2015">Akademy 2015</a>.</p>

<p>Some of the 2015 Randa projects are:</p>
<ul>
<li><a href="https://community.kde.org/KDEConnect">KDE Connect</a> integrated communication for multiple devices</li>
<li>KDE apps on Android</li>
<li><a href="https://www.digikam.org">digiKam</a> photo management</li>
<li><a href="https://www.kde.org/applications/education/gcompris/">GCompris</a> educational software</li>
<li>Interface design</li>
<li>
Personal Information Management</li>
<li><a href="https://www.kdenlive.org">Kdenlive</a> video editor</li>
<li>
KDE and mobile integration with <a href="https://owncloud.org">ownCloud</a> user-controlled cloud storage and applications</li>
<li>Security and privacy</li>
</ul>
The organizers have included other top developers so that the Randa Meetings produce extraordinary results.</p>


<script type="text/javascript">
    (function($) {
        var cache = [];
        // Arguments are image paths relative to the current page.
        $.preLoadImages = function() {
            var args_len = arguments.length;
            for (var i = args_len; i--;) {
            var cacheImage = document.createElement('img');
            cacheImage.src = arguments[i];
            cache.push(cacheImage);
            }
            }
    })(jQuery)

    $(document).ready(function() {
        jQuery.preLoadImages("randa1.jpg","randa2.jpg","randa3.jpg","randa4.jpg", "randa5.jpg");
        $('.images').cycle({
            fx: 'fade',
            timeout: 6000,
            speed: 500,
            next : $('#next'),
            prev : $('#previous')
        });

        var show = function(e) {
            var teaserPosition = $('.images').offset();
            var next = $('#next');
            var prev = $('#previous');
            var left = (teaserPosition.left + $('.images').width()) / 2;
            next.css({top:teaserPosition.top, left:left}).show();
            prev.css({top:teaserPosition.top+$('.images').height()-prev.height(), left:left}).show();
        };

        var hide = function(time) {
            $('#next').hide();
            $('#previous').hide();
        };

        var pause = function() {
            $('.images').cycle('pause');
        }

        var resume = function() {
            $('.images').cycle('resume');
        }

        $('.images').mouseover(show);
        $('.images').mouseout(hide);
        $('.images').mouseover(pause);
        $('.images').mouseout(resume);
        $('#next').mouseover(show);
        $('#previous').mouseover(show);
    });
</script>
<div class="images" style='display: block; margin: auto; float:right; width: 400px; height: 300px; '>
    <img width="400" style="position: relative" src="randa1.jpg" alt="" />
    <img width="400" style="display: none; position: relative" src="randa2.jpg" alt="" />
    <img width="400" style="display: none; position: relative" src="randa3.jpg" alt="" />
    <img width="400" style="display: none; position: relative" src="randa4.jpg" alt="" />
    <img width="400" style="display: none; position: relative" src="randa5.jpg" alt="" />
</div>


<p>This year, the Randa Meetings fundraising campaign has been expanded to include all KDE sprints. In addition to the Randa Meetings, various KDE work teams gather for a few days throughout the year to make exceptional progress that simply would not be possible working only via the Internet and email. As one long-time Randa participant said:
<blockquote>In <a href="https://en.wikipedia.org/wiki/Agile_software_development">agile development</a>, sprints are a very important element to push the project forward. While sprints can be done over the web, they are hindered by timezones, external distractions, availability of contributors, etc. In-person sprints are more productive as all the hindrances of the web meetings are eliminated and productivity is greatly enhanced overall.</blockquote></p>

<p>The results of sprints are directly reflected in the quality of KDE software for users. Consequently, sprints and similar high impact get-togethers are one of 
the primary uses of funds within KDE. This means that the KDE Community consistently offers the best of Free and Open technology for anyone to use.</p>

<p>To support development sprints, we have set a fundraising goal of €<?php echo $goal; ?>. Would you make a donation to support KDE development? Any amount will help and is appreciated.  <b>The fundraising campaign has been extended until September 30 so that the participants at the Randa Meetings can report on what they accomplished.</b></p>

<p>Donors can choose to receive a <b>memorable thank you</b> from the Randa Meetings. If you want to receive such a thank you memento, please send an email to <a href="mailto:fundraiser@kde.org">fundraiser@kde.org</a>. Include your name and full mailing address.</i></p>

<p>Links:</p>
<ul>
  <li><a href="https://sprints.kde.org">KDE Sprints webpage</a></li>
  <li><a href="http://www.randa-meetings.ch">Website of the Randa Meetings</a></li>
  <li><a href="http://sprints.kde.org/sprint/271">Participants and plans for the Randa Meetings 2015</a></li>
  <li><a href="http://community.kde.org/Sprints/Randa">More information about past and current Randa Meetings</a></li>
</ul>

<!-- FIXME: Put real link to dot story <p>Read more about the Randa Meetings and the Fundraising on <a href="http://dot.kde.org/2014/05/27/randa-moving-kde-forward">KDE.News</a> where you can write your comments.</p> -->


<!--
<p>Blog posts about what people plan to work on in Randa:</p>
<ul>
  <li>Blog 1</li>
  <li>Blog 2</li>
</ul>
-->

<p>All money received during this fundraising campaign will be used for KDE sprints, including the Randa Meetings.</p>

<p>If you prefer to use international bank transfers please <a href="/community/donations/others.php">see this page</a>.<br />
Please write us <a href="mailto:randa@kde.org">an email</a> so we can add you to the list of donors manually.</p>

<h3><a name="donorlist"></a>List of donations</h3>
<?php echo $table; ?>

</main>
<?php
  require('../../aether/footer.php');
