<?php
    require('../../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Donation received - Thank you!"
    ]);

    require('../../aether/header.php');
    $site_root = "../../";
?>

<main class="container">

<h1>Donation received - Thank you!</h1>

<p>
<?php i18n("Thank you very much for your donation to the Year End 2016 fundraiser!"); ?>
<br>
<br>
<?php i18n("In case your donation qualifies for a greeting card gift we will contact you at the beginning of December at your paypal email address to ask for the design you want and address you want to send them to."); ?>
<br>
<br>
<?php print i18n_var('Remember you can become a "KDE Supporting Member" by doing recurring donations. Learn more at <a href="%1">%2/</a>.', "https://relate.kde.org", "https://relate.kde.org"); ?>
<br>
<br>
<?php print i18n_var('You can see your donation on <a href="%1">the Year End 2016 fundraiser page</a>.', "index.php"); ?>
</p>

</main>
<?php
  require('../../aether/footer.php');
