<?php
    // Don't show the global donation square, it's confusing in this page
    $page_disablekdeevdonatebutton = true;

    include_once ("functions.inc");

    $translation_file = "kde-org";

    require('../../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => i18n_noop("Make the World a Better Place! - KDE End of Year 2016 Fundraising"),
        'cssFile' => '/css/announce.css'
    ]);

    require('../../aether/header.php');
    $site_root = "../../";

    require("www_config.php");

    $countStmt = $dbConnection->prepare("SELECT COUNT(*) FROM yearend2016donations ORDER BY CREATED_AT DESC;");
    $countStmt->execute();
    $n = $countStmt->fetchColumn();

    $res = $dbConnection->query("SELECT *, UNIX_TIMESTAMP(date) AS date_t FROM yearend2016donations ORDER BY date DESC;");

    $total = 0;
    $table = "<table border='1' width='100%'>";
    $table.="<tr><th width='20'></th><th width='180'>".i18n_var("Date")."</th><th width='80'>".i18n_var("Amount")."</th><th style='text-align:left'>".i18n_var("Donor Name")."</th></tr>";
    $index = $n;
    while ($row = $res->fetch()) {
        $msg = htmlspecialchars($row["message"]);
        if ($msg == "") {
            $msg = "<i>".i18n_var("Anonymous donation")."</i>";
        }
        $amount = $row["amount"];
        $table.="<tr>";
        $table.="<td align='center'>".$index."</td>";
        $table.="<td align='center'>".date("jS F Y", $row["date_t"])."</td>";
        $table.="<td align='right'>&euro;&nbsp;".number_format($amount,2)."</td>";
        $total += $amount;
        $table.="<td>".$msg."</td>";
        $table.="</tr>";
        $index--;
    }
    $table.="</table><br/>";
    $total = number_format($total, 0, ".", "&#8201;");
    
    // This is select from select seems mysql specific?
    $query = "select MIN(sum_amount) from ( select sum(amount) as sum_amount from yearend2016donations group by email order by sum_amount DESC LIMIT 9 ) as tops;";
    $limitStmt = $dbConnection->prepare($query);
    $limitStmt->execute();
    $limit_amount = $limitStmt->fetchColumn();

    $query = "select * from ( select email, sum(amount) as sum_amount from yearend2016donations group by email order by sum_amount ) as tops where sum_amount >= :limit_amount order by sum_amount DESC;";
    $q = $dbConnection->prepare($query);
    $q->execute(['limit_amount' => $limit_amount]);

    $top9table = "<table border='1' width='100%'>";
    $top9table.="<tr><th width='20'></th><th width='180'>".i18n_var("Total Amount")."</th><th style='text-align:left'>".i18n_var("Donor Name")."</th></tr>";
    $count = 0;
    $last_amount = -1;
    $last_was_same_amount = false;
    $q2 = $dbConnection->prepare('select distinct message as name from yearend2016donations where email=:email');
    while ($row = $q->fetch()) {
        $amount = $row["sum_amount"];
        $email = $row['email'];
        $q2->execute(['email' => $email]);
        $donation_count = 0;
        while ($row2 = $q2->fetch()) {
            $msg = $row2["name"];
            $donation_count++;
        }
        
        if ($donation_count !== 1) {
            $msg = "<i>".i18n_var("Anonymous donation")."</i>";
        }
        if ($msg == "") {
            $msg = "<i>".i18n_var("Anonymous donation")."</i>";
        }

        if ($amount == $last_amount) {
            // Do not touch index
        } else {
            // Set index to correct value
            $index = $count + 1;
        }

        $top9table.="<tr>";
        if ($amount == $limit_amount) {
            $top9table.="<td align='center'>".$index." *</td>";
        } else {
            $top9table.="<td align='center'>".$index."</td>";
        }
        $top9table.="<td align='center'>&euro;&nbsp;".number_format($amount,2)."</td>";
        $top9table.="<td>".$msg."</td>";
        $top9table.="</tr>";
        $last_amount = $amount;
        $count++;
    }
    $top9table.="</table><br/>";
    if ($count - $index == 0) {
        // This means that the last entry is a 9 and is not tied with anyone else, so change the 9 * to a simple 9
        $top9table = str_replace("<td align='center'>9 *</td>", "<td align='center'>9</td>", $top9table);
    } else if (10 - $index == $count - $index + 1) {
        // This means that the last entries are tied but they add up to 9 exactly so no random needed either
        $top9table = str_replace("<td align='center'>".$index." *</td>", "<td align='center'>".$index."</td>", $top9table);
    } else {
        $top9table.=i18n_var("* %1 art print winners will be randomly selected out of these %2 donors", 10 - $index, $count - $index + 1);
    }
?>

<script type="text/javascript">
        function changeItemNumber(elem) {
            if (elem.checked) {
                document.getElementById('item_number').value = "Donation to End of Year 2016 Campaign by KDE e.V.";
            } else {
                document.getElementById('item_number').value = "Anonymous donation to End of Year 2016 Campaign by KDE e.V.";
            }
        }
        $(document).ready(function() {
            changeItemNumber(document.getElementById("showdonorlist"));
        });
</script>

<main class="container">

<h1>Make the World a Better Place! - KDE End of Year 2016 Fundraising</h1>

<?php
    echo "<p class='current-amount' style='font-size:xx-large' align='center'>".i18n_var("%1 raised", "€&#8202;$total")."</p>";
?>

<?php
    $gmt_year = gmdate("Y");
    if ($gmt_year < 2017)
    {
?>

<div class="formholder" style='float:right;'>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" style='padding-left:1em; padding-right:1em;'>
 <input type="hidden" name="business" value="kde-ev-paypal@kde.org" />
 <input type="hidden" name="cmd" value="_donations" />
 <input type="hidden" name="lc" value="GB" />
 <input type="hidden" name="item_name" value="KDE e.V." />
 <input type="hidden" name="currency_code" value="EUR" />
 <input type="hidden" id="item_number" name="item_number" value="Donation to End of Year 2016 Campaign by KDE e.V." />
 <input type="hidden" name="cbt" value="Return to www.kde.org" />
 <input type="hidden" name="no_note " value="1" />
 <input type="hidden" name="return" value="https://www.kde.org/fundraisers/yearend2016/thanks_paypal.php" />
 <input type="hidden" name="notify_url" value="https://www.kde.org/fundraisers/yearend2016/notify.php" />
 <br/>
 <div align="center">
 <input type='text' name="amount" value='40' style="vertical-align:middle;text-align:right;" size="4" /> €
    <button style='cursor: pointer; background-color: #0070BB; border: 1px solid #0060AB; color: #FFF; height: 27px;' type='submit'><?php i18n("Donate"); ?></button>
    <br/>
    <br/>
 <input id="showdonorlist" type="checkbox" checked="checked" onclick="changeItemNumber(this)" /><?php print i18n_var("Show my name on the <a href='%1'>donor list</a>", "#donorlist");?>
 </div>
</form>
</div>

<?php
    }
    else
    {
        print i18n_var("The KDE End of Year 2016 Fundraiser has finished. Thank you everybody who supported us in this fundraiser. Please visit the <a href='%1'>KDE donation page</a> if you would like to support us further.", "https://www.kde.org/community/donations/index.php");
    }
?>

<p style="font-size:x-small">
<?php i18n("Also available in: "); ?> <a href="?site_locale=en">English</a> | <a href="?site_locale=ca">Català</a> | <a href="?site_locale=it">Italiano</a> | <a href="?site_locale=sv">Svenska</a> | <a href="?site_locale=uk">Українська</a>
</p>

<p>
<?php i18n("As we approach the end of the year, we begin the season of giving. What would suit the holidays better than giving to the entire world?"); ?>
</p>

<p>
<?php i18n("Here is a unique way to give back to KDE, allowing us to keep giving free software to humankind."); ?>
</p>

<p>
<?php i18n("KDE is committed to improving technology and software to make the world a better place. We produce great quality free software that everyone is free to use or modify without any cost or restriction."); ?>
</p>

<script type="text/javascript">
    (function($) {
        var cache = [];
        // Arguments are image paths relative to the current page.
        $.preLoadImages = function() {
            var args_len = arguments.length;
            for (var i = args_len; i--;) {
            var cacheImage = document.createElement('img');
            cacheImage.src = arguments[i];
            cache.push(cacheImage);
            }
            }
    })(jQuery)

    $(document).ready(function() {
        jQuery.preLoadImages("fundraiser-calligrasheets.jpg", "fundraiser-gwenview.jpg", "fundraiser-kalgebra.jpg", "fundraiser-kate.jpg", "fundraiser-klettres.jpg", "fundraiser-okular.jpg");
        $('.images').cycle({
            fx: 'fade',
            timeout: 6000,
            speed: 500,
            next : $('#next'),
            prev : $('#previous')
        });

        var show = function(e) {
            var teaserPosition = $('.images').offset();
            var next = $('#next');
            var prev = $('#previous');
            var left = (teaserPosition.left + $('.images').width()) / 2;
            next.css({top:teaserPosition.top, left:left}).show();
            prev.css({top:teaserPosition.top+$('.images').height()-prev.height(), left:left}).show();
        };

        var hide = function(time) {
            $('#next').hide();
            $('#previous').hide();
        };

        var pause = function() {
            $('.images').cycle('pause');
        }

        var resume = function() {
            $('.images').cycle('resume');
        }

        $('.images').mouseover(show);
        $('.images').mouseout(hide);
        $('.images').mouseover(pause);
        $('.images').mouseout(resume);
        $('#next').mouseover(show);
        $('#previous').mouseover(show);
    });
</script>
<p>
<div class="images">
    <img width="750" height="411" src="discover.jpg" alt="" />
    <img width="750" height="407" style="display: none;" src="okular.jpg" alt="" />
    <img width="750" height="411" style="display: none;" src="konqueror.jpg" alt="" />
    <img width="750" height="416" style="display: none;" src="kdevelop.jpg" alt="" />
    <img width="750" height="411" style="display: none;" src="dolphin.jpg" alt="" />
</div>
</p>

<p>
<?php i18n("We want to bring the solutions we are offering to the next level. By participating in this fundraiser, you'll be part of the improvements we'll put into our products, for example our educational software, so kids can have better tools for school; our productivity applications, so you have the best tools for the workplace; and our desktop so we can all have a fun and productive experience when interacting with our computers."); ?>
</p>

<p><b><em>
<?php i18n("Donating to KDE is not for you, it is for the entire world."); ?>
</em></b></p>

<p>
<?php print i18n_var("As a way to say thank you, starting with %1 we will send a KDE-themed postcard, designed by our community of artists, to an address of your choice. You will get an extra card for every additional %2 donation. Get cards for yourself and for your family and friends to show them you care for freedom. It's the perfect way to spread the festive cheer and donate to your favorite project at the same time.", "<em id='card'>€&#8202;30</em>", "<em id='extracard'>€&#8202;10</em>"); ?>
</p>

<p>
<a href="postcard01.png"><img height="200" src="postcard01_small.jpg" alt="Postcard Design #1" /></a>
<a href="postcard02.png"><img width="200" src="postcard02_small.jpg" alt="Postcard Design #2" /></a>
<br />
<a href="postcard03.png"><img height="200" src="postcard03_small.jpg" alt="Postcard Design #3" /></a>
<a href="postcard04.png"><img width="200" src="postcard04_small.jpg" alt="Postcard Design #4" /></a>
</p>

<p>
<?php print i18n_var("In addition we would like to give a special thank you to our highest donors. We are giving away 9 <b>unique</b> art prints in size A4. At our last conference, QtCon, we asked 46 prominent current and former KDE contributors (including core developers from Plasma, KDE Edu and Qt, board members, designers, community organizers, translators and more) to sign artwork created for KDE's 20th birthday. This is a one-time chance to get a nice and unique KDE print for your wall."); ?>
</p>

<p>
<img height="117" src="art1.jpg" alt="Art Sample #1" /></a>
<img width="208" src="art2.jpg" alt="Art Sample #2" /></a>
</p>

<p>
<?php print i18n_var("The top 9 donors are calculated aggregating by paypal address, make sure you use the same account if you donate multiple times."); ?>
</p>

<p>
<?php i18n("This campaign will end on December 31st 2016 at 23:59 UTC."); ?>
</p>

<h3><?php i18n("Where Your Donations Go"); ?></h3>
<p>
<?php print i18n_var("Last year KDE spent about %1 on travel and accomodation of more than 100 contributors for various sprints throughout the year.", "€&#8202;50&thinsp;000"); ?>
</p>

<p>
<?php print i18n_var("<a href='%1'>Sprints</a> are in person meetings and are really important for a team of hard-working volunteers around the world to focus their efforts and discuss technical matters around the project. The amount of output we get from the sprints is really worthwhile for all users of the software.", "https://community.kde.org/Sprints"); ?>
</p>

<p>
<?php print i18n_var("Remaining money gets spent on our infrastructure, we have a large portfolio of servers hosting websites, code, continuous integration and a lot more. A full breakdown can be seen in our <a href='%1'>quarterly reports</a>.", "http://ev.kde.org/reports/"); ?>
</p>

<h4><?php i18n("Notes:"); ?></h4>
<ul>
    <li><?php i18n("We will use your paypal email address to contact you and ask for the addresses to send the postcards."); ?></li>
    <li><?php i18n("You will be able to choose between the generic thank you or custom text for the postcard."); ?></li>
    <li><?php i18n("The first shipment of cards will be December 8th, after that there will be a shipment every week until the end of the Fundraiser."); ?></li>
    <li><?php i18n("If there is a tie in the last position of the top 9 donors we'll let random.org select the winner amongst the tied donors."); ?></li>
</ul>

<p>
<?php print i18n_var("If you prefer to use international bank transfers please <a href='%1'>see this page</a>.", "/community/donations/others.php#moneytransfer"); ?>
<br />
<?php print i18n_var("Please write us <a href='%1'>an email</a> so we can add you to the list of donors manually.", "mailto:kde-ev-campaign@kde.org"); ?>
</p>

<h3><a name="topdonorlist"></a><?php i18n("Top 9 donors"); ?></h3>
<?php echo $top9table; ?>

<h3><a name="donorlist"></a><?php i18n("List of donations"); ?></h3>
<?php echo $table; ?>

</main>
<?php
  require('../../aether/footer.php');
