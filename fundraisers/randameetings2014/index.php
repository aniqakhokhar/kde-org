<?php

  // Don't show the global donation square, it's confusing in this page
  $page_disablekdeevdonatebutton = true;

  $page_title="Randa Meetings 2014 fundraising";

  $goal=20000;
//   $teaser=true;
//   $teaserFiles = array("randa1.jpg", "randa2.jpg", "randa3.jpg", "randa4.jpg");
    require('../../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Randa Meetings 2014 fundraising",
        'cssFile' => '/css/announce.css'
    ]);

    require('../../aether/header.php');
    $site_root = "../../";

    require("www_config.php");

    $res = $dbConnection->prepare("SELECT *, UNIX_TIMESTAMP(date) AS date_t FROM randameetings2014donations ORDER BY date DESC;");
    $res->execute();
    $total = 0;
    $count = 0;
    $table = "<table border='1' width='100%'>";
    $table.="<tr><th width='20'>No.</th><th width='100'>Date</th><th width='80'>Amount</th><th style='text-align:left'>Donor Name</th></tr>";
    while ($row = $res->fetch()) {
            $msg = htmlspecialchars($row["message"]);
            if ($msg == "") {
                    $msg = "<i>Anonymous donation</i>";
            }
            $total += $row["amount"];
            $count++;

            $table.="<tr>";
            $table.="<td align='center'>".$row["id"]."</td>";
            $table.="<td align='center'>".date("jS F Y", $row["date_t"])."</td>";
            $table.="<td align='right'>&euro;&nbsp;".number_format($row["amount"],2)."</td>";
            $table.="<td>".$msg."</td>";
            $table.="</tr>";
    }
    $table.="</table><br/>";

    $percent=round($total * 100 / $goal);
    $percent=min($percent, 100);
    $graph_style = "width: 100%; height: 30px; border: 1px solid #888; background: rgb(204,204,204);
                    position: relative;";
    $bar_style = "height: 30px; background: rgb(68,132,242); width: ".$percent."%";
?>

<main class="container">

<h1>Randa Meetings 2014 fundraising</h1>

<?php /* <!--<div class="formholder" style='float:right; width:40%;'>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="business" value="kde-ev-paypal@kde.org">--> */ ?>

<?php
    echo "<p align='center'>€$total raised of a €$goal goal</p>";
    echo "<div style='$graph_style'><div style='$bar_style'></div></div>";
    echo "<br/>"
/*<!-- <input type="hidden" name="cmd" value="_donations" />
 <input type="hidden" name="lc" value="GB" />
 <input type="hidden" name="item_name" value="KDE e.V." />
 <input type="hidden" name="item_number" value="Donation to Randa Meetings 2014 KDE e.V." />
 <input type="hidden" name="currency_code" value="EUR" />
 <input type="hidden" name="cbt" value="Return to www.kde.org" />
 <input type="hidden" name="on0" value="Show Name on donor List" />
 <input type="hidden" name="no_note " value="1" />
 <input type="hidden" name="return" value="https://www.kde.org/fundraisers/randameetings2014/thanks_paypal.php">
 <input type="hidden" name="notify_url" value="https://www.kde.org/fundraisers/randameetings2014/notify.php">
 <div align="center">
 <input type='text' name="amount" value='40' style="vertical-align:middle;text-align:right;" size="4" /> €
 <input type="image" name="submit" src="https://www.paypal.com/en_US/i/btn/btn_donate_LG.gif" alt="PayPal"  style="vertical-align:middle;" />
 <br/>
 <br/>
 Show my name on the <a href="#donorlist">donor list</a> <select name="os0">
    <option value="Yes">Yes</option>
    <option value="No">No</option>
 </select>
 </div>
</form>
</div>--> */ ?>

<b>The Randa Meetings 2014 Fundraiser has finished. Thank you everybody who supported us in this fundraiser. We didn't reach the set goal but we collected quite some money and that means there is going to be Randa Meetings in August 2014. See <a href="http://planet.kde.org">http://planet.kde.org</a> for more information to come and go to the <a href="https://www.kde.org/community/donations/index.php">KDE donation page</a> if you want to support us further.</b>

<p>For the fifth year, the intense sprints in Randa, Switzerland will include key KDE projects and top developers, all working concurrently under one roof, isolated from noise and distractions.</p>

<p>Previous Randa Meetings have been concentrated and productive, generating exceptional results; organizers and participants expect the same and more this year. And the meetings have produced significant breakthroughs for KDE software users and developers.</p>

<ul>
  <li><a href="http://dot.kde.org/2011/06/29/platform-frameworks-kde-hackers-meet-switzerland">KDE Frameworks 5 were started at the Randa Meetings in 2011</a></li>
  <li><a href="http://www.kdenlive.org/users/j-b-m/randa-kde-sprint">Kdenlive came closer to KDE in 2011</a></li>
  <li><a href="http://dot.kde.org/2010/06/19/report-successful-multimedia-and-edu-sprint-randa">After a discussion with the Qt offices in Brisbane it was decided to keep Phonon and further develop it</a></li>
</ul>

<p>Participants donate their time to help improve the software you love and this is why we need money to cover hard expenses like accommodation and travel to get the volunteer contributors to Randa. If you are not attending, you can still support the Randa Meetings by making a donation. As in the past, the Randa Meetings will benefit everyone who uses KDE software.</p>

<p>We have a fundraising goal of €<?php echo $goal; ?>. Please donate what you can to help make the Randa Meetings 2014 possible. This fund campaign ends on the 9th of July (one month before the beginning of the Randa Meetings 2014).</p>


<script type="text/javascript">
    (function($) {
        var cache = [];
        // Arguments are image paths relative to the current page.
        $.preLoadImages = function() {
            var args_len = arguments.length;
            for (var i = args_len; i--;) {
            var cacheImage = document.createElement('img');
            cacheImage.src = arguments[i];
            cache.push(cacheImage);
            }
            }
    })(jQuery)

    $(document).ready(function() {
        jQuery.preLoadImages("randa1.jpg","randa2.jpg","randa3.jpg","randa4.jpg");
        $('.images').cycle({
            fx: 'fade',
            timeout: 6000,
            speed: 500,
            next : $('#next'),
            prev : $('#previous')
        });

        var show = function(e) {
            var teaserPosition = $('.images').offset();
            var next = $('#next');
            var prev = $('#previous');
            var left = (teaserPosition.left + $('.images').width()) / 2;
            next.css({top:teaserPosition.top, left:left}).show();
            prev.css({top:teaserPosition.top+$('.images').height()-prev.height(), left:left}).show();
        };

        var hide = function(time) {
            $('#next').hide();
            $('#previous').hide();
        };

        var pause = function() {
            $('.images').cycle('pause');
        }

        var resume = function() {
            $('.images').cycle('resume');
        }

        $('.images').mouseover(show);
        $('.images').mouseout(hide);
        $('.images').mouseover(pause);
        $('.images').mouseout(resume);
        $('#next').mouseover(show);
        $('#previous').mouseover(show);
    });
</script>
<div class="images" style='display: block; margin: auto; float:right;'>
    <img width="400" src="randa1.jpg" alt="" />
    <img width="400" style="display: none;" src="randa2.jpg" alt="" />
    <img width="400" style="display: none;" src="randa3.jpg" alt="" />
    <img width="400" style="display: none;" src="randa4.jpg" alt="" />
</div>


<p>Randa Meetings 2014 projects:</p>
<ul>
  <li><a href="http://amarok.kde.org/">Amarok/Multimedia/Phonon</a></li>
  <li><a href="http://mail.kde.org/pipermail/kde-frameworks-devel/2014-April/014575.html">KDE Books</a></li>
  <li><a href="http://edu.kde.org">KDE Edu</a> and <a href="http://www.gcompris.net">GCompris</a></li>
  <li><a href="http://community.kde.org/Frameworks/Porting_Notes">KDE Frameworks 5 porting</a></li>
  <li><a href="http://www.proli.net/2014/03/19/kde-sdk-next-how-will-we-develop-in-a-year/">KDE SDK</a></li>
  <li><a href="http://www.kdenlive.org">Kdenlive</a></li>
  <li><a href="http://userbase.kde.org/Gluon">Gluon</a></li>
</ul>

<p>Links:</p>
<ul>
  <li><a href="http://www.randa-meetings.ch">Website of the Randa Meetings</a></li>
  <li><a href="http://sprints.kde.org/sprint/212">Participants and plans for the Randa Meetings 2014</a></li>
  <li><a href="http://community.kde.org/Sprints/Randa">More information about past and current Randa Meetings</a></li>
</ul>

<p>Read more about the Randa Meetings and the Fundraising on <a href="http://dot.kde.org/2014/05/27/randa-moving-kde-forward">KDE.News</a> where you can write your comments.</p>

<p>The costs for the Randa Meetings 2014 are composed of the following items:</p>
<ul>
  <li>International travel costs: EUR 10'000</li>
  <li>Swiss train tickets: EUR 3'000</li>
  <li>House rental: EUR 4'000</li>
  <li>Expense allowance: EUR 2'000</li>
  <li>Miscellaneous: EUR 1'000</li>
</ul>

<!--
<p>Blog posts about what people plan to work on in Randa:</p>
<ul>
  <li>Blog 1</li>
  <li>Blog 2</li>
</ul>
-->

<p>Even if we don't reach our ambitious goal, the money received will be spent for the Randa Meetings (although with fewer participants) or other KDE purposes (like other sprints).</p>

<p>If you prefer to use international bank transfers please <a href="/community/donations/#moneytransfer">see this page</a>.<br />
Please write us <a href="mailto:randa@kde.org">an email</a> so we can add you to the list of donors manually.</p>

<h3><a name="donorlist"></a>List of donations</h3>
<?php echo $table; ?>

</main>
<?php
  require('../../aether/footer.php');
