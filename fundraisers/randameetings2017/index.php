  <!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:url" content="https://www.kde.org/fundraisers/randameetings2016/"/>
    <meta property="og:image" content="https://www.kde.org/fundraisers/randameetings2016/images/fundraising2017-ogimage.jpg"/>
    <meta property="og:type" content="website"/>
    <meta property="og:description" content="KDE is one of the biggest free software communities in the world and has been delivering high quality technology for nearly two decades. Randa Meetings is the largest sprint organized by KDE and, in 2017, it will be centered on bringing KDE technology on every device. This fundraising campaign aims at supporting the continuity of KDE efforts, over the year, towards this goal."/>
    <link rel="shortcut icon" href="images/kde-mini.png" />
    <title>KDE &#8210; Randa Meetings 2017 Fundraising Campaign</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>

<body class="top-navigation" data-spy="scroll" data-target="#navbar" onload="fixNavbarOffset()">

    <?php
        $page_title="Randa Meetings 2017 fundraising";

        $goal_fmt=number_format(20000);
        $goal=20000;
        $enddate="2017-09-30";
        $daystogo=floor((strtotime($enddate)-time())/(60*60*24));

        require_once("www_config.php");

        $countStmt = $dbConnection->prepare("SELECT COUNT(*) FROM randameetings2017donations ORDER BY CREATED_AT DESC;");
        $countStmt->execute();
        $n = $countStmt->fetchColumn();

        $res = $dbConnection->prepare("SELECT *, UNIX_TIMESTAMP(CREATED_AT) AS date_t FROM randameetings2017donations ORDER BY CREATED_AT DESC;");
        $res->execute();
        $total = 0;
        $count = $n;
        $n = $count;
        $table = "";
        while ($row = $res->fetch()) {
                $name = htmlspecialchars($row["donor_name"]);
                if ($name == "")
                    $name = "<i>Anonymous donation</i>";
                $total += $row["amount"];

                $table.="<tr>";
                $table.="<td>".$n."</td>";
                $table.="<td>".date("jS F Y", $row["date_t"])."</td>";
                $table.="<td>&euro;".number_format($row["amount"],2)."</td>";
                $table.="<td>".$name."</td>";
                $table.="</tr>";
                $n--;
        }
        $table.="";
        $dbConnection = null;
        $percent=round($total * 100 / $goal);
        $percent=min($percent, 100);
        $graph_style = "width: 100%; height: 30px; border: 1px solid #888; background: rgb(204,204,204);
                        position: relative;";
        $bar_style = "height: 30px; background: rgb(68,132,242); width: ".$percent."%";
    ?>

    <div id="wrapper">
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom white-bg">
                <nav class="navbar navbar-fixed-top" role="navigation">
                    <div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                            <i class="fa fa-reorder"></i>
                        </button>
                        <a href="https://ev.kde.org/" class="navbar-brand" target="_blank">
                            <img class="img-responsive" src="images/logo.png" alt="logo"/>
                        </a>
                    </div>
                    <div class="navbar-collapse collapse" id="navbar">
                        <ul class="nav navbar-nav">
                            <li class="active">
                                <a aria-expanded="false" role="button" href="#">Randa Meetings 2017 Fundraising Campaign</a>
                            </li>
                            <li>
                                <a role="button" href="#about"> About </a>
                            </li>
                            <li>
                                <a role="button" href="#donations"> Donations </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <br/><br/>
            <div class="wrapper wrapper-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="ibox float-e-margins">
                                <img class="img-responsive" src="images/banner-fundraising2017.jpg" alt="banner"/>
                            </div>
                        </div>
                    </div>
                    <?php
                      if(isset($_GET["returning"]) && trim($_GET["returning"]) == 'true') {
                        $alert="<div class='row'>";
                        $alert.="<div class='col-md-12'>";
                        $alert.="<div class='alert alert-info text-center'>";
                        $alert.="<h2>Thanks for donating to Randa Meetings 2017 Fundraising Campaign!</h2>";
                        $alert.="</div></div></div>";
                        echo $alert;
                      }
                    ?>
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class='alert alert-info text-center'>
                                <h5><strong>[update]</strong> The Randa Meetings 2017 Fundraiser has finished. Thank you everybody who supported us in this fundraiser. We didn't reach the set goal but we collected quite some money and that means there will be more KDE Sprints thanks to your support! <a href="http://planet.kde.org" target="_blank">Planet KDE</a> for more information to come and go to the <a href="https://www.kde.org/community/donations/index.php" target="_blank"></h5>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Randa Meetings 2017 - Making KDE more accessible!</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content text-center">
                                  <br/>
                                  <img class="embed-responsive-item" src="images/konqi-app-dev.png"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Status</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins pull-right text-success"><i class="fa fa-users"></i></h1>
                                    <h1 class="no-margins"><?php echo $count; ?></h1>
                                    <small>Total backers</small>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins pull-right text-success"><i class="fa fa-tachometer"></i></h1>
                                    <h1 class="no-margins current-amount">&euro;<?php echo round($total,2); ?></h1>
                                    <small>Pledged of &euro;<?php echo $goal_fmt; ?> goal</small>
                                    <p></p>
                                    <h2><?php echo $percent; ?>%</h2>
                                    <div class="progress progress-mini">
                                        <?php echo "<div style='width: ".$percent."%;' class='progress-bar'></div>"; ?>
                                    </div>
                                </div>

                                <!-- <div class="ibox-content">
                                    <h1 class="no-margins pull-right text-success"><i class="fa fa-calendar"></i></h1>
                                    <h1 class="no-margins"><?php echo $daystogo; ?></h1>
                                    <small>Days to go</small>
                                </div> -->
                                <!-- <div class="ibox-content">
                                    <h1 class="no-margins pull-right text-success"><i class="fa fa-heart"></i></h1>
                                    <a data-toggle="modal" class="btn btn-success" href="#modal-form">Back this campaign</a>

                                    <div id="modal-form" class="modal fade" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12 b-r"><h3 class="m-t-none m-b">Donate to Randa Meetings 2017 Fundraising Campaign</h3>
                                                            <p>Your support is quite appreciated!</p>
                                                            <form role="form" action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                                                <input type="hidden" name="business" value="kde-ev-paypal@kde.org">
                                                                <input type="hidden" name="cmd" value="_donations" />
                                                                <input type="hidden" name="lc" value="GB" />
                                                                <input type="hidden" name="item_name" value="KDE e.V. - Donation to Randa Meetings 2017 Fundraising Campaign" />
                                                                <input type="hidden" name="currency_code" value="EUR" />
                                                                <input type="hidden" name="cbt" value="Return to www.kde.org" />
                                                                <input type="hidden" name="on0" value="Show name on donor List" />
                                                                <input type="hidden" name="no_note" value="1" />
                                                                <input type="hidden" name="return" value="https://www.kde.org/fundraisers/randameetings2017/index.php?returning=true">
                                                                <input type="hidden" name="notify_url" value="https://www.kde.org/fundraisers/randameetings2017/notify.php">

                                                                <div class="form-group"><label>Amount (&euro;)</label> <input type="text" class="form-control" name="amount" value='50'></div>

                                                                <div class="form-group">
                                                                    <label>Show my name on the <a href="#donations">donors list</a>?</label>
                                                                    <div class="i-checks"><label><input type="radio" value="Yes" name="os0"> <i></i> Yes </label></div>
                                                                    <div class="i-checks"><label><input type="radio" value="No" name="os0"> <i></i> No </label></div>
                                                                </div>

                                                                <div>
                                                                    <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Donate</strong></button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row anchor" id="about">
                        <div class="col-md-7">
                          <!-- About the campign  -->
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>About this fundraising campaign</h5>
                                </div>
                                <div class="ibox-content">
                                    <p><a href="http://randa-meetings.ch/" target="_blank"><strong>Randa Meetings</strong></a> are annual KDE development sprints that have typically been dedicated to bringing our technology to different operating systems and form factors. This year, Randa Meetings are all about accessibility. But what do we mean by that?</p>
<p>At <strong><a href="http://kde.org">KDE</a></strong>, we understand technology is not always easy, and different conditions and abilities make our software harder to use. As we want Free Software to be universal, we must strive to put KDE apps into everybody's hands, including yours.</p>
<p>We want to focus on things that tend to fall by the wayside; problems that are annoying, but not for everyone.</p>
                                    </div>
                            </div>
                            <!-- end of About -->
                                <!-- How Are We doing that? -->
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>How are we doing that?</h5>
                                </div>
                                <div class="ibox-content">
                                    <p>During Randa Meetings 2017, we will gather in the quietness of the Swiss mountains and push several different projects in that direction. David Edmundson, for example, plans to spend his time on improving navigation on Plasma for those who prefer to use a keyboard over a mouse. This will help users with reduced mobility that find moving a mouse cumbersome. And Adriaan de Groot will be working on Calamares, an application that helps install operating systems. Adriaan will make Calamares more accessible to visually impaired users by improving integration with the Orca screenreader.</p>

                                    <p>Apart from the two projects mentioned above, we will also have developers from
                                    <a href="http://www.gcompris.net/index-en.html">GCompris</a>, <a href="https://kdenlive.org/">Kdenlive</a>, <a href="http://www.kubuntu.org/">Kubuntu</a>, <a href="https://kmymoney.org/">KMyMoney</a>, <a href="https://www.kde.org/applications/office/kontact/">Kontact</a>, <a href="https://kube.kde.org">Kube</a>, <a href="https://atelier.kde.org">Atelier</a>,
                                    <a href="https://edu.kde.org/"> KDEEdu</a>, <a href="https://www.digikam.org/">digiKam</a>, <a href="https://www.wikitolearn.org/">WikiToLearn</a>, and <a href="https://krita.org">Krita</a>, all working together, solving the most
                                    annoying accessibility issues.</p>
                                </div>
                            </div>
                            <!-- end of How Are We doing that? -->

                            <!-- About KDE -->
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>About KDE</h5>
                                </div>
                                <div class="ibox-content">
                                  <p>As one of the biggest Free Software communities in the world, KDE has been delivering high-quality technology and spreading the principles of hacker culture for nearly two decades. KDE brings together users, developers, maintainers, translators and many more contributors from across six continents and over fifty countries, all of them working with the bonds and spirits of a truthful community.</p>
                                  <p><strong>KDE</strong> has a vision:</p>
                                  <div class='row'>
                                      <div class='col-md-12'>
                                          <div class='alert alert-info text-center'>
                                              <h5>A world in which everyone has control over their digital life and enjoys freedom and privacy.</h5>
                                          </div>
                                      </div>

                                    </div>
                                    <p>To make Randa Meetings 2017 possible, we need your help. Please donate so we can make KDE more accessible for everyone.</p>
                                </div>
                              </div>
                              <!-- end of About KDE -->
                        </div>
                        <div class="col-md-5">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Randa Meetings 2016 &#8210; Selected Pictures</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="carousel slide" id="carousel1">
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <a href="images/2016groupPhoto.jpg" title="Randa Meetings 2016 Group Photo" data-gallery=""><img alt="image" class="img-responsive" src="images/2016groupPhoto.jpg"></a>
                                            </div>
                                            <div class="item">
                                                <a href="images/1.jpg" title="Full Concentration" data-gallery=""><img alt="image" class="img-responsive" src="images/1.jpg"></a>
                                            </div>
                                            <div class="item">
                                                <a href="images/lunch.jpg" title="Barbecue Time!" data-gallery=""><img alt="image" class="img-responsive" src="images/lunch.jpg"></a>
                                            </div>
                                            <div class="item">
                                                <a href="images/2.jpg" title="Marble in Action" data-gallery=""><img alt="image" class="img-responsive" src="images/2.jpg"></a>
                                            </div>
                                            <div class="item">
                                                <a href="images/4.jpg" title="Cooperation Unites All of Us" data-gallery=""><img alt="image" class="img-responsive" src="images/4.jpg"></a>
                                            </div>
                                            <div class="item">
                                                <a href="images/7.jpg" title="" data-gallery=""><img alt="image" class="img-responsive" src="images/7.jpg"></a>
                                            </div>
                                            <div class="item">
                                                <a href="images/8.jpg" title="Field trip!" data-gallery=""><img alt="image" class="img-responsive" src="images/8.jpg"></a>
                                            </div>
                                            <div class="item">
                                                <a href="images/3.jpg" title="The Perfect Atmosphere" data-gallery=""><img alt="image" class="img-responsive" src="images/3.jpg"></a>
                                            </div>
                                            <div class="item">
                                                <a href="images/6.jpg" title="Randa Meetings Rocks" data-gallery=""><img alt="image" class="img-responsive" src="images/6.jpg"></a>
                                            </div>
                                            <div id="blueimp-gallery" class="blueimp-gallery">
                                                <div class="slides"></div>
                                                <h3 class="title"></h3>
                                                <a class="prev">‹</a>
                                                <a class="next">›</a>
                                                <a class="close">×</a>
                                                <a class="play-pause"></a>
                                                <ol class="indicator"></ol>
                                            </div>
                                        </div>
                                        <a data-slide="prev" href="#carousel1" class="left carousel-control">
                                            <span class="icon-prev"></span>
                                        </a>
                                        <a data-slide="next" href="#carousel1" class="right carousel-control">
                                            <span class="icon-next"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Randa Meetings 2015 &#8210; Short Presentations</h5>
                                </div>
                                <div class="ibox-content">
                                    <br/>
                                    <div class="tabs-container">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#tab-1"><small>Calligra</small></a></li>
                                            <li class=""><a data-toggle="tab" href="#tab-2"><small>CI</small></a></li>
                                            <li class=""><a data-toggle="tab" href="#tab-3"><small>GCompris</small></a></li>
                                            <li class=""><a data-toggle="tab" href="#tab-4"><small>KDE on Android</small></a></li>
                                            <li class=""><a data-toggle="tab" href="#tab-5"><small>Plasma</small></a></li>
                                            <li class=""><a data-toggle="tab" href="#tab-6"><small>QML Web</small></a></li>
                                            <li class=""><a data-toggle="tab" href="#tab-7"><small>VDG</small></a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="tab-1" class="tab-pane active">
                                                <div class="panel-body">
                                                    <figure>
                                                        <video id="my_video_1" controls data-setup="{}">
                                                            <source src="https://files.kde.org/randa/Calligra.mp4" type='video/mp4'>
                                                        </video>
                                                    </figure>
                                                </div>
                                            </div>
                                            <div id="tab-2" class="tab-pane">
                                                <div class="panel-body">
                                                    <figure>
                                                        <video id="my_video_2" controls data-setup="{}">
                                                            <source src="https://files.kde.org/randa/Continuous%20Integration.mp4" type='video/mp4'>
                                                        </video>
                                                    </figure>
                                                </div>
                                            </div>
                                            <div id="tab-3" class="tab-pane">
                                                <div class="panel-body">
                                                    <figure>
                                                        <video id="my_video_3" controls data-setup="{}">
                                                            <source src="https://files.kde.org/randa/GCompris.mp4" type='video/mp4'>
                                                        </video>
                                                    </figure>
                                                </div>
                                            </div>
                                            <div id="tab-4" class="tab-pane">
                                                <div class="panel-body">
                                                    <figure>
                                                        <video id="my_video_4" controls data-setup="{}">
                                                            <source src="https://files.kde.org/randa/KDEonAndroid.mp4" type='video/mp4'>
                                                        </video>
                                                    </figure>
                                                </div>
                                            </div>
                                            <div id="tab-5" class="tab-pane">
                                                <div class="panel-body">
                                                    <figure>
                                                        <video id="my_video_5" controls data-setup="{}">
                                                            <source src="https://files.kde.org/randa/Plasma.mp4" type='video/mp4'>
                                                        </video>
                                                    </figure>
                                                </div>
                                            </div>
                                            <div id="tab-6" class="tab-pane">
                                                <div class="panel-body">
                                                    <figure>
                                                        <video id="my_video_6" controls data-setup="{}">
                                                            <source src="https://files.kde.org/randa/QMLweb.mp4" type='video/mp4'>
                                                        </video>
                                                    </figure>
                                                </div>
                                            </div>
                                            <div id="tab-7" class="tab-pane">
                                                <div class="panel-body">
                                                    <figure>
                                                        <video id="my_video_7" controls data-setup="{}">
                                                            <source src="https://files.kde.org/randa/VDG.mp4" type='video/mp4'>
                                                        </video>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Randa Meetings 2016 &#8210; What did we do?</h5>
                                </div>
                                <div class="ibox-content">
                                    <br/>
                                    <div class="tabs-container">
                                        <div class="alert alert-success text-center">
                                            Read the <a class="alert-link" href="https://community.kde.org/Sprints/Randa/2016#Blog_posts_and_other_news_about_the_meeting" target="_blank">Randa Meetings 2016 Blog Posts!</a>!
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row anchor" id="helpexamples">
                        <div class="col-md-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Help make this vision come true!</h5>
                                </div>
                                <div class="ibox-content">
                                    <p>Some examples of the direct connection between your donation and the realization of KDE technology on every device:</p>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="widget style1 navy-bg">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <i class="fa fa-user fa-5x"></i>
                                                    </div>
                                                    <div class="col-md-10 text-right">
                                                        <span>You + five friends support a Randa Meetings 2017's participant</span>
                                                        <h2 class="font-bold">&euro;50 each</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="widget style1 lazur-bg">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <i class="fa fa-server fa-5x"></i>
                                                    </div>
                                                    <div class="col-md-10 text-right">
                                                        <span>You + a friend support our CI and development infrastructure</span>
                                                        <h2 class="font-bold">&euro;100 each</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="widget style1 yellow-bg">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <i class="fa fa-gear fa-5x"></i>
                                                    </div>
                                                    <div class="col-md-10 text-right">
                                                        <span>You keep the ball rolling by supporting other sprints by the end of the year</span>
                                                        <h2 class="font-bold">&euro;300</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>All donations to Randa Meetings 2017 Fundraising Campaign are one-time donations!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row anchor" id="donations">
                        <div class="col-md-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>List of donations</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date </th>
                                                <th>Amount </th>
                                                <th>Donor Name </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <?php echo $table; ?>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="row text-center">
            <a href="https://ev.kde.org/" target="_blank">
                <img src="images/logo.png" alt=""/>
            </a>
        </div>
        <div class="social-icons">
            <ul>
                <li>
                    <a class="envelope" href="mailto:kde-ev-board@kde.org">
                        <i class="fa fa-envelope" title="Contact the KDE e.V. Board of Directors"></i>
                    </a>
                </li>
                <li>
                    <a class="twitter" href="https://twitter.com/kdecommunity" target="_blank">
                        <i class="fa fa-twitter" title="The KDE Community - Twitter"></i>
                    </a>
                </li>
                <li>
                    <a class="dribbble" href="https://plus.google.com/105126786256705328374/" target="_blank">
                        <i class="fa fa-google-plus" title="The KDE Community - Google+"></i>
                    </a>
                </li>
                <li>
                    <a class="facebook" href="https://www.facebook.com/kde/" target="_blank">
                        <i class="fa fa-facebook" title="The KDE Community - Facebook"></i>
                    </a>
                </li>
                <li>
                    <a class="linkedin" href="https://dot.kde.org" target="_blank">
                        <i class="fa fa-newspaper-o" title="dot.kde.org"></i>
                    </a>
                </li>
            </ul>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12 text-center">
                  The Randa Meetings 2017 fundraising campaign's banner on top of this page is a work of KDE VDG Member Andres Betts, used under <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank">CC BY 4.0</a>. This banner is licensed under <a href="http://creativecommons.org/licenses/by/4.0" target="_blank">CC BY 4.0</a> by Andres Betts.
            </div>
            <div class="col-md-12 text-center">
                KDE<sup>&#174;</sup> and <a href="/media/images/trademark_kde_gear_black_logo.png">the K Desktop Environment<sup>&#174;</sup> logo</a> are registered trademarks of <a href="http://ev.kde.org/" title="Homepage of the KDE non-profit Organization">KDE e.V.</a> | <a href="http://www.kde.org/community/whatiskde/impressum.php">Legal</a>
            </div>
        </div>
  </div>
    <!-- Mainly scripts -->
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- Flot -->
    <script src="js/plugins/flot/jquery.flot.js"></script>
    <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="js/plugins/flot/jquery.flot.resize.js"></script>

    <!-- ChartJS-->
    <script src="js/plugins/chartJs/Chart.min.js"></script>

    <!-- Peity -->
    <script src="js/plugins/peity/jquery.peity.min.js"></script>
    <!-- Peity demo -->
    <script src="js/demo/peity-demo.js"></script>


    <script>
        $(document).ready(function() {


            var d1 = [[1262304000000, 6], [1264982400000, 3057], [1267401600000, 20434], [1270080000000, 31982], [1272672000000, 26602], [1275350400000, 27826], [1277942400000, 24302], [1280620800000, 24237], [1283299200000, 21004], [1285891200000, 12144], [1288569600000, 10577], [1291161600000, 10295]];
            var d2 = [[1262304000000, 5], [1264982400000, 200], [1267401600000, 1605], [1270080000000, 6129], [1272672000000, 11643], [1275350400000, 19055], [1277942400000, 30062], [1280620800000, 39197], [1283299200000, 37000], [1285891200000, 27000], [1288569600000, 21000], [1291161600000, 17000]];

            var data1 = [
                { label: "Data 1", data: d1, color: '#17a084'},
                { label: "Data 2", data: d2, color: '#127e68' }
            ];
            $.plot($("#flot-chart1"), data1, {
                xaxis: {
                    tickDecimals: 0
                },
                series: {
                    lines: {
                        show: true,
                        fill: true,
                        fillColor: {
                            colors: [{
                                opacity: 1
                            }, {
                                opacity: 1
                            }]
                        },
                    },
                    points: {
                        width: 0.1,
                        show: false
                    },
                },
                grid: {
                    show: false,
                    borderWidth: 0
                },
                legend: {
                    show: false,
                }
            });

            var lineData = {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [
                    {
                        label: "Example dataset",
                        fillColor: "rgba(220,220,220,0.5)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [65, 59, 40, 51, 36, 25, 40]
                    },
                    {
                        label: "Example dataset",
                        fillColor: "rgba(26,179,148,0.5)",
                        strokeColor: "rgba(26,179,148,0.7)",
                        pointColor: "rgba(26,179,148,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(26,179,148,1)",
                        data: [48, 48, 60, 39, 56, 37, 30]
                    }
                ]
            };

            var lineOptions = {
                scaleShowGridLines: true,
                scaleGridLineColor: "rgba(0,0,0,.05)",
                scaleGridLineWidth: 1,
                bezierCurve: true,
                bezierCurveTension: 0.4,
                pointDot: true,
                pointDotRadius: 4,
                pointDotStrokeWidth: 1,
                pointHitDetectionRadius: 20,
                datasetStroke: true,
                datasetStrokeWidth: 2,
                datasetFill: true,
                responsive: true,
            };


            var ctx = document.getElementById("lineChart").getContext("2d");
            var myNewChart = new Chart(ctx).Line(lineData, lineOptions);

        });
    </script>
    <script src="js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
    <!-- Piwik -->
    <script type="text/javascript">
        var pkBaseURL = "https://stats.kde.org/";
        document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
        piwik_action_name = 'Experience Freedom!';
        piwik_idsite = 1;
        piwik_url = pkBaseURL + "piwik.php";
        piwik_log(piwik_action_name, piwik_idsite, piwik_url);
    </script>
    <script type="text/javascript" src="/media/javascripts/piwikbanner.js"></script>
    <object><noscript><p>Analytics <img src="https://stats.kde.org/piwik.php?idsite=1" style="border:0" alt=""/></p></noscript></object>
    <!-- End Piwik Tag -->
</body>
</html>
