<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:url" content="https://www.kde.org/fundraisers/randameetings2016/"/>
    <meta property="og:image" content="https://www.kde.org/fundraisers/randameetings2016/images/fundraising2016-ogimage.png"/>
    <meta property="og:type" content="website"/>
    <meta property="og:description" content="KDE is one of the biggest free software communities in the world and has been delivering high quality technology for nearly two decades. Randa Meetings is the largest sprint organized by KDE and, in 2016, it will be centered on bringing KDE technology on every device. This fundraising campaign aims at supporting the continuity of KDE efforts, over the year, towards this goal."/>
  
    <title>KDE &#8210; Randa Meetings 2016 Fundraising Campaign</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>

<body class="top-navigation" data-spy="scroll" data-target="#navbar" onload="fixNavbarOffset()">

    <?php
        $page_disablekdeevdonatebutton = true;
        $page_title="Randa Meetings 2016 fundraising";

        $goal=24000;
        $enddate="2016-07-22";
        $daystogo=floor((strtotime($enddate)-time())/(60*60*24));

        require("www_config.php");

        $res = $dbConnection->prepare("SELECT *, UNIX_TIMESTAMP(date) AS date_t FROM randameetings2016donations ORDER BY date DESC;");
        $res->execute();
        $total = 0;
        $count = 0;
        $table = "";
        while ($row = $res->fetch()) {
                $msg = htmlspecialchars($row["message"]);
                if ($msg == "") {
                        $msg = "<i>Anonymous donation</i>";
                }
                $total += $row["amount"];
                $count++;

                $table.="<tr>";
                $table.="<td>".$row["id"]."</td>";
                $table.="<td>".date("jS F Y", $row["date_t"])."</td>";
                $table.="<td>&euro;".number_format($row["amount"],2)."</td>";
                $table.="<td>".$msg."</td>";
                $table.="</tr>";
        }
        $table.="";

        $percent=round($total * 100 / $goal);
        $percent=min($percent, 100);
        $graph_style = "width: 100%; height: 30px; border: 1px solid #888; background: rgb(204,204,204);
                        position: relative;";
        $bar_style = "height: 30px; background: rgb(68,132,242); width: ".$percent."%";
    ?>

    <div id="wrapper">
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom white-bg">
                <nav class="navbar navbar-fixed-top" role="navigation">
                    <div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                            <i class="fa fa-reorder"></i>
                        </button>
                        <a href="https://ev.kde.org/" class="navbar-brand" target="_blank">
                            <img class="img-responsive" src="images/logo.png" alt="logo"/>
                        </a>
                    </div>
                    <div class="navbar-collapse collapse" id="navbar">
                        <ul class="nav navbar-nav">
                            <li class="active">
                                <a aria-expanded="false" role="button" href="#">Randa Meetings 2016 Fundraising Campaign</a>
                            </li>
                            <li>
                                <a role="button" href="#about"> About </a>
                            </li>
                            <li>
                                <a role="button" href="#donations"> Donations </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <br/><br/>
            <div class="wrapper wrapper-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="ibox float-e-margins">
                                <img class="img-responsive" src="images/banner-fundraising2016.png" alt="banner"/>
                            </div>
                        </div>
                    </div>
                    <?php
                      if(isset($_GET["returning"]) && trim($_GET["returning"]) == 'true') {
                        $alert="<div class='row'>";
                        $alert.="<div class='col-md-12'>";
                        $alert.="<div class='alert alert-info text-center'>";
                        $alert.="<h2>Thanks for donating to Randa Meetings 2016 Fundraising Campaign!</h2>";
                        $alert.="</div></div></div>";
                        echo $alert;
                      }
                    ?>
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class='alert alert-info text-center'>
                                <h5><strong>[update]</strong> The Randa Meetings 2016 Fundraiser has finished. Thank you everybody who supported us in this fundraiser. We didn't reach the set goal but we collected quite some money and that means there will be more KDE Sprints thanks to your support! See <a href="http://planet.kde.org" target="_blank">Planet KDE</a> for more information to come and go to the <a href="https://www.kde.org/community/donations/index.php" target="_blank">KDE donation page</a> if you want to support us further.</h5>
                            </div>
                        </div>
<!--
                        <div class='col-md-12'>
                            <div class='alert alert-info text-center'>
                                <h5><strong>[update]</strong> The fundraising campaign has been extended until 21th July. By extending the campaign, more information can be provided about what was actually accomplished in Randa Meetings 2016 and how you can support the work we will do in next months. Thanks a lot for your valuable donation!</h5>
                            </div>
                        </div>a
-->
                    </div>
                    <div class="row">
                        <div class="col-md-7">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Randa Meetings 2016 &#8210; KDE technology on every device!</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content text-center">
                                    <div class="embed-responsive embed-responsive-4by3">
                                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/yua6M9jqoEk"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Status</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins pull-right text-success"><i class="fa fa-users"></i></h1>
                                    <h1 class="no-margins"><?php echo $count; ?></h1>
                                    <small>Total backers</small>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins pull-right text-success"><i class="fa fa-tachometer"></i></h1>
                                    <h1 class="no-margins current-amount">&euro;<?php echo $total; ?></h1>
                                    <small>Pledged of &euro;<?php echo $goal; ?> goal</small>
                                    <p></p>
                                    <h2><?php echo $percent; ?>%</h2>
                                    <div class="progress progress-mini">
                                        <?php echo "<div style='width: ".$percent."%;' class='progress-bar'></div>"; ?>
                                    </div>
                                </div>
<?php /*
                                <div class="ibox-content">
                                    <h1 class="no-margins pull-right text-success"><i class="fa fa-calendar"></i></h1>
                                    <h1 class="no-margins"><?php echo $daystogo; ?></h1>
                                    <small>Days to go</small>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins pull-right text-success"><i class="fa fa-heart"></i></h1>
                                    <a data-toggle="modal" class="btn btn-success" href="#modal-form">Back this campaign</a>a
                                    
                                    <div id="modal-form" class="modal fade" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12 b-r"><h3 class="m-t-none m-b">Donate to Randa Meetings 2016 Fundraising Campaign</h3>
                                                            <p>Your support is quite appreciated!</p>
                                                            <form role="form" action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                                                <input type="hidden" name="business" value="kde-ev-paypal@kde.org">
                                                                <input type="hidden" name="cmd" value="_donations" />
                                                                <input type="hidden" name="lc" value="GB" />
                                                                <input type="hidden" name="item_name" value="KDE e.V. - Donation to Randa Meetings 2016 Fundraising Campaign" />
                                                                <input type="hidden" name="currency_code" value="EUR" />
                                                                <input type="hidden" name="cbt" value="Return to www.kde.org" />
                                                                <input type="hidden" name="on0" value="Show name on donor List" />
                                                                <input type="hidden" name="no_note" value="1" />
                                                                <input type="hidden" name="return" value="https://www.kde.org/fundraisers/randameetings2016/index.php?returning=true">
                                                                <input type="hidden" name="notify_url" value="https://www.kde.org/fundraisers/randameetings2016/notify.php">

                                                                <div class="form-group"><label>Amount (&euro;)</label> <input type="text" class="form-control" name="amount" value='50'></div>
                                                                
                                                                <div class="form-group">
                                                                    <label>Show my name on the <a href="#donations">donors list</a>?</label>
                                                                    <div class="i-checks"><label><input type="radio" value="Yes" name="os0"> <i></i> Yes </label></div>
                                                                    <div class="i-checks"><label><input type="radio" value="No" name="os0"> <i></i> No </label></div>
                                                                </div>
                                                                
                                                                <div>
                                                                    <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Donate</strong></button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
 */ ?>
                            </div>
                        </div>
                    </div>
                    <div class="row anchor" id="about">
                        <div class="col-md-7">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>About this fundraising campaign</h5>
                                </div>
                                <div class="ibox-content">
                                    <p><a href="http://www.kde.org" target="_blank"><strong>KDE</strong></a> is one of the biggest free software communities in the world and has been delivering high quality technology and spreading the principles of hacker culture for nearly two decades. KDE brings together users, developers, maintainers, translators and many more contributors from across six continents and over fifty countries, all of them working with the bonds and spirits of a truthful community.</p>
                                    <p>KDE has a vision:</p>
                                    <div class='row'>
                                        <div class='col-md-12'>
                                            <div class='alert alert-info text-center'>
                                                <h5>A world in which everyone has control over their digital life and enjoys freedom and privacy</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <p><a href="http://randa-meetings.ch/" target="_blank"><strong>Randa Meetings</strong></a> is the largest sprint organized by KDE. Roughly fifty KDE contributors meet yearly at Swiss alps to enjoy seven days of intense in-person work, pushing KDE technologies forward and discussing how to address the next-generation demands for software systems. One of the biggest challenges we've historically had for KDE technology is that we haven't completely reached the platforms where all of our users work. Aligned with our vision and pursuing the delivery of KDE technologies to every user, Randa Meetings 2016 &#8210; which happened from 12th to 19th June &#8210; was centered on bringing <strong>KDE technology on every device</strong>.</p>
                                    <p>This fundraising campaign aims at supporting the continuity of KDE efforts, over the year, towards the following goal:</p>
                                    <div class='row'>
                                        <div class='col-md-12'>
                                            <div class='alert alert-info text-center'>
                                                <h5>KDE technology on every device; be it in your pocket, in your bag or perched on your table; with the clear intention to start delivering software that will empower its users regardless of the platform they use</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <p>Different teams worked in this direction at Randa Meetings 2016 and are still working on it. Making software work on the platforms is not everything that's needed. We'll also need to have the infrastructure that makes sure this effort will have continuity over time, maintaining the quality our users deserve. This also means investing quite some effort in the KDE DevOps technology such as the CI (Continuous Integration) system for the different platforms and mechanisms to properly distribute binaries.</p>
                                </div>
                            </div>
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>How are we doing that?</h5>
                                </div>
                                <div class="ibox-content">
                                    <p>Randa Meetings 2016 activities and subsequent efforts were centered around three important topics:</p>
                                    <ol>
                                        <li>Have a defined set of KDE Applications ported to specific platform of interest.</li>
                                        <li>Come up with a reasonably generic plan to distribute on each platform.</li>
                                        <li>Come up with a sustainability plan for keeping those applications and binaries up to date.</li>
                                    </ol>
                                    <p>Here's a list of applications and platforms that were tackled by Randa Meetings 2016 participants: <a href="https://marble.kde.org/" target="_blank">Marble</a> on Android and Linux; <a href="https://kdenlive.org/" target="_blank">Kdenlive</a> on Windows; <a href="https://edu.kde.org/applications/s/artikulate" target="_blank">Artikulate</a> on Android; <a href="https://www.calligra.org/" target="_blank">Calligra</a> on Android; <a href="https://inqlude.org/groups/kde-frameworks.html" target="_blank">KDE Frameworks</a> on Android and Windows; <a href="https://kate-editor.org/" target="_blank">Kate</a> on OS X and Windows; <a href="https://www.kdevelop.org/" target="_blank">KDevelop</a> on OS X and Windows; <a href="https://cmollekopf.wordpress.com/2016/03/02/so-what-is-kube-and-who-is-sink/" target="_blank">Kube</a> on Windows, OS X, and Android; <a href="https://community.kde.org/KDEConnect" target="_blank">KDE Connect</a> on Windows; and <a href="https://edu.kde.org/labplot/" target="_blank">LabPlot</a> on OS X and Windows.</p>
                                    <p>As for distribution efforts, we investigated the use of technologies such as <a href="http://appimage.org/" target="_blank">AppImage</a> and <a href="http://flatpak.org/" target="_blank">Flatpak</a> to build distribution-independent bundled applications.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Randa Meetings 2015 &#8210; Selected Pictures</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="carousel slide" id="carousel1">
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <a href="images/21109560869_983ea46190_k.jpg" title="Randa Meetings 2015 Group Photo" data-gallery=""><img alt="image" class="img-responsive" src="images/21109560869_983ea46190_k.jpg"></a>
                                            </div>
                                            <div class="item">
                                                <a href="images/21115798180_d04ffc1550_k.jpg" title="Plasma Team - Full Concentration" data-gallery=""><img alt="image" class="img-responsive" src="images/21115798180_d04ffc1550_k.jpg"></a>
                                            </div>
                                            <div class="item">
                                                <a href="images/21116816609_1690396139_k.jpg" title="Lunch Time" data-gallery=""><img alt="image" class="img-responsive" src="images/21116816609_1690396139_k.jpg"></a>
                                            </div>
                                            <div class="item">
                                                <a href="images/21117288038_b999313216_k.jpg" title="GCompris in Action" data-gallery=""><img alt="image" class="img-responsive" src="images/21117288038_b999313216_k.jpg"></a>
                                            </div>
                                            <div class="item">
                                                <a href="images/21143953398_b059582585_k.jpg" title="Cooperation Unites All of Us" data-gallery=""><img alt="image" class="img-responsive" src="images/21143953398_b059582585_k.jpg"></a>
                                            </div>
                                            <div class="item">
                                                <a href="images/21294641562_30f9c184d8_k.jpg" title="Unleashed Discussions" data-gallery=""><img alt="image" class="img-responsive" src="images/21294641562_30f9c184d8_k.jpg"></a>
                                            </div>
                                            <div class="item">
                                                <a href="images/21304127075_1cf6e32597_k.jpg" title="Quite Picky Users" data-gallery=""><img alt="image" class="img-responsive" src="images/21304127075_1cf6e32597_k.jpg"></a>
                                            </div>
                                            <div class="item">
                                                <a href="images/21304182125_b58177bf31_k.jpg" title="The Last Mile" data-gallery=""><img alt="image" class="img-responsive" src="images/21304182125_b58177bf31_k.jpg"></a>
                                            </div>
                                            <div class="item">
                                                <a href="images/21313283681_2e5abf6472_k.jpg" title="The Perfect Atmosphere" data-gallery=""><img alt="image" class="img-responsive" src="images/21313283681_2e5abf6472_k.jpg"></a>
                                            </div>
                                            <div class="item">
                                                <a href="images/21321156942_28cfc504a9_k.jpg" title="Randa Meetings Rocks" data-gallery=""><img alt="image" class="img-responsive" src="images/21321156942_28cfc504a9_k.jpg"></a>
                                            </div>
                                            <div id="blueimp-gallery" class="blueimp-gallery">
                                                <div class="slides"></div>
                                                <h3 class="title"></h3>
                                                <a class="prev">‹</a>
                                                <a class="next">›</a>
                                                <a class="close">×</a>
                                                <a class="play-pause"></a>
                                                <ol class="indicator"></ol>
                                            </div>
                                        </div>
                                        <a data-slide="prev" href="#carousel1" class="left carousel-control">
                                            <span class="icon-prev"></span>
                                        </a>
                                        <a data-slide="next" href="#carousel1" class="right carousel-control">
                                            <span class="icon-next"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Randa Meetings 2015 &#8210; Short Presentations</h5>
                                </div>
                                <div class="ibox-content">
                                    <br/>
                                    <div class="tabs-container">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#tab-1"><small>Calligra</small></a></li>
                                            <li class=""><a data-toggle="tab" href="#tab-2"><small>CI</small></a></li>
                                            <li class=""><a data-toggle="tab" href="#tab-3"><small>GCompris</small></a></li>
                                            <li class=""><a data-toggle="tab" href="#tab-4"><small>KDE on Android</small></a></li>
                                            <li class=""><a data-toggle="tab" href="#tab-5"><small>Plasma</small></a></li>
                                            <li class=""><a data-toggle="tab" href="#tab-6"><small>QML Web</small></a></li>
                                            <li class=""><a data-toggle="tab" href="#tab-7"><small>VDG</small></a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="tab-1" class="tab-pane active">
                                                <div class="panel-body">
                                                    <figure>
                                                        <video id="my_video_1" controls data-setup="{}">
                                                            <source src="https://files.kde.org/randa/Calligra.mp4" type='video/mp4'>
                                                        </video>
                                                    </figure>
                                                </div>
                                            </div>
                                            <div id="tab-2" class="tab-pane">
                                                <div class="panel-body">
                                                    <figure>
                                                        <video id="my_video_2" controls data-setup="{}">
                                                            <source src="https://files.kde.org/randa/Continuous%20Integration.mp4" type='video/mp4'>
                                                        </video>
                                                    </figure>
                                                </div>
                                            </div>
                                            <div id="tab-3" class="tab-pane">
                                                <div class="panel-body">
                                                    <figure>
                                                        <video id="my_video_3" controls data-setup="{}">
                                                            <source src="https://files.kde.org/randa/GCompris.mp4" type='video/mp4'>
                                                        </video>
                                                    </figure>
                                                </div>
                                            </div>
                                            <div id="tab-4" class="tab-pane">
                                                <div class="panel-body">
                                                    <figure>
                                                        <video id="my_video_4" controls data-setup="{}">
                                                            <source src="https://files.kde.org/randa/KDEonAndroid.mp4" type='video/mp4'>
                                                        </video>
                                                    </figure>
                                                </div>
                                            </div>
                                            <div id="tab-5" class="tab-pane">
                                                <div class="panel-body">
                                                    <figure>
                                                        <video id="my_video_5" controls data-setup="{}">
                                                            <source src="https://files.kde.org/randa/Plasma.mp4" type='video/mp4'>
                                                        </video>
                                                    </figure>
                                                </div>
                                            </div>
                                            <div id="tab-6" class="tab-pane">
                                                <div class="panel-body">
                                                    <figure>
                                                        <video id="my_video_6" controls data-setup="{}">
                                                            <source src="https://files.kde.org/randa/QMLweb.mp4" type='video/mp4'>
                                                        </video>
                                                    </figure>
                                                </div>
                                            </div>
                                            <div id="tab-7" class="tab-pane">
                                                <div class="panel-body">
                                                    <figure>
                                                        <video id="my_video_7" controls data-setup="{}">
                                                            <source src="https://files.kde.org/randa/VDG.mp4" type='video/mp4'>
                                                        </video>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Randa Meetings 2015 &#8210; Final Report</h5>
                                </div>
                                <div class="ibox-content">
                                    <br/>
                                    <div class="tabs-container">
                                        <div class="alert alert-success text-center">
                                            Read the <a class="alert-link" href="https://dot.kde.org/2015/12/07/randa-meetings-2015-huge-success-again" target="_blank">Randa Meetings 2015 Final Report</a>!
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row anchor" id="helpexamples">
                        <div class="col-md-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Help make this vision come true!</h5>
                                </div>
                                <div class="ibox-content">
                                    <p>Some examples of the direct connection between your donation and the realization of KDE technology on every device:</p>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="widget style1 navy-bg">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <i class="fa fa-user fa-5x"></i>
                                                    </div>
                                                    <div class="col-md-10 text-right">
                                                        <span>You + five friends support a Randa Meetings 2016's participant</span>
                                                        <h2 class="font-bold">&euro;50 each</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="widget style1 lazur-bg">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <i class="fa fa-server fa-5x"></i>
                                                    </div>
                                                    <div class="col-md-10 text-right">
                                                        <span>You + a friend support our CI and development infrastructure</span>
                                                        <h2 class="font-bold">&euro;100 each</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="widget style1 yellow-bg">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <i class="fa fa-gear fa-5x"></i>
                                                    </div>
                                                    <div class="col-md-10 text-right">
                                                        <span>You keep the ball rolling by supporting other sprints by the end of the year</span>
                                                        <h2 class="font-bold">&euro;300</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>All donations to Randa Meetings 2016 Fundraising Campaign are one-time donations!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row anchor" id="donations">
                        <div class="col-md-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>List of donations</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date </th>
                                                <th>Amount </th>
                                                <th>Donor Name </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <?php echo $table; ?>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="row text-center">
            <a href="https://ev.kde.org/" target="_blank">
                <img src="images/logo.png" alt=""/>
            </a>
        </div>
        <div class="social-icons">
            <ul>
                <li>
                    <a class="envelope" href="mailto:kde-ev-board@kde.org">
                        <i class="fa fa-envelope" title="Contact the KDE e.V. Board of Directors"></i>
                    </a>
                </li>
                <li>
                    <a class="twitter" href="https://twitter.com/kdecommunity" target="_blank">
                        <i class="fa fa-twitter" title="The KDE Community - Twitter"></i>
                    </a>
                </li>
                <li>
                    <a class="dribbble" href="https://plus.google.com/105126786256705328374/" target="_blank">
                        <i class="fa fa-google-plus" title="The KDE Community - Google+"></i>
                    </a>
                </li>
                <li>
                    <a class="facebook" href="https://www.facebook.com/kde/" target="_blank">
                        <i class="fa fa-facebook" title="The KDE Community - Facebook"></i>
                    </a>
                </li>
                <li>
                    <a class="linkedin" href="https://dot.kde.org" target="_blank">
                        <i class="fa fa-newspaper-o" title="dot.kde.org"></i>
                    </a>
                </li>
            </ul>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12 text-center">
                  The Randa Meetings 2016 fundraising campaign's banner on top of this page is a derivative of <a href="https://en.wikipedia.org/wiki/Post-PC_era#/media/File:The_iOS_family_pile_%282012%29.jpg" target="_blank">Blake Patterson's work</a>, used under <a href="http://creativecommons.org/licenses/by/2.0" target="_blank">CC BY 2.0</a>. This banner is licensed under <a href="http://creativecommons.org/licenses/by/2.0" target="_blank">CC BY 2.0</a> by <a href="http://sandroandrade.org" target="_blank">Sandro Andrade</a>.
            </div>
            <div class="col-md-12 text-center">
                KDE<sup>&#174;</sup> and <a href="/media/images/trademark_kde_gear_black_logo.png">the K Desktop Environment<sup>&#174;</sup> logo</a> are registered trademarks of <a href="http://ev.kde.org/" title="Homepage of the KDE non-profit Organization">KDE e.V.</a> | <a href="http://www.kde.org/community/whatiskde/impressum.php">Legal</a>
            </div>
        </div>
  </div>
    <!-- Mainly scripts -->
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- Flot -->
    <script src="js/plugins/flot/jquery.flot.js"></script>
    <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="js/plugins/flot/jquery.flot.resize.js"></script>

    <!-- ChartJS-->
    <script src="js/plugins/chartJs/Chart.min.js"></script>

    <!-- Peity -->
    <script src="js/plugins/peity/jquery.peity.min.js"></script>
    <!-- Peity demo -->
    <script src="js/demo/peity-demo.js"></script>


    <script>
        $(document).ready(function() {


            var d1 = [[1262304000000, 6], [1264982400000, 3057], [1267401600000, 20434], [1270080000000, 31982], [1272672000000, 26602], [1275350400000, 27826], [1277942400000, 24302], [1280620800000, 24237], [1283299200000, 21004], [1285891200000, 12144], [1288569600000, 10577], [1291161600000, 10295]];
            var d2 = [[1262304000000, 5], [1264982400000, 200], [1267401600000, 1605], [1270080000000, 6129], [1272672000000, 11643], [1275350400000, 19055], [1277942400000, 30062], [1280620800000, 39197], [1283299200000, 37000], [1285891200000, 27000], [1288569600000, 21000], [1291161600000, 17000]];

            var data1 = [
                { label: "Data 1", data: d1, color: '#17a084'},
                { label: "Data 2", data: d2, color: '#127e68' }
            ];
            $.plot($("#flot-chart1"), data1, {
                xaxis: {
                    tickDecimals: 0
                },
                series: {
                    lines: {
                        show: true,
                        fill: true,
                        fillColor: {
                            colors: [{
                                opacity: 1
                            }, {
                                opacity: 1
                            }]
                        },
                    },
                    points: {
                        width: 0.1,
                        show: false
                    },
                },
                grid: {
                    show: false,
                    borderWidth: 0
                },
                legend: {
                    show: false,
                }
            });

            var lineData = {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [
                    {
                        label: "Example dataset",
                        fillColor: "rgba(220,220,220,0.5)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [65, 59, 40, 51, 36, 25, 40]
                    },
                    {
                        label: "Example dataset",
                        fillColor: "rgba(26,179,148,0.5)",
                        strokeColor: "rgba(26,179,148,0.7)",
                        pointColor: "rgba(26,179,148,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(26,179,148,1)",
                        data: [48, 48, 60, 39, 56, 37, 30]
                    }
                ]
            };

            var lineOptions = {
                scaleShowGridLines: true,
                scaleGridLineColor: "rgba(0,0,0,.05)",
                scaleGridLineWidth: 1,
                bezierCurve: true,
                bezierCurveTension: 0.4,
                pointDot: true,
                pointDotRadius: 4,
                pointDotStrokeWidth: 1,
                pointHitDetectionRadius: 20,
                datasetStroke: true,
                datasetStrokeWidth: 2,
                datasetFill: true,
                responsive: true,
            };


            var ctx = document.getElementById("lineChart").getContext("2d");
            var myNewChart = new Chart(ctx).Line(lineData, lineOptions);

        });
    </script>
    <script src="js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
    <!-- Piwik -->
    <script type="text/javascript">
        var pkBaseURL = "https://stats.kde.org/";
        document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
        piwik_action_name = 'Experience Freedom!';
        piwik_idsite = 1;
        piwik_url = pkBaseURL + "piwik.php";
        piwik_log(piwik_action_name, piwik_idsite, piwik_url);
    </script>
    <script type="text/javascript" src="/media/javascripts/piwikbanner.js"></script>
    <object><noscript><p>Analytics <img src="https://stats.kde.org/piwik.php?idsite=1" style="border:0" alt=""/></p></noscript></object>
    <!-- End Piwik Tag -->
</body>

</html>
